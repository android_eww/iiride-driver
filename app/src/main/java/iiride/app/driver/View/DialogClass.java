package iiride.app.driver.View;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import iiride.app.driver.R;


public class DialogClass {

    private Context context;
    private int style=0;
    private Dialog dialog;

    public DialogClass(Context context, int style)
    {
        this.context = context;
        this.style = style;

        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.open_loader_new, null);

        dialog = new Dialog(context,0);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(view);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    public void showDialog()
    {
        Log.e("call dialog","Dialog class dialog.showDialog()");
        dialog.show();
    }
    public boolean isShowingDialog()
    {
        Log.e("call dialog","Dialog class dialog.showDialog()");
        return dialog.isShowing();
    }

    public void hideDialog()
    {
        if (dialog!=null)
        {
            if (dialog.isShowing())
            {
                Log.e("call dialog","Dialog class dialog.dismiss()");
//                ttt=0;
                dialog.dismiss();
            }
            else
            {
                Log.e("call","Dialog class isShowing = false");
            }
        }
        else
        {
            Log.e("call","dialog = null");
        }
    }

  /*  public DialogClass(Context context, int style)
    {
        this.context = context;
        this.style = style;
    }

    public void showDialog()
    {
        showPopup();
    }

    public void hideDialog()
    {
        if (dialog!=null)
        {
            if (dialog.isShowing())
            {
                Log.e("call","Dialog class dialog.dismiss()");
                dialog.dismiss();
            }
            else
            {
                Log.e("call","Dialog class isShowing = false");
            }
        }
        else
        {
            Log.e("call","dialog = null");
        }
    }



    /*//******************************** showPopup() *****************************************************************************************
    private void showPopup() {

        Log.e("call","Dialog class dialog.showDialog()");
        AVLoadingIndicatorView avi;

        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.open_loader_new, null);

        avi = (AVLoadingIndicatorView)view.findViewById(R.id.avi);

        dialog = new Dialog(context,0);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(view);


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();


    }//End...*/
}
