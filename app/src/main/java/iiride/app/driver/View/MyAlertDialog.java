package iiride.app.driver.View;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import androidx.core.content.ContextCompat;
import android.text.Html;

import iiride.app.driver.R;


public class MyAlertDialog extends AlertDialog.Builder {

    private Context context;
    private int flag;

    public MyAlertDialog(Context context) {
        super(context);
        this.context=context;

    }

    public void setAlertDialog(int flag,String message)
    {
        this.flag = flag;

        if (flag==1)
        {
            showOKDialog(message);
        }
        else if(flag==2)
        {
            showOkCancleDialog(message);
        }
    }

    public void showOKDialog(String message)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
//        builder.setTitle(context.getString(R.string.dialog_title));
        builder.setCancelable(false);
        builder.setMessage(Html.fromHtml("<font color='#000000'>"+message.trim()+"</font>"));

        String positiveText = "Ok";
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // positive button logic
                        dialog.dismiss();
                    }
                });

        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(context, R.color.colorRed));
            }
        });
        dialog.show();
    }

    public void showOkCancleDialog(String message)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage(Html.fromHtml("<font color='#000000'>"+message.trim()+"</font>"));
        String positiveText = "Ok";
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // positive button logic
                        dialog.dismiss();
                    }
                });

        String negativeText = "Cancel";
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // negative button logic
                        dialog.dismiss();
                    }
                });

        final AlertDialog dialog = builder.create();
        // display dialog

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(context.getResources().getColor(R.color.colorRed));
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(context.getResources().getColor(R.color.colorRed));
            }
        });

        dialog.show();
    }
}
