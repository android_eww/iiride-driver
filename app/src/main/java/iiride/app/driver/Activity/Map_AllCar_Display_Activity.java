package iiride.app.driver.Activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import iiride.app.driver.Application.TiCKTOC_Driver_Application;
import iiride.app.driver.Comman.Constants;
import iiride.app.driver.Comman.WebServiceAPI;
import iiride.app.driver.Others.GPSTracker;


import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Map_AllCar_Display_Activity extends AppCompatActivity implements View.OnClickListener{

    Map_AllCar_Display_Activity activity;
    MapView mMapView;

    GoogleMap googleMap;
    GPSTracker gpsTracker;
    Handler handler;
    double latitude, longitude;
    Marker marker, markerDrivers;
    ImageView iv_cancel_dialog;

    private AQuery aQuery;
    DialogClass dialogClass;
    String TAG = "ShowAllDriver";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_all_car_display);

        activity = Map_AllCar_Display_Activity.this;

        aQuery = new AQuery(activity);

        Init(savedInstanceState);
    }

    private void Init(Bundle savedInstanceState) {

        iv_cancel_dialog = (ImageView) findViewById(R.id.iv_cancel_dialog);
        mMapView = (MapView) findViewById(R.id.mapView_all_car);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume();
        mMapView.setOnClickListener(this);
        iv_cancel_dialog.setOnClickListener(this);
        // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                // For showing a move to my location button
                if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    return;
                }
                googleMap.setMyLocationEnabled(false);
                // For dropping a marker at a point on the Map
                RefreshLocation();
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.mapView_all_car:
                onBackPressed();
                break;

            case R.id.iv_cancel_dialog:
                onBackPressed();
                break;
        }

    }

    public void RefreshLocation()
    {
        GPSTracker tracker = new GPSTracker(activity);
        if (!tracker.canGetLocation())
        {
            tracker.showSettingsAlert();
        }
        else
        {
            UpdateLocation();
        }
    }

    private void UpdateLocation()
    {
        gpsTracker = new GPSTracker(activity);
        gpsTracker.getLocation();

        Constants.newgpsLatitude = gpsTracker.getLatitude()+"";
        Constants.newgpsLongitude = gpsTracker.getLongitude()+"";

        Log.e("getLatitude()", "rrrrrrrrrrrrrr" + gpsTracker.getLatitude());
        Log.e("getLongitude()", "rrrrrrrrrrrrrr" + gpsTracker.getLongitude());
        Log.e("latitude", "rrrrrrrrrrrrrr" + Constants.newgpsLatitude);
        Log.e("longitude", "rrrrrrrrrrrrr" + Constants.newgpsLongitude);

        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();

        LatLng sydney = new LatLng(latitude, longitude);
        if (marker != null)
        {
            marker.remove();
        }
        marker = googleMap.addMarker(new MarkerOptions().position(sydney).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

        // For zooming automatically to the location of the marker
        CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(12).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        ShowAllDriver();
    }

    private void ShowAllDriver()
    {
        dialogClass = new DialogClass(activity, 1);
        dialogClass.showDialog();

        Map<String, Object> params = new HashMap<String, Object>();

        String url = "http://54.206.55.185/web/Passenger_Api/Driver/";

        Log.e(TAG, "URL = " + url);
        Log.e(TAG, "PARAMS = " + params);

        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>(){

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {

                    int responseCode = status.getCode();
                    Log.e(TAG, "responseCode\n\nUpdateDriverProfile = " + responseCode);
                    Log.e(TAG, "Response\n\nUpdateDriverProfile = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");
                                if (json.has("drivers"))
                                {
                                    JSONArray driverArray = json.getJSONArray("drivers");
                                    if (driverArray.length()>0)
                                    {
                                        String Id="", Fullname="", Lat="", Lng="";
                                        for (int i=0; i<driverArray.length(); i++)
                                        {
                                            JSONObject driverObj = driverArray.getJSONObject(i);
                                            if (driverObj.has("Id"))
                                            {
                                                Id= driverObj.getString("Id");
                                            }
                                            if (driverObj.has("Fullname"))
                                            {
                                                Fullname= driverObj.getString("Fullname");
                                            }
                                            if (driverObj.has("Lat"))
                                            {
                                                Lat= driverObj.getString("Lat");
                                            }
                                            if (driverObj.has("Lng"))
                                            {
                                                Lng= driverObj.getString("Lng");
                                            }

                                            if (Lat!=null && Lng!=null && !Lat.equalsIgnoreCase("") && !Lng.equalsIgnoreCase(""))
                                            {
                                                LatLng sydney = new LatLng(Double.parseDouble(Lat), Double.parseDouble(Lng));
                                                markerDrivers = googleMap.addMarker(new MarkerOptions().position(sydney).icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_car_from_top)).rotation(90).anchor(0.5f, 0.5f));
                                            }
                                        }
                                        dialogClass.hideDialog();
                                    }
                                    else
                                    {
                                        Log.e("driverObject", "null");
                                        dialogClass.hideDialog();
                                    }
                                }
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                            }
                        }
                        else
                        {
                            Log.e("status", "no status");
                            dialogClass.hideDialog();
                        }
                    }
                    else
                    {
                        Log.e(TAG, "getMessage = " + "null");
                        dialogClass.hideDialog();
                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG, "Exception = " + e.getMessage() + "something_is_wrong");
                    dialogClass.hideDialog();
                }


            }
        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, android.R.anim.fade_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            TiCKTOC_Driver_Application.setCurrentActivity(activity);
        }
    }
}
