package iiride.app.driver.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import iiride.app.driver.Application.TiCKTOC_Driver_Application;
import iiride.app.driver.Comman.WebServiceAPI;


import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ForgotPassword_Activity extends AppCompatActivity implements View.OnClickListener {

    ForgotPassword_Activity activity;

    ImageView iv_back;
    TextView tv_submit;
    EditText et_email_forgotPass;
    LinearLayout main_layout;

    DialogClass dialogClass;
    AQuery aQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        activity = ForgotPassword_Activity.this;

        aQuery = new AQuery(ForgotPassword_Activity.this);

        init();
    }

    private void init() {

        main_layout = (LinearLayout) findViewById(R.id.main_layout);

        iv_back = (ImageView) findViewById(R.id.iv_back);
        tv_submit = (TextView) findViewById(R.id.tv_submit);
        et_email_forgotPass = (EditText) findViewById(R.id.et_email_forgotPass);

        iv_back.setOnClickListener(this);
        tv_submit.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.tv_submit:
                SubmitEmail_ForgotPass();
                break;
        }
    }

    private void SubmitEmail_ForgotPass()
    {
        if (TextUtils.isEmpty(et_email_forgotPass.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_email),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else if(!isEmailValid(et_email_forgotPass.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_valid_email),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else
        {
            userForgotPassword();
        }
    }


    private void userForgotPassword()
    {
        dialogClass = new DialogClass(activity, 1);
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_FORGOT_PASSWORD;

        params.put(WebServiceAPI.FORGOT_PASSWORD_PARAM_EMAIL, et_email_forgotPass.getText().toString());

        Log.e("url", "userForgotPassword = " + url);
        Log.e("param", "userForgotPassword = " + params);


        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "userForgotPassword = " + responseCode);
                    Log.e("Response", "userForgotPassword = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");
                                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                                builder.setCancelable(false);
                                builder.setMessage(Html.fromHtml("<font color='#000000'>"+getResources().getString(R.string.your_password_reset_email_is_send)+"</font>"));

                                String positiveText = "Ok";
                                builder.setPositiveButton(positiveText,
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                                onBackPressed();
                                            }
                                        });

                                final AlertDialog dialog = builder.create();
                                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                    @Override
                                    public void onShow(DialogInterface arg0) {
                                        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
                                    }
                                });
                                dialog.show();
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message")) {
                                new SnackbarUtils(main_layout, json.getString("message"),
                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                    }
                } catch (Exception e) {
                    Log.e("userForgotPassword", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }.header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            TiCKTOC_Driver_Application.setCurrentActivity(activity);
        }
    }
}
