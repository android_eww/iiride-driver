package iiride.app.driver.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import iiride.app.driver.Adapter.CreditCardList_Adapter;
import iiride.app.driver.Application.TiCKTOC_Driver_Application;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;
import iiride.app.driver.Others.ConnectivityReceiver;


import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Wallet_Balance_TransferToBank_Activity extends AppCompatActivity implements View.OnClickListener
{
    public static Wallet_Balance_TransferToBank_Activity activity;

    LinearLayout ll_back, main_layout;
    ImageView iv_back;
    TextView tv_currentBalance, tv_selectCard, tv_accountHolder_Name, tv_Abn, tv_BankName, tv_BankAccountNo, tv_Bsb, tv_TransferToBank;
    EditText et_amountEnter;

    String walletBalance = "",ClickcardId="", holderName="", abn="", bankName="", bankAcBo="", bsb="";

    Intent intent;
    public static int flagResumeUse=0;

    private AQuery aQuery;
    DialogClass dialogClass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_balance_withdraw);

        activity = Wallet_Balance_TransferToBank_Activity.this;

        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 1);

        flagResumeUse=0;
        ClickcardId="";

        initUI();
    }

    private void initUI()
    {
        main_layout = (LinearLayout) findViewById(R.id.main_layout);
        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        tv_currentBalance = (TextView) findViewById(R.id.tv_currentBalance);
        tv_selectCard = (TextView) findViewById(R.id.tv_selectCard);

        tv_TransferToBank = (TextView) findViewById(R.id.tv_TransferToBank);
        tv_accountHolder_Name = (TextView) findViewById(R.id.tv_accountHolder_Name);
        tv_Abn = (TextView) findViewById(R.id.tv_Abn);
        tv_BankName = (TextView) findViewById(R.id.tv_BankName);
        tv_BankAccountNo = (TextView) findViewById(R.id.tv_BankAccountNo);
        tv_Bsb = (TextView) findViewById(R.id.tv_Bsb);
        et_amountEnter = (EditText) findViewById(R.id.et_amountEnter);

        ll_back.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        tv_selectCard.setOnClickListener(this);
        tv_TransferToBank.setOnClickListener(this);

        walletBalance = SessionSave.getUserSession(Comman.DRIVER_WALLET_BALLENCE, activity);
        if (walletBalance!=null && !walletBalance.equalsIgnoreCase(""))
        {
            tv_currentBalance.setText("$"+walletBalance);

            if (walletBalance!=null && !walletBalance.equalsIgnoreCase(""))
            {
                String mm = "$"+String.format("%.2f",Double.parseDouble(walletBalance));
                tv_currentBalance.setText(mm);
            }
            else
            {
                tv_currentBalance.setVisibility(View.GONE);
            }
        }
        else
        {
            tv_currentBalance.setText("--");
        }

        et_amountEnter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                if(charSequence.length()>0)
                {
                    if (charSequence.toString().trim().length()==1 && charSequence.toString().equalsIgnoreCase("."))
                    {
                        et_amountEnter.setText("");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        holderName = SessionSave.getUserSession(Comman.USER_BANK_ACCOUNT_HOLDER_NAME, activity);
        abn = SessionSave.getUserSession(Comman.USER_ABN, activity);
        bankName = SessionSave.getUserSession(Comman.USER_BANK_NAME, activity);
        bankAcBo = SessionSave.getUserSession(Comman.USER_BANK_AC_NO, activity);
        bsb = SessionSave.getUserSession(Comman.USER_BSB, activity);

        if (holderName!=null && !holderName.equalsIgnoreCase(""))
        {
            tv_accountHolder_Name.setText(holderName);
        }
        else
        {
            tv_accountHolder_Name.setText("");
        }

        if (abn!=null && !abn.equalsIgnoreCase(""))
        {
            tv_Abn.setText(abn);
        }
        else
        {
            tv_Abn.setText("");
        }

        if (bankName!=null && !bankName.equalsIgnoreCase(""))
        {
            tv_BankName.setText(bankName);
        }
        else
        {
            tv_BankName.setText("");
        }

        if (bankAcBo!=null && !bankAcBo.equalsIgnoreCase(""))
        {
            tv_BankAccountNo.setText(bankAcBo);
        }
        else
        {
            tv_BankAccountNo.setText("");
        }

        if (bsb!=null && !bsb.equalsIgnoreCase(""))
        {
            tv_Bsb.setText(bsb);
        }
        else
        {
            tv_Bsb.setText("");
        }
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.tv_selectCard:
                intent = new Intent(activity, Wallet_Cards_Activity.class);
                intent.putExtra("from","TransferToBank");
                startActivity(intent);
                break;

            case R.id.tv_TransferToBank:
                checkData();
                break;
        }
    }

    private void checkData() {
        if (TextUtils.isEmpty(et_amountEnter.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_amount),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else
        {
            String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
            if (driverId!=null && !driverId.equalsIgnoreCase(""))
            {
                if (ConnectivityReceiver.isConnected())
                {
                    callTransferToBank(driverId);
                }
                else
                {
                    new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }
    }

    private void callTransferToBank(String driverId)
    {
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();
        String url = WebServiceAPI.WEB_SERVICE_TRANSFERT_TO_BANK;

        params.put(WebServiceAPI.PARAM_TRANSFERT_TO_BANK_DRIVER_ID, driverId);
        params.put(WebServiceAPI.PARAM_TRANSFERT_TO_BANK_AMOUNT, et_amountEnter.getText().toString());
        params.put(WebServiceAPI.PARAM_TRANSFERT_TO_BANK_HOLDER_NAME, holderName);
        params.put(WebServiceAPI.PARAM_TRANSFERT_TO_BANK_ABN, abn );
        params.put(WebServiceAPI.PARAM_TRANSFERT_TO_BANK_BANK_NAME, bankName);
        params.put(WebServiceAPI.PARAM_TRANSFERT_TO_BANK_BSB, bsb);
        params.put(WebServiceAPI.PARAM_TRANSFERT_TO_BANK_ACCOUNT_NO, bankAcBo);

        Log.e("url", "callTransferToBank = " + url);
        Log.e("param", "callTransferToBank = " + params);

        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "callTransferToBank = " + responseCode);
                    Log.e("Response", "callTransferToBank = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");
                                if (json.has("walletBalance"))
                                {
                                    String walletBalance = json.getString("walletBalance");
                                    if (walletBalance!=null && !walletBalance.equalsIgnoreCase(""))
                                    {
                                        SessionSave.saveUserSession(Comman.DRIVER_WALLET_BALLENCE, walletBalance, activity);
                                        tv_currentBalance.setText("$" + walletBalance);
                                    }
                                }
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                }
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent intent = new Intent(activity, Wallet_Transfer_History_Activity.class);
                                        startActivity(intent);
                                        overridePendingTransition(R.anim.right_in, R.anim.left_out);
                                        finish();
                                    }
                                },1000);
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        } else {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message")) {
                                new SnackbarUtils(main_layout, json.getString("message"),
                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }
                    } else {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                    }
                } catch (Exception e) {
                    Log.e("callTransferToBank", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }.header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            TiCKTOC_Driver_Application.setCurrentActivity(activity);
        }
        if (flagResumeUse==1)
        {
            if (CreditCardList_Adapter.ClickecardNumber!=null && !CreditCardList_Adapter.ClickecardNumber.equalsIgnoreCase(""))
            {
                tv_selectCard.setText(CreditCardList_Adapter.ClickecardNumber);
                ClickcardId = CreditCardList_Adapter.ClickcardId;
            }
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity) != null && !SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("")
                && SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
        {
            Intent intent = new Intent(activity,Create_Passcode_Activity.class);
            startActivity(intent);

            if (Wallet_Balance_Activity.activity!=null)
            {
                Wallet_Balance_Activity.activity.finish();
            }
            if (Wallet_Balance_TopUp_Activity.activity!=null)
            {
                Wallet_Balance_TopUp_Activity.activity.finish();
            }
            if (Wallet_Cards_Activity.activity!=null)
            {
                Wallet_Cards_Activity.activity.finish();
            }
            if (Wallet_Transfer_Activity.activity!=null)
            {
                Wallet_Transfer_Activity.activity.finish();
            }
            if (Wallet_Transfer_History_Activity.activity!=null)
            {
                Wallet_Transfer_History_Activity.activity.finish();
            }
            finish();
        }
    }
}