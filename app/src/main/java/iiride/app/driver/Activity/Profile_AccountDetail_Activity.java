package iiride.app.driver.Activity;

import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import iiride.app.driver.Application.TiCKTOC_Driver_Application;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;
import iiride.app.driver.Others.ConnectivityReceiver;


import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Profile_AccountDetail_Activity extends AppCompatActivity implements View.OnClickListener{

    Profile_AccountDetail_Activity activity;

    ImageView iv_save;
    LinearLayout ll_save, main_layout, ll_back;

    EditText et_acountHolder_name, et_abn, et_bank_name, et_bsb, et_bankAccount, et_serviceDescription;

    DialogClass dialogClass;
    AQuery aQuery;
    String userId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_accountdetail);

        activity = Profile_AccountDetail_Activity.this;

        aQuery = new AQuery(activity);

        initUI();
    }

    private void initUI() {
        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        iv_save = (ImageView) findViewById(R.id.iv_save);

        ll_save = (LinearLayout) findViewById(R.id.ll_save);
        main_layout = (LinearLayout) findViewById(R.id.main_layout);

        et_acountHolder_name = (EditText) findViewById(R.id.et_acountHolder_name);
        et_abn = (EditText) findViewById(R.id.et_abn);
        et_bank_name = (EditText) findViewById(R.id.et_bank_name);
        et_bsb = (EditText) findViewById(R.id.et_bsb);
        et_bankAccount = (EditText) findViewById(R.id.et_bankAccount);
        et_serviceDescription = (EditText) findViewById(R.id.et_serviceDescription);


        if (SessionSave.getUserSession(Comman.USER_BANK_ACCOUNT_HOLDER_NAME,activity)!= null && !SessionSave.getUserSession(Comman.USER_BANK_ACCOUNT_HOLDER_NAME,activity).equalsIgnoreCase(""))
        {
            et_acountHolder_name.setText(SessionSave.getUserSession(Comman.USER_BANK_ACCOUNT_HOLDER_NAME,activity));
        }

        if (SessionSave.getUserSession(Comman.USER_ABN,activity)!= null && !SessionSave.getUserSession(Comman.USER_ABN,activity).equalsIgnoreCase(""))
        {
            et_abn.setText(SessionSave.getUserSession(Comman.USER_ABN,activity));
        }
        if (SessionSave.getUserSession(Comman.USER_BANK_NAME,activity)!= null && !SessionSave.getUserSession(Comman.USER_BANK_NAME,activity).equalsIgnoreCase(""))
        {
            et_bank_name.setText(SessionSave.getUserSession(Comman.USER_BANK_NAME,activity));
        }

        if (SessionSave.getUserSession(Comman.USER_BSB,activity)!= null && !SessionSave.getUserSession(Comman.USER_BSB,activity).equalsIgnoreCase(""))
        {
            et_bsb.setText(SessionSave.getUserSession(Comman.USER_BSB,activity));
        }

        if (SessionSave.getUserSession(Comman.USER_BANK_AC_NO,activity)!= null && !SessionSave.getUserSession(Comman.USER_BANK_AC_NO,activity).equalsIgnoreCase(""))
        {
            et_bankAccount.setText(SessionSave.getUserSession(Comman.USER_BANK_AC_NO,activity));
        }

        if (SessionSave.getUserSession(Comman.USER_SERVICE_DESCRIPTION,activity)!= null && !SessionSave.getUserSession(Comman.USER_SERVICE_DESCRIPTION,activity).equalsIgnoreCase(""))
        {
            et_serviceDescription.setText(SessionSave.getUserSession(Comman.USER_SERVICE_DESCRIPTION,activity));
        }


        ll_back.setOnClickListener(this);
        iv_save.setOnClickListener(this);
        ll_save.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.iv_save:
                CheckData();
                break;

            case R.id.ll_save:
                CheckData();
                break;
        }
    }

    private void CheckData()
    {
        if (TextUtils.isEmpty(et_acountHolder_name.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_company_holder_name),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_abn.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_abn),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_serviceDescription.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_service_description),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_bank_name.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_bank_name),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_bsb.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_bsb),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_bankAccount.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_bank_no),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else
        {
            userId = SessionSave.getUserSession(Comman.USER_ID, activity);
            if (userId!=null && !userId.equalsIgnoreCase(""))
            {
                if (ConnectivityReceiver.isConnected())
                {
                    UpdateAccountDetail(userId);
                }
                else
                {
                    new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }
    }

    private void UpdateAccountDetail(String userId)
    {
        /*DriverId,AccountHolderName,BankName,BankAcNo,BSB,ABN*/

        dialogClass = new DialogClass(activity, 1);
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_UPDATE_BANK_INFO ;

        params.put(WebServiceAPI.UPDATE_BANK_INFO_PARAM_DRIVER_ID, userId);
        params.put(WebServiceAPI.UPDATE_BANK_INFO_PARAM_ACCOUNT_HOLDER_NAME, et_acountHolder_name.getText().toString());
        params.put(WebServiceAPI.UPDATE_BANK_INFO_PARAM_BANK_NAME, et_bank_name.getText().toString());
        params.put(WebServiceAPI.UPDATE_BANK_INFO_PARAM_BANK_AC_NO, et_bankAccount.getText().toString());
        params.put(WebServiceAPI.UPDATE_BANK_INFO_PARAM_BSB, et_bsb.getText().toString());
        params.put(WebServiceAPI.UPDATE_BANK_INFO_PARAM_ABN, et_abn.getText().toString());
        params.put(WebServiceAPI.UPDATE_BANK_INFO_PARAM_SERVICE_DESCRIPTION, et_serviceDescription.getText().toString());

        Log.e("url", "UpdateCarDetail = " + url);
        Log.e("param", "UpdateCarDetail = " + params);


        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "UpdateCarDetail = " + responseCode);
                    Log.e("Response", "UpdateCarDetail = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                if (json.has("profile"))
                                {
                                    JSONObject driverProfile = json.getJSONObject("profile");
                                    if (driverProfile!=null)
                                    {
                                        String DriverId="", CompanyId="", DispatcherId="", Email="", FullName="", MobileNo="", Gender="", Image="", QRCode="", Password="", Address="", SubUrb = ""
                                                , City="", State="", Country="", Zipcode="", ReferralCode="", DriverLicense="", AccreditationCertificate="", DriverLicenseExpire=""
                                                , AccreditationCertificateExpire="", BankHolderName, BankName="", BankAcNo="", BSB="", Lat="", Lng="", Status=""
                                                , Availability="", DriverDuty="", ABN="", serviceDescription="", DCNumber="", ProfileComplete="", CategoryId="", Balance="", ReferralAmount="";
                                        if (driverProfile.has("Id"))
                                        {
                                            DriverId = driverProfile.getString("Id");
                                            SessionSave.saveUserSession(Comman.USER_ID, DriverId, activity);
                                        }
                                        if (driverProfile.has("CompanyId"))
                                        {
                                            CompanyId = driverProfile.getString("CompanyId");
                                            SessionSave.saveUserSession(Comman.USER_COMPANY_ID, CompanyId, activity);
                                        }
                                        if (driverProfile.has("DispatcherId"))
                                        {
                                            DispatcherId = driverProfile.getString("DispatcherId");
                                            SessionSave.saveUserSession(Comman.USER_DISPATHER_ID, DispatcherId, activity);
                                        }
                                        if (driverProfile.has("Email"))
                                        {
                                            Email = driverProfile.getString("Email");
                                            SessionSave.saveUserSession(Comman.USER_EMAIL, Email, activity);
                                        }
                                        if (driverProfile.has("Fullname"))
                                        {
                                            FullName = driverProfile.getString("Fullname");
                                            SessionSave.saveUserSession(Comman.USER_FULL_NAME, FullName, activity);
                                        }


                                        if (driverProfile.has("MobileNo"))
                                        {
                                            MobileNo = driverProfile.getString("MobileNo");
                                            SessionSave.saveUserSession(Comman.USER_MOBILE_NUMBER, MobileNo, activity);
                                        }
                                        if (driverProfile.has("Gender"))
                                        {
                                            Gender = driverProfile.getString("Gender");
                                            SessionSave.saveUserSession(Comman.USER_GENDER, Gender, activity);
                                        }
                                        if (driverProfile.has("Image"))
                                        {
                                            Image = driverProfile.getString("Image");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_IMAGE, Image, activity);
                                        }
                                        if (driverProfile.has("QRCode"))
                                        {
                                            QRCode = driverProfile.getString("QRCode");
                                            SessionSave.saveUserSession(Comman.USER_QR_CODE, QRCode, activity);
                                        }
                                        if (driverProfile.has("Password"))
                                        {
                                            Password = driverProfile.getString("Password");
                                            SessionSave.saveUserSession(Comman.USER_PASSWORD, Password, activity);
                                        }
                                        if (driverProfile.has("Address"))
                                        {
                                            Address = driverProfile.getString("Address");
                                            SessionSave.saveUserSession(Comman.USER_RESIDENTIAL_ADDRESS, Address, activity);
                                        }
                                        if (driverProfile.has("SubUrb"))
                                        {
                                            SubUrb = driverProfile.getString("SubUrb");
                                            SessionSave.saveUserSession(Comman.USER_SUB_URB, SubUrb, activity);
                                        }


                                        if (driverProfile.has("City"))
                                        {
                                            City = driverProfile.getString("City");
                                            SessionSave.saveUserSession(Comman.USER_CITY, City, activity);
                                        }
                                        if (driverProfile.has("State"))
                                        {
                                            State = driverProfile.getString("State");
                                            SessionSave.saveUserSession(Comman.USER_STATE, State, activity);
                                        }
                                        if (driverProfile.has("Country"))
                                        {
                                            Country = driverProfile.getString("Country");
                                            SessionSave.saveUserSession(Comman.USER_COUNTRY, Country, activity);
                                        }
                                        if (driverProfile.has("ZipCode"))
                                        {
                                            Zipcode = driverProfile.getString("ZipCode");
                                            SessionSave.saveUserSession(Comman.USER_POST_CODE, Zipcode, activity);
                                        }
                                        if (driverProfile.has("ReferralCode"))
                                        {
                                            ReferralCode = driverProfile.getString("ReferralCode");
                                            SessionSave.saveUserSession(Comman.USER_REFERRAL_CODE, ReferralCode, activity);
                                        }



                                        if (driverProfile.has("DriverLicense"))
                                        {
                                            DriverLicense = driverProfile.getString("DriverLicense");
                                            SessionSave.saveUserSession(Comman.USER_DRIVER_LICENCE, DriverLicense, activity);
                                        }
                                        if (driverProfile.has("AccreditationCertificate"))
                                        {
                                            AccreditationCertificate = driverProfile.getString("AccreditationCertificate");
                                            SessionSave.saveUserSession(Comman.USER_ACCREDITATION_CERTY, AccreditationCertificate, activity);
                                        }
                                        if (driverProfile.has("DriverLicenseExpire"))
                                        {
                                            DriverLicenseExpire = driverProfile.getString("DriverLicenseExpire");
                                            SessionSave.saveUserSession(Comman.USER_DRIVER_lICENSE_EXPIRE, DriverLicenseExpire, activity);
                                        }
                                        if (driverProfile.has("AccreditationCertificateExpire"))
                                        {
                                            AccreditationCertificateExpire = driverProfile.getString("AccreditationCertificateExpire");
                                            SessionSave.saveUserSession(Comman.USER_ACCREDITATION_CERTY_EXPIRE, AccreditationCertificateExpire, activity);
                                        }
                                        if (driverProfile.has("BankHolderName"))
                                        {
                                            BankHolderName = driverProfile.getString("BankHolderName");
                                            SessionSave.saveUserSession(Comman.USER_BANK_ACCOUNT_HOLDER_NAME, BankHolderName, activity);
                                        }
                                        if (driverProfile.has("BankName"))
                                        {
                                            BankName = driverProfile.getString("BankName");
                                            SessionSave.saveUserSession(Comman.USER_BANK_NAME, BankName, activity);
                                        }



                                        if (driverProfile.has("BankAcNo"))
                                        {
                                            BankAcNo = driverProfile.getString("BankAcNo");
                                            SessionSave.saveUserSession(Comman.USER_BANK_AC_NO, BankAcNo, activity);
                                        }
                                        if (driverProfile.has("BSB"))
                                        {
                                            BSB = driverProfile.getString("BSB");
                                            SessionSave.saveUserSession(Comman.USER_BSB, BSB, activity);
                                        }
                                        if (driverProfile.has("Lat"))
                                        {
                                            Lat = driverProfile.getString("Lat");
                                            SessionSave.saveUserSession(Comman.USER_LATITUDE, Lat, activity);
                                        }
                                        if (driverProfile.has("Lng"))
                                        {
                                            Lng = driverProfile.getString("Lng");
                                            SessionSave.saveUserSession(Comman.USER_LONGITUDE, Lng, activity);
                                        }
                                        if (driverProfile.has("Status"))
                                        {
                                            Status = driverProfile.getString("Status");
                                            SessionSave.saveUserSession(Comman.USER_STATE, Status, activity);
                                        }
                                        if (driverProfile.has("Availability"))
                                        {
                                            Availability = driverProfile.getString("Availability");
                                            SessionSave.saveUserSession(Comman.USER_AVAILABILITY, Availability, activity);
                                        }



                                        if (driverProfile.has("DriverDuty"))
                                        {
                                            DriverDuty = driverProfile.getString("DriverDuty");
                                            SessionSave.saveUserSession(Comman.USER_DRIVER_DUTY, DriverDuty, activity);
                                        }
                                        if (driverProfile.has("ABN"))
                                        {
                                            ABN = driverProfile.getString("ABN");
                                            SessionSave.saveUserSession(Comman.USER_ABN, ABN, activity);
                                        }
                                        if (driverProfile.has("Description"))
                                        {
                                            serviceDescription = driverProfile.getString("Description");
                                            SessionSave.saveUserSession(Comman.USER_SERVICE_DESCRIPTION, serviceDescription, activity);
                                        }
                                        if (driverProfile.has("DCNumber"))
                                        {
                                            DCNumber = driverProfile.getString("DCNumber");
                                            SessionSave.saveUserSession(Comman.USER_DC_NUMBER, DCNumber, activity);
                                        }
                                        if (driverProfile.has("ProfileComplete"))
                                        {
                                            ProfileComplete = driverProfile.getString("ProfileComplete");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_COMPLETE, ProfileComplete, activity);
                                        }

                                        if (driverProfile.has("CategoryId"))
                                        {
                                            CategoryId = driverProfile.getString("CategoryId");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_CATEGORY_ID, CategoryId, activity);
                                        }
                                        if (driverProfile.has("Balance"))
                                        {
                                            Balance = driverProfile.getString("Balance");
                                            SessionSave.saveUserSession(Comman.DRIVER_WALLET_BALLENCE, Balance, activity);
                                        }
                                        if (driverProfile.has("ReferralAmount"))
                                        {
                                            ReferralAmount = driverProfile.getString("ReferralAmount");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_REFERRAL_AMOUNT, ReferralAmount, activity);
                                        }


                                        if (driverProfile.has("Vehicle"))
                                        {
                                            JSONObject driverVehicle = driverProfile.getJSONObject("Vehicle");
                                            if (driverVehicle!=null)
                                            {
                                                String VehicleId, VehicleModel, CarCompany, CarColor, VehicleRegistrationNo, RegistrationCertificate, VehicleInsuranceCertificate
                                                        , RegistrationCertificateExpire, VehicleInsuranceCertificateExpire, VehicleImage, Description, VehicleModelName;

                                                if (driverVehicle.has("Id"))
                                                {
                                                    VehicleId = driverVehicle.getString("Id");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_ID, VehicleId, activity);
                                                }
                                                if (driverVehicle.has("VehicleModel"))
                                                {
                                                    VehicleModel = driverVehicle.getString("VehicleModel");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL, VehicleModel, activity);
                                                }
                                                if (driverVehicle.has("Company"))
                                                {
                                                    CarCompany = driverVehicle.getString("Company");
                                                    SessionSave.saveUserSession(Comman.USER_CAR_COMPANY, CarCompany, activity);
                                                }
                                                if (driverVehicle.has("Color"))
                                                {
                                                    CarColor = driverVehicle.getString("Color");
                                                    SessionSave.saveUserSession(Comman.USER_CAR_COLOR, CarColor, activity);
                                                }
                                                if (driverVehicle.has("VehicleRegistrationNo"))
                                                {
                                                    VehicleRegistrationNo = driverVehicle.getString("VehicleRegistrationNo");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_REGISTRATION_NO, VehicleRegistrationNo, activity);
                                                }


                                                if (driverVehicle.has("RegistrationCertificate"))
                                                {
                                                    RegistrationCertificate = driverVehicle.getString("RegistrationCertificate");
                                                    SessionSave.saveUserSession(Comman.USER_REGISTRATION_CERTY, RegistrationCertificate, activity);
                                                }
                                                if (driverVehicle.has("VehicleInsuranceCertificate"))
                                                {
                                                    VehicleInsuranceCertificate = driverVehicle.getString("VehicleInsuranceCertificate");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_INSURANCE_CERTY, VehicleInsuranceCertificate, activity);
                                                }
                                                if (driverVehicle.has("RegistrationCertificateExpire"))
                                                {
                                                    RegistrationCertificateExpire = driverVehicle.getString("RegistrationCertificateExpire");
                                                    SessionSave.saveUserSession(Comman.USER_REGISTRATION_CERTY_EXPIRE, RegistrationCertificateExpire, activity);
                                                }
                                                if (driverVehicle.has("VehicleInsuranceCertificateExpire"))
                                                {
                                                    VehicleInsuranceCertificateExpire = driverVehicle.getString("VehicleInsuranceCertificateExpire");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_INSURANCE_CERTY_EXPIRE, VehicleInsuranceCertificateExpire, activity);
                                                }
                                                if (driverVehicle.has("VehicleImage"))
                                                {
                                                    VehicleImage = driverVehicle.getString("VehicleImage");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE, VehicleImage, activity);
                                                }

                                                if (driverVehicle.has("Description"))
                                                {
                                                    Description = driverVehicle.getString("Description");
                                                    SessionSave.saveUserSession(Comman.USER_CAR_DESCRIPTION, Description, activity);
                                                }
                                                if (driverVehicle.has("VehicleClass"))
                                                {
                                                    VehicleModelName = driverVehicle.getString("VehicleClass");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL_NAME, VehicleModelName, activity);

                                                    Log.e("VehicleModelName","VehicleModelName : "+VehicleModelName);
                                                    if (VehicleModelName!=null && VehicleModelName.contains("First Class"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "First Class", activity);
                                                    }
                                                    else if (VehicleModelName.contains("Business Class"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", activity);
                                                    }
                                                    else if (VehicleModelName.contains("Economy"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Economy", activity);
                                                    }
                                                    else if (VehicleModelName.contains("Taxi"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Taxi", activity);
                                                    }
                                                    else if (VehicleModelName.contains("LUX-VAN"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "LUX-VAN", activity);
                                                    }
                                                    else if (VehicleModelName.contains("Disability"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Disability", activity);
                                                    }
                                                    else
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", activity);
                                                    }
                                                } else {
                                                    SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", activity);
                                                }
                                                dialogClass.hideDialog();
                                            }
                                            else
                                            {
                                                dialogClass.hideDialog();
                                            }
                                            dialogClass.hideDialog();
                                            new SnackbarUtils(main_layout, getResources().getString(R.string.profile_updated_successfully),
                                                    ContextCompat.getColor(activity, R.color.snakbar_color),
                                                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                        }
                                        else
                                        {
                                            dialogClass.hideDialog();
                                            if (json.has("message")) {
                                                new SnackbarUtils(main_layout, json.getString("message"),
                                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                            }
                                        }
                                        Log.e("GGGGGGGGGGG","GGGGGGGGGGG :"+SessionSave.getUserSession(Comman.USER_ID, activity));

                                    }
                                    else
                                    {
                                        dialogClass.hideDialog();
                                        if (json.has("message")) {
                                            new SnackbarUtils(main_layout, json.getString("message"),
                                                    ContextCompat.getColor(activity, R.color.snakbar_color),
                                                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                        }
                                    }
                                }
                                else
                                {
                                    dialogClass.hideDialog();
                                    if (json.has("message")) {
                                        new SnackbarUtils(main_layout, json.getString("message"),
                                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                    }
                                }

                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message")) {
                                new SnackbarUtils(main_layout, json.getString("message"),
                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                    }
                } catch (Exception e) {
                    Log.e("UpdateCarDetail", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }.header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            TiCKTOC_Driver_Application.setCurrentActivity(activity);
        }
    }
}