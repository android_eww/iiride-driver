package iiride.app.driver.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import iiride.app.driver.Application.TiCKTOC_Driver_Application;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;


import iiride.app.driver.R;
import iiride.app.driver.View.SnackbarUtils;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import android.telephony.SmsManager;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.List;


public class Invite_Friend_Activity extends AppCompatActivity implements View.OnClickListener{

    public static Invite_Friend_Activity activity;

    LinearLayout ll_back, main_layout;
    ImageView iv_back;

    ImageView iv_prof_inviteDrive;

    LinearLayout ll_faceBook, ll_twitter, ll_email, ll_whatsApp, ll_SMS;
    TextView tv_ReferralAmount, tv_ReferralCode;

    private static final int MY_PERMISSIONS_REQUEST_SEND_SMS =0 ;

    String ReferralCode, ReferralAmount, profileImage="";
    Transformation mTransformation;
    ProgressBar progBar_Image;
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100, RESULT_PICK_CONTACT = 800;;
    private ListView lstNames;
    Spanned message ;

    private BroadcastReceiver sentStatusReceiver, deliveredStatusReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_driver);

        activity = Invite_Friend_Activity.this;

        mTransformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(100)
                .borderColor(ContextCompat.getColor(activity, R.color.colorBlack))
                .borderWidthDp(2)
                .oval(true)
                .build();

        initUI();
    }

    private void initUI() {
        main_layout = (LinearLayout) findViewById(R.id.main_layout);
        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        iv_back = (ImageView) findViewById(R.id.iv_back);

        progBar_Image = (ProgressBar) findViewById(R.id.progBar_Image);

        ll_faceBook = (LinearLayout) findViewById(R.id.ll_faceBook);
        ll_twitter = (LinearLayout) findViewById(R.id.ll_twitter);
        ll_email = (LinearLayout) findViewById(R.id.ll_email);
        ll_whatsApp = (LinearLayout) findViewById(R.id.ll_whatsApp);
        ll_SMS = (LinearLayout) findViewById(R.id.ll_SMS);

        tv_ReferralAmount = (TextView) findViewById(R.id.tv_ReferralAmount);
        tv_ReferralCode = (TextView) findViewById(R.id.tv_ReferralCode);

        iv_prof_inviteDrive = (ImageView) findViewById(R.id.iv_prof_inviteDrive);

        ReferralCode = SessionSave.getUserSession(Comman.USER_REFERRAL_CODE, activity);
        ReferralAmount = SessionSave.getUserSession(Comman.USER_PROFILE_REFERRAL_AMOUNT, activity);
        profileImage = SessionSave.getUserSession(Comman.USER_PROFILE_IMAGE, activity);

        if (ReferralCode!=null && !ReferralCode.equalsIgnoreCase(""))
        {
            tv_ReferralCode.setText(ReferralCode);
        }

        if (ReferralAmount!=null && !ReferralAmount.equalsIgnoreCase(""))
        {
//            ReferralAmount = String.format("%.2f", ReferralAmount);
            Log.e("ReferralAmount","ReferralAmount : "+ReferralAmount);
            double x = Double.parseDouble(ReferralAmount);
            DecimalFormat df = new DecimalFormat("#0.00");
            tv_ReferralAmount.setText(df.format(x));
        }

        if (profileImage != null && !profileImage.equalsIgnoreCase("")) {
            progBar_Image.setVisibility(View.VISIBLE);
            Picasso.with(activity)
                    .load(profileImage)
                    .transform(mTransformation)
                    .fit()
                    .into(iv_prof_inviteDrive, new Callback() {

                        @Override
                        public void onSuccess() {
                            progBar_Image.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            iv_prof_inviteDrive.setImageResource(R.drawable.prof_image_demo);
                            progBar_Image.setVisibility(View.GONE);
                        }
                    });
        } else {
            iv_prof_inviteDrive.setImageResource(R.drawable.prof_image_demo);
        }

        ll_back.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        ll_faceBook.setOnClickListener(this);
        ll_twitter.setOnClickListener(this);
        ll_email.setOnClickListener(this);
        ll_whatsApp.setOnClickListener(this);
        ll_SMS.setOnClickListener(this);

       /* message = Html.fromHtml("<p>"+SessionSave.getUserSession(Comman.USER_FULL_NAME, activity)+" has invited you to become a <b>TiCKTOC</b> Driver.</p>"
                +"<a href=\"https://goo.gl/y4xSuP\">click here https://goo.gl/y4xSuP</a>"
                + "<p>Your invite code is : "+SessionSave.getUserSession(Comman.USER_REFERRAL_CODE, activity)+"</p>"
                +"<a href=\"www.ticktoc.net\">www.ticktoc.net</a><br/>"
                +"<a href=\"www.facebook.com/ticktoc.net\">www.facebook.com/ticktoc.net</a>");*/

//        message = Html.fromHtml("<p>"+SessionSave.getUserSession(Comman.USER_FULL_NAME, activity)+" has invited you to become a <b>iiRidde</b> Driver.</p>"
//                +"<a href=\"https://iiride.com\">click here https://iiride.com</a>"
//                + "<p>Your invite code is : "+SessionSave.getUserSession(Comman.USER_REFERRAL_CODE, activity)+"</p>"
//                +"<a href=\"https://iiride.com/\">https://iiride.com/</a>");

        message = Html.fromHtml("<p>"+SessionSave.getUserSession(Comman.USER_FULL_NAME, activity)+" has invited you to become a <b>iiRidde</b> Driver.</p>"
                +"<a href=\"https://play.google.com/store/apps/details?id=iiride.app.driver\">click here https://play.google.com/store/apps/details?id=iiride.app.driver</a>"
                + "<p>Your invite code is : "+SessionSave.getUserSession(Comman.USER_REFERRAL_CODE, activity)+"</p>");

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.ll_faceBook:
                Open_FaceBook();
                break;

            case R.id.ll_twitter:
                Open_Twitter();
                break;

            case R.id.ll_email:
                Open_Email();
                break;

            case R.id.ll_whatsApp:
                Open_Whatsapp();
                break;

            case R.id.ll_SMS:
                OpenSMS();
                break;
        }
    }


    private void Open_FaceBook()
    {
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.facebook.katana");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, message.toString());
        whatsappIntent.putExtra(Intent.EXTRA_TEXT   , message.toString());
        try
        {
            activity.startActivity(whatsappIntent);
        }
        catch (android.content.ActivityNotFoundException ex)
        {
            new SnackbarUtils(main_layout, getResources().getString(R.string.facebook_is_not_install),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
    }

    private void Open_Twitter()
    {
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.twitter.android");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, message.toString());
        try
        {
            activity.startActivity(whatsappIntent);
        }
        catch (android.content.ActivityNotFoundException ex)
        {
            new SnackbarUtils(main_layout, getResources().getString(R.string.twitter_is_not_install),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
    }

    private void Open_Email()
    {
        try
        {
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto:")); // only email apps should handle this
            intent.putExtra(Intent.EXTRA_EMAIL, "developer.eww@gmail.com");
            intent.putExtra(Intent.EXTRA_SUBJECT, "iiRide Partner");
            intent.putExtra(Intent.EXTRA_TEXT   , message.toString());
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
        catch (android.content.ActivityNotFoundException e)
        {
            Log.e("kkkkkkkk","kkkkkkkkkkkk "+e.toString());
            new SnackbarUtils(main_layout, getResources().getString(R.string.email_send_error),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }

      /*  Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.setData(Uri.parse("mailto:"));
        i.putExtra(Intent.EXTRA_EMAIL, "developer.eww@gmail.com");
        i.putExtra(Intent.EXTRA_SUBJECT, "TiCKTOC Driver");
        i.putExtra(Intent.EXTRA_TEXT   , message.toString());
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Log.e("kkkkkkkk","kkkkkkkkkkkk "+ex.toString());
            new SnackbarUtils(main_layout, getResources().getString(R.string.email_send_error),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }*/
    }

    private void Open_Whatsapp()
    {
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.whatsapp");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, message.toString());
        try
        {
            activity.startActivity(whatsappIntent);
        }
        catch (android.content.ActivityNotFoundException ex)
        {
            new SnackbarUtils(main_layout, getResources().getString(R.string.whats_app_is_not_install),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
    }
    private void OpenSMS()
    {
       Log.e("Send SMS", "etsrhftmfymuytmym");
       /* String phoneNo = "9409714455";
        String message = "TickToc App Driver";
         try
        {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, message, null, null);
            new SnackbarUtils(main_layout,"message sent",
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        catch (Exception e)
        {
            new SnackbarUtils(main_layout, getResources().getString(R.string.sms_failed),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
          Log.e("kkkkkkkk","kkkkkkkkkkkk "+e.toString());
        }*/

        Intent smsIntent = new Intent(android.content.Intent.ACTION_VIEW);
        //smsIntent.setType("vnd.android-dir/mms-sms");
        //smsIntent.putExtra("address","your desired phoneNumber");
        smsIntent.setData(Uri.parse("sms:"));
        smsIntent.putExtra("sms_body",message.toString());
        startActivity(smsIntent);

        //showContacts();
    }

    private void showContacts() {
        // Check the SDK version and whether the permission is already granted or not.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED)
        {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
        }
        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED)
        {
            requestPermissions(new String[]{Manifest.permission.SEND_SMS}, PERMISSIONS_REQUEST_READ_CONTACTS);
        }
        else
        {
            Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
            startActivityForResult(contactPickerIntent, RESULT_PICK_CONTACT);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // check whether the result is ok
        Log.e("MainActivity", "onActivityResult");
        if (resultCode == RESULT_OK)
        {
            Log.e("MainActivity", "RESULT_OK");
            // Check for the request code, we might be usign multiple startActivityForReslut
            switch (requestCode)
            {
                case RESULT_PICK_CONTACT:

                    Log.e("MainActivity", "contactPicked");
                    contactPicked(data);
                    break;
            }
        }
        else
        {
            Log.e("MainActivity", "Failed to pick contact");
        }
    }
    /**
     * Query the Uri and read contact details. Handle the picked contact data.
     * @param data
     */
    private void contactPicked(Intent data)
    {
        Cursor cursor = null;
        try {
            String phoneNo = "" ;
            String name = null;
            // getData() method will have the Content Uri of the selected contact
            Uri uri = data.getData();
            //Query the content uri
            cursor = getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            // column index of the phone number
            int  phoneIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            // column index of the contact name
            int  nameIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            phoneNo = cursor.getString(phoneIndex);
            name = cursor.getString(nameIndex);
            // Set the value to the textviews
//            textView1.setText(name);
//            textView2.setText(phoneNo);
            Log.e("contactPicked","name : "+name);
            Log.e("contactPicked","phoneNo : "+phoneNo);

//            String message = "TickToc App Driver";

            if (phoneNo!=null && !phoneNo.equalsIgnoreCase(""))
            {
                OpenSMS(phoneNo);
               /* try
                {
                    Log.e("message.toString()","message.toString() : "+message.toString());
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(phoneNo, null, message.toString(), null, null);
                    new SnackbarUtils(main_layout,"message sent",
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
                catch (Exception e)
                {
                    new SnackbarUtils(main_layout, getResources().getString(R.string.sms_failed),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                    Log.e("kkkkkkkk","kkkkkkkkkkkk "+e.toString());
                }*/
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void OpenSMS(String phoneNo) {

        final String phone = phoneNo.trim().replace(" ","");

        String message = SessionSave.getUserSession(Comman.USER_FULL_NAME, activity)+" has invited you to become a TiCKTOC Driver." +
                "\nclick here https://goo.gl/y4xSuP" +
                "\nYour invite code is: " + SessionSave.getUserSession(Comman.USER_REFERRAL_CODE, activity) +
                "\nwww.facebook.com/ticktoc.net";

        Log.e("call","phone number = "+phone);
        Log.e("call","message = "+message);
        //Check if the phoneNumber is empty
        if (phone.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please Enter a Valid Phone Number", Toast.LENGTH_SHORT).show();
        } else {

            final SmsManager sms = SmsManager.getDefault();
            // if message length is too long messages are divided
            List<String> messages = sms.divideMessage(message);
            for (final String msg : messages) {

                final PendingIntent sentIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_SENT"), 0);
                final PendingIntent deliveredIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_DELIVERED"), 0);

                new Handler().postDelayed(new Runnable() {
                    public void run() {

                        sms.sendTextMessage(phone, null, msg, sentIntent, deliveredIntent);
                    }
                }, 1000);

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                showContacts();
            } else {
                Toast.makeText(this, "Until you grant the permission, we canot display the names", Toast.LENGTH_SHORT).show();
            }
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Drawer_Activity.flagResumePass=0;
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            TiCKTOC_Driver_Application.setCurrentActivity(activity);
        }

        sentStatusReceiver=new BroadcastReceiver() {

            @Override
            public void onReceive(Context arg0, Intent arg1) {
                String s = "Unknown Error";
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        s = "Message Sent Successfully !!";
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        s = "Generic Failure Error";
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        s = "Error : No Service Available";
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        s = "Error : Null PDU";
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        s = "Error : Radio is off";
                        break;
                    default:
                        break;
                }
                new SnackbarUtils(main_layout, s+"",
                        ContextCompat.getColor(activity, R.color.snakbar_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
            }
        };

        deliveredStatusReceiver=new BroadcastReceiver() {

            @Override
            public void onReceive(Context arg0, Intent arg1) {
                String s = "Message Not Delivered";
                switch(getResultCode()) {
                    case Activity.RESULT_OK:
                        s = "Message Delivered Successfully";
                        break;
                    case Activity.RESULT_CANCELED:
                        break;
                }
                new SnackbarUtils(main_layout, s+"",
                        ContextCompat.getColor(activity, R.color.snakbar_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
            }
        };
        registerReceiver(sentStatusReceiver, new IntentFilter("SMS_SENT"));
        registerReceiver(deliveredStatusReceiver, new IntentFilter("SMS_DELIVERED"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(sentStatusReceiver);
        unregisterReceiver(deliveredStatusReceiver);
    }
}
