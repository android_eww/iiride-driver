package iiride.app.driver.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import iiride.app.driver.Been.CreditCard_List_Been;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;
import iiride.app.driver.Nfc.IRefreshable;
import iiride.app.driver.Nfc.NFCUtils;
import iiride.app.driver.Nfc.Provider;
import iiride.app.driver.Nfc.SimpleAsyncTask;
import iiride.app.driver.Others.ConnectivityReceiver;


import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.SnackbarUtils;
import iiride.app.driver.Application.TiCKTOC_Driver_Application;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.github.devnied.emvnfccard.model.EmvCard;
import com.github.devnied.emvnfccard.parser.EmvParser;
import com.github.devnied.emvnfccard.utils.AtrUtils;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import fr.devnied.bitlib.BytesUtils;
import io.card.payment.CardIOActivity;
import io.card.payment.CardType;
import io.card.payment.CreditCard;


public class Add_Card_In_List_Activity extends AppCompatActivity implements View.OnClickListener{

    public static Add_Card_In_List_Activity activity;

    private  static String TAG = Add_Card_In_List_Activity.class.getSimpleName();

    private static final int REQUEST_SCAN = 100;
    private static final int REQUEST_AUTOTEST =  200;

    private String strCardNumber,strCardType,strCardExpiry;

    LinearLayout main_layout, ll_back, ll_cardNumber, ll_valid_through, ll_cvv, ll_alias;
    ImageView iv_back, iv_CardImage;
    TextView tv_valid_through, tv_add_payment_method, tv_scanCard, tv_scanNfcCard;
    EditText et_CardNumber, et_cvv, et_alias;
    View view_cvv, view_valid_through, view_cardNumber, view_alias;
    private String cardNumber;

    ///for Bottom Dialog
    private BottomSheetDialog mBottomSheetDialog;
    private NumberPicker numberPickerMonth,numberPickerYear;
    private String[] valueMonth ;
    private String[] valueYear ;
    int MonthInNumber=0, YearInNumber=0 ;
    private String[] monthName = {
            "Jan", "Feb", "Mar",
            "Apr", "May", "Jun",
            "Jul", "Aug", "Sep",
            "Oct", "Nov", "Dec"
    };
    private AQuery aQuery;
    DialogClass dialogClass;
    String ExpiryDate="";

    boolean validornot = false;

    private NFCUtils mNfcUtils;
    private ProgressDialog mDialog;
    private AlertDialog mAlertDialog;
    private Provider mProvider = new Provider();
    private EmvCard mReadCard;
    private WeakReference<IRefreshable> mRefreshableContent;
    private byte[] lastAts;

    boolean flagScan = false;
    String year="",month="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_add_card_in_list);

        activity = Add_Card_In_List_Activity.this;
        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 1);

        mNfcUtils = new NFCUtils(this);
        flagScan = false;

        initUI();
    }

    private void initUI() {
        main_layout = (LinearLayout) findViewById(R.id.main_layout);
        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        ll_cardNumber = (LinearLayout) findViewById(R.id.ll_cardNumber);
        ll_valid_through = (LinearLayout) findViewById(R.id.ll_valid_through);
        ll_cvv = (LinearLayout) findViewById(R.id.ll_cvv);
        ll_alias = (LinearLayout) findViewById(R.id.ll_alias);

        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_CardImage = (ImageView) findViewById(R.id.iv_CardImage);

        tv_valid_through = (TextView) findViewById(R.id.tv_valid_through);
        tv_add_payment_method = (TextView) findViewById(R.id.tv_add_payment_method);
        tv_scanCard = (TextView) findViewById(R.id.tv_scanCard);
        tv_scanNfcCard = (TextView) findViewById(R.id.tv_scanNfcCard);

        view_cvv = (View) findViewById(R.id.view_cvv);
        view_valid_through = (View) findViewById(R.id.view_valid_through);
        view_cardNumber = (View) findViewById(R.id.view_cardNumber);
        view_alias = (View) findViewById(R.id.view_alias);

        et_CardNumber = (EditText) findViewById(R.id.et_CardNumber);
        et_cvv = (EditText) findViewById(R.id.et_cvv);
        et_alias = (EditText) findViewById(R.id.et_alias);

        et_CardNumber.setFilters(new InputFilter[] { new InputFilter.LengthFilter(19) });
        et_CardNumber.addTextChangedListener(new FourDigitCardFormatWatcher());

        et_CardNumber.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                flagScan = false;
                return false;
            }
        });

        tv_valid_through.setOnClickListener(this);
        ll_valid_through.setOnClickListener(this);
        ll_back.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        tv_scanCard.setOnClickListener(this);
        tv_scanNfcCard.setOnClickListener(this);
        tv_add_payment_method.setOnClickListener(this);

        et_cvv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                view_cvv.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorRed));
                view_cardNumber.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorBlack));

                return false;
            }
        });
        view_cardNumber.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                view_cardNumber.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorRed));
                view_cvv.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorBlack));

                return false;
            }
        });



    }

    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.tv_valid_through:
                showBottomSheetDailog_payment();
                break;

            case R.id.ll_valid_through:
                showBottomSheetDailog_payment();
                break;

            case R.id.tv_scanCard:
                flagScan = true;
                et_CardNumber.setText("");
                et_alias.setText("");
                et_cvv.setText("");
                ExpiryDate="";
                cardScane();
                break;

            case R.id.tv_scanNfcCard:
                flagScan = true;
                et_CardNumber.setText("");
                et_alias.setText("");
                et_cvv.setText("");
                ExpiryDate="";
                cardNfcScane();
                break;

            case R.id.tv_add_payment_method:
                LoginFlagForCurrentUser();
                break;
        }
    }

    private void cardScane() {

        Intent intent = new Intent(activity, CardIOActivity.class)
                .putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true)
                .putExtra(CardIOActivity.EXTRA_SCAN_EXPIRY, true)
                .putExtra(CardIOActivity.EXTRA_GUIDE_COLOR, Color.TRANSPARENT)
                .putExtra("debug_autoAcceptResult", true);;
        startActivityForResult(intent, REQUEST_SCAN);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.v(TAG, "onActivityResult(" + requestCode + ", " + resultCode + ", " + data + ")");


        if ((requestCode == REQUEST_SCAN || requestCode == REQUEST_AUTOTEST) && data != null
                && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT))
        {
            CreditCard result = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

            Log.e(TAG, "Rusult: " + result);
            if (result != null)
            {

                strCardNumber = "Card number: " + result.getFormattedCardNumber();
                et_CardNumber.setText(result.getRedactedCardNumber());

                CardType cardType = result.getCardType();
                strCardType = "Card type: " + cardType.name();
                et_alias.setText(cardType.name());

                strCardExpiry = "Expiry: " + result.expiryMonth + "/" + result.expiryYear + "\n";

                Calendar cal = Calendar.getInstance();
                SimpleDateFormat month_date = new SimpleDateFormat("MMM");
                cal.set(Calendar.MONTH,result.expiryMonth-1);
                String month_name = month_date.format(cal.getTime());

                tv_valid_through.setText(month_name + "," + result.expiryYear);

                String strCard= month_name + "," + result.expiryYear;
                String[] abc = strCard.split(",");
                String abc1 = abc[0];
                try {
                    Date date = new SimpleDateFormat("MMMM").parse(abc1);
                    Calendar cal1 = Calendar.getInstance();
                    cal1.setTime(date);
                    Log.e("cal1.get(Clendar.MONTH)","cal1.get(Calendar.MONTH)"+cal1.get(Calendar.MONTH));
                    abc1 = String.valueOf((cal1.get(Calendar.MONTH)+1));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String abc2 = abc[1];
                abc2 = abc2.substring(Math.max(abc2.length() - 2, 0));

                if (abc1!=null && abc2!=null)
                {
                    strCardExpiry = abc1+"/"+abc2;
                    ExpiryDate = strCardExpiry;
                }
                else {
                    ExpiryDate = "";
                }
            }
        }

        Log.e(TAG, "strCardNumber: " + strCardNumber);
        Log.e(TAG, "strCardType: " + strCardType);
        Log.e(TAG, "strCardExpiry: " + strCardExpiry);



    }

    private void cardNfcScane()
    {
        mNfcUtils.enableDispatch();

        // Close
        if (mAlertDialog != null && mAlertDialog.isShowing())
        {
            mAlertDialog.cancel();
        }

        // Check if NFC is available
        if (!NFCUtils.isNfcAvailable(getApplicationContext()))

        {

            AlertDialog.Builder alertbox = new AlertDialog.Builder(this);

            alertbox.setTitle(getString(R.string.msg_info));
            alertbox.setMessage(getString(R.string.msg_nfc_not_available));
            alertbox.setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(final DialogInterface dialog, final int which) {
                    dialog.dismiss();
                }
            });
            alertbox.setCancelable(false);
            mAlertDialog = alertbox.show();
        }
        else
        {
            nfcScan();
        }
    }

    private void nfcScan()
    {
        onNewIntent(getIntent());
    }

    @Override
    protected void onNewIntent(final Intent intent) {
        super.onNewIntent(intent);
        final Tag mTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        if (mTag != null) {

            new SimpleAsyncTask() {

                private IsoDep mTagcomm;
                private EmvCard mCard;
                private boolean mException;

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();

//                    backToHomeScreen();
                    mProvider.getLog().setLength(0);
                    // Show dialog
                    if (mDialog == null) {
                        mDialog = ProgressDialog.show(activity, getString(R.string.card_reading),
                                getString(R.string.card_reading_desc), true, false);
                    } else {
                        mDialog.show();
                    }
                }

                @Override
                protected void doInBackground() {

                    mTagcomm = IsoDep.get(mTag);
                    if (mTagcomm == null) {
                        Toast.makeText(getApplicationContext(), R.string.error_communication_nfc, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    mException = false;

                    try {
                        mReadCard = null;
                        // Open connection
                        mTagcomm.connect();
                        lastAts = getAts(mTagcomm);

                        mProvider.setmTagCom(mTagcomm);

                        EmvParser parser = new EmvParser(mProvider, true);
                        mCard = parser.readEmvCard();
                        if (mCard != null) {
                            mCard.setAtrDescription(extractAtsDescription(lastAts));
                        }

                    } catch (IOException e) {
                        mException = true;
                    } finally {
                        // close tagcomm
                        IOUtils.closeQuietly(mTagcomm);
                    }
                }

                @Override
                protected void onPostExecute(final Object result) {
                    // close dialog
                    if (mDialog != null) {
                        mDialog.cancel();
                    }

                    if (!mException) {
                        if (mCard != null) {
                            if (StringUtils.isNotBlank(mCard.getCardNumber())) {
                                Toast.makeText(getApplicationContext(), R.string.card_read, Toast.LENGTH_SHORT).show();
                                mReadCard = mCard;

                                String str = mReadCard.getExpireDate().toString();
                                String[] splited = str.trim().split("\\s+");
                                String month = splited[1];
                                String year = splited[splited.length - 1];

                                Log.e("HomeActivity",
                                        "Card Number:- " + mReadCard.getCardNumber() +
                                                "\nCard Exp:- " + mReadCard.getExpireDate() +
                                                "\nCard Month:- " + month +
                                                "\nCard Year:- " + year +
                                                "\nCard Type:- " + mReadCard.getType() +
                                                "\nCard First Name:- " + mReadCard.getHolderFirstname() +
                                                "\nCard LastName:- " + mReadCard.getHolderLastname() +
                                                "\nCard Application:- " + mReadCard.getApplicationLabel() +
                                                "\nCard Aid:- " + mReadCard.getAid() +
                                                "\nCard LeftPinTrsy:- " + mReadCard.getLeftPinTry() +
                                                "\nCard Service:- " + mReadCard.getService());

                                cardNumber = mReadCard.getCardNumber();
                                tv_valid_through.setText(month + "," + year);

                                String str1 = month + "," + year;
                                String[] abc = str1.split(",");
                                if (abc.length>1)
                                {
                                    String abc1 = abc[0];
                                    try {
                                        Date date = new SimpleDateFormat("MMMM").parse(abc1);
                                        Calendar cal = Calendar.getInstance();
                                        cal.setTime(date);
                                        Log.e("cal.get(Calendar.MONTH)","cal.get(Calendar.MONTH)"+cal.get(Calendar.MONTH));
                                        abc1 = String.valueOf((cal.get(Calendar.MONTH)+1));
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    String abc2 = abc[1];
                                    abc2 = abc2.substring(Math.max(abc2.length() - 2, 0));

                                    if (abc1!=null && abc2!=null)
                                    {
                                        strCardExpiry = abc1+"/"+abc2;
                                        ExpiryDate = strCardExpiry;
                                    }
                                    else {
                                        ExpiryDate = "";
                                    }
                                }
                                else {
                                    ExpiryDate = "";
                                }

                                if (cardNumber.length()>4)
                                {
                                    strCardNumber = cardNumber;
                                    String strCard = "";
                                    for (int i=0; i<cardNumber.length(); i++)
                                    {
                                        if (i==0)
                                        {
                                            strCard = cardNumber.substring(i,(i+1));
                                        }
                                        else if ((i%4)==0)
                                        {
                                            strCard = strCard + " " + cardNumber.substring(i,(i+1));
                                        }
                                        else
                                        {
                                            strCard = strCard + cardNumber.substring(i,(i+1));
                                        }
                                    }
                                    cardNumber = cardNumber.replace(" ","");
                                    String cNoEncry = getFormattedCardNumber(cardNumber);

                                    et_CardNumber.setText(cNoEncry);
                                }


                            } else if (mCard.isNfcLocked()) {
                                Toast.makeText(getApplicationContext(), R.string.nfc_locked, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.error_card_unknown, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.error_communication_nfc, Toast.LENGTH_SHORT).show();
                    }

                    refreshContent();
                }

            }.execute();
        }
    }

    public Collection<String> extractAtsDescription(final byte[] pAts) {
        return AtrUtils.getDescriptionFromAts(BytesUtils.bytesToString(pAts));
    }

    private void refreshContent() {
        if (mRefreshableContent != null && mRefreshableContent.get() != null) {
            mRefreshableContent.get().update();
        }
    }

    private byte[] getAts(final IsoDep pIso)
    {
        byte[] ret = null;
        if (pIso.isConnected()) {
            // Extract ATS from NFC-A
            ret = pIso.getHistoricalBytes();
            if (ret == null) {
                // Extract ATS from NFC-B
                ret = pIso.getHiLayerResponse();
            }
        }
        return ret;
    }



    public StringBuffer getLog() {
        return mProvider.getLog();
    }

    public EmvCard getCard() {
        return mReadCard;
    }


    public void setRefreshableContent(final IRefreshable pRefreshable) {
        mRefreshableContent = new WeakReference<IRefreshable>(pRefreshable);
    }

    /**
     * Method used to clear data
     */
    public void clear() {
        mReadCard = null;
        mProvider.getLog().setLength(0);
        IRefreshable content = mRefreshableContent.get();
        if (content != null) {
            content.update();
        }
    }

    /**
     * Get the last ATS
     *
     * @return the last card ATS
     */
    public byte[] getLastAts() {
        return lastAts;
    }

    public void LoginFlagForCurrentUser()
    {
        if (TextUtils.isEmpty(et_CardNumber.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.add_payment_error_message),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else if (TextUtils.isEmpty(et_cvv.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.add_payment_error_message),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else if (TextUtils.isEmpty(et_alias.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.add_payment_error_message),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else if (TextUtils.isEmpty(ExpiryDate))
        {
            new SnackbarUtils(main_layout, getString(R.string.add_payment_error_message),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else if(!checkDateValidation(Integer.parseInt(year),Integer.parseInt(month)))
        {
            new SnackbarUtils(main_layout, getString(R.string.plaese_enter_vali_expiry_date),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else
        {
            Log.e("length","et_CardNumber"+et_CardNumber.getText().toString().trim().length());
            if(et_CardNumber.getText().length()>11) {
                if (!flagScan)
                {
                    FindCardType(et_CardNumber.getText());
                    if (validornot)
                    {
                        String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                        if (driverId!=null && !driverId.equalsIgnoreCase(""))
                        {
                            if (ConnectivityReceiver.isConnected())
                            {
                                AddCardIn_List(driverId);
                            }
                            else
                            {
                                new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }

                    }
                    else {
                        new SnackbarUtils(main_layout, getString(R.string.invalid_card_number),
                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                    }
                }
                else
                {
                    String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                    if (driverId!=null && !driverId.equalsIgnoreCase(""))
                    {
                        if (ConnectivityReceiver.isConnected())
                        {
                            AddCardIn_List(driverId);
                        }
                        else
                        {
                            new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                                    ContextCompat.getColor(activity, R.color.snakbar_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                        }
                    }
                }
            }
            else
            {
                new SnackbarUtils(main_layout, getString(R.string.invalid_card_number),
                        ContextCompat.getColor(activity, R.color.snakbar_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
            }
        }
    }


    public void showBottomSheetDailog_payment() {

        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_payment, null);
        mBottomSheetDialog = new BottomSheetDialog(activity);
        mBottomSheetDialog.setContentView(view);


        final TextView tv_Ok, tv_Cancle;

        tv_Ok = (TextView) view.findViewById(R.id.bottom_sheet_payment_ok_textview);
        tv_Cancle = (TextView) view.findViewById(R.id.bottom_sheet_payment_cancle_textview);
        numberPickerMonth = (NumberPicker) view.findViewById(R.id.numberPickerMonth);
        numberPickerYear = (NumberPicker) view.findViewById(R.id.numberPickerYear);

        numberPickerMonth.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        numberPickerYear.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        setDividerColor(numberPickerMonth,ContextCompat.getColor(activity, R.color.colorRed));
        setDividerColor(numberPickerYear,ContextCompat.getColor(activity, R.color.colorRed));

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH)+1;

        Log.e("Date","year"+year+"month"+month);

        numberPickerMonth.setMinValue(1);
        numberPickerMonth.setMaxValue(12);
        numberPickerYear.setMinValue(year);
        numberPickerYear.setMaxValue(year+5);

        valueMonth = new String[12];
        valueYear = new String[6];


        for(int i=0; i<12; i++)
        {
            valueMonth[i] = monthName[i]+"";
            Log.d("valueNumber","valueMonth : "+ valueMonth[i]);
        }

        for(int i=0; i<6; i++)
        {
            valueYear[i] = (year+i)+"";
            Log.d("valueNumber","valueYear : "+ valueYear[i]);
        }

        numberPickerMonth.setValue(month);
        numberPickerMonth.setDisplayedValues(monthName);
        numberPickerYear.setValue(year);


        tv_Cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mBottomSheetDialog!=null)
                {
                    mBottomSheetDialog.dismiss();
                }
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBottomSheetDialog!=null)
                {
                    MonthInNumber = numberPickerMonth.getValue();
                    YearInNumber = numberPickerYear.getValue();

                    Log.e("MonthInNumber"," = "+MonthInNumber);
                    Log.e("YearInNumber"," = "+YearInNumber);
                    String str = String.valueOf(YearInNumber);
                    String substring = str.substring(Math.max(str.length() - 2, 0));

                    if (substring!=null && !substring.equalsIgnoreCase(""))
                    {
                        ExpiryDate = MonthInNumber +"/"+ substring;
                    }
                    else
                    {
                        ExpiryDate ="";
                    }


                    tv_valid_through.setText(monthName[MonthInNumber-1]);
                    tv_valid_through.append(", "+YearInNumber);

                    setValue(MonthInNumber,YearInNumber);

                    mBottomSheetDialog.dismiss();
                }
            }
        });

        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });

        mBottomSheetDialog.show();
    }

    private void setValue(int monthInNumber, int yearInNumber)
    {
        year = String.valueOf(yearInNumber);
        month = String.valueOf(monthInNumber);
    }

    private void setDividerColor(NumberPicker picker, int color) {

        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    public class FourDigitCardFormatWatcher implements TextWatcher {

        // Change this to what you want... ' ', '-' etc..
        private static final char space = ' ';

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s)
        {
            // Remove spacing char
            if (s.length() > 0 && (s.length() % 5) == 0) {
                final char c = s.charAt(s.length() - 1);
                if (space == c) {
                    s.delete(s.length() - 1, s.length());
                }
            }
            // Insert char where needed.
            if (s.length() > 0 && (s.length() % 5) == 0) {
                char c = s.charAt(s.length() - 1);
                // Only if its a digit where there should be a space we insert a space
                if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 3) {
                    s.insert(s.length() - 1, String.valueOf(space));
                }
            }
            FindCardType(s);
           /* if (flagScan)
            {
                strCardNumber = s.toString();
                Log.e("scan true","s  ; "+s);
                StringBuilder numbre = new StringBuilder(s.toString());
                for (int i=0; i< numbre.length(); i++)
                {
                    if (i<15)
                    {
                        Log.e("scan true","s  i="+i+" ; "+String.valueOf(numbre.charAt(i)));
                        if ((String.valueOf(numbre.charAt(i))).contains(" ")) {
                        }else {
                            numbre.setCharAt(i, '*');
                        }
                    }
                }
                Log.e("scan true","numbre  ; "+numbre);
                s.replace(0, s.length(),s.toString());
            }
            else
            {

            }*/
        }
    }

    public String getFormattedCardNumber(String input)
    {
        String cardNumber = "";
        String strCardFirst = input.substring(0,(input.length()-4));
        String strCardEnd = input.substring(input.length()-4);

        Log.e("call","strCardFirst = "+strCardFirst);
        Log.e("call","strCardEnd = "+strCardEnd);

        String displayCard = "";
        for (int i=0; i<strCardFirst.length(); i++)
        {
            displayCard = displayCard + "*";
        }

        displayCard = displayCard + strCardEnd;



        for (int i=0; i<displayCard.length(); i++)
        {
            if (i>3 && (i%4)==0)
            {
                cardNumber = cardNumber  + " " + displayCard.substring(i,i+1);
            }
            else
            {
                cardNumber = cardNumber  + displayCard.substring(i,i+1);
            }
        }

        return cardNumber;
    }


    public void FindCardType(CharSequence s) {
        String valid = "INVALID";

        if (s.toString().length() > 5) {
            String number = s.toString().replace(" ", "");
            String digit1 = number.substring(0, 1);
            String digit2 = number.substring(0, 2);
            String digit3 = number.substring(0, 3);
            String digit4 = number.substring(0, 4);

            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
            et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.colorBlack));

            if (digit1.equals("4")) {
                iv_CardImage.setBackgroundResource(R.drawable.card_visa);
                if (number.length() == 13 || number.length() == 16) {
                    valid = "VISA";
                    validornot = validCCNumber(number);

                    if (number.length() > 12) {
                        if (validornot) {
                            et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.green));
                            iv_CardImage.setBackgroundResource(R.drawable.card_visa);
                        } else {
                            et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
                        /*    new SnackbarUtils(findViewById(R.id.main_layout), getResources().getString(R.string.card_is_not_valid),
                                    ContextCompat.getColor(activity, R.color.snakbar_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();*/
                            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
                        }
                    }
                }
            } else if (digit4.equalsIgnoreCase("5018") || digit4.equalsIgnoreCase("5020") || digit4.equalsIgnoreCase("5038") || digit4.equalsIgnoreCase("5612") || digit4.equalsIgnoreCase("5893")
                    || digit4.equalsIgnoreCase("6304") || digit4.equalsIgnoreCase("6759") || digit4.equalsIgnoreCase("6761") || digit4.equalsIgnoreCase("6762") || digit4.equalsIgnoreCase("6763")
                    || digit4.equalsIgnoreCase("0604") || digit4.equalsIgnoreCase("6390")) {
                iv_CardImage.setBackgroundResource(R.drawable.card_maestro);
                if (number.length() == 16) {
                    valid = "MAESTRO";
                    Log.e("MAESTRO", "MAESTRO");
                    validornot = validCCNumber(number);

                    if (number.length() == 16) {
                        if (validornot) {
                            et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.green));
                            iv_CardImage.setBackgroundResource(R.drawable.card_maestro);

                        } else {
                            et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
                            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
                        }
                    }
                }
            } else if (digit2.equals("34") || digit2.equals("37")) {
                iv_CardImage.setBackgroundResource(R.drawable.card_american);
                if (number.length() == 15) {
                    valid = "AMERICAN_EXPRESS";
                    validornot = validCCNumber(number);

                    if (number.length() == 15) {
                        if (validornot) {
                            et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.green));
                            iv_CardImage.setBackgroundResource(R.drawable.card_american);

                        } else {
                            et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
                            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
                        }
                    }
                }
            } else if (digit2.equals("36") || digit2.equals("38") || (digit3.compareTo("300") >= 0 && digit3.compareTo("305") <= 0)) {
                iv_CardImage.setBackgroundResource(R.drawable.card_dinner);
                if (number.length() == 14) {
                    valid = "DINERS_CLUB";
                    validornot = validCCNumber(number);

                    if (number.length() == 14) {
                        if (validornot) {
                            iv_CardImage.setBackgroundResource(R.drawable.card_dinner);
                            et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.green));

                        } else {
                            et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
                            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
                        }
                    }
                }

            } else if (digit1.equals("6")) {
                iv_CardImage.setBackgroundResource(R.drawable.card_discover);
                if (number.length() == 16) {
                    valid = "DISCOVER";
                    validornot = validCCNumber(number);

                    if (number.length() == 16) {
                        if (validornot) {
                            iv_CardImage.setBackgroundResource(R.drawable.card_discover);
                            et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.green));

                        } else {
                            et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
                            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
                        }
                    }
                }

            } else if (digit2.equals("35")) {
                iv_CardImage.setBackgroundResource(R.drawable.card_jcbs);
                if (number.length() == 16 || number.length() == 17 || number.length() == 18 || number.length() == 19) {
                    valid = "JBC";
                    validornot = validCCNumber(number);

                    if (number.length() > 15) {
                        if (validornot) {
                            iv_CardImage.setBackgroundResource(R.drawable.card_jcbs);
                            et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.green));

                        } else {
                            et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
                            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
                        }
                    }
                }

            } else if (digit2.compareTo("51") >= 0 && digit2.compareTo("55") <= 0 || digit1.equalsIgnoreCase("2")) {
                iv_CardImage.setBackgroundResource(R.drawable.card_master);
                if (number.length() == 16)
                    valid = "MASTERCARD";
                validornot = validCCNumber(number);

                if (number.length() == 16) {
                    if (validornot) {
                        iv_CardImage.setBackgroundResource(R.drawable.card_master);
                        et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.green));

                    } else {
                        et_CardNumber.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
                        iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
                    }
                }
            }
        }
        else {
            iv_CardImage.setBackgroundResource(R.drawable.card_dummy);
        }

    }

    public static boolean validCCNumber(String n) {
        try {

            int j = n.length();

            String [] s1 = new String[j];
            for (int i=0; i < n.length(); i++) s1[i] = "" + n.charAt(i);

            int checksum = 0;

            for (int i=s1.length-1; i >= 0; i-= 2) {
                int k = 0;

                if (i > 0) {
                    k = Integer.valueOf(s1[i-1]).intValue() * 2;
                    if (k > 9) {
                        String s = "" + k;
                        k = Integer.valueOf(s.substring(0,1)).intValue() +
                                Integer.valueOf(s.substring(1)).intValue();
                    }
                    checksum += Integer.valueOf(s1[i]).intValue() + k;
                }
                else
                    checksum += Integer.valueOf(s1[0]).intValue();
            }
            return ((checksum % 10) == 0);
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            TiCKTOC_Driver_Application.setCurrentActivity(activity);
        }
    }


    private void AddCardIn_List(String driverId)
    {
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_ADD_NEW_CARD;
        String cardNumber;
        if(!flagScan)
        {
            cardNumber = et_CardNumber.getText().toString();
        }
        else
        {
            cardNumber = strCardNumber;
        }
        cardNumber = cardNumber.replace(" ","");

        params.put(WebServiceAPI.ADD_NEW_CARD_PARAM_DRIVER_ID, driverId);
        params.put(WebServiceAPI.ADD_NEW_CARD_PARAM_CARD_NO, cardNumber);
        params.put(WebServiceAPI.ADD_NEW_CARD_PARAM_CVV, et_cvv.getText().toString());
        params.put(WebServiceAPI.ADD_NEW_CARD_PARAM_EXPIRY, ExpiryDate);
        params.put(WebServiceAPI.ADD_NEW_CARD_PARAM_ALIAS, et_alias.getText().toString());

        Log.e("url", "AddCardIn_List = " + url);
        Log.e("param", "AddCardIn_List = " + params);


        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "AddCardIn_List = " + responseCode);
                    Log.e("Response", "AddCardIn_List = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");
                                Wallet_Cards_Activity.cardList.clear();
                                if (json.has("message"))
                                {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                }
                                if (json.has("cards"))
                                {
                                    String cards = json.getString("cards");
                                    if (cards!=null && !cards.equalsIgnoreCase(""))
                                    {
                                        JSONArray cardsArray = json.getJSONArray("cards");
                                        if (cardsArray!=null && cardsArray.length()>0)
                                        {
                                            for (int i=0; i<cardsArray.length(); i++)
                                            {
                                                String Id = "", CardNum="", CardNum2="", Type="", Alias="";

                                                JSONObject cardObj = cardsArray.getJSONObject(i);
                                                if (cardObj.has("Id"))
                                                {
                                                    Id = cardObj.getString("Id");
                                                }
                                                if (cardObj.has("CardNum"))
                                                {
                                                    CardNum = cardObj.getString("CardNum");
                                                }
                                                if (cardObj.has("CardNum2"))
                                                {
                                                    CardNum2 = cardObj.getString("CardNum2");
                                                }
                                                if (cardObj.has("Type"))
                                                {
                                                    Type = cardObj.getString("Type");
                                                }
                                                if (cardObj.has("Alias"))
                                                {
                                                    Alias = cardObj.getString("Alias");
                                                }
                                                Log.e("GetCardList","Id : "+Id+"\nCardNum : "+CardNum+"\nCardNum2 : "+CardNum2+"\nType : "+Type+"\nAlias : "+Alias);
                                                Wallet_Cards_Activity.cardList.add(new CreditCard_List_Been(Id, Alias, CardNum2, Type));
                                            }
                                            SessionSave.saveUserSession(Comman.DRIVER_CARD_COUNT_ADDED_IN_LIST, Wallet_Cards_Activity.cardList.size()+"", activity);
                                        }
                                        else
                                        {
                                            Log.e("status", "false");
                                            dialogClass.hideDialog();
                                            if (json.has("message")) {
                                                new SnackbarUtils(main_layout, json.getString("message"),
                                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Log.e("status", "false");
                                        dialogClass.hideDialog();
                                        if (json.has("message")) {
                                            new SnackbarUtils(main_layout, json.getString("message"),
                                                    ContextCompat.getColor(activity, R.color.snakbar_color),
                                                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                        }
                                    }
                                }
                                else
                                {
                                    Log.e("status", "false");
                                    dialogClass.hideDialog();
                                    if (json.has("message")) {
                                        new SnackbarUtils(main_layout, json.getString("message"),
                                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                    }
                                }
                                dialogClass.hideDialog();
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        finish();
                                    }
                                }, 300);
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message")) {
                                new SnackbarUtils(main_layout, json.getString("message"),
                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();

                    }
                }
                catch (Exception e)
                {
                    Log.e("AddCardIn_List", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }.header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        mNfcUtils.disableDispatch();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (Wallet_Cards_Activity.from!=null && Wallet_Cards_Activity.from.equalsIgnoreCase("drawer"))
        {

        }
        else
        {
            if (SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity) != null && !SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("")
                    && SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
            {
                Intent intent = new Intent(activity,Create_Passcode_Activity.class);
                startActivity(intent);
                if(Wallet_Cards_Activity.activity!=null)
                {
                    Wallet_Cards_Activity.activity.finish();
                }
                finish();
            }
        }
    }

    public boolean checkDateValidation(int year,int month)
    {
        Log.e(TAG, "checkDateValidation() year:- " + year);
        Log.e(TAG, "checkDateValidation() month:- " + month);

        int day = getLastDayOfMonth(month+"/"+year);

        Calendar today = Calendar.getInstance();
        today.clear(Calendar.HOUR); today.clear(Calendar.MINUTE); today.clear(Calendar.SECOND);
        Date todayDate = today.getTime();

        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, day);
        //Date dateSpecified = c.getTime();

        Calendar myCalendar = new GregorianCalendar(year, month-1, day);
        Date dateSpecified = myCalendar.getTime();

        if(dateSpecified.before(todayDate))
        {
            return false;
        }
        return true;
    }

    public int getLastDayOfMonth(String dateString)
    {
        Log.e(TAG, "getLastDayOfMonth() dateString:- " + dateString);

        DateFormat dateFormat = new SimpleDateFormat("MM/yy");
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(dateFormat.parse(dateString));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }
}