package iiride.app.driver.Activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import iiride.app.driver.Application.TiCKTOC_Driver_Application;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Fragment.Register_Attachment_Fragment;
import iiride.app.driver.Fragment.Register_Email_Fragment;
import iiride.app.driver.Fragment.Register_ProfileDetail_Fragment;
import iiride.app.driver.Fragment.Register_VehicleType_Fragment;
import iiride.app.driver.Fragment.Register_Verification_Fragment;
import iiride.app.driver.R;


public class Registration_Tab_Activity extends AppCompatActivity implements View.OnClickListener
{
    public static Registration_Tab_Activity activity;


    public static ImageView iv_back;
    public static LinearLayout ll_back;

    LinearLayout ll_1, ll_2, ll_3, ll_4, ll_5;
    ImageView iv_1, iv_2, iv_3, iv_4, iv_5;


    public static String clickedType="";
    public static int ClickTabNumber = 0;

    FrameLayout frameRegister;

    String Email_session="", Otp_session="", profile_session="", vehicle_session="";
    public static TextView tv_signOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_tab);

        activity = Registration_Tab_Activity.this;

        clickedType="";
        ClickTabNumber = 0;



        initUI();
    }

    private void initUI() {

        frameRegister = (FrameLayout) findViewById(R.id.frame_Register);

        iv_back = (ImageView) findViewById(R.id.iv_back);

        tv_signOut = (TextView) findViewById(R.id.tv_signOut);

        ll_back = (LinearLayout) findViewById(R.id.ll_back);

        ll_1 = (LinearLayout) findViewById(R.id.ll_1);
        ll_2 = (LinearLayout) findViewById(R.id.ll_2);
        ll_3 = (LinearLayout) findViewById(R.id.ll_3);
        ll_4 = (LinearLayout) findViewById(R.id.ll_4);
        ll_5 = (LinearLayout) findViewById(R.id.ll_5);

        iv_1 = (ImageView) findViewById(R.id.iv_1);
        iv_2 = (ImageView) findViewById(R.id.iv_2);
        iv_3 = (ImageView) findViewById(R.id.iv_3);
        iv_4 = (ImageView) findViewById(R.id.iv_4);
        iv_5 = (ImageView) findViewById(R.id.iv_5);


        iv_back.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        tv_signOut.setOnClickListener(this);
        ll_1.setOnClickListener(this);
        ll_2.setOnClickListener(this);
        ll_3.setOnClickListener(this);
        ll_4.setOnClickListener(this);
        ll_5.setOnClickListener(this);

        Email_session = SessionSave.getUserSession(Comman.USER_EMAIL,activity);
        Otp_session = SessionSave.getUserSession(Comman.REGISTER_OTP_ACTIVITY,activity);
        profile_session = SessionSave.getUserSession(Comman.REGISTER_PROFILE_ACTIVITY,activity);
        vehicle_session = SessionSave.getUserSession(Comman.REGISTER_VEHICLE_ACTIVITY,activity);

        if (Email_session!=null && !Email_session.equalsIgnoreCase(""))
        {
            if (Otp_session!=null && !Otp_session.equalsIgnoreCase("") && Otp_session.equalsIgnoreCase("1"))
            {
                if (profile_session!=null && !profile_session.equalsIgnoreCase("") && profile_session.equalsIgnoreCase("1"))
                {
                    if (vehicle_session!=null && !vehicle_session.equalsIgnoreCase("") && vehicle_session.equalsIgnoreCase("1"))
                    {
                        setTabAttachment();
                    }
                    else
                    {
                        setTabCarInfo1();
                    }
                }
                else
                {
                    setTabAddProfileDetails();
                }
            }
            else
            {
                setTabVarification();
            }
        }
        else
        {
            setTabEmail();
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.ll_back:
                onBackPressed();
                break;

           /* case R.id.ll_1:
                setTabEmail();
                break;

            case R.id.ll_2:
               setTabVarification();
                break;

            case R.id.ll_3:
                setTabAddProfileDetails();
                break;

            case R.id.ll_4:
                setTabCarInfo1();
                break;

            case R.id.ll_5:
                setTabAttachment();
                break;*/

            case R.id.tv_signOut:
                SessionSave.clearUserSession(activity);
                Intent intent = new Intent(getApplicationContext(), Main_Activity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                break;

        }
    }



    public void setTabEmail()
    {
        Register_Email_Fragment fragment = new Register_Email_Fragment();
        FragmentTransaction transaction =getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_Register, fragment);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();

        ClickTabNumber = 0;
        iv_1.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.icon_mail_selected));
        iv_2.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.icon_otp));
        iv_3.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.icon_tab_profile));
        iv_4.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.icon_vehicle));
        iv_5.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.icon_attachment));
    }


    public void setTabVarification()
    {
        Register_Verification_Fragment fragment = new Register_Verification_Fragment();
        FragmentTransaction transaction =getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.right_in, R.anim.left_out);
        transaction.replace(R.id.frame_Register, fragment);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();

        ClickTabNumber = 1;
        iv_1.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.icon_mail_selected));
        iv_2.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.icon_otp_selected));
        iv_3.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.icon_tab_profile));
        iv_4.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.icon_vehicle));
        iv_5.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.icon_attachment));
    }

    public void setTabAddProfileDetails()
    {
        Register_ProfileDetail_Fragment fragment = new Register_ProfileDetail_Fragment();
        FragmentTransaction transaction =getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.right_in, R.anim.left_out);
        transaction.replace(R.id.frame_Register, fragment);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();

        ClickTabNumber = 2;
        iv_1.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.icon_mail_selected));
        iv_2.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.icon_otp_selected));
        iv_3.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.icon_tab_profile_selected));
        iv_4.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.icon_vehicle));
        iv_5.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.icon_attachment));
    }


    public void setTabCarInfo1()
    {
        Register_VehicleType_Fragment fragment = new Register_VehicleType_Fragment();
        FragmentTransaction transaction =getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.right_in, R.anim.left_out);
        transaction.replace(R.id.frame_Register, fragment);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();

        ClickTabNumber = 3;
        iv_1.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.icon_mail_selected));
        iv_2.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.icon_otp_selected));
        iv_3.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.icon_tab_profile_selected));
        iv_4.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.icon_vehicle_selected));
        iv_5.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.icon_attachment));
    }

    public void setTabAttachment()
    {
        Register_Attachment_Fragment fragment = new Register_Attachment_Fragment();
        FragmentTransaction transaction =getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.right_in, R.anim.left_out);
        transaction.replace(R.id.frame_Register, fragment);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();

        ClickTabNumber = 4;
        iv_1.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.icon_mail_selected));
        iv_2.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.icon_otp_selected));
        iv_3.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.icon_tab_profile_selected));
        iv_4.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.icon_vehicle_selected));
        iv_5.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.icon_attachment_selected));
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void LogoutButton_Display(int i)
    {
        if (i==1)
        {
            tv_signOut.setVisibility(View.VISIBLE);
            ll_back.setVisibility(View.GONE);
        }
        else
        {
            tv_signOut.setVisibility(View.GONE);
            ll_back.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            TiCKTOC_Driver_Application.setCurrentActivity(activity);
        }
    }
}
