package iiride.app.driver.Activity;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import iiride.app.driver.Application.TiCKTOC_Driver_Application;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;
import iiride.app.driver.Fragment.Tick_Pay_Fragment;
import iiride.app.driver.Others.ConnectivityReceiver;


import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Receipt_Invoice_Activity extends AppCompatActivity implements View.OnClickListener{

    public static Receipt_Invoice_Activity activity;

    Spinner Spinner_sendVia;
    TextView tv_send;
    LinearLayout main_layout, ll_back;

    String[] items = new String[] { "Send Via..", "Send Via Email", "Send Via SMS" };
    private String total="";
    private EditText et_Name,et_Description,et_Amount,et_Email,et_Phone;
    public static int selectedInvoice = 0;
    private AQuery aQuery;
    DialogClass dialogClass;

    private RadioGroup radioGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt_invoice);

        activity = Receipt_Invoice_Activity.this;
        dialogClass = new DialogClass(activity,0);
        aQuery = new AQuery(activity);

        if (this.getIntent()!=null)
        {
            if (this.getIntent().getStringExtra("total")!=null)
            {
                total = this.getIntent().getStringExtra("total");
            }
            else
            {
                total = "";
            }
        }
        else
        {
            total = "";
        }
        selectedInvoice = 0;
        init();
    }

    private void init() {

        main_layout = (LinearLayout) findViewById(R.id.main_layout);
        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        Spinner_sendVia = (Spinner) findViewById(R.id.Spinner_sendVia);
        tv_send = (TextView) findViewById(R.id.tv_send);

        et_Name = (EditText) findViewById(R.id.et_name);
        et_Description = (EditText) findViewById(R.id.et_description);
        et_Amount = (EditText) findViewById(R.id.et_Amount);
        et_Amount.setEnabled(false);
        et_Amount.setText("$"+total);
        et_Email = (EditText) findViewById(R.id.et_Email);
        et_Phone = (EditText) findViewById(R.id.et_Phone);

        String description = SessionSave.getUserSession(Comman.USER_SERVICE_DESCRIPTION, activity);
        if (description!=null && !description.equalsIgnoreCase(""))
        {
            et_Description.setText(description);
        }

        ll_back.setOnClickListener(this);
        tv_send.setOnClickListener(this);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, R.layout.spinner_item, items);

        Spinner_sendVia.setAdapter(adapter);

        Spinner_sendVia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e("item", (String) parent.getItemAtPosition(position));

                selectedInvoice = position;
                if (position==0)
                {
                    et_Email.setVisibility(View.GONE);
                    et_Phone.setVisibility(View.GONE);
                }
                else if (position==1)
                {
                    et_Email.setVisibility(View.VISIBLE);
                    et_Phone.setVisibility(View.GONE);
                }
                else if (position==2)
                {
                    et_Email.setVisibility(View.GONE);
                    et_Phone.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        selectedInvoice=1;
        et_Email.setVisibility(View.VISIBLE);
        et_Phone.setVisibility(View.GONE);
//        radioGroup.clearCheck();

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                if (null != rb && checkedId > -1) {
//                    Toast.makeText(activity, rb.getText(), Toast.LENGTH_SHORT).show();
                    if (rb.getText().toString().equalsIgnoreCase("Send Via Email"))
                    {
                        selectedInvoice=1;
                        et_Email.setVisibility(View.VISIBLE);
                        et_Phone.setVisibility(View.GONE);
                    }
                    else
                    {
                        selectedInvoice=2;
                        et_Email.setVisibility(View.GONE);
                        et_Phone.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.tv_send:
               /* if (selectedInvoice==0)
                {
                    InternetDialog internetDialog = new InternetDialog(activity);
                    internetDialog.showDialog("Please select Invoice Type!");
                }
                else*/ if (et_Name.getText().toString().trim().equalsIgnoreCase(""))
                {
                    new SnackbarUtils(main_layout, "Please enter Customer/Business name.",
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
                else if (et_Description.getText().toString().trim().equalsIgnoreCase(""))
                {
                    new SnackbarUtils(main_layout, "Please enter Description.",
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
                else if (et_Amount.getText().toString().trim().equalsIgnoreCase(""))
                {
                    new SnackbarUtils(main_layout, "Please enter Amount.",
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
                else
                {
                    Log.e("call","enter in else");
                    if (selectedInvoice==1)
                    {
                        if (et_Email.getText().toString().trim().equalsIgnoreCase(""))
                        {
                            new SnackbarUtils(main_layout, "Please enter Email.",
                                    ContextCompat.getColor(activity, R.color.snakbar_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                        }
                        else if (!TiCKTOC_Driver_Application.isEmailValid(et_Email.getText().toString().trim()))
                        {
                            new SnackbarUtils(main_layout, "Please enter valid Email.",
                                    ContextCompat.getColor(activity, R.color.snakbar_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                        }
                        else
                        {
                            Log.e("call","enter in else 111");
                            if (ConnectivityReceiver.isConnected())
                            {
                                send();
                            }
                            else
                            {
                                new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }
                    }
                    else
                    {
                        if (et_Phone.getText().toString().trim().equalsIgnoreCase(""))
                        {
                            new SnackbarUtils(main_layout, "Please enter Phone Number.",
                                    ContextCompat.getColor(activity, R.color.snakbar_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                        }
                        else if (et_Phone.getText().toString().trim().length()!=10)
                        {
                            new SnackbarUtils(main_layout, "Please enter valid Phone Number.",
                                    ContextCompat.getColor(activity, R.color.snakbar_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                        }
                        else
                        {
                            Log.e("call","enter in else 222");
                            if (ConnectivityReceiver.isConnected())
                            {
                                send();
                            }
                            else
                            {
                                new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            TiCKTOC_Driver_Application.setCurrentActivity(activity);
        }
    }

    public void send()
    {
        dialogClass.showDialog();
        String url = WebServiceAPI.WEB_SERVICE_TICK_PAY_INVOICE;
        //TickpayId,InvoiceType,CustomerName,Notes,Amount,Email,MobileNo(InvoiceType : SMS/Email)

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_TICKPAY_ID, Tick_Pay_Fragment.payId);

        if (selectedInvoice==1)
        {
            params.put(WebServiceAPI.PARAM_INVOICE_TYPE, WebServiceAPI.PARAM_INVOICE_TYPE_EMAIL);
            params.put(WebServiceAPI.PARAM_EMAIL, et_Email.getText().toString());
        }
        else if (selectedInvoice==2)
        {
            params.put(WebServiceAPI.PARAM_INVOICE_TYPE, WebServiceAPI.PARAM_INVOICE_TYPE_SMS);
            params.put(WebServiceAPI.PARAM_MOBILE_NO, et_Phone.getText().toString());
        }

        params.put(WebServiceAPI.PARAM_CUSTOMER_NAME, et_Name.getText().toString());
        params.put(WebServiceAPI.PARAM_NOTES, et_Description.getText().toString());
//        params.put(WebServiceAPI.PARAM_AMOUNT, et_Amount.getText().toString().substring(1,et_Amount.getText().toString().length()));
        params.put(WebServiceAPI.PARAM_AMOUNT, Tick_Pay_Fragment.InvoiceRate);


        Log.e("call", "url = " + url);
        Log.e("call", "params = " + params);

        aQuery.ajax(url.trim(), params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                dialogClass.hideDialog();
                                String message = "Payment Receipt send successfully!";

                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                }
                                showSuccessMessage(message);
                            }
                            else
                            {
                                String message = "Please try again later";
                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                }
                                dialogClass.hideDialog();
                                new SnackbarUtils(main_layout, message,
                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }
                        else
                        {
                            String message = "Please try again later";
                            if (json.has("message"))
                            {
                                message = json.getString("message");
                            }
                            dialogClass.hideDialog();
                            new SnackbarUtils(main_layout, message,
                                    ContextCompat.getColor(activity, R.color.snakbar_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                        }
                    }
                    else
                    {
                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout, getResources().getString(R.string.something_is_wrong),
                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();
                    new SnackbarUtils(main_layout, getResources().getString(R.string.something_is_wrong),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }

        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }
    public void showSuccessMessage(String message)
    {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.tick_pay_ok_dialog);

        TextView tv_Ok = (TextView) dialog.findViewById(R.id.Ok);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        LinearLayout ll_Ok = (LinearLayout) dialog.findViewById(R.id.ok_layout);

        tv_Message.setText(message);

        ll_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finish();
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finish();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }
}
