package iiride.app.driver.Activity;

import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import iiride.app.driver.Application.TiCKTOC_Driver_Application;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.SocketSingleObject;
import iiride.app.driver.Comman.WebServiceAPI;
import iiride.app.driver.Others.ConnectivityReceiver;


import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.SnackbarUtils;
import iiride.app.driver.notification.FCMUtil;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONObject;


public class Splash_Activity extends AppCompatActivity {

    Splash_Activity activity;
    private static final String TAG = "Splash_Activity";

    public int SPLASH_TIME_OUT = 3000;
    private AQuery aQuery;
    DialogClass dialogClass;
    RelativeLayout main_layout;
    String message = "", version = "";
    String DialogType = "", COMP_UPDATE = "CompUpdate", OPTIONAL_UPDATE = "OptionalUpdate", UNDER_MAINTENANCE = "UnderMaintenance";
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        activity = Splash_Activity.this;

        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 1);
//        String token = InstanceID.getInstance(getApplicationContext()).getToken(senderID, GCM);

        NotificationManager nMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nMgr.cancelAll();
        //SessionSave.saveUserSession(Comman.NOTIFICATION_PUT_EXTRA, "BookLaterDriverNotify", getApplicationContext());

        new Thread(new Runnable() {
            public void run() {
                try
                {
                    FirebaseInstanceId.getInstance().getInstanceId()
                            .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                                @Override
                                public void onComplete(@NonNull Task<InstanceIdResult> task) {

                                    try
                                    {
                                        token = task.getResult().getToken();
                                        Log.e(TAG, "getTokan() token:- " + token);

                                        SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);

                                        SharedPreferences.Editor editor = pref.edit();
                                        editor.putString("token", token);
                                        editor.commit();

                                        FCMUtil.saveToken(activity, token);

                                        Log.e(TAG, "getTokan() FCMUtil token:- " + FCMUtil.getFcmToken(activity));
                                        SessionSave.saveToken(Comman.DEVICE_TOKEN, token, activity);
                                    }
                                    catch (Exception e)
                                    {
                                        Log.e(TAG, "getTokan() Exception:- " + e.getMessage());
                                    }

                                }
                            });
                }
                catch (Exception e)
                {
                    Log.e("DEVICE_TOKEN", "Failed to complete token refresh", e);
                }
            }
        }).start();

        main_layout = (RelativeLayout) findViewById(R.id.main_layout);

        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;

           /* ///And you can get the version code by using this
            int verCode = pInfo.versionCode;*/
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (version != null && !version.equalsIgnoreCase("")) {
            if (ConnectivityReceiver.isConnected()) {
                checkAppSetting(version);
            } else {
                new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                        ContextCompat.getColor(activity, R.color.snakbar_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
            }
        }
    }

    private void init() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                String LoginFlag_User = SessionSave.getUserSession(Comman.LOGIN_FLAG_USER, activity);
                String Email_session = SessionSave.getUserSession(Comman.REGISTER_EMAIL_ACTIVITY, activity);
                Log.e("wwww", "wwwwwwwwwwwwwww" + LoginFlag_User);
                if (LoginFlag_User != null && !LoginFlag_User.equalsIgnoreCase("") && LoginFlag_User.equalsIgnoreCase("1")) {
                    Intent i = new Intent(Splash_Activity.this, Drawer_Activity.class);
                    startActivity(i);
                    finish();
                } else if (Email_session != null && !Email_session.equalsIgnoreCase("") && Email_session.equalsIgnoreCase("1")) {
                    Intent i = new Intent(Splash_Activity.this, Registration_Tab_Activity.class);
                    startActivity(i);
                    finish();
                } else {
                    Intent i = new Intent(Splash_Activity.this, Main_Activity.class);
                    startActivity(i);
                    finish();
                }
                disconnectSocket();

            }
        }, SPLASH_TIME_OUT);
    }

    public void disconnectSocket() {
        Log.e("call", "connectSocket()");
        try {
            if (Comman.socket != null && Comman.socket.connected()) {
                Comman.socket.disconnect();
                Comman.socket = null;
                SocketSingleObject.instance = null;
            } else {
                Log.e("call", "Socket already disconnected.");
            }
        } catch (Exception e) {
            Log.e("call", "Socket connect exception = " + e.getMessage());
        }
    }

    private void checkAppSetting(String version) {
        dialogClass.showDialog();
        String url = WebServiceAPI.WEB_SERVICE_APP_SETTING + version + WebServiceAPI.APP_SETTING_APP_TYPE;

        Log.e("checkAppSetting", "url = " + url);
        message = "";

        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json != null) {
                        SessionSave.saveInit(Comman.INIT_DATA, json.toString(), activity);
                        if (json.has("message")) {
                            message = json.getString("message");
                        }
                        if (json.has("status")) {
                            dialogClass.hideDialog();
                            if (json.getBoolean("status")) {
                                if (json.has("update")) {
                                    if (json.getBoolean("update")) {
                                        init();
                                    } else {
                                        DialogType = OPTIONAL_UPDATE;
                                        OpenDialogForNotice(DialogType, message);
                                    }
                                } else {
                                    init();
                                }
                            } else {
                                if (json.has("update")) {
                                    if (json.getBoolean("update")) {
                                        DialogType = COMP_UPDATE;
                                        OpenDialogForNotice(DialogType, message);
                                    } else {
                                        DialogType = UNDER_MAINTENANCE;
                                        OpenDialogForNotice(DialogType, message);
                                    }
                                } else {
                                    if (json.has("maintenance")) {
                                        if (json.getBoolean("maintenance")) {
                                            DialogType = UNDER_MAINTENANCE;
                                            OpenDialogForNotice(DialogType, message);
                                        }
                                    } else {
                                        DialogType = "";
                                        OpenDialogForNotice(DialogType, message);
                                    }
                                }
                            }
                        } else {
                            dialogClass.hideDialog();
                            DialogType = "";
                            OpenDialogForNotice(DialogType, message);
                        }

                    } else {
                        Log.e("checkAppSetting", "json null");
                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout, getResources().getString(R.string.something_is_wrong),
                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                    }
                } catch (Exception e) {
                    Log.e("Exception", "Exception " + e.toString());
                    dialogClass.hideDialog();
                    new SnackbarUtils(main_layout, getResources().getString(R.string.something_is_wrong),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }

    private void OpenDialogForNotice(final String dialogType, String message) {
        final Dialog dialogNotice = new Dialog(activity, R.style.DialogTheme);
        dialogNotice.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogNotice.getWindow().getAttributes());
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.MATCH_PARENT;
        dialogNotice.getWindow().setAttributes(lp);
        dialogNotice.setCancelable(false);
        dialogNotice.setContentView(R.layout.dialog_app_notice);

        final TextView tv_message, tv_update, tv_updateLater;

        tv_message = (TextView) dialogNotice.findViewById(R.id.tv_message);
        tv_update = (TextView) dialogNotice.findViewById(R.id.tv_update);
        tv_updateLater = (TextView) dialogNotice.findViewById(R.id.tv_updateLater);


        if (dialogType != null && !dialogType.equalsIgnoreCase("")) {
            if (dialogType.equalsIgnoreCase(COMP_UPDATE)) {
                tv_message.setText(message);
                tv_update.setText("Update");
                tv_updateLater.setVisibility(View.GONE);
            } else if (dialogType.equalsIgnoreCase(OPTIONAL_UPDATE)) {
                tv_message.setText(message);
                tv_update.setText("Update");
                tv_updateLater.setVisibility(View.VISIBLE);
            } else if (dialogType.equalsIgnoreCase(UNDER_MAINTENANCE)) {
                tv_message.setText(message);
                tv_update.setText("ok");
                tv_updateLater.setVisibility(View.GONE);
            }
        } else {
            tv_message.setText(getResources().getString(R.string.something_is_wrong));
            tv_update.setText("ok");
            tv_updateLater.setVisibility(View.GONE);
        }

        tv_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialogNotice.dismiss();
                if (!dialogType.equalsIgnoreCase(UNDER_MAINTENANCE)) {
                    OpenPlayStoreLink();
                } else {
                    finish();
                }

            }
        });

        tv_updateLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogNotice.dismiss();
                init();
            }
        });
        dialogNotice.show();
    }

    private void OpenPlayStoreLink() {
        final String appPackageName = activity.getPackageName(); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (activity != null) {
            TiCKTOC_Driver_Application.setCurrentActivity(activity);
        }
    }

}
