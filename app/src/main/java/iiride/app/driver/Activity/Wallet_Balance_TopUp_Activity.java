package iiride.app.driver.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import iiride.app.driver.Adapter.CreditCardList_Adapter;
import iiride.app.driver.Application.TiCKTOC_Driver_Application;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;
import iiride.app.driver.Others.ConnectivityReceiver;


import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Wallet_Balance_TopUp_Activity extends AppCompatActivity implements View.OnClickListener
{
    public static Wallet_Balance_TopUp_Activity activity;

    LinearLayout ll_back;
    ImageView iv_back;
    TextView tv_selectCard, tv_add_funds;
    Intent intent;
    public static int flagResumeUse=0;
    String ClickcardId="";
    EditText et_topUpAmount;

    private AQuery aQuery;
    private DialogClass dialogClass;
    LinearLayout main_layout;

    int resumeFlag=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_balance_addfunds);

        activity = Wallet_Balance_TopUp_Activity.this;

        flagResumeUse=0;
        ClickcardId="";
        resumeFlag=1;

        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 1);

        initUI();
    }

    private void initUI()
    {
        main_layout = (LinearLayout) findViewById(R.id.main_layout);
        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        tv_selectCard = (TextView) findViewById(R.id.tv_selectCard);
        tv_add_funds = (TextView) findViewById(R.id.tv_add_funds);
        et_topUpAmount = (EditText) findViewById(R.id.et_topUpAmount);

        et_topUpAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                if(charSequence.length()>0)
                {
                    if (charSequence.toString().trim().length()==1 && charSequence.toString().equalsIgnoreCase("."))
                    {
                        et_topUpAmount.setText("");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        ll_back.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        tv_selectCard.setOnClickListener(this);
        tv_add_funds.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.tv_selectCard:
                resumeFlag=0;
                intent = new Intent(activity, Wallet_Cards_Activity.class);
                intent.putExtra("from","TopUp");
                startActivity(intent);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                break;

            case R.id.tv_add_funds:
                checkData();
                break;
        }
    }

    private void checkData() {

        if (CreditCardList_Adapter.ClickecardNumber == null || TextUtils.isEmpty(CreditCardList_Adapter.ClickecardNumber))
        {
            new SnackbarUtils(main_layout, "Please select Card",
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else if (CreditCardList_Adapter.ClickcardId == null || CreditCardList_Adapter.ClickcardId.equalsIgnoreCase(""))
        {
            new SnackbarUtils(main_layout, "Please select Card",
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else if (TextUtils.isEmpty(et_topUpAmount.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, "Please enter amount",
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else
        {
            String userId = SessionSave.getUserSession(Comman.USER_ID, activity);
            if (userId!=null && !userId.equalsIgnoreCase(""))
            {
                if (ConnectivityReceiver.isConnected())
                {
                    sendMoney(userId);
                }
                else
                {
                    new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }
    }

    public void sendMoney(String userId)
    {
        dialogClass.showDialog();

        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_ADD_MONEY;

        params.put(WebServiceAPI.ADD_MONEY_PARAM_DRIVER_ID,userId);
        params.put(WebServiceAPI.ADD_MONEY_PARAM_AMOUNT,et_topUpAmount.getText().toString());
        params.put(WebServiceAPI.ADD_MONEY_PARAM_CARD_ID,CreditCardList_Adapter.ClickcardId);

        Log.e("sendMoney", "URL = " + url);
        Log.e("sendMoney", "PARAMS = " + params);

        aQuery.ajax(url, params , JSONObject.class , new AjaxCallback<JSONObject>()
        {
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status)
            {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("sendMoney", "responseCode = " + responseCode);
                    Log.e("sendMoney", "Response = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if(json.getBoolean("status"))
                            {
                                Log.e("sendMoney","Status = " + "true");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                }

                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        finish();
                                    }
                                }, 300);
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }

                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message")) {
                                new SnackbarUtils(main_layout, json.getString("message"),
                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                            }

                        }

                    }
                    else
                    {
                        Log.e("sendMoney", "getMessage = "  + "Null");
                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout,getString(R.string.something_is_wrong),
                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                    }

                }
                catch (Exception e)
                {
                    Log.e("sendMoney", "Exception = " + e.getMessage() + "somthing_is_wrong");
                    dialogClass.hideDialog();
                    new SnackbarUtils(main_layout,getString(R.string.something_is_wrong),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();

                }

            }
        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY_NAME,WebServiceAPI.HEADER_KEY_VALUE));
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            TiCKTOC_Driver_Application.setCurrentActivity(activity);
        }
        if (flagResumeUse==1)
        {
            if (CreditCardList_Adapter.ClickecardNumber!=null && !CreditCardList_Adapter.ClickecardNumber.equalsIgnoreCase(""))
            {
                tv_selectCard.setText(CreditCardList_Adapter.ClickecardNumber);
                ClickcardId = CreditCardList_Adapter.ClickcardId;
            }
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (resumeFlag==1)
        {
            if (SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity) != null && !SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("")
                    && SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
            {
                resumeFlag = 0;
                Intent intent = new Intent(activity,Create_Passcode_Activity.class);
                startActivity(intent);
                if (Wallet_Balance_Activity.activity!=null)
                {
                    Wallet_Balance_Activity.activity.finish();
                }
                if (Wallet_Balance_TransferToBank_Activity.activity!=null)
                {
                    Wallet_Balance_TransferToBank_Activity.activity.finish();
                }
                if (Wallet_Cards_Activity.activity!=null)
                {
                    Wallet_Cards_Activity.activity.finish();
                }
                if (Wallet_Transfer_Activity.activity!=null)
                {
                    Wallet_Transfer_Activity.activity.finish();
                }
                if (Wallet_Transfer_History_Activity.activity!=null)
                {
                    Wallet_Transfer_History_Activity.activity.finish();
                }
                finish();
            }
        }
        else
        {
            resumeFlag=1;
        }
    }
}