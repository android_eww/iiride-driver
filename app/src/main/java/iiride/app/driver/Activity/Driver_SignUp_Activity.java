package iiride.app.driver.Activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import iiride.app.driver.Application.TiCKTOC_Driver_Application;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.R;


/**
 * Created by EXCELLENT-14 on 09-Oct-17.
 */

public class Driver_SignUp_Activity extends AppCompatActivity implements View.OnClickListener{

    Driver_SignUp_Activity activity;

    TextView tv_next;
    LinearLayout ll_next_to_show, ll_termsPrivacy, ll_privacyStatement;

    Intent intent;
    Animation slide_right, slide_left;
    CheckBox checkBox_male, checkBox_Female, checkBox_others;

    int intNext = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_registration);

        activity = Driver_SignUp_Activity.this;

        SessionSave.saveUserSession(Comman.REGISTER_EMAIL_ACTIVITY,"0", activity);
        SessionSave.saveUserSession(Comman.REGISTER_OTP_ACTIVITY, "0", activity);
        SessionSave.saveUserSession(Comman.REGISTER_PROFILE_ACTIVITY, "0", activity);
        SessionSave.saveUserSession(Comman.REGISTER_VEHICLE_ACTIVITY, "0", activity);
        SessionSave.saveUserSession(Comman.REGISTER_ATTACHMENT_ACTIVITY, "0", activity);

        init();
    }

    private void init() {

        intNext = 0;

        tv_next = (TextView) findViewById(R.id.tv_next);
        ll_next_to_show = (LinearLayout) findViewById(R.id.ll_next_to_show);
        ll_termsPrivacy = (LinearLayout) findViewById(R.id.ll_termsPrivacy);
        ll_privacyStatement = (LinearLayout) findViewById(R.id.ll_privacyStatement);

        ll_privacyStatement.setVisibility(View.GONE);
        ll_termsPrivacy.setVisibility(View.VISIBLE);
        ll_next_to_show.setVisibility(View.GONE);

        checkBox_male = (CheckBox) findViewById(R.id.checkBox_male);
        checkBox_Female = (CheckBox) findViewById(R.id.checkBox_Female);
        checkBox_others = (CheckBox) findViewById(R.id.checkBox_others);

        slide_right = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_slide_in_right);
        slide_left = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_slide_in_left);


        tv_next.setOnClickListener(this);
        checkBox_male.setOnClickListener(this);
        checkBox_Female.setOnClickListener(this);
        checkBox_others.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tv_next:
                if (intNext==0)
                {
                    ll_next_to_show.setVisibility(View.VISIBLE);
                    ll_next_to_show.setAnimation(slide_left);
                    ll_privacyStatement.setVisibility(View.VISIBLE);
                    ll_privacyStatement.setAnimation(slide_left);
                    ll_termsPrivacy.setVisibility(View.GONE);
                    intNext =1;
                }
                else
                {
//                    intent = new Intent(activity, )
                }
                break;

            case R.id.checkBox_male:
                checkBox_male.setChecked(true);
                checkBox_Female.setChecked(false);
                checkBox_others.setChecked(false);
                break;

            case R.id.checkBox_Female:
                checkBox_male.setChecked(false);
                checkBox_Female.setChecked(true);
                checkBox_others.setChecked(false);
                break;

            case R.id.checkBox_others:
                checkBox_male.setChecked(false);
                checkBox_Female.setChecked(false);
                checkBox_others.setChecked(true);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            TiCKTOC_Driver_Application.setCurrentActivity(activity);
        }
    }
}
