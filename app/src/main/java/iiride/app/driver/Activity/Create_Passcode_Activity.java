package iiride.app.driver.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;


import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Create_Passcode_Activity extends AppCompatActivity
{
    public static Create_Passcode_Activity activity;

    TextView tv_zero, tv_one, tv_two, tv_three, tv_four, tv_five, tv_six, tv_seven, tv_eight, tv_nine, tv_headerPassword, tv_forgot;
    TextView tv_1, tv_2, tv_3, tv_4;
    LinearLayout shakeLayout, main_layout, ll_Erase;

    Handler handler;
    int showDialogPass=0;
    boolean activateET1=false, activateET2=false, activateET3=false, activateET4=false;

    String sessionPasscode="", firstPasscode="", secondPasscode="";
    int firstTimeCreate=0;
    public static int NfcFlag = 0;

    DialogClass dialogClass;
    AQuery aQuery;
    Intent intent;
    String from="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_passcode);

        activity = Create_Passcode_Activity.this;
        sessionPasscode="";
        firstPasscode="";
        secondPasscode="";
        Drawer_Activity.PasscodeBackPress=0;
        Drawer_Activity.flagResumePass=0;
        Wallet__Activity.flagResumePass=0;

        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 1);

        intent = getIntent();
        from="";
        if(intent != null)
        {
            Log.e("from","from :1111111111111 ");
            from = intent.getStringExtra("from");
        }
        else
        {
            Log.e("from","from :22222222222222222 ");
            from="";
        }
        Log.e("from","from 333333333333333: "+from);

        NfcFlag = 0;
        init();
    }

    private void init() {

        main_layout = (LinearLayout) findViewById(R.id.main_layout);
        shakeLayout = (LinearLayout) findViewById(R.id.shakeLayout);
//        ll_forgot = (LinearLayout) findViewById(R.id.ll_forgot);
        ll_Erase = (LinearLayout) findViewById(R.id.ll_Erase);

        tv_headerPassword = (TextView) findViewById(R.id.tv_headerPassword);

        tv_zero = (TextView) findViewById(R.id.tv_zero);
        tv_one = (TextView) findViewById(R.id.tv_one);
        tv_two = (TextView) findViewById(R.id.tv_two);
        tv_three = (TextView) findViewById(R.id.tv_three);

        tv_four = (TextView) findViewById(R.id.tv_four);
        tv_five = (TextView) findViewById(R.id.tv_five);
        tv_six = (TextView) findViewById(R.id.tv_six);
        tv_seven = (TextView) findViewById(R.id.tv_seven);

        tv_eight = (TextView) findViewById(R.id.tv_eight);
        tv_nine = (TextView) findViewById(R.id.tv_nine);

        tv_1 = (TextView) findViewById(R.id.tv_1);
        tv_2 = (TextView) findViewById(R.id.tv_2);
        tv_3 = (TextView) findViewById(R.id.tv_3);
        tv_4 = (TextView) findViewById(R.id.tv_4);
        tv_forgot = (TextView) findViewById(R.id.tv_forgot);

        activateET1=false;

        sessionPasscode = SessionSave.getUserSession(Comman.CREATED_PASSCODE, activity);
        Log.e("from","from 44444444444444444444: "+from);
        if (from!=null && !from.equalsIgnoreCase("") && from.equalsIgnoreCase("changePassword"))
        {
            tv_headerPassword.setText(getResources().getString(R.string.passcode_enter_old_pass));
            firstTimeCreate=3;
            tv_forgot.setVisibility(View.VISIBLE);
        }
        else {
            if (sessionPasscode!=null && !sessionPasscode.equalsIgnoreCase(""))
            {
                tv_headerPassword.setText(getResources().getString(R.string.passcode_required));
                firstTimeCreate=2;
                tv_forgot.setVisibility(View.VISIBLE);
            }
            else
            {
                firstTimeCreate=0;
                tv_forgot.setVisibility(View.GONE);
                tv_headerPassword.setText(getResources().getString(R.string.passcode_create));
            }
        }


        tv_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setCancelable(false);
                builder.setMessage("If you can't remember your passcode, you must sign out of TiCKTOC and login with your email address and password.");

                String positiveText = getString(android.R.string.ok);
                builder.setPositiveButton(positiveText,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                                if (driverId != null && !driverId.equalsIgnoreCase("")) {
                                    DriverLogOut(driverId, dialog);
                                }
                            }
                        });

                builder.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                final AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        ll_Erase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!activateET1)
                {
                   Log.e("jjjjjjjj","11111111111111111");
                }
                else if(!activateET2)
                {
                    Log.e("jjjjjjjj","2222222222222222222");
                    tv_1.setText("");
                    activateET1=false;
                }
                else if(!activateET3)
                {
                    Log.e("jjjjjjjj","3333333333333333");
                    tv_2.setText("");
                    activateET2=false;
                }
                else if(!activateET4)
                {
                    Log.e("jjjjjjjj","444444444444444444444");
                    tv_3.setText("");
                    activateET3=false;
                }
            }
        });
        tv_zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!activateET1)
                {
                    setZero(tv_1);
                    activateET1=true;
                }
                else if(!activateET2)
                {
                    setZero(tv_2);
                    activateET2=true;
                }
                else if(!activateET3)
                {
                    setZero(tv_3);
                    activateET3=true;
                }
                else if(!activateET4)
                {
                    setZero(tv_4);
                    activateET4=true;

                    activateET1=false;
                    activateET2=false;
                    activateET3=false;
                    activateET4=false;
                }
            }
        });

        tv_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!activateET1)
                {
                    setOne(tv_1);
                    activateET1=true;
                }
                else if(!activateET2)
                {
                    setOne(tv_2);
                    activateET2=true;
                }
                else if(!activateET3)
                {
                    setOne(tv_3);
                    activateET3=true;
                }
                else if(!activateET4)
                {
                    setOne(tv_4);
                    activateET4=true;

                    activateET1=false;
                    activateET2=false;
                    activateET3=false;
                    activateET4=false;
                }
            }
        });

        tv_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!activateET1)
                {
                    setTwo(tv_1);
                    activateET1=true;
                }
                else if(!activateET2)
                {
                    setTwo(tv_2);
                    activateET2=true;
                }
                else if(!activateET3)
                {
                    setTwo(tv_3);
                    activateET3=true;
                }
                else if(!activateET4)
                {
                    setTwo(tv_4);
                    activateET4=true;

                    activateET1=false;
                    activateET2=false;
                    activateET3=false;
                    activateET4=false;
                }

            }
        });

        tv_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!activateET1)
                {
                    setThree(tv_1);
                    activateET1=true;
                }
                else if(!activateET2)
                {
                    setThree(tv_2);
                    activateET2=true;
                }
                else if(!activateET3)
                {
                    setThree(tv_3);
                    activateET3=true;
                }
                else if(!activateET4)
                {
                    setThree(tv_4);
                    activateET4=true;

                    activateET1=false;
                    activateET2=false;
                    activateET3=false;
                    activateET4=false;
                }
            }
        });

        tv_four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!activateET1)
                {
                    setFour(tv_1);
                    activateET1=true;
                }
                else if(!activateET2)
                {
                    setFour(tv_2);
                    activateET2=true;
                }
                else if(!activateET3)
                {
                    setFour(tv_3);
                    activateET3=true;
                }
                else if(!activateET4)
                {
                    setFour(tv_4);
                    activateET4=true;

                    activateET1=false;
                    activateET2=false;
                    activateET3=false;
                    activateET4=false;
                }

            }
        });

        tv_five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!activateET1)
                {
                    setFive(tv_1);
                    activateET1=true;
                }
                else if(!activateET2)
                {
                    setFive(tv_2);
                    activateET2=true;
                }
                else if(!activateET3)
                {
                    setFive(tv_3);
                    activateET3=true;
                }
                else if(!activateET4)
                {
                    setFive(tv_4);
                    activateET4=true;

                    activateET1=false;
                    activateET2=false;
                    activateET3=false;
                    activateET4=false;
                }

            }
        });

        tv_six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!activateET1)
                {
                    setSix(tv_1);
                    activateET1=true;
                }
                else if(!activateET2)
                {
                    setSix(tv_2);
                    activateET2=true;
                }
                else if(!activateET3)
                {
                    setSix(tv_3);
                    activateET3=true;
                }
                else if(!activateET4)
                {
                    setSix(tv_4);
                    activateET4=true;

                    activateET1=false;
                    activateET2=false;
                    activateET3=false;
                    activateET4=false;
                }
            }
        });

        tv_seven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!activateET1)
                {
                    setSeven(tv_1);
                    activateET1=true;
                }
                else if(!activateET2)
                {
                    setSeven(tv_2);
                    activateET2=true;
                }
                else if(!activateET3)
                {
                    setSeven(tv_3);
                    activateET3=true;
                }
                else if(!activateET4)
                {
                    setSeven(tv_4);
                    activateET4=true;

                    activateET1=false;
                    activateET2=false;
                    activateET3=false;
                    activateET4=false;
                }
            }
        });

        tv_eight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!activateET1)
                {
                    setEight(tv_1);
                    activateET1=true;
                }
                else if(!activateET2)
                {
                    setEight(tv_2);
                    activateET2=true;
                }
                else if(!activateET3)
                {
                    setEight(tv_3);
                    activateET3=true;
                }
                else if(!activateET4)
                {
                    setEight(tv_4);
                    activateET4=true;

                    activateET1=false;
                    activateET2=false;
                    activateET3=false;
                    activateET4=false;
                }
            }
        });

        tv_nine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!activateET1)
                {
                    setNine(tv_1);
                    activateET1=true;
                }
                else if(!activateET2)
                {
                    setNine(tv_2);
                    activateET2=true;
                }
                else if(!activateET3)
                {
                    setNine(tv_3);
                    activateET3=true;
                }
                else if(!activateET4)
                {
                    setNine(tv_4);
                    activateET4=true;

                    activateET1=false;
                    activateET2=false;
                    activateET3=false;
                    activateET4=false;
                }
            }
        });

        tv_4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(tv_4.getText().length() == 1)
                {
                    tv_zero.setEnabled(false);
                    tv_one.setEnabled(false);
                    tv_two.setEnabled(false);
                    tv_three.setEnabled(false);
                    tv_four.setEnabled(false);
                    tv_five.setEnabled(false);
                    tv_six.setEnabled(false);
                    tv_seven.setEnabled(false);
                    tv_eight.setEnabled(false);
                    tv_nine.setEnabled(false);

                    handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            callView();
                        }
                    }, 500);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    private void callView()
    {
        Log.e("from","firstTimeCreate 44444444444444444444: "+firstTimeCreate);
        if (firstTimeCreate==0)
        {
            NfcFlag = 0;
            Log.e("vvvvvvvvvvvvvv","first Time Create");
            firstTimeCreate=1;
            firstPasscode = tv_1.getText().toString() + tv_2.getText().toString() + tv_3.getText().toString() + tv_4.getText().toString();
            tv_headerPassword.setText(getResources().getString(R.string.passcode_retype));
            tv_forgot.setVisibility(View.GONE);
            tv_1.setText("");
            tv_2.setText("");
            tv_3.setText("");
            tv_4.setText("");

            tv_zero.setEnabled(true);
            tv_one.setEnabled(true);
            tv_two.setEnabled(true);
            tv_three.setEnabled(true);
            tv_four.setEnabled(true);
            tv_five.setEnabled(true);
            tv_six.setEnabled(true);
            tv_seven.setEnabled(true);
            tv_eight.setEnabled(true);
            tv_nine.setEnabled(true);
        }
        else if (firstTimeCreate==1)
        {
            Log.e("vvvvvvvvvvvvvv","second Time Create");
            secondPasscode = tv_1.getText().toString() + tv_2.getText().toString() + tv_3.getText().toString() + tv_4.getText().toString();
            if (firstPasscode!=null && !firstPasscode.equalsIgnoreCase("") && firstPasscode.equalsIgnoreCase(secondPasscode))
            {
                NfcFlag = 1;
                Log.e("vvvvvvvvvvvvvv","passcode is matched");
                SessionSave.saveUserSession(Comman.CREATED_PASSCODE, firstPasscode, activity);
                SessionSave.saveUserSession(Comman.IS_PASSCODE_REQUIRED, "1",activity);
//                                    Drawer_Activity.passcodeScn_OntimeWatch=1;
                finish();
            }
            else
            {
                NfcFlag = 0;
                Log.e("vvvvvvvvvvvvvv","passcode is nottttttttt matched");
                Animation animation = AnimationUtils.loadAnimation(activity, R.anim.shake);
                shakeLayout.startAnimation(animation);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        tv_1.setText("");
                        tv_2.setText("");
                        tv_3.setText("");
                        tv_4.setText("");
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        tv_zero.setEnabled(true);
                        tv_one.setEnabled(true);
                        tv_two.setEnabled(true);
                        tv_three.setEnabled(true);
                        tv_four.setEnabled(true);
                        tv_five.setEnabled(true);
                        tv_six.setEnabled(true);
                        tv_seven.setEnabled(true);
                        tv_eight.setEnabled(true);
                        tv_nine.setEnabled(true);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        }
        else if (firstTimeCreate==2)
        {
            String passwordFromUser = SessionSave.getUserSession(Comman.CREATED_PASSCODE, activity);
            secondPasscode = tv_1.getText().toString() + tv_2.getText().toString() + tv_3.getText().toString() + tv_4.getText().toString();
            if (passwordFromUser!=null && !passwordFromUser.equalsIgnoreCase("") && passwordFromUser.equalsIgnoreCase(secondPasscode))
            {
                NfcFlag = 1;
//                                    Drawer_Activity.passcodeScn_OntimeWatch=1;
                SessionSave.saveUserSession(Comman.IS_PASSCODE_REQUIRED, "1",activity);
                finish();
            }
            else
            {
                NfcFlag = 0;
                Log.e("vvvvvvvvvvvvvv","passcode is nottttttttt matched");
                Animation animation = AnimationUtils.loadAnimation(activity, R.anim.shake);
                shakeLayout.startAnimation(animation);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        tv_1.setText("");
                        tv_2.setText("");
                        tv_3.setText("");
                        tv_4.setText("");
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        tv_zero.setEnabled(true);
                        tv_one.setEnabled(true);
                        tv_two.setEnabled(true);
                        tv_three.setEnabled(true);
                        tv_four.setEnabled(true);
                        tv_five.setEnabled(true);
                        tv_six.setEnabled(true);
                        tv_seven.setEnabled(true);
                        tv_eight.setEnabled(true);
                        tv_nine.setEnabled(true);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        }
        else if (firstTimeCreate==3)
        {
            String passwordFromUser = SessionSave.getUserSession(Comman.CREATED_PASSCODE, activity);
            secondPasscode = tv_1.getText().toString() + tv_2.getText().toString() + tv_3.getText().toString() + tv_4.getText().toString();
            if (passwordFromUser!=null && !passwordFromUser.equalsIgnoreCase("") && passwordFromUser.equalsIgnoreCase(secondPasscode))
            {
                firstTimeCreate=0;

                tv_1.setText("");
                tv_2.setText("");
                tv_3.setText("");
                tv_4.setText("");

                tv_zero.setEnabled(true);
                tv_one.setEnabled(true);
                tv_two.setEnabled(true);
                tv_three.setEnabled(true);
                tv_four.setEnabled(true);
                tv_five.setEnabled(true);
                tv_six.setEnabled(true);
                tv_seven.setEnabled(true);
                tv_eight.setEnabled(true);
                tv_nine.setEnabled(true);

                tv_forgot.setVisibility(View.GONE);
                tv_headerPassword.setText(getResources().getString(R.string.passcode_create));

            }
            else
            {
                NfcFlag = 0;
                Log.e("vvvvvvvvvvvvvv","passcode is nottttttttt matched");
                Animation animation = AnimationUtils.loadAnimation(activity, R.anim.shake);
                shakeLayout.startAnimation(animation);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        tv_1.setText("");
                        tv_2.setText("");
                        tv_3.setText("");
                        tv_4.setText("");
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        tv_zero.setEnabled(true);
                        tv_one.setEnabled(true);
                        tv_two.setEnabled(true);
                        tv_three.setEnabled(true);
                        tv_four.setEnabled(true);
                        tv_five.setEnabled(true);
                        tv_six.setEnabled(true);
                        tv_seven.setEnabled(true);
                        tv_eight.setEnabled(true);
                        tv_nine.setEnabled(true);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        }
    }

    private void setZero(TextView editText)
    {
        editText.setText("0");
    }
    private void setOne(TextView editText)
    {
        editText.setText("1");
    }
    private void setTwo(TextView editText)
    {
        editText.setText("2");
    }
    private void setThree(TextView editText)
    {
        editText.setText("3");
    }
    private void setFour(TextView editText)
    {
        editText.setText("4");
    }
    private void setFive(TextView editText)
    {
        editText.setText("5");
    }
    private void setSix(TextView editText)
    {
        editText.setText("6");
    }
    private void setSeven(TextView editText)
    {
        editText.setText("7");
    }
    private void setEight(TextView editText)
    {
        editText.setText("8");
    }
    private void setNine(TextView editText)
    {
        editText.setText("9");
    }


    private void DriverLogOut(String driverId, final DialogInterface dialog)
    {
        dialogClass.showDialog();

        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_DRIVER_LOGOUT + driverId;

        Log.e("DriverLogOut", "URL = " + url);
        Log.e("DriverLogOut", "PARAMS = " + params);

        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("DriverLogOut", "responseCode = " + responseCode);
                    Log.e("DriverLogOut", "Response = " + json);

                    if (json != null) {
                        if (json.has("status")) {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");
                                dialogClass.hideDialog();
                                dialog.dismiss();
                                SessionSave.clearUserSession(activity);
                                Intent intent = new Intent(getApplicationContext(), Main_Activity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            } else {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        } else {
                            Log.e("status", "no status");
                            dialogClass.hideDialog();
                            if (json.has("message")) {
                                new SnackbarUtils(main_layout, json.getString("message"),
                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }
                    } else {
                        Log.e("DriverLogOut", "getMessage = " + "null");
                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout, getResources().getString(R.string.something_is_wrong),
                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                    }
                } catch (Exception e) {
                    Log.e("DriverLogOut", "Exception = " + e.getMessage() + "something_is_wrong");
                    dialogClass.hideDialog();
                }
            }
        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Drawer_Activity.PasscodeBackPress = 1;
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }
}