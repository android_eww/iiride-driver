package iiride.app.driver.Activity;

import android.os.Bundle;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import iiride.app.driver.Application.TiCKTOC_Driver_Application;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;
import iiride.app.driver.Others.ConnectivityReceiver;


import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Change_password_Activity extends AppCompatActivity implements View.OnClickListener{

    Change_password_Activity activity;
    String TAG = "changeDriverPassword";
    LinearLayout main_layout, ll_back;
    EditText et_new_password;
    TextView tv_submit_password;

    private AQuery aQuery;
    DialogClass dialogClass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        activity = Change_password_Activity.this;

        aQuery = new AQuery(activity);

        main_layout = (LinearLayout)findViewById(R.id.main_layout);
        ll_back = (LinearLayout)findViewById(R.id.ll_back);

        et_new_password = (EditText)findViewById(R.id.et_new_password);

        tv_submit_password = (TextView)findViewById(R.id.tv_submit_password);

        tv_submit_password.setOnClickListener(this);
        ll_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tv_submit_password:
                checkData();
                break;

            case R.id.ll_back:
                onBackPressed();
                break;
        }
    }

    private void checkData() {

        if (TextUtils.isEmpty(et_new_password.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_mobile_number),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else
        {
            String userId = SessionSave.getUserSession(Comman.USER_ID, activity);
            if (userId!=null && !userId.equalsIgnoreCase(""))
            {
                if (ConnectivityReceiver.isConnected())
                {
                    changeDriverPassword(userId);
                }
                else
                {
                    new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }
    }

    private void changeDriverPassword(String userId)
    {
        dialogClass = new DialogClass(activity, 1);
        dialogClass.showDialog();

        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_CHANGE_PASSWORD;

        params.put(WebServiceAPI.CHANGE_PASSWORD_PARAM_DRIVER_ID,userId);
        params.put(WebServiceAPI.CHANGE_PASSWORD_PARAM_PASSWORD,et_new_password.getText().toString());

        Log.e(TAG, "URL = " + url);
        Log.e(TAG, "PARAMS = " + params);

        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>(){

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {

                    int responseCode = status.getCode();
                    Log.e(TAG, "responseCode = " + responseCode);
                    Log.e(TAG, "Response = " + json);

                    if (json != null)
                    {
                        if (json.has("message"))
                        {
                            dialogClass.hideDialog();
                            String message = json.getString("message");
                            new SnackbarUtils(main_layout, message,
                                    ContextCompat.getColor(activity, R.color.snakbar_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                            et_new_password.setText("");

                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    onBackPressed();
                                }
                            }, 2000);
                        }
                        else
                        {
                            dialogClass.hideDialog();
                            new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                    ContextCompat.getColor(activity, R.color.snakbar_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                        }
                    }
                    else
                    {
                        Log.e(TAG, "getMessage = " + "null");
                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                    }
                }catch (Exception e){
                    Log.e(TAG, "Exception = " + e.getMessage() + "something_is_wrong");
                    dialogClass.hideDialog();
                    new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }


            }
        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Drawer_Activity.flagResumePass=0;
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            TiCKTOC_Driver_Application.setCurrentActivity(activity);
        }
    }
}

