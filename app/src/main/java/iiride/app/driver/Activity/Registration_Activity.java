package iiride.app.driver.Activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import iiride.app.driver.Application.TiCKTOC_Driver_Application;
import iiride.app.driver.R;


/**
 * Created by EXCELLENT-14 on 09-Oct-17.
 */

public class Registration_Activity extends AppCompatActivity implements View.OnClickListener{

    public static Registration_Activity activity;

    TextView tv_diver, tv_service_provider;

    ImageView iv_back;

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        activity = Registration_Activity.this;

        init();
    }

    private void init() {

        tv_diver = (TextView) findViewById(R.id.tv_diver);
        tv_service_provider = (TextView) findViewById(R.id.tv_service_provider);
        iv_back = (ImageView) findViewById(R.id.iv_back);


        tv_diver.setOnClickListener(this);
        tv_service_provider.setOnClickListener(this);
        iv_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tv_diver:
                intent = new Intent(activity, Registration_Tab_Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                break;

            case R.id.tv_service_provider:
                intent = new Intent(activity, Service_Provider_Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                break;

            case R.id.iv_back:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            TiCKTOC_Driver_Application.setCurrentActivity(activity);
        }
    }
}
