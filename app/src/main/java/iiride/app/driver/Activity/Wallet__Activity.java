package iiride.app.driver.Activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import iiride.app.driver.Application.TiCKTOC_Driver_Application;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;
import iiride.app.driver.Fragment.BpayFragment;
import iiride.app.driver.Fragment.EntertianmentFragment;
import iiride.app.driver.Fragment.TravelFragment;
import iiride.app.driver.Others.ConnectivityReceiver;


import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Wallet__Activity extends AppCompatActivity implements View.OnClickListener
{
    public static Wallet__Activity activity;

    private LinearLayout  ll_transfer, ll_balance, ll_cards;
    private LinearLayout ll_Back;
    private ImageView iv_Back;
    private TextView tv_Title;
    private Intent intent;
    String walleteBallence = "0";

    private ImageView imageBack, iv_bpay, iv_travel, iv_entertainment;
    private TextView tv_bpay, tv_travel, tv_entertainment, tv_WalletBallence;

    private LinearLayout ll_bpay, ll_travel, ll_entertainment;
    private FrameLayout fl_frame;
    private Fragment bpayFragment, travelFragmetn, entertainmentFregment;
    private int tabPrivPossition = 0,tabCurrPosition = 0;
    private SharedPreferences permissionStatus;
    private boolean sentToSettings = false;
    private static final int PERMISSION_REQUEST_CODE = 1;

    RelativeLayout main_layout;

    private AQuery aQuery;
    private DialogClass dialogClass;
    public static int flagResumePass=0;
    int resumeFlag=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);

        activity = Wallet__Activity.this;

        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 1);

        flagResumePass=0;
        resumeFlag=1;

        main_layout = (RelativeLayout) findViewById(R.id.main_layout);

        walleteBallence = SessionSave.getUserSession(Comman.DRIVER_WALLET_BALLENCE,activity);
        permissionStatus = getSharedPreferences("permissionStatus",MODE_PRIVATE);
        takePermision(0);
        initUI();
    }

    public void takePermision(int flag)
    {
        if (ActivityCompat.checkSelfPermission(Wallet__Activity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
            Log.e("call"," permision 1111111111");
            if (ActivityCompat.shouldShowRequestPermissionRationale(Wallet__Activity.this, Manifest.permission.CAMERA))
            {
                Log.e("call"," permision 222222222222");
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(Wallet__Activity.this);
                builder.setTitle(getResources().getString(R.string.need_camera_permission));
                builder.setMessage(getResources().getString(R.string.this_app_need_camera_permission));
                builder.setPositiveButton(getResources().getString(R.string.grant), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call"," permision 33333333");
                        ActivityCompat.requestPermissions(Wallet__Activity.this, new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE);
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Log.e("call"," permision 44444444444");
                    }
                });
                builder.show();
            }
            else
            {
                Log.e("call"," permision 5555555555");
                //just request the permission
                ActivityCompat.requestPermissions(Wallet__Activity.this, new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE);
            }
            Log.e("call"," permision 6666666666666");
            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.CAMERA,true);
            editor.commit();
        }
        else
        {
            if (flag==1)
            {
                flagResumePass=0;
                Intent intent = new Intent(Wallet__Activity.this, Wallet_Transfer_Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        Log.e("call","onRequestPermissionResult call");
        switch (requestCode)
        {
            case PERMISSION_REQUEST_CODE:
                Log.e("call","onRequestPermissionResult call111");
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    Log.e("call","onRequestPermissionResult call 222");
                }
                else
                {
                    Log.e("call","onRequestPermissionsResult permission not granted");
                }
                break;
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Log.e("call","onRequestPermissionResult call 333");
        if (sentToSettings)
        {
            if (ActivityCompat.checkSelfPermission(Wallet__Activity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

            }
        }
    }

    private void initUI() {
        ll_Back = (LinearLayout) findViewById(R.id.ll_back);
        iv_Back = (ImageView) findViewById(R.id.iv_back);

        tv_WalletBallence = (TextView) findViewById(R.id.tv_WalletBallence);

        fl_frame = (FrameLayout) findViewById(R.id.fl_frame);

        ll_bpay = (LinearLayout) findViewById(R.id.ll_bpay);
        ll_travel = (LinearLayout) findViewById(R.id.ll_travel);
        ll_entertainment = (LinearLayout) findViewById(R.id.ll_entertainment);

        iv_bpay = (ImageView) findViewById(R.id.iv_bpay);
        iv_travel = (ImageView) findViewById(R.id.iv_travel);
        iv_entertainment = (ImageView) findViewById(R.id.iv_entertainment);

        tv_bpay = (TextView) findViewById(R.id.tv_bpay);
        tv_travel = (TextView) findViewById(R.id.tv_travel);
        tv_entertainment = (TextView) findViewById(R.id.tv_entertainment);

        ll_transfer = (LinearLayout) findViewById(R.id.ll_transfer);
        ll_balance = (LinearLayout) findViewById(R.id.ll_balance);
        ll_cards = (LinearLayout) findViewById(R.id.ll_cards);

        ll_Back.setOnClickListener(activity);
        iv_Back.setOnClickListener(activity);

        ll_transfer.setOnClickListener(activity);
        ll_balance.setOnClickListener(activity);
        ll_cards.setOnClickListener(activity);
        ll_bpay.setOnClickListener(activity);
        ll_travel.setOnClickListener(activity);
        ll_entertainment.setOnClickListener(activity);

        String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
        if (driverId!=null && !driverId.equalsIgnoreCase(""))
        {
            if (ConnectivityReceiver.isConnected())
            {
                GetHistory(driverId);
            }
            else
            {
                new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                        ContextCompat.getColor(activity, R.color.snakbar_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.ll_balance:
                flagResumePass=0;
                resumeFlag=0;
                intent = new Intent(Wallet__Activity.this, Wallet_Balance_Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                break;

            case R.id.ll_transfer:
                takePermision(1);
                resumeFlag=0;
                break;

            case R.id.ll_cards:
//                if (MainActivity.CardFlag==0)
//                {
//                    intent = new Intent(Wallet__Activity.this, Add_Card_In_List_Activity.class);
//                    startActivity(intent);
//                    overridePendingTransition(R.anim.right_in, R.anim.left_out);
//                }
//                else
//                {
                flagResumePass=0;
                resumeFlag=0;
                    intent = new Intent(Wallet__Activity.this, Wallet_Cards_Activity.class);
                    intent.putExtra("from","");
                    startActivity(intent);
                    overridePendingTransition(R.anim.right_in, R.anim.left_out);
//                }
                break;

            case R.id.ll_bpay:
                tabPrivPossition = tabCurrPosition;
                tabCurrPosition = 0;
                bpayFragment();
                break;

            case R.id.ll_travel:
                tabPrivPossition = tabCurrPosition;
                tabCurrPosition = 1;
                travelFragmetn();
                break;

            case R.id.ll_entertainment:
                tabPrivPossition = tabCurrPosition;
                tabCurrPosition = 2;
                entertainmentFregment();
                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Drawer_Activity.flagResumePass=0;
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    private void bpayFragment()
    {
        btnClick(1);

        if (tabCurrPosition == tabPrivPossition)
        {

            bpayFragment = new BpayFragment();
            FragmentTransaction ftBpay = getSupportFragmentManager().beginTransaction();
            ftBpay.replace(R.id.fl_frame,bpayFragment);
            ftBpay.commit();
        }
        if (tabCurrPosition < tabPrivPossition)
        {
            bpayFragment = new BpayFragment();
            FragmentTransaction ftBpay = getSupportFragmentManager().beginTransaction();
            ftBpay.setCustomAnimations(R.anim.left_in,R.anim.right_out);
            ftBpay.replace(R.id.fl_frame,bpayFragment);
            ftBpay.commit();
        }
        if (tabCurrPosition > tabPrivPossition)
        {
            bpayFragment = new BpayFragment();
            FragmentTransaction ftBpay = getSupportFragmentManager().beginTransaction();
            ftBpay.setCustomAnimations(R.anim.left_in,R.anim.right_out);
            ftBpay.replace(R.id.fl_frame,bpayFragment);
            ftBpay.commit();
        }
    }

    private void GetHistory(String driverId)
    {
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_TRANSACTION_HISTORY + driverId;

        Log.e("url", "GetCardList = " + url);
        Log.e("param", "GetCardList = " + params);


        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "GetCardList = " + responseCode);
                    Log.e("Response", "GetCardList = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");
                                if (json.has("walletBalance"))
                                {
                                    String walletBalance = json.getString("walletBalance");
                                    if (walletBalance!=null && !walletBalance.equalsIgnoreCase(""))
                                    {
                                        SessionSave.saveUserSession(Comman.DRIVER_WALLET_BALLENCE, walletBalance, activity);
                                        walleteBallence = SessionSave.getUserSession(Comman.DRIVER_WALLET_BALLENCE,activity);
                                        if (walleteBallence!=null && !walleteBallence.equalsIgnoreCase(""))
                                        {
                                            String mm = "$"+String.format("%.2f",Double.parseDouble(walleteBallence));
                                            tv_WalletBallence.setText(mm);
                                        }
                                        else
                                        {
                                            tv_WalletBallence.setVisibility(View.GONE);
                                        }
                                    }
                                    bpayFragment();
                                }
                                else
                                {
                                    tv_WalletBallence.setVisibility(View.GONE);
                                    Log.e("history", "no history");
                                    dialogClass.hideDialog();
                                    if (json.has("message")) {
                                        new SnackbarUtils(main_layout, json.getString("message"),
                                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                    }
                                    bpayFragment();
                                }
                                dialogClass.hideDialog();
                            }
                            else
                            {
                                tv_WalletBallence.setVisibility(View.GONE);
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                }
                                bpayFragment();
                            }
                        }
                        else
                        {
                            tv_WalletBallence.setVisibility(View.GONE);
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message")) {
                                new SnackbarUtils(main_layout, json.getString("message"),
                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                            }
                            bpayFragment();
                        }
                    }
                    else
                    {
                        tv_WalletBallence.setVisibility(View.GONE);
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                        bpayFragment();
                    }
                }
                catch (Exception e)
                {
                    tv_WalletBallence.setVisibility(View.GONE);
                    Log.e("GetCardList", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                    bpayFragment();
                }
            }
        }.header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }


    private void travelFragmetn()
    {
        btnClick(2);

        if (tabCurrPosition == tabPrivPossition)
        {
            travelFragmetn = new TravelFragment();
            FragmentTransaction ftTravel = getSupportFragmentManager().beginTransaction();
            ftTravel.replace(R.id.fl_frame,travelFragmetn);
            ftTravel.commit();
        }
        if (tabCurrPosition < tabPrivPossition)
        {

            travelFragmetn = new TravelFragment();
            FragmentTransaction ftTravel = getSupportFragmentManager().beginTransaction();
            ftTravel.setCustomAnimations(R.anim.left_in,R.anim.right_out);
            ftTravel.replace(R.id.fl_frame,travelFragmetn);
            ftTravel.commit();
        }
        if (tabCurrPosition > tabPrivPossition)
        {

            travelFragmetn = new TravelFragment();
            FragmentTransaction ftTravel = getSupportFragmentManager().beginTransaction();
            ftTravel.setCustomAnimations(R.anim.right_in,R.anim.left_out);
            ftTravel.replace(R.id.fl_frame,travelFragmetn);
            ftTravel.commit();
        }
    }

    private void entertainmentFregment()
    {
        btnClick(3);

        if (tabCurrPosition == tabPrivPossition)
        {
            entertainmentFregment = new EntertianmentFragment();
            FragmentTransaction ftEntertainment = getSupportFragmentManager().beginTransaction();
            ftEntertainment.replace(R.id.fl_frame,entertainmentFregment);
            ftEntertainment.commit();
        }
        if (tabCurrPosition < tabPrivPossition)
        {
            entertainmentFregment = new EntertianmentFragment();
            FragmentTransaction ftEntertainment = getSupportFragmentManager().beginTransaction();
            ftEntertainment.setCustomAnimations(R.anim.left_in,R.anim.right_out);
            ftEntertainment.replace(R.id.fl_frame,entertainmentFregment);
            ftEntertainment.commit();
        }
        if (tabCurrPosition > tabPrivPossition)
        {
            entertainmentFregment = new EntertianmentFragment();
            FragmentTransaction ftEntertainment = getSupportFragmentManager().beginTransaction();
            ftEntertainment.setCustomAnimations(R.anim.right_in,R.anim.left_out);
            ftEntertainment.replace(R.id.fl_frame,entertainmentFregment);
            ftEntertainment.commit();
        }
    }

    private void btnClick(int i)
    {

        if (i == 1)
        {
            iv_bpay.setBackgroundResource(R.drawable.ic_dollar_select);
            iv_travel.setBackgroundResource(R.drawable.ic_travel_bag_unselect);
            iv_entertainment.setBackgroundResource(R.drawable.ic_entertainment_unselect);
            tv_bpay.setTextColor(getResources().getColor(R.color.colorSelectText));
            tv_travel.setTextColor(getResources().getColor(R.color.colorUnselectText));
            tv_entertainment.setTextColor(getResources().getColor(R.color.colorUnselectText));

        }
        else if(i == 2)
        {
            iv_bpay.setBackgroundResource(R.drawable.ic_dollar_unselect);
            iv_travel.setBackgroundResource(R.drawable.ic_travel_bag_select);
            iv_entertainment.setBackgroundResource(R.drawable.ic_entertainment_unselect);
            tv_bpay.setTextColor(getResources().getColor(R.color.colorUnselectText));
            tv_travel.setTextColor(getResources().getColor(R.color.colorSelectText));
            tv_entertainment.setTextColor(getResources().getColor(R.color.colorUnselectText));

        }
        else
        {
            iv_bpay.setBackgroundResource(R.drawable.ic_dollar_unselect);
            iv_travel.setBackgroundResource(R.drawable.ic_travel_bag_unselect);
            iv_entertainment.setBackgroundResource(R.drawable.ic_entertainment_select);
            tv_bpay.setTextColor(getResources().getColor(R.color.colorUnselectText));
            tv_travel.setTextColor(getResources().getColor(R.color.colorUnselectText));
            tv_entertainment.setTextColor(getResources().getColor(R.color.colorSelectText));
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            TiCKTOC_Driver_Application.setCurrentActivity(activity);
        }

        if (Drawer_Activity.PasscodeBackPress==1)
        {
            onBackPressed();
        }

        walleteBallence = SessionSave.getUserSession(Comman.DRIVER_WALLET_BALLENCE,activity);
        if (walleteBallence!=null && !walleteBallence.equalsIgnoreCase(""))
        {
            String mm = "$"+String.format("%.2f",Double.parseDouble(walleteBallence));
            tv_WalletBallence.setText(mm);
        }
        else
        {
            tv_WalletBallence.setVisibility(View.GONE);
        }

      /*  if (flagResumePass==1)
        {
            if (SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity) != null && !SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("")
                    && SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
            {
                Intent intentv = new Intent(activity, Create_Passcode_Activity.class);
                startActivity(intentv);
            }
        }
        */
        flagResumePass=0;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (resumeFlag==1)
        {
            if (SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity) != null && !SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("")
                    && SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
            {
                resumeFlag = 0;
                Intent intent = new Intent(activity,Create_Passcode_Activity.class);
                startActivity(intent);
            }
        }
        else
        {
            resumeFlag=1;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        flagResumePass=1;
    }
}
