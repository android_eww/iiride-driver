package iiride.app.driver.Activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import iiride.app.driver.Application.TiCKTOC_Driver_Application;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;
import iiride.app.driver.Fragment.Wallet_Transfer_ReceiveMoney_Fragment;
import iiride.app.driver.Fragment.Wallet_Transfer_SendMoney_Fragment;
import iiride.app.driver.Others.ConnectivityReceiver;


import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.MyAlertDialog;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Wallet_Transfer_Activity extends AppCompatActivity implements View.OnClickListener
{
    String TAG = "walletTransferActivity";

    public static Wallet_Transfer_Activity activity;
    public Wallet_Transfer_SendMoney_Fragment fragment;

    public static LinearLayout main_layout;
    private LinearLayout ll_back,ll_send_money;
    ImageView iv_back;
    RelativeLayout rl_send_money, rl_receive_money, rl_history;

    FrameLayout frame_transfer;
    String clickName = "";
    String CLICK_SEND_MONEY="send_money", CLICK_RECEIVE_MONEY="receive_money";

    private TextView tv_dollar,tv_send_money;
    private EditText et_send_money;

    public static String getQrCodeResult = "";

    private AQuery aQuery;
    private DialogClass dialogClass;
    private MyAlertDialog myAlertDialog;
    int resumeFlag=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_transfer);


        activity = Wallet_Transfer_Activity.this;

        aQuery = new AQuery(activity);
        fragment = new Wallet_Transfer_SendMoney_Fragment();
        dialogClass = new DialogClass(activity, 1);

        resumeFlag=1;

        myAlertDialog = new MyAlertDialog(activity);
        initUI();
    }

    private void initUI()
    {
        main_layout = (LinearLayout) findViewById(R.id.main_layout);

        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        ll_send_money = (LinearLayout) findViewById(R.id.ll_send_money);

        rl_send_money = (RelativeLayout) findViewById(R.id.rl_send_money);
        rl_receive_money = (RelativeLayout) findViewById(R.id.rl_receive_money);
        rl_history = (RelativeLayout) findViewById(R.id.rl_history);

        iv_back = (ImageView) findViewById(R.id.iv_back);

        frame_transfer = (FrameLayout) findViewById(R.id.frame_transfer);

        tv_dollar = (TextView)findViewById(R.id.tv_dollar);
        tv_send_money = (TextView)findViewById(R.id.tv_send_money);
        et_send_money = (EditText)findViewById(R.id.et_send_money);

        ll_back.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        tv_send_money.setOnClickListener(this);

        rl_send_money.setOnClickListener(this);
        rl_receive_money.setOnClickListener(this);
        rl_history.setOnClickListener(this);

        et_send_money.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                if(charSequence.length()>0)
                {
                    if (charSequence.toString().trim().length()==1 && charSequence.toString().equalsIgnoreCase("."))
                    {
                        et_send_money.setText("");
                        tv_dollar.setVisibility(View.GONE);
                    }
                    else
                    {
                        tv_dollar.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    tv_dollar.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        setSendMoney();
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.rl_send_money:
                setSendMoney();
                break;

            case R.id.rl_receive_money:
                setReceiveMoney();
                break;

            case R.id.rl_history:
                setHistory();
                break;

            case R.id.tv_send_money:
                checkData();
                break;
        }
    }

    private void checkData() {

        if (et_send_money == null || TextUtils.isEmpty(et_send_money.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_send_amount),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else if (getQrCodeResult == null || getQrCodeResult.equalsIgnoreCase(""))
        {
            new SnackbarUtils(main_layout, getString(R.string.scan_bar_code_error_message),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else
        {
            String userId = SessionSave.getUserSession(Comman.USER_ID, activity);
            if (userId!=null && !userId.equalsIgnoreCase(""))
            {
                if (ConnectivityReceiver.isConnected())
                {
                    sendMoney();
                }
                else
                {
                    new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }

            }
        }
    }

    public void sendMoney()
    {
        dialogClass.showDialog();

        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_SEND_MONEY;

        params.put(WebServiceAPI.SEND_MONEY_PARAM_QRCODE,getQrCodeResult);
        params.put(WebServiceAPI.SEND_MONEY_PARAM_SENDER_ID,SessionSave.getUserSession(Comman.USER_ID, activity));
        params.put(WebServiceAPI.SEND_MONEY_PARAM_AMOUNT,et_send_money.getText().toString());

        Log.e(TAG, "URL = " + url);
        Log.e(TAG, "PARAMS = " + params);

        aQuery.ajax(url, params , JSONObject.class , new AjaxCallback<JSONObject>()
        {
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status)
            {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG, "responseCode = " + responseCode);
                    Log.e(TAG, "Response = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if(json.getBoolean("status"))
                            {
                                Log.e(TAG,"Status = " + "true");
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    String message = json.getString("message");
                                    myAlertDialog.setCancelable(false);
                                    myAlertDialog.setAlertDialog(1,message);
                                    refreshLayout();

                                }
                                if (json.has("walletBalance"))
                                {
                                    String walletBalance = json.getString("walletBalance");
                                    SessionSave.saveUserSession(Comman.DRIVER_WALLET_BALLENCE,walletBalance,activity);

                                }

                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(Wallet_Transfer_Activity.main_layout, json.getString("message"),
                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }

                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message")) {
                                new SnackbarUtils(Wallet_Transfer_Activity.main_layout, json.getString("message"),
                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                            }

                        }

                    }
                    else
                    {
                        Log.e(TAG, "getMessage = "  + "Null");
                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout,getString(R.string.something_is_wrong),
                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                    }

                }
                catch (Exception e)
                {
                    Log.e(TAG, "Exception = " + e.getMessage() + "somthing_is_wrong");
                    dialogClass.hideDialog();
                    new SnackbarUtils(main_layout,getString(R.string.something_is_wrong),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();

                }

            }
        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY_NAME,WebServiceAPI.HEADER_KEY_VALUE));



    }

    private void refreshLayout() {

        fragment.refreshScanLayout();
        tv_dollar.setVisibility(View.GONE);
        et_send_money.setText("");
        dialogClass.hideDialog();
    }


    public void setSendMoney()
    {
        if (!clickName.equalsIgnoreCase(CLICK_SEND_MONEY))
        {
            ll_send_money.setVisibility(View.VISIBLE);

            Wallet_Transfer_SendMoney_Fragment fragment = new Wallet_Transfer_SendMoney_Fragment();
            FragmentTransaction transaction =getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.right_in, R.anim.left_out);
            transaction.replace(R.id.frame_transfer, fragment);
            transaction.addToBackStack(null);
            transaction.commitAllowingStateLoss();
        }
        clickName = CLICK_SEND_MONEY;
    }


    public void setReceiveMoney()
    {
        if (!clickName.equalsIgnoreCase(CLICK_RECEIVE_MONEY))
        {

            ll_send_money.setVisibility(View.GONE);

            Wallet_Transfer_ReceiveMoney_Fragment fragment = new Wallet_Transfer_ReceiveMoney_Fragment();
            FragmentTransaction transaction =getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.right_in, R.anim.left_out);
            transaction.replace(R.id.frame_transfer, fragment);
            transaction.addToBackStack(null);
            transaction.commitAllowingStateLoss();

        }
        clickName = CLICK_RECEIVE_MONEY;

    }

    public void setHistory()
    {
        resumeFlag=0;
        Intent intent = new Intent(activity, Wallet_Balance_TransferToBank_Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }


    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            TiCKTOC_Driver_Application.setCurrentActivity(activity);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (resumeFlag==1)
        {
            if (SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity) != null && !SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("")
                    && SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
            {
                resumeFlag = 0;
                Intent intent = new Intent(activity,Create_Passcode_Activity.class);
                startActivity(intent);
                if (Wallet_Balance_Activity.activity!=null)
                {
                    Wallet_Balance_Activity.activity.finish();
                }
                if (Wallet_Balance_TopUp_Activity.activity!=null)
                {
                    Wallet_Balance_TopUp_Activity.activity.finish();
                }
                if (Wallet_Balance_TransferToBank_Activity.activity!=null)
                {
                    Wallet_Balance_TransferToBank_Activity.activity.finish();
                }
                if (Wallet_Cards_Activity.activity!=null)
                {
                    Wallet_Cards_Activity.activity.finish();
                }
                if (Wallet_Transfer_History_Activity.activity!=null)
                {
                    Wallet_Transfer_History_Activity.activity.finish();
                }
                finish();
            }
        }
        else
        {
            resumeFlag=1;
        }
    }
}