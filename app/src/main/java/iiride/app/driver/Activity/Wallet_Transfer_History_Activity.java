package iiride.app.driver.Activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import iiride.app.driver.Adapter.Transfer_History_Adapter;
import iiride.app.driver.Application.TiCKTOC_Driver_Application;
import iiride.app.driver.Been.Transfer_History_Been;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;
import iiride.app.driver.Others.ConnectivityReceiver;


import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Wallet_Transfer_History_Activity extends AppCompatActivity implements View.OnClickListener
{
    public static Wallet_Transfer_History_Activity activity;

    LinearLayout ll_back, main_layout;
    ImageView iv_back;

    RecyclerView rv_transferHistory;
    private List<Transfer_History_Been> list = new ArrayList<>();
    Transfer_History_Adapter adapter;

    private AQuery aQuery;
    DialogClass dialogClass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_transfer_history);

        activity = Wallet_Transfer_History_Activity.this;
        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 1);

        initUI();
    }

    private void initUI()
    {
        main_layout = (LinearLayout) findViewById(R.id.main_layout);
        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        iv_back = (ImageView) findViewById(R.id.iv_back);

        rv_transferHistory = (RecyclerView) findViewById(R.id.rv_transferHistory);
        rv_transferHistory.setVisibility(View.GONE);

        adapter = new Transfer_History_Adapter(activity, list);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        rv_transferHistory.setLayoutManager(mLayoutManager);
        rv_transferHistory.setItemAnimator(new DefaultItemAnimator());
        rv_transferHistory.setAdapter(adapter);

        ll_back.setOnClickListener(this);
        iv_back.setOnClickListener(this);

        String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
        if (driverId!=null && !driverId.equalsIgnoreCase(""))
        {
            if (ConnectivityReceiver.isConnected())
            {
                GetHistory(driverId);
            }
            else
            {
                new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                        ContextCompat.getColor(activity, R.color.snakbar_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
            }
        }
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.iv_back:
                onBackPressed();
                break;
        }
    }

    private void GetHistory(String driverId)
    {
        list.clear();
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_TRANSACTION_HISTORY + driverId;

        Log.e("url", "GetCardList = " + url);
        Log.e("param", "GetCardList = " + params);


        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "GetCardList = " + responseCode);
                    Log.e("Response", "GetCardList = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");
                                if (json.has("walletBalance"))
                                {
                                    String walletBalance = json.getString("walletBalance");
                                    if (walletBalance!=null && !walletBalance.equalsIgnoreCase(""))
                                    {
                                        SessionSave.saveUserSession(Comman.DRIVER_WALLET_BALLENCE, walletBalance, activity);
                                    }
                                }
                                if (json.has("history"))
                                {
                                    String history = json.getString("history");
                                    if (history!=null && !history.equalsIgnoreCase(""))
                                    {
                                        JSONArray historyArray = json.getJSONArray("history");
                                        if (historyArray!=null && historyArray.length()>0)
                                        {
                                            for (int i=0; i<historyArray.length(); i++)
                                            {
                                                String WalletId="", UpdatedDate="", Amount="", Type="", Description="", Status="";
                                                JSONObject historyObj = historyArray.getJSONObject(i);
                                                if (historyObj.has("WalletId"))
                                                {
                                                    WalletId = historyObj.getString("WalletId");
                                                }
                                                if (historyObj.has("UpdatedDate"))
                                                {
                                                    UpdatedDate = historyObj.getString("UpdatedDate");
                                                }
                                                if (historyObj.has("Amount"))
                                                {
                                                    Amount = historyObj.getString("Amount");
                                                }
                                                if (historyObj.has("Type"))
                                                {
                                                    Type = historyObj.getString("Type");
                                                }
                                                if (historyObj.has("Description"))
                                                {
                                                    Description = historyObj.getString("Description");
                                                }
                                                if (historyObj.has("Status"))
                                                {
                                                    Status = historyObj.getString("Status");
                                                }
                                                list.add(new Transfer_History_Been(Description , UpdatedDate,Type+" "+Amount, Status));
                                            }
                                            dialogClass.hideDialog();
                                            adapter.notifyDataSetChanged();
                                            rv_transferHistory.setVisibility(View.VISIBLE);
                                        }
                                        else
                                        {
                                            Log.e("historyArray", "null");
                                            dialogClass.hideDialog();
                                            rv_transferHistory.setVisibility(View.GONE);
                                            if (json.has("message")) {
                                                new SnackbarUtils(main_layout, json.getString("message"),
                                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Log.e("history", "nulll");
                                        dialogClass.hideDialog();
                                        rv_transferHistory.setVisibility(View.GONE);
                                        if (json.has("message")) {
                                            new SnackbarUtils(main_layout, json.getString("message"),
                                                    ContextCompat.getColor(activity, R.color.snakbar_color),
                                                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                        }
                                    }
                                }
                                else
                                {
                                    Log.e("history", "no history");
                                    dialogClass.hideDialog();
                                    rv_transferHistory.setVisibility(View.GONE);
                                    if (json.has("message")) {
                                        new SnackbarUtils(main_layout, json.getString("message"),
                                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                    }
                                }
                                dialogClass.hideDialog();
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                rv_transferHistory.setVisibility(View.GONE);
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            rv_transferHistory.setVisibility(View.GONE);
                            if (json.has("message")) {
                                new SnackbarUtils(main_layout, json.getString("message"),
                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        rv_transferHistory.setVisibility(View.GONE);
                        new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();

                    }
                }
                catch (Exception e)
                {
                    Log.e("GetCardList", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    rv_transferHistory.setVisibility(View.GONE);
                    new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }.header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            TiCKTOC_Driver_Application.setCurrentActivity(activity);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity) != null && !SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("")
                && SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
        {
            Intent intent = new Intent(activity,Create_Passcode_Activity.class);
            startActivity(intent);
            if (Wallet_Balance_Activity.activity!=null)
            {
                Wallet_Balance_Activity.activity.finish();
            }
            if (Wallet_Balance_TopUp_Activity.activity!=null)
            {
                Wallet_Balance_TopUp_Activity.activity.finish();
            }
            if (Wallet_Balance_TransferToBank_Activity.activity!=null)
            {
                Wallet_Balance_TransferToBank_Activity.activity.finish();
            }
            if (Wallet_Cards_Activity.activity!=null)
            {
                Wallet_Cards_Activity.activity.finish();
            }
            if (Wallet_Transfer_Activity.activity!=null)
            {
                Wallet_Transfer_Activity.activity.finish();
            }
            finish();
        }
    }
}