package iiride.app.driver.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import iiride.app.driver.Application.TiCKTOC_Driver_Application;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;
import iiride.app.driver.Others.ConnectivityReceiver;

import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;




public class Profile_DriverProfile_Activity extends AppCompatActivity implements View.OnClickListener{

    Profile_DriverProfile_Activity activity;
    LinearLayout ll_save, ll_back;
    ImageView iv_back, iv_profilePic, iv_save;
    RelativeLayout rl_profile_pic;
    LinearLayout main_layout;
    EditText et_email_profile, et_full_name_profile, et_mobile_profile, et_residential_address_profile, et_subarb_profile, et_post_code_profile, et_city_profile, et_country_profile, et_state_profile;
    CheckBox checkBox_male, checkBox_Female, checkBox_others;
    String MALE="Male", FEMALE ="Female", OTHERS="Others", chackBox_Selected ="";

    String chooseCamera="camera", chooseGallery="gallery", userChoosenTask="";
    private static final int MY_REQUEST_CODE = 102;
    private int GALLERY = 1, CAMERA = 0;
    byte[] Profile_ByteImage=null;
    Transformation mTransformation;

    String encodedImage;

    private AQuery aQuery;
    DialogClass dialogClass;
    Bitmap BitmapLLL;

    Spinner spinner_CompanyId;
    JSONArray jsonArray_CompanyId = new JSONArray();
    List<String> list_companyName = new ArrayList<String>();
    List<String> list_CompanyId = new ArrayList<>() ;

    /*DriverId,CompanyId,Fullname,Gender,Address,Suburb,Zipcode,City,State,Country,DriverImage*/
    Uri mImageUri;
    private Uri imageUri;
    Bitmap bitmapImage;
    String picturePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_profile);

        activity = Profile_DriverProfile_Activity.this;

        mImageUri=null;
        imageUri=null;
        bitmapImage=null;
        picturePath=null;

        initUI();
    }

    private void initUI()
    {
        aQuery = new AQuery(activity);
        main_layout = (LinearLayout) findViewById(R.id.main_layout);

        ll_save = (LinearLayout) findViewById(R.id.ll_save);
        ll_back = (LinearLayout) findViewById(R.id.ll_back);

        spinner_CompanyId = (Spinner) findViewById(R.id.spinner_CompanyId);

        et_email_profile = (EditText) findViewById(R.id.et_email_profile);
        et_full_name_profile = (EditText) findViewById(R.id.et_full_name_profile);
        et_mobile_profile = (EditText) findViewById(R.id.et_mobile_profile);
        et_residential_address_profile = (EditText) findViewById(R.id.et_residential_address_profile);
        et_subarb_profile = (EditText) findViewById(R.id.et_subarb_profile);
        et_post_code_profile = (EditText) findViewById(R.id.et_post_code_profile);
        et_city_profile = (EditText) findViewById(R.id.et_city_profile);
        et_state_profile = (EditText) findViewById(R.id.et_state_profile);
        et_country_profile = (EditText) findViewById(R.id.et_country_profile);

        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_profilePic = (ImageView) findViewById(R.id.iv_profilePic);
        iv_save = (ImageView) findViewById(R.id.iv_save);

        rl_profile_pic = (RelativeLayout) findViewById(R.id.rl_profile_pic);

        checkBox_male = (CheckBox) findViewById(R.id.checkBox_male);
        checkBox_male.setChecked(true);
        chackBox_Selected = MALE;
        checkBox_Female = (CheckBox) findViewById(R.id.checkBox_Female);
        checkBox_others = (CheckBox) findViewById(R.id.checkBox_others);

        checkBox_male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              selectMale();
            }
        });
        checkBox_Female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               selectFemale();
            }
        });
        checkBox_others.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               selectOthers();
            }
        });

        rl_profile_pic.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        ll_back.setOnClickListener(this);
        ll_save.setOnClickListener(this);
        iv_save.setOnClickListener(this);

        spinner_CompanyId.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                Log.e("tttttttt","onItemSelected : "+position);
                Log.e("tttttttt","onItemSelected 11 : "+spinner_CompanyId.getSelectedItem().toString());
                try
                {
                    JSONObject jsonObj = new JSONObject(SessionSave.getUserSession(Comman.USER_COMPANY_ID_ARRAY,activity));
                    JSONArray jsonArray = jsonObj.getJSONArray(Comman.USER_COMPANY_ARRAY_NAME_FOR_STORE);
                    for (int i=0; i<jsonArray.length(); i++)
                    {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String CompanyId = jsonObject.getString(Comman.USER_COMPANY_ID_FOR_STORE);
                        String CompanyName = jsonObject.getString(Comman.USER_COMPANY_NAME_FOR_STORE);
                        if (String.valueOf(spinner_CompanyId.getSelectedItem())!=null && !String.valueOf(spinner_CompanyId.getSelectedItem()).equalsIgnoreCase("")
                                && String.valueOf(spinner_CompanyId.getSelectedItem()).equalsIgnoreCase(CompanyName))
                        {
                            Log.e("selected ", " :"+String.valueOf(spinner_CompanyId.getSelectedItem()+" : "+CompanyId+"\n "+jsonObject.getString(Comman.USER_COMPANY_CITY_FOR_STORE)));
                            et_city_profile.setText(jsonObject.getString(Comman.USER_COMPANY_CITY_FOR_STORE));
                            et_state_profile.setText(jsonObject.getString(Comman.USER_COMPANY_STATE_FOR_STORE));
                            et_country_profile.setText(jsonObject.getString(Comman.USER_COMPANY_COUNTRY_FOR_STORE));
                        }
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });

        mTransformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(100)
                .borderColor(ContextCompat.getColor(activity, R.color.colorBlack))
                .borderWidthDp(2)
                .oval(true)
                .build();

        String UserImage = SessionSave.getUserSession(Comman.USER_PROFILE_IMAGE, activity);

        if(UserImage!=null && !UserImage.equalsIgnoreCase(""))
        {
            Picasso.with(activity)
                    .load(SessionSave.getUserSession(Comman.USER_PROFILE_IMAGE,activity))
                    .fit()
                    .transform(mTransformation)
                    .into(iv_profilePic);

            Picasso.with(this)
                    .load(SessionSave.getUserSession(Comman.USER_PROFILE_IMAGE,activity))
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded (final Bitmap bitmap, Picasso.LoadedFrom from) {
                            BitmapLLL = bitmap;
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {}

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {}
                    });
        }
        else
        {
            iv_profilePic.setImageResource(R.drawable.prof_image_demo);
        }

        Log.e("USER_GENDER", "USER_GENDER USER_GENDER : "+SessionSave.getUserSession(Comman.USER_GENDER,activity));
        if (SessionSave.getUserSession(Comman.USER_GENDER,activity)!= null && !SessionSave.getUserSession(Comman.USER_GENDER,activity).equalsIgnoreCase(""))
        {
            if (SessionSave.getUserSession(Comman.USER_GENDER,activity).equals(FEMALE))
            {
                selectFemale();
            }
            else  if (SessionSave.getUserSession(Comman.USER_GENDER,activity).equals(OTHERS))
            {
               selectOthers();
            }
            else
            {
                selectMale();
            }
        }

        if (SessionSave.getUserSession(Comman.USER_EMAIL,activity)!= null && !SessionSave.getUserSession(Comman.USER_EMAIL,activity).equalsIgnoreCase(""))
        {
            et_email_profile.setText(SessionSave.getUserSession(Comman.USER_EMAIL,activity));
        }

        if (SessionSave.getUserSession(Comman.USER_MOBILE_NUMBER,activity)!= null && !SessionSave.getUserSession(Comman.USER_MOBILE_NUMBER,activity).equalsIgnoreCase(""))
        {
            et_mobile_profile.setText(SessionSave.getUserSession(Comman.USER_MOBILE_NUMBER,activity));
        }

        if (SessionSave.getUserSession(Comman.USER_FULL_NAME,activity)!= null && !SessionSave.getUserSession(Comman.USER_FULL_NAME,activity).equalsIgnoreCase(""))
        {
            et_full_name_profile.setText(SessionSave.getUserSession(Comman.USER_FULL_NAME,activity));
        }

        if (SessionSave.getUserSession(Comman.USER_RESIDENTIAL_ADDRESS,activity)!= null && !SessionSave.getUserSession(Comman.USER_RESIDENTIAL_ADDRESS,activity).equalsIgnoreCase(""))
        {
            et_residential_address_profile.setText(SessionSave.getUserSession(Comman.USER_RESIDENTIAL_ADDRESS,activity));
        }
        if (SessionSave.getUserSession(Comman.USER_SUB_URB,activity)!= null && !SessionSave.getUserSession(Comman.USER_SUB_URB,activity).equalsIgnoreCase(""))
        {
            et_subarb_profile.setText(SessionSave.getUserSession(Comman.USER_SUB_URB,activity));
        }

        if (SessionSave.getUserSession(Comman.USER_POST_CODE,activity)!= null && !SessionSave.getUserSession(Comman.USER_POST_CODE,activity).equalsIgnoreCase(""))
        {
            et_post_code_profile.setText(SessionSave.getUserSession(Comman.USER_POST_CODE,activity));
        }

        if (SessionSave.getUserSession(Comman.USER_CITY,activity)!= null && !SessionSave.getUserSession(Comman.USER_CITY,activity).equalsIgnoreCase(""))
        {
            et_city_profile.setText(SessionSave.getUserSession(Comman.USER_CITY,activity));
        }

        if (SessionSave.getUserSession(Comman.USER_COUNTRY,activity)!= null && !SessionSave.getUserSession(Comman.USER_COUNTRY,activity).equalsIgnoreCase(""))
        {
            et_country_profile.setText(SessionSave.getUserSession(Comman.USER_COUNTRY,activity));
        }

        if (SessionSave.getUserSession(Comman.USER_STATE,activity)!= null && !SessionSave.getUserSession(Comman.USER_STATE,activity).equalsIgnoreCase(""))
        {
            et_state_profile.setText(SessionSave.getUserSession(Comman.USER_STATE,activity));
        }

        if (ConnectivityReceiver.isConnected())
        {
            UpdateCompanyIdList();
        }
        else
        {
            new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }

    }

    public void selectMale()
    {
        chackBox_Selected = MALE;
        checkBox_male.setChecked(true);
        checkBox_Female.setChecked(false);
        checkBox_others.setChecked(false);
    }
    public void selectFemale()
    {
        chackBox_Selected = FEMALE;
        checkBox_male.setChecked(false);
        checkBox_Female.setChecked(true);
        checkBox_others.setChecked(false);
    }
    public void selectOthers()
    {
        chackBox_Selected = OTHERS;
        checkBox_male.setChecked(false);
        checkBox_Female.setChecked(false);
        checkBox_others.setChecked(true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.rl_profile_pic:
                ShowPictureDialog();
                break;

            case R.id.iv_save:
                saveProfileDetail();
                break;

            case R.id.ll_save:
                saveProfileDetail();
                break;
        }
    }

    private void UpdateCompanyIdList()
    {
        dialogClass = new DialogClass(activity, 1);
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_GET_COMPANY_ID_LIST ;

        Log.e("url", "UpdateCompanyIdList = " + url);
        Log.e("param", "UpdateCompanyIdList = " + params);


        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "UpdateCompanyIdList = " + responseCode);
                    Log.e("Response", "UpdateCompanyIdList = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");
                                dialogClass.hideDialog();
                                if (json.has("company"))
                                {
                                    String  CompanyId="", CompanyName="", Address="", City="", State="", Country="";
                                    jsonArray_CompanyId = new JSONArray();
                                    list_companyName.clear();

                                    JSONArray arrayCompany = json.getJSONArray("company");

                                    list_CompanyId.clear();
                                    for (int i=0; i<arrayCompany.length() ; i++)
                                    {
                                        JSONObject objectCompany = arrayCompany.getJSONObject(i);
                                        if (objectCompany.has("Id"))
                                        {
                                            CompanyId = objectCompany.getString("Id");
                                            list_CompanyId.add(CompanyId);
                                        }
                                        if (objectCompany.has("CompanyName"))
                                        {
                                            CompanyName = objectCompany.getString("CompanyName");
                                        }
                                        if (objectCompany.has("City"))
                                        {
                                            City = objectCompany.getString("City");
                                        }
                                        if (objectCompany.has("State"))
                                        {
                                            State = objectCompany.getString("State");
                                        }
                                        if (objectCompany.has("Country"))
                                        {
                                            Country = objectCompany.getString("Country");
                                        }
                                        JSONObject companyIdObject = new JSONObject();
                                        companyIdObject.put(Comman.USER_COMPANY_ID_FOR_STORE, CompanyId);
                                        companyIdObject.put(Comman.USER_COMPANY_NAME_FOR_STORE, CompanyName);
                                        companyIdObject.put(Comman.USER_COMPANY_CITY_FOR_STORE, City);
                                        companyIdObject.put(Comman.USER_COMPANY_STATE_FOR_STORE, State);
                                        companyIdObject.put(Comman.USER_COMPANY_COUNTRY_FOR_STORE, Country);
                                        jsonArray_CompanyId.put(companyIdObject);
                                        list_companyName.add(CompanyName);
                                    }

                                    JSONObject studentsObj = new JSONObject();
                                    studentsObj.put(Comman.USER_COMPANY_ARRAY_NAME_FOR_STORE, jsonArray_CompanyId);

                                    String jsonCompanyIdStr = studentsObj.toString();
                                    SessionSave.saveUserSession(Comman.USER_COMPANY_ID_ARRAY, jsonCompanyIdStr, activity);

                                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, list_companyName);
                                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    spinner_CompanyId.setAdapter(dataAdapter);

                                    Log.e("jsonArray_CompanyId", "jsonArray_CompanyIdj222222222222222 :"+SessionSave.getUserSession(Comman.USER_COMPANY_ID_ARRAY,activity));
//                                    list_CompanyId
                                    if (SessionSave.getUserSession(Comman.USER_COMPANY_ID,activity)!= null && !SessionSave.getUserSession(Comman.USER_COMPANY_ID,activity).equalsIgnoreCase(""))
                                    {
                                        Log.e("jsonArray_CompanyId","111111111111111");
                                        if (list_CompanyId.size()>0)
                                        {
                                            Log.e("jsonArray_CompanyId","222222222222222");

                                            for (int i=0; i<list_CompanyId.size(); i++)
                                            {
                                                Log.e("jsonArray_CompanyId","333333333333333");

                                                if (list_CompanyId.get(i).equalsIgnoreCase(SessionSave.getUserSession(Comman.USER_COMPANY_ID,activity))){
                                                    Log.e("jsonArray_CompanyId","USER_COMPANY_ID : "+SessionSave.getUserSession(Comman.USER_COMPANY_ID,activity));
                                                    Log.e("jsonArray_CompanyId","list_CompanyId.get(i) : "+list_CompanyId.get(i));
                                                    Log.e("jsonArray_CompanyId","444444444444444 : "+i);
                                                    spinner_CompanyId.setSelection(i);
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message")) {
                                new SnackbarUtils(main_layout, json.getString("message"),
                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                    }
                } catch (Exception e) {
                    Log.e("UpdateCompanyIdList", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }


    private void saveProfileDetail()
    {
        if (TextUtils.isEmpty(et_full_name_profile.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_full_name),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        /*else  if (TextUtils.isEmpty(et_mobile_profile.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_mobile_number),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }*/
        /*else if(et_mobile_profile.getText().toString().length() != 9)
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_valid_mobile_number),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }*/
        else  if (TextUtils.isEmpty(et_residential_address_profile.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_residential_address),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_subarb_profile.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_sub_arb),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_post_code_profile.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_post_code),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_city_profile.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_city),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_country_profile.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_country),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_state_profile.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_state),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else
        {
            try
            {
                JSONObject jsonObj = new JSONObject(SessionSave.getUserSession(Comman.USER_COMPANY_ID_ARRAY,activity));
                JSONArray jsonArray = jsonObj.getJSONArray(Comman.USER_COMPANY_ARRAY_NAME_FOR_STORE);
                for (int i=0; i<jsonArray.length(); i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    String CompanyId = jsonObject.getString(Comman.USER_COMPANY_ID_FOR_STORE);
                    String CompanyName = jsonObject.getString(Comman.USER_COMPANY_NAME_FOR_STORE);
                    if (String.valueOf(spinner_CompanyId.getSelectedItem())!=null && !String.valueOf(spinner_CompanyId.getSelectedItem()).equalsIgnoreCase("")
                            && String.valueOf(spinner_CompanyId.getSelectedItem()).equalsIgnoreCase(CompanyName))
                    {
                        SessionSave.saveUserSession(Comman.USER_COMPANY_ID,CompanyId, activity);
                        Log.e("selected ", " :"+String.valueOf(spinner_CompanyId.getSelectedItem()+" : "+CompanyId));
                    }
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }

            String driverId =  SessionSave.getUserSession(Comman.USER_ID, activity);
            if (driverId!=null && !driverId.equalsIgnoreCase(""))
            {
                if (ConnectivityReceiver.isConnected())
                {
                    UpdateDriverProfile(driverId);
                }
                else
                {
                    new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }
    }



    private void UpdateDriverProfile(String driverId)
    {
        dialogClass = new DialogClass(activity, 1);
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_UPDATE_DRIVER_BASIC_INFO ;

        /*DriverId,CompanyId,Fullname,Gender,Address,Suburb,Zipcode,City,State,Country,DriverImage*/

        params.put(WebServiceAPI.UPDATE_DRIVER_BASIC_INFO_PARAM_DIVER_ID, driverId);
        params.put(WebServiceAPI.UPDATE_DRIVER_BASIC_INFO_PARAM_COMPANY_ID,SessionSave.getUserSession(Comman.USER_COMPANY_ID,activity));

        params.put(WebServiceAPI.UPDATE_DRIVER_BASIC_INFO_PARAM_FULL_NAME, et_full_name_profile.getText().toString());
        params.put(WebServiceAPI.UPDATE_DRIVER_BASIC_INFO_PARAM_GENDER, chackBox_Selected);

        params.put(WebServiceAPI.UPDATE_DRIVER_BASIC_INFO_PARAM_ADDRESS, et_residential_address_profile.getText().toString());
        params.put(WebServiceAPI.UPDATE_DRIVER_BASIC_INFO_PARAM_SUB_URB, et_subarb_profile.getText().toString());

        params.put(WebServiceAPI.UPDATE_DRIVER_BASIC_INFO_PARAM_ZIPCODE, et_post_code_profile.getText().toString());
        params.put(WebServiceAPI.UPDATE_DRIVER_BASIC_INFO_PARAM_CITY, et_city_profile.getText().toString());

        params.put(WebServiceAPI.UPDATE_DRIVER_BASIC_INFO_PARAM_STATE, et_state_profile.getText().toString());
        params.put(WebServiceAPI.UPDATE_DRIVER_BASIC_INFO_PARAM_COUNTRY, et_country_profile.getText().toString());
        if (Profile_ByteImage!=null)
        {
            params.put(WebServiceAPI.UPDATE_DRIVER_BASIC_INFO_PARAM_DRIVER_IMAGE, Profile_ByteImage);
        }

        Log.e("url", "UpdateDriverProfile = " + url);
        Log.e("param", "UpdateDriverProfile = " + params);


        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "UpdateDriverProfile = " + responseCode);
                    Log.e("Response", "UpdateDriverProfile = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");
                                if (json.has("profile"))
                                {
                                    JSONObject driverProfile = json.getJSONObject("profile");
                                    if (driverProfile!=null)
                                    {
                                        String DriverId="", CompanyId="", DispatcherId="", Email="", FullName="", MobileNo="", Gender="", Image="", QRCode="", Password="", Address="", SubUrb = ""
                                                , City="", State="", Country="", Zipcode="", ReferralCode="", DriverLicense="", AccreditationCertificate="", DriverLicenseExpire=""
                                                , AccreditationCertificateExpire="", BankHolderName, BankName="", BankAcNo="", BSB="", Lat="", Lng="", Status=""
                                                , Availability="", DriverDuty="", ABN="", serviceDescription="", DCNumber="", ProfileComplete="", CategoryId="", Balance="", ReferralAmount="";

                                        if (driverProfile.has("Id"))
                                        {
                                            DriverId = driverProfile.getString("Id");
                                            SessionSave.saveUserSession(Comman.USER_ID, DriverId, activity);
                                        }
                                        if (driverProfile.has("CompanyId"))
                                        {
                                            CompanyId = driverProfile.getString("CompanyId");
                                            SessionSave.saveUserSession(Comman.USER_COMPANY_ID, CompanyId, activity);
                                        }
                                        if (driverProfile.has("DispatcherId"))
                                        {
                                            DispatcherId = driverProfile.getString("DispatcherId");
                                            SessionSave.saveUserSession(Comman.USER_DISPATHER_ID, DispatcherId, activity);
                                        }
                                        if (driverProfile.has("Email"))
                                        {
                                            Email = driverProfile.getString("Email");
                                            SessionSave.saveUserSession(Comman.USER_EMAIL, Email, activity);
                                        }
                                        if (driverProfile.has("Fullname"))
                                        {
                                            FullName = driverProfile.getString("Fullname");
                                            SessionSave.saveUserSession(Comman.USER_FULL_NAME, FullName, activity);
                                        }


                                        if (driverProfile.has("MobileNo"))
                                        {
                                            MobileNo = driverProfile.getString("MobileNo");
                                            SessionSave.saveUserSession(Comman.USER_MOBILE_NUMBER, MobileNo, activity);
                                        }
                                        if (driverProfile.has("Gender"))
                                        {
                                            Gender = driverProfile.getString("Gender");
                                            Log.e("USER_GENDER","Gender : "+Gender);
                                            SessionSave.saveUserSession(Comman.USER_GENDER, Gender, activity);
                                        }
                                        if (driverProfile.has("Image"))
                                        {
                                            Image = driverProfile.getString("Image");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_IMAGE, Image, activity);
                                        }
                                        if (driverProfile.has("QRCode"))
                                        {
                                            QRCode = driverProfile.getString("QRCode");
                                            SessionSave.saveUserSession(Comman.USER_QR_CODE, QRCode, activity);
                                        }
                                        if (driverProfile.has("Password"))
                                        {
                                            Password = driverProfile.getString("Password");
                                            SessionSave.saveUserSession(Comman.USER_PASSWORD, Password, activity);
                                        }
                                        if (driverProfile.has("Address"))
                                        {
                                            Address = driverProfile.getString("Address");
                                            SessionSave.saveUserSession(Comman.USER_RESIDENTIAL_ADDRESS, Address, activity);
                                        }
                                        if (driverProfile.has("SubUrb"))
                                        {
                                            SubUrb = driverProfile.getString("SubUrb");
                                            SessionSave.saveUserSession(Comman.USER_SUB_URB, SubUrb, activity);
                                        }


                                        if (driverProfile.has("City"))
                                        {
                                            City = driverProfile.getString("City");
                                            SessionSave.saveUserSession(Comman.USER_CITY, City, activity);
                                        }
                                        if (driverProfile.has("State"))
                                        {
                                            State = driverProfile.getString("State");
                                            SessionSave.saveUserSession(Comman.USER_STATE, State, activity);
                                        }
                                        if (driverProfile.has("Country"))
                                        {
                                            Country = driverProfile.getString("Country");
                                            SessionSave.saveUserSession(Comman.USER_COUNTRY, Country, activity);
                                        }
                                        if (driverProfile.has("ZipCode"))
                                        {
                                            Zipcode = driverProfile.getString("ZipCode");
                                            SessionSave.saveUserSession(Comman.USER_POST_CODE, Zipcode, activity);
                                        }
                                        if (driverProfile.has("ReferralCode"))
                                        {
                                            ReferralCode = driverProfile.getString("ReferralCode");
                                            SessionSave.saveUserSession(Comman.USER_REFERRAL_CODE, ReferralCode, activity);
                                        }



                                        if (driverProfile.has("DriverLicense"))
                                        {
                                            DriverLicense = driverProfile.getString("DriverLicense");
                                            SessionSave.saveUserSession(Comman.USER_DRIVER_LICENCE, DriverLicense, activity);
                                        }
                                        if (driverProfile.has("AccreditationCertificate"))
                                        {
                                            AccreditationCertificate = driverProfile.getString("AccreditationCertificate");
                                            SessionSave.saveUserSession(Comman.USER_ACCREDITATION_CERTY, AccreditationCertificate, activity);
                                        }
                                        if (driverProfile.has("DriverLicenseExpire"))
                                        {
                                            DriverLicenseExpire = driverProfile.getString("DriverLicenseExpire");
                                            SessionSave.saveUserSession(Comman.USER_DRIVER_lICENSE_EXPIRE, DriverLicenseExpire, activity);
                                        }
                                        if (driverProfile.has("AccreditationCertificateExpire"))
                                        {
                                            AccreditationCertificateExpire = driverProfile.getString("AccreditationCertificateExpire");
                                            SessionSave.saveUserSession(Comman.USER_ACCREDITATION_CERTY_EXPIRE, AccreditationCertificateExpire, activity);
                                        }
                                        if (driverProfile.has("BankHolderName"))
                                        {
                                            BankHolderName = driverProfile.getString("BankHolderName");
                                            SessionSave.saveUserSession(Comman.USER_BANK_ACCOUNT_HOLDER_NAME, BankHolderName, activity);
                                        }
                                        if (driverProfile.has("BankName"))
                                        {
                                            BankName = driverProfile.getString("BankName");
                                            SessionSave.saveUserSession(Comman.USER_BANK_NAME, BankName, activity);
                                        }



                                        if (driverProfile.has("BankAcNo"))
                                        {
                                            BankAcNo = driverProfile.getString("BankAcNo");
                                            SessionSave.saveUserSession(Comman.USER_BANK_AC_NO, BankAcNo, activity);
                                        }
                                        if (driverProfile.has("BSB"))
                                        {
                                            BSB = driverProfile.getString("BSB");
                                            SessionSave.saveUserSession(Comman.USER_BSB, BSB, activity);
                                        }
                                        if (driverProfile.has("Lat"))
                                        {
                                            Lat = driverProfile.getString("Lat");
                                            SessionSave.saveUserSession(Comman.USER_LATITUDE, Lat, activity);
                                        }
                                        if (driverProfile.has("Lng"))
                                        {
                                            Lng = driverProfile.getString("Lng");
                                            SessionSave.saveUserSession(Comman.USER_LONGITUDE, Lng, activity);
                                        }
                                        if (driverProfile.has("Status"))
                                        {
                                            Status = driverProfile.getString("Status");
                                            SessionSave.saveUserSession(Comman.USER_STATE, Status, activity);
                                        }
                                        if (driverProfile.has("Availability"))
                                        {
                                            Availability = driverProfile.getString("Availability");
                                            SessionSave.saveUserSession(Comman.USER_AVAILABILITY, Availability, activity);
                                        }



                                        if (driverProfile.has("DriverDuty"))
                                        {
                                            DriverDuty = driverProfile.getString("DriverDuty");
                                            SessionSave.saveUserSession(Comman.USER_DRIVER_DUTY, DriverDuty, activity);
                                        }
                                        if (driverProfile.has("ABN"))
                                        {
                                            ABN = driverProfile.getString("ABN");
                                            SessionSave.saveUserSession(Comman.USER_ABN, ABN, activity);
                                        }
                                        if (driverProfile.has("Description"))
                                        {
                                            serviceDescription = driverProfile.getString("Description");
                                            SessionSave.saveUserSession(Comman.USER_SERVICE_DESCRIPTION, serviceDescription, activity);
                                        }
                                        if (driverProfile.has("DCNumber"))
                                        {
                                            DCNumber = driverProfile.getString("DCNumber");
                                            SessionSave.saveUserSession(Comman.USER_DC_NUMBER, DCNumber, activity);
                                        }
                                        if (driverProfile.has("ProfileComplete"))
                                        {
                                            ProfileComplete = driverProfile.getString("ProfileComplete");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_COMPLETE, ProfileComplete, activity);
                                        }

                                        if (driverProfile.has("CategoryId"))
                                        {
                                            CategoryId = driverProfile.getString("CategoryId");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_CATEGORY_ID, CategoryId, activity);
                                        }
                                        if (driverProfile.has("Balance"))
                                        {
                                            Balance = driverProfile.getString("Balance");
                                            SessionSave.saveUserSession(Comman.DRIVER_WALLET_BALLENCE, Balance, activity);
                                        }
                                        if (driverProfile.has("ReferralAmount"))
                                        {
                                            ReferralAmount = driverProfile.getString("ReferralAmount");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_REFERRAL_AMOUNT, ReferralAmount, activity);
                                        }


                                        if (driverProfile.has("Vehicle"))
                                        {
                                            JSONObject driverVehicle = driverProfile.getJSONObject("Vehicle");
                                            if (driverVehicle!=null)
                                            {
                                                String VehicleId, VehicleModel, CarCompany, CarColor, VehicleRegistrationNo, RegistrationCertificate, VehicleInsuranceCertificate
                                                        , RegistrationCertificateExpire, VehicleInsuranceCertificateExpire, VehicleImage, Description, VehicleModelName;

                                                if (driverVehicle.has("Id"))
                                                {
                                                    VehicleId = driverVehicle.getString("Id");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_ID, VehicleId, activity);
                                                }
                                                if (driverVehicle.has("VehicleModel"))
                                                {
                                                    VehicleModel = driverVehicle.getString("VehicleModel");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL, VehicleModel, activity);
                                                }
                                                if (driverVehicle.has("Company"))
                                                {
                                                    CarCompany = driverVehicle.getString("Company");
                                                    SessionSave.saveUserSession(Comman.USER_CAR_COMPANY, CarCompany, activity);
                                                }
                                                if (driverVehicle.has("Color"))
                                                {
                                                    CarColor = driverVehicle.getString("Color");
                                                    SessionSave.saveUserSession(Comman.USER_CAR_COLOR, CarColor, activity);
                                                }
                                                if (driverVehicle.has("VehicleRegistrationNo"))
                                                {
                                                    VehicleRegistrationNo = driverVehicle.getString("VehicleRegistrationNo");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_REGISTRATION_NO, VehicleRegistrationNo, activity);
                                                }


                                                if (driverVehicle.has("RegistrationCertificate"))
                                                {
                                                    RegistrationCertificate = driverVehicle.getString("RegistrationCertificate");
                                                    SessionSave.saveUserSession(Comman.USER_REGISTRATION_CERTY, RegistrationCertificate, activity);
                                                }
                                                if (driverVehicle.has("VehicleInsuranceCertificate"))
                                                {
                                                    VehicleInsuranceCertificate = driverVehicle.getString("VehicleInsuranceCertificate");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_INSURANCE_CERTY, VehicleInsuranceCertificate, activity);
                                                }
                                                if (driverVehicle.has("RegistrationCertificateExpire"))
                                                {
                                                    RegistrationCertificateExpire = driverVehicle.getString("RegistrationCertificateExpire");
                                                    SessionSave.saveUserSession(Comman.USER_REGISTRATION_CERTY_EXPIRE, RegistrationCertificateExpire, activity);
                                                }
                                                if (driverVehicle.has("VehicleInsuranceCertificateExpire"))
                                                {
                                                    VehicleInsuranceCertificateExpire = driverVehicle.getString("VehicleInsuranceCertificateExpire");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_INSURANCE_CERTY_EXPIRE, VehicleInsuranceCertificateExpire, activity);
                                                }
                                                if (driverVehicle.has("VehicleImage"))
                                                {
                                                    VehicleImage = driverVehicle.getString("VehicleImage");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE, VehicleImage, activity);
                                                }

                                                if (driverVehicle.has("Description"))
                                                {
                                                    Description = driverVehicle.getString("Description");
                                                    SessionSave.saveUserSession(Comman.USER_CAR_DESCRIPTION, Description, activity);
                                                }
                                                if (driverVehicle.has("VehicleClass"))
                                                {
                                                    VehicleModelName = driverVehicle.getString("VehicleClass");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL_NAME, VehicleModelName, activity);

                                                    Log.e("VehicleModelName","VehicleModelName : "+VehicleModelName);
                                                    if (VehicleModelName!=null && VehicleModelName.contains("First Class"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "First Class", activity);
                                                    }
                                                    else if (VehicleModelName.contains("Business Class"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", activity);
                                                    }
                                                    else if (VehicleModelName.contains("Economy"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Economy", activity);
                                                    }
                                                    else if (VehicleModelName.contains("Taxi"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Taxi", activity);
                                                    }
                                                    else if (VehicleModelName.contains("LUX-VAN"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "LUX-VAN", activity);
                                                    }
                                                    else if (VehicleModelName.contains("Disability"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Disability", activity);
                                                    }
                                                    else
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", activity);
                                                    }
                                                } else {
                                                    SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", activity);
                                                }
                                                dialogClass.hideDialog();
                                            }
                                            else
                                            {
                                                //////// else
                                                dialogClass.hideDialog();
                                            }
                                            dialogClass.hideDialog();
                                            new SnackbarUtils(main_layout, getResources().getString(R.string.profile_updated_successfully),
                                                    ContextCompat.getColor(activity, R.color.snakbar_color),
                                                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();

                                            Log.e("GGGGGGGGGGG","GGGGGGGGGGG :"+SessionSave.getUserSession(Comman.USER_ID, activity));
                                        }
                                        else
                                        {
                                            dialogClass.hideDialog();
                                            if (json.has("message")) {
                                                new SnackbarUtils(main_layout, json.getString("message"),
                                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                            }
                                        }

                                    }
                                    else
                                    {
                                        dialogClass.hideDialog();
                                        if (json.has("message")) {
                                            new SnackbarUtils(main_layout, json.getString("message"),
                                                    ContextCompat.getColor(activity, R.color.snakbar_color),
                                                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                        }
                                    }
                                }
                                else
                                {
                                    dialogClass.hideDialog();
                                    if (json.has("message"))
                                    {
                                        new SnackbarUtils(main_layout, json.getString("message"),
                                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                    }
                                }
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message")) {
                                new SnackbarUtils(main_layout, json.getString("message"),
                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                    }
                } catch (Exception e) {
                    Log.e("UpdateDriverProfile", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }.header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }



    private void ShowPictureDialog()
    {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_REQUEST_CODE);
        }
        else
        {
            AlertDialog.Builder pictureDialog = new AlertDialog.Builder(activity);
            pictureDialog.setTitle("Select Action");
            String[] pictureDialogItems = {"Select photo from gallery", "Capture photo from camera" };
            pictureDialog.setItems(pictureDialogItems,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:
                                    choosePhotoFromGallary();
                                    break;
                                case 1:
                                    takePhotoFromCamera();
                                    break;
                            }
                        }
                    });
            pictureDialog.show();
        }
    }

    public void choosePhotoFromGallary()
    {
        userChoosenTask =chooseGallery;
        Intent intent_gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent_gallery, GALLERY);
    }

    private void takePhotoFromCamera()
    {
        userChoosenTask = chooseCamera;
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "NewPicture");
        imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        Log.e("imageUri", "imageUri11111111111111 : "+imageUri);
        startActivityForResult(intent, CAMERA);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.e("showPictureDialog", "onRequestPermissionsResult requestCode "+requestCode);
        switch (requestCode) {
            case MY_REQUEST_CODE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED)
                {
                    ShowPictureDialog();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK)
        {
            if (requestCode == GALLERY)
            {
                onSelectFromGalleryResult(data);
            }
            else if (requestCode == CAMERA)
            {
                onCaptureImageResult();
            }
            else
            {
                Profile_ByteImage=null;
            }
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data)
    {
        Bitmap bitmap= null;
        try
        {
            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
        }
        catch (IOException e) {
            e.printStackTrace();
            Log.e("MediaStore","MediaStore");
        }

        bitmapImage = getResizedBitmap(bitmap,400);

        if (bitmapImage!=null)
        {
            Picasso.with(activity)
                    .load(data.getData())
                    .fit()
                    .transform(mTransformation)
                    .into(iv_profilePic);
            Profile_ByteImage = ConvertToByteArray(bitmapImage);
        }
        else
        {
            Profile_ByteImage=null;
            new SnackbarUtils(main_layout, getString(R.string.please_select_profile_again),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
    }


    private void onCaptureImageResult()
    {
        try
        {
            Log.e("imageUri", "imageUri2222222222222222222 : "+imageUri);
            Bitmap thumbnail = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
            Log.e("#########","bmp height = "+thumbnail.getHeight());
            Log.e("#########","bmp width = "+thumbnail.getWidth());
            thumbnail = getResizedBitmap(thumbnail,400);
            bitmapImage=thumbnail;

            if (thumbnail!=null)
            {
                Picasso.with(activity)
                        .load(imageUri)
                        .fit()
                        .transform(mTransformation)
                        .into(iv_profilePic);
                Profile_ByteImage = ConvertToByteArray(thumbnail);

            }
            else
            {
                iv_profilePic.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.prof_image_demo));
                Profile_ByteImage=null;
                new SnackbarUtils(main_layout, getString(R.string.please_select_profile_again),
                        ContextCompat.getColor(activity, R.color.snakbar_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
            }
        }
        catch (Exception e)
        {
            iv_profilePic.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.prof_image_demo));
            SessionSave.saveUserSession(Comman.USER_PROFILE_IMAGE, "",activity);
            Profile_ByteImage=null;
            Log.e("call","exception = "+e.getMessage());
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public byte[] ConvertToByteArray(Bitmap bmp)
    {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 60, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public static Bitmap decodeBase64(String input)
    {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            TiCKTOC_Driver_Application.setCurrentActivity(activity);
        }
    }
}