package iiride.app.driver.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import iiride.app.driver.Application.TiCKTOC_Driver_Application;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.R;


public class WebViewActivity extends AppCompatActivity implements OnClickListener{
	
	private WebView webview;
	private String value;

	private LinearLayout ll_Back;
	private TextView tvTitle;
	private ProgressBar progress;
	public static WebViewActivity activity;
	private String title = "Detail";

	public static int resumeFlag = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_webview);

		activity = WebViewActivity.this;

		ll_Back = (LinearLayout) findViewById(R.id.ll_back);
		tvTitle = (TextView) findViewById(R.id.tv_title);

		Intent intent = getIntent();
		value  = intent.getExtras().getString("url");
		title = intent.getExtras().getString("title");

		if (title!=null)
		{
			tvTitle.setText(title.trim());
		}


		ll_Back.setOnClickListener(this);

		progress = (ProgressBar) findViewById(R.id.progress_bar);
		webview = (WebView) findViewById(R.id.webView1);
		WebSettings webSettings = webview.getSettings();
		webSettings.setBuiltInZoomControls(false);
		webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
		webSettings.setJavaScriptEnabled(true);
		webSettings.setJavaScriptCanOpenWindowsAutomatically(true);

		if(value.contains("http://"))
		{
			webview.loadUrl(value);
		}
		else if(value.contains("https://"))
		{
			webview.loadUrl(value);
		}

		webview.setWebViewClient(new myWebClient());
		progress.setVisibility(View.VISIBLE);

		webview.setWebChromeClient(new WebChromeClient()
		{
			public void onProgressChanged(WebView view, int progressInt)
			{
				if (progressInt < 80 && progress.getVisibility() == ProgressBar.GONE)
				{
					progress.setVisibility(ProgressBar.VISIBLE);
				}

				if (progressInt >= 80)
				{
					progress.setVisibility(ProgressBar.GONE);
				}
			}
		});

	}

	@Override
	protected void onResume() {
		super.onResume();
		TiCKTOC_Driver_Application.setCurrentActivity(activity);
	}

	public class myWebClient extends WebViewClient
	{
		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
// TODO Auto-generated method stub
			super.onPageStarted(view, url, favicon);
		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
// TODO Auto-generated method stub
			progress.setVisibility(View.VISIBLE);
			view.loadUrl(url);
			return true;

		}

		@Override
		public void onPageFinished(WebView view, String url) {
// TODO Auto-generated method stub
			super.onPageFinished(view, url);
			progress.setVisibility(View.GONE);
		}
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId())
		{
			case R.id.ll_back:
				onBackPressed();
				break;
		}
	}
	

	@Override
	public void onBackPressed() {
		if (webview != null && webview.canGoBack()) {
			webview.goBack();
		}else {
			super.onBackPressed();
			overridePendingTransition(R.anim.left_in, R.anim.right_out);
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (event.getAction() == KeyEvent.ACTION_DOWN)
		{
			switch (keyCode)
			{
				case KeyEvent.KEYCODE_BACK:
					if (webview.canGoBack())
					{
						webview.goBack();
					}
					else
					{
						finish();
					}
					return true;
			}

		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onRestart() {
		super.onRestart();

		if (resumeFlag==1)
		{
			if (SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity)!=null && SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
			{
				resumeFlag = 0;
				if (Wallet__Activity.activity != null)
				{
					Wallet__Activity.activity.finish();
				}
				Intent intent = new Intent(WebViewActivity.this,Create_Passcode_Activity.class);
				intent.putExtra("from","Wallet__Activity");
				startActivity(intent);
				finish();
			}
		}
		else
		{
			resumeFlag=1;
		}
	}
}
