package iiride.app.driver.Activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.hbb20.CountryCodePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import iiride.app.driver.Application.TiCKTOC_Driver_Application;
import iiride.app.driver.Been.initData.InitData;
import iiride.app.driver.Comman.Constants;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;
import iiride.app.driver.Others.GPSTracker;
import iiride.app.driver.Others.Utility;
import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.SnackbarUtils;


/**
 * Created by EXCELLENT-14 on 09-Oct-17.
 */

public class Service_Provider_Activity extends AppCompatActivity {

    Service_Provider_Activity activity;

    TextView tvDone, tv_service_provider;
    ImageView iv_back;

    private LinearLayout main_layout;
    private EditText etCompanyName, etAddress, etAbn, etContactName, etMobileNumber, etEmail, etPhone, etPassword, etConfirmPassword, etBankName, etBsb, etAccNumber;
    Intent intent;

    DialogClass dialogClass;
    private AQuery aQuery;
    String cityName = "", cityId = "";
    private CountryCodePicker codePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_provider);

        activity = Service_Provider_Activity.this;
        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 1);
        init();
    }

    private void init() {
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        codePicker = findViewById(R.id.countryCodePicker);

        tvDone = findViewById(R.id.tvDone);
        main_layout = findViewById(R.id.main_layout);

        etCompanyName = findViewById(R.id.etCompanyName);
        etAbn = findViewById(R.id.etAbn);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        etConfirmPassword = findViewById(R.id.etConfirmPassword);
        etPhone = findViewById(R.id.etPhone);
        etAddress = findViewById(R.id.etAddress);
        etContactName = findViewById(R.id.etContactName);
        etMobileNumber = findViewById(R.id.etMobileNumber);
        etBankName = findViewById(R.id.etBankName);
        etBsb = findViewById(R.id.etBsb);
        etAccNumber = findViewById(R.id.etAccNumber);

        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateDetail();
            }
        });

        getCityName();
        initializeCountry();
    }

    private void initializeCountry()
    {
        try
        {
            if (SessionSave.getInitDataString(activity)!=null && !SessionSave.getInitDataString(activity).equalsIgnoreCase(""))
            {
                JSONObject jsonObject = new JSONObject(SessionSave.getInitDataString(activity));

                if (jsonObject!=null && jsonObject.has("countries"))
                {
                    JSONArray jsonArray = jsonObject.getJSONArray("countries");

                    if (jsonArray!=null && jsonArray.length()>0)
                    {
                        String country = null;
                        String countryNameCode = "AU";
                        for (int i=0; i<jsonArray.length(); i++)
                        {
                            JSONObject countryObject = jsonArray.getJSONObject(i);

                            if (country==null)
                            {
                                if (countryObject!=null && countryObject.has("CountryCode"))
                                {
                                    if (countryObject.getString("CountryCode")!=null && countryObject.getString("CountryCode").equalsIgnoreCase("AU"))
                                    {
                                        Log.e("pos::", i + "");
                                        country = "AU";
                                        countryNameCode = "AU";

                                    }
                                }

                                if (countryObject!=null && countryObject.has("CountryCode"))
                                {
                                    if (countryObject.getString("CountryCode")!=null && countryObject.getString("CountryCode").equalsIgnoreCase("US"))
                                    {
                                        Log.e("pos::", i + "");
                                        country = "US";
                                        countryNameCode = "US";
                                    }
                                }
                            }
                            else
                            {
                                if (countryObject!=null && countryObject.has("CountryCode"))
                                {
                                    if (countryObject.getString("CountryCode")!=null && countryObject.getString("CountryCode").equalsIgnoreCase("AU"))
                                    {
                                        Log.e("pos::", i + "");
                                        country += ",AU";

                                    }
                                }

                                if (countryObject!=null && countryObject.has("CountryCode"))
                                {
                                    if (countryObject.getString("CountryCode")!=null && countryObject.getString("CountryCode").equalsIgnoreCase("US"))
                                    {
                                        Log.e("pos::", i + "");
                                        country += ",US";
                                    }
                                }
                            }
                        }

                        if (country != null)
                            codePicker.setCustomMasterCountries(country);
                        else
                            codePicker.setCustomMasterCountries("");

                        codePicker.setCountryForNameCode(countryNameCode);
                    }
                    else
                    {
                        /*codePicker.setCustomMasterCountries("AU,US");
                        codePicker.setCountryForNameCode("AU,US");*/
                    }
                }
                else
                {
                    /*codePicker.setCustomMasterCountries("AU,US");
                    codePicker.setCountryForNameCode("AU,US");*/
                }
            }
            else
            {
                /*codePicker.setCustomMasterCountries("AU,US");
                codePicker.setCountryForNameCode("AU,US");*/
            }

        }
        catch (Exception e)
        {
            Log.e("call","country parse exception : "+e.getMessage());
            /*codePicker.setCustomMasterCountries("AU,US");
            codePicker.setCountryForNameCode("AU,US");*/
        }
    }

    private void validateDetail() {

        if (etCompanyName.getText().toString().trim().isEmpty())
        {

            new SnackbarUtils(main_layout, getString(R.string.please_enter_company_name),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else if (etAddress.getText().toString().trim().isEmpty())
        {

            new SnackbarUtils(main_layout, getString(R.string.please_enter_company_address),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else if (etAbn.getText().toString().trim().isEmpty())
        {

            new SnackbarUtils(main_layout, getString(R.string.please_enter_abn),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else if (etContactName.getText().toString().trim().isEmpty())
        {

            new SnackbarUtils(main_layout, getString(R.string.please_enter_contact_name),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        /*else if (etMobileNumber.getText().toString().trim().isEmpty())
        {

            new SnackbarUtils(main_layout, getString(R.string.please_enter_mobile_number),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }*/
        else if(codePicker.getSelectedCountryCodeWithPlus().equalsIgnoreCase("+61") && etMobileNumber.getText().toString().length() != 9)
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_valid_mobile_number),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else if(codePicker.getSelectedCountryCodeWithPlus().equalsIgnoreCase("+1") && etMobileNumber.getText().toString().length() != 10)
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_valid_mobile_number),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else if (etEmail.getText().toString().trim().isEmpty())
        {

            new SnackbarUtils(main_layout, getString(R.string.please_enter_email),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else if (!isEmailValid(etEmail.getText().toString().trim()))
        {

            new SnackbarUtils(main_layout, getString(R.string.please_enter_valid_email),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else if (etPhone.getText().toString().trim().isEmpty())
        {

            new SnackbarUtils(main_layout, getString(R.string.please_enter_business_phone_number),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else
            {
            submitForm();
        }
    }

    public boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity != null) {
            TiCKTOC_Driver_Application.setCurrentActivity(activity);
        }
    }

    private void getCityName() {

        final InitData initData = SessionSave.getInitData(activity);
        GPSTracker gpsTracker = new GPSTracker(activity);
        /*if (gpsTracker.canGetLocation())
        {
            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
            List<Address> addresses = null;
            try {
                addresses = geocoder.getFromLocation(gpsTracker.getLatitude(), gpsTracker.getLongitude(), 1);
                //addresses = geocoder.getFromLocation(-37.946048, 145.085055, 1);
                cityName = addresses.get(0).getLocality();
                Log.e("TAG", "***** cityName = "+ cityName);
                if(initData != null && initData.getCityList() != null){
                    for(int  i = 0;i<initData.getCityList().size();i++){
                        if(initData.getCityList().get(i).getCityName().equalsIgnoreCase(cityName)){
                            cityId = initData.getCityList().get(i).getId();
                            break;
                        }
                    }
                }
                String stateName = addresses.get(0).getAddressLine(1);
                String countryName = addresses.get(0).getAddressLine(2);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {*/
        if (!Constants.newgpsLatitude.equalsIgnoreCase("") && !Constants.newgpsLongitude.equalsIgnoreCase("") && !Constants.newgpsLatitude.equalsIgnoreCase("0") && !Constants.newgpsLongitude.equalsIgnoreCase("0")) {

            String urlLocation = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + Constants.newgpsLatitude + "," + Constants.newgpsLongitude + "&key=" + getString(R.string.api_key_google_map);
            Log.w("urlLocation", "" + urlLocation);
            dialogClass = new DialogClass(activity, 1);
            dialogClass.showDialog();
            aQuery.ajax(urlLocation.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {

                    try {
                        int responseCode = status.getCode();
                        Log.e("TAGCityFind", "responseCode = " + responseCode);
                        Log.e("TAGCityFind", "Response = " + json);
                        dialogClass.hideDialog();

                        JSONArray resultsAry = json.getJSONArray("results");
                        JSONObject jObj = new JSONObject(resultsAry.getString(0));
                        JSONArray addCompAry = jObj.getJSONArray("address_components");
                        String short_name = "";
                        int idxCity = -1;

                        boolean isContainsCity = false;
                        boolean isFoundCity = false;

                        for (int adComp = 0; adComp < addCompAry.length(); adComp++) {
                            JSONObject cityObj = addCompAry.getJSONObject(adComp);
                            JSONArray typesAry = cityObj.getJSONArray("types");

                            for (int j = 0; j < typesAry.length(); j++) {
                                if (typesAry.get(j).toString().equalsIgnoreCase("administrative_area_level_2")) {
                                    isContainsCity = true;
                                    idxCity = adComp;
                                    break;
                                }
                            }
                        }

                        for (int adComp = 0; adComp < addCompAry.length(); adComp++) {
                            JSONObject cityObj = addCompAry.getJSONObject(adComp);
                            JSONArray typesAry = cityObj.getJSONArray("types");

                            if (typesAry.length() > 1) {
                                if (/*typesAry.get(0).toString().equalsIgnoreCase("administrative_area_level_2")*/isContainsCity && adComp == idxCity && typesAry.getString(1).equalsIgnoreCase("political")) {
                                    cityName = cityObj.getString("long_name");
                                    short_name = cityObj.getString("short_name");
                                    for (int i = 0; i < initData.getCityList().size(); i++) {
                                        String listCityName = initData.getCityList().get(i).getCityName().toLowerCase().trim();
                                        if (cityName.toLowerCase().trim().equalsIgnoreCase(listCityName)) {
                                            isFoundCity = true;
                                            cityId = initData.getCityList().get(i).getId();
                                            break;
                                        } else if (short_name.toLowerCase().trim().equalsIgnoreCase(listCityName)) {
                                            isFoundCity = true;
                                            cityName = short_name;
                                            cityId = initData.getCityList().get(i).getId();
                                            break;
                                        }
                                    }
                                    if (!cityId.equalsIgnoreCase(""))
                                        break;
                                } else if (typesAry.length() > 1) {
                                    if (!isContainsCity && typesAry.get(0).toString().equalsIgnoreCase("locality") && typesAry.getString(1).equalsIgnoreCase("political")) {
                                        // JSONObject cityObjFinal = new JSONObject(addCompAry.getString(1));
                                        cityName = cityObj.getString("long_name");
                                        short_name = cityObj.getString("short_name");
                                        for (int i = 0; i < initData.getCityList().size(); i++) {
                                            String listCityName = initData.getCityList().get(i).getCityName().toLowerCase().trim();
                                            if (cityName.toLowerCase().trim().equalsIgnoreCase(listCityName)) {
                                                isFoundCity = true;
                                                cityId = initData.getCityList().get(i).getId();
                                                break;
                                            } else if (short_name.toLowerCase().trim().equalsIgnoreCase(listCityName)) {
                                                isFoundCity = true;
                                                cityName = short_name;
                                                cityId = initData.getCityList().get(i).getId();
                                                break;
                                            }
                                        }
                                        if (!cityId.equalsIgnoreCase(""))
                                            break;
                                    }
                                }
                            }
                        }
                        if(!isFoundCity){
                            cityName = "Melbourne";
                            cityId = "4";
                        }

                        Log.w("resultAry", "" + cityName);
                    } catch (JSONException e) {
                        Log.e("Exception", "Exception " + e.toString());
                        dialogClass.hideDialog();

                    }
                }

            }.method(AQuery.METHOD_GET));

        }

        /*if (cityName == null || cityName.equalsIgnoreCase("")) {
            String ip="";

            ip = Utility.getIPAddress(true);
            Log.e("TAG", "***** IP = "+ ip);// IPv4
            Log.e("TAG", "***** IP = "+ Utility.getIPAddress(false));// IPv6
            String url = "http://ip-api.com/json/"+ip;
            Log.e("TAG","URL = "+url);
            dialogClass = new DialogClass(activity, 1);
            dialogClass.showDialog();
            aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {

                    try
                    {
                        int responseCode = status.getCode();
                        Log.e("TAG", "responseCode = " + responseCode);
                        Log.e("TAG", "Response = " + json);
                        dialogClass.hideDialog();
                        cityName = json.optString("city");
                        if(initData != null && initData.getCityList() != null){
                            for(int  i = 0;i<initData.getCityList().size();i++){
                                if(initData.getCityList().get(i).getCityName().equalsIgnoreCase(cityName)){
                                    cityId = initData.getCityList().get(i).getId();
                                    break;
                                }
                            }
                        }
                        //{"status":"success","country":"India","countryCode":"IN","region":"GJ","regionName":"Gujarat","city":"Ahmedabad","zip":"380001","lat":23.0276,"lon":72.5871,"timezone":"Asia\/Kolkata","isp":"Reliance Jio Infocomm Limited","org":"RJIL Gujarat LTE SUBSCRIBER PUBLIC","as":"AS55836 Reliance Jio Infocomm Limited","query":"157.32.236.138"}
                    }
                    catch (Exception e)
                    {
                        Log.e("Exception","Exception "+e.toString());
                        dialogClass.hideDialog();

                    }
                }

            }.method(AQuery.METHOD_GET));
        }*/
        //}
    }


    private void submitForm() {
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_SERVICE_PROVIDR_FORM;

        params.put(WebServiceAPI.PARAM_COMPANY_NAME, etCompanyName.getText().toString());
        params.put(WebServiceAPI.PARAM_ABN, etAbn.getText().toString());
        params.put(WebServiceAPI.PARAM_EMAIL_, etEmail.getText().toString());
//        params.put(WebServiceAPI.PARAM_MOBILE_NO_, etMobileNumber.getText().toString());
        params.put(WebServiceAPI.PARAM_MOBILE_NO_, codePicker.getSelectedCountryCodeWithPlus()+etMobileNumber.getText().toString().trim());
        params.put(WebServiceAPI.PARAM_PHYSICAL_ADDRESS, etAddress.getText().toString());
        params.put(WebServiceAPI.PARAM_CONTACT_NAME, etContactName.getText().toString());
        params.put(WebServiceAPI.PARAM_BUSINESS_PHONE_NUMBER, etPhone.getText().toString());
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_CITY_ID, cityId);

        Log.e("url", "UserSignIn = " + url);
        Log.e("param", "UserSignIn = " + params);


        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "UserSignIn = " + responseCode);
                    Log.e("Response", "UserSignIn = " + json);

                    dialogClass.hideDialog();
                    if (json != null) {
                        if (json.has("status")) {
                            if (json.getBoolean("status")) {
                                new SnackbarUtils(main_layout, json.getString("message"),
                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();

                                Log.e("status", "true");
                                etCompanyName.setText("");
                                etAddress.setText("");
                                etAbn.setText("");
                                etContactName.setText("");
                                etMobileNumber.setText("");
                                etEmail.setText("");
                                etPhone.setText("");
                            } else {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        } else {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message")) {
                                new SnackbarUtils(main_layout, json.getString("message"),
                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }
                    } else {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                    }
                } catch (Exception e) {
                    Log.e("UserSignIn", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }
}
