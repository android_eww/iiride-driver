package iiride.app.driver.Activity;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import iiride.app.driver.Application.TiCKTOC_Driver_Application;
import iiride.app.driver.R;


public class Driver_News_Detail_Activity extends AppCompatActivity implements View.OnClickListener{

    private static String TAG = Driver_News_Detail_Activity.class.getSimpleName();
    public static Driver_News_Detail_Activity activity;

    private TextView textTitle;
    private WebView webView;

    private ProgressBar progressBar;
    LinearLayout ll_back;
    String newsTitle , newsUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_news_detial);

        activity = Driver_News_Detail_Activity.this;

        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        textTitle = (TextView)findViewById(R.id.tv_title);

        init();
    }

    private void init()
    {
        webView = (WebView)findViewById(R.id.webView);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);

        Intent getIntent = getIntent();

        if (getIntent!=null)
        {
            if (getIntent.getStringExtra("newsTitle")!=null && !getIntent.getStringExtra("newsTitle").equalsIgnoreCase(""))
            {
                newsTitle = getIntent.getStringExtra("newsTitle");
            }
            else
            {
                newsTitle = "News Detials";
            }
            if (getIntent.getStringExtra("newsUrl")!=null && !getIntent.getStringExtra("newsUrl").equalsIgnoreCase(""))
            {
                newsUrl = getIntent.getStringExtra("newsUrl");
            }
            else
            {
                newsUrl = "";
            }
        }
        else {
            newsTitle = "News Detials";
            newsUrl = "";
        }

        Log.e(TAG,"newsTitle:"+newsTitle);
        Log.e(TAG,"newsUrl:"+newsUrl);

        textTitle.setText(newsTitle);

        if (newsUrl != null && !newsUrl.equalsIgnoreCase(""))
        {
            progressBar.setVisibility(View.VISIBLE);

            WebSettings webSettings = webView.getSettings();
            webSettings.setBuiltInZoomControls(false);
            webSettings.setCacheMode(webSettings.LOAD_NO_CACHE);
            webSettings.setJavaScriptEnabled(true);
            webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
            webView.setWebViewClient(new MyWebViewClient());

            webView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    progressBar.setVisibility(View.VISIBLE);
                    return true;
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    Toast.makeText(activity, "Error:" + description, Toast.LENGTH_SHORT).show();

                }
            });
            webView.loadUrl(newsUrl);

        }
        else
        {
            Toast.makeText(activity,"Please Try Agian",Toast.LENGTH_SHORT).show();
        }

        ll_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;
        }
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            progressBar.setVisibility(View.VISIBLE);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            System.out.println("on finish");
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        if (webView != null && webView.canGoBack()) {
            webView.goBack();
        }else {
            super.onBackPressed();
            overridePendingTransition(R.anim.left_in, R.anim.right_out);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            TiCKTOC_Driver_Application.setCurrentActivity(activity);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN)
        {
            switch (keyCode)
            {
                case KeyEvent.KEYCODE_BACK:
                    if (webView.canGoBack())
                    {
                        webView.goBack();
                    }
                    else
                    {
                        finish();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }
}
