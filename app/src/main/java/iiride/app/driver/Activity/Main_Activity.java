package iiride.app.driver.Activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import iiride.app.driver.Application.TiCKTOC_Driver_Application;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.Constants;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;
import iiride.app.driver.Others.GPSTracker;


import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.MyAlertDialog;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Main_Activity extends AppCompatActivity implements View.OnClickListener{

    public static Main_Activity activity;

    TextView tv_signIn, tv_registration, tv_ForgotPass;
    EditText et_email_login, et_password_login;

    LinearLayout main_layout;

    Intent intent;

    private AQuery aQuery;
    DialogClass dialogClass;

//    String userLatitude="50.605846", userLongitude="7.206919";

    GPSTracker gpsTracker;
    private static final int PERMISSION_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        activity = Main_Activity.this;

        aQuery = new AQuery(Main_Activity.this);
        dialogClass = new DialogClass(activity, 1);
        gpsTracker = new GPSTracker(Main_Activity.this);

        checkGPS();

        init();
    }

    public void checkGPS()
    {
        if (checkPermission())
        {
            if (gpsTracker.canGetLocation())
            {
                gpsTracker.getLocation();

                Constants.newgpsLatitude = gpsTracker.getLatitude()+"";
                Constants.newgpsLongitude = gpsTracker.getLongitude()+"";

                Log.e("latitude", "" + Constants.newgpsLatitude);
                Log.e("longitude", "" + Constants.newgpsLongitude);
            }
            else
            {
                gpsTracker.showSettingsAlert();
            }
        }
        else
        {
            requestPermission();
        }
    }

    public boolean checkPermission() {

        int result = ContextCompat.checkSelfPermission(Main_Activity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }

    public void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION))
        {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        } else {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        Log.e("call","onRequestPermissionsResult() 11 = "+requestCode);

        switch (requestCode)
        {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    Log.e("call","onRequestPermissionsResult() 22 = ");
                    if (gpsTracker.canGetLocation())
                    {
                        Log.e("call","onRequestPermissionsResult() 33 = ");
                        gpsTracker.getLocation();

                        Constants.newgpsLatitude = gpsTracker.getLatitude() + "";
                        Constants.newgpsLongitude = gpsTracker.getLongitude() + "";
                    }
                    else
                    {
                        Log.e("call","onRequestPermissionsResult() 44 = ");
//                        AlertMessageNoGps();
                    }
                }
                else
                {
                    Log.e("call","onRequestPermissionsResult() 55 = ");
                    MyAlertDialog dialog = new MyAlertDialog(Main_Activity.this);
                    dialog.setCancelable(false);
                    dialog.setAlertDialog(1, getResources().getString(R.string.permission_denied_for_location));
                }
                break;
        }
    }

    private void init() {

        main_layout = (LinearLayout) findViewById(R.id.main_layout);

        tv_signIn = (TextView) findViewById(R.id.tv_signIn);
        tv_registration = (TextView) findViewById(R.id.tv_registration);
        tv_ForgotPass = (TextView) findViewById(R.id.tv_ForgotPass);

        et_email_login = (EditText) findViewById(R.id.et_email_login);
        et_password_login = (EditText) findViewById(R.id.et_password_login);

        tv_signIn.setOnClickListener(this);
        tv_registration.setOnClickListener(this);
        tv_ForgotPass.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.tv_signIn:
                DriverLogin();
//                intent = new Intent(activity, Drawer_Activity.class);
//                startActivity(intent);
//                overridePendingTransition(R.anim.right_in, R.anim.left_out);
//                finish();
                break;

            case R.id.tv_registration:
                intent = new Intent(activity, Registration_Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                break;

            case R.id.tv_ForgotPass:
                intent = new Intent(activity, ForgotPassword_Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                break;

        }

    }

    private void DriverLogin()
    {
        String Token = SessionSave.getToken(Comman.DEVICE_TOKEN, activity);
        Log.e("Token","Token"+Token);
        if (Token!=null && !Token.equalsIgnoreCase(""))
        {
            if (TextUtils.isEmpty(et_email_login.getText().toString()))
            {
                new SnackbarUtils(main_layout, getString(R.string.please_enter_email),
                        ContextCompat.getColor(activity, R.color.snakbar_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
            }
            else if (TextUtils.isEmpty(et_password_login.getText().toString()))
            {
                new SnackbarUtils(main_layout, getString(R.string.please_enter_password),
                        ContextCompat.getColor(activity, R.color.snakbar_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
            }
            else if(!isEmailValid(et_email_login.getText().toString()))
            {
                new SnackbarUtils(main_layout, getString(R.string.please_enter_valid_email),
                        ContextCompat.getColor(activity, R.color.snakbar_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
            }
            else
            {
                gpsTracker = new GPSTracker(activity);
                if (!gpsTracker.canGetLocation())
                {
                    gpsTracker.showSettingsAlert();
                }
                else
                {
                    gpsTracker.getLocation();
                    Constants.newgpsLatitude = gpsTracker.getLatitude()+"";
                    Constants.newgpsLongitude = gpsTracker.getLongitude()+"";

//                Log.e("getLatitude()", "rrrrrrrrrrrrrr" + gpsTracker.getLatitude());
//                Log.e("getLongitude()", "rrrrrrrrrrrrrr" + gpsTracker.getLongitude());
//                Log.e("latitude", "rrrrrrrrrrrrrr" + Constants.newgpsLatitude);
//                Log.e("longitude", "rrrrrrrrrrrrr" + Constants.newgpsLongitude);

                    UserSignIn();
                }
            }
        } else {
            new SnackbarUtils(main_layout, getString(R.string.please_restart_app),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }

    }

    private void UserSignIn()
    {
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_LOGIN;

        params.put(WebServiceAPI.LOGIN_PARAM_DEVICE_TYPE, Comman.DEVICE_TYPE);
        params.put(WebServiceAPI.LOGIN_PARAM_TOKEN, SessionSave.getToken(Comman.DEVICE_TOKEN, activity));
        params.put(WebServiceAPI.LOGIN_PARAM_USERNAME, et_email_login.getText().toString());
        params.put(WebServiceAPI.LOGIN_PARAM_PASSWORD, et_password_login.getText().toString());
        params.put(WebServiceAPI.LOGIN_PARAM_LATITUDE, Constants.newgpsLatitude);
        params.put(WebServiceAPI.LOGIN_PARAM_LONGITUDE, Constants.newgpsLongitude);

        Log.e("url", "UserSignIn = " + url);
        Log.e("param", "UserSignIn = " + params);


        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "UserSignIn = " + responseCode);
                    Log.e("Response", "UserSignIn = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");
                                if (json.has("driver"))
                                {
                                    JSONObject driverObject = json.getJSONObject("driver");
                                    if (driverObject!=null)
                                    {
                                        if (driverObject.has("profile"))
                                        {
                                            SessionSave.saveUserSession(Comman.USER_PREFERENCE_KEY_TICK_PAY_SPLASH,"0",activity);

                                            JSONObject driverProfile = driverObject.getJSONObject("profile");
                                            if (driverProfile!=null)
                                            {
                                                String DriverId="", CompanyId="", DispatcherId="", Email="", FullName="", MobileNo="", Gender="", Image="", QRCode="", Password="", Address="", SubUrb = ""
                                                        , City="", State="", Country="", Zipcode="", ReferralCode="", DriverLicense="", AccreditationCertificate="", DriverLicenseExpire=""
                                                        , AccreditationCertificateExpire="", BankHolderName, BankName="", BankAcNo="", BSB="", Lat="", Lng="", Status=""
                                                        , Availability="", DriverDuty="", ABN="", serviceDescription="", DCNumber="", ProfileComplete="", CategoryId="", Balance="", ReferralAmount="";
                                                if (driverProfile.has("Id"))
                                                {
                                                    DriverId = driverProfile.getString("Id");
                                                    SessionSave.saveUserSession(Comman.USER_ID, DriverId, activity);
                                                }
                                                if (driverProfile.has("CompanyId"))
                                                {
                                                    CompanyId = driverProfile.getString("CompanyId");
                                                    SessionSave.saveUserSession(Comman.USER_COMPANY_ID, CompanyId, activity);
                                                }
                                                if (driverProfile.has("CityId"))
                                                {
                                                    SessionSave.saveUserSession(Comman.USER_CITY_ID, driverProfile.getString("CityId"), activity);
                                                }
                                                if (driverProfile.has("DispatcherId"))
                                                {
                                                    DispatcherId = driverProfile.getString("DispatcherId");
                                                    SessionSave.saveUserSession(Comman.USER_DISPATHER_ID, DispatcherId, activity);
                                                }
                                                if (driverProfile.has("Email"))
                                                {
                                                    Email = driverProfile.getString("Email");
                                                    SessionSave.saveUserSession(Comman.USER_EMAIL, Email, activity);
                                                }
                                                if (driverProfile.has("Fullname"))
                                                {
                                                    FullName = driverProfile.getString("Fullname");
                                                    SessionSave.saveUserSession(Comman.USER_FULL_NAME, FullName, activity);
                                                }


                                                if (driverProfile.has("MobileNo"))
                                                {
                                                    MobileNo = driverProfile.getString("MobileNo");
                                                    SessionSave.saveUserSession(Comman.USER_MOBILE_NUMBER, MobileNo, activity);
                                                }
                                                if (driverProfile.has("Gender"))
                                                {
                                                    Gender = driverProfile.getString("Gender");
                                                    SessionSave.saveUserSession(Comman.USER_GENDER, Gender, activity);
                                                }
                                                if (driverProfile.has("Image"))
                                                {
                                                    Image = driverProfile.getString("Image");
                                                    SessionSave.saveUserSession(Comman.USER_PROFILE_IMAGE, Image, activity);
                                                }
                                                if (driverProfile.has("QRCode"))
                                                {
                                                    QRCode = driverProfile.getString("QRCode");
                                                    SessionSave.saveUserSession(Comman.USER_QR_CODE, QRCode, activity);
                                                }
                                                if (driverProfile.has("Password"))
                                                {
                                                    Password = driverProfile.getString("Password");
                                                    SessionSave.saveUserSession(Comman.USER_PASSWORD, Password, activity);
                                                }
                                                if (driverProfile.has("Address"))
                                                {
                                                    Address = driverProfile.getString("Address");
                                                    SessionSave.saveUserSession(Comman.USER_RESIDENTIAL_ADDRESS, Address, activity);
                                                }
                                                if (driverProfile.has("SubUrb"))
                                                {
                                                    SubUrb = driverProfile.getString("SubUrb");
                                                    SessionSave.saveUserSession(Comman.USER_SUB_URB, SubUrb, activity);
                                                }


                                                if (driverProfile.has("City"))
                                                {
                                                    City = driverProfile.getString("City");
                                                    SessionSave.saveUserSession(Comman.USER_CITY, City, activity);
                                                }
                                                if (driverProfile.has("State"))
                                                {
                                                    State = driverProfile.getString("State");
                                                    SessionSave.saveUserSession(Comman.USER_STATE, State, activity);
                                                }
                                                if (driverProfile.has("Country"))
                                                {
                                                    Country = driverProfile.getString("Country");
                                                    SessionSave.saveUserSession(Comman.USER_COUNTRY, Country, activity);
                                                }
                                                if (driverProfile.has("ZipCode"))
                                                {
                                                    Zipcode = driverProfile.getString("ZipCode");
                                                    SessionSave.saveUserSession(Comman.USER_POST_CODE, Zipcode, activity);
                                                }
                                                if (driverProfile.has("ReferralCode"))
                                                {
                                                    ReferralCode = driverProfile.getString("ReferralCode");
                                                    SessionSave.saveUserSession(Comman.USER_REFERRAL_CODE, ReferralCode, activity);
                                                }

                                                if (driverProfile.has("DriverLicense"))
                                                {
                                                    DriverLicense = driverProfile.getString("DriverLicense");
                                                    SessionSave.saveUserSession(Comman.USER_DRIVER_LICENCE, DriverLicense, activity);
                                                }
                                                if (driverProfile.has("AccreditationCertificate"))
                                                {
                                                    AccreditationCertificate = driverProfile.getString("AccreditationCertificate");
                                                    SessionSave.saveUserSession(Comman.USER_ACCREDITATION_CERTY, AccreditationCertificate, activity);
                                                }
                                                if (driverProfile.has("DriverLicenseExpire"))
                                                {
                                                    DriverLicenseExpire = driverProfile.getString("DriverLicenseExpire");
                                                    SessionSave.saveUserSession(Comman.USER_DRIVER_lICENSE_EXPIRE, DriverLicenseExpire, activity);
                                                }
                                                if (driverProfile.has("AccreditationCertificateExpire"))
                                                {
                                                    AccreditationCertificateExpire = driverProfile.getString("AccreditationCertificateExpire");
                                                    SessionSave.saveUserSession(Comman.USER_ACCREDITATION_CERTY_EXPIRE, AccreditationCertificateExpire, activity);
                                                }
                                                if (driverProfile.has("BankHolderName"))
                                                {
                                                    BankHolderName = driverProfile.getString("BankHolderName");
                                                    SessionSave.saveUserSession(Comman.USER_BANK_ACCOUNT_HOLDER_NAME, BankHolderName, activity);
                                                }
                                                if (driverProfile.has("BankName"))
                                                {
                                                    BankName = driverProfile.getString("BankName");
                                                    SessionSave.saveUserSession(Comman.USER_BANK_NAME, BankName, activity);
                                                }



                                                if (driverProfile.has("BankAcNo"))
                                                {
                                                    BankAcNo = driverProfile.getString("BankAcNo");
                                                    SessionSave.saveUserSession(Comman.USER_BANK_AC_NO, BankAcNo, activity);
                                                }
                                                if (driverProfile.has("BSB"))
                                                {
                                                    BSB = driverProfile.getString("BSB");
                                                    SessionSave.saveUserSession(Comman.USER_BSB, BSB, activity);
                                                }
                                                if (driverProfile.has("Lat"))
                                                {
                                                    Lat = driverProfile.getString("Lat");
                                                    SessionSave.saveUserSession(Comman.USER_LATITUDE, Lat, activity);
                                                }
                                                if (driverProfile.has("Lng"))
                                                {
                                                    Lng = driverProfile.getString("Lng");
                                                    SessionSave.saveUserSession(Comman.USER_LONGITUDE, Lng, activity);
                                                }
                                                if (driverProfile.has("Status"))
                                                {
                                                    Status = driverProfile.getString("Status");
                                                    SessionSave.saveUserSession(Comman.USER_STATE, Status, activity);
                                                }
                                                if (driverProfile.has("Availability"))
                                                {
                                                    Availability = driverProfile.getString("Availability");
                                                    SessionSave.saveUserSession(Comman.USER_AVAILABILITY, Availability, activity);
                                                }



                                                if (driverProfile.has("DriverDuty"))
                                                {
                                                    DriverDuty = driverProfile.getString("DriverDuty");
                                                    SessionSave.saveUserSession(Comman.USER_DRIVER_DUTY, DriverDuty, activity);
                                                }
                                                if (driverProfile.has("ABN"))
                                                {
                                                    ABN = driverProfile.getString("ABN");
                                                    SessionSave.saveUserSession(Comman.USER_ABN, ABN, activity);
                                                }
                                                if (driverProfile.has("Description"))
                                                {
                                                    serviceDescription = driverProfile.getString("Description");
                                                    SessionSave.saveUserSession(Comman.USER_SERVICE_DESCRIPTION, serviceDescription, activity);
                                                }
                                                if (driverProfile.has("DCNumber"))
                                                {
                                                    DCNumber = driverProfile.getString("DCNumber");
                                                    SessionSave.saveUserSession(Comman.USER_DC_NUMBER, DCNumber, activity);
                                                }
                                                if (driverProfile.has("ProfileComplete"))
                                                {
                                                    ProfileComplete = driverProfile.getString("ProfileComplete");
                                                    SessionSave.saveUserSession(Comman.USER_PROFILE_COMPLETE, ProfileComplete, activity);
                                                }

                                                if (driverProfile.has("CategoryId"))
                                                {
                                                    CategoryId = driverProfile.getString("CategoryId");
                                                    SessionSave.saveUserSession(Comman.USER_PROFILE_CATEGORY_ID, CategoryId, activity);
                                                }
                                                if (driverProfile.has("Balance"))
                                                {
                                                    Balance = driverProfile.getString("Balance");
                                                    SessionSave.saveUserSession(Comman.DRIVER_WALLET_BALLENCE, Balance, activity);
                                                }
                                                if (driverProfile.has("ReferralAmount"))
                                                {
                                                    ReferralAmount = driverProfile.getString("ReferralAmount");
                                                    SessionSave.saveUserSession(Comman.USER_PROFILE_REFERRAL_AMOUNT, ReferralAmount, activity);
                                                }


                                                if (driverProfile.has("Vehicle"))
                                                {
                                                    JSONObject driverVehicle = driverProfile.getJSONObject("Vehicle");
                                                    if (driverVehicle!=null)
                                                    {
                                                        String VehicleId, VehicleModel, CarCompany, CarColor, VehicleRegistrationNo, RegistrationCertificate, VehicleInsuranceCertificate
                                                                , RegistrationCertificateExpire, VehicleInsuranceCertificateExpire, VehicleImage, Description, VehicleModelName;

                                                        if (driverVehicle.has("Id"))
                                                        {
                                                            VehicleId = driverVehicle.getString("Id");
                                                            SessionSave.saveUserSession(Comman.USER_VEHICLE_ID, VehicleId, activity);
                                                        }
                                                        if (driverVehicle.has("VehicleModel"))
                                                        {
                                                            VehicleModel = driverVehicle.getString("VehicleModel");
                                                            SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL, VehicleModel, activity);
                                                        }
                                                        if (driverVehicle.has("Company"))
                                                        {
                                                            CarCompany = driverVehicle.getString("Company");
                                                            SessionSave.saveUserSession(Comman.USER_CAR_COMPANY, CarCompany, activity);
                                                        }
                                                        if (driverVehicle.has("Color"))
                                                        {
                                                            CarColor = driverVehicle.getString("Color");
                                                            SessionSave.saveUserSession(Comman.USER_CAR_COLOR, CarColor, activity);
                                                        }
                                                        if (driverVehicle.has("VehicleRegistrationNo"))
                                                        {
                                                            VehicleRegistrationNo = driverVehicle.getString("VehicleRegistrationNo");
                                                            SessionSave.saveUserSession(Comman.USER_VEHICLE_REGISTRATION_NO, VehicleRegistrationNo, activity);
                                                        }


                                                        if (driverVehicle.has("RegistrationCertificate"))
                                                        {
                                                            RegistrationCertificate = driverVehicle.getString("RegistrationCertificate");
                                                            SessionSave.saveUserSession(Comman.USER_REGISTRATION_CERTY, RegistrationCertificate, activity);
                                                        }
                                                        if (driverVehicle.has("VehicleInsuranceCertificate"))
                                                        {
                                                            VehicleInsuranceCertificate = driverVehicle.getString("VehicleInsuranceCertificate");
                                                            SessionSave.saveUserSession(Comman.USER_VEHICLE_INSURANCE_CERTY, VehicleInsuranceCertificate, activity);
                                                        }
                                                        if (driverVehicle.has("RegistrationCertificateExpire"))
                                                        {
                                                            RegistrationCertificateExpire = driverVehicle.getString("RegistrationCertificateExpire");
                                                            SessionSave.saveUserSession(Comman.USER_REGISTRATION_CERTY_EXPIRE, RegistrationCertificateExpire, activity);
                                                        }
                                                        if (driverVehicle.has("VehicleInsuranceCertificateExpire"))
                                                        {
                                                            VehicleInsuranceCertificateExpire = driverVehicle.getString("VehicleInsuranceCertificateExpire");
                                                            SessionSave.saveUserSession(Comman.USER_VEHICLE_INSURANCE_CERTY_EXPIRE, VehicleInsuranceCertificateExpire, activity);
                                                        }
                                                        if (driverVehicle.has("VehicleImage"))
                                                        {
                                                            VehicleImage = driverVehicle.getString("VehicleImage");
                                                            SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE, VehicleImage, activity);
                                                        }

                                                        if (driverVehicle.has("Description"))
                                                        {
                                                            Description = driverVehicle.getString("Description");
                                                            SessionSave.saveUserSession(Comman.USER_CAR_DESCRIPTION, Description, activity);
                                                        }
                                                        if (driverVehicle.has("VehicleClass"))
                                                        {
                                                            VehicleModelName = driverVehicle.getString("VehicleClass");
                                                            SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL_NAME, VehicleModelName, activity);

                                                            Log.e("VehicleModelName","VehicleModelName : "+VehicleModelName);
                                                            if (VehicleModelName!=null && VehicleModelName.contains("First Class"))
                                                            {
                                                                SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "First Class", activity);
                                                            }
                                                            else if (VehicleModelName.contains("Business Class"))
                                                            {
                                                                SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", activity);
                                                            }
                                                            else if (VehicleModelName.contains("Economy"))
                                                            {
                                                                SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Economy", activity);
                                                            }
                                                            else if (VehicleModelName.contains("Taxi"))
                                                            {
                                                                SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Taxi", activity);
                                                            }
                                                            else if (VehicleModelName.contains("LUX-VAN"))
                                                            {
                                                                SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "LUX-VAN", activity);
                                                            }
                                                            else if (VehicleModelName.contains("Disability"))
                                                            {
                                                                SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Disability", activity);
                                                            }
                                                            else
                                                            {
                                                                SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", activity);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", activity);
                                                        }
                                                    }
                                                    Log.e("GGGGGGGGGGG","GGGGGGGGGGG :"+SessionSave.getUserSession(Comman.USER_ID, activity));

                                                    SessionSave.saveUserSession(Comman.LOGIN_FLAG_USER,"1",activity);
                                                    SessionSave.saveUserSession(Comman.CREATED_PASSCODE,"",activity);
                                                    intent = new Intent(activity, Drawer_Activity.class);
                                                    startActivity(intent);
                                                    overridePendingTransition(R.anim.right_in, R.anim.left_out);
                                                    dialogClass.hideDialog();
                                                    finish();
                                                }
                                                else
                                                {
                                                    dialogClass.hideDialog();
                                                    if (json.has("message")) {
                                                        new SnackbarUtils(main_layout, json.getString("message"),
                                                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                                    }
                                                }

                                            }
                                            else
                                            {
                                                dialogClass.hideDialog();
                                                if (json.has("message")) {
                                                    new SnackbarUtils(main_layout, json.getString("message"),
                                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                                }
                                            }
                                        }
                                        else
                                        {
                                            dialogClass.hideDialog();
                                            if (json.has("message")) {
                                                new SnackbarUtils(main_layout, json.getString("message"),
                                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Log.e("DRIVER", "NULL");
                                        dialogClass.hideDialog();
                                        if (json.has("message")) {
                                            new SnackbarUtils(main_layout, json.getString("message"),
                                                    ContextCompat.getColor(activity, R.color.snakbar_color),
                                                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                        }
                                    }
                                }
                                else
                                {
                                    Log.e("DRIVER", "NO");
                                    dialogClass.hideDialog();
                                    if (json.has("message")) {
                                        new SnackbarUtils(main_layout, json.getString("message"),
                                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                    }
                                }
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message")) {
                                new SnackbarUtils(main_layout, json.getString("message"),
                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                    }
                } catch (Exception e) {
                    Log.e("UserSignIn", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }


    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            TiCKTOC_Driver_Application.setCurrentActivity(activity);
        }
    }
}
