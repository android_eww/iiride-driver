package iiride.app.driver.Activity;

import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import iiride.app.driver.Adapter.DriverNewsAdapter;
import iiride.app.driver.Application.TiCKTOC_Driver_Application;
import iiride.app.driver.Been.DriverNewsBeen;
import iiride.app.driver.Others.ConnectivityReceiver;


import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class Driver_News_Activity extends AppCompatActivity implements View.OnClickListener {

    private static String TAG = Driver_News_Activity.class.getSimpleName();
    private Driver_News_Activity activity;

    private AQuery aQuery;
    private RecyclerView recyclerView;
    LinearLayout ll_back, main_layout;

    private List<DriverNewsBeen> driverNewsArList = new ArrayList<DriverNewsBeen>();
    private DriverNewsAdapter driverNewsAdapter;
    private LinearLayoutManager layoutManager;

    private String urlNews, newsUrl, apiKey;

    private ProgressBar progressBar;
    DialogClass dialogClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_news);

        activity = Driver_News_Activity.this;
        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 1);

        main_layout = (LinearLayout) findViewById(R.id.main_layout);

        init();
    }

    private void init()
    {
        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);


        driverNewsAdapter = new DriverNewsAdapter(driverNewsArList,activity);
        layoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(driverNewsAdapter);

        ll_back.setOnClickListener(this);

        if (ConnectivityReceiver.isConnected())
        {
            getAllNews();
        }
        else
        {
            new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
    }

    private void getAllNews()
    {
        progressBar.setVisibility(View.VISIBLE);
        newsUrl = "https://newsapi.org/v2/top-headlines?sources=google-news&apiKey=";
        apiKey = "90727bb768584fd7b64b66c9190921e0";
        urlNews = newsUrl  + apiKey;

        Log.e(TAG,"URL:"+urlNews);
        aQuery.ajax(urlNews, null, JSONObject.class, new AjaxCallback<JSONObject>()
        {
            @Override
            public void callback(String urlNews, JSONObject json, AjaxStatus status)
            {
                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG, "responseCode:" + responseCode);
                    Log.e(TAG, "Response:" + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getString("status").equalsIgnoreCase("ok"))
                            {
                                Log.e(TAG,"status ok");
                                if (json.has("articles"))
                                {
                                    progressBar.setVisibility(View.GONE);
                                    JSONArray drivernewsArray = json.getJSONArray("articles");

                                    if (drivernewsArray != null && drivernewsArray.length()>0)
                                    {
                                        Log.e(TAG,"drivernewsArray" + drivernewsArray);
                                        Log.e(TAG,"drivernewsArrayLength" + drivernewsArray.length());

                                        for (int i = 0; i < drivernewsArray.length(); i++)
                                        {
                                            JSONObject drivernewsObject = drivernewsArray.getJSONObject(i);
                                            Log.e(TAG,"drivernewsObject" + drivernewsObject);

                                            if (drivernewsObject != null)
                                            {
                                                String title="", description="", url="", urlToImage="";

                                                if (drivernewsObject.has("title"))
                                                {
                                                    title = drivernewsObject.getString("title");
                                                }
                                                if (drivernewsObject.has("description"))
                                                {
                                                    description = drivernewsObject.getString("description");
                                                }
                                                if (drivernewsObject.has("url"))
                                                {
                                                    url = drivernewsObject.getString("url");
                                                }
                                                if (drivernewsObject.has("urlToImage"))
                                                {
                                                    urlToImage = drivernewsObject.getString("urlToImage");
                                                }

                                                driverNewsArList.add(new DriverNewsBeen(title, description, url, urlToImage));

                                            }
                                            else
                                            {
                                                Log.e(TAG,"drivernewsObject Null");
                                                Log.e("status", "false");
                                                dialogClass.hideDialog();
                                                if (json.has("message")) {
                                                    new SnackbarUtils(main_layout, json.getString("message"),
                                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                                }
                                            }
                                        }

                                        driverNewsAdapter.notifyDataSetChanged();

                                    }
                                    else
                                    {
                                        progressBar.setVisibility(View.GONE);
                                        Log.e(TAG,"drivernewsArray Null");
                                        Log.e("status", "false");
                                        dialogClass.hideDialog();
                                        if (json.has("message")) {
                                            new SnackbarUtils(main_layout, json.getString("message"),
                                                    ContextCompat.getColor(activity, R.color.snakbar_color),
                                                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                        }
                                    }
                                }
                                else
                                {
                                    progressBar.setVisibility(View.GONE);
                                    String message = "please try Again Leter";
                                    if (json.has("message"))
                                    {
                                        message = json.getString("message");

                                    }
                                    Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
                                    Log.e("status", "false");
                                    dialogClass.hideDialog();
                                    if (json.has("message")) {
                                        new SnackbarUtils(main_layout, json.getString("message"),
                                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                    }
                                }
                            }
                            else
                            {
                                progressBar.setVisibility(View.GONE);
                                String message = "please try Again Leter";
                                if (json.has("message"))
                                {
                                    message = json.getString("message");

                                }
                                Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }

                        }
                        else
                        {
                            progressBar.setVisibility(View.GONE);
                            Log.e(TAG,"no status found");
                            Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                }
                        }

                    }
                    else
                    {
                        progressBar.setVisibility(View.GONE);
                        Log.e(TAG, "Null Json");
                    dialogClass.hideDialog();
                            new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                    }
                }
                catch (Exception e)
                {
                    progressBar.setVisibility(View.GONE);
                    Log.e(TAG, e.getMessage() + "somthig_went_wrong");
                    dialogClass.hideDialog();
                            new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            TiCKTOC_Driver_Application.setCurrentActivity(activity);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Drawer_Activity.flagResumePass=0;
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }
}
