package iiride.app.driver.Activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import iiride.app.driver.Adapter.CreditCardList_Adapter;
import iiride.app.driver.Application.TiCKTOC_Driver_Application;
import iiride.app.driver.Been.CreditCard_List_Been;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;
import iiride.app.driver.Others.ConnectivityReceiver;


import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Wallet_Cards_Activity extends AppCompatActivity implements View.OnClickListener{

    public static Wallet_Cards_Activity activity;

    LinearLayout main_layout, ll_back, ll_add_card;
    ImageView iv_back;

    private RecyclerView recyclerView;
    private CreditCardList_Adapter adapter;
    public static List<CreditCard_List_Been> cardList = new ArrayList<>();

    private AQuery aQuery;
    DialogClass dialogClass;

    Intent intentExtra;
    public static String from="";

    public static int resumeFlag=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_add_card);

        activity = Wallet_Cards_Activity.this;
        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 1);

        intentExtra = getIntent();

        from="";
        if (intentExtra!=null)
        {
            from = intentExtra.getStringExtra("from");
        }
        else
        {
            from="";
        }
        resumeFlag=1;

        Log.e("from","from : "+from);
        /*if (from!=null && from.equalsIgnoreCase("drawer"))
        {
            resumeFlag=0;
        }
        else
        {
            resumeFlag=1;
        }*/

        initUI();
    }

    private void initUI()
    {
        main_layout = (LinearLayout) findViewById(R.id.main_layout);
        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        ll_add_card = (LinearLayout) findViewById(R.id.ll_add_card);
        iv_back = (ImageView) findViewById(R.id.iv_back);

        recyclerView = (RecyclerView) findViewById(R.id.rv_CreditCard);
        adapter = new CreditCardList_Adapter(activity, cardList, main_layout);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        ll_back.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        ll_add_card.setOnClickListener(this);

        String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
        String cardCount = SessionSave.getUserSession(Comman.DRIVER_CARD_COUNT_ADDED_IN_LIST, activity);

        if (cardCount!=null && !cardCount.equalsIgnoreCase(""))
        {
            if (Integer.parseInt(cardCount)==0)
            {
                OpenNextActivity();
            }
            else
            {
                if (Drawer_Activity.callCardListMethod==0)
                {
                    if (driverId != null && !driverId.equalsIgnoreCase(""))
                    {
                        if (ConnectivityReceiver.isConnected())
                        {
                            GetCardList(driverId);
                        }
                        else
                        {
                            new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                                    ContextCompat.getColor(activity, R.color.snakbar_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                        }
                    }
                }
            }
        }
        else
        {
            if (driverId!=null && !driverId.equalsIgnoreCase(""))
            {
                if (ConnectivityReceiver.isConnected())
                {
                    GetCardList(driverId);
                }
                else
                {
                    new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.ll_add_card:
                OpenNextActivity();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Drawer_Activity.flagResumePass=0;
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void OpenNextActivity()
    {
        resumeFlag=0;
        Intent i =  new Intent(activity, Add_Card_In_List_Activity.class);
        startActivity(i);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();

        if (activity!=null)
        {
            TiCKTOC_Driver_Application.setCurrentActivity(activity);
        }
    }

    private void GetCardList(String driverId)
    {
        cardList.clear();
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_DABITCARD_LIST + driverId;

        Log.e("url", "GetCardList = " + url);
        Log.e("param", "GetCardList = " + params);


        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "GetCardList = " + responseCode);
                    Log.e("Response", "GetCardList = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");
                                dialogClass.hideDialog();
                                if (json.has("cards"))
                                {
                                    String cards = json.getString("cards");
                                    if (cards!=null && !cards.equalsIgnoreCase(""))
                                    {
                                        JSONArray cardsArray = json.getJSONArray("cards");
                                        if (cardsArray!=null && cardsArray.length()>0)
                                        {
                                            for (int i=0; i<cardsArray.length(); i++)
                                            {
                                                String Id = "", CardNum="", CardNum2="", Type="", Alias="";

                                                JSONObject cardObj = cardsArray.getJSONObject(i);
                                                if (cardObj.has("Id"))
                                                {
                                                    Id = cardObj.getString("Id");
                                                }
                                                if (cardObj.has("CardNum"))
                                                {
                                                    CardNum = cardObj.getString("CardNum");
                                                }
                                                if (cardObj.has("CardNum2"))
                                                {
                                                    CardNum2 = cardObj.getString("CardNum2");
                                                }
                                                if (cardObj.has("Type"))
                                                {
                                                    Type = cardObj.getString("Type");
                                                }
                                                if (cardObj.has("Alias"))
                                                {
                                                    Alias = cardObj.getString("Alias");
                                                }
                                                Log.e("GetCardList","Id : "+Id+"\nCardNum : "+CardNum+"\nCardNum2 : "+CardNum2+"\nType : "+Type+"\nAlias : "+Alias);
                                                cardList.add(new CreditCard_List_Been(Id, Alias, CardNum2, Type));
                                            }
                                            SessionSave.saveUserSession(Comman.DRIVER_CARD_COUNT_ADDED_IN_LIST, cardList.size()+"", activity);
                                            if (cardList.size()>0)
                                            {
                                                adapter.notifyDataSetChanged();
                                            }
                                            else
                                            {
                                                OpenNextActivity();
                                            }
                                            Drawer_Activity.callCardListMethod=1;
                                        }
                                        else
                                        {
                                            Log.e("cardsArray", "null");
                                            SessionSave.saveUserSession(Comman.DRIVER_CARD_COUNT_ADDED_IN_LIST, "0", activity);
                                            dialogClass.hideDialog();
                                            Drawer_Activity.callCardListMethod=1;
                                            if (json.has("message")) {
                                                new SnackbarUtils(main_layout, json.getString("message"),
                                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                            }
                                            OpenNextActivity();
                                        }
                                    }
                                    else
                                    {
                                        Log.e("cards", "null");
                                        dialogClass.hideDialog();
                                        Drawer_Activity.callCardListMethod=1;
                                        SessionSave.saveUserSession(Comman.DRIVER_CARD_COUNT_ADDED_IN_LIST, "0", activity);
                                        if (json.has("message")) {
                                            new SnackbarUtils(main_layout, json.getString("message"),
                                                    ContextCompat.getColor(activity, R.color.snakbar_color),
                                                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                        }
                                        OpenNextActivity();
                                    }
                                }
                                else
                                {
                                    Log.e("cards", "noy found");
                                    dialogClass.hideDialog();
                                    Drawer_Activity.callCardListMethod=1;
                                    SessionSave.saveUserSession(Comman.DRIVER_CARD_COUNT_ADDED_IN_LIST, "0", activity);
                                    if (json.has("message")) {
                                        new SnackbarUtils(main_layout, json.getString("message"),
                                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                    }
                                    OpenNextActivity();
                                }
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                SessionSave.saveUserSession(Comman.DRIVER_CARD_COUNT_ADDED_IN_LIST, "0", activity);
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            SessionSave.saveUserSession(Comman.DRIVER_CARD_COUNT_ADDED_IN_LIST, "0", activity);
                            if (json.has("message")) {
                                new SnackbarUtils(main_layout, json.getString("message"),
                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        SessionSave.saveUserSession(Comman.DRIVER_CARD_COUNT_ADDED_IN_LIST, "0", activity);
                        new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();

                    }
                }
                catch (Exception e)
                {
                    Log.e("GetCardList", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    SessionSave.saveUserSession(Comman.DRIVER_CARD_COUNT_ADDED_IN_LIST, "0", activity);
                    new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }.header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (Wallet_Cards_Activity.from!=null && Wallet_Cards_Activity.from.equalsIgnoreCase("drawer"))
        {

        }
        else
        {
            if (resumeFlag==1)
            {
                if (SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity) != null && !SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("")
                        && SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED,activity).equalsIgnoreCase("1"))
                {
                    resumeFlag = 0;
                    Intent intent = new Intent(activity,Create_Passcode_Activity.class);
                    startActivity(intent);
                    if (Wallet_Balance_Activity.activity!=null)
                    {
                        Wallet_Balance_Activity.activity.finish();
                    }
                    if (Wallet_Balance_TopUp_Activity.activity!=null)
                    {
                        Wallet_Balance_TopUp_Activity.activity.finish();
                    }
                    if (Wallet_Balance_TransferToBank_Activity.activity!=null)
                    {
                        Wallet_Balance_TransferToBank_Activity.activity.finish();
                    }
                    if (Wallet_Transfer_Activity.activity!=null)
                    {
                        Wallet_Transfer_Activity.activity.finish();
                    }
                    if (Wallet_Transfer_History_Activity.activity!=null)
                    {
                        Wallet_Transfer_History_Activity.activity.finish();
                    }
                    finish();
                }
            }
            else
            {
                resumeFlag=1;
            }
        }
    }
}