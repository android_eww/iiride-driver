package iiride.app.driver.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import iiride.app.driver.Application.TiCKTOC_Driver_Application;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;
import iiride.app.driver.Others.ConnectivityReceiver;


import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class Profile_DocumentDetail_Activity extends AppCompatActivity implements View.OnClickListener{

    Profile_DocumentDetail_Activity activity;

    TextView  tv_driver_licence_expiry, tv_accreditation_expiry, tv_carRegistration_expiry, tv_vehicle_Insurance_expiry;
    LinearLayout ll_driver_licence, ll_acrreditation_certy, ll_car_registration_certy, ll_vehicle_Insurance, ll_save, ll_back, main_layout;
    ImageView iv_driverLicence, iv_accreditation_certy, iv_car_Registration, iv_vehicle_insurance;
    String ImageViewClicked = "", DRIVER_LICENCE="driverLicence", ACRREDITATION= "accreditation_certy", CAR_REGISTRATION="car_Registration", VEHICLE_INSURANCE="vehicle_insurance"
            , CAR_IMAGE="driver_Image";
    String chooseCamera="camera", chooseGallery="gallery", userChoosenTask="";
    RelativeLayout rl_carImage;

    Intent intent;

    private static final String IMAGE_DIRECTORY = "/TickToc";
    private int GALLERY = 1, CAMERA = 0;
    private static final int MY_REQUEST_CODE_CAMERA = 101;
    private static final int MY_REQUEST_CODE_STORAGE = 100;

    Bitmap finalImageBitmap, oldBitmap;

    byte[] DriverLicence_ByteImage=null, Accreditation_certy_ByteImage=null, Car_Registration_ByteImage=null, Vehicle_insurance_ByteImage=null, CarImage_ByteImage=null;

    DialogClass dialogClass;
    AQuery aQuery;

    ProgressBar progressbar;


    private DatePickerDialog fromDatePickerDialog;
    private SimpleDateFormat dateFormatter;

    ImageView iv_back, iv_save, iv_carImage;

    Transformation mTransformation;

    Uri mImageUri;
    private Uri imageUri;
    Bitmap bitmapImage;
    String picturePath;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_document_detail);

        mImageUri=null;
        imageUri=null;
        bitmapImage=null;
        picturePath=null;

        activity = Profile_DocumentDetail_Activity.this;

        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 1);

        main_layout = (LinearLayout) findViewById(R.id.main_layout);

        initUI();
    }

    private void initUI() {

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);


        iv_carImage = (ImageView) findViewById(R.id.iv_carImage);
        iv_save = (ImageView) findViewById(R.id.iv_save);
        ll_save = (LinearLayout) findViewById(R.id.ll_save);
        rl_carImage = (RelativeLayout) findViewById(R.id.rl_carImage);

        ll_driver_licence = (LinearLayout) findViewById(R.id.ll_driver_licence);
        ll_acrreditation_certy = (LinearLayout) findViewById(R.id.ll_acrreditation_certy);
        ll_car_registration_certy = (LinearLayout) findViewById(R.id.ll_car_registration_certy);
        ll_vehicle_Insurance = (LinearLayout) findViewById(R.id.ll_vehicle_Insurance);

        tv_driver_licence_expiry = (TextView) findViewById(R.id.tv_driver_licence_expiry);
        tv_accreditation_expiry = (TextView) findViewById(R.id.tv_accreditation_expiry);
        tv_carRegistration_expiry = (TextView) findViewById(R.id.tv_carRegistration_expiry);
        tv_vehicle_Insurance_expiry = (TextView) findViewById(R.id.tv_vehicle_Insurance_expiry);

        iv_driverLicence = (ImageView) findViewById(R.id.iv_driverLicence);
        iv_accreditation_certy = (ImageView) findViewById(R.id.iv_accreditation_certy);
        iv_car_Registration = (ImageView) findViewById(R.id.iv_car_Registration);
        iv_vehicle_insurance = (ImageView) findViewById(R.id.iv_vehicle_insurance);

        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        iv_back = (ImageView) findViewById(R.id.iv_back);

        mTransformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(100)
                .borderColor(ContextCompat.getColor(activity, R.color.colorBlack))
                .borderWidthDp(2)
                .oval(true)
                .build();

        ll_back.setOnClickListener(this);
        iv_back.setOnClickListener(this);

        rl_carImage.setOnClickListener(this);
        iv_save.setOnClickListener(this);
        ll_save.setOnClickListener(this);
        ll_driver_licence.setOnClickListener(this);
        ll_acrreditation_certy.setOnClickListener(this);
        ll_car_registration_certy.setOnClickListener(this);
        ll_vehicle_Insurance.setOnClickListener(this);


        String CarImage = SessionSave.getUserSession(Comman.USER_VEHICLE_IMAGE, activity);
        String DriverLicense = SessionSave.getUserSession(Comman.USER_DRIVER_LICENCE, activity);
        String AccreditationCertificate = SessionSave.getUserSession(Comman.USER_ACCREDITATION_CERTY, activity);
        String RegistrationCertificate = SessionSave.getUserSession(Comman.USER_REGISTRATION_CERTY, activity);
        String VehicleInsuranceCertificate = SessionSave.getUserSession(Comman.USER_VEHICLE_INSURANCE_CERTY, activity);

        String DriverLicenseExpire = SessionSave.getUserSession(Comman.USER_DRIVER_lICENSE_EXPIRE, activity);
        String AccreditationCertificateExpire = SessionSave.getUserSession(Comman.USER_ACCREDITATION_CERTY_EXPIRE, activity);
        String RegistrationCertificateExpire = SessionSave.getUserSession(Comman.USER_REGISTRATION_CERTY_EXPIRE, activity);
        String VehicleInsuranceCertificateExpire = SessionSave.getUserSession(Comman.USER_VEHICLE_INSURANCE_CERTY_EXPIRE, activity);

        if(DriverLicenseExpire!=null && !DriverLicenseExpire.equalsIgnoreCase(""))
        {
            tv_driver_licence_expiry.setText(DriverLicenseExpire);
        }
        else
        {
            tv_driver_licence_expiry.setText("");
        }

        if(AccreditationCertificateExpire!=null && !AccreditationCertificateExpire.equalsIgnoreCase(""))
        {
            tv_accreditation_expiry.setText(AccreditationCertificateExpire);
        }
        else
        {
            tv_accreditation_expiry.setText("");
        }

        if(RegistrationCertificateExpire!=null && !RegistrationCertificateExpire.equalsIgnoreCase(""))
        {
            tv_carRegistration_expiry.setText(RegistrationCertificateExpire);
        }
        else
        {
            tv_carRegistration_expiry.setText("");
        }

        if(VehicleInsuranceCertificateExpire!=null && !VehicleInsuranceCertificateExpire.equalsIgnoreCase(""))
        {
            tv_vehicle_Insurance_expiry.setText(VehicleInsuranceCertificateExpire);
        }
        else
        {
            tv_vehicle_Insurance_expiry.setText("");
        }

        if(CarImage!=null && !CarImage.equalsIgnoreCase(""))
        {
            progressbar.setVisibility(View.VISIBLE);
            Picasso.with(activity)
                    .load(CarImage)
                    .fit()
                    .transform(mTransformation)
                    .into(iv_carImage, new Callback() {

                        @Override
                        public void onSuccess() {
                            progressbar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            iv_accreditation_certy.setImageResource(R.drawable.icon_demo_imageview);
                            progressbar.setVisibility(View.GONE);
                        }
                    });
        }
        else
        {
            iv_accreditation_certy.setImageResource(R.drawable.icon_demo_imageview);
        }


        if(DriverLicense!=null && !DriverLicense.equalsIgnoreCase(""))
        {
//            Picasso.with(activity)
//                    .load(DriverLicense)
//                    .fit()
//                    .into(iv_driverLicence);
            Picasso.with(activity)
                    .load(DriverLicense)
                    .fit()
                    .into(iv_driverLicence, new Callback() {

                        @Override
                        public void onSuccess() {
                            progressbar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            iv_driverLicence.setImageResource(R.drawable.icon_demo_imageview);
                            progressbar.setVisibility(View.GONE);
                        }
                    });

        }
        else
        {
            iv_driverLicence.setImageResource(R.drawable.icon_demo_imageview);
        }

        if(AccreditationCertificate!=null && !AccreditationCertificate.equalsIgnoreCase(""))
        {
//            Picasso.with(activity)
//                    .load(AccreditationCertificate)
//                    .fit()
//                    .into(iv_accreditation_certy);
            Picasso.with(activity)
                    .load(AccreditationCertificate)
                    .fit()
                    .into(iv_accreditation_certy, new Callback() {

                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                            iv_accreditation_certy.setImageResource(R.drawable.icon_demo_imageview);
                        }
                    });
        }
        else
        {
            iv_accreditation_certy.setImageResource(R.drawable.icon_demo_imageview);
        }

        if(RegistrationCertificate!=null && !RegistrationCertificate.equalsIgnoreCase(""))
        {
//            Picasso.with(activity)
//                    .load(RegistrationCertificate)
//                    .fit()
//                    .into(iv_car_Registration);
            Picasso.with(activity)
                    .load(RegistrationCertificate)
                    .fit()
                    .into(iv_car_Registration, new Callback() {

                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                            iv_car_Registration.setImageResource(R.drawable.icon_demo_imageview);
                        }
                    });
        }
        else
        {
            iv_car_Registration.setImageResource(R.drawable.icon_demo_imageview);
        }

        if(VehicleInsuranceCertificate!=null && !VehicleInsuranceCertificate.equalsIgnoreCase(""))
        {
//            Picasso.with(activity)
//                    .load(VehicleInsuranceCertificate)
//                    .fit()
//                    .into(iv_vehicle_insurance);
            Picasso.with(activity)
                    .load(VehicleInsuranceCertificate)
                    .fit()
                    .into(iv_vehicle_insurance, new Callback() {

                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                            iv_vehicle_insurance.setImageResource(R.drawable.icon_demo_imageview);
                        }
                    });
        }
        else
        {
            iv_vehicle_insurance.setImageResource(R.drawable.icon_demo_imageview);
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
//            case R.id.iv_save:
//                checkData();
//                break;
//
//            case R.id.ll_save:
//                checkData();
//                break;

            case R.id.rl_carImage:
                ImageViewClicked = CAR_IMAGE;
                showPictureDialog();
                break;

            case R.id.ll_driver_licence:
                ImageViewClicked = DRIVER_LICENCE;
                showPictureDialog();
                break;

            case R.id.ll_acrreditation_certy:
                ImageViewClicked = ACRREDITATION;
                showPictureDialog();
                break;

            case R.id.ll_car_registration_certy:
                ImageViewClicked = CAR_REGISTRATION;
                showPictureDialog();
                break;

            case R.id.ll_vehicle_Insurance:
                ImageViewClicked = VEHICLE_INSURANCE;
                showPictureDialog();
                break;

            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.iv_back:
                onBackPressed();
                break;
        }
    }


    private void UpdateDocumentWithExpiry(String driverId,byte[] image, String expiry, int flag)
    {
        Log.e("nnnnnnnnnnn","nnnnnnnnnnnnnnnnnnnnnnnnnn : "+flag);
        if (ConnectivityReceiver.isConnected())
        {
            dialogClass.showDialog();
            Map<String, Object> params = new HashMap<String, Object>();

            String url = WebServiceAPI.WEB_SERVICE_UPDATE_DOCS;

            params.put(WebServiceAPI.UPDATE_DOCUMENT_PARAM_DIVER_ID, driverId);
            if (flag==1)
            {
                params.put(WebServiceAPI.UPDATE_DOCUMENT_PARAM_LICENSE_EXPIRY, expiry);
                if (image!=null)
                {
                    params.put(WebServiceAPI.UPDATE_DOCUMENT_PARAM_DIVER_LICENCE,image);
                }
            }
            else if (flag==2)
            {
                params.put(WebServiceAPI.UPDATE_DOCUMENT_PARAM_ACCREDITATION_EXPIRY, expiry);
                if (image!=null)
                {
                    params.put(WebServiceAPI.UPDATE_DOCUMENT_PARAM_DIVER_ACCREDITATION_CERTY,image);
                }
            }
            else if (flag==3)
            {
                params.put(WebServiceAPI.UPDATE_DOCUMENT_PARAM_REGISTERATION_CERTY_EXPIRY, expiry);
                if (image!=null)
                {
                    params.put(WebServiceAPI.UPDATE_DOCUMENT_PARAM_DIVER_REGISTRATION_CERTY,image);
                }
            }
            else if (flag==4)
            {
                params.put(WebServiceAPI.UPDATE_DOCUMENT_PARAM_INSURANCE_CERTY_EXPIRY, expiry);
                if (image!=null)
                {
                    params.put(WebServiceAPI.UPDATE_DOCUMENT_PARAM_DIVER_VEHICLE_INSURANCE_CERY,image);
                }
            }
            else if (flag==5)
            {
                if (image!=null)
                {
                    params.put(WebServiceAPI.UPDATE_DOCUMENT_PARAM_VEHICLE_IMAGE,image);
                }
            }

            Log.e("url", "UpdateDriverProfile = " + url);
            Log.e("param", "UpdateDriverProfile = " + params);


            aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {

                    try
                    {
                        int responseCode = status.getCode();
                        Log.e("responseCode", "UpdateDriverProfile = " + responseCode);
                        Log.e("Response", "UpdateDriverProfile = " + json);

                        dialogClass.hideDialog();
                        if (json != null)
                        {
                            if (json.has("status"))
                            {
                                if (json.has("profile"))
                                {
                                    JSONObject driverProfile = json.getJSONObject("profile");
                                    if (driverProfile!=null)
                                    {
                                        String DriverId="", CompanyId="", DispatcherId="", Email="", FullName="", MobileNo="", Gender="", Image="", Password="", Address="", SubUrb = ""
                                                , City="", State="", Country="", Zipcode="", ReferralCode="", DriverLicense="", AccreditationCertificate="", DriverLicenseExpire=""
                                                , AccreditationCertificateExpire="", BankHolderName, BankName="", BankAcNo="", BSB="", Lat="", Lng="", Status=""
                                                , Availability="", DriverDuty="", ABN="", serviceDescription="", DCNumber="", ProfileComplete="";

                                        if (driverProfile.has("Id"))
                                        {
                                            DriverId = driverProfile.getString("Id");
                                            SessionSave.saveUserSession(Comman.USER_ID, DriverId, activity);
                                        }
                                        if (driverProfile.has("CompanyId"))
                                        {
                                            CompanyId = driverProfile.getString("CompanyId");
                                            SessionSave.saveUserSession(Comman.USER_COMPANY_ID, CompanyId, activity);
                                        }
                                        if (driverProfile.has("DispatcherId"))
                                        {
                                            DispatcherId = driverProfile.getString("DispatcherId");
                                            SessionSave.saveUserSession(Comman.USER_DISPATHER_ID, DispatcherId, activity);
                                        }
                                        if (driverProfile.has("Email"))
                                        {
                                            Email = driverProfile.getString("Email");
                                            SessionSave.saveUserSession(Comman.USER_EMAIL, Email, activity);
                                        }
                                        if (driverProfile.has("Fullname"))
                                        {
                                            FullName = driverProfile.getString("Fullname");
                                            SessionSave.saveUserSession(Comman.USER_FULL_NAME, FullName, activity);
                                        }


                                        if (driverProfile.has("MobileNo"))
                                        {
                                            MobileNo = driverProfile.getString("MobileNo");
                                            SessionSave.saveUserSession(Comman.USER_MOBILE_NUMBER, MobileNo, activity);
                                        }
                                        if (driverProfile.has("Gender"))
                                        {
                                            Gender = driverProfile.getString("Gender");
                                            SessionSave.saveUserSession(Comman.USER_GENDER, Gender, activity);
                                        }
                                        if (driverProfile.has("Image"))
                                        {
                                            Image = driverProfile.getString("Image");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_IMAGE, Image, activity);
                                        }
                                        if (driverProfile.has("Password"))
                                        {
                                            Password = driverProfile.getString("Password");
                                            SessionSave.saveUserSession(Comman.USER_PASSWORD, Password, activity);
                                        }
                                        if (driverProfile.has("Address"))
                                        {
                                            Address = driverProfile.getString("Address");
                                            SessionSave.saveUserSession(Comman.USER_RESIDENTIAL_ADDRESS, Address, activity);
                                        }
                                        if (driverProfile.has("SubUrb"))
                                        {
                                            SubUrb = driverProfile.getString("SubUrb");
                                            SessionSave.saveUserSession(Comman.USER_SUB_URB, SubUrb, activity);
                                        }


                                        if (driverProfile.has("City"))
                                        {
                                            City = driverProfile.getString("City");
                                            SessionSave.saveUserSession(Comman.USER_CITY, City, activity);
                                        }
                                        if (driverProfile.has("State"))
                                        {
                                            State = driverProfile.getString("State");
                                            SessionSave.saveUserSession(Comman.USER_STATE, State, activity);
                                        }
                                        if (driverProfile.has("Country"))
                                        {
                                            Country = driverProfile.getString("Country");
                                            SessionSave.saveUserSession(Comman.USER_COUNTRY, Country, activity);
                                        }
                                        if (driverProfile.has("ZipCode"))
                                        {
                                            Zipcode = driverProfile.getString("ZipCode");
                                            SessionSave.saveUserSession(Comman.USER_POST_CODE, Zipcode, activity);
                                        }
                                        if (driverProfile.has("ReferralCode"))
                                        {
                                            ReferralCode = driverProfile.getString("ReferralCode");
                                            SessionSave.saveUserSession(Comman.USER_REFERRAL_CODE, ReferralCode, activity);
                                        }



                                        if (driverProfile.has("DriverLicense"))
                                        {
                                            DriverLicense = driverProfile.getString("DriverLicense");
                                            SessionSave.saveUserSession(Comman.USER_DRIVER_LICENCE, DriverLicense, activity);
                                        }
                                        if (driverProfile.has("AccreditationCertificate"))
                                        {
                                            AccreditationCertificate = driverProfile.getString("AccreditationCertificate");
                                            SessionSave.saveUserSession(Comman.USER_ACCREDITATION_CERTY, AccreditationCertificate, activity);
                                        }
                                        if (driverProfile.has("DriverLicenseExpire"))
                                        {
                                            DriverLicenseExpire = driverProfile.getString("DriverLicenseExpire");
                                            SessionSave.saveUserSession(Comman.USER_DRIVER_lICENSE_EXPIRE, DriverLicenseExpire, activity);
                                        }
                                        if (driverProfile.has("AccreditationCertificateExpire"))
                                        {
                                            AccreditationCertificateExpire = driverProfile.getString("AccreditationCertificateExpire");
                                            SessionSave.saveUserSession(Comman.USER_ACCREDITATION_CERTY_EXPIRE, AccreditationCertificateExpire, activity);
                                        }
                                        if (driverProfile.has("BankHolderName"))
                                        {
                                            BankHolderName = driverProfile.getString("BankHolderName");
                                            SessionSave.saveUserSession(Comman.USER_BANK_ACCOUNT_HOLDER_NAME, BankHolderName, activity);
                                        }
                                        if (driverProfile.has("BankName"))
                                        {
                                            BankName = driverProfile.getString("BankName");
                                            SessionSave.saveUserSession(Comman.USER_BANK_NAME, BankName, activity);
                                        }



                                        if (driverProfile.has("BankAcNo"))
                                        {
                                            BankAcNo = driverProfile.getString("BankAcNo");
                                            SessionSave.saveUserSession(Comman.USER_BANK_AC_NO, BankAcNo, activity);
                                        }
                                        if (driverProfile.has("BSB"))
                                        {
                                            BSB = driverProfile.getString("BSB");
                                            SessionSave.saveUserSession(Comman.USER_BSB, BSB, activity);
                                        }
                                        if (driverProfile.has("Lat"))
                                        {
                                            Lat = driverProfile.getString("Lat");
                                            SessionSave.saveUserSession(Comman.USER_LATITUDE, Lat, activity);
                                        }
                                        if (driverProfile.has("Lng"))
                                        {
                                            Lng = driverProfile.getString("Lng");
                                            SessionSave.saveUserSession(Comman.USER_LONGITUDE, Lng, activity);
                                        }
                                        if (driverProfile.has("Status"))
                                        {
                                            Status = driverProfile.getString("Status");
                                            SessionSave.saveUserSession(Comman.USER_STATE, Status, activity);
                                        }
                                        if (driverProfile.has("Availability"))
                                        {
                                            Availability = driverProfile.getString("Availability");
                                            SessionSave.saveUserSession(Comman.USER_AVAILABILITY, Availability, activity);
                                        }



                                        if (driverProfile.has("DriverDuty"))
                                        {
                                            DriverDuty = driverProfile.getString("DriverDuty");
                                            SessionSave.saveUserSession(Comman.USER_DRIVER_DUTY, DriverDuty, activity);
                                        }
                                        if (driverProfile.has("ABN"))
                                        {
                                            ABN = driverProfile.getString("ABN");
                                            SessionSave.saveUserSession(Comman.USER_ABN, ABN, activity);
                                        }
                                        if (driverProfile.has("Description"))
                                        {
                                            serviceDescription = driverProfile.getString("Description");
                                            SessionSave.saveUserSession(Comman.USER_SERVICE_DESCRIPTION, serviceDescription, activity);
                                        }
                                        if (driverProfile.has("DCNumber"))
                                        {
                                            DCNumber = driverProfile.getString("DCNumber");
                                            SessionSave.saveUserSession(Comman.USER_DC_NUMBER, DCNumber, activity);
                                        }
                                        if (driverProfile.has("ProfileComplete"))
                                        {
                                            ProfileComplete = driverProfile.getString("ProfileComplete");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_COMPLETE, ProfileComplete, activity);
                                        }


                                        if (driverProfile.has("Vehicle"))
                                        {
                                            JSONObject driverVehicle = driverProfile.getJSONObject("Vehicle");
                                            if (driverVehicle!=null)
                                            {
                                                String VehicleId, VehicleModel, CarCompany, CarColor, VehicleRegistrationNo, RegistrationCertificate, VehicleInsuranceCertificate
                                                        , RegistrationCertificateExpire, VehicleInsuranceCertificateExpire, VehicleImage, Description, VehicleModelName;

                                                if (driverVehicle.has("Id"))
                                                {
                                                    VehicleId = driverVehicle.getString("Id");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_ID, VehicleId, activity);
                                                }
                                                if (driverVehicle.has("VehicleModel"))
                                                {
                                                    VehicleModel = driverVehicle.getString("VehicleModel");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL, VehicleModel, activity);
                                                }
                                                if (driverVehicle.has("Company"))
                                                {
                                                    CarCompany = driverVehicle.getString("Company");
                                                    SessionSave.saveUserSession(Comman.USER_CAR_COMPANY, CarCompany, activity);
                                                }
                                                if (driverVehicle.has("Color"))
                                                {
                                                    CarColor = driverVehicle.getString("Color");
                                                    SessionSave.saveUserSession(Comman.USER_CAR_COLOR, CarColor, activity);
                                                }
                                                if (driverVehicle.has("VehicleRegistrationNo"))
                                                {
                                                    VehicleRegistrationNo = driverVehicle.getString("VehicleRegistrationNo");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_REGISTRATION_NO, VehicleRegistrationNo, activity);
                                                }


                                                if (driverVehicle.has("RegistrationCertificate"))
                                                {
                                                    RegistrationCertificate = driverVehicle.getString("RegistrationCertificate");
                                                    SessionSave.saveUserSession(Comman.USER_REGISTRATION_CERTY, RegistrationCertificate, activity);
                                                }
                                                if (driverVehicle.has("VehicleInsuranceCertificate"))
                                                {
                                                    VehicleInsuranceCertificate = driverVehicle.getString("VehicleInsuranceCertificate");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_INSURANCE_CERTY, VehicleInsuranceCertificate, activity);
                                                }
                                                if (driverVehicle.has("RegistrationCertificateExpire"))
                                                {
                                                    RegistrationCertificateExpire = driverVehicle.getString("RegistrationCertificateExpire");
                                                    SessionSave.saveUserSession(Comman.USER_REGISTRATION_CERTY_EXPIRE, RegistrationCertificateExpire, activity);
                                                }
                                                if (driverVehicle.has("VehicleInsuranceCertificateExpire"))
                                                {
                                                    VehicleInsuranceCertificateExpire = driverVehicle.getString("VehicleInsuranceCertificateExpire");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_INSURANCE_CERTY_EXPIRE, VehicleInsuranceCertificateExpire, activity);
                                                }
                                                if (driverVehicle.has("VehicleImage"))
                                                {
                                                    VehicleImage = driverVehicle.getString("VehicleImage");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE, VehicleImage, activity);
                                                }

                                                if (driverVehicle.has("Description"))
                                                {
                                                    Description = driverVehicle.getString("Description");
                                                    SessionSave.saveUserSession(Comman.USER_CAR_DESCRIPTION, Description, activity);
                                                }
                                                if (driverVehicle.has("VehicleClass"))
                                                {
                                                    VehicleModelName = driverVehicle.getString("VehicleClass");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL_NAME, VehicleModelName, activity);

                                                    Log.e("VehicleModelName","VehicleModelName : "+VehicleModelName);
                                                    if (VehicleModelName!=null && VehicleModelName.contains("First Class"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "First Class", activity);
                                                    }
                                                    else if (VehicleModelName.contains("Business Class"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", activity);
                                                    }
                                                    else if (VehicleModelName.contains("Economy"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Economy", activity);
                                                    }
                                                    else if (VehicleModelName.contains("Taxi"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Taxi", activity);
                                                    }
                                                    else if (VehicleModelName.contains("LUX-VAN"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "LUX-VAN", activity);
                                                    }
                                                    else if (VehicleModelName.contains("Disability"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Disability", activity);
                                                    }
                                                    else
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", activity);
                                                    }
                                                } else {
                                                    SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", activity);
                                                }
                                                dialogClass.hideDialog();
                                            }
                                            else
                                            {
                                                //////// else
                                                dialogClass.hideDialog();
                                            }
                                            dialogClass.hideDialog();
                                        }
                                        else
                                        {
                                            dialogClass.hideDialog();
                                            if (json.has("message")) {
                                                new SnackbarUtils(main_layout, json.getString("message"),
                                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                            }
                                        }
                                        Log.e("GGGGGGGGGGG","GGGGGGGGGGG :"+SessionSave.getUserSession(Comman.USER_ID, activity));
                                    }
                                    else
                                    {
                                        dialogClass.hideDialog();
                                        if (json.has("message")) {
                                            new SnackbarUtils(main_layout, json.getString("message"),
                                                    ContextCompat.getColor(activity, R.color.snakbar_color),
                                                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                        }
                                    }
                                }
                                else
                                {
                                    Log.e("status", "false");
                                    dialogClass.hideDialog();
                                    if (json.has("message")) {
                                        new SnackbarUtils(main_layout, json.getString("message"),
                                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                    }
                                }
                            }
                            else
                            {
                                Log.e("json", "no status found");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "null");
                            dialogClass.hideDialog();
                            new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                    ContextCompat.getColor(activity, R.color.snakbar_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                        }
                    } catch (Exception e) {
                        Log.e("UpdateDriverProfile", "Exception : " + e.toString());
                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                    }
                }
            }.header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
        }
        else
        {
            new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
    }

    private void showPictureDialog()
    {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, MY_REQUEST_CODE_CAMERA);
        }
        else if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_REQUEST_CODE_STORAGE);
        }
        else
        {
            AlertDialog.Builder pictureDialog = new AlertDialog.Builder(activity);
            pictureDialog.setTitle("Select Action");
            String[] pictureDialogItems = {"Select photo from gallery", "Capture photo from camera" };
            pictureDialog.setItems(pictureDialogItems,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:
                                    choosePhotoFromGallary();
                                    break;
                                case 1:
                                    takePhotoFromCamera();
                                    break;
                            }
                        }
                    });
            pictureDialog.show();
        }
    }

    public void choosePhotoFromGallary()
    {
        userChoosenTask =chooseGallery;
        Intent intent_gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent_gallery, GALLERY);

    }

    private void takePhotoFromCamera()
    {
        userChoosenTask = chooseCamera;
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "NewPicture");
        imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        Log.e("imageUri", "imageUri11111111111111 : "+imageUri);
        startActivityForResult(intent, CAMERA);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.e("showPictureDialog", "onRequestPermissionsResult requestCode "+requestCode);
        switch (requestCode) {
            case MY_REQUEST_CODE_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    Log.e("showPictureDialog","checkSelfPermission CAMERA permission granted");
                    showPictureDialog();
                }
                break;

            case MY_REQUEST_CODE_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    Log.e("showPictureDialog","checkSelfPermission GALLERY permission granted");
                    showPictureDialog();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK)
        {
            if (requestCode == GALLERY)
            {
                onSelectFromGalleryResult(data);
            }
            else if (requestCode == CAMERA)
            {
                onCaptureImageResult();
            }
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data)
    {
        Bitmap bitmap= null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("MediaStore","MediaStore");
        }

        bitmapImage = getResizedBitmap(bitmap,400);

        if (ImageViewClicked!=null && !ImageViewClicked.equalsIgnoreCase(""))
        {
            if (ImageViewClicked.equalsIgnoreCase(DRIVER_LICENCE))
            {
                Picasso.with(activity)
                        .load(data.getData())
                        .fit()
                        .into(iv_driverLicence);
                DriverLicence_ByteImage = ConvertToByteArray(bitmapImage);
                OpenPopopFor_datePicker(tv_driver_licence_expiry, iv_driverLicence);
            }
            else if(ImageViewClicked.equalsIgnoreCase(ACRREDITATION))
            {
                Picasso.with(activity)
                        .load(data.getData())
                        .fit()
                        .into(iv_accreditation_certy);
                Accreditation_certy_ByteImage = ConvertToByteArray(bitmapImage);
                OpenPopopFor_datePicker(tv_accreditation_expiry, iv_accreditation_certy);
            }
            else if(ImageViewClicked.equalsIgnoreCase(CAR_REGISTRATION))
            {
                Picasso.with(activity)
                        .load(data.getData())
                        .fit()
                        .into(iv_car_Registration);
                Car_Registration_ByteImage = ConvertToByteArray(bitmapImage);
                OpenPopopFor_datePicker(tv_carRegistration_expiry, iv_car_Registration);
            }
            else if(ImageViewClicked.equalsIgnoreCase(VEHICLE_INSURANCE))
            {
                Picasso.with(activity)
                        .load(data.getData())
                        .fit()
                        .into(iv_vehicle_insurance);
                Vehicle_insurance_ByteImage = ConvertToByteArray(bitmapImage);
                OpenPopopFor_datePicker(tv_vehicle_Insurance_expiry, iv_vehicle_insurance);
            }
            else if(ImageViewClicked.equalsIgnoreCase(CAR_IMAGE))
            {
                Picasso.with(activity)
                        .load(data.getData())
                        .fit()
                        .transform(mTransformation)
                        .into(iv_carImage);
                CarImage_ByteImage = ConvertToByteArray(bitmapImage);
                String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                if (driverId!=null && !driverId.equalsIgnoreCase("")) {
                    UpdateDocumentWithExpiry(driverId, CarImage_ByteImage, "", 5);
                }
            }
        }
    }

    private void onCaptureImageResult()
    {
        try
        {
            Log.e("imageUri", "imageUri2222222222222222222 : "+imageUri);
            Bitmap thumbnail = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
            Log.e("#########","bmp height = "+thumbnail.getHeight());
            Log.e("#########","bmp width = "+thumbnail.getWidth());
            thumbnail = getResizedBitmap(thumbnail,400);
            bitmapImage=thumbnail;

            if (ImageViewClicked!=null && !ImageViewClicked.equalsIgnoreCase(""))
            {
                if (ImageViewClicked.equalsIgnoreCase(DRIVER_LICENCE))
                {
                    Picasso.with(activity)
                            .load(imageUri)
                            .fit()
                            .into(iv_driverLicence);
                    DriverLicence_ByteImage = ConvertToByteArray(thumbnail);

                    OpenPopopFor_datePicker(tv_driver_licence_expiry, iv_driverLicence);
                }
                else if(ImageViewClicked.equalsIgnoreCase(ACRREDITATION))
                {
                    Picasso.with(activity)
                            .load(imageUri)
                            .fit()
                            .into(iv_accreditation_certy);
                    Accreditation_certy_ByteImage = ConvertToByteArray(thumbnail);
                    OpenPopopFor_datePicker(tv_accreditation_expiry, iv_accreditation_certy);
                }
                else if(ImageViewClicked.equalsIgnoreCase(CAR_REGISTRATION))
                {
                    Picasso.with(activity)
                            .load(imageUri)
                            .fit()
                            .into(iv_car_Registration);
                    Car_Registration_ByteImage = ConvertToByteArray(thumbnail);
                    OpenPopopFor_datePicker(tv_carRegistration_expiry, iv_car_Registration);
                }
                else if(ImageViewClicked.equalsIgnoreCase(VEHICLE_INSURANCE))
                {
                    Picasso.with(activity)
                            .load(imageUri)
                            .fit()
                            .into(iv_vehicle_insurance);
                    Vehicle_insurance_ByteImage = ConvertToByteArray(thumbnail);
                    OpenPopopFor_datePicker(tv_vehicle_Insurance_expiry, iv_vehicle_insurance);
                }
                else if(ImageViewClicked.equalsIgnoreCase(CAR_IMAGE))
                {
                    Picasso.with(activity)
                            .load(imageUri)
                            .fit()
                            .transform(mTransformation)
                            .into(iv_carImage);
                    CarImage_ByteImage = ConvertToByteArray(thumbnail);
                    String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                    if (driverId!=null && !driverId.equalsIgnoreCase("")) {
                        UpdateDocumentWithExpiry(driverId, CarImage_ByteImage, "", 5);
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.e("call","exception = "+e.getMessage());
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public void OpenPopopFor_datePicker(final TextView textView, final ImageView imageView)
    {
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(activity, R.style.DialogThemeReal, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                textView.setText(dateFormatter.format(newDate.getTime()));

                if (ImageViewClicked!=null && !ImageViewClicked.equalsIgnoreCase(""))
                {
                    String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                    if (driverId!=null && !driverId.equalsIgnoreCase(""))
                    {
                        if (ImageViewClicked.equalsIgnoreCase(DRIVER_LICENCE))
                        {
                            UpdateDocumentWithExpiry(driverId,DriverLicence_ByteImage,tv_driver_licence_expiry.getText().toString().trim(),1);
                        }
                        else if (ImageViewClicked.equalsIgnoreCase(ACRREDITATION))
                        {
                            UpdateDocumentWithExpiry(driverId,Accreditation_certy_ByteImage,tv_accreditation_expiry.getText().toString().trim(),2);
                        }
                        else if (ImageViewClicked.equalsIgnoreCase(CAR_REGISTRATION))
                        {
                            UpdateDocumentWithExpiry(driverId,Car_Registration_ByteImage,tv_carRegistration_expiry.getText().toString().trim(),3);
                        }
                        else if (ImageViewClicked.equalsIgnoreCase(VEHICLE_INSURANCE))
                        {
                            UpdateDocumentWithExpiry(driverId,Vehicle_insurance_ByteImage,tv_vehicle_Insurance_expiry.getText().toString().trim(),4);
                        }
                    }
                }
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
//        fromDatePickerDialog.setSpinnersShown
        fromDatePickerDialog.setTitle("Please add Expiry Date");
        fromDatePickerDialog.show();
        fromDatePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Log.e("yyyyyy","yyyyyyyyyyy: ");
                textView.setText("");
                imageView.setImageResource(R.drawable.icon_camera);
            }
        });
    }

    public byte[] ConvertToByteArray(Bitmap bmp)
    {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 10, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            TiCKTOC_Driver_Application.setCurrentActivity(activity);
        }
    }
}