package iiride.app.driver.Activity;

import android.app.Activity;
import android.app.Dialog;

import android.os.Bundle;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import iiride.app.driver.Adapter.Register_vehi_deliv_Type_Adapter;
import iiride.app.driver.Application.TiCKTOC_Driver_Application;
import iiride.app.driver.Been.Register_vehi_deliv_Type_Been;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;
import iiride.app.driver.Others.ConnectivityReceiver;


import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.MyAlertDialog;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Profile_CarDetail_Activity extends AppCompatActivity implements View.OnClickListener{

    Profile_CarDetail_Activity activity;

    ImageView iv_back, iv_save, iv_carImageInRow;
    LinearLayout ll_save, main_layout, ll_back, ll_car_row;

    TextView tv_car_model, tv_car_Registration_no;

    public static String clickedType="";
    private static final int MY_REQUEST_CODE = 101;
    String chooseCamera="camera", chooseGallery="gallery", userChoosenTask="";
    private int GALLERY = 1, CAMERA = 2;
    byte[] DriverCar_ByteImage=null;
    Transformation mTransformation;
    String encodedImage;
    public static String VEHICLE_TYPE = "car_and_texi", DELIVERY_TYPE = "delivery_service";

    RelativeLayout  rl_check_carTexi, rl_check_deliveryService, rl_vehicleType, rl_deliveryType;
    LinearLayout ll_choose_option, ll_dialog;
    EditText et_vehiRegistrationNo, et_carCompany, et_carColor;
    CheckBox  cb_deliveryService;
    AppCompatCheckBox cb_carTexi;
    ListView lv_vehicle_type, lv_deliveryType;
    TextView tv_dialog_cancel, tv_dialog_done;
    DialogClass dialogClass;
    AQuery aQuery;
    ArrayList<Register_vehi_deliv_Type_Been> list_vehicleItem = new ArrayList<>();
    Register_vehi_deliv_Type_Adapter adapter;
    Dialog dialog_openOption;
    List<String> list_deviceModelId ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_car_list_driver);

        activity = Profile_CarDetail_Activity.this;

        main_layout = (LinearLayout) findViewById(R.id.main_layout);
        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 1);

        initUI();
    }

    private void initUI() {
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_save = (ImageView) findViewById(R.id.iv_save);
        iv_carImageInRow = (ImageView) findViewById(R.id.iv_carImageInRow);
        tv_car_model = (TextView) findViewById(R.id.tv_car_model);
        tv_car_Registration_no = (TextView) findViewById(R.id.tv_car_Registration_no);

        ll_car_row = (LinearLayout) findViewById(R.id.ll_car_row);
        ll_save = (LinearLayout) findViewById(R.id.ll_save);
        ll_back = (LinearLayout) findViewById(R.id.ll_back);


        mTransformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(100)
                .borderColor(ContextCompat.getColor(activity, R.color.colorGray))
                .borderWidthDp(0)
                .oval(true)
                .build();

        if (SessionSave.getUserSession(Comman.USER_CAR_COMPANY,activity)!= null && !SessionSave.getUserSession(Comman.USER_CAR_COMPANY,activity).equalsIgnoreCase(""))
        {
            tv_car_model.setText(SessionSave.getUserSession(Comman.USER_CAR_COMPANY,activity));
        }

        if (SessionSave.getUserSession(Comman.USER_VEHICLE_REGISTRATION_NO,activity)!= null && !SessionSave.getUserSession(Comman.USER_VEHICLE_REGISTRATION_NO,activity).equalsIgnoreCase(""))
        {
            tv_car_Registration_no.setText(SessionSave.getUserSession(Comman.USER_VEHICLE_REGISTRATION_NO,activity));
        }

        String VehiImage = SessionSave.getUserSession(Comman.USER_VEHICLE_IMAGE, activity);

        if(VehiImage!=null && !VehiImage.equalsIgnoreCase(""))
        {
            Picasso.with(activity)
                    .load(VehiImage)
                    .fit()
                    .into(iv_carImageInRow);
        }
        else
        {
            iv_carImageInRow.setImageResource(R.drawable.icon_car_with_ring);
        }

        /*DriverId,VehicleClass,VehicleColor,CompanyModel,VehicleRegistrationNo*/
        iv_back.setOnClickListener(this);
        ll_back.setOnClickListener(this);
        iv_save.setOnClickListener(this);
        ll_save.setOnClickListener(this);
        ll_car_row.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.iv_save:
                new SnackbarUtils(main_layout, activity.getResources().getString(R.string.in_process),
                        ContextCompat.getColor(activity, R.color.snakbar_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                break;

            case R.id.ll_save:
                new SnackbarUtils(main_layout, activity.getResources().getString(R.string.in_process),
                        ContextCompat.getColor(activity, R.color.snakbar_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                break;

            case R.id.ll_car_row:
                showDialog(activity);
                break;
        }
    }

    public void showDialog(final Activity activity)
    {
        dialog_openOption = new Dialog(activity,R.style.DialogTheme);
        dialog_openOption.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog_openOption.getWindow().getAttributes());
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.MATCH_PARENT;
        dialog_openOption.getWindow().setAttributes(lp);
        dialog_openOption.setCancelable(true);
        dialog_openOption.setContentView(R.layout.dialog_cardetail_driver);

        ll_dialog = (LinearLayout) dialog_openOption.findViewById(R.id.ll_dialog);
        rl_check_carTexi = (RelativeLayout) dialog_openOption.findViewById(R.id.rl_check_carTexi);
        rl_check_deliveryService = (RelativeLayout) dialog_openOption.findViewById(R.id.rl_check_deliveryService);
        rl_vehicleType = (RelativeLayout) dialog_openOption.findViewById(R.id.rl_vehicleType);
        rl_deliveryType = (RelativeLayout) dialog_openOption.findViewById(R.id.rl_deliveryType);

        ll_choose_option = (LinearLayout) dialog_openOption.findViewById(R.id.ll_choose_option);

        et_vehiRegistrationNo = (EditText) dialog_openOption.findViewById(R.id.et_vehiRegistrationNo);
        et_carCompany = (EditText) dialog_openOption.findViewById(R.id.et_carCompany);
        et_carColor = (EditText) dialog_openOption.findViewById(R.id.et_carColor);

        cb_carTexi = (AppCompatCheckBox) dialog_openOption.findViewById(R.id.cb_carTexi);
        cb_deliveryService = (CheckBox) dialog_openOption.findViewById(R.id.cb_deliveryService);

        lv_vehicle_type = (ListView) dialog_openOption.findViewById(R.id.lv_vehicle_type);
        lv_deliveryType = (ListView) dialog_openOption.findViewById(R.id.lv_deliveryType);

        tv_dialog_cancel = (TextView) dialog_openOption.findViewById(R.id.tv_dialog_cancel);
        tv_dialog_done = (TextView) dialog_openOption.findViewById(R.id.tv_dialog_done);

        if (SessionSave.getUserSession(Comman.USER_VEHICLE_REGISTRATION_NO,activity)!= null && !SessionSave.getUserSession(Comman.USER_VEHICLE_REGISTRATION_NO,activity).equalsIgnoreCase(""))
        {
            et_vehiRegistrationNo.setText(SessionSave.getUserSession(Comman.USER_VEHICLE_REGISTRATION_NO,activity));
        }

        if (SessionSave.getUserSession(Comman.USER_CAR_COMPANY,activity)!= null && !SessionSave.getUserSession(Comman.USER_CAR_COMPANY,activity).equalsIgnoreCase(""))
        {
            et_carCompany.setText(SessionSave.getUserSession(Comman.USER_CAR_COMPANY,activity));
        }

        if (SessionSave.getUserSession(Comman.USER_CAR_COLOR,activity)!= null && !SessionSave.getUserSession(Comman.USER_CAR_COLOR,activity).equalsIgnoreCase(""))
        {
            et_carColor.setText(SessionSave.getUserSession(Comman.USER_CAR_COLOR,activity));
        }

        if (SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL,activity)!= null && !SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL,activity).equalsIgnoreCase(""))
        {
            list_deviceModelId = Arrays.asList(SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL,activity).split("\\s*,\\s*"));
        }

        if (SessionSave.getUserSession(Comman.USER_PROFILE_CATEGORY_ID,activity)!=null && !SessionSave.getUserSession(Comman.USER_PROFILE_CATEGORY_ID,activity).equalsIgnoreCase(""))
        {
            if (SessionSave.getUserSession(Comman.USER_PROFILE_CATEGORY_ID,activity).equalsIgnoreCase("2"))
            {
                cb_deliveryService.setChecked(true);
            }
            else
            {
                cb_carTexi.setChecked(true);
            }
        }
        if (cb_carTexi.isChecked())
        {
            clickedType = VEHICLE_TYPE;
            if (ConnectivityReceiver.isConnected())
            {
                dialogClass.showDialog();
                FindWhichTypeIs_Visible(ll_dialog);
            }
            else
            {
                new SnackbarUtils(ll_dialog, getString(R.string.not_connected_to_internet),
                        ContextCompat.getColor(activity, R.color.snakbar_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
            }
        }
        else
        {
            clickedType = DELIVERY_TYPE;
            if (ConnectivityReceiver.isConnected())
            {
                dialogClass.showDialog();
                FindWhichTypeIs_Visible(ll_dialog);
            }
            else
            {
                new SnackbarUtils(ll_dialog, getString(R.string.not_connected_to_internet),
                        ContextCompat.getColor(activity, R.color.snakbar_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
            }
        }


        cb_carTexi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cb_carTexi.setChecked(true);
                cb_deliveryService.setChecked(false);
                clickedType = VEHICLE_TYPE;
                if (ConnectivityReceiver.isConnected())
                {
                    dialogClass.showDialog();
                    FindWhichTypeIs_Visible(ll_dialog);
                }
                else
                {
                    new SnackbarUtils(ll_dialog, getString(R.string.not_connected_to_internet),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        });

        cb_deliveryService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cb_carTexi.setChecked(false);
                cb_deliveryService.setChecked(true);
                clickedType = DELIVERY_TYPE;
                if (ConnectivityReceiver.isConnected())
                {
                    dialogClass.showDialog();
                    FindWhichTypeIs_Visible(ll_dialog);
                }
                else
                {
                    new SnackbarUtils(ll_dialog, getString(R.string.not_connected_to_internet),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        });
        /*cb_carTexi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.e("check","cb_carTexi"+isChecked);
                if (isChecked)
                {
                    cb_deliveryService.setChecked(false);
                    clickedType = VEHICLE_TYPE;
                    FindWhichTypeIs_Visible();
                }
                else
                {

                }
            }
        });

        cb_deliveryService.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.e("check","cb_deliveryService"+isChecked);
                if (isChecked)
                {
                    cb_carTexi.setChecked(false);
                    clickedType = DELIVERY_TYPE;
                    FindWhichTypeIs_Visible();
                }
                else
                {

                }
            }
        });*/


        tv_dialog_done.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected())
                {
                    CheckData(ll_dialog);
                }
                else
                {
                    new SnackbarUtils(ll_dialog, getString(R.string.not_connected_to_internet),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        });
        tv_dialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_openOption.dismiss();
            }
        });

        dialog_openOption.show();
    }

    private void CheckData(LinearLayout ll_dialog) {
        if (TextUtils.isEmpty(et_vehiRegistrationNo.getText().toString()))
        {
            new SnackbarUtils(ll_dialog, getString(R.string.please_enter_vehicle_registration_no),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_carCompany.getText().toString()))
        {
            new SnackbarUtils(ll_dialog, getString(R.string.please_enter_car_company),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_carColor.getText().toString()))
        {
            new SnackbarUtils(ll_dialog, getString(R.string.please_enter_car_color),
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
        else
        {
            GotoNextPage();
        }
    }
    private void GotoNextPage()
    {
        String AllSelectedVehicle="", VehicleId="";

        for (int i=0; i<list_vehicleItem.size() ; i++)
        {
            if (list_vehicleItem.get(i).getStatus().equalsIgnoreCase("1"))
            {
                if (AllSelectedVehicle !=null && !AllSelectedVehicle.equalsIgnoreCase(""))
                {
                    AllSelectedVehicle = AllSelectedVehicle + "," + list_vehicleItem.get(i).getBicycleName();
                    VehicleId = VehicleId + "," + list_vehicleItem.get(i).getId();
                }
                else
                {
                    AllSelectedVehicle = list_vehicleItem.get(i).getBicycleName();
                    VehicleId =  list_vehicleItem.get(i).getId();
                }
            }
        }
        if (VehicleId!= null && !VehicleId.equalsIgnoreCase(""))
        {
            SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL, VehicleId, activity);

            Log.e("tttttttttt", "tttttttttttt :" + AllSelectedVehicle);

            String userId = SessionSave.getUserSession(Comman.USER_ID, activity);
            if (userId!=null && !userId.equalsIgnoreCase(""))
            {
                if (ConnectivityReceiver.isConnected())
                {
                    UpdateCarDetail(userId);
                    dialog_openOption.cancel();
                }
                else
                {
                    new SnackbarUtils(ll_dialog, getString(R.string.not_connected_to_internet),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }
        else {
            new SnackbarUtils(ll_dialog, "You have to select at least 1 item.",
                    ContextCompat.getColor(activity, R.color.snakbar_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
        }
    }

    private void UpdateCarDetail(String driverId)
    {
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_UPDATE_DRIVER_CAR_INFO ;

        params.put(WebServiceAPI.UPDATE_DRIVER_CAR_INFO_PARAM_DIVER_ID, driverId);
        params.put(WebServiceAPI.UPDATE_DRIVER_CAR_INFO_PARAM_VEHICLE_CLASS,SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL, activity));
        params.put(WebServiceAPI.UPDATE_DRIVER_CAR_INFO_PARAM_VEHICLE_COLOR, et_carColor.getText().toString());
        params.put(WebServiceAPI.UPDATE_DRIVER_CAR_INFO_PARAM_VEHICLE_MODEL, et_carCompany.getText().toString());
        params.put(WebServiceAPI.UPDATE_DRIVER_CAR_INFO_PARAM_VEHICLE_REGISTRATION_NO, et_vehiRegistrationNo.getText().toString());

        Log.e("url", "UpdateCarDetail = " + url);
        Log.e("param", "UpdateCarDetail = " + params);


        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "UpdateCarDetail = " + responseCode);
                    Log.e("Response", "UpdateCarDetail = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                if (json.has("profile"))
                                {
                                    JSONObject driverProfile = json.getJSONObject("profile");
                                    if (driverProfile!=null)
                                    {
                                        String DriverId="", CompanyId="", DispatcherId="", Email="", FullName="", MobileNo="", Gender="", Image="", QRCode="", Password="", Address="", SubUrb = ""
                                                , City="", State="", Country="", Zipcode="", ReferralCode="", DriverLicense="", AccreditationCertificate="", DriverLicenseExpire=""
                                                , AccreditationCertificateExpire="", BankHolderName, BankName="", BankAcNo="", BSB="", Lat="", Lng="", Status=""
                                                , Availability="", DriverDuty="", ABN="", serviceDescription="", DCNumber="", ProfileComplete="", CategoryId="", Balance="", ReferralAmount="";
                                        if (driverProfile.has("Id"))
                                        {
                                            DriverId = driverProfile.getString("Id");
                                            SessionSave.saveUserSession(Comman.USER_ID, DriverId, activity);
                                        }
                                        if (driverProfile.has("CompanyId"))
                                        {
                                            CompanyId = driverProfile.getString("CompanyId");
                                            SessionSave.saveUserSession(Comman.USER_COMPANY_ID, CompanyId, activity);
                                        }
                                        if (driverProfile.has("DispatcherId"))
                                        {
                                            DispatcherId = driverProfile.getString("DispatcherId");
                                            SessionSave.saveUserSession(Comman.USER_DISPATHER_ID, DispatcherId, activity);
                                        }
                                        if (driverProfile.has("Email"))
                                        {
                                            Email = driverProfile.getString("Email");
                                            SessionSave.saveUserSession(Comman.USER_EMAIL, Email, activity);
                                        }
                                        if (driverProfile.has("Fullname"))
                                        {
                                            FullName = driverProfile.getString("Fullname");
                                            SessionSave.saveUserSession(Comman.USER_FULL_NAME, FullName, activity);
                                        }


                                        if (driverProfile.has("MobileNo"))
                                        {
                                            MobileNo = driverProfile.getString("MobileNo");
                                            SessionSave.saveUserSession(Comman.USER_MOBILE_NUMBER, MobileNo, activity);
                                        }
                                        if (driverProfile.has("Gender"))
                                        {
                                            Gender = driverProfile.getString("Gender");
                                            SessionSave.saveUserSession(Comman.USER_GENDER, Gender, activity);
                                        }
                                        if (driverProfile.has("Image"))
                                        {
                                            Image = driverProfile.getString("Image");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_IMAGE, Image, activity);
                                        }
                                        if (driverProfile.has("QRCode"))
                                        {
                                            QRCode = driverProfile.getString("QRCode");
                                            SessionSave.saveUserSession(Comman.USER_QR_CODE, QRCode, activity);
                                        }
                                        if (driverProfile.has("Password"))
                                        {
                                            Password = driverProfile.getString("Password");
                                            SessionSave.saveUserSession(Comman.USER_PASSWORD, Password, activity);
                                        }
                                        if (driverProfile.has("Address"))
                                        {
                                            Address = driverProfile.getString("Address");
                                            SessionSave.saveUserSession(Comman.USER_RESIDENTIAL_ADDRESS, Address, activity);
                                        }
                                        if (driverProfile.has("SubUrb"))
                                        {
                                            SubUrb = driverProfile.getString("SubUrb");
                                            SessionSave.saveUserSession(Comman.USER_SUB_URB, SubUrb, activity);
                                        }


                                        if (driverProfile.has("City"))
                                        {
                                            City = driverProfile.getString("City");
                                            SessionSave.saveUserSession(Comman.USER_CITY, City, activity);
                                        }
                                        if (driverProfile.has("State"))
                                        {
                                            State = driverProfile.getString("State");
                                            SessionSave.saveUserSession(Comman.USER_STATE, State, activity);
                                        }
                                        if (driverProfile.has("Country"))
                                        {
                                            Country = driverProfile.getString("Country");
                                            SessionSave.saveUserSession(Comman.USER_COUNTRY, Country, activity);
                                        }
                                        if (driverProfile.has("ZipCode"))
                                        {
                                            Zipcode = driverProfile.getString("ZipCode");
                                            SessionSave.saveUserSession(Comman.USER_POST_CODE, Zipcode, activity);
                                        }
                                        if (driverProfile.has("ReferralCode"))
                                        {
                                            ReferralCode = driverProfile.getString("ReferralCode");
                                            SessionSave.saveUserSession(Comman.USER_REFERRAL_CODE, ReferralCode, activity);
                                        }



                                        if (driverProfile.has("DriverLicense"))
                                        {
                                            DriverLicense = driverProfile.getString("DriverLicense");
                                            SessionSave.saveUserSession(Comman.USER_DRIVER_LICENCE, DriverLicense, activity);
                                        }
                                        if (driverProfile.has("AccreditationCertificate"))
                                        {
                                            AccreditationCertificate = driverProfile.getString("AccreditationCertificate");
                                            SessionSave.saveUserSession(Comman.USER_ACCREDITATION_CERTY, AccreditationCertificate, activity);
                                        }
                                        if (driverProfile.has("DriverLicenseExpire"))
                                        {
                                            DriverLicenseExpire = driverProfile.getString("DriverLicenseExpire");
                                            SessionSave.saveUserSession(Comman.USER_DRIVER_lICENSE_EXPIRE, DriverLicenseExpire, activity);
                                        }
                                        if (driverProfile.has("AccreditationCertificateExpire"))
                                        {
                                            AccreditationCertificateExpire = driverProfile.getString("AccreditationCertificateExpire");
                                            SessionSave.saveUserSession(Comman.USER_ACCREDITATION_CERTY_EXPIRE, AccreditationCertificateExpire, activity);
                                        }
                                        if (driverProfile.has("BankHolderName"))
                                        {
                                            BankHolderName = driverProfile.getString("BankHolderName");
                                            SessionSave.saveUserSession(Comman.USER_BANK_ACCOUNT_HOLDER_NAME, BankHolderName, activity);
                                        }
                                        if (driverProfile.has("BankName"))
                                        {
                                            BankName = driverProfile.getString("BankName");
                                            SessionSave.saveUserSession(Comman.USER_BANK_NAME, BankName, activity);
                                        }



                                        if (driverProfile.has("BankAcNo"))
                                        {
                                            BankAcNo = driverProfile.getString("BankAcNo");
                                            SessionSave.saveUserSession(Comman.USER_BANK_AC_NO, BankAcNo, activity);
                                        }
                                        if (driverProfile.has("BSB"))
                                        {
                                            BSB = driverProfile.getString("BSB");
                                            SessionSave.saveUserSession(Comman.USER_BSB, BSB, activity);
                                        }
                                        if (driverProfile.has("Lat"))
                                        {
                                            Lat = driverProfile.getString("Lat");
                                            SessionSave.saveUserSession(Comman.USER_LATITUDE, Lat, activity);
                                        }
                                        if (driverProfile.has("Lng"))
                                        {
                                            Lng = driverProfile.getString("Lng");
                                            SessionSave.saveUserSession(Comman.USER_LONGITUDE, Lng, activity);
                                        }
                                        if (driverProfile.has("Status"))
                                        {
                                            Status = driverProfile.getString("Status");
                                            SessionSave.saveUserSession(Comman.USER_STATE, Status, activity);
                                        }
                                        if (driverProfile.has("Availability"))
                                        {
                                            Availability = driverProfile.getString("Availability");
                                            SessionSave.saveUserSession(Comman.USER_AVAILABILITY, Availability, activity);
                                        }



                                        if (driverProfile.has("DriverDuty"))
                                        {
                                            DriverDuty = driverProfile.getString("DriverDuty");
                                            SessionSave.saveUserSession(Comman.USER_DRIVER_DUTY, DriverDuty, activity);
                                        }
                                        if (driverProfile.has("ABN"))
                                        {
                                            ABN = driverProfile.getString("ABN");
                                            SessionSave.saveUserSession(Comman.USER_ABN, ABN, activity);
                                        }
                                        if (driverProfile.has("Description"))
                                        {
                                            serviceDescription = driverProfile.getString("Description");
                                            SessionSave.saveUserSession(Comman.USER_SERVICE_DESCRIPTION, serviceDescription, activity);
                                        }
                                        if (driverProfile.has("DCNumber"))
                                        {
                                            DCNumber = driverProfile.getString("DCNumber");
                                            SessionSave.saveUserSession(Comman.USER_DC_NUMBER, DCNumber, activity);
                                        }
                                        if (driverProfile.has("ProfileComplete"))
                                        {
                                            ProfileComplete = driverProfile.getString("ProfileComplete");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_COMPLETE, ProfileComplete, activity);
                                        }

                                        if (driverProfile.has("CategoryId"))
                                        {
                                            CategoryId = driverProfile.getString("CategoryId");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_CATEGORY_ID, CategoryId, activity);
                                        }
                                        if (driverProfile.has("Balance"))
                                        {
                                            Balance = driverProfile.getString("Balance");
                                            SessionSave.saveUserSession(Comman.DRIVER_WALLET_BALLENCE, Balance, activity);
                                        }
                                        if (driverProfile.has("ReferralAmount"))
                                        {
                                            ReferralAmount = driverProfile.getString("ReferralAmount");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_REFERRAL_AMOUNT, ReferralAmount, activity);
                                        }


                                        if (driverProfile.has("Vehicle"))
                                        {
                                            JSONObject driverVehicle = driverProfile.getJSONObject("Vehicle");
                                            if (driverVehicle!=null)
                                            {
                                                String VehicleId, VehicleModel, CarCompany, CarColor, VehicleRegistrationNo, RegistrationCertificate, VehicleInsuranceCertificate
                                                        , RegistrationCertificateExpire, VehicleInsuranceCertificateExpire, VehicleImage, Description, VehicleModelName;

                                                if (driverVehicle.has("Id"))
                                                {
                                                    VehicleId = driverVehicle.getString("Id");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_ID, VehicleId, activity);
                                                }
                                                if (driverVehicle.has("VehicleModel"))
                                                {
                                                    VehicleModel = driverVehicle.getString("VehicleModel");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL, VehicleModel, activity);
                                                }
                                                if (driverVehicle.has("Company"))
                                                {
                                                    CarCompany = driverVehicle.getString("Company");
                                                    SessionSave.saveUserSession(Comman.USER_CAR_COMPANY, CarCompany, activity);
                                                    tv_car_model.setText(SessionSave.getUserSession(Comman.USER_CAR_COMPANY,activity));
                                                }
                                                if (driverVehicle.has("Color"))
                                                {
                                                    CarColor = driverVehicle.getString("Color");
                                                    SessionSave.saveUserSession(Comman.USER_CAR_COLOR, CarColor, activity);
                                                }
                                                if (driverVehicle.has("VehicleRegistrationNo"))
                                                {
                                                    VehicleRegistrationNo = driverVehicle.getString("VehicleRegistrationNo");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_REGISTRATION_NO, VehicleRegistrationNo, activity);
                                                    tv_car_Registration_no.setText(SessionSave.getUserSession(Comman.USER_VEHICLE_REGISTRATION_NO,activity));
                                                }


                                                if (driverVehicle.has("RegistrationCertificate"))
                                                {
                                                    RegistrationCertificate = driverVehicle.getString("RegistrationCertificate");
                                                    SessionSave.saveUserSession(Comman.USER_REGISTRATION_CERTY, RegistrationCertificate, activity);
                                                }
                                                if (driverVehicle.has("VehicleInsuranceCertificate"))
                                                {
                                                    VehicleInsuranceCertificate = driverVehicle.getString("VehicleInsuranceCertificate");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_INSURANCE_CERTY, VehicleInsuranceCertificate, activity);
                                                }
                                                if (driverVehicle.has("RegistrationCertificateExpire"))
                                                {
                                                    RegistrationCertificateExpire = driverVehicle.getString("RegistrationCertificateExpire");
                                                    SessionSave.saveUserSession(Comman.USER_REGISTRATION_CERTY_EXPIRE, RegistrationCertificateExpire, activity);
                                                }
                                                if (driverVehicle.has("VehicleInsuranceCertificateExpire"))
                                                {
                                                    VehicleInsuranceCertificateExpire = driverVehicle.getString("VehicleInsuranceCertificateExpire");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_INSURANCE_CERTY_EXPIRE, VehicleInsuranceCertificateExpire, activity);
                                                }
                                                if (driverVehicle.has("VehicleImage"))
                                                {
                                                    VehicleImage = driverVehicle.getString("VehicleImage");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE, VehicleImage, activity);
                                                }

                                                if (driverVehicle.has("Description"))
                                                {
                                                    Description = driverVehicle.getString("Description");
                                                    SessionSave.saveUserSession(Comman.USER_CAR_DESCRIPTION, Description, activity);
                                                }
                                                if (driverVehicle.has("VehicleClass"))
                                                {
                                                    VehicleModelName = driverVehicle.getString("VehicleClass");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL_NAME, VehicleModelName, activity);

                                                    Log.e("VehicleModelName","VehicleModelName : "+VehicleModelName);
                                                    if (VehicleModelName!=null && VehicleModelName.contains("First Class"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "First Class", activity);
                                                    }
                                                    else if (VehicleModelName.contains("Business Class"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", activity);
                                                    }
                                                    else if (VehicleModelName.contains("Economy"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Economy", activity);
                                                    }
                                                    else if (VehicleModelName.contains("Taxi"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Taxi", activity);
                                                    }
                                                    else if (VehicleModelName.contains("LUX-VAN"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "LUX-VAN", activity);
                                                    }
                                                    else if (VehicleModelName.contains("Disability"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Disability", activity);
                                                    }
                                                    else
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", activity);
                                                    }
                                                } else {
                                                    SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", activity);
                                                }
                                                dialogClass.hideDialog();
                                            }
                                            else
                                            {
                                                dialogClass.hideDialog();
                                            }
                                            dialogClass.hideDialog();

                                            if (json.has("message"))
                                            {
                                                /*new SnackbarUtils(main_layout, json.getString("message"),
                                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();*/
                                                MyAlertDialog dialog = new MyAlertDialog(activity);
                                                dialog.setAlertDialog(1, json.getString("message"));
                                            }
                                            else
                                            {
                                                new SnackbarUtils(main_layout, getResources().getString(R.string.profile_updated_successfully),
                                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                            }
                                        }
                                        else
                                        {
                                            dialogClass.hideDialog();
                                            if (json.has("message")) {
                                                new SnackbarUtils(main_layout, json.getString("message"),
                                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                            }
                                        }
                                        Log.e("GGGGGGGGGGG","GGGGGGGGGGG :"+SessionSave.getUserSession(Comman.USER_ID, activity));

                                        if (SessionSave.getUserSession(Comman.USER_VEHICLE_REGISTRATION_NO,activity)!= null && !SessionSave.getUserSession(Comman.USER_VEHICLE_REGISTRATION_NO,activity).equalsIgnoreCase(""))
                                        {
                                            et_vehiRegistrationNo.setText(SessionSave.getUserSession(Comman.USER_VEHICLE_REGISTRATION_NO,activity));
                                        }

                                        if (SessionSave.getUserSession(Comman.USER_CAR_COMPANY,activity)!= null && !SessionSave.getUserSession(Comman.USER_CAR_COMPANY,activity).equalsIgnoreCase(""))
                                        {
                                            et_carCompany.setText(SessionSave.getUserSession(Comman.USER_CAR_COMPANY,activity));
                                        }

                                        if (SessionSave.getUserSession(Comman.USER_CAR_COLOR,activity)!= null && !SessionSave.getUserSession(Comman.USER_CAR_COLOR,activity).equalsIgnoreCase(""))
                                        {
                                            et_carColor.setText(SessionSave.getUserSession(Comman.USER_CAR_COLOR,activity));
                                        }
                                    }
                                    else
                                    {
                                        dialogClass.hideDialog();
                                        if (json.has("message")) {
                                            new SnackbarUtils(main_layout, json.getString("message"),
                                                    ContextCompat.getColor(activity, R.color.snakbar_color),
                                                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                        }
                                    }
                                }
                                else
                                {
                                    dialogClass.hideDialog();
                                    if (json.has("message")) {
                                        new SnackbarUtils(main_layout, json.getString("message"),
                                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                    }
                                }

                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message")) {
                                new SnackbarUtils(main_layout, json.getString("message"),
                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                    }
                } catch (Exception e) {
                    Log.e("UpdateCarDetail", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }.header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }

    private void FindWhichTypeIs_Visible(final LinearLayout ll_dialog)
    {
        list_vehicleItem.clear();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_CAR_CLASS+SessionSave.getUserSession(Comman.USER_CITY_ID, activity);

        Log.e("url", "FindWhichTypeIs_Visible = " + url);
        Log.e("param", "FindWhichTypeIs_Visible = " + params);


        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "FindWhichTypeIs_Visible = " + responseCode);
                    Log.e("Response", "FindWhichTypeIs_Visible = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                list_vehicleItem.clear();
                                String Id ="", CategoryId = "", Name="", Sort="", BaseFare="", MinKm="", PerKmCharge="", CancellationFee="", NightCharge="",NightTimeFrom=""
                                        , NightTimeTo="", SpecialEventSurcharge="", SpecialEventTimeFrom="", SpecialEventTimeTo="", WaitingTimeCost="", MinuteFare="", BookingFee=""
                                        , Capacity="", Image="", Description="", Status="";
                                Log.e("status", "true");

                                if(clickedType.equalsIgnoreCase(VEHICLE_TYPE))
                                {
                                    rl_vehicleType.setVisibility(View.VISIBLE);
                                    rl_deliveryType.setVisibility(View.GONE);

                                    if (json.has("cars_and_taxi"))
                                    {
                                        String cars_and_taxi = json.getString("cars_and_taxi");
                                        if (cars_and_taxi!=null && !cars_and_taxi.equalsIgnoreCase(""))
                                        {
                                            JSONArray arrayCarTaxi = json.getJSONArray("cars_and_taxi");
                                            if(arrayCarTaxi.length() == 0){
                                                SessionSave.saveUserSession(Comman.USER_CITY_ID,"4", activity);
                                                FindWhichTypeIs_Visible(ll_dialog);
                                            }
                                            for (int i= 0; i<arrayCarTaxi.length(); i++)
                                            {
                                                JSONObject objectCarTexi = arrayCarTaxi.getJSONObject(i);
                                                if (objectCarTexi.has("Id"))
                                                {
                                                    Id = objectCarTexi.getString("Id");
                                                }

                                                if (objectCarTexi.has("Name"))
                                                {
                                                    Name = objectCarTexi.getString("Name");
                                                }

                                                if (objectCarTexi.has("Description"))
                                                {
                                                    Description = objectCarTexi.getString("Description");
                                                }
                                                int checked=0;
                                                for (int l=0; l<list_deviceModelId.size(); l++)
                                                {
                                                    Log.e("list_deviceModelId","list_deviceModelId22 : "+list_deviceModelId.get(l));
                                                    if (list_deviceModelId.get(l).equalsIgnoreCase(Id))
                                                    {
                                                        checked=1;
                                                    }
                                                }
                                                if (checked==1)
                                                {
                                                    list_vehicleItem.add(new Register_vehi_deliv_Type_Been(Id, Name, Description, "1"));
                                                }
                                                else
                                                {
                                                    list_vehicleItem.add(new Register_vehi_deliv_Type_Been(Id, Name, Description, "0"));
                                                }
                                            }
                                            adapter = new Register_vehi_deliv_Type_Adapter(activity, list_vehicleItem, ll_dialog, clickedType);
                                            lv_vehicle_type.setAdapter(adapter);
                                            setListViewHeightBasedOnChildren(lv_vehicle_type);

                                            Log.e("list size", "list sizeeeeeeeeeeeee :"+list_vehicleItem.size());

                                        }
                                        else
                                        {
                                            Log.e("cars_and_taxi", "null");
                                        }
                                    }
                                }
                                else
                                {
                                    rl_vehicleType.setVisibility(View.GONE);
                                    rl_deliveryType.setVisibility(View.VISIBLE);

                                    if (json.has("delivery_services"))
                                    {
                                        String delivery_services = json.getString("delivery_services");
                                        if (delivery_services!=null && !delivery_services.equalsIgnoreCase(""))
                                        {
                                            JSONArray arrayDeliveryServices = json.getJSONArray("delivery_services");
                                            for (int i= 0; i<arrayDeliveryServices.length(); i++)
                                            {
                                                JSONObject objectDeliveryService = arrayDeliveryServices.getJSONObject(i);
                                                if (objectDeliveryService.has("Id"))
                                                {
                                                    Id = objectDeliveryService.getString("Id");
                                                }

                                                if (objectDeliveryService.has("Name"))
                                                {
                                                    Name = objectDeliveryService.getString("Name");
                                                }

                                                if (objectDeliveryService.has("Description"))
                                                {
                                                    Description = objectDeliveryService.getString("Description");
                                                }
                                                int checked=0;
                                                for (int l=0; l<list_deviceModelId.size(); l++)
                                                {
                                                    Log.e("list_deviceModelId","list_deviceModelId22 : "+list_deviceModelId.get(l));
                                                   if (list_deviceModelId.get(l).equalsIgnoreCase(Id))
                                                   {
                                                       checked=1;
                                                   }
                                                }
                                                if (checked==1)
                                                {
                                                    list_vehicleItem.add(new Register_vehi_deliv_Type_Been(Id, Name, Description, "1"));
                                                }
                                                else
                                                {
                                                    list_vehicleItem.add(new Register_vehi_deliv_Type_Been(Id, Name, Description, "0"));
                                                }

                                            }
//                                            adapter.notifyDataSetChanged();
                                            adapter = new Register_vehi_deliv_Type_Adapter(activity, list_vehicleItem, ll_dialog, clickedType);
                                            lv_deliveryType.setAdapter(adapter);
                                            setListViewHeightBasedOnChildren(lv_deliveryType);


                                            Log.e("list size", "list sizeeeeeeeeeeeee :"+list_vehicleItem.size());
                                        }
                                        else
                                        {
                                            Log.e("cars_and_taxi", "null");
                                        }
                                    }
                                }
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        dialogClass.hideDialog();
                                    }
                                },100);
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(ll_dialog, json.getString("message"),
                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message")) {
                                new SnackbarUtils(ll_dialog, json.getString("message"),
                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        new SnackbarUtils(ll_dialog, getString(R.string.something_is_wrong),
                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                    }
                } catch (Exception e) {
                    Log.e("FindWhichTypeIs_Visible", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    new SnackbarUtils(ll_dialog, getString(R.string.something_is_wrong),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }.header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            TiCKTOC_Driver_Application.setCurrentActivity(activity);
        }
    }
}
