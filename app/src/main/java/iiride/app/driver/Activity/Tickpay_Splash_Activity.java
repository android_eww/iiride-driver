package iiride.app.driver.Activity;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;


import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.R;


public class Tickpay_Splash_Activity extends AppCompatActivity {

    public static Tickpay_Splash_Activity activity;

    private TextView ll_Close;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tickpay_splash);

        init();
    }

    private void init() {

        activity = Tickpay_Splash_Activity.this;


        ll_Close = (TextView) findViewById(R.id.start);
        ll_Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startNextActivity();
            }
        });

    }

    private void startNextActivity()
    {
        SessionSave.saveUserSession(Comman.USER_PREFERENCE_KEY_TICK_PAY_SPLASH,"1",activity);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Drawer_Activity.PasscodeBackPress==1)
        {
            onBackPressed();
            SessionSave.saveUserSession(Comman.USER_PREFERENCE_KEY_TICK_PAY_SPLASH,"0",activity);
        }
    }
}

