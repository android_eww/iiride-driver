package iiride.app.driver.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;

import androidx.annotation.NonNull;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.navigation.NavigationView;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import iiride.app.driver.Application.TiCKTOC_Driver_Application;
import iiride.app.driver.Been.initData.InitData;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.Constants;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.SocketSingleObject;
import iiride.app.driver.Comman.WebServiceAPI;
import iiride.app.driver.Fragment.DispatchJob_Fragment;
import iiride.app.driver.Fragment.Hailed_Fair_Fragment;
import iiride.app.driver.Fragment.Map_Fragment;
import iiride.app.driver.Fragment.MyJob_Fragment;
import iiride.app.driver.Fragment.Pending_Job_List_Fragment;
import iiride.app.driver.Fragment.Tick_Pay_Fragment;
import iiride.app.driver.Nfc.IRefreshable;
import iiride.app.driver.Nfc.NFCUtils;
import iiride.app.driver.Nfc.Provider;
import iiride.app.driver.Nfc.SimpleAsyncTask;
import iiride.app.driver.Others.BubbleService;
import iiride.app.driver.Others.ConnectivityReceiver;

import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.MyAlertDialog;
import iiride.app.driver.Others.GPSTracker;
import iiride.app.driver.View.SnackbarUtils;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import com.github.devnied.emvnfccard.model.EmvCard;
import com.github.devnied.emvnfccard.parser.EmvParser;
import com.github.devnied.emvnfccard.utils.AtrUtils;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;


import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import fr.devnied.bitlib.BytesUtils;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class Drawer_Activity extends AppCompatActivity {

    public static Drawer_Activity activity;

    private DrawerLayout mDrawer;
    private NavigationView navigationView;
    private View navHeader;
    LinearLayout ll_drawer_icon;
    public static LinearLayout main_layout;

    ImageView iv_drawer_icon;
    TextView header_name, header_email, tv_title_screen, header_ratePoint;
    ImageView iv_title, header_Prof_image;

    SwitchCompat switch_online;
    ProgressBar progBar_HeaderImage;
    public static Transformation mTransformation;

    ////for Passcode back press
    public static int PasscodeBackPress = 0;


    private static final int PERMISSION_REQUEST_CODE = 1, REQUEST_CALL_PHONE = 3, DRAW_OVER_OTHER_APP_PERMISSION = 99;

    // index to identify current nav menu item
    public static int navItemIndex = 0;
    public static int tabItemIndex = 0;

    // flag to load home fragment when user presses back key
    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;

    private FrameLayout frame;

    ActionBarDrawerToggle mDrawerToggle;

    DialogClass dialogClass;
    AQuery aQuery;

    GPSTracker gpsTracker;
    Intent intent;

    Runnable runnable;
    Handler handler, handler1;
    int count = 30;

    public static BottomSheetBehavior mBottomSheetBehavior;
    public static LinearLayout ll_bottomShit_Main;

    public static TextView tv_drop_off_location, tv_pick_up_location, tv_passName, tv_PassengerNote, tv_PassengerFlightNo;
    public static ImageView iv_passenger, iv_call_passenger, iv_mesg_passenger;
    public static LinearLayout ll_closeInfo, ll_PassengerNote, ll_PassengerFlightNo;

    String DriverDuty;
    String TAG = "AcceptDispatchJob_Req";

    int requestDialogOntimeVisible = 0;
    public static boolean flagScan = false;


    //////////////////
//    FrameLayout frame_home_fragment;

    LinearLayout ll_home, ll_dispatch, ll_mayJob, ll_hailed_fare, ll_pay;
    ImageView iv_home, iv_dispatch, iv_mayJob, iv_hailed_fare, iv_pay;
    TextView tv_home, tv_dispatch, tv_mayJob, tv_hailed_fare, tv_pay, tv_FutureBookingCount, tv_FutureBookingCount_Label;
    public static Map_Fragment map_fragment;
    public static DispatchJob_Fragment dispatchJob_fragment;
    public static MyJob_Fragment myJob_fragment;
    public static Hailed_Fair_Fragment hailed_fail_fragment;
    public static Tick_Pay_Fragment tick_pay_fragment;
    public static String passMobNo = "";

    public static int callCardListMethod = 0;


    //NFC Scanner
    private NFCUtils mNfcUtils;
    private AlertDialog mAlertDialog;
    private ProgressDialog mDialog;
    private Provider mProvider = new Provider();
    private EmvCard mReadCard;
    private WeakReference<IRefreshable> mRefreshableContent;
    private byte[] lastAts;

    private String cardNumber;
    private String strCardNumber, strCardType, strCardExpiry;

    MediaPlayer ringTone;
    public static int flagResumePass = 0;
    String NotifiFlag = "";
    String cityName = "", cityId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);

        activity = Drawer_Activity.this;
        mNfcUtils = new NFCUtils(this);

        aQuery = new AQuery(activity);
        dialogClass = new DialogClass(activity, 1);

        main_layout = (LinearLayout) findViewById(R.id.main_layout);

        gpsTracker = new GPSTracker(Drawer_Activity.this);


        callCardListMethod = 0;
        PasscodeBackPress = 0;
        flagResumePass = 0;
        NotifiFlag = "";

        checkGPS();


        init(savedInstanceState);
    }

    private void init(Bundle savedInstanceState) {

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nvView);
        navHeader = navigationView.getHeaderView(0);

        header_name = (TextView) navHeader.findViewById(R.id.header_name);
        header_email = (TextView) navHeader.findViewById(R.id.header_email);
        header_Prof_image = (ImageView) navHeader.findViewById(R.id.img_header_bg);
        header_ratePoint = (TextView) navHeader.findViewById(R.id.header_ratePoint);
        progBar_HeaderImage = (ProgressBar) navHeader.findViewById(R.id.progBar_HeaderImage);

        //for  Disable dark fading in Navigation Drawer
        mDrawer.setScrimColor(getResources().getColor(android.R.color.transparent));

        switch_online = (SwitchCompat) findViewById(R.id.switch_online);


        mHandler = new Handler();

        mTransformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(100)
                .borderColor(ContextCompat.getColor(activity, R.color.colorBlack))
                .borderWidthDp(2)
                .borderColor(activity.getResources().getColor(R.color.colorRed))
                .oval(true)
                .build();

        iv_drawer_icon = (ImageView) findViewById(R.id.iv_drawer_icon);

        iv_title = (ImageView) findViewById(R.id.iv_title);
        tv_title_screen = (TextView) findViewById(R.id.tv_title_screen);

        frame = (FrameLayout) findViewById(R.id.frame_main);
        ll_drawer_icon = (LinearLayout) findViewById(R.id.ll_drawer_icon);

        header_Prof_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mDrawer.closeDrawers();
                Intent intent = new Intent(activity, Profile__Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);

            }
        });

        handler = new Handler();
        handler1 = new Handler();

        String UserValidStatus = SessionSave.getUserSession(Comman.USER_DRIVER_DUTY, activity);
        if (UserValidStatus != null && !UserValidStatus.equalsIgnoreCase("") && UserValidStatus.equalsIgnoreCase("1")) {
            switch_online.setChecked(true);
            connectSocket();
        } else {
            switch_online.setChecked(false);
            disconnectSocket();
        }

        switch_online.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.e("switch_compat", isChecked + "");
            }
        });

        switch_online.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch_online.setEnabled(false);
                String TripFlag = SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, activity);
                if (TripFlag != null && !TripFlag.equalsIgnoreCase("")) {
                    if (TripFlag.equalsIgnoreCase("0")) {

                        if (map_fragment.isAdded()) {
                            Map_Fragment.clearMap();
                        } else {
                            setTab_Home();
                        }
                    }
                }
                String UserId = SessionSave.getUserSession(Comman.USER_ID, activity);
                if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
                } else {
                    try {
                        int locationMode = Settings.Secure.getInt(activity.getContentResolver(), Settings.Secure.LOCATION_MODE);
                        int LOCATION_MODE_HIGH_ACCURACY = 3;
                        if (locationMode == LOCATION_MODE_HIGH_ACCURACY) {
                            //request location updates
                            if (checkPermission()) {
                                gpsTracker = new GPSTracker(Drawer_Activity.this);
                                if (gpsTracker.canGetLocation()) {
                                    gpsTracker.getLocation();

                                    Constants.newgpsLatitude = gpsTracker.getLatitude() + "";
                                    Constants.newgpsLongitude = gpsTracker.getLongitude() + "";

                                    Log.e("latitude", "" + Constants.newgpsLatitude);
                                    Log.e("longitude", "" + Constants.newgpsLongitude);

                                    if (UserId != null && !UserId.equalsIgnoreCase("")) {
                                        ChangeDriverOnlineStatus(UserId);
                                    } else {
                                        switch_online.setEnabled(true);
                                    }
                                }
                            } else {
                                requestPermission();
                            }

                        } else {
                            DriverDuty = SessionSave.getUserSession(Comman.USER_DRIVER_DUTY, activity);
                            if (DriverDuty != null && !DriverDuty.equalsIgnoreCase("") && DriverDuty.equalsIgnoreCase("1")) {
                                switch_online.setChecked(true);
                            } else {
                                switch_online.setChecked(false);
                                SessionSave.saveUserSession(Comman.USER_DRIVER_DUTY, "0", activity);
                            }
                            TiCKTOC_Driver_Application.setCurrentActivity(activity);
                            //redirect user to settings page
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(Drawer_Activity.this);
                            alertDialog.setTitle("GPS is settings");

                            alertDialog.setMessage("Please set your Location Mode to High accuracy.");

                            // On pressing Settings button
                            alertDialog.setPositiveButton("Settings",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                        }
                                    });
                            alertDialog.show();
                            switch_online.setEnabled(true);

                        }
                    } catch (Settings.SettingNotFoundException e) {
                        e.printStackTrace();
                        switch_online.setEnabled(true);
                    }
                }
            }

        });


        loadNavHeader();
        // initializing navigation menu
        setUpNavigationView();

        if (savedInstanceState == null) {
//            navItemIndex = 0;
//            loadHomeFragment();
        }

        iv_drawer_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawer.isDrawerOpen(GravityCompat.START)) {
                    mDrawer.closeDrawer(GravityCompat.START);
                } else {
                    mDrawer.openDrawer(GravityCompat.START);
                    loadNavHeader();
                }
            }
        });

        ll_drawer_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawer.isDrawerOpen(GravityCompat.START)) {
                    mDrawer.closeDrawer(GravityCompat.START);
                } else {
                    mDrawer.openDrawer(GravityCompat.START);
                    loadNavHeader();
                }
            }
        });

        ll_bottomShit_Main = (LinearLayout) findViewById(R.id.ll_bottomShit_Main);
        tv_drop_off_location = (TextView) findViewById(R.id.tv_drop_off_location);
        tv_pick_up_location = (TextView) findViewById(R.id.tv_pick_up_location);
        tv_passName = (TextView) findViewById(R.id.tv_passName);
        iv_passenger = (ImageView) findViewById(R.id.iv_passenger);
        iv_call_passenger = (ImageView) findViewById(R.id.iv_call_passenger);
        iv_mesg_passenger = (ImageView) findViewById(R.id.iv_mesg_passenger);
        ll_closeInfo = (LinearLayout) findViewById(R.id.ll_closeInfo);
        ll_PassengerFlightNo = (LinearLayout) findViewById(R.id.ll_PassengerFlightNo);
        tv_PassengerFlightNo = (TextView) findViewById(R.id.tv_PassengerFlightNo);
        ll_PassengerNote = (LinearLayout) findViewById(R.id.ll_PassengerNote);
        tv_PassengerNote = (TextView) findViewById(R.id.tv_PassengerNote);

        iv_call_passenger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (passMobNo != null && !passMobNo.equalsIgnoreCase("")) {
                    Log.e("passMobNo", "passMobNo : " + passMobNo);
                    callToDriver(passMobNo);
                }
            }
        });

        iv_mesg_passenger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (passMobNo != null && !passMobNo.equalsIgnoreCase("")) {
                    Log.e("passMobNo", "passMobNo : " + passMobNo);
                    sendSMS(passMobNo);
                }
            }
        });

        ll_closeInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        });


        FrameLayout parentThatHasBottomSheetBehavior = (FrameLayout) findViewById(R.id.frame_layout);
        mBottomSheetBehavior = BottomSheetBehavior.from(parentThatHasBottomSheetBehavior);

        if (mBottomSheetBehavior != null) {
            Log.e("call", "mBottomSheetBehavior = " + mBottomSheetBehavior.getState());
            mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    Log.e("call", "onStateChanged = " + mBottomSheetBehavior.getState());
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    Log.e("call", "onSlide = " + mBottomSheetBehavior.getState());
                }
            });
        }
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);


        ll_home = (LinearLayout) findViewById(R.id.ll_home);
        ll_dispatch = (LinearLayout) findViewById(R.id.ll_dispatch);
        ll_mayJob = (LinearLayout) findViewById(R.id.ll_mayJob);
        ll_hailed_fare = (LinearLayout) findViewById(R.id.ll_hailed_fare);
        ll_pay = (LinearLayout) findViewById(R.id.ll_pay);

        iv_home = (ImageView) findViewById(R.id.iv_home);
        iv_dispatch = (ImageView) findViewById(R.id.iv_dispatch);
        iv_mayJob = (ImageView) findViewById(R.id.iv_mayJob);
        iv_hailed_fare = (ImageView) findViewById(R.id.iv_hailed_fare);
        iv_pay = (ImageView) findViewById(R.id.iv_pay);

        tv_FutureBookingCount = (TextView) findViewById(R.id.tv_FutureBookingCount);
        tv_FutureBookingCount_Label = (TextView) findViewById(R.id.tv_FutureBookingCount_Label);
        tv_FutureBookingCount.setVisibility(View.GONE);
        tv_FutureBookingCount_Label.setVisibility(View.GONE);
        tv_home = (TextView) findViewById(R.id.tv_home);
        tv_dispatch = (TextView) findViewById(R.id.tv_dispatch);
        tv_mayJob = (TextView) findViewById(R.id.tv_mayJob);
        tv_hailed_fare = (TextView) findViewById(R.id.tv_hailed_fare);
        tv_pay = (TextView) findViewById(R.id.tv_pay);


        map_fragment = new Map_Fragment();
        dispatchJob_fragment = new DispatchJob_Fragment();
        myJob_fragment = new MyJob_Fragment();
        hailed_fail_fragment = new Hailed_Fair_Fragment();
        tick_pay_fragment = new Tick_Pay_Fragment();

        ll_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTab_Home();
            }
        });
        ll_dispatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTab_Dispatch();
            }
        });
        ll_mayJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTab_MyJob();
            }
        });
        ll_hailed_fare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTab_HailedFare();
            }
        });
        ll_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTab_Pay();
            }
        });

        tv_FutureBookingCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tv_FutureBookingCount_Label.getVisibility() == View.VISIBLE) {
                    tv_FutureBookingCount_Label.setVisibility(View.GONE);
                } else {
                    tv_FutureBookingCount_Label.setVisibility(View.VISIBLE);
                }
            }
        });

        tv_FutureBookingCount_Label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_FutureBookingCount_Label.setVisibility(View.GONE);
                SessionSave.saveUserSession(Comman.NOTIFICATION_PUT_EXTRA, "BookLaterDriverNotify", getApplicationContext());
                intent = new Intent(TiCKTOC_Driver_Application.currentActivity(), Drawer_Activity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                setTab_MyJob();
                MyJob_Fragment.Call_SubFragment();
            }
        });

        NotifiFlag = SessionSave.getUserSession(Comman.NOTIFICATION_PUT_EXTRA, activity);
        if (NotifiFlag != null && !NotifiFlag.equalsIgnoreCase("")) {
            if (NotifiFlag.equalsIgnoreCase("BookLaterDriverNotify")) {
                setTab_Home();///added by saurav(12-2-2020)
                setTab_MyJob();
            } else if (NotifiFlag.equalsIgnoreCase("RejectDispatchJobRequest")) {
                setTab_Home();///added by saurav(12-2-2020)
                setTab_MyJob();
            } else if (NotifiFlag.equalsIgnoreCase("RejectBooking")) {
                setTab_Home();///added by saurav(12-2-2020)
                setTab_MyJob();
            } else if (NotifiFlag.equalsIgnoreCase("CancelRequest")) {
                setTab_Home();///added by saurav(12-2-2020)
                setTab_MyJob();
            } else {
                setTab_Home();
            }
        } else {
            setTab_Home();
        }

        NotifiFlag = "";

        getCityName();
    }

    public void Set_FutureBookingCount() {
        if (tv_FutureBookingCount != null) {
            String oldCount = SessionSave.getUserSession(Comman.NOTIFICATION_COUNT_FUTURE_BOOKING, getApplicationContext());
            if (oldCount != null && !oldCount.trim().equalsIgnoreCase("")) {
                int Count = Integer.parseInt(oldCount);
                if (Count > 0) {
                    tv_FutureBookingCount.setVisibility(View.VISIBLE);
                    tv_FutureBookingCount.setText(Count + "");
                } else {
                    tv_FutureBookingCount.setVisibility(View.GONE);
                }
            } else {
                tv_FutureBookingCount.setVisibility(View.GONE);
            }
        }
    }

    public void askForSystemOverlayPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
            //If the draw over permission is not available open the settings screen
            //to grant the permission.
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, DRAW_OVER_OTHER_APP_PERMISSION);
        }
    }
    /* public static void askForSystemOverlayPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(Drawer_Activity.activity)) {

            //If the draw over permission is not available open the settings screen
            //to grant the permission.
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + ((Drawer_Activity)activity).getPackageName()));
            ((Drawer_Activity)activity).startActivityForResult(intent, DRAW_OVER_OTHER_APP_PERMISSION);
        }
    }*/

    private void setTab_Home() {

        tabItemIndex = 0;
        flagResumePass = 0;
        navItemIndex = 0;
        activity.stopService(new Intent(activity, BubbleService.class));
        shouldLoadHomeFragOnBackPress = false;
        /*https://guides.codepath.com/android/Creating-and-Using-Fragments*/
        setImageHeaderOnToolbar();

        iv_home.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_home_select));
        iv_dispatch.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_dispatch_unlselect));
        iv_mayJob.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_job_unselect));
        iv_hailed_fare.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.hailed_fare_unselected));
        iv_pay.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_tick_pay_unselect));

        tv_home.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
        tv_dispatch.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));
        tv_mayJob.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));
        tv_hailed_fare.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));
        tv_pay.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        if (dispatchJob_fragment.isAdded()) {
            ft.hide(dispatchJob_fragment);
        }
        if (myJob_fragment.isAdded()) {
            ft.hide(myJob_fragment);
        }
        if (hailed_fail_fragment.isAdded()) {
            ft.hide(hailed_fail_fragment);
        }
        if (tick_pay_fragment.isAdded()) {
            ft.hide(tick_pay_fragment);
        }

        if (map_fragment.isAdded()) {
            ft.show(map_fragment);
        } else {
            ft.add(R.id.frame_main, map_fragment);
        }
        if (activity != null) {
            ft.commitAllowingStateLoss();
        } else if (NotifiFlag.equalsIgnoreCase("AddMoney")) {
            Intent intent = new Intent(activity, Wallet_Transfer_History_Activity.class);
            startActivity(intent);
            SessionSave.saveUserSession(Comman.NOTIFICATION_PUT_EXTRA, "", getApplicationContext());
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        } else if (NotifiFlag.equalsIgnoreCase("TransferMoney")) {
            Intent intent = new Intent(activity, Wallet_Transfer_History_Activity.class);
            startActivity(intent);
            SessionSave.saveUserSession(Comman.NOTIFICATION_PUT_EXTRA, "", getApplicationContext());
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        } else if (NotifiFlag.equalsIgnoreCase("Tickpay")) {
            Intent intent = new Intent(activity, Wallet_Transfer_History_Activity.class);
            startActivity(intent);
            SessionSave.saveUserSession(Comman.NOTIFICATION_PUT_EXTRA, "", getApplicationContext());
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }
    }

    private void setTab_Dispatch() {
        tabItemIndex = 1;
        navItemIndex = 0;
        flagResumePass = 0;
        shouldLoadHomeFragOnBackPress = true;
        setTitleOfScreenToolbar("Dispatch Jobs");

        iv_home.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_home_unselect));
        iv_dispatch.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_dispatch_lselect));
        iv_mayJob.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_job_unselect));
        iv_hailed_fare.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.hailed_fare_unselected));
        iv_pay.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_tick_pay_unselect));

        tv_home.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));
        tv_dispatch.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
        tv_mayJob.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));
        tv_hailed_fare.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));
        tv_pay.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        if (map_fragment.isAdded()) {
            ft.hide(map_fragment);
        }
        if (myJob_fragment.isAdded()) {
            ft.hide(myJob_fragment);
        }
        if (hailed_fail_fragment.isAdded()) {
            ft.hide(hailed_fail_fragment);
        }
        if (tick_pay_fragment.isAdded()) {
            ft.hide(tick_pay_fragment);
        }

        if (dispatchJob_fragment.isAdded()) {
            ft.show(dispatchJob_fragment);
            dispatchJob_fragment.goBackToFirst();
        } else {
            ft.add(R.id.frame_main, dispatchJob_fragment);
        }
        if (activity != null) {
            ft.commitAllowingStateLoss();
        }
    }

    private void setTab_MyJob() {
        tabItemIndex = 2;
        navItemIndex = 0;
        flagResumePass = 0;
        shouldLoadHomeFragOnBackPress = true;
        setTitleOfScreenToolbar("My Job");

        iv_home.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_home_unselect));
        iv_dispatch.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_dispatch_unlselect));
        iv_mayJob.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_job_select));
        iv_hailed_fare.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.hailed_fare_unselected));
        iv_pay.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_tick_pay_unselect));


        tv_home.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));
        tv_dispatch.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));
        tv_mayJob.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
        tv_hailed_fare.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));
        tv_pay.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();


        if (map_fragment.isAdded()) {
            ft.hide(map_fragment);
        }
        if (dispatchJob_fragment.isAdded()) {
            ft.hide(dispatchJob_fragment);
        }
        if (hailed_fail_fragment.isAdded()) {
            ft.hide(hailed_fail_fragment);
        }
        if (tick_pay_fragment.isAdded()) {
            ft.hide(tick_pay_fragment);
        }
        if (myJob_fragment.isAdded()) {
            ft.show(myJob_fragment);
            myJob_fragment.setMainLayoutVisible();
        } else {
            ft.add(R.id.frame_main, myJob_fragment);
        }
       /* if (pastJobList_fragment.isAdded()) { ft.hide(pastJobList_fragment); }
        if (dispatchedJobListFragment.isAdded()) { ft.hide(dispatchedJobListFragment); }
        if (pendingJobListFragment.isAdded()) { ft.hide(pendingJobListFragment); }
        if (Future_JobBooking_List_Fragment.isAdded()) { ft.hide(Future_JobBooking_List_Fragment); }*/
        if (activity != null) {
            ft.commitAllowingStateLoss();
        }
    }

    private void setTab_HailedFare() {
        tabItemIndex = 3;
        navItemIndex = 0;
        flagResumePass = 0;
        shouldLoadHomeFragOnBackPress = true;
        setTitleOfScreenToolbar("Hailed Fare");

        iv_home.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_home_unselect));
        iv_dispatch.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_dispatch_unlselect));
        iv_mayJob.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_job_unselect));
        iv_hailed_fare.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.hailed_fare_selected));
        iv_pay.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_tick_pay_unselect));


        tv_home.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));
        tv_dispatch.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));
        tv_mayJob.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));
        tv_hailed_fare.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));
        tv_pay.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        if (dispatchJob_fragment.isAdded()) {
            ft.hide(dispatchJob_fragment);
        }
        if (myJob_fragment.isAdded()) {
            ft.hide(myJob_fragment);
        }
        if (map_fragment.isAdded()) {
            ft.hide(map_fragment);
        }
        if (tick_pay_fragment.isAdded()) {
            ft.hide(tick_pay_fragment);
        }

        if (hailed_fail_fragment.isAdded()) { // if the fragment is already in container
            ft.show(hailed_fail_fragment);
        } else { // fragment needs to be added to frame container
            ft.add(R.id.frame_main, hailed_fail_fragment);
        }

        if (activity != null) {
            ft.commitAllowingStateLoss();
        }
    }

    private void setTab_Pay() {
        tabItemIndex = 4;
        navItemIndex = 2;
        flagResumePass = 0;

        nfcScanTicpay();
        PasscodeBackPress = 0;
        String abcd = SessionSave.getUserSession(Comman.USER_PREFERENCE_KEY_TICK_PAY_SPLASH, activity);
        if (abcd != null && !abcd.equalsIgnoreCase("") && abcd.equalsIgnoreCase("1")) {
            shouldLoadHomeFragOnBackPress = true;

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

            if (dispatchJob_fragment.isAdded()) {
                ft.hide(dispatchJob_fragment);
            }
            if (myJob_fragment.isAdded()) {
                ft.hide(myJob_fragment);
            }
            if (hailed_fail_fragment.isAdded()) {
                ft.hide(hailed_fail_fragment);
            }
            if (map_fragment.isAdded()) {
                ft.hide(map_fragment);
            }
            if (map_fragment.isAdded()) {
                ft.hide(map_fragment);
            }

            if (tick_pay_fragment.isAdded()) {
                setTitleOfScreenToolbar("Tick Pay");

                iv_home.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_home_unselect));
                iv_dispatch.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_dispatch_unlselect));
                iv_mayJob.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_job_unselect));
                iv_hailed_fare.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.hailed_fare_unselected));
                iv_pay.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_tick_pay_select));


                tv_home.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));
                tv_dispatch.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));
                tv_mayJob.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));
                tv_hailed_fare.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));
                tv_pay.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));

                // if the fragment is already in container
                ft.show(tick_pay_fragment);
                if (activity != null) {
                    ft.commitAllowingStateLoss();
                }

                if (SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED, activity) != null && !SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED, activity).equalsIgnoreCase("")
                        && SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED, activity).equalsIgnoreCase("1")) {
                    Intent intentv = new Intent(Drawer_Activity.this, Create_Passcode_Activity.class);
                    startActivity(intentv);
                }
            } else { // fragment needs to be added to frame container
                if (ConnectivityReceiver.isConnected()) {
                    setTitleOfScreenToolbar("TiCKPAY");

                    iv_home.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_home_unselect));
                    iv_dispatch.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_dispatch_unlselect));
                    iv_mayJob.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_job_unselect));
                    iv_hailed_fare.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.hailed_fare_unselected));
                    iv_pay.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_tick_pay_select));


                    tv_home.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));
                    tv_dispatch.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));
                    tv_mayJob.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));
                    tv_hailed_fare.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));
                    tv_pay.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));

                    ft.add(R.id.frame_main, tick_pay_fragment);
                    if (activity != null) {
                        ft.commitAllowingStateLoss();
                    }
                    if (SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED, activity) != null && !SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED, activity).equalsIgnoreCase("")
                            && SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED, activity).equalsIgnoreCase("1")) {
                        Intent intentv = new Intent(Drawer_Activity.this, Create_Passcode_Activity.class);
                        startActivity(intentv);
                    }
                } else {
                    new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        } else {
            intent = new Intent(Drawer_Activity.this, Tickpay_Splash_Activity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    shouldLoadHomeFragOnBackPress = true;

                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

                    if (dispatchJob_fragment.isAdded()) {
                        ft.hide(dispatchJob_fragment);
                    }
                    if (myJob_fragment.isAdded()) {
                        ft.hide(myJob_fragment);
                    }
                    if (hailed_fail_fragment.isAdded()) {
                        ft.hide(hailed_fail_fragment);
                    }
                    if (map_fragment.isAdded()) {
                        ft.hide(map_fragment);
                    }
                    if (map_fragment.isAdded()) {
                        ft.hide(map_fragment);
                    }

                    if (tick_pay_fragment.isAdded()) { // if the fragment is already in container
                        setTitleOfScreenToolbar("Tick Pay");

                        iv_home.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_home_unselect));
                        iv_dispatch.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_dispatch_unlselect));
                        iv_mayJob.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_job_unselect));
                        iv_hailed_fare.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.hailed_fare_unselected));
                        iv_pay.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_tick_pay_select));


                        tv_home.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));
                        tv_dispatch.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));
                        tv_mayJob.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));
                        tv_hailed_fare.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));
                        tv_pay.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));

                        // if the fragment is already in container
                        ft.show(tick_pay_fragment);
                        if (activity != null) {
                            ft.commitAllowingStateLoss();
                        }
                        if (SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED, activity) != null && !SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED, activity).equalsIgnoreCase("")
                                && SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED, activity).equalsIgnoreCase("1")) {
                            Intent intentv = new Intent(Drawer_Activity.this, Create_Passcode_Activity.class);
                            startActivity(intentv);
                        }

                    } else { // fragment needs to be added to frame container
                        if (ConnectivityReceiver.isConnected()) {
                            setTitleOfScreenToolbar("Tick Pay");

                            iv_home.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_home_unselect));
                            iv_dispatch.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_dispatch_unlselect));
                            iv_mayJob.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_job_unselect));
                            iv_hailed_fare.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.hailed_fare_unselected));
                            iv_pay.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.icon_tick_pay_select));


                            tv_home.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));
                            tv_dispatch.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));
                            tv_mayJob.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));
                            tv_hailed_fare.setTextColor(ContextCompat.getColor(activity, R.color.colorGray));
                            tv_pay.setTextColor(ContextCompat.getColor(activity, R.color.colorRed));

                            ft.add(R.id.frame_main, tick_pay_fragment);
                            if (activity != null) {
                                ft.commitAllowingStateLoss();
                            }

                            if (SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED, activity) != null && !SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED, activity).equalsIgnoreCase("")
                                    && SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED, activity).equalsIgnoreCase("1")) {
                                Intent intentv = new Intent(Drawer_Activity.this, Create_Passcode_Activity.class);
                                startActivity(intentv);
                            }

                        } else {
                            new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                                    ContextCompat.getColor(activity, R.color.snakbar_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                        }
                    }
                }
            }, 100);
        }
    }

    private void loadNavHeader() {
        String UserName = SessionSave.getUserSession(Comman.USER_FULL_NAME, activity);
        Log.e("UserName", "UserName : " + UserName);
        String UserEmail = SessionSave.getUserSession(Comman.USER_EMAIL, activity);
        String UserImage = SessionSave.getUserSession(Comman.USER_PROFILE_IMAGE, activity);
        if (UserName != null && !UserName.equalsIgnoreCase("")) {
            header_name.setText(UserName);
        } else {
            header_name.setVisibility(View.INVISIBLE);
        }
        if (UserEmail != null && !UserEmail.equalsIgnoreCase("")) {
            header_email.setText(UserEmail);
        } else {
            header_email.setVisibility(View.INVISIBLE);
        }

        String UserProfile = SessionSave.getUserSession(Comman.USER_PROFILE_IMAGE, activity);
        Log.e("UserProfile", "UserProfile : " + UserProfile);

        if (UserImage != null && !UserImage.equalsIgnoreCase("")) {
            progBar_HeaderImage.setVisibility(View.VISIBLE);
            Picasso.with(activity)
                    .load(UserImage)
                    .transform(mTransformation)
                    .fit()
                    .into(header_Prof_image, new Callback() {

                        @Override
                        public void onSuccess() {
                            progBar_HeaderImage.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            header_Prof_image.setImageResource(R.drawable.prof_image_demo);
                            progBar_HeaderImage.setVisibility(View.GONE);
                        }
                    });

        } else {
            header_Prof_image.setImageResource(R.drawable.prof_image_demo);
        }

        String ratePoint = SessionSave.getUserSession(Comman.USER_RATE_POINT, activity);
        if (ratePoint != null && !ratePoint.equalsIgnoreCase("")) {
            header_ratePoint.setText(ratePoint);
        }
    }

    private void loadHomeFragment() {
        selectNavMenu();
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments

                switch (navItemIndex) {
                    case 0:
                        intent = new Intent(activity, Wallet_Cards_Activity.class);
                        intent.putExtra("from", "drawer");
                        startActivity(intent);
                        overridePendingTransition(R.anim.right_in, R.anim.left_out);
                        break;

                    case 1:
                        PasscodeBackPress = 0;
                        intent = new Intent(Drawer_Activity.this, Wallet__Activity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.right_in, R.anim.left_out);

                        if (SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED, activity) != null && !SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED, activity).equalsIgnoreCase("")
                                && SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED, activity).equalsIgnoreCase("1")) {
                            Intent intentv = new Intent(Drawer_Activity.this, Create_Passcode_Activity.class);
                            startActivity(intentv);
                        }
                        break;

                    case 2:
                        setTab_Pay();
                        Tick_Pay_Fragment tick_pay_fragment = new Tick_Pay_Fragment();
                        FragmentTransaction fragmentTransaction1 = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction1.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                        fragmentTransaction1.replace(R.id.frame_main, tick_pay_fragment);
                        fragmentTransaction1.commitAllowingStateLoss();
                        break;

                    case 3:
                        intent = new Intent(Drawer_Activity.this, WeeklyEarninigsActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.right_in, R.anim.left_out);
                        break;

                    case 4:
                        intent = new Intent(Drawer_Activity.this, Driver_News_Activity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.right_in, R.anim.left_out);
                        break;

                    case 5:
                        intent = new Intent(Drawer_Activity.this, Invite_Friend_Activity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.right_in, R.anim.left_out);
                        break;

                    case 6:
                        intent = new Intent(Drawer_Activity.this, Change_password_Activity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.right_in, R.anim.left_out);
                        break;

                    case 7:
                        intent = new Intent(Drawer_Activity.this, Setting_Activity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.right_in, R.anim.left_out);
                        break;

                    case 8:
                        String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                        String token = SessionSave.getToken(Comman.DEVICE_TOKEN, activity);
                        if (token != null && !token.equalsIgnoreCase("")) {
                            if (driverId != null && !driverId.equalsIgnoreCase("")) {
                                if (ConnectivityReceiver.isConnected()) {
                                    DriverLogOut(driverId, token);
                                } else {
                                    new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        } else {
                            new SnackbarUtils(main_layout, getString(R.string.please_restart_app),
                                    ContextCompat.getColor(activity, R.color.snakbar_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                        }

                        break;
                }
            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }

        //Closing drawer on item click
        mDrawer.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    private void DriverLogOut(String driverId, String token) {
        aQuery = new AQuery(activity);
        dialogClass.showDialog();

        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_DRIVER_LOGOUT + driverId + "/" + token;

        Log.e(TAG, "URL = " + url);
        Log.e(TAG, "PARAMS = " + params);

        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e(TAG, "responseCode = " + responseCode);
                    Log.e(TAG, "Response = " + json);

                    if (json != null) {
                        if (json.has("status")) {
                            if (json.getBoolean("status")) {
                                Log.e("status", "true");
                                dialogClass.hideDialog();

                               /* if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                }*/

                                SessionSave.clearUserSession(activity);
                                intent = new Intent(getApplicationContext(), Main_Activity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            } else {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        } else {
                            Log.e("status", "no status");
                            dialogClass.hideDialog();
                            if (json.has("message")) {
                                new SnackbarUtils(main_layout, json.getString("message"),
                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }
                    } else {
                        Log.e(TAG, "getMessage = " + "null");
                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout, getResources().getString(R.string.something_is_wrong),
                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception = " + e.getMessage() + "something_is_wrong");
                    dialogClass.hideDialog();
                }
            }
        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }


    public void checkGPS() {
        if (checkPermission()) {
            if (gpsTracker.canGetLocation()) {
                gpsTracker.getLocation();

                Constants.newgpsLatitude = gpsTracker.getLatitude() + "";
                Constants.newgpsLongitude = gpsTracker.getLongitude() + "";

                Log.e("latitude", "" + Constants.newgpsLatitude);
                Log.e("longitude", "" + Constants.newgpsLongitude);
            }
        } else {
            requestPermission();
        }
    }

    public boolean checkPermission() {

        int result = ContextCompat.checkSelfPermission(Drawer_Activity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    public void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        } else {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Log.e("call", "onRequestPermissionsResult() 11 = " + requestCode);

        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                switch_online.setEnabled(true);
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("call", "onRequestPermissionsResult() 22 = ");
                    if (gpsTracker.canGetLocation()) {
                        Log.e("call", "onRequestPermissionsResult() 33 = ");
                        gpsTracker = new GPSTracker(Drawer_Activity.this);
                        gpsTracker.getLocation();

                        Constants.newgpsLatitude = gpsTracker.getLatitude() + "";
                        Constants.newgpsLongitude = gpsTracker.getLongitude() + "";
                    } else {
                        Log.e("call", "onRequestPermissionsResult() 44 = ");
//                        AlertMessageNoGps();
                    }
                    DriverDuty = SessionSave.getUserSession(Comman.USER_DRIVER_DUTY, activity);
                    if (DriverDuty.equalsIgnoreCase("1")) {
                        switch_online.setChecked(true);
                    } else if (DriverDuty.equalsIgnoreCase("0")) {
                        switch_online.setChecked(false);
                    }
                    TiCKTOC_Driver_Application.setCurrentActivity(activity);
                } else {
                    Log.e("call", "onRequestPermissionsResult() 55 = ");
                    DriverDuty = SessionSave.getUserSession(Comman.USER_DRIVER_DUTY, activity);
                    DriverDuty = SessionSave.getUserSession(Comman.USER_DRIVER_DUTY, activity);
                    if (DriverDuty.equalsIgnoreCase("1")) {
                        switch_online.setChecked(true);
                    } else if (DriverDuty.equalsIgnoreCase("0")) {
                        switch_online.setChecked(false);
                    }
                    TiCKTOC_Driver_Application.setCurrentActivity(activity);
                    MyAlertDialog dialog = new MyAlertDialog(Drawer_Activity.this);
                    dialog.setCancelable(false);
                    dialog.setAlertDialog(1, getResources().getString(R.string.permission_denied_for_location));
                }
                break;

            /*case REQUEST_CALL_PHONE:
                callToDriver();
                break;*/

            case 0:
                if (myJob_fragment != null)
                    myJob_fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    public void setTitleOfScreenToolbar(String titleName) {
        iv_title.setVisibility(View.GONE);
        tv_title_screen.setVisibility(View.VISIBLE);
        if (titleName != null && !titleName.equalsIgnoreCase("")) {
            tv_title_screen.setText(titleName);
        } else {
            iv_title.setVisibility(View.VISIBLE);
            tv_title_screen.setVisibility(View.GONE);
        }
    }

    public void setImageHeaderOnToolbar() {
        iv_title.setVisibility(View.VISIBLE);
        tv_title_screen.setVisibility(View.GONE);
    }

    private void selectNavMenu() {
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }


    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_payment_option:
                        navItemIndex = 0;
                        break;

                    case R.id.nav_wallet:
                        navItemIndex = 1;
                        break;

                    case R.id.nav_tickpay:
                        navItemIndex = 2;
                        break;

                    case R.id.nav_weekly_earning:
                        navItemIndex = 3;
                        break;

                    case R.id.nav_driver_news:
                        navItemIndex = 4;
                        break;

                    case R.id.nav_invite_drivers:
                        navItemIndex = 5;
                        break;

                    case R.id.nav_change_password:
                        navItemIndex = 6;
                        break;

                    case R.id.nav_setting:
                        navItemIndex = 7;
                        break;

                    case R.id.nav_logout:
                        navItemIndex = 8;
                        break;

                    default:
                        navItemIndex = 0;
                        break;
                }

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                loadHomeFragment();

                return true;
            }
        });

        /////by puttting this code here drawer can be easily open/close
        mDrawer.setScrimColor(Color.TRANSPARENT);
//        mDrawer.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, null, R.string.openDrawer, R.string.closeDrawer) {
            @Override
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                super.onDrawerSlide(drawerView, 0); // this disables the arrow @ completed state
                invalidateOptionsMenu();
            }

            @SuppressLint("NewApi")
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, 0);
                invalidateOptionsMenu();
            }
        };
        mDrawer.setDrawerListener(mDrawerToggle);

    }

    private void ChangeDriverOnlineStatus(String userId) {
        dialogClass.showDialog();

        String url = WebServiceAPI.WEB_SERVICE_CHANGE_DRIVER_SHIFT_STATUS;

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.CHANGE_DRIVER_SHIFT_STATUS_PARAM_USER_ID, userId);
        params.put(WebServiceAPI.CHANGE_DRIVER_SHIFT_STATUS_PARAM_LAT, Constants.newgpsLatitude);
        params.put(WebServiceAPI.CHANGE_DRIVER_SHIFT_STATUS_PARAM_LNG, Constants.newgpsLongitude);


        Log.e("url", "ChangeDriverOnlineStatus = " + url);
        Log.e("param", "ChangeDriverOnlineStatus = " + params);

        DriverDuty = SessionSave.getUserSession(Comman.USER_DRIVER_DUTY, activity);


        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "ChangeDriverOnlineStatus = " + responseCode);
                    Log.e("Response", "ChangeDriverOnlineStatus = " + json);

                    if (json != null) {
                        if (json.has("status")) {
                            if (json.getBoolean("status")) {
                                Log.e("status", "true");
                                dialogClass.hideDialog();
                                if (json.has("duty")) {
                                    String duty = json.getString("duty");
                                    if (duty != null && !duty.equalsIgnoreCase("")) {
                                        if (duty.equalsIgnoreCase("on")) {
                                            switch_online.setChecked(true);
                                            SessionSave.saveUserSession(Comman.USER_DRIVER_DUTY, "1", activity);
                                            connectSocket();
                                        } else {
                                            switch_online.setChecked(false);
                                            SessionSave.saveUserSession(Comman.USER_DRIVER_DUTY, "0", activity);
                                        }

                                        TiCKTOC_Driver_Application.setCurrentActivity(activity);
                                        switch_online.setEnabled(true);

                                        if (json.has("message")) {
                                            new SnackbarUtils(main_layout, json.getString("message"),
                                                    ContextCompat.getColor(activity, R.color.snakbar_color),
                                                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                        }
                                    } else {
                                        dialogClass.hideDialog();
                                        switch_online.setEnabled(true);
                                        if (DriverDuty != null && !DriverDuty.equalsIgnoreCase("")) {
                                            if (DriverDuty.equalsIgnoreCase("1")) {
                                                switch_online.setChecked(true);
                                            } else if (DriverDuty.equalsIgnoreCase("0")) {
                                                switch_online.setChecked(false);
                                            }
                                        } else {
                                            switch_online.setChecked(false);
                                            SessionSave.saveUserSession(Comman.USER_DRIVER_DUTY, "0", activity);
                                        }
                                        TiCKTOC_Driver_Application.setCurrentActivity(activity);
                                        if (json.has("message")) {
                                            new SnackbarUtils(main_layout, json.getString("message"),
                                                    ContextCompat.getColor(activity, R.color.snakbar_color),
                                                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                        }
                                    }
                                } else {
                                    dialogClass.hideDialog();
                                    switch_online.setEnabled(true);
                                    if (DriverDuty != null && !DriverDuty.equalsIgnoreCase("")) {
                                        if (DriverDuty.equalsIgnoreCase("1")) {
                                            switch_online.setChecked(true);
                                        } else if (DriverDuty.equalsIgnoreCase("0")) {
                                            switch_online.setChecked(false);
                                        }
                                    } else {
                                        switch_online.setChecked(false);
                                        SessionSave.saveUserSession(Comman.USER_DRIVER_DUTY, "0", activity);
                                    }
                                    TiCKTOC_Driver_Application.setCurrentActivity(activity);
                                    if (json.has("message")) {
                                        new SnackbarUtils(main_layout, json.getString("message"),
                                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                    }
                                }

                               /* if (DriverDuty != null && !DriverDuty.equalsIgnoreCase("")) {
                                    if (DriverDuty.equalsIgnoreCase("1")) {
                                        switch_online.setChecked(false);
                                        SessionSave.saveUserSession(Comman.USER_DRIVER_DUTY, "0", activity);
                                    } else if (DriverDuty.equalsIgnoreCase("0")) {
                                        switch_online.setChecked(true);
                                        SessionSave.saveUserSession(Comman.USER_DRIVER_DUTY, "1", activity);
                                        connectSocket();
                                    }
                                }
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                }*/
                            } else {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                switch_online.setEnabled(true);
                                if (DriverDuty != null && !DriverDuty.equalsIgnoreCase("")) {
                                    if (DriverDuty.equalsIgnoreCase("1")) {
                                        switch_online.setChecked(true);
                                    } else if (DriverDuty.equalsIgnoreCase("0")) {
                                        switch_online.setChecked(false);
                                    }
                                } else {
                                    switch_online.setChecked(false);
                                    SessionSave.saveUserSession(Comman.USER_DRIVER_DUTY, "0", activity);
                                }
                                TiCKTOC_Driver_Application.setCurrentActivity(activity);
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(activity, R.color.snakbar_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        } else {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            switch_online.setEnabled(true);
                            if (DriverDuty != null && !DriverDuty.equalsIgnoreCase("")) {
                                if (DriverDuty.equalsIgnoreCase("1")) {
                                    switch_online.setChecked(true);
                                } else if (DriverDuty.equalsIgnoreCase("0")) {
                                    switch_online.setChecked(false);
                                }
                            } else {
                                switch_online.setChecked(false);
                                SessionSave.saveUserSession(Comman.USER_DRIVER_DUTY, "0", activity);
                            }
                            TiCKTOC_Driver_Application.setCurrentActivity(activity);
                            if (json.has("message")) {
                                new SnackbarUtils(main_layout, json.getString("message"),
                                        ContextCompat.getColor(activity, R.color.snakbar_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                        ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }
                    } else {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        switch_online.setEnabled(true);
                        if (DriverDuty != null && !DriverDuty.equalsIgnoreCase("")) {
                            if (DriverDuty.equalsIgnoreCase("1")) {
                                switch_online.setChecked(true);
                            } else if (DriverDuty.equalsIgnoreCase("0")) {
                                switch_online.setChecked(false);
                            }
                        } else {
                            switch_online.setChecked(false);
                            SessionSave.saveUserSession(Comman.USER_DRIVER_DUTY, "0", activity);
                        }
                        TiCKTOC_Driver_Application.setCurrentActivity(activity);
                        if (ConnectivityReceiver.isConnected()) {
                            new SnackbarUtils(main_layout, getString(R.string.not_connected_with_socket),
                                    ContextCompat.getColor(activity, R.color.snakbar_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                        } else {
                            new SnackbarUtils(Drawer_Activity.main_layout, getString(R.string.not_connected_to_internet),
                                    ContextCompat.getColor(activity, R.color.snakbar_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                    ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                        }
                    }
                } catch (Exception e) {
                    Log.e("ChangeDriverStatus", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    switch_online.setEnabled(true);
                    if (DriverDuty != null && !DriverDuty.equalsIgnoreCase("")) {
                        if (DriverDuty.equalsIgnoreCase("1")) {
                            switch_online.setChecked(true);
                        } else if (DriverDuty.equalsIgnoreCase("0")) {
                            switch_online.setChecked(false);
                        }
                    } else {
                        switch_online.setChecked(false);
                        SessionSave.saveUserSession(Comman.USER_DRIVER_DUTY, "0", activity);
                    }
                    TiCKTOC_Driver_Application.setCurrentActivity(activity);
                    new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                            ContextCompat.getColor(activity, R.color.snakbar_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }


    public void connectSocket() {
        Log.e("call", "connectSocket()");
        try {
            if (Comman.socket != null && Comman.socket.connected()) {
                Log.e("call", "Ping socket already connected");
                startTimer();
            } else {
                Log.e("call", "Ping socket try to connecting...");
                Comman.socket = null;
                SocketSingleObject.instance = null;
                Comman.socket = SocketSingleObject.get(activity).getSocket();
                Comman.socket.on(WebServiceAPI.SERVER_SOCKET_DRIVER_ARRIVE_BOOKING_REQUEST, onReceiveBooking_Request);
                Comman.socket.on(WebServiceAPI.SERVER_SOCKET_GET_BOOKING_DETAIL_AFTER_ACCEPTING_REQUEST, onReceiveBookingInfo);
                Comman.socket.on(WebServiceAPI.SERVER_SOCKET_RECEIVE_CANCEL_NOTIFICATION, onReceive_CancelTrip_Notification);
                Comman.socket.on(WebServiceAPI.SERVER_SOCKET_UPDTED_BOOKING_DETAIL, onUpdatedBookingDetail);
                Comman.socket.on(Socket.EVENT_CONNECT, onConnect);
                Comman.socket.on(Socket.EVENT_DISCONNECT, onDisconnect);

                Comman.socket.on(WebServiceAPI.SERVER_SOCKET_ARIVE_ADVANCE_BOOKING_REQUEST, onReceiveBookLater_Request);
                Comman.socket.on(WebServiceAPI.SERVER_SOCKET_RECEIVE_CANCEL_NOTIFICATION_LATER, onReceive_CancelTrip_Notification_Later);
                Comman.socket.on(WebServiceAPI.SERVER_SOCKET_GET_BOOKING_DETAIL_AFTER_ACCEPTING_REQUEST_LATER, onReceiveBookingInfoLater);

                Comman.socket.on(WebServiceAPI.SERVER_SOCKET_BOOK_LATER_DRIVER_NOTIFY, onBookLaterDriverNotify);
                Comman.socket.on(WebServiceAPI.SOCKET_SESSION_ERROR, onSessionError);

                Comman.socket.connect();
            }
        } catch (Exception e) {
            Log.e("call", "Socket connect exception = " + e.getMessage());
        }
    }

    public void disconnectSocket() {
        Log.e("call", "disconnectSocket()");
        try {
            if (Comman.socket != null && Comman.socket.connected()) {
                Comman.socket.disconnect();
                Comman.socket = null;
                SocketSingleObject.instance = null;
            } else {
                Log.e("call", "Socket already disconnected.");
            }
        } catch (Exception e) {
            Log.e("call", "Socket connect exception = " + e.getMessage());
        }
    }

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("socket", "connected");

                    startTimer();
                }
            });
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("socket", "diconnected");
                }
            });
        }
    };

    private Emitter.Listener onReceiveBooking_Request = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("kkkkkkkkk", "kkkkkkkkkkk onReceiveBooking_Request args.length :" + args.length);
                    if (args.length > 0 && args[0] != null) {
                        SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "0", activity);
                        Log.e("kkkkkkkkk", "kkkkkkkkkkk onReceiveBooking_Request args[0] :" + args[0]);
                        try {
                            JSONObject jsonObject = new JSONObject(args[0].toString());
                            String BookingId = "", Booking_message = "", Booking_Type = "", Booking_PickupLocation = "", Booking_DropoffLocation = "", Booking_GrandTotal = "";
                            if (jsonObject != null) {
                                if (jsonObject.has("BookingId")) {
                                    BookingId = jsonObject.getString("BookingId");
                                }
                                if (jsonObject.has("message")) {
                                    Booking_message = jsonObject.getString("message");
                                }
                               /* if (jsonObject.has("type")) {
                                    Booking_Type = jsonObject.getString("type");
                                }*/
                                if (jsonObject.has("PickupLocation")) {
                                    Booking_PickupLocation = jsonObject.getString("PickupLocation");
                                    SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LOCATION, Booking_PickupLocation, activity);
                                }
                                if (jsonObject.has("DropoffLocation")) {
                                    Booking_DropoffLocation = jsonObject.getString("DropoffLocation");
                                    SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LOCATION, Booking_DropoffLocation, activity);
                                }
                                if (jsonObject.has("GrandTotal")) {
                                    Booking_GrandTotal = jsonObject.getString("GrandTotal");
                                }
                                if (jsonObject.has("BookingType")) {
                                    Booking_Type = jsonObject.getString("BookingType");
                                } else {
                                    Booking_Type = "";
                                }
                                Log.e("kkkkkkkkk", "BookingId : " + BookingId
                                        + "\n Booking_message: " + Booking_message
                                        + "\n Booking_PickupLocation: " + Booking_PickupLocation
                                        + "\n Booking_DropoffLocation: " + Booking_DropoffLocation
                                        + "\n Booking_GrandTotal: " + Booking_GrandTotal
                                        + "\n Booking_BookingType: " + Booking_Type);
                            }
                            Log.e("TiCKTOC_Driver_App", "setCurrentActivity : " + TiCKTOC_Driver_Application.currentActivity().getLocalClassName());

                            if (TiCKTOC_Driver_Application.currentActivity() != null) {
                                OpenDialog_ReceiveBookingRequest(TiCKTOC_Driver_Application.currentActivity(), BookingId, Booking_message, "onReceiveBooking_Request", Booking_Type, Booking_GrandTotal);
                            } else {
                                OpenDialog_ReceiveBookingRequest(activity, BookingId, Booking_message, "onReceiveBooking_Request", Booking_Type, Booking_GrandTotal);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    };

    private Emitter.Listener onBookLaterDriverNotify = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("kkkkkkkkk", "kkkkkkkkkkk onBookLaterDriverNotify args.length :" + args.length);
                    if (args.length > 0 && args[0] != null) {
                        SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "0", activity);
                        Log.e("kkkkkkkkk", "kkkkkkkkkkk onBookLaterDriverNotify args[0] :" + args[0]);
                        try {
                            JSONObject jsonObject = new JSONObject(args[0].toString());
                            String Booking_message = "New Book Later request arrived from TiCKTOC";
                            if (jsonObject != null) {
                                if (jsonObject.has("message")) {
                                    Booking_message = jsonObject.getString("message");
                                }
                                Log.e("kkkkkkkkk", "message message: " + Booking_message);
                            }
                            Log.e("TiCKTOC_Driver_App", "setCurrentActivity : " + TiCKTOC_Driver_Application.currentActivity().getLocalClassName());

                            final Dialog dialog;
                            ringTone = MediaPlayer.create(activity, R.raw.request_ring);
                            ringTone.setLooping(false);
                            ringTone.start();

                            if (TiCKTOC_Driver_Application.currentActivity() != null) {
                                dialog = new Dialog(TiCKTOC_Driver_Application.currentActivity(), R.style.DialogTheme);
                            } else {
                                dialog = new Dialog(activity, R.style.DialogTheme);
                            }
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            lp.copyFrom(dialog.getWindow().getAttributes());
                            lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
                            lp.height = LinearLayout.LayoutParams.MATCH_PARENT;
                            dialog.getWindow().setAttributes(lp);
                            dialog.setCancelable(true);
                            dialog.setContentView(R.layout.dialog_receive_cancel_trip);

                            TextView tv_dialog_ok, tv_title, tv_message;

                            tv_title = (TextView) dialog.findViewById(R.id.tv_title);
                            tv_title.setVisibility(View.VISIBLE);
                            tv_dialog_ok = (TextView) dialog.findViewById(R.id.tv_dialog_ok);
                            tv_message = (TextView) dialog.findViewById(R.id.tv_message);

                            tv_title.setText("iiRide");
                            tv_message.setText(Booking_message);

                            tv_dialog_ok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    ringTone.stop();
                                    SessionSave.saveUserSession(Comman.NOTIFICATION_PUT_EXTRA, "BookLaterDriverNotify", getApplicationContext());
                                    dialog.dismiss();
                                    /*if(TiCKTOC_Driver_Application.currentActivity() instanceof Drawer_Activity)
                                    {
                                        setTab_MyJob();
                                        MyJob_Fragment.Call_SubFragment();
                                    }
                                    else
                                    {

                                    }*/

                                    intent = new Intent(TiCKTOC_Driver_Application.currentActivity(), Drawer_Activity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                    startActivity(intent);

                                    setTab_MyJob();
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            MyJob_Fragment.Call_SubFragment();
                                        }
                                    }, 1000);

                                }
                            });
                            dialog.show();

                        } catch (JSONException e) {
                            if (ringTone != null) {
                                ringTone.stop();
                            }
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    };

    private Emitter.Listener onSessionError = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("kkkkkkkkk", "kkkkkkkkkkk onSessionError args.length :" + args.length);
                    SessionSave.clearUserSession(activity);

                    final Dialog dialog;
                    if (TiCKTOC_Driver_Application.currentActivity() != null) {
                        dialog = new Dialog(TiCKTOC_Driver_Application.currentActivity(), R.style.DialogTheme);
                    } else {
                        dialog = new Dialog(activity, R.style.DialogTheme);
                    }
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    lp.copyFrom(dialog.getWindow().getAttributes());
                    lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
                    lp.height = LinearLayout.LayoutParams.MATCH_PARENT;
                    dialog.getWindow().setAttributes(lp);
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.dialog_receive_cancel_trip);

                    TextView tv_dialog_ok, tv_title, tv_message;

                    tv_title = (TextView) dialog.findViewById(R.id.tv_title);
                    tv_title.setVisibility(View.VISIBLE);
                    tv_dialog_ok = (TextView) dialog.findViewById(R.id.tv_dialog_ok);
                    tv_message = (TextView) dialog.findViewById(R.id.tv_message);

                    tv_title.setText("Session Expire");
                    tv_message.setText(getResources().getString(R.string.session_expire));

                    tv_dialog_ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            intent = new Intent(TiCKTOC_Driver_Application.currentActivity(), Main_Activity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
            });
        }
    };

    private Emitter.Listener onReceiveBookLater_Request = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("kkkkkkkkk", "kkkkkkkkkkk onReceiveBookLater_Request args.length :" + args.length);
                    if (args.length > 0 && args[0] != null) {
                        SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "1", activity);
                        Log.e("kkkkkkkkk", "kkkkkkkkkkk onReceiveBookLater_Request args[0] :" + args[0]);
                        try {
                            JSONObject jsonObject = new JSONObject(args[0].toString());
                            String BookingId = "", Booking_message = "";
                            if (jsonObject != null) {
                                if (jsonObject.has("BookingId")) {
                                    BookingId = jsonObject.getString("BookingId");
                                }
                                if (jsonObject.has("message")) {
                                    Booking_message = jsonObject.getString("message");
                                }
                                Log.e("kkkkkkkkk", "BookingId : " + BookingId + " Booking_message: " + Booking_message);
                            }
                            Log.e("TiCKTOC_Driver_App", "setCurrentActivity : " + TiCKTOC_Driver_Application.currentActivity().getLocalClassName());

                            if (TiCKTOC_Driver_Application.currentActivity() != null) {
                                OpenDialog_ReceiveBookingRequest(TiCKTOC_Driver_Application.currentActivity(), BookingId, Booking_message, "onReceiveBookLater_Request", "", "");
                            } else {
                                OpenDialog_ReceiveBookingRequest(activity, BookingId, Booking_message, "onReceiveBookLater_Request", "", "");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    };


    private Emitter.Listener onReceiveBookingInfo = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("kkkkkkkkk", "kkkkkkkkkkk onReceiveBookingInfo args.length :" + args.length);
                    if (args.length > 0 && args[0] != null) {
                        Log.e("kkkkkkkkk", "kkkkkkkkkkk onReceiveBookingInfo args[0] :" + args[0]);

                        try {
                            JSONObject jsonObject = new JSONObject(args[0].toString());
                            String BookingId = "", PassengerId = "", DropOffLat = "", DropOffLon = "", DropoffLocation = "", PickupLat = "", PickupLng = "", PickupLocation = "", Status = "", Notes = "", BookingType = "";
                            if (jsonObject != null) {
                                if (jsonObject.has("BookingInfo")) {
                                    JSONArray bookingInfoArray = jsonObject.getJSONArray("BookingInfo");
                                    for (int i = 0; i < bookingInfoArray.length(); i++) {
                                        JSONObject bookingInfoObject = bookingInfoArray.getJSONObject(i);
                                        if (bookingInfoObject != null) {
                                            if (bookingInfoObject.has("Id")) {
                                                BookingId = bookingInfoObject.getString("Id");
                                                SessionSave.saveUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, BookingId, activity);
                                            }
                                            if (bookingInfoObject.has("PassengerId")) {
                                                PassengerId = bookingInfoObject.getString("PassengerId");
                                                SessionSave.saveUserSession(Comman.PASSENGER_ID, PassengerId, activity);
                                            }
                                            if (bookingInfoObject.has("DropOffLat")) {
                                                DropOffLat = bookingInfoObject.getString("DropOffLat");
                                                if (DropOffLat != null && !DropOffLat.equalsIgnoreCase("")) {
                                                    SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LAT, DropOffLat, activity);
                                                } else {
                                                    SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LAT, "0", activity);
                                                }
                                            } else {
                                                SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LAT, "0", activity);
                                            }
                                            if (bookingInfoObject.has("DropOffLon")) {
                                                DropOffLon = bookingInfoObject.getString("DropOffLon");
                                                if (DropOffLon != null && !DropOffLon.equalsIgnoreCase("")) {
                                                    SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LNG, DropOffLon, activity);
                                                } else {
                                                    SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LNG, "0", activity);
                                                }
                                            } else {
                                                SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LNG, "0", activity);
                                            }
                                            if (bookingInfoObject.has("DropoffLocation")) {
                                                DropoffLocation = bookingInfoObject.getString("DropoffLocation");
                                                SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LOCATION, DropoffLocation, activity);
                                            }
                                            if (bookingInfoObject.has("PickupLat")) {
                                                PickupLat = bookingInfoObject.getString("PickupLat");
                                                if (PickupLat != null && !PickupLat.equalsIgnoreCase("")) {
                                                    SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LAT, PickupLat, activity);
                                                } else {
                                                    SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LAT, "0", activity);
                                                }
                                            } else {
                                                SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LAT, "0", activity);
                                            }
                                            if (bookingInfoObject.has("PickupLng")) {
                                                PickupLng = bookingInfoObject.getString("PickupLng");
                                                if (PickupLng != null && !PickupLng.equalsIgnoreCase("")) {
                                                    SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LNG, PickupLng, activity);
                                                } else {
                                                    SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LNG, "0", activity);
                                                }
                                            } else {
                                                SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LNG, "0", activity);
                                            }
                                            if (bookingInfoObject.has("PickupLocation")) {
                                                PickupLocation = bookingInfoObject.getString("PickupLocation");
                                                SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LOCATION, PickupLocation, activity);
                                            }
                                            if (bookingInfoObject.has("Status")) {
                                                Status = bookingInfoObject.getString("Status");
                                            }
                                            if (bookingInfoObject.has("BookingType")) {
                                                BookingType = bookingInfoObject.getString("BookingType");
                                                SessionSave.saveUserSession(Comman.PASSENGER_BOOKING_TYPE, BookingType, activity);
                                            } else {
                                                SessionSave.saveUserSession(Comman.PASSENGER_BOOKING_TYPE, "", activity);
                                            }

                                            if (bookingInfoObject.has("Notes")) {
                                                Notes = bookingInfoObject.getString("Notes");
                                                SessionSave.saveUserSession(Comman.PASSENGER_NOTE, Notes, activity);
                                            } else {
                                                SessionSave.saveUserSession(Comman.PASSENGER_NOTE, "", activity);
                                            }
                                            Log.e("kkkkkkkkk", "BookingId : " + BookingId
                                                    + "\n PassengerId: " + PassengerId
                                                    + "\n DropOffLat: " + DropOffLat
                                                    + "\n DropOffLon: " + DropOffLon
                                                    + "\n DropoffLocation: " + DropoffLocation
                                                    + "\n PickupLat: " + PickupLat
                                                    + "\n PickupLng: " + PickupLng
                                                    + "\n PickupLocation: " + PickupLocation
                                                    + "\n Status: " + Status
                                                    + "\n Notes: " + Notes);
                                        }
                                    }
                                    if (jsonObject.has("PassengerInfo")) {
                                        String PassengerInfo = jsonObject.getString("PassengerInfo");
                                        if (PassengerInfo != null && !PassengerInfo.equalsIgnoreCase("")) {
                                            JSONArray passInfoArray = jsonObject.getJSONArray("PassengerInfo");
                                            for (int i = 0; i < passInfoArray.length(); i++) {
                                                JSONObject passInfoObj = passInfoArray.getJSONObject(i);
                                                if (passInfoObj.has("Fullname")) {
                                                    String Fullname = passInfoObj.getString("Fullname");
                                                    SessionSave.saveUserSession(Comman.PASSENGER_NAME, Fullname, activity);
                                                }
                                                if (passInfoObj.has("MobileNo")) {
                                                    String MobileNo = passInfoObj.getString("MobileNo");
                                                    SessionSave.saveUserSession(Comman.PASSENGER_MOBILE_NO, MobileNo, activity);
                                                }
                                                if (passInfoObj.has("Image")) {
                                                    String Image = WebServiceAPI.BASE_URL_IMAGE + passInfoObj.getString("Image");
                                                    SessionSave.saveUserSession(Comman.PASSENGER_IMAGE, Image, activity);
                                                }
                                            }

                                            Log.e("TiCKTOC_Driver_App", "TiCKTOC_Driver_Application.currentActivity() : " + TiCKTOC_Driver_Application.currentActivity());
                                            if (TiCKTOC_Driver_Application.currentActivity() instanceof Drawer_Activity) {
                                                Log.e("TiCKTOC_Driver_App", "");
                                            } else {
                                                Intent intent = new Intent(TiCKTOC_Driver_Application.currentActivity(), Drawer_Activity.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                                startActivity(intent);
                                            }

                                            setTab_Home();
                                            Map_Fragment map_fragment = new Map_Fragment();
                                            map_fragment.SetDriverToPickUpLocation();
                                        }
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    };

    private Emitter.Listener onReceiveBookingInfoLater = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("kkkkkkkkk", "onReceiveBookingInfoLater args.length :" + args.length);
                    if (args.length > 0 && args[0] != null) {
                        Log.e("kkkkkkkkk", "onReceiveBookingInfoLater args[0] :" + args[0]);

                        try {
                            JSONObject jsonObject = new JSONObject(args[0].toString());
                            String BookingId = "", PassengerId = "", DropOffLat = "", DropOffLon = "", DropoffLocation = "", PickupLat = "", PickupLng = "", PickupLocation = "", Status = "", FlightNumber = "", Notes = "", PassengerType = "", PassengerContact = "", PaymentType = "", BookingType = "";
                            if (jsonObject != null) {
                                if (jsonObject.has("BookingInfo")) {
                                    JSONArray bookingInfoArray = jsonObject.getJSONArray("BookingInfo");
                                    for (int i = 0; i < bookingInfoArray.length(); i++) {
                                        JSONObject bookingInfoObject = bookingInfoArray.getJSONObject(i);
                                        if (bookingInfoObject != null) {
                                            if (bookingInfoObject.has("Id")) {
                                                BookingId = bookingInfoObject.getString("Id");
                                                SessionSave.saveUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, BookingId, activity);
                                            }
                                            if (bookingInfoObject.has("PassengerId")) {
                                                PassengerId = bookingInfoObject.getString("PassengerId");
                                                SessionSave.saveUserSession(Comman.PASSENGER_ID, PassengerId, activity);
                                            }
                                            if (bookingInfoObject.has("DropOffLat")) {
                                                DropOffLat = bookingInfoObject.getString("DropOffLat");
                                                SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LAT, DropOffLat, activity);
                                            }
                                            if (bookingInfoObject.has("DropOffLon")) {
                                                DropOffLon = bookingInfoObject.getString("DropOffLon");
                                                SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LNG, DropOffLon, activity);
                                            }
                                            if (bookingInfoObject.has("DropoffLocation")) {
                                                DropoffLocation = bookingInfoObject.getString("DropoffLocation");
                                                SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LOCATION, DropoffLocation, activity);
                                            }
                                            if (bookingInfoObject.has("PickupLat")) {
                                                PickupLat = bookingInfoObject.getString("PickupLat");
                                                SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LAT, PickupLat, activity);
                                            }
                                            if (bookingInfoObject.has("PickupLng")) {
                                                PickupLng = bookingInfoObject.getString("PickupLng");
                                                SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LNG, PickupLng, activity);
                                            }
                                            if (bookingInfoObject.has("PickupLocation")) {
                                                PickupLocation = bookingInfoObject.getString("PickupLocation");
                                                SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LOCATION, PickupLocation, activity);
                                            }
                                            if (bookingInfoObject.has("Status")) {
                                                Status = bookingInfoObject.getString("Status");
                                            }

                                            if (bookingInfoObject.has("PaymentType")) {
                                                PaymentType = bookingInfoObject.getString("PaymentType");
                                                SessionSave.saveUserSession(Comman.PASSENGER_PAYMENT_TYPE, PaymentType, activity);
                                            } else {
                                                SessionSave.saveUserSession(Comman.PASSENGER_PAYMENT_TYPE, "", activity);
                                            }

                                            if (bookingInfoObject.has("PassengerType")) {
                                                PassengerType = bookingInfoObject.getString("PassengerType");
                                                SessionSave.saveUserSession(Comman.PASSENGER_TYPE, PassengerType, activity);
                                            } else {
                                                SessionSave.saveUserSession(Comman.PASSENGER_TYPE, "", activity);
                                            }

                                            if (bookingInfoObject.has("PassengerName")) {
                                                String Fullname = bookingInfoObject.getString("PassengerName");
                                                SessionSave.saveUserSession(Comman.PASSENGER_NAME, Fullname, Drawer_Activity.activity);
                                            }

                                            if (bookingInfoObject.has("PassengerContact")) {
                                                PassengerContact = bookingInfoObject.getString("PassengerContact");
                                                SessionSave.saveUserSession(Comman.PASSENGER_MOB_NO_OTHER, PassengerContact, activity);
                                            }

                                            if (bookingInfoObject.has("BookingType")) {
                                                BookingType = bookingInfoObject.getString("BookingType");
                                                SessionSave.saveUserSession(Comman.PASSENGER_BOOKING_TYPE, BookingType, activity);
                                            } else {
                                                SessionSave.saveUserSession(Comman.PASSENGER_BOOKING_TYPE, "", activity);
                                            }

                                            if (bookingInfoObject.has("FlightNumber")) {
                                                FlightNumber = bookingInfoObject.getString("FlightNumber");
                                                SessionSave.saveUserSession(Comman.PASSENGER_FLIGHT_NO, FlightNumber, activity);
                                            } else {
                                                SessionSave.saveUserSession(Comman.PASSENGER_FLIGHT_NO, "", activity);
                                            }

                                            if (bookingInfoObject.has("Notes")) {
                                                Notes = bookingInfoObject.getString("Notes");
                                                SessionSave.saveUserSession(Comman.PASSENGER_NOTE, Notes, activity);
                                            } else {
                                                SessionSave.saveUserSession(Comman.PASSENGER_NOTE, "", activity);
                                            }

                                            Log.e("kkkkkkkkk", "BookingId : " + BookingId
                                                    + "\n PassengerId: " + PassengerId
                                                    + "\n DropOffLat: " + DropOffLat
                                                    + "\n DropOffLon: " + DropOffLon
                                                    + "\n DropoffLocation: " + DropoffLocation
                                                    + "\n PickupLat: " + PickupLat
                                                    + "\n PickupLng: " + PickupLng
                                                    + "\n PickupLocation: " + PickupLocation
                                                    + "\n Status: " + Status
                                                    + "\n FlightNumber: " + FlightNumber
                                                    + "\n Notes: " + Notes
                                                    + "\n PassengerType: " + PassengerType);
                                        }
                                    }
                                    if (PassengerType != null && !PassengerType.equalsIgnoreCase("") && (PassengerType.equalsIgnoreCase("others") || PassengerType.equalsIgnoreCase("other"))) {

                                    } else {
                                        if (jsonObject.has("PassengerInfo")) {
                                            String PassengerInfo = jsonObject.getString("PassengerInfo");
                                            if (PassengerInfo != null && !PassengerInfo.equalsIgnoreCase("")) {
                                                JSONArray passInfoArray = jsonObject.getJSONArray("PassengerInfo");
                                                for (int i = 0; i < passInfoArray.length(); i++) {
                                                    JSONObject passInfoObj = passInfoArray.getJSONObject(i);
                                                    if (passInfoObj.has("Fullname")) {
                                                        String Fullname = passInfoObj.getString("Fullname");
                                                        SessionSave.saveUserSession(Comman.PASSENGER_NAME, Fullname, activity);
                                                    }
                                                    if (passInfoObj.has("MobileNo")) {
                                                        String MobileNo = passInfoObj.getString("MobileNo");
                                                        SessionSave.saveUserSession(Comman.PASSENGER_MOBILE_NO, MobileNo, activity);
                                                    }
                                                    if (passInfoObj.has("Image")) {
                                                        String image = passInfoObj.getString("Image");
                                                        SessionSave.saveUserSession(Comman.PASSENGER_IMAGE, image, activity);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    Log.e("TiCKTOC_Driver_App", "TiCKTOC_Driver_Application.currentActivity() : " + TiCKTOC_Driver_Application.currentActivity());
                                    if (TiCKTOC_Driver_Application.currentActivity() instanceof Drawer_Activity) {
                                        Log.e("TiCKTOC_Driver_App", "");
                                    } else {
                                        Intent intent = new Intent(TiCKTOC_Driver_Application.currentActivity(), Drawer_Activity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                        startActivity(intent);
                                    }

                                    setTab_Home();
                                    ExpandBottomShit();
                                    map_fragment.SetDriverToPickUpLocation();
                                    //Map_Fragment map_fragment = new Map_Fragment();

                                    /*if(map_fragment != null && map_fragment.isAdded())
                                    {map_fragment.SetDriverToPickUpLocation();}
                                    else
                                    {setTab_Home();}*/
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    };

    private Emitter.Listener onReceive_CancelTrip_Notification = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("kkkkkkkkk", " onReceive_CancelTrip_Notification args.length :" + args.length);
                    ////{"message":"Trip has been canceled by passenger"}
                    SessionSave.saveUserSession(Comman.ACCEPT_REQUEST_FLAG, "0", activity);
                    SessionSave.saveUserSession(Comman.START_TRIP_FLAG, "0", activity);
                    SessionSave.saveUserSession(Comman.STOP_WAITING, "0", activity);
                    SessionSave.saveUserSession(Comman.START_WAITING, "0", activity);
                    SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "", activity);

                    if (args.length > 0 && args[0] != null) {
                        Log.e("kkkkkkkkk", " onReceive_CancelTrip_Notification args[0] :" + args[0]);
                        if (TiCKTOC_Driver_Application.currentActivity() != null) {
                            OpenDialog_CancelTrip_Notification(TiCKTOC_Driver_Application.currentActivity());
                        } else {
                            OpenDialog_CancelTrip_Notification(activity);
                        }

                    }
                }
            });
        }
    };

    private Emitter.Listener onUpdatedBookingDetail = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Log.e("kkkkkkkkk", " onUpdatedBookingDetail");

                    if (args.length > 0 && args[0] != null)
                    {
                        Log.e("kkkkkkkkk", " onUpdatedBookingDetail args[0] :" + args[0]);
                        try
                        {
                            String passengerType="";

                            JSONObject jsonObject = new JSONObject(args[0].toString());
                            String BookingId = "", PassengerId = "", DropOffLat = "", DropOffLon = "", DropoffLocation = "", PickupLat = "", PickupLng = "", PickupLocation = "", Status = "", Notes = "", BookingType = "";
                            if (jsonObject != null)
                            {
                                if (jsonObject.has("BookingInfo"))
                                {
                                    JSONArray bookingInfoArray = jsonObject.getJSONArray("BookingInfo");
                                    for (int i = 0; i < bookingInfoArray.length(); i++)
                                    {
                                        JSONObject bookingInfoObject = bookingInfoArray.getJSONObject(i);
                                        if (bookingInfoObject != null)
                                        {
                                            if (bookingInfoObject.has("Id"))
                                            {
                                                BookingId = bookingInfoObject.getString("Id");
                                                SessionSave.saveUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, BookingId, activity);
                                            }
                                            if (bookingInfoObject.has("PassengerId"))
                                            {
                                                PassengerId = bookingInfoObject.getString("PassengerId");
                                                SessionSave.saveUserSession(Comman.PASSENGER_ID, PassengerId, activity);
                                            }

                                            if (bookingInfoObject.has("PassengerType") &&
                                                    bookingInfoObject.getString("PassengerType")!=null &&
                                            bookingInfoObject.getString("PassengerType").equalsIgnoreCase("other"))
                                            {
                                                passengerType = bookingInfoObject.getString("PassengerType");
                                                SessionSave.saveUserSession(Comman.PASSENGER_TYPE,passengerType, activity);
                                                if (bookingInfoObject.has("PassengerName"))
                                                {
                                                    String fullname = bookingInfoObject.getString("PassengerName");
                                                    SessionSave.saveUserSession(Comman.PASSENGER_NAME,fullname,activity);
                                                }

                                                if (bookingInfoObject.has("PassengerContact"))
                                                {
                                                    String number = bookingInfoObject.getString("PassengerContact");
                                                    SessionSave.saveUserSession(Comman.PASSENGER_MOBILE_NO,number,activity);
                                                    SessionSave.saveUserSession(Comman.PASSENGER_MOB_NO_OTHER,number,activity);
                                                }
                                                SessionSave.saveUserSession(Comman.PASSENGER_IMAGE,"",activity);
                                            }
                                            else
                                            {
                                                passengerType = "";
                                            }

                                            if (bookingInfoObject.has("DropOffLat"))
                                            {
                                                DropOffLat = bookingInfoObject.getString("DropOffLat");
                                                if (DropOffLat != null && !DropOffLat.equalsIgnoreCase(""))
                                                {
                                                    SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LAT, DropOffLat, activity);
                                                }
                                                else
                                                {
                                                    SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LAT, "0", activity);
                                                }
                                            }
                                            else
                                            {
                                                SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LAT, "0", activity);
                                            }
                                            if (bookingInfoObject.has("DropOffLon"))
                                            {
                                                DropOffLon = bookingInfoObject.getString("DropOffLon");
                                                if (DropOffLon != null && !DropOffLon.equalsIgnoreCase(""))
                                                {
                                                    SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LNG, DropOffLon, activity);
                                                }
                                                else
                                                {
                                                    SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LNG, "0", activity);
                                                }
                                            }
                                            else
                                            {
                                                SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LNG, "0", activity);
                                            }
                                            if (bookingInfoObject.has("DropoffLocation"))
                                            {
                                                DropoffLocation = bookingInfoObject.getString("DropoffLocation");
                                                SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LOCATION, DropoffLocation, activity);
                                            }
                                            if (bookingInfoObject.has("PickupLat"))
                                            {
                                                PickupLat = bookingInfoObject.getString("PickupLat");
                                                if (PickupLat != null && !PickupLat.equalsIgnoreCase(""))
                                                {
                                                    SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LAT, PickupLat, activity);
                                                }
                                                else
                                                {
                                                    SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LAT, "0", activity);
                                                }
                                            }
                                            else
                                            {
                                                SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LAT, "0", activity);
                                            }
                                            if (bookingInfoObject.has("PickupLng"))
                                            {
                                                PickupLng = bookingInfoObject.getString("PickupLng");
                                                if (PickupLng != null && !PickupLng.equalsIgnoreCase(""))
                                                {
                                                    SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LNG, PickupLng, activity);
                                                }
                                                else
                                                {
                                                    SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LNG, "0", activity);
                                                }
                                            }
                                            else
                                            {
                                                SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LNG, "0", activity);
                                            }
                                            if (bookingInfoObject.has("PickupLocation"))
                                            {
                                                PickupLocation = bookingInfoObject.getString("PickupLocation");
                                                SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LOCATION, PickupLocation, activity);
                                            }
                                            if (bookingInfoObject.has("Status"))
                                            {
                                                Status = bookingInfoObject.getString("Status");
                                            }
                                            if (bookingInfoObject.has("BookingType"))
                                            {
                                                BookingType = bookingInfoObject.getString("BookingType");
                                                SessionSave.saveUserSession(Comman.PASSENGER_BOOKING_TYPE, BookingType, activity);
                                            }
                                            else
                                            {
                                                SessionSave.saveUserSession(Comman.PASSENGER_BOOKING_TYPE, "", activity);
                                            }

                                            if (bookingInfoObject.has("Notes"))
                                            {
                                                Notes = bookingInfoObject.getString("Notes");
                                                SessionSave.saveUserSession(Comman.PASSENGER_NOTE, Notes, activity);
                                            }
                                            else
                                            {
                                                SessionSave.saveUserSession(Comman.PASSENGER_NOTE, "", activity);
                                            }
                                            Log.e("kkkkkkkkk", "BookingId : " + BookingId
                                                    + "\n PassengerId: " + PassengerId
                                                    + "\n DropOffLat: " + DropOffLat
                                                    + "\n DropOffLon: " + DropOffLon
                                                    + "\n DropoffLocation: " + DropoffLocation
                                                    + "\n PickupLat: " + PickupLat
                                                    + "\n PickupLng: " + PickupLng
                                                    + "\n PickupLocation: " + PickupLocation
                                                    + "\n Status: " + Status
                                                    + "\n Notes: " + Notes);
                                        }
                                    }

                                    if (passengerType!=null &&
                                            !passengerType.equalsIgnoreCase("other"))
                                    {
                                        if (jsonObject.has("PassengerInfo"))
                                        {
                                            String PassengerInfo = jsonObject.getString("PassengerInfo");
                                            if (PassengerInfo != null && !PassengerInfo.equalsIgnoreCase(""))
                                            {
                                                JSONArray passInfoArray = jsonObject.getJSONArray("PassengerInfo");
                                                for (int i = 0; i < passInfoArray.length(); i++)
                                                {
                                                    JSONObject passInfoObj = passInfoArray.getJSONObject(i);
                                                    if (passInfoObj.has("Fullname"))
                                                    {
                                                        String Fullname = passInfoObj.getString("Fullname");
                                                        SessionSave.saveUserSession(Comman.PASSENGER_NAME, Fullname, activity);
                                                    }
                                                    if (passInfoObj.has("MobileNo"))
                                                    {
                                                        String MobileNo = passInfoObj.getString("MobileNo");
                                                        SessionSave.saveUserSession(Comman.PASSENGER_MOBILE_NO, MobileNo, activity);
                                                    }
                                                    if (passInfoObj.has("Image"))
                                                    {
                                                        String Image = WebServiceAPI.BASE_URL_IMAGE + passInfoObj.getString("Image");
                                                        SessionSave.saveUserSession(Comman.PASSENGER_IMAGE, Image, activity);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    Log.e("TiCKTOC_Driver_App", "TiCKTOC_Driver_Application.currentActivity() : " + TiCKTOC_Driver_Application.currentActivity());
                                    if (TiCKTOC_Driver_Application.currentActivity() instanceof Drawer_Activity)
                                    {
                                        Log.e("TiCKTOC_Driver_App", "");
                                    }
                                    else
                                    {
                                        Intent intent = new Intent(TiCKTOC_Driver_Application.currentActivity(), Drawer_Activity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                        startActivity(intent);
                                    }
                                    setTab_Home();
                                    Map_Fragment map_fragment = new Map_Fragment();
                                    if (SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity).equalsIgnoreCase("")
                                            && SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity).equalsIgnoreCase("1"))
                                    {
                                        map_fragment.SetDriverTo_DropOffLocation(1);
                                    }
                                    else
                                    {
                                        map_fragment.SetDriverToPickUpLocation();
                                    }
                                }
                            }
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    };

    private Emitter.Listener onReceive_CancelTrip_Notification_Later = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("kkkkkkkkk", " onReceive_CancelTrip_Notification args.length :" + args.length);
                    ////{"message":"Trip has been canceled by passenger"}
                    SessionSave.saveUserSession(Comman.ACCEPT_REQUEST_FLAG, "0", activity);
                    SessionSave.saveUserSession(Comman.START_TRIP_FLAG, "0", activity);
                    SessionSave.saveUserSession(Comman.STOP_WAITING, "0", activity);
                    SessionSave.saveUserSession(Comman.START_WAITING, "0", activity);
                    SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "", activity);

                    if (args.length > 0 && args[0] != null) {
                        Log.e("kkkkkkkkk", " onReceive_CancelTrip_Notification args[0] :" + args[0]);
                        if (TiCKTOC_Driver_Application.currentActivity() != null) {
                            OpenDialog_CancelTrip_Notification(TiCKTOC_Driver_Application.currentActivity());
                        } else {
                            OpenDialog_CancelTrip_Notification(activity);
                        }
                    }
                }
            });
        }
    };


    /*public void startTimer() {
        runnable = new Runnable() {

            @Override
            public void run() {
                try {
                    if (Comman.socket != null && Comman.socket.connected()) {
                        // Sending an object
                        JSONObject obj = new JSONObject();
                        try {
                            Log.e("kkkkkkkkk", "SessionSave.getUserSession(Comman.USER_ID, activity) : " + SessionSave.getUserSession(Comman.USER_ID, activity));
                            obj.put(WebServiceAPI.SOCKET_UPDATE_DRIVER_LOACATION_KEY_OBJ_DRIVER_ID, SessionSave.getUserSession(Comman.USER_ID, activity));
                            obj.put(WebServiceAPI.SOCKET_UPDATE_DRIVER_LOACATION_KEY_OBJ_DRIVER_LAT, Constants.newgpsLatitude);
                            obj.put(WebServiceAPI.SOCKET_UPDATE_DRIVER_LOACATION_KEY_OBJ_DRIVER_LNG, Constants.newgpsLongitude);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Comman.socket.emit(WebServiceAPI.SERVER_SOCKET_UPDATE_DRIVER_LOACATION_KEY, obj);
                    } else {
                        Log.e("call", "socket is not connected.......");
                    }
                    handler1.postDelayed(runnable, 5000);
                } catch (Exception e) {
                    Log.e("call", "error in sending latlong");
                }
            }
        };
        handler1.postDelayed(runnable, 0);
    }*/
    public void startTimer() {
        Log.e("startTimer", "startTimer 111111111111");
        String UserValidStatus = SessionSave.getUserSession(Comman.USER_DRIVER_DUTY, activity);
        if (UserValidStatus != null && !UserValidStatus.equalsIgnoreCase("") && UserValidStatus.equalsIgnoreCase("1")) {
            try {
                if (Comman.socket != null && Comman.socket.connected()) {
                    // Sending an object
                    JSONObject obj = new JSONObject();
                    try {
                        Log.e("startTimer", "SessionSave.getUserSession(Comman.USER_ID, activity) : " + SessionSave.getUserSession(Comman.USER_ID, activity));
                        obj.put(WebServiceAPI.SOCKET_UPDATE_DRIVER_LOACATION_KEY_OBJ_DRIVER_ID, SessionSave.getUserSession(Comman.USER_ID, activity));
                        obj.put(WebServiceAPI.SOCKET_UPDATE_DRIVER_LOACATION_KEY_OBJ_DRIVER_LAT, Constants.newgpsLatitude);
                        obj.put(WebServiceAPI.SOCKET_UPDATE_DRIVER_LOACATION_KEY_OBJ_DRIVER_LNG, Constants.newgpsLongitude);
                        String Token = SessionSave.getToken(Comman.DEVICE_TOKEN, activity);
                        if (Token != null && !Token.equalsIgnoreCase("")) {
                            obj.put(WebServiceAPI.SOCKET_UPDATE_DRIVER_LOACATION_KEY_OBJ_TOKEN, Token);
                            Comman.socket.emit(WebServiceAPI.SERVER_SOCKET_UPDATE_DRIVER_LOACATION_KEY, obj);
                        }
                        Log.e("startTimer", "Token : " + Token + "\nObject : " + obj.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e("startTimer call", "socket is not connected.......");
                }
            } catch (Exception e) {
                Log.e("startTimer call", "error in sending latlong");
            }
        } else {
            Log.e("startTimer", "startTimer 333333333333");
        }
    }

    public void OpenDialog_ReceiveBookingRequest(Activity activity, final String bookingId, String booking_message, final String requestType, String Booking_Type, String Booking_GrandTotal) {
        if (requestDialogOntimeVisible == 0) {
            ringTone = MediaPlayer.create(activity, R.raw.request_ring);
            ringTone.setLooping(true);
            ringTone.start();
            requestDialogOntimeVisible = 1;
            final Dialog dialogBookingRequest = new Dialog(activity, R.style.DialogTheme);
            dialogBookingRequest.requestWindowFeature(Window.FEATURE_NO_TITLE);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialogBookingRequest.getWindow().getAttributes());
            lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
            lp.height = LinearLayout.LayoutParams.MATCH_PARENT;
            dialogBookingRequest.getWindow().setAttributes(lp);
            dialogBookingRequest.setCancelable(false);
            dialogBookingRequest.setContentView(R.layout.dialog_receive_request);

            final TextView tv_dialog_reject, tv_dialog_accept, tv_message, tv_count, tv_GrandTotal, tv_ReqPickUp, tv_ReqDropOff;
            final RelativeLayout rl_pickUpDropOff;

            tv_count = (TextView) dialogBookingRequest.findViewById(R.id.tv_count);
            tv_message = (TextView) dialogBookingRequest.findViewById(R.id.tv_message);
            tv_message.setText(booking_message);
            tv_dialog_reject = (TextView) dialogBookingRequest.findViewById(R.id.tv_dialog_reject);
            tv_dialog_accept = (TextView) dialogBookingRequest.findViewById(R.id.tv_dialog_accept);
            tv_GrandTotal = (TextView) dialogBookingRequest.findViewById(R.id.tv_GrandTotal);
            tv_ReqPickUp = (TextView) dialogBookingRequest.findViewById(R.id.tv_ReqPickUp);
            tv_ReqDropOff = (TextView) dialogBookingRequest.findViewById(R.id.tv_ReqDropOff);
            rl_pickUpDropOff = (RelativeLayout) dialogBookingRequest.findViewById(R.id.rl_pickUpDropOff);

            if (Booking_Type != null && !Booking_Type.equalsIgnoreCase("") && Booking_Type.equalsIgnoreCase("dispatch")) {
                rl_pickUpDropOff.setVisibility(View.VISIBLE);
                tv_GrandTotal.setVisibility(View.VISIBLE);
                if (Booking_GrandTotal != null && !Booking_GrandTotal.equalsIgnoreCase("")) {
                    tv_GrandTotal.setText("Grand Total is $" + Booking_GrandTotal);
                }

                String pickUp = SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LOCATION, activity);
                String dropOff = SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LOCATION, activity);

                if (pickUp != null && !pickUp.equalsIgnoreCase("")) {
                    tv_ReqPickUp.setText(pickUp);
                }
                if (dropOff != null && !dropOff.equalsIgnoreCase("")) {
                    tv_ReqDropOff.setText(dropOff);
                }
            } else {
                rl_pickUpDropOff.setVisibility(View.GONE);
                tv_GrandTotal.setVisibility(View.GONE);
            }

            CircularProgressBar circularProgressBar = (CircularProgressBar) dialogBookingRequest.findViewById(R.id.yourCircularProgressbar);

            circularProgressBar.setColor(ContextCompat.getColor(this, R.color.colorRed));
            circularProgressBar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorTransparentHalfRed));
            circularProgressBar.setProgressBarWidth(17);
            circularProgressBar.setBackgroundProgressBarWidth(10);
            int animationDuration = 31000; // 2500ms = 2,5s
            circularProgressBar.setProgress(0);
            circularProgressBar.setProgressWithAnimation(100, animationDuration);
            count = 30;
            handler.postDelayed(new Runnable() {
                public void run() {
                    if (count > 0) {
                        tv_count.setText(count + "");
                        count--;
                        handler.postDelayed(this, 1000);
                    } else {
                        tv_count.setText("0");
                        handler.removeCallbacksAndMessages(null);
                        requestDialogOntimeVisible = 0;
                        RequestRejected(bookingId, requestType);
                        ringTone.stop();
                        dialogBookingRequest.dismiss();
                    }

                }
            }, 1000);

            tv_dialog_reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    handler.removeCallbacksAndMessages(null);
                    requestDialogOntimeVisible = 0;
                    RequestRejected(bookingId, requestType);
                    ringTone.stop();
                    dialogBookingRequest.dismiss();
                }
            });

            tv_dialog_accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    handler.removeCallbacksAndMessages(null);
                    RequestAccepted(bookingId, requestType);
                    ringTone.stop();
                    dialogBookingRequest.dismiss();
                }
            });

            dialogBookingRequest.show();
        } else {
            requestDialogOntimeVisible = 1;
            RequestRejected(bookingId, requestType);
        }
    }

    private void RequestAccepted(String BookingId, String requestType) {
        requestDialogOntimeVisible = 0;
        try {
            if (Comman.socket != null && Comman.socket.connected()) {
                // Sending an object
                JSONObject obj = new JSONObject();
                if (requestType != null && !requestType.equalsIgnoreCase("")) {
                    if (requestType.equalsIgnoreCase("onReceiveBooking_Request")) {
                        try {
                            gpsTracker = new GPSTracker(activity);
                            gpsTracker.getLocation();

                            if (gpsTracker.getLatitude() != 0 && gpsTracker.getLongitude() != 0) {
                                Constants.newgpsLatitude = gpsTracker.getLatitude() + "";
                                Constants.newgpsLongitude = gpsTracker.getLongitude() + "";
                                Log.e("kkkkkkkkk", "RequestAccepted Comman.USER_ID : " + SessionSave.getUserSession(Comman.USER_ID, activity));
                                obj.put(WebServiceAPI.SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST_BOOKING_ID, BookingId);
                                obj.put(WebServiceAPI.SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST_DRIVER_ID, SessionSave.getUserSession(Comman.USER_ID, activity));
                                obj.put(WebServiceAPI.SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST_LAT, gpsTracker.getLatitude());
                                obj.put(WebServiceAPI.SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST_LONG, gpsTracker.getLongitude());

                                SessionSave.saveUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, BookingId, activity);
                                SessionSave.saveUserSession(Comman.ACCEPT_REQUEST_FLAG, "1", activity);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.e("RequestAccepted", "RequestAccepted");
                        Comman.socket.emit(WebServiceAPI.SERVER_SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST, obj);
                    } else {
                        try {
                            gpsTracker = new GPSTracker(activity);
                            gpsTracker.getLocation();

                            if (gpsTracker.getLatitude() != 0 && gpsTracker.getLongitude() != 0) {
                                Constants.newgpsLatitude = gpsTracker.getLatitude() + "";
                                Constants.newgpsLongitude = gpsTracker.getLongitude() + "";

                                Log.e("kkkkkkkkk", "RequestAccepted Comman.USER_ID : " + SessionSave.getUserSession(Comman.USER_ID, activity));
                                obj.put(WebServiceAPI.SOCKET_DRIVER_ACCEPT_ADVANCE_BOOKING_REQUEST_BOOKING_ID, BookingId);
                                obj.put(WebServiceAPI.SOCKET_DRIVER_ACCEPT_ADVANCE_BOOKING_REQUEST_DRIVER_ID, SessionSave.getUserSession(Comman.USER_ID, activity));
                                obj.put(WebServiceAPI.SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST_LAT, gpsTracker.getLatitude());
                                obj.put(WebServiceAPI.SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST_LONG, gpsTracker.getLongitude());

                                SessionSave.saveUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, BookingId, activity);
                                SessionSave.saveUserSession(Comman.ACCEPT_REQUEST_FLAG, "0", activity);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Comman.socket.emit(WebServiceAPI.SERVER_SOCKET_DRIVER_ACCEPT_ADVANCE_BOOKING_REQUEST, obj);
                    }
                }
            } else {
                Log.e("call", "socket is not connected.......");
            }
        } catch (Exception e) {
            Log.e("call", "error in sending latlong");
        }
    }

    private void RequestRejected(String BookingId, String requestType) {
        try {
            if (Comman.socket != null && Comman.socket.connected()) {
                // Sending an object
                JSONObject obj = new JSONObject();
                if (requestType != null && !requestType.equalsIgnoreCase("")) {
                    if (requestType.equalsIgnoreCase("onReceiveBooking_Request")) {
                        try {
                            Log.e("kkkkkkkkk", "RequestRejected Comman.USER_ID : " + SessionSave.getUserSession(Comman.USER_ID, activity));
                            obj.put(WebServiceAPI.SOCKET_DRIVER_REJECT_BOOKING_REQUEST_BOOKING_ID, BookingId);
                            obj.put(WebServiceAPI.SOCKET_DRIVER_REJECT_BOOKING_REQUEST_DRIVER_ID, SessionSave.getUserSession(Comman.USER_ID, activity));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Comman.socket.emit(WebServiceAPI.SERVER_SOCKET_DRIVER_REJECT_BOOKING_REQUEST, obj);
                    } else {
                        try {
                            Log.e("kkkkkkkkk", "RequestRejected Comman.USER_ID : " + SessionSave.getUserSession(Comman.USER_ID, activity));
                            obj.put(WebServiceAPI.SOCKET_DRIVER_REJECT_BOOK_LATER_REQUEST_BOOKING_ID, BookingId);
                            obj.put(WebServiceAPI.SOCKET_DRIVER_REJECT_BOOK_LATER_REQUEST_DRIVER_ID, SessionSave.getUserSession(Comman.USER_ID, activity));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Comman.socket.emit(WebServiceAPI.SERVER_SOCKET_DRIVER_REJECT_BOOK_LATER_REQUEST, obj);
                    }
                }
            } else {
                Log.e("call", "socket is not connected.......");
            }
        } catch (Exception e) {
            Log.e("call", "error in sending latlong");
        }
    }

    private void getCityName() {

        final InitData initData = SessionSave.getInitData(activity);
        GPSTracker gpsTracker = new GPSTracker(activity);

        Log.w("newgpsLatitude", "" + Constants.newgpsLatitude);
        Log.w("newgpsLatitude", "" + Constants.newgpsLongitude);
        if (!Constants.newgpsLatitude.equalsIgnoreCase("") && !Constants.newgpsLongitude.equalsIgnoreCase("") && !Constants.newgpsLatitude.equalsIgnoreCase("0") && !Constants.newgpsLongitude.equalsIgnoreCase("0")) {

            String urlLocation = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + Constants.newgpsLatitude + "," + Constants.newgpsLongitude + "&key=" + getString(R.string.api_key_google_map);
            Log.w("urlLocation", "" + urlLocation);
            dialogClass = new DialogClass(activity, 1);
            dialogClass.showDialog();
            aQuery.ajax(urlLocation.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {

                    try {
                        int responseCode = status.getCode();
                        Log.e("TAGCityFind", "responseCode = " + responseCode);
                        Log.e("TAGCityFind", "Response = " + json);
                        dialogClass.hideDialog();

                        JSONArray resultsAry = json.getJSONArray("results");
                        JSONObject jObj = new JSONObject(resultsAry.getString(0));
                        JSONArray addCompAry = jObj.getJSONArray("address_components");
                        String short_name = "";
                        int idxCity = -1;
                        boolean isContainsCity = false;

                        boolean isFoundCity = false;

                        for (int adComp = 0; adComp < addCompAry.length(); adComp++) {
                            JSONObject cityObj = addCompAry.getJSONObject(adComp);
                            JSONArray typesAry = cityObj.getJSONArray("types");

                            for (int j = 0; j < typesAry.length(); j++) {
                                if (typesAry.get(j).toString().equalsIgnoreCase("administrative_area_level_2")) {
                                    isContainsCity = true;
                                    idxCity = adComp;
                                    break;
                                }
                            }
                        }

                        for (int adComp = 0; adComp < addCompAry.length(); adComp++) {
                            JSONObject cityObj = addCompAry.getJSONObject(adComp);
                            JSONArray typesAry = cityObj.getJSONArray("types");
                            if (typesAry.length() > 1) {
                                if (/*typesAry.getString(0).equalsIgnoreCase("administrative_area_level_2")*/ isContainsCity && adComp == idxCity && typesAry.getString(1).equalsIgnoreCase("political")) {
                                    cityName = cityObj.getString("long_name");
                                    short_name = cityObj.getString("short_name");
                                    for (int i = 0; i < initData.getCityList().size(); i++) {
                                        String listCityName = initData.getCityList().get(i).getCityName().toLowerCase().trim();
                                        if (cityName.equalsIgnoreCase(listCityName)) {
                                            isFoundCity = true;
                                            cityId = initData.getCityList().get(i).getId();
                                            SessionSave.saveUserSession(Comman.USER_CITY_ID, cityId, activity);
                                            break;
                                        } else if (short_name.equalsIgnoreCase(listCityName)) {
                                            isFoundCity = true;
                                            cityName = short_name;
                                            cityId = initData.getCityList().get(i).getId();
                                            SessionSave.saveUserSession(Comman.USER_CITY_ID, cityId, activity);
                                            break;
                                        }
                                    }
                                    break;
                                } else if (!isContainsCity && typesAry.getString(0).equalsIgnoreCase("locality") && typesAry.getString(1).equalsIgnoreCase("political")) {
                                    // JSONObject cityObjFinal = new JSONObject(addCompAry.getString(1));
                                    cityName = cityObj.getString("long_name");
                                    for (int i = 0; i < initData.getCityList().size(); i++) {
                                        String listCityName = initData.getCityList().get(i).getCityName().toLowerCase().trim();
                                        if (cityName.equalsIgnoreCase(listCityName)) {
                                            cityId = initData.getCityList().get(i).getId();
                                            isFoundCity = true;
                                            SessionSave.saveUserSession(Comman.USER_CITY_ID, cityId, activity);
                                            break;
                                        } else if (short_name.equalsIgnoreCase(listCityName)) {
                                            cityName = short_name;
                                            isFoundCity = true;
                                            cityId = initData.getCityList().get(i).getId();
                                            SessionSave.saveUserSession(Comman.USER_CITY_ID, cityId, activity);
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                        }

                        if(!isFoundCity){
                            cityName = "Melbourne";
                            cityId = "4";
                            SessionSave.saveUserSession(Comman.USER_CITY_ID, cityId, activity);
                        }

                        Log.w("drawerActivityresultAry", "" + cityName);
                    } catch (JSONException e) {
                        Log.e("Exception", "Exception " + e.toString());
                        dialogClass.hideDialog();

                    }
                }

            }.method(AQuery.METHOD_GET));

        }
    }


    private void OpenDialog_CancelTrip_Notification(Activity activity) {
        Map_Fragment.clearMap();
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        final Dialog dialog = new Dialog(activity, R.style.DialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(lp);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_receive_cancel_trip);

        TextView tv_dialog_ok, tv_title, tv_message;

        tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        tv_title.setVisibility(View.VISIBLE);
        tv_dialog_ok = (TextView) dialog.findViewById(R.id.tv_dialog_ok);
        tv_message = (TextView) dialog.findViewById(R.id.tv_message);

        tv_title.setText(getResources().getString(R.string.cancel_request));
        tv_message.setText(getResources().getString(R.string.trip_canceled_by_passenger));

        tv_dialog_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    public void StartTrip(String bookingId, String driverId) {
        Log.e("kkkkkkkkk", "StartTrip");
        try {
            if (Comman.socket != null && Comman.socket.connected()) {
                Log.e("kkkkkkkkk", "StartTrip socket connected");
                // Sending an object
                JSONObject obj = new JSONObject();
                if (SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, activity) != null && !SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, activity).equalsIgnoreCase("")) {
                    if (SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, activity).equalsIgnoreCase("0")) {
                        try {
                            Log.e("kkkkkkkkk", "StartTrip Comman.USER_ID : " + SessionSave.getUserSession(Comman.USER_ID, activity));
                            obj.put(WebServiceAPI.SOCKET_PICKUP_PASSENGER_BOOKING_ID, bookingId);
                            obj.put(WebServiceAPI.SOCKET_PICKUP_PASSENGER_DRIVER_ID, driverId);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Comman.socket.emit(WebServiceAPI.SERVER_SOCKET_PICKUP_PASSENGER, obj);
                    } else {
                        try {
                            Log.e("kkkkkkkkk", "StartTrip Comman.USER_ID : " + SessionSave.getUserSession(Comman.USER_ID, activity));
                            obj.put(WebServiceAPI.SOCKET_PICKUP_PASSENGER_BOOK_LATER_BOOKING_ID, bookingId);
                            obj.put(WebServiceAPI.SOCKET_PICKUP_PASSENGER_BOOK_LATER_DRIVER_ID, driverId);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Comman.socket.emit(WebServiceAPI.SERVER_SOCKET_PICKUP_PASSENGER_BOOK_LATER, obj);
                    }
                }
            } else {
                Log.e("call StartTrip", "socket is not connected.......");
            }
        } catch (Exception e) {
            Log.e("call StartTrip", "error in sending latlong");
        }
    }

    public void HoldTrip(String bookingId) {
        try {
            if (Comman.socket != null && Comman.socket.connected()) {
                // Sending an object
                JSONObject obj = new JSONObject();
                if (SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, activity) != null && !SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, activity).equalsIgnoreCase("")) {
                    if (SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, activity).equalsIgnoreCase("0")) {
                        try {
                            Log.e("kkkkkkkkk", "HoldTrip bookingId : " + bookingId);
                            obj.put(WebServiceAPI.SOCKET_HOLD_PASSENGER_BOOKING_ID, bookingId);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Comman.socket.emit(WebServiceAPI.SERVER_SOCKET_HOLD_PASSENGER, obj);
                    } else {
                        try {
                            Log.e("kkkkkkkkk", "HoldTrip bookingId : " + bookingId);
                            obj.put(WebServiceAPI.SOCKET_HOLD_PASSENGER_lATER_BOOKING_ID, bookingId);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Comman.socket.emit(WebServiceAPI.SERVER_SOCKET_HOLD_PASSENGER_lATER, obj);
                    }
                }

            } else {
                Log.e("call HoldTrip", "socket is not connected.......");
            }
        } catch (Exception e) {
            Log.e("call HoldTrip", "error in sending latlong");
        }
    }

    public void EndHoldTrip(String bookingId) {
        try {
            if (Comman.socket != null && Comman.socket.connected()) {
                // Sending an object
                JSONObject obj = new JSONObject();
                if (SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, activity) != null && !SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, activity).equalsIgnoreCase("")) {
                    if (SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, activity).equalsIgnoreCase("0")) {
                        try {
                            Log.e("kkkkkkkkk", "EndHoldTrip bookingId : " + bookingId);
                            obj.put(WebServiceAPI.END_SOCKET_HOLD_PASSENGER_BOOKING_ID, bookingId);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Comman.socket.emit(WebServiceAPI.SERVER_SOCKET_END_HOLD_PASSENGER, obj);
                    } else {
                        try {
                            Log.e("kkkkkkkkk", "EndHoldTrip bookingId : " + bookingId);
                            obj.put(WebServiceAPI.END_SOCKET_HOLD_PASSENGER_LATER_BOOKING_ID, bookingId);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Comman.socket.emit(WebServiceAPI.SERVER_SOCKET_END_HOLD_PASSENGER_LATER, obj);
                    }
                }
            } else {
                Log.e("call EndHoldTrip", "socket is not connected.......");
            }
        } catch (Exception e) {
            Log.e("call EndHoldTrip", "error in sending latlong");
        }
    }


    public void NotifyPassengerForAdvancedTrip(String BookingId) {
        try {
            if (Comman.socket != null && Comman.socket.connected()) {
                // Sending an object
                JSONObject obj = new JSONObject();
                try {
                    String driverId = SessionSave.getUserSession(Comman.USER_ID, activity);
                    Log.e("kkkkkkkkk", "NotifyPassengerForAdvancedTrip driverId : " + driverId);
                    if (driverId != null && !driverId.equalsIgnoreCase("")) {
                        obj.put(WebServiceAPI.SOCKET_NOTIFY_PASSENGE_FOR_ADVANCE_TRIP_DRIVER_ID, driverId);
                        obj.put(WebServiceAPI.SOCKET_NOTIFY_PASSENGE_FOR_ADVANCE_TRIP_BOOKING_ID, BookingId);
                    } else {
                        Log.e("kkkkkkkkk", "NotifyPassengerForAdvancedTrip driverId null");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Comman.socket.emit(WebServiceAPI.SERVER_SOCKET_NOTIFY_PASSENGE_FOR_ADVANCE_TRIP, obj);

                SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "1", activity);
                SessionSave.saveUserSession(Comman.ACCEPT_REQUEST_FLAG, "1", activity);
                setTab_Home();
            } else {
                Log.e("call NotifyPassTrip", "socket is not connected.......");
            }
        } catch (Exception e) {
            Log.e("call NotifyPassTrip", "error in sending latlong");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Thread.currentThread();
        if (Comman.socket != null && Comman.socket.connected()) {
            Comman.socket.disconnect();
            Comman.socket.off(Socket.EVENT_CONNECT, onConnect);
            Comman.socket.off(Socket.EVENT_DISCONNECT, onDisconnect);
            Comman.socket.off(WebServiceAPI.SERVER_SOCKET_DRIVER_ARRIVE_BOOKING_REQUEST, onReceiveBooking_Request);
            Comman.socket.off(WebServiceAPI.SERVER_SOCKET_GET_BOOKING_DETAIL_AFTER_ACCEPTING_REQUEST, onReceiveBookingInfo);
            Comman.socket.off(WebServiceAPI.SERVER_SOCKET_UPDTED_BOOKING_DETAIL, onUpdatedBookingDetail);
            Comman.socket.off(WebServiceAPI.SERVER_SOCKET_RECEIVE_CANCEL_NOTIFICATION, onReceive_CancelTrip_Notification);

            Comman.socket.off(WebServiceAPI.SERVER_SOCKET_ARIVE_ADVANCE_BOOKING_REQUEST, onReceiveBookLater_Request);
            Comman.socket.off(WebServiceAPI.SERVER_SOCKET_RECEIVE_CANCEL_NOTIFICATION_LATER, onReceive_CancelTrip_Notification_Later);
            Comman.socket.off(WebServiceAPI.SERVER_SOCKET_GET_BOOKING_DETAIL_AFTER_ACCEPTING_REQUEST_LATER, onReceiveBookingInfoLater);
            Comman.socket.off(WebServiceAPI.SERVER_SOCKET_BOOK_LATER_DRIVER_NOTIFY, onBookLaterDriverNotify);
            Comman.socket.off(WebServiceAPI.SOCKET_SESSION_ERROR, onSessionError);
            handler.removeCallbacks(runnable);
            handler.removeCallbacksAndMessages(null);
            handler1.removeCallbacks(runnable);
            handler1.removeCallbacksAndMessages(null);
        }
    }


    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawers();
            return;
        }

        if (shouldLoadHomeFragOnBackPress) {
            setTab_Home();
            return;
        }
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public static void ExpandBottomShit() {
        if (mBottomSheetBehavior != null) {
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

            String dropLoc = SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LOCATION, activity);
            String pickLoc = SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LOCATION, activity);
            String passName = SessionSave.getUserSession(Comman.PASSENGER_NAME, activity);
            passMobNo = SessionSave.getUserSession(Comman.PASSENGER_MOBILE_NO, activity);
            String passImage = SessionSave.getUserSession(Comman.PASSENGER_IMAGE, activity);
            String passFlightNo = SessionSave.getUserSession(Comman.PASSENGER_FLIGHT_NO, activity);
            String passNote = SessionSave.getUserSession(Comman.PASSENGER_NOTE, activity);
            String passType = SessionSave.getUserSession(Comman.PASSENGER_TYPE, activity);

            if (passImage != null & !passImage.equalsIgnoreCase("")) {
                if (passImage.contains("http")) {
                } else {
                    passImage = WebServiceAPI.BASE_URL_IMAGE + passImage;
                }
            } else {
                passImage = "";
            }

            if (dropLoc != null && !dropLoc.equalsIgnoreCase("")) {
                tv_drop_off_location.setText(dropLoc);
            }

            if (pickLoc != null && !pickLoc.equalsIgnoreCase("")) {
                tv_pick_up_location.setText(pickLoc);
            }

            if (passName != null && !passName.equalsIgnoreCase("")) {
                tv_passName.setText(passName);
            }

            if (passType != null && !passType.equalsIgnoreCase("") && (passType.equalsIgnoreCase("others") || passType.equalsIgnoreCase("other"))) {
                passMobNo = SessionSave.getUserSession(Comman.PASSENGER_MOB_NO_OTHER, activity);
            } else {
                passMobNo = SessionSave.getUserSession(Comman.PASSENGER_MOBILE_NO, activity);
            }

            Log.e("passMobNo", "passMobNo : " + passMobNo);

            if (passImage != null && !passImage.equalsIgnoreCase("")) {
                Picasso.with(activity)
                        .load(passImage)
                        .transform(mTransformation)
                        .fit()
                        .into(iv_passenger, new Callback() {

                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                                iv_passenger.setImageResource(R.drawable.prof_image_demo);
                            }
                        });
            } else {
                iv_passenger.setImageResource(R.drawable.prof_image_demo);
            }

            if (passFlightNo != null && !passFlightNo.equalsIgnoreCase("")) {
                ll_PassengerFlightNo.setVisibility(View.VISIBLE);
                tv_PassengerFlightNo.setText(passFlightNo);
            } else {
                ll_PassengerFlightNo.setVisibility(View.GONE);
            }

            if (passNote != null && !passNote.equalsIgnoreCase("")) {
                ll_PassengerNote.setVisibility(View.VISIBLE);
                tv_PassengerNote.setText(passNote);
            } else {
                ll_PassengerNote.setVisibility(View.GONE);
            }
        }
    }

    private void sendSMS(String passMobNo) {
        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        sendIntent.setData(Uri.parse("sms:" + passMobNo));
        startActivity(sendIntent);
    }

    public void callToDriver(String numer) {
        /*TelephonyManager telMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        MyAlertDialog internetDialog = null;
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                internetDialog = new MyAlertDialog(activity);
                internetDialog.showOKDialog("Sim card not available");
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                internetDialog = new MyAlertDialog(activity);
                internetDialog.showOKDialog("Sim state network locked");
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                internetDialog = new MyAlertDialog(activity);
                internetDialog.showOKDialog("Sim state pin required");
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                internetDialog = new MyAlertDialog(activity);
                internetDialog.showOKDialog("Sim state puk required");
                break;
            case TelephonyManager.SIM_STATE_READY:
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + passMobNo));
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL_PHONE);
                } else {
                    startActivity(intent);
                }
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                internetDialog = new MyAlertDialog(activity);
                internetDialog.showOKDialog("Sim state unknown");
                break;
        }*/
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+numer));
        startActivity(intent);
    }

    public void nfcScanTicpay() {
        Tick_Pay_Fragment.emptyFild();
        flagScan = true;
        Log.e(TAG, "nfcScanTikpay:- nfcScanTicpay()");

        mNfcUtils.enableDispatch();
        // Close
        if (mAlertDialog != null && mAlertDialog.isShowing()) {
            mAlertDialog.cancel();
        }

        // Check if NFC is available
        if (!NFCUtils.isNfcAvailable(activity)) {
            AlertDialog.Builder alertbox = new AlertDialog.Builder(activity);
            alertbox.setTitle(getString(R.string.msg_info));
            alertbox.setMessage(getString(R.string.msg_nfc_not_available));
            alertbox.setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(final DialogInterface dialog, final int which) {
                    dialog.dismiss();
                }
            });
            alertbox.setCancelable(false);
            mAlertDialog = alertbox.show();
        } else {
            onNewIntent(getIntent());
        }
    }

    @Override
    public void onNewIntent(final Intent intent) {
        super.onNewIntent(intent);

        Log.e(TAG, "Nfc ScanStart");

       /* if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(getIntent().getAction()))
        {
            final Tag mTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            byte[] tagId = mTag.getId();*/


        final Tag mTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        if (mTag != null) {
            new SimpleAsyncTask() {

                private IsoDep mTagcomm;
                private EmvCard mCard;
                private boolean mException;

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();

//                    backToHomeScreen();
                    mProvider.getLog().setLength(0);
                    // Show dialog
                    if (mDialog == null) {
                        mDialog = ProgressDialog.show(activity, getString(R.string.card_reading),
                                getString(R.string.card_reading_desc), true, false);
                    } else {
                        mDialog.show();
                    }
                }

                @Override
                protected void doInBackground() {

                    mTagcomm = IsoDep.get(mTag);
                    if (mTagcomm == null) {
//                        Toast.makeText(activity, R.string.error_communication_nfc, Toast.LENGTH_SHORT).show();
                        final Dialog dialog = new Dialog(activity, R.style.DialogTheme);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        lp.copyFrom(dialog.getWindow().getAttributes());
                        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
                        lp.height = LinearLayout.LayoutParams.MATCH_PARENT;
                        dialog.getWindow().setAttributes(lp);
                        dialog.setCancelable(true);
                        dialog.setContentView(R.layout.dialog_receive_cancel_trip);

                        TextView tv_dialog_ok, tv_title, tv_message;

                        tv_title = (TextView) dialog.findViewById(R.id.tv_title);
                        tv_title.setVisibility(View.GONE);
                        tv_dialog_ok = (TextView) dialog.findViewById(R.id.tv_dialog_ok);
                        tv_message = (TextView) dialog.findViewById(R.id.tv_message);

                        tv_message.setText("Oops! Scanning failed, please try again");

                        tv_dialog_ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                nfcScanTicpay();
                            }
                        });
                        dialog.show();
                        return;
                    }
                    mException = false;

                    try {
                        mReadCard = null;
                        // Open connection
                        mTagcomm.connect();
                        lastAts = getAts(mTagcomm);

                        mProvider.setmTagCom(mTagcomm);

                        EmvParser parser = new EmvParser(mProvider, true);
                        mCard = parser.readEmvCard();
                        if (mCard != null) {
                            mCard.setAtrDescription(extractAtsDescription(lastAts));
                        }

                    } catch (IOException e) {
                        mException = true;
                    } finally {
                        // close tagcomm
                        IOUtils.closeQuietly(mTagcomm);
                    }
                }

                @Override
                protected void onPostExecute(final Object result) {
                    // close dialog
                    if (mDialog != null) {
                        mDialog.cancel();
                    }

                    if (!mException) {
                        if (mCard != null) {
                            if (StringUtils.isNotBlank(mCard.getCardNumber())) {
                                Toast.makeText(activity, R.string.card_read, Toast.LENGTH_SHORT).show();
                                mReadCard = mCard;

                                String str = mReadCard.getExpireDate().toString();
                                String[] splited = str.trim().split("\\s+");
                                String month = splited[1];
                                String year = splited[splited.length - 1];

                                Log.e("HomeActivity",
                                        "Card Number:- " + mReadCard.getCardNumber() +
                                                "\nCard Exp:- " + mReadCard.getExpireDate() +
                                                "\nCard Month:- " + month +
                                                "\nCard Year:- " + year +
                                                "\nCard Type:- " + mReadCard.getType() +
                                                "\nCard First Name:- " + mReadCard.getHolderFirstname() +
                                                "\nCard LastName:- " + mReadCard.getHolderLastname() +
                                                "\nCard Application:- " + mReadCard.getApplicationLabel() +
                                                "\nCard Aid:- " + mReadCard.getAid() +
                                                "\nCard LeftPinTrsy:- " + mReadCard.getLeftPinTry() +
                                                "\nCard Service:- " + mReadCard.getService());


                                cardNumber = mReadCard.getCardNumber();
                                strCardExpiry = month + "," + year;

                                if (cardNumber.length() > 4) {
                                    String strCardNum = "";
                                    for (int i = 0; i < cardNumber.length(); i++) {
                                        if (i == 0) {
                                            strCardNum = cardNumber.substring(i, (i + 1));
                                        } else if ((i % 4) == 0) {
                                            strCardNum = strCardNum + " " + cardNumber.substring(i, (i + 1));
                                        } else {
                                            strCardNum = strCardNum + cardNumber.substring(i, (i + 1));
                                        }
                                    }

                                    tick_pay_fragment.getResultNfc(strCardNum, strCardExpiry);
                                }


                            } else if (mCard.isNfcLocked()) {
                                Toast.makeText(activity, R.string.nfc_locked, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(activity, R.string.error_card_unknown, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(activity, R.string.error_communication_nfc, Toast.LENGTH_SHORT).show();

                        final Dialog dialog = new Dialog(activity, R.style.DialogTheme);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        lp.copyFrom(dialog.getWindow().getAttributes());
                        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
                        lp.height = LinearLayout.LayoutParams.MATCH_PARENT;
                        dialog.getWindow().setAttributes(lp);
                        dialog.setCancelable(true);
                        dialog.setContentView(R.layout.dialog_receive_cancel_trip);

                        TextView tv_dialog_ok, tv_title, tv_message;

                        tv_title = (TextView) dialog.findViewById(R.id.tv_title);
                        tv_title.setVisibility(View.GONE);
                        tv_dialog_ok = (TextView) dialog.findViewById(R.id.tv_dialog_ok);
                        tv_message = (TextView) dialog.findViewById(R.id.tv_message);

                        tv_message.setText("Oops! Scanning failed, please try again");

                        tv_dialog_ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                nfcScanTicpay();
                            }
                        });
                        dialog.show();
                    }

                    refreshContent();
                }

            }.execute();
        } else {
            Log.e(TAG, "mTag Null");
        }
//        }
    }


    public Collection<String> extractAtsDescription(final byte[] pAts) {
        return AtrUtils.getDescriptionFromAts(BytesUtils.bytesToString(pAts));
    }

    private void refreshContent() {
        if (mRefreshableContent != null && mRefreshableContent.get() != null) {
            mRefreshableContent.get().update();
        }
    }

    private byte[] getAts(final IsoDep pIso) {
        byte[] ret = null;
        if (pIso.isConnected()) {
            // Extract ATS from NFC-A
            ret = pIso.getHistoricalBytes();
            if (ret == null) {
                // Extract ATS from NFC-B
                ret = pIso.getHiLayerResponse();
            }
        }
        return ret;
    }

    public StringBuffer getLog() {
        return mProvider.getLog();
    }

    public EmvCard getCard() {
        return mReadCard;
    }


    public void setRefreshableContent(final IRefreshable pRefreshable) {
        mRefreshableContent = new WeakReference<IRefreshable>(pRefreshable);
    }

    public void clear() {
        mReadCard = null;
        mProvider.getLog().setLength(0);
        IRefreshable content = mRefreshableContent.get();
        if (content != null) {
            content.update();
        }
    }

    public byte[] getLastAts() {
        return lastAts;
    }

    @Override
    protected void onResume() {
        super.onResume();
        activity = Drawer_Activity.this;
        if (activity != null) {
            TiCKTOC_Driver_Application.setCurrentActivity(activity);
        }

        if (PasscodeBackPress == 1) {
            setTab_Home();
        }

        if (Create_Passcode_Activity.NfcFlag == 1) {
            if (tabItemIndex == 4) {
                nfcScanTicpay();
                Create_Passcode_Activity.NfcFlag = 0;
            }
        }

        if (flagResumePass == 1) {
            if (tabItemIndex == 4) {
                if (SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED, activity) != null && !SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED, activity).equalsIgnoreCase("")
                        && SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED, activity).equalsIgnoreCase("1")) {
                    Intent intentv = new Intent(activity, Create_Passcode_Activity.class);
                    startActivity(intentv);
                }
            }
        }
        flagResumePass = 0;

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mNfcUtils != null) {
            mNfcUtils.disableDispatch();
        }
        if (tabItemIndex == 4) {
            flagResumePass = 1;
        }
    }
}
