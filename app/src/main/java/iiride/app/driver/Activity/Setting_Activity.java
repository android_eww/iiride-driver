package iiride.app.driver.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import iiride.app.driver.Application.TiCKTOC_Driver_Application;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.R;


public class Setting_Activity extends AppCompatActivity implements View.OnClickListener{

    Setting_Activity activity;
    String TAG = "Setting_Activity";

    LinearLayout main_layout, ll_back, ll_change_passcode, ll_profile;
    SwitchCompat sw_passcode;

    public static int  flagResume = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        activity = Setting_Activity.this;

        flagResume = 0;

        main_layout = (LinearLayout)findViewById(R.id.main_layout);
        ll_back = (LinearLayout)findViewById(R.id.ll_back);
        ll_change_passcode = (LinearLayout)findViewById(R.id.ll_change_passcode);
        ll_profile = (LinearLayout)findViewById(R.id.ll_profile);
        sw_passcode = (SwitchCompat) findViewById(R.id.sw_passcode);

        ll_back.setOnClickListener(this);
        ll_change_passcode.setOnClickListener(this);
        ll_profile.setOnClickListener(this);

        Log.e("IS_PASSCODE_REQUIRED","IS_PASSCODE_REQUIRED : "+SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED, activity));
        String CheckMark = SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED, activity);
        if (CheckMark != null && !CheckMark.equalsIgnoreCase("") && CheckMark.equalsIgnoreCase("1"))
        {
            sw_passcode.setChecked(true);
            ll_change_passcode.setVisibility(View.VISIBLE);
        }
        else
        {
            sw_passcode.setChecked(false);
            ll_change_passcode.setVisibility(View.GONE);
        }

        sw_passcode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                Log.e("switch_compat", isChecked + "");
                if (isChecked)
                {
                    if (SessionSave.getUserSession(Comman.CREATED_PASSCODE,activity) != null && !SessionSave.getUserSession(Comman.CREATED_PASSCODE,activity).equalsIgnoreCase(""))
                    {
                        SessionSave.saveUserSession(Comman.IS_PASSCODE_REQUIRED,"1",activity);
                        ll_change_passcode.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        ll_change_passcode.setVisibility(View.GONE);
                        Handler handler = new Handler();
                        Runnable runnable = new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                call_create_pass();
                            }
                        };
                        handler.postDelayed(runnable,300);
                    }
                }
                else
                {
                    SessionSave.saveUserSession(Comman.IS_PASSCODE_REQUIRED,"0",activity);
                    ll_change_passcode.setVisibility(View.GONE);
                }
            }
        });
    }

    private void call_create_pass()
    {
        Intent intent = new Intent(activity,Create_Passcode_Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.ll_back:
                onBackPressed();
                break;

            case R.id.ll_change_passcode:
                call_change_passcode();
                break;

            case R.id.ll_profile:
                call_profile();
                break;
        }

    }

    private void call_profile()
    {
        Intent intent = new Intent(activity,Profile__Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    private void call_change_passcode()
    {
        Intent intent = new Intent(activity,Create_Passcode_Activity.class);
        intent.putExtra("from","changePassword");
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Drawer_Activity.flagResumePass=0;
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activity!=null)
        {
            TiCKTOC_Driver_Application.setCurrentActivity(activity);
        }

        if (flagResume==1)
        {
            Log.e("chalo","flagResume == 1");
            if (SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED, activity)!=null && !SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED, activity).equalsIgnoreCase("")
                    && SessionSave.getUserSession(Comman.IS_PASSCODE_REQUIRED, activity).equalsIgnoreCase("1"))
            {
                Log.e("chalo","IS_PASSCODE_REQUIRED === 1");
                if (SessionSave.getUserSession(Comman.CREATED_PASSCODE, activity)!=null && !SessionSave.getUserSession(Comman.CREATED_PASSCODE, activity).equalsIgnoreCase(""))
                {
                    Log.e("chalo","CREATED_PASSCODE  != null");
                    sw_passcode.setChecked(true);
                    ll_change_passcode.setVisibility(View.VISIBLE);
                    SessionSave.saveUserSession(Comman.IS_PASSCODE_REQUIRED,"1",activity);
                }
                else
                {
                    Log.e("chalo","CREATED_PASSCODE  == null");
                    sw_passcode.setChecked(false);
                    SessionSave.saveUserSession(Comman.IS_PASSCODE_REQUIRED,"0",activity);
                    ll_change_passcode.setVisibility(View.GONE);
                }
            }
            else
            {
                Log.e("chalo","IS_PASSCODE_REQUIRED === null or 0");
                sw_passcode.setChecked(false);
                SessionSave.saveUserSession(Comman.IS_PASSCODE_REQUIRED,"0",activity);
                ll_change_passcode.setVisibility(View.GONE);
            }
        }
        flagResume=1;
    }

}

