//package com.Ticktoc.driver.Others;
//
///**
// * Created by ADMIN on 10/23/2016.
// */
//import android.app.Activity;
//import android.app.Dialog;
//import android.content.Context;
//import android.content.Intent;
//import android.content.res.Resources;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.graphics.Matrix;
//import android.graphics.drawable.BitmapDrawable;
//import android.graphics.drawable.Drawable;
//import android.os.Bundle;
//import android.os.StrictMode;
//import android.support.v4.app.Fragment;
//import android.support.v4.view.PagerAdapter;
//import android.support.v4.view.ViewPager;
//import android.text.Html;
//import android.util.Log;
//import android.util.TypedValue;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.Window;
//import android.view.WindowManager;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//
//import DialogClass;
//import com.androidquery.AQuery;
//import com.androidquery.callback.AjaxCallback;
//import com.androidquery.callback.AjaxStatus;
//
//import com.squareup.picasso.Picasso;
//
//import org.json.JSONArray;
//import org.json.JSONObject;
//
//import java.text.DecimalFormat;
//
//public class All_Fragment extends Fragment {
//
//    private TextView tv_Matcolor,tv_NoMate;//,tv_Customize;//tv_Nomat,
//    private LinearLayout ll_WithMat,ll_Black,ll_White,ll_BlackAndWhite;
//    public static String str_MatColor = "0";//0 for no mat,1 for black,2 for white
//    private ViewPager viewPager;
//    public static int selectedPosition = 0;
//    private static int currentPage = 0;
//    private int prePagePosition = 0;
//    private ViewPagerAdapter adapter;
//    public static ImageView ll_Frame;
//    public static LinearLayout ll_Background;
//    public static ImageView iv_Image;
//    public static RelativeLayout mainRelativeLayout;
////    public static int popupFrame_Height=96,popupFrame_Width=96,popupBackcolor_Height=80,popupBackcolor_widtht=80;
//    public static int indexSliderView=0;
//    public static int newHeight = 0, newWidth = 0;
//    public static int newHeight_Popup = 0, newWidth_Popup = 0;
//    private Context context;
////    private List<ViewPager_Been> list = new ArrayList<ViewPager_Been>();
////    private CircleImageView iv_MatColor1,iv_MatColor2,iv_MatColor3,iv_MatColor4,iv_MatColor5;
//
//    private DialogClass dialogClass;
//    private AQuery aQuery;
//    public static int selectedTabPosition = 0;
//    public static int preSelectedTabPosition = -1;
//    private int[] matWidthArray = new int[6];
//    public static String lastPrice = "";
//
//    //for frame detail
//    public static String FrameUrl = "";
//    public static Bitmap FrameBitmap = null;
//    public static String FrameImageUrl = "";
//    public static String FrameMatColor = "0";
//    public static String FrameTitle = "";
//    public static int FrameMatIndex = 0;
//    public static int FrameImageHeight=0;
//    public static int FrameImageWidth=0;
//    public static String FrameHeart = "no";
//
//    public All_Fragment() {
//        // Required empty public constructor
//    }
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//    }
//
//    @Override
//    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//
//        View view = inflater.inflate(R.layout.fragment_all, container, false);
//
//        dialogClass = new DialogClass(context,0);
//        aQuery = new AQuery(context);
//
//        tv_Matcolor = (TextView)view.findViewById(R.id.fragment_all_mat_color_textview);
//        tv_NoMate = (TextView)view.findViewById(R.id.fragment_all_no_mat_textview);
//
////        tv_Customize = (TextView)view.findViewById(R.id.fragment_all_customize_textview);
//        viewPager = (ViewPager) view.findViewById(R.id.fragment_all_viewpager);
//        ll_BlackAndWhite = (LinearLayout) view.findViewById(R.id.fragment_all_mat_color_image_layout);
//        ll_WithMat = (LinearLayout) view.findViewById(R.id.fragment_all_with_mat_layout);
//        ll_Black = (LinearLayout) view.findViewById(R.id.fragment_all_black_layout);
//        ll_White = (LinearLayout) view.findViewById(R.id.fragment_all_white_layout);
//
//        str_MatColor = "0";
//        StyleIt_Activity.strColor = "2";
////        tv_NoMate.setTextColor(getResources().getColor(R.color.orange_color));
//        tv_Matcolor.setTextColor(getResources().getColor(R.color.orange_color));
////        tv_Customize.setTextColor(getActivity().getResources().getColor(R.color.orange_color));
////        tv_Customize.setLatoBold();
//
//
//        Log.e("call","HomeTab_Activity.editPosition = "+HomeTab_Activity.editPosition);
//
//        if (HomeTab_Activity.editPosition!=-1)
//        {
//            Log.e("call","HomeTab_Activity.editPosition111111111111111111");
//            DatabaseHandler db = new DatabaseHandler(getActivity());
//
//            new Cart_Been();
//            if (db.getContactsCount()>0)
//            {
//                Log.e("call","HomeTab_Activity.editPosition2222222222222222");
//                List<Cart_Been> list = new ArrayList<Cart_Been>();
//                list = db.getAllContacts();
//
//                if (list.size()>0)
//                {
//                    Log.e("call","HomeTab_Activity.editPosition3333333333333333333");
//                    Log.e("call","HomeTab_Activity.editPosition3333333333333333333");
//                    Log.e("call","HomeTab_Activity.editPosition3333333333333333333 = "+list.get(HomeTab_Activity.editPosition).getColorMat());
//                    if (list.get(HomeTab_Activity.editPosition).getColorMat().equalsIgnoreCase("2"))
//                    {
//                        str_MatColor = "2";
//                        StyleIt_Activity.strColor = "2";
//                        StyleIt_Activity.strMatColor = "2";
//                    }
//                    else if (list.get(HomeTab_Activity.editPosition).getColorMat().equalsIgnoreCase("1"))
//                    {
//                        str_MatColor = "1";
//                        StyleIt_Activity.strColor = "1";
//                        StyleIt_Activity.strMatColor = "1";
//                    }
//                    else
//                    {
//                        str_MatColor = "0";
//                        StyleIt_Activity.strColor = "2";
//                        StyleIt_Activity.strMatColor = "0";
//                    }
//                }
//            }
//
//        }
//        setHeightAndWidth();
//
//        tv_Matcolor.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                str_MatColor = "2";
//                StyleIt_Activity.strColor = "2";
//                StyleIt_Activity.strMatColor = "2";
//                if (selectedTabPosition!=preSelectedTabPosition)
//                {
//                    setHeightAndWidth();
//                }
//                preSelectedTabPosition = selectedTabPosition;
//                selectedTabPosition = 1;
//                indexSliderView=3;
//                StyleIt_Activity.strIndex = "3";
//                ll_BlackAndWhite.setVisibility(View.VISIBLE);
//                tv_Matcolor.setTextColor(getActivity().getResources().getColor(R.color.orange_color));
//                tv_NoMate.setTextColor(getActivity().getResources().getColor(R.color.gray_divider));
////                tv_Customize.setTextColor(getActivity().getResources().getColor(R.color.orange_color));
//                loadMatColor();
//            }
//        });
//
//        tv_NoMate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                str_MatColor = "2";
//                StyleIt_Activity.strColor = "2";
//                StyleIt_Activity.strMatColor = "2";
//                if (selectedTabPosition!=preSelectedTabPosition)
//                {
//                    setHeightAndWidth();
//                }
//                preSelectedTabPosition = selectedTabPosition;
//                selectedTabPosition = 0;
//                indexSliderView=0;
//                StyleIt_Activity.strIndex = "0";
//                ll_BlackAndWhite.setVisibility(View.GONE);
//                tv_Matcolor.setTextColor(getActivity().getResources().getColor(R.color.gray_divider));
//                tv_NoMate.setTextColor(getActivity().getResources().getColor(R.color.orange_color));
////                tv_Customize.setTextColor(getActivity().getResources().getColor(R.color.orange_color));
//                loadMatColor();
//            }
//        });
//
////        tv_Customize.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                openCustomizationPopup();
////            }
////        });
//
//        ll_Black.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                str_MatColor = "1";
//                StyleIt_Activity.strColor = "1";
//                StyleIt_Activity.strMatColor = "1";
//                loadMatColor();
//            }
//        });
//
//        ll_White.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                str_MatColor = "2";
//                StyleIt_Activity.strColor = "2";
//                StyleIt_Activity.strMatColor = "2";
//                loadMatColor();
//            }
//        });
//
//        loadMatColor();
//
//        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//                Log.e("call","onPageScrolled = "+position);
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//
//                selectedPosition = position;
//                Log.e("call","onPageSelected = "+position);
//                StyleIt_Activity.selectedListPosition = position;
//                if (StyleIt_Activity.loadMore && position == StyleIt_Activity.list.size()-1)
//                {
//                    getPageWiseFrameList();
//                }
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//                Log.e("call","onPageScrollStateChanged = "+state);
//            }
//        });
//        return view;
//    }
//
//    public void setHeightAndWidth()
//    {
//        Log.e("cropIt","image cropImageHeight = "+CropIt_Activity.cropImageHeight);
//        Log.e("cropIt","image cropImageWidth = "+CropIt_Activity.cropImageWidth);
//
//        if (HomeTab_Activity.clickFlag==9)
//        {
//            Log.e("call","square image detect");
//            newHeight=90;
//            newWidth=90;
//
//            newHeight_Popup = 90;
//            newWidth_Popup = 90;
//        }
//        else
//        {
//            if (CropIt_Activity.cropImageHeight>CropIt_Activity.cropImageWidth)
//            {
//                Log.e("call","portrait image detect");
//                newHeight=125;
//                newWidth=80;
//
//                newHeight_Popup = 125;
//                newWidth_Popup = 80;
//
//            }
//            else if (CropIt_Activity.cropImageHeight<CropIt_Activity.cropImageWidth)
//            {
//                Log.e("call","landscap image detect");
//                newHeight=80;
//                newWidth=125;
//
//                newHeight_Popup = 80;
//                newWidth_Popup = 125;
//            }
//            else
//            {
//                Log.e("call","square image detect");
//                newHeight=90;
//                newWidth=90;
//
//                newHeight_Popup = 90;
//                newWidth_Popup = 90;
//            }
//        }
//    }
//
//    public void openCustomizationPopup()
//    {
//
//        Log.e("call","openCustomizationPopup newWidth = "+newWidth);
//        Log.e("call","openCustomizationPopup newHeight = "+newHeight);
//
//        final LinearLayout ll_Done,ll_Close,ll_FrameWidthOne,ll_FrameWidthSecond,ll_FrameWidthThird;
//        final RangeSliderView sliderView;
//        final TextView tv_FrameWidthOne,tv_FrameWidthSecond,tv_FrameWidthThird,tv_Done,tv_CustomizeMatWidth;
//        final ImageView imagePopup;
//        final SvgImageView iv_HeartPopup;
//        final LinearLayout ll_BackcolorPopup;
//        final ImageView ll_FramePopup;
//        Bitmap dbBitmap;
//        DatabaseHandler db;
//        List<Cart_Been> cartList = new ArrayList<Cart_Been>();
//
//        db = new DatabaseHandler(context);
//        cartList = db.getAllContacts();
//        if (CropIt_Activity.cropImageHeight>CropIt_Activity.cropImageWidth)
//        {
//            Log.e("call","portrait image detect");
//
//            newHeight_Popup = 110;
//            newWidth_Popup = 65;
//
//        }
//        else if (CropIt_Activity.cropImageHeight<CropIt_Activity.cropImageWidth)
//        {
//            Log.e("call","landscap image detect");
//
//            newHeight_Popup = 65;
//            newWidth_Popup = 110;
//        }
//        else
//        {
//            Log.e("call","square image detect");
//
//            newHeight_Popup = 90;
//            newWidth_Popup = 90;
//        }
//
//        LayoutInflater layoutInflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View view = layoutInflater.inflate(R.layout.customize_frame_popup, null);
//
//        ll_Done = (LinearLayout)view.findViewById(R.id.customize_frame_popup_done_layout);
//        tv_Done = (TextView) view.findViewById(R.id.customize_frame_popup_done_textview);
//        tv_CustomizeMatWidth = (TextView) view.findViewById(R.id.customize_frame_popup_mat_width_textview);
//        ll_Close = (LinearLayout)view.findViewById(R.id.customize_frame_popup_close_layout);
//
//        ll_BackcolorPopup = (LinearLayout)view.findViewById(R.id.back_color);
//        ll_FramePopup = (ImageView) view.findViewById(R.id.frame);
//        imagePopup = (ImageView) view.findViewById(R.id.image);
//        iv_HeartPopup = (SvgImageView) view.findViewById(R.id.heart);
//
//        if (str_MatColor!=null && str_MatColor.equalsIgnoreCase("1"))
//        {
//            ll_BackcolorPopup.setBackgroundColor(getResources().getColor(R.color.black_color));
//        }
//        else
//        {
//            ll_BackcolorPopup.setBackgroundColor(getResources().getColor(R.color.white_color));
//        }
//
//        ll_FrameWidthOne = (LinearLayout)view.findViewById(R.id.one_layout);
//        ll_FrameWidthSecond = (LinearLayout)view.findViewById(R.id.second_layout);
//        ll_FrameWidthThird = (LinearLayout)view.findViewById(R.id.third_layout);
//
//        tv_FrameWidthOne = (TextView) view.findViewById(R.id.one_textview);
//        tv_FrameWidthSecond = (TextView) view.findViewById(R.id.second_textview);
//        tv_FrameWidthThird = (TextView) view.findViewById(R.id.third_textview);
//
//        sliderView = (RangeSliderView) view.findViewById(R.id.rsv_small);
//
//        Log.e("call","slider index = "+sliderView.getCurrentIndex());
//
//        sliderView.setCurrentIndex(indexSliderView);
//        tv_CustomizeMatWidth.setText(matWidthArray[indexSliderView]+"");
//
//        RelativeLayout.LayoutParams layoutParamsImage = new RelativeLayout.LayoutParams(getPixel(newWidth_Popup),getPixel(newHeight_Popup));
//        layoutParamsImage.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//        imagePopup.setLayoutParams(layoutParamsImage);
//        imagePopup.setScaleType(ImageView.ScaleType.FIT_XY);
//        iv_HeartPopup.setLayoutParams(layoutParamsImage);
//        iv_HeartPopup.setScaleType(ImageView.ScaleType.FIT_XY);
//
//        if (indexSliderView==0)
//        {
//            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(getPixel(newWidth_Popup+10),getPixel(newHeight_Popup+10));
//            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//            ll_FramePopup.setLayoutParams(layoutParams);
//            ll_FramePopup.setScaleType(ImageView.ScaleType.FIT_XY);
//
//            RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(getPixel(newWidth_Popup),getPixel(newHeight_Popup));
//            layoutParams1.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//            ll_BackcolorPopup.setLayoutParams(layoutParams1);
//
//            imagePopup.setScaleType(ImageView.ScaleType.FIT_XY);
//            iv_HeartPopup.setScaleType(ImageView.ScaleType.FIT_XY);
//        }
//        else if (indexSliderView==1)
//        {
//            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(getPixel(newWidth_Popup+40),getPixel(newHeight_Popup+40));
//            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//            ll_FramePopup.setLayoutParams(layoutParams);
//            ll_FramePopup.setScaleType(ImageView.ScaleType.FIT_XY);
//
//            RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(getPixel(newWidth_Popup+30),getPixel(newHeight_Popup+30));
//            layoutParams1.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//            ll_BackcolorPopup.setLayoutParams(layoutParams1);
//
//            imagePopup.setScaleType(ImageView.ScaleType.FIT_XY);
//            iv_HeartPopup.setScaleType(ImageView.ScaleType.FIT_XY);
//        }
//        else if (indexSliderView==2)
//        {
//            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(getPixel(newWidth_Popup+50),getPixel(newHeight_Popup+50));
//            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//            ll_FramePopup.setLayoutParams(layoutParams);
//            ll_FramePopup.setScaleType(ImageView.ScaleType.FIT_XY);
//
//            RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(getPixel(newWidth_Popup+40),getPixel(newHeight_Popup+40));
//            layoutParams1.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//            ll_BackcolorPopup.setLayoutParams(layoutParams1);
//
//            imagePopup.setScaleType(ImageView.ScaleType.FIT_XY);
//            iv_HeartPopup.setScaleType(ImageView.ScaleType.FIT_XY);
//        }
//        else if (indexSliderView==3)
//        {
//            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(getPixel(newWidth_Popup+60),getPixel(newHeight_Popup+60));
//            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//            ll_FramePopup.setLayoutParams(layoutParams);
//            ll_FramePopup.setScaleType(ImageView.ScaleType.FIT_XY);
//
//            RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(getPixel(newWidth_Popup+50),getPixel(newHeight_Popup+50));
//            layoutParams1.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//            ll_BackcolorPopup.setLayoutParams(layoutParams1);
//
//            imagePopup.setScaleType(ImageView.ScaleType.FIT_XY);
//            iv_HeartPopup.setScaleType(ImageView.ScaleType.FIT_XY);
//        }
//        else if (indexSliderView==4)
//        {
//            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(getPixel(newWidth_Popup+70),getPixel(newHeight_Popup+70));
//            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//            ll_FramePopup.setLayoutParams(layoutParams);
//            ll_FramePopup.setScaleType(ImageView.ScaleType.FIT_XY);
//
//            RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(getPixel(newWidth_Popup+60),getPixel(newHeight_Popup+60));
//            layoutParams1.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//            ll_BackcolorPopup.setLayoutParams(layoutParams1);
//
//            imagePopup.setScaleType(ImageView.ScaleType.FIT_XY);
//            iv_HeartPopup.setScaleType(ImageView.ScaleType.FIT_XY);
//        }
//        else if (indexSliderView==5)
//        {
//            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(getPixel(newWidth_Popup+80),getPixel(newHeight_Popup+80));
//            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//            ll_FramePopup.setLayoutParams(layoutParams);
//            ll_FramePopup.setScaleType(ImageView.ScaleType.FIT_XY);
//
//            RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(getPixel(newWidth_Popup+70),getPixel(newHeight_Popup+70));
//            layoutParams1.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//            ll_BackcolorPopup.setLayoutParams(layoutParams1);
//
//            imagePopup.setScaleType(ImageView.ScaleType.FIT_XY);
//            iv_HeartPopup.setScaleType(ImageView.ScaleType.FIT_XY);
//        }
//
//        Picasso.with(context).load(StyleIt_Activity.list.get(selectedPosition).getthumb()).into(ll_FramePopup);
//
//        sliderView.setOnSlideListener(new RangeSliderView.OnSlideListener() {
//            @Override
//            public void onSlide(int index) {
//
//                Log.e("call","onSlide newWidth_Popup = "+newWidth_Popup);
//                Log.e("call","onSlide newHeight_Popup = "+newHeight_Popup);
//                tv_CustomizeMatWidth.setText(matWidthArray[index]+"");
//                if (index==0)
//                {
//                    Log.e("index"," = "+index);
//
//                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(getPixel(newWidth_Popup+10),getPixel(newHeight_Popup+10));
//                    layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//                    ll_FramePopup.setLayoutParams(layoutParams);
//                    ll_FramePopup.setScaleType(ImageView.ScaleType.FIT_XY);
//
//                    RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(getPixel(newWidth_Popup),getPixel(newHeight_Popup));
//                    layoutParams1.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//                    ll_BackcolorPopup.setLayoutParams(layoutParams1);
//
//                    imagePopup.setScaleType(ImageView.ScaleType.FIT_XY);
//                    iv_HeartPopup.setScaleType(ImageView.ScaleType.FIT_XY);
//
//                }
//                else if (index==1)
//                {
//                    Log.e("index"," = "+index);
//
//                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(getPixel(newWidth_Popup+40),getPixel(newHeight_Popup+40));
//                    layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//                    ll_FramePopup.setLayoutParams(layoutParams);
//                    ll_FramePopup.setScaleType(ImageView.ScaleType.FIT_XY);
//
//                    RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(getPixel(newWidth_Popup+30),getPixel(newHeight_Popup+30));
//                    layoutParams1.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//                    ll_BackcolorPopup.setLayoutParams(layoutParams1);
//
//                    imagePopup.setScaleType(ImageView.ScaleType.FIT_XY);
//                    iv_HeartPopup.setScaleType(ImageView.ScaleType.FIT_XY);
//                }
//                else if (index==2)
//                {
//                    Log.e("index"," = "+index);
//
//                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(getPixel(newWidth_Popup+50),getPixel(newHeight_Popup+50));
//                    layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//                    ll_FramePopup.setLayoutParams(layoutParams);
//                    ll_FramePopup.setScaleType(ImageView.ScaleType.FIT_XY);
//
//                    RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(getPixel(newWidth_Popup+40),getPixel(newHeight_Popup+40));
//                    layoutParams1.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//                    ll_BackcolorPopup.setLayoutParams(layoutParams1);
//
//                    imagePopup.setScaleType(ImageView.ScaleType.FIT_XY);
//                    iv_HeartPopup.setScaleType(ImageView.ScaleType.FIT_XY);
//                }
//                else if (index==3)
//                {
//                    Log.e("index"," = "+index);
//
//                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(getPixel(newWidth_Popup+60),getPixel(newHeight_Popup+60));
//                    layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//                    ll_FramePopup.setLayoutParams(layoutParams);
//                    ll_FramePopup.setScaleType(ImageView.ScaleType.FIT_XY);
//
//                    RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(getPixel(newWidth_Popup+50),getPixel(newHeight_Popup+50));
//                    layoutParams1.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//                    ll_BackcolorPopup.setLayoutParams(layoutParams1);
//
//                    imagePopup.setScaleType(ImageView.ScaleType.FIT_XY);
//                    iv_HeartPopup.setScaleType(ImageView.ScaleType.FIT_XY);
//                }
//                else if (index==4)
//                {
//                    Log.e("index"," = "+index);
//
//                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(getPixel(newWidth_Popup+70),getPixel(newHeight_Popup+70));
//                    layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//                    ll_FramePopup.setLayoutParams(layoutParams);
//                    ll_FramePopup.setScaleType(ImageView.ScaleType.FIT_XY);
//
//                    RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(getPixel(newWidth_Popup+60),getPixel(newHeight_Popup+60));
//                    layoutParams1.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//                    ll_BackcolorPopup.setLayoutParams(layoutParams1);
//
//                    imagePopup.setScaleType(ImageView.ScaleType.FIT_XY);
//                    iv_HeartPopup.setScaleType(ImageView.ScaleType.FIT_XY);
//                }
//                else if (index==5)
//                {
//                    Log.e("index"," = "+index);
//
//                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(getPixel(newWidth_Popup+80),getPixel(newHeight_Popup+80));
//                    layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//                    ll_FramePopup.setLayoutParams(layoutParams);
//                    ll_FramePopup.setScaleType(ImageView.ScaleType.FIT_XY);
//
//                    RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(getPixel(newWidth_Popup+70),getPixel(newHeight_Popup+70));
//                    layoutParams1.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
//                    ll_BackcolorPopup.setLayoutParams(layoutParams1);
//
//                    imagePopup.setScaleType(ImageView.ScaleType.FIT_XY);
//                    iv_HeartPopup.setScaleType(ImageView.ScaleType.FIT_XY);
//                }
//            }
//        });
//
//        if (HomeTab_Activity.editPosition == -1)
//        {
//            if (HomeTab_Activity.clickFlag==3)
//            {
//                imagePopup.setVisibility(View.GONE);
//                iv_HeartPopup.setVisibility(View.VISIBLE);
//            }
//            else
//            {
//                imagePopup.setVisibility(View.VISIBLE);
//                iv_HeartPopup.setVisibility(View.GONE);
//            }
//        }
//        else
//        {
//            if (cartList.get(HomeTab_Activity.editPosition).getheart().equalsIgnoreCase("yes"))
//            {
//                imagePopup.setVisibility(View.GONE);
//                iv_HeartPopup.setVisibility(View.VISIBLE);
//            }
//            else
//            {
//                imagePopup.setVisibility(View.VISIBLE);
//                iv_HeartPopup.setVisibility(View.GONE);
//            }
//        }
//
//        Log.e("callpopup","HomTabActivity editPosition = "+HomeTab_Activity.editPosition);
//        if (HomeTab_Activity.editPosition==-1)
//        {
//            if (CropIt_Activity.bmp!=null)
//            {
//                imagePopup.setImageBitmap(CropIt_Activity.bmp);
//                iv_HeartPopup.setImageBitmap(CropIt_Activity.bmp);
//            }
//
//        }
//        else
//        {
//            byte[] image = cartList.get(HomeTab_Activity.editPosition).getbase64();
//            dbBitmap = DbBitmapUtility.getImage(image);
//            imagePopup.setImageBitmap(dbBitmap);
//            iv_HeartPopup.setImageBitmap(dbBitmap);
//        }
//
//        final Dialog dialog = new Dialog(getActivity(),R.style.BounceAnimation);//R.style.PauseDialog
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(view);
//        dialog.setCancelable(true);
//
//
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        lp.copyFrom(dialog.getWindow().getAttributes());
//        lp.width = LinearLayout.LayoutParams.WRAP_CONTENT;
//        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
//        lp.gravity = Gravity.CENTER;
//
//        dialog.getWindow().setAttributes(lp);
//
//
//        ll_Done.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//
//                indexSliderView = sliderView.getCurrentIndex();
//                StyleIt_Activity.strIndex = indexSliderView+"";
//                if (indexSliderView==0)
//                {
//                    setIndex();
//                }
//                else if (indexSliderView==1)
//                {
//                    setIndex();
//                }
//                else if (indexSliderView==2)
//                {
//                    setIndex();
//                }
//                else if (indexSliderView==3)
//                {
//                    setIndex();
//                }
//                else if (indexSliderView==4)
//                {
//                    setIndex();
//                }
//                else if (indexSliderView==5)
//                {
//                    setIndex();
//                }
//
//                dialog.dismiss();
//
//            }
//        });
//
//        tv_Done.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                indexSliderView = sliderView.getCurrentIndex();
//                StyleIt_Activity.strIndex = indexSliderView+"";
//                if (indexSliderView==0)
//                {
//                    setIndex();
//                }
//                else if (indexSliderView==1)
//                {
//                    setIndex();
//                }
//                else if (indexSliderView==2)
//                {
//                    setIndex();
//                }
//                else if (indexSliderView==3)
//                {
//                    setIndex();
//                }
//                else if (indexSliderView==4)
//                {
//                    setIndex();
//                }
//                else if (indexSliderView==5)
//                {
//                    setIndex();
//                }
//
//                dialog.dismiss();
//
//            }
//        });
//
//
//        ll_Close.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                dialog.dismiss();
//            }
//        });
//
//        ll_FrameWidthOne.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//
//                ll_FrameWidthOne.setBackgroundResource(R.drawable.rounded_corner_border_layout_orange_selected_left_round);
//                ll_FrameWidthSecond.setBackgroundResource(R.drawable.unselected_all_side_order_orange);
//                ll_FrameWidthThird.setBackgroundResource(R.drawable.rounded_corner_border_layout_orange_unselected_right_round);
//
//                tv_FrameWidthOne.setTextColor(getActivity().getResources().getColor(R.color.white_color));
//                tv_FrameWidthSecond.setTextColor(getActivity().getResources().getColor(R.color.orange_color));
//                tv_FrameWidthThird.setTextColor(getActivity().getResources().getColor(R.color.orange_color));
//            }
//        });
//
//        tv_FrameWidthOne.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//
//                ll_FrameWidthOne.setBackgroundResource(R.drawable.rounded_corner_border_layout_orange_selected_left_round);
//                ll_FrameWidthSecond.setBackgroundResource(R.drawable.unselected_all_side_order_orange);
//                ll_FrameWidthThird.setBackgroundResource(R.drawable.rounded_corner_border_layout_orange_unselected_right_round);
//
//                tv_FrameWidthOne.setTextColor(getActivity().getResources().getColor(R.color.white_color));
//                tv_FrameWidthSecond.setTextColor(getActivity().getResources().getColor(R.color.orange_color));
//                tv_FrameWidthThird.setTextColor(getActivity().getResources().getColor(R.color.orange_color));
//            }
//        });
//
//        ll_FrameWidthSecond.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//
//                ll_FrameWidthOne.setBackgroundResource(R.drawable.rounded_corner_border_layout_orange_unselected_left_round);
//                ll_FrameWidthSecond.setBackgroundResource(R.drawable.selected_all_side_order_orange);
//                ll_FrameWidthThird.setBackgroundResource(R.drawable.rounded_corner_border_layout_orange_unselected_right_round);
//
//                tv_FrameWidthOne.setTextColor(getActivity().getResources().getColor(R.color.orange_color));
//                tv_FrameWidthSecond.setTextColor(getActivity().getResources().getColor(R.color.white_color));
//                tv_FrameWidthThird.setTextColor(getActivity().getResources().getColor(R.color.orange_color));
//            }
//        });
//
//        tv_FrameWidthSecond.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//
//                ll_FrameWidthOne.setBackgroundResource(R.drawable.rounded_corner_border_layout_orange_unselected_left_round);
//                ll_FrameWidthSecond.setBackgroundResource(R.drawable.selected_all_side_order_orange);
//                ll_FrameWidthThird.setBackgroundResource(R.drawable.rounded_corner_border_layout_orange_unselected_right_round);
//
//                tv_FrameWidthOne.setTextColor(getActivity().getResources().getColor(R.color.orange_color));
//                tv_FrameWidthSecond.setTextColor(getActivity().getResources().getColor(R.color.white_color));
//                tv_FrameWidthThird.setTextColor(getActivity().getResources().getColor(R.color.orange_color));
//            }
//        });
//
//        ll_FrameWidthThird.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//
//                ll_FrameWidthOne.setBackgroundResource(R.drawable.rounded_corner_border_layout_orange_unselected_left_round);
//                ll_FrameWidthSecond.setBackgroundResource(R.drawable.unselected_all_side_order_orange);
//                ll_FrameWidthThird.setBackgroundResource(R.drawable.rounded_corner_border_layout_orange_selected_right_round);
//
//                tv_FrameWidthOne.setTextColor(getActivity().getResources().getColor(R.color.orange_color));
//                tv_FrameWidthSecond.setTextColor(getActivity().getResources().getColor(R.color.orange_color));
//                tv_FrameWidthThird.setTextColor(getActivity().getResources().getColor(R.color.white_color));
//            }
//        });
////
//        tv_FrameWidthThird.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//
//                ll_FrameWidthOne.setBackgroundResource(R.drawable.rounded_corner_border_layout_orange_unselected_left_round);
//                ll_FrameWidthSecond.setBackgroundResource(R.drawable.unselected_all_side_order_orange);
//                ll_FrameWidthThird.setBackgroundResource(R.drawable.rounded_corner_border_layout_orange_selected_right_round);
//
//                tv_FrameWidthOne.setTextColor(getActivity().getResources().getColor(R.color.orange_color));
//                tv_FrameWidthSecond.setTextColor(getActivity().getResources().getColor(R.color.orange_color));
//                tv_FrameWidthThird.setTextColor(getActivity().getResources().getColor(R.color.white_color));
//            }
//        });
//
//        dialog.show();
//    }
//
//
//
//    public void setIndex()
//    {
//        if (CropIt_Activity.cropImageHeight>CropIt_Activity.cropImageWidth)
//        {
//            Log.e("call","setIndexZero portrait image detect");
//            newHeight = 110;
//            newWidth = 65;
//
//        }
//        else if (CropIt_Activity.cropImageHeight<CropIt_Activity.cropImageWidth)
//        {
//            Log.e("call","setIndexZero landscap image detect");
//            newHeight = 65;
//            newWidth = 110;
//        }
//        else
//        {
//            Log.e("call","setIndexZero square image detect");
//            newHeight = 90;
//            newWidth = 90;
//        }
//
//        viewPager.setAdapter(adapter);
//    }
//
//    public int getPixel(int dp)
//    {
//        Resources r = getResources();
//        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
//
//        int result = (int) px;
//        return  result;
//    }
//
//    public static String html2text(String html) {
//        return Jsoup.parse(html).text();
//    }
//
//    public void loadMatColor()
//    {
//        DatabaseHandler db = new DatabaseHandler(getActivity());
//        if (HomeTab_Activity.editPosition==-1)
//        {
//            if (StyleIt_Activity.from!=null && StyleIt_Activity.from.equalsIgnoreCase("mat"))
//            {
//                if (HomeTab_Activity.clickFlag==3)
//                {
//                    ll_WithMat.setVisibility(View.GONE);
//                    Log.e("call","ll_WithMat VISIBLE");
//                }
//                else
//                {
//                    ll_WithMat.setVisibility(View.VISIBLE);
//                    Log.e("call","ll_WithMat VISIBLE");
//                }
//            }
//            else
//            {
//                ll_WithMat.setVisibility(View.GONE);
//                Log.e("call","ll_WithMat GONE");
//            }
//
//            Log.e("call","list size = "+StyleIt_Activity.list.size());
//        }
//        else
//        {
//            if (db.getContactsCount()>0)
//            {
//                List<Cart_Been> cart_beens = new ArrayList<Cart_Been>();
//
//                cart_beens = db.getAllContacts();
//                if (cart_beens.size()>0)
//                {
//                    if (!cart_beens.get(HomeTab_Activity.editPosition).getTag().equalsIgnoreCase("heartagram"))
//                    {
//                        ll_WithMat.setVisibility(View.VISIBLE);
//                        Log.e("call","ll_WithMat VISIBLE");
//                    }
//                    else
//                    {
//                        ll_WithMat.setVisibility(View.GONE);
//                        Log.e("call","ll_WithMat GONE");
//                    }
//
//                    Log.e("call","list size = "+StyleIt_Activity.list.size());
//                }
//                else
//                {
//                    if (StyleIt_Activity.from!=null && StyleIt_Activity.from.equalsIgnoreCase("mat"))
//                    {
//                        if (HomeTab_Activity.clickFlag==3)
//                        {
//                            ll_WithMat.setVisibility(View.GONE);
//                            Log.e("call","ll_WithMat VISIBLE");
//                        }
//                        else
//                        {
//                            ll_WithMat.setVisibility(View.VISIBLE);
//                            Log.e("call","ll_WithMat VISIBLE");
//                        }
//                    }
//                    else
//                    {
//                        ll_WithMat.setVisibility(View.GONE);
//                        Log.e("call","ll_WithMat GONE");
//                    }
//
//                    Log.e("call","list size = "+StyleIt_Activity.list.size());
//                }
//
//            }
//            else
//            {
//                if (StyleIt_Activity.from!=null && StyleIt_Activity.from.equalsIgnoreCase("mat"))
//                {
//                    ll_WithMat.setVisibility(View.VISIBLE);
//                    Log.e("call","ll_WithMat VISIBLE");
//                }
//                else
//                {
//                    ll_WithMat.setVisibility(View.GONE);
//                    Log.e("call","ll_WithMat GONE");
//                }
//
//                Log.e("call","list size = "+StyleIt_Activity.list.size());
//            }
//        }
//        adapter = new ViewPagerAdapter();
//        viewPager.setAdapter(adapter);
//    }
//
//    private class ViewPagerAdapter extends PagerAdapter
//    {
//
//        LayoutInflater inflater;
//
//        public ViewPagerAdapter()
//        {
//            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//            StrictMode.setThreadPolicy(policy);
//        }
//
//        @Override
//        public int getCount() {
//            return StyleIt_Activity.list.size();
//        }
//
//        @Override
//        public boolean isViewFromObject(View view, Object object) {
//            return view == ((LinearLayout) object);
//        }
//
//        @Override
//        public Object instantiateItem(ViewGroup container, final int position) {
//            final Context context = getActivity();
//
//            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            if ()
//            View itemView = inflater.inflate(R.layout.row_view_pager_all_fragment, container,false);
//
//            ((ViewPager) container).addView(itemView, 0);
//            return itemView;
//        }
//
//
//        @Override
//        public void destroyItem(ViewGroup container, int position, Object object) {
//            ((ViewPager) container).removeView((LinearLayout) object);
//        }
//
//    }
//
//    public void getPageWiseFrameList()
//    {
//        StyleIt_Activity.page = StyleIt_Activity.page + 1;
//        String url = WebServiceAPI.API_GET_FRAME + StyleIt_Activity.page;
//
//        Log.e("call","first url = "+url);
//
//        dialogClass.showDialog();
//        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {
//
//            @Override
//            public void callback(String url, JSONObject json, AjaxStatus status) {
//
//                try
//                {
//                    int responseCode = status.getCode();
//                    Log.e("responseCode", " = " + responseCode);
//                    Log.e("Response", " = " + json);
//
//                    if (json!=null)
//                    {
//                        Log.e("call", "json not null");
//                        if (json.has("products"))
//                        {
//                            JSONArray products = json.getJSONArray("products");
//
//                            if (products!=null && products.length()>0)
//                            {
//                                StyleIt_Activity.preListSize = StyleIt_Activity.list.size();
//
//                                for (int i=0; i<products.length(); i++)
//                                {
//                                    JSONObject data = products.getJSONObject(i);
//
//                                    if (data!=null)
//                                    {
//                                        String id="",thumb="",title="",short_description="";
//
//                                        if (data.has("id"))
//                                        {
//                                            id = data.getString("id");
//                                        }
//
//                                        if (data.has("thumb"))
//                                        {
//                                            thumb = data.getString("thumb");
//                                        }
//                                        else
//                                        {
//                                            Log.e("call","thumb not available at = "+i);
//                                        }
//
//                                        if (data.has("title"))
//                                        {
//                                            title = data.getString("title");
//                                        }
//                                        else
//                                        {
//                                            Log.e("call","title not available at = "+i);
//                                        }
//
//                                        if (data.has("short_description"))
//                                        {
//                                            short_description = data.getString("short_description");
//                                        }
//                                        else
//                                        {
//                                            Log.e("call","short_description not available at = "+i);
//                                        }
//
//                                        if (thumb!=null && !thumb.trim().equalsIgnoreCase(""))
//                                        {
//                                            StyleIt_Activity.list.add(new Product_Been(id,thumb,title,short_description));
//                                        }
//                                    }
//                                    else
//                                    {
//                                        Log.e("call","data null at = "+i);
//                                    }
//                                }
//
//                                dialogClass.hideDialog();
//                                StyleIt_Activity.loadMore = true;
//                                viewPager.setAdapter(adapter);
//                                viewPager.setCurrentItem(StyleIt_Activity.preListSize-1);
//                            }
//                            else
//                            {
//                                StyleIt_Activity.loadMore = false;
//                                Log.e("call","products is null or length is zero");
//                                dialogClass.hideDialog();
//                            }
//                        }
//                        else
//                        {
//                            StyleIt_Activity.loadMore = false;
//                            Log.e("call","products not available");
//                            dialogClass.hideDialog();
//                        }
//                    }
//                    else
//                    {
//                        StyleIt_Activity.loadMore = false;
//                        Log.e("call","json null");
//                        dialogClass.hideDialog();
//                    }
//                }
//                catch (Exception e)
//                {
//                    StyleIt_Activity.loadMore = false;
//                    dialogClass.hideDialog();
//                }
//            }
//
//        });
//    }
//
//}