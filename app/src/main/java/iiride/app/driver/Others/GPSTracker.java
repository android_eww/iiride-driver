package iiride.app.driver.Others;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import android.util.Log;

import iiride.app.driver.Comman.Constants;

public class GPSTracker extends Service implements LocationListener {

	private Context mContext;
	// flag for GPS status
	boolean isGPSEnabled = false;

	// flag for network status
	boolean isNetworkEnabled = false;

	boolean canGetLocation = false;

	public Location location; // location
	double latitude; // latitude
	double longitude; // longitude
	double precision, new_latitude, new_longitude;

	// The minimum distance to change Updates in meters
	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1; // 10 meters

	// The minimum time between updates in milliseconds
	private static final long MIN_TIME_BW_UPDATES = 2000; // 1 minute

	// Declaring a Location Manager
	protected LocationManager locationManager;

	public GPSTracker(Context mContext) {
		this.mContext = mContext;
		getLocation();
	}

	public Location getLocation() {
		try
		{
			locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);

			// getting GPS status
			isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

			// getting network status
			isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

			if (!isGPSEnabled && !isNetworkEnabled)
			{
				// no network provider is enabled
				location = null;
			}
			else
			{
				this.canGetLocation = true;
				// First get location from Network Provider
				if (isNetworkEnabled)
				{
					if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
					{
						Constants.newgpsLatitude = "0";
						Constants.newgpsLongitude = "0";
						location = null;
						return null;
					}
					locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
					if (locationManager != null)
					{
						location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
						if (location != null)
						{
							latitude = location.getLatitude();
							longitude = location.getLongitude();

							Constants.newgpsLatitude = location.getLatitude() + "";
							Constants.newgpsLongitude = location.getLongitude() + "";

							precision = Math.pow(10, 6);
							new_latitude = (double) ((int) (precision * latitude)) / precision;
							new_longitude = (double) ((int) (precision * longitude)) / precision;

							Log.e("latitude##########", "" + latitude);
							Log.e("longitude##########", "" + longitude);
						}
					}

				}

				if (isGPSEnabled)
				{
					if (location == null)
					{
						if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
						{
							Constants.newgpsLatitude = "0";
							Constants.newgpsLongitude = "0";
							location = null;
							return null;
						}
						locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
						if (locationManager != null)
						{
							location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
							if (location != null)
							{
								latitude = location.getLatitude();
								longitude = location.getLongitude();

								precision =  Math.pow(10, 6);
								new_latitude = (double)((int)(precision * latitude))/precision;
								new_longitude = (double)((int)(precision * longitude))/precision;
							}
						}
					}
				}

				if (location!=null)
				{
					Constants.newgpsLatitude = location.getLatitude()+"";
					Constants.newgpsLongitude = location.getLongitude()+"";
				}
				else
				{
					Constants.newgpsLatitude = "0";
					Constants.newgpsLongitude = "0";
				}
			}

		}
		catch (Exception e)
		{
			Log.e("Exception"," = "+e.getMessage());
			e.printStackTrace();
		}
		return location;
	}

	@Override
	public void onLocationChanged(Location location)
	{
		this.canGetLocation = true;
		this.location = location;
		latitude = location.getLatitude();
		longitude = location.getLongitude();
		precision =  Math.pow(10, 6);
		new_latitude = (double)((int)(precision * latitude))/precision;
		new_longitude = (double)((int)(precision * longitude))/precision;
		Constants.newgpsLatitude = location.getLatitude()+"";
		Constants.newgpsLongitude = location.getLongitude()+"";
		Log.e("Location ",""+ Constants.newgpsLatitude+" "+ Constants.newgpsLongitude);

	}

	public void showSettingsAlert() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

		alertDialog.setTitle("GPS is settings");
		alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

		alertDialog.setPositiveButton("Settings",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						mContext.startActivity(intent);
					}
				});

		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		alertDialog.show();
	}

	@Override
	public void onProviderDisabled(String provider) {
		Log.e("call","onProviderDisabled()");
	}

	@Override
	public void onProviderEnabled(String provider)
	{
		Log.e("call","onProviderEnabled()");
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras)
	{
		Log.e("call","onStatusChanged() status = "+status);
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	/**
	 * Function to get latitude
	 * */
	public double getLatitude() {
		if (location != null) {
			latitude = location.getLatitude();

			precision =  Math.pow(10, 6);
			new_latitude = (double)((int)(precision * latitude))/precision;

			//dFormat = new DecimalFormat("#.######"); 
			//latitude= Double.valueOf(dFormat .format(latitude));
		}

		// return latitude
		return new_latitude;
	}

	/**
	 * Function to get longitude
	 * */
	public double getLongitude() {
		if (location != null) {
			longitude = location.getLongitude();

			precision =  Math.pow(10, 6);
			new_longitude = (double)((int)(precision * longitude))/precision;

			//dFormat = new DecimalFormat("#.######"); 
			//longitude= Double.valueOf(dFormat .format(longitude));
		}

		// return longitude
		return new_longitude;
	}

	/**
	 * Function to check if best network provider
	 * @return boolean
	 * */
	public boolean canGetLocation() {
		return this.canGetLocation;
	}

//	public void showSettingsAlert() {
//		// TODO Auto-generated method stub
//	}
}