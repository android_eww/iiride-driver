package iiride.app.driver.Util;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Utility {
    public static ArrayList<String> nameOfEvent = new ArrayList<String>();
    public static ArrayList<String> startDates = new ArrayList<String>();
    public static ArrayList<String> endDates = new ArrayList<String>();
    public static ArrayList<String> descriptions = new ArrayList<String>();

    // read the calendar events based on the query and return the list of event to requested class
    public static ArrayList<String> readCalendarEvent(Context context) {
        Cursor cursor = context.getContentResolver()
                .query(Uri.parse("content://com.android.calendar/events"),
                        new String[]{"calendar_id", "title", "description",
                                "dtstart", "dtend", "eventLocation"}, null,
                        null, null);
        cursor.moveToFirst();
        // fetching calendars name
        String CNames[] = new String[cursor.getCount()];

        // fetching calendars id
        nameOfEvent.clear();
        startDates.clear();
        endDates.clear();
        descriptions.clear();
        for (int i = 0; i < CNames.length; i++) {

            nameOfEvent.add(cursor.getString(1));
            startDates.add(getDate(Long.parseLong(cursor.getString(3))));
            if (cursor.getString(4) != null) {
                endDates.add(getDate(Long.parseLong(cursor.getString(4))));
            }
            descriptions.add(cursor.getString(2));
            CNames[i] = cursor.getString(1);
            cursor.moveToNext();

        }
        return nameOfEvent;
    }

    public static String getDate(long milliSeconds) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    // convert the input stream data to string
    public static StringBuilder inputStreamToString(InputStream is) {
        String rLine = "";
        StringBuilder answer = new StringBuilder();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));

        try {
            while ((rLine = rd.readLine()) != null) {
                answer.append(rLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return answer;
    }

    public static String getDateInFormat(String dateStr)
    {
        try {
            SimpleDateFormat input = new SimpleDateFormat("hh:mm a dd/MM/yyyy");
            Date dateObj = input.parse(dateStr);
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a dd MMMM yyyy");
            return sdf.format(dateObj);
        }
        catch (Exception e)
        {
            return "";
        }
    }

    public static String getDateInFormat1(String dateStr)
    {
        //dateStr = "2020-03-05 18:45:09";
        try {
            SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd hh:mm");
            Date dateObj = input.parse(dateStr);
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a dd MMMM yyyy");
            return sdf.format(dateObj);
        }
        catch (Exception e)
        {
            return "";
        }
    }
}
