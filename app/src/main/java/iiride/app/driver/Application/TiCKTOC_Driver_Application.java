package iiride.app.driver.Application;

import android.app.Activity;
import androidx.multidex.MultiDexApplication;

import android.util.Log;
import android.view.WindowManager;

import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Others.ConnectivityReceiver;


public class TiCKTOC_Driver_Application extends MultiDexApplication {

    private static TiCKTOC_Driver_Application mInstance;
    private static Activity activity;


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public static void setCurrentActivity(Activity currentActivity) {
        activity = currentActivity;
        String UserdutyStatus = SessionSave.getUserSession(Comman.USER_DRIVER_DUTY, activity);
        Log.e("TiCKTOC_Driver_App","setCurrentActivity : "+currentActivity.getLocalClassName().toString());

        ///// for sleep mode of screen
        if (UserdutyStatus != null && !UserdutyStatus.equalsIgnoreCase("") && UserdutyStatus.equalsIgnoreCase("1"))
        {
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            Log.e("FLAG_KEEP_SCREEN_ON","FLAG_KEEP_SCREEN_ON : onnnnnnn");
        }
        else
        {
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            Log.e("FLAG_KEEP_SCREEN_ON","FLAG_KEEP_SCREEN_ON : offffffff");
        }
    }

    public static Activity currentActivity() {
        return activity;
    }

    public static boolean isEmailValid(CharSequence input) {
//        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
//        Matcher matcher = pattern.matcher(input);
//        return (!matcher.matches());
        return android.util.Patterns.EMAIL_ADDRESS.matcher(input).matches();
    }

    public static synchronized TiCKTOC_Driver_Application getInstance() {
        return mInstance;
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

}