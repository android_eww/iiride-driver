package iiride.app.driver.Comman;


import io.socket.client.Socket;

public class Comman {

    ///// USER DETAIL
    public static String PREFRENCE_USER = "userDetail";
    public static String INIT_PREF = "initPref";

    public static String PREFRENCE_TOKEN = "prefToken";
    public static String DEVICE_TOKEN = "deviceToken";
    public static String INIT_DATA = "init_data";

    public static String DEVICE_TYPE = "2";

    public static String NOTIFICATION_PUT_EXTRA = "fromNotification";

    ///register flag
    public static String LOGIN_FLAG_USER = "loginFlagUser";
    public static String REGISTER_EMAIL_ACTIVITY = "registerEmailActivity";
    public static String REGISTER_OTP_ACTIVITY = "registerOtpActivity";
    public static String REGISTER_PROFILE_ACTIVITY = "registerProfileActivity";
    public static String REGISTER_VEHICLE_ACTIVITY = "registerVehicleActivity";
    public static String REGISTER_ATTACHMENT_ACTIVITY = "registerAttachmentActivity";

    public static String USER_ONE_TIME_OTP = "userOtp";
    public static String USER_INVITED_CODE = "userInvitedCode";
    public static String USER_COMPANY_ID_ARRAY = "userCompanyId_Array";
    public static String USER_COMPANY_ARRAY_NAME_FOR_STORE = "Array_CompanyId";
    public static String USER_COMPANY_ID_FOR_STORE = "CompanyId";
    public static String USER_COMPANY_NAME_FOR_STORE = "CompanyName";
    public static String USER_COMPANY_CITY_FOR_STORE = "CompanyCity";
    public static String USER_COMPANY_STATE_FOR_STORE = "CompanyState";
    public static String USER_COMPANY_COUNTRY_FOR_STORE = "CompanyCountry";

    //////Login
    /*DriverId="", CompanyId, DispatcherId, Email, FullName, MobileNo, Gender, Image, Password, Address, City, State, Country, Zipcode, RefferralCode
           , DriverLicense, AccreditationCertificate, DriverLicenseExpire, AccreditationCertificateExpire, BankName, BankAcNo, Lat, Lng, Status
              , Availability, DriverDuty, ABN, DCNumber, ProfileComplete*/

    public static String USER_CAR_MODEL_FOR_MAKER = "userCarModelForMarker";

    public static String USER_ID = "DriverId";
    public static String USER_COMPANY_ID = "userCompanyId";
    public static String USER_DISPATHER_ID = "carModel";
    public static String USER_EMAIL = "userEmail";
    public static String USER_FULL_NAME = "fullName";

    public static String USER_MOBILE_NUMBER = "userMobileNumber";
    public static String USER_COUNTRY_CODE = "countryCode";
    public static String USER_GENDER = "userGender";
    public static String USER_PROFILE_IMAGE = "userProfileImage";
    public static String USER_PASSWORD = "userPassword";
    public static String USER_RESIDENTIAL_ADDRESS = "residentialAddress";
    public static String USER_SUB_URB = "suurb";

    public static String USER_CITY = "userCity";
    public static String USER_STATE = "userState";
    public static String USER_COUNTRY = "userCountry";
    public static String USER_POST_CODE = "userPostCode";//zipcode
    public static String USER_REFERRAL_CODE = "userReferral_code";

    public static String USER_DRIVER_LICENCE = "DriverLicense";
    public static String USER_ACCREDITATION_CERTY = "AccreditationCertificate";
    public static String USER_DRIVER_lICENSE_EXPIRE = "DriverLicenseExpire";
    public static String USER_ACCREDITATION_CERTY_EXPIRE = "AccreditationCertificateExpire";

    public static String USER_BANK_NAME = "BankName";
    public static String USER_BANK_AC_NO = "BankAcNo";
    public static String USER_BANK_ACCOUNT_HOLDER_NAME = "userBankHolderName";
    public static String USER_BSB = "userBsb";
    public static String USER_ABN = "ABN";
    public static String USER_SERVICE_DESCRIPTION = "serviceDescription";


    public static String USER_LATITUDE = "userLatitude";
    public static String USER_LONGITUDE = "userLongitude";
    public static String USER_STATUS = "Status";
    public static String USER_AVAILABILITY = "Availability";

    public static String USER_DRIVER_DUTY = "DriverDuty";
    public static String USER_DC_NUMBER = "DCNumber";
    public static String USER_PROFILE_COMPLETE = "ProfileComplete";
    public static String USER_PROFILE_CATEGORY_ID = "CategoryId";
    public static String USER_PROFILE_REFERRAL_AMOUNT = "ReferralAmount";

      /*VehicleId, VehicleModel, CarCompany, CarColor, VehicleRegistrationNo, RegistrationCertificate, VehicleInsuranceCertificate
                , RegistrationCertificateExpire, VehicleInsuranceCertificateExpire, VehicleImage, Description, VehicleModelName*/

    public static String USER_VEHICLE_ID = "VehicleId";
    public static String USER_VEHICLE_MODEL = "VehicleModel";
    public static String USER_CAR_COMPANY = "CarCompany";
    public static String USER_CAR_COLOR = "CarColor";
    public static String USER_CITY_ID = "CityId";
    public static String USER_VEHICLE_REGISTRATION_NO = "VehicleRegistrationNo";

    public static String USER_REGISTRATION_CERTY = "RegistrationCertificate";
    public static String USER_VEHICLE_INSURANCE_CERTY = "VehicleInsuranceCertificate";
    public static String USER_REGISTRATION_CERTY_EXPIRE = "RegistrationCertificateExpire";
    public static String USER_VEHICLE_INSURANCE_CERTY_EXPIRE = "VehicleInsuranceCertificateExpire";
    public static String USER_VEHICLE_IMAGE = "VehicleImage";

    public static String USER_CAR_DESCRIPTION = "Description";
    public static String USER_VEHICLE_MODEL_NAME = "VehicleModelName";
    public static String USER_RATE_POINT = "ratePoint";



    //////////Passenger for tripppp session

    public static Socket socket;

    public static String USER_SOCKET_BOOKING_REQUEST_BOOKING_ID = "BookingId";
    public static String PASSENGER_ID = "passengerId";

    public static String PASSENGER_PICKUP_LOCATION = "passengerPickUpLoc";
    public static String PASSENGER_PICKUP_LAT = "passengerPickUpLat";
    public static String PASSENGER_PICKUP_LNG = "passengerPickUpLng";
    public static String PASSENGER_DROP_OFF_LOCATION = "passengerDropOffLoc";
    public static String PASSENGER_DROP_OFF_LAT = "passengerDropOffLat";
    public static String PASSENGER_DROP_OFF_LNG = "passengerDropOffLng";

    public static String DRIVER_START_TRIP_LAT = "driverStartTripLat";
    public static String DRIVER_START_TRIP_LNG = "driverStartTripLng";
    public static String DRIVER_END_TRIP_LAT = "driverEndTripLat";
    public static String DRIVER_END_TRIP_LNG = "driverEndTripLng";

    public static String PASSENGER_NAME = "passengerName";
    public static String PASSENGER_MOBILE_NO = "passengerMobNo";
    public static String PASSENGER_IMAGE = "passengerImage";
    public static String PASSENGER_NOTE = "passengerNote";
    public static String PASSENGER_FLIGHT_NO = "passengerFlightNo";
    public static String PASSENGER_TYPE = "passengerType";
    public static String PASSENGER_MOB_NO_OTHER = "passengerMobNoOther";
    public static String PASSENGER_PAYMENT_TYPE = "passengerPaymentType";
    public static String PASSENGER_BOOKING_TYPE = "BookingType";

    ////save session for start to end trip
    public static String ACCEPT_REQUEST_FLAG = "acceptReqFlag";
    public static String START_TRIP_FLAG = "startTrip";
    public static String STOP_WAITING= "stopWaiting";
    public static String START_WAITING = "startWaiting";
    public static String BOOKING_REQUEST_FOR = "bookingReqFor";   //// 0 for current,,, 1 for later booking

    public static String NOTIFICATION_COUNT_FUTURE_BOOKING = "notifiCountFutureBooking";

    ////wallet
    public static String DRIVER_WALLET_BALLENCE = "wallet_Balance";
    public static String DRIVER_CARD_COUNT_ADDED_IN_LIST = "cardCount";
    public static String USER_QR_CODE = "userQrCode";
    public static String USER_PREFERENCE_KEY_TICK_PAY_SPLASH = "splashTickPay";


    public static String USER_PREFERENCE_WEEKLY_EARNING_START_DATE = "startDate_WeeklyEarn";
    public static String USER_PREFERENCE_WEEKLY_EARNING_END_DATE = "endDate_WeeklyEarn";


    public static String CREATED_PASSCODE = "createdPasscode";
    public static String IS_PASSCODE_REQUIRED = "requiredPass";

}
