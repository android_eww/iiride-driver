package iiride.app.driver.Comman;

import android.content.Context;

import java.net.URISyntaxException;
import java.util.concurrent.TimeUnit;

import io.socket.client.IO;
import io.socket.client.Socket;
import okhttp3.OkHttpClient;

/**
 * Created by rupenmakhecha on 06/01/17.
 */

public class SocketSingleObject {

        public static SocketSingleObject instance;
        private static final String SERVER_ADDRESS = "https://iiride.com:8080";//http://3.105.155.250:8080";//"https://www.ticktoc.net:8080";
//        private static final String SERVER_ADDRESS = "http://54.206.55.185:8080";
        private Socket mSocket;
        private Context context;

        public SocketSingleObject(Context context) {
            this.context = context;
            this.mSocket = getServerSocket();
        }

        public static SocketSingleObject get(Context context){
            if(instance == null){
                instance = getSync(context);
            }
            instance.context = context;
            return instance;
        }

        private static synchronized SocketSingleObject getSync(Context context) {
            if(instance == null){
                instance = new SocketSingleObject(context);
            }
            return instance;
        }

        public Socket getSocket(){
            return this.mSocket;
        }

        public Socket getServerSocket() {
            try
            {
                /*IO.Options opts = new IO.Options();
                opts.forceNew = true;
                opts.reconnection = true;
                mSocket = IO.socket(SERVER_ADDRESS,opts);//, opts*/
                IO.Options opts = new IO.Options();
                opts.forceNew = true;
                opts.reconnection = false;
                OkHttpClient okHttpClient = new OkHttpClient.Builder()
                        .connectTimeout(20, TimeUnit.SECONDS)
                        .writeTimeout(1, TimeUnit.MINUTES)
                        .readTimeout(1, TimeUnit.MINUTES)
                        .build();
                IO.setDefaultOkHttpCallFactory(okHttpClient);
                IO.setDefaultOkHttpWebSocketFactory(okHttpClient);
                mSocket = IO.socket(SERVER_ADDRESS, opts);
                return mSocket;
            }
            catch (URISyntaxException e)
            {
                throw new RuntimeException(e);
            }
        }
}
