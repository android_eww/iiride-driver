package iiride.app.driver.Comman;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.google.gson.Gson;

import iiride.app.driver.Been.initData.InitData;

/**
 * This common class to store the require data by using SharedPreferences.
 */
public class SessionSave
{
    //Store data's into sharedPreference
    public static void saveUserSession(String key, String value, Context context) {
        Editor editor = context.getSharedPreferences(Comman.PREFRENCE_USER, Activity.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.apply();
    }

    // Get Data's from SharedPreferences
    public static String getUserSession(String key, Context context) {
        SharedPreferences prefs = context.getSharedPreferences(Comman.PREFRENCE_USER, Activity.MODE_PRIVATE);
        return prefs.getString(key, "");
    }

    //Store data's into sharedPreference
    public static void clearUserSession(Context context) {
        Editor editor = context.getSharedPreferences(Comman.PREFRENCE_USER, Activity.MODE_PRIVATE).edit();
        editor.clear();
        editor.apply();
    }

    public static void saveToken(String key, String value, Context context) {
        Editor editor = context.getSharedPreferences(Comman.PREFRENCE_TOKEN, Activity.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.apply();
    }

    // Get Data's from SharedPreferences
    public static String getToken(String key, Context context) {
        SharedPreferences prefs = context.getSharedPreferences(Comman.PREFRENCE_TOKEN, Activity.MODE_PRIVATE);
        return prefs.getString(key, "");
    }



    public static void saveInit(String key, String value, Context context) {
        Editor editor = context.getSharedPreferences(Comman.INIT_PREF, Activity.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static InitData getInitData(Context context){
        SharedPreferences prefs = context.getSharedPreferences(Comman.INIT_PREF, Activity.MODE_PRIVATE);
        return new Gson().fromJson(prefs.getString(Comman.INIT_DATA,""),InitData.class);
    }

    public static String getInitDataString(Context context){
        SharedPreferences prefs = context.getSharedPreferences(Comman.INIT_PREF, Activity.MODE_PRIVATE);
        return prefs.getString(Comman.INIT_DATA,"");
    }
}
