package iiride.app.driver.Comman;


public interface WebServiceAPI {

    String BASE_URL_DRIVER = "https://iiride.com/Drvier_Api/";//http://3.105.155.250/web/Drvier_Api/";//"https://www.ticktoc.net/web/Drvier_Api/";
    String BASE_URL_IMAGE = "https://iiride.com/";//http://3.105.155.250/web";//"https://www.ticktoc.net/web/";

//  String BASE_URL_DRIVER = "http://54.206.55.185/web/Drvier_Api/";
//  String BASE_URL_IMAGE = "http://54.206.55.185/web/";

//    String BASE_URL_DRIVER = "http://3.105.155.250/web/Drvier_Api/";

    String HEADER_KEY_NAME = "key";
    String HEADER_KEY_VALUE = "iiRide@123*#*";

    /// For socket
    String SERVER_ADDRESS = "https://www.ticktoc.net:8080";

    //Update Driver Location SOKET
    String SERVER_SOCKET_UPDATE_DRIVER_LOACATION_KEY = "UpdateDriverLatLong";
    String SOCKET_UPDATE_DRIVER_LOACATION_KEY_OBJ_DRIVER_ID = "DriverId";
    String SOCKET_UPDATE_DRIVER_LOACATION_KEY_OBJ_DRIVER_LAT = "Lat";
    String SOCKET_UPDATE_DRIVER_LOACATION_KEY_OBJ_DRIVER_LNG = "Long";
    String SOCKET_UPDATE_DRIVER_LOACATION_KEY_OBJ_TOKEN = "Token";

    ////// AriveBookingRequest
    String SERVER_SOCKET_DRIVER_ARRIVE_BOOKING_REQUEST = "AriveBookingRequest";

    ////// Reject Booking request //BookingId,DriverId
    String SERVER_SOCKET_DRIVER_REJECT_BOOKING_REQUEST = "ForwardBookingRequestToAnother";
    String SOCKET_DRIVER_REJECT_BOOKING_REQUEST_BOOKING_ID = "BookingId";
    String SOCKET_DRIVER_REJECT_BOOKING_REQUEST_DRIVER_ID = "DriverId";

    ////// Accept booking request //BookingId,DriverId
    String SERVER_SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST = "AcceptBookingRequest";
    String SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST_BOOKING_ID = "BookingId";
    String SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST_DRIVER_ID = "DriverId";
    String SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST_LAT = "Lat";
    String SOCKET_DRIVER_ACCEPT_BOOKING_REQUEST_LONG = "Long";


    ////Get booking details after booking request accepted
    String SERVER_SOCKET_GET_BOOKING_DETAIL_AFTER_ACCEPTING_REQUEST = "BookingInfo";

    ////Pickup passenger by driver // BookingId,DriverId
    String SERVER_SOCKET_PICKUP_PASSENGER = "PickupPassenger";
    String SOCKET_PICKUP_PASSENGER_BOOKING_ID = "BookingId";
    String SOCKET_PICKUP_PASSENGER_DRIVER_ID = "DriverId";

    /////Start hold trip //BookingId
    String SERVER_SOCKET_HOLD_PASSENGER = "StartHoldTrip";
    String SOCKET_HOLD_PASSENGER_BOOKING_ID = "BookingId";

    /////End hold trip //BookingId
    String SERVER_SOCKET_END_HOLD_PASSENGER = "EndHoldTrip";
    String END_SOCKET_HOLD_PASSENGER_BOOKING_ID = "BookingId";


    /////Received cancel trip notification to driver
    String SERVER_SOCKET_RECEIVE_CANCEL_NOTIFICATION = "DriverCancelTripNotification";
    String SERVER_SOCKET_UPDTED_BOOKING_DETAIL = "UpdateBookingDetails";

    ///////////Receive book later booking request
    String SERVER_SOCKET_ARIVE_ADVANCE_BOOKING_REQUEST = "AriveAdvancedBookingRequest";


    ////// Accept book later booking request
    String SERVER_SOCKET_DRIVER_ACCEPT_ADVANCE_BOOKING_REQUEST = "AcceptAdvancedBookingRequest";
    String SOCKET_DRIVER_ACCEPT_ADVANCE_BOOKING_REQUEST_BOOKING_ID = "BookingId";
    String SOCKET_DRIVER_ACCEPT_ADVANCE_BOOKING_REQUEST_DRIVER_ID = "DriverId";


    //////Reject book later booking request
    String SERVER_SOCKET_DRIVER_REJECT_BOOK_LATER_REQUEST = "ForwardAdvancedBookingRequestToAnother";
    String SOCKET_DRIVER_REJECT_BOOK_LATER_REQUEST_BOOKING_ID = "BookingId";
    String SOCKET_DRIVER_REJECT_BOOK_LATER_REQUEST_DRIVER_ID = "DriverId";

    ////Pickup passenger by driver in book later request
    String SERVER_SOCKET_PICKUP_PASSENGER_BOOK_LATER = "AdvancedBookingPickupPassenger";
    String SOCKET_PICKUP_PASSENGER_BOOK_LATER_BOOKING_ID = "BookingId";
    String SOCKET_PICKUP_PASSENGER_BOOK_LATER_DRIVER_ID = "DriverId";

    /////Received cancel trip notification to driver
    String SERVER_SOCKET_RECEIVE_CANCEL_NOTIFICATION_LATER = "AdvancedBookingDriverCancelTripNotification";


    /////Start Start hold trip later
    String SERVER_SOCKET_HOLD_PASSENGER_lATER = "AdvancedBookingStartHoldTrip";
    String SOCKET_HOLD_PASSENGER_lATER_BOOKING_ID = "BookingId";

    /////End hold trip later
    String SERVER_SOCKET_END_HOLD_PASSENGER_LATER = "AdvancedBookingEndHoldTrip";
    String END_SOCKET_HOLD_PASSENGER_LATER_BOOKING_ID = "BookingId";

    ////Get booking details after booking request accepted later
    String SERVER_SOCKET_GET_BOOKING_DETAIL_AFTER_ACCEPTING_REQUEST_LATER = "AdvancedBookingInfo";

    ///Notify passenger befor start trip
    String SERVER_SOCKET_NOTIFY_PASSENGE_FOR_ADVANCE_TRIP = "NotifyPassengerForAdvancedTrip";
    String SOCKET_NOTIFY_PASSENGE_FOR_ADVANCE_TRIP_BOOKING_ID = "BookingId";
    String SOCKET_NOTIFY_PASSENGE_FOR_ADVANCE_TRIP_DRIVER_ID = "DriverId";


    /// New book later request arrived notification
    String SERVER_SOCKET_BOOK_LATER_DRIVER_NOTIFY = "BookLaterDriverNotify";

    /// New book later request arrived notification
    String SOCKET_SESSION_ERROR = "SessionError";


    ///lOGIN :: Username,Password,Lat,Lng
    String WEB_SERVICE_LOGIN = BASE_URL_DRIVER + "Login";
    String LOGIN_PARAM_DEVICE_TYPE = "DeviceType";
    String LOGIN_PARAM_TOKEN = "Token";
    String LOGIN_PARAM_USERNAME = "Username";
    String LOGIN_PARAM_PASSWORD = "Password";
    String LOGIN_PARAM_LATITUDE = "Lat";
    String LOGIN_PARAM_LONGITUDE = "Lng";
    String LOGIN_PARAM_DEVICE_ID = "Token";


    ////FORGOT PASSWORD : Email
    String WEB_SERVICE_FORGOT_PASSWORD = BASE_URL_DRIVER + "ForgotPassword";
    String FORGOT_PASSWORD_PARAM_EMAIL = "Email";


    ///////OtpForRegister
    String WEB_SERVICE_OTP_FOR_REGISTER = BASE_URL_DRIVER + "OtpForRegister";
    String OTP_FOR_REGISTER_PARAM_EMAIL = "Email";
    String REGISTER_PARAM_DEVICE_ID = "Token";


    ////// Vehical Model list
    String WEB_SERVICE_FOR_VEHICLE_MODEL_LIST = BASE_URL_DRIVER + "TaxiModel/";


    ///Driver register ::
    String WEB_SERVICE_OTP_FOR_DRIVER_REGISTER = BASE_URL_DRIVER + "Register";
    String DRIVER_REGISTER_PARAM_DEVICE_TYPE = "DeviceType";
    String DRIVER_REGISTER_PARAM_TOKEN = "Token";
    String DRIVER_REGISTER_PARAM_EMAIL = "Email";
    String DRIVER_REGISTER_PARAM_MOBILE_NUMBER = "MobileNo";
    String DRIVER_REGISTER_PARAM_FULL_NAME = "Fullname";
    String DRIVER_REGISTER_PARAM_GENDER = "Gender";
    String DRIVER_REGISTER_PARAM_PASSWORD = "Password";
    String DRIVER_REGISTER_PARAM_ADDRESS = "Address";
    String DRIVER_REGISTER_PARAM_REFERRAL_CODE = "ReferralCode";
    String DRIVER_REGISTER_PARAM_LATITUDE = "Lat";
    String DRIVER_REGISTER_PARAM_LONGITUDE = "Lng";
    String DRIVER_REGISTER_PARAM_VEHICLE_CLASS = "VehicleClass";
    String DRIVER_REGISTER_PARAM_DRIVER_LICENCE = "DriverLicence";
    String DRIVER_REGISTER_PARAM_CAR_REGISTRATION = "CarRegistrationCertificate";
    String DRIVER_REGISTER_PARAM_ACCREDITATION_CERTY = "AccreditationCertificate";
    String DRIVER_REGISTER_PARAM_VEHICLE_INSURANCE = "VehicleInsuranceCertificate";

    String DRIVER_REGISTER_PARAM_COMPANY_ID = "CompanyId";
    String DRIVER_REGISTER_PARAM_SUB_URB = "Suburb";
    String DRIVER_REGISTER_PARAM_CITY = "City";
    String DRIVER_REGISTER_PARAM_COUNTRY = "Country";
    String DRIVER_REGISTER_PARAM_STATE = "State";
    String DRIVER_REGISTER_PARAM_ZIPCODE = "Zipcode";
    String DRIVER_REGISTER_PARAM_DRIVER_IMAGE = "DriverImage";
    String DRIVER_REGISTER_PARAM_DRIVER_LICENCE_EXPIRY = "DriverLicenseExpire";

    String DRIVER_REGISTER_PARAM_ACCREDITATION_CERTY_EXPIRY = "AccreditationCertificateExpire";
    String DRIVER_REGISTER_PARAM_BANK_ACCOUNT_HOLDER_NALE = "AccountHolderName";
    String DRIVER_REGISTER_PARAM_BANK_NAME = "BankName";
    String DRIVER_REGISTER_PARAM_BANK_ACCOUNT_NO = "BankAcNo";
    String DRIVER_REGISTER_PARAM_BSB = "BSB";
    String DRIVER_REGISTER_PARAM_ABN = "ABN";
    String DRIVER_REGISTER_PARAM_SERVICE_DESCRIPTION = "Description";
    String DRIVER_REGISTER_PARAM_VEHICLE_COLOR = "VehicleColor";
    String DRIVER_REGISTER_PARAM_VEHICLE_RIGISTRATION_NO = "VehicleRegistrationNo";
    String DRIVER_REGISTER_PARAM_REGISTRATION_CERTY_EXPIRY = "RegistrationCertificateExpire";
    String DRIVER_REGISTER_PARAM_VEHICLE_INSURANCE_CERTY_EXPIRE = "VehicleInsuranceCertificateExpire";
    String DRIVER_REGISTER_PARAM_VEHICLE_IMAGE = "VehicleImage";
    String DRIVER_REGISTER_PARAM_COMPANY_MODEL = "CompanyModel";
    String DRIVER_REGISTER_PARAM_CITY_ID = "CityId";


    ////////Change driver Status //DriverId, Lat, Lng
    String WEB_SERVICE_CHANGE_DRIVER_SHIFT_STATUS = BASE_URL_DRIVER + "ChangeDriverShiftStatus/";//DriverId
    String CHANGE_DRIVER_SHIFT_STATUS_PARAM_USER_ID = "DriverId";
    String CHANGE_DRIVER_SHIFT_STATUS_PARAM_LAT = "Lat";
    String CHANGE_DRIVER_SHIFT_STATUS_PARAM_LNG = "Lng";


    ///// http://54.206.55.185/web/Drvier_Api/GetDriverProfile/driver_id
    String WEB_SERVICE_GET_DRIVER_PROFILE = BASE_URL_DRIVER + "GetDriverProfile/";//driver_id

    ///// Company list
    String WEB_SERVICE_GET_COMPANY_ID_LIST = BASE_URL_DRIVER + "Company";



    /////Update Prfile///UpdateDriverBasicInfo
    /*DriverId,CompanyId,Fullname,Gender,Address,Suburb,Zipcode,City,State,Country,DriverImage*/
    String WEB_SERVICE_UPDATE_DRIVER_BASIC_INFO = BASE_URL_DRIVER + "UpdateDriverBasicInfo";//driver_id
    String UPDATE_DRIVER_BASIC_INFO_PARAM_DIVER_ID = "DriverId";
    String UPDATE_DRIVER_BASIC_INFO_PARAM_COMPANY_ID = "CompanyId";
    String UPDATE_DRIVER_BASIC_INFO_PARAM_FULL_NAME = "Fullname";
    String UPDATE_DRIVER_BASIC_INFO_PARAM_GENDER = "Gender";
    String UPDATE_DRIVER_BASIC_INFO_PARAM_ADDRESS = "Address";
    String UPDATE_DRIVER_BASIC_INFO_PARAM_SUB_URB = "Suburb";
    String UPDATE_DRIVER_BASIC_INFO_PARAM_ZIPCODE = "Zipcode";
    String UPDATE_DRIVER_BASIC_INFO_PARAM_CITY = "City";
    String UPDATE_DRIVER_BASIC_INFO_PARAM_STATE = "State";
    String UPDATE_DRIVER_BASIC_INFO_PARAM_COUNTRY = "Country";
    String UPDATE_DRIVER_BASIC_INFO_PARAM_DRIVER_IMAGE = "DriverImage";


    //Update All driver documents
    String WEB_SERVICE_UPDATE_DOCUMENT = BASE_URL_DRIVER + "UpdateDocument";//driver_id
    String WEB_SERVICE_UPDATE_DOCS = BASE_URL_DRIVER + "UpdateDocs";
    String UPDATE_DOCUMENT_PARAM_DIVER_ID = "DriverId";
    String UPDATE_DOCUMENT_PARAM_DIVER_LICENCE = "DriverLicence";
    String UPDATE_DOCUMENT_PARAM_DIVER_VEHICLE_INSURANCE_CERY = "VehicleInsuranceCertificate";
    String UPDATE_DOCUMENT_PARAM_DIVER_ACCREDITATION_CERTY = "AccreditationCertificate";
    String UPDATE_DOCUMENT_PARAM_DIVER_REGISTRATION_CERTY = "CarRegistrationCertificate";
    String UPDATE_DOCUMENT_PARAM_VEHICLE_IMAGE = "VehicleImage";
    String UPDATE_DOCUMENT_PARAM_LICENSE_EXPIRY = "DriverLicenseExpire";
    String UPDATE_DOCUMENT_PARAM_INSURANCE_CERTY_EXPIRY = "VehicleInsuranceCertificateExpire";
    String UPDATE_DOCUMENT_PARAM_ACCREDITATION_EXPIRY = "AccreditationCertificateExpire";
    String UPDATE_DOCUMENT_PARAM_REGISTERATION_CERTY_EXPIRY = "RegistrationCertificateExpire";


    ////Update Driver Car Info /*DriverId,VehicleClass,VehicleColor,CompanyModel,VehicleRegistrationNo*/
    String WEB_SERVICE_UPDATE_DRIVER_CAR_INFO = BASE_URL_DRIVER + "UpdateVehicleInfo";//driver_id
    String UPDATE_DRIVER_CAR_INFO_PARAM_DIVER_ID = "DriverId";
    String UPDATE_DRIVER_CAR_INFO_PARAM_VEHICLE_CLASS = "VehicleClass";
    String UPDATE_DRIVER_CAR_INFO_PARAM_VEHICLE_COLOR = "VehicleColor";
    String UPDATE_DRIVER_CAR_INFO_PARAM_VEHICLE_MODEL = "CompanyModel";
    String UPDATE_DRIVER_CAR_INFO_PARAM_VEHICLE_REGISTRATION_NO = "VehicleRegistrationNo";


    //Update Driver bank info // DriverId,AccountHolderName,BankName,BankAcNo,BSB,ABN
    String WEB_SERVICE_UPDATE_BANK_INFO = BASE_URL_DRIVER + "UpdateBankInfo";//driver_id
    String UPDATE_BANK_INFO_PARAM_DRIVER_ID = "DriverId";
    String UPDATE_BANK_INFO_PARAM_ACCOUNT_HOLDER_NAME = "AccountHolderName";
    String UPDATE_BANK_INFO_PARAM_BANK_NAME = "BankName";
    String UPDATE_BANK_INFO_PARAM_BANK_AC_NO = "BankAcNo";
    String UPDATE_BANK_INFO_PARAM_BSB = "BSB";
    String UPDATE_BANK_INFO_PARAM_ABN = "ABN";
    String UPDATE_BANK_INFO_PARAM_SERVICE_DESCRIPTION = "Description";


    /////Completed trip successfully//BookingId,TripDistance,NightFareApplicable,PromoCode,PaymentType,PaymentStatus,TransactionId,TollFee
    String WEB_SERVICE_COMPLETE_TRIP = BASE_URL_DRIVER + "SubmitCompleteBooking";
    String COMPLETE_TRIP_PARAM_BOOKING_ID = "BookingId";
    String COMPLETE_TRIP_PARAM_TRIP_DISTANCE = "TripDistance";
    String COMPLETE_TRIP_PARAM_NIGHT_FARE_APPLICABLE = "NightFareApplicable";///"0"
    String COMPLETE_TRIP_PARAM_PROMO_CODE = "PromoCode"; //" "
    String COMPLETE_TRIP_PARAM_PAYMENT_TYPE = "PaymentType";// cash , wallet, online
    String COMPLETE_TRIP_PARAM_PAYMENT_STATUS = "PaymentStatus"; //" "
    String COMPLETE_TRIP_PARAM_TRANSACTION_ID = "TransactionId"; // " "
    String COMPLETE_TRIP_PARAM_TOLL_FEE = "TollFee"; //"0"
    String COMPLETE_TRIP_PARAM_DROP_OFF_LOCATION = "DropoffLocation"; //"0"
    String COMPLETE_TRIP_PARAM_LAT = "Lat"; //"0"
    String COMPLETE_TRIP_PARAM_LONG = "Long"; //"0"


    /////COMPLETE TRIP OF BOOK LATER
    String WEB_SERVICE_COMPLETE_TRIP_LATER = BASE_URL_DRIVER + "SubmitCompleteAdvancedBooking";


    ///change password
    String WEB_SERVICE_CHANGE_PASSWORD = BASE_URL_DRIVER + "ChangePassword";
    String CHANGE_PASSWORD_PARAM_DRIVER_ID = "DriverId";
    String CHANGE_PASSWORD_PARAM_PASSWORD = "Password";


    ///////get list of Distpatch jb
//    String WEB_SERVICE_DISPATCH_JOB_LIST = BASE_URL_DRIVER + "DispatchJob/";

    /////List of my dispatch job (For dispatch job menu)
    String WEB_SERVICE_MY_DISPATCH_JOB_LIST = BASE_URL_DRIVER + "MyDispatchJob/";

    ////List of nearby upcoming dipatch job(for future booking)
    String WEB_SERVICE_FUTURE_BOOKING_LIST = BASE_URL_DRIVER + "FutureBooking/";

    ////Accept dispatch job request
    String WEB_SERVICE_ACCEPT_FUTURE_JOB_REQUEST = BASE_URL_DRIVER + "AcceptDispatchJobRequest/";

    ///////Booking history
    String WEB_SERVICE_BOOKING_HISTORY_LIST = BASE_URL_DRIVER + "BookingHistory/";


    /////Driver Logout : /DriverId
    String WEB_SERVICE_DRIVER_LOGOUT = BASE_URL_DRIVER + "Logout/";


    ////Submit book now for dispatch job
    /// ,,,,,,PaymentType(collect,account)
    String WEB_SERVICE_DISPATCH_JOB_BOOK_NOW = BASE_URL_DRIVER + "SubmitBookNowByDispatchJob";
    String DISPATCH_JOB_BOOK_NOW_PARAM_DRIVER_ID = "DriverId";
    String DISPATCH_JOB_BOOK_NOW_PARAM_MODEL_ID = "ModelId";
    String DISPATCH_JOB_BOOK_NOW_PARAM_PICK_UP_LOC = "PickupLocation";
    String DISPATCH_JOB_BOOK_NOW_PARAM_DROP_OFF_LOC = "DropoffLocation";
    String DISPATCH_JOB_BOOK_NOW_PARAM_PASSENGER_NAME = "PassengerName";
    String DISPATCH_JOB_BOOK_NOW_PARAM_PASSENGER_CONTACT_NO = "PassengerContact";
    String DISPATCH_JOB_BOOK_NOW_PARAM_PASSENGER_EMAIL = "PassengerEmail";
    String DISPATCH_JOB_BOOK_NOW_PARAM_FARE_AMOUNT = "FareAmount";
    String DISPATCH_JOB_BOOK_NOW_PARAM_PAYMENT_TYPE = "PaymentType";
    String DISPATCH_JOB_BOOK_NOW_PARAM_FLIGHT_NO = "FlightNumber";
    String DISPATCH_JOB_BOOK_NOW_PARAM_NOTE = "Notes";

    ///Submit book later for dispatch job
    String WEB_SERVICE_DISPATCH_JOB_BOOK_LATER = BASE_URL_DRIVER + "SubmitBookLaterByDispatchJob";
    String DISPATCH_JOB_BOOK_LATER_PARAM_PICK_UP_DATE_TIME = "PickupDateTime";


    //////Current trip FLOW
    String WEB_SERVICE_CURRENT_TRIP_FLOW = BASE_URL_DRIVER + "CurrentBooking/";

    ////Add new card ///DriverId,CardNo,Cvv,Expiry,Alias (CarNo : 4444555511115555,Expiry:09/20)
    String WEB_SERVICE_ADD_NEW_CARD = BASE_URL_DRIVER + "AddNewCard";
    String ADD_NEW_CARD_PARAM_DRIVER_ID = "DriverId";
    String ADD_NEW_CARD_PARAM_CARD_NO = "CardNo";
    String ADD_NEW_CARD_PARAM_CVV = "Cvv";
    String ADD_NEW_CARD_PARAM_EXPIRY = "Expiry";
    String ADD_NEW_CARD_PARAM_ALIAS = "Alias";


    /////Card listing
    String WEB_SERVICE_DABITCARD_LIST = BASE_URL_DRIVER + "Cards/";//DriverId


    /////Add Money ////DriverId,Amount,CardId
    String WEB_SERVICE_ADD_MONEY = BASE_URL_DRIVER + "AddMoney";
    String ADD_MONEY_PARAM_DRIVER_ID = "DriverId";
    String ADD_MONEY_PARAM_AMOUNT = "Amount";
    String ADD_MONEY_PARAM_CARD_ID = "CardId";


    /////Transaction History
    String WEB_SERVICE_TRANSACTION_HISTORY = BASE_URL_DRIVER + "TransactionHistory/";///DriverId


    //////Tickpay ///DriverId,Name,Amount,CardNo,CVV,Expiry
    String WEB_SERVICE_TICKPAY = BASE_URL_DRIVER + "Tickpay";
    String TICKPAY_PARAM_DRIVER_ID = "DriverId";
    String TICKPAY_PARAM_DRIVER_NAME = "Name";
    String TICKPAY_PARAM_AMOUNT = "Amount";
    String TICKPAY_PARAM_CARD_NO = "CardNo";
    String TICKPAY_PARAM_CVV = "CVV";
    String TICKPAY_PARAM_CARD_EXPIRY = "Expiry";
    String TICKPAY_PARAM_ABN = "ABN";


    // get qr code detials //QRCode
    String WEB_SERVICE_QR_CODE_DETAILS = BASE_URL_DRIVER + "QRCodeDetails/";
    String PARAM_QR_CODE = "QRCode";

    // Send Money   //QRCode,SenderId,Amount
    String WEB_SERVICE_SEND_MONEY = BASE_URL_DRIVER + "SendMoney";
    String SEND_MONEY_PARAM_QRCODE = "QRCode";
    String SEND_MONEY_PARAM_SENDER_ID = "SenderId";
    String SEND_MONEY_PARAM_AMOUNT = "Amount";


    ////Remove Card
    String WEB_SERVICE_REMOVE_CARD = BASE_URL_DRIVER + "RemoveCard/";///DriverId/CardId

    //Get Tickpay Rate
    String WEB_SERVICE_GET_TICKPAY_RATE = BASE_URL_DRIVER + "GetTickpayRate";

    //send invoice
    String WEB_SERVICE_TICK_PAY_INVOICE = BASE_URL_DRIVER + "TickpayInvoice";
    String PARAM_TICKPAY_ID = "TickpayId";
    String PARAM_INVOICE_TYPE = "InvoiceType";
    String PARAM_INVOICE_TYPE_EMAIL = "Email";
    String PARAM_INVOICE_TYPE_SMS = "SMS";
    String PARAM_CUSTOMER_NAME = "CustomerName";
    String PARAM_NOTES = "Notes";
    String PARAM_AMOUNT = "Amount";
    String PARAM_EMAIL = "Email";
    String PARAM_MOBILE_NO = "MobileNo";

    ////// App settings
    String WEB_SERVICE_APP_SETTING = BASE_URL_DRIVER + "Init/";///version/app_type
    String APP_SETTING_APP_TYPE = "/AndroidDriver";///version/app_type


    //////Weekly Earning /DriverId
    String WEB_SERVICE_WEEKLY_EARNING = BASE_URL_DRIVER + "WeeklyEaring/";

    /////Rating and comment   ////BookingId,Rating,Comment,BookingType(BookNow,BookLater)
    String WEB_SERVICE_RATING_AND_COMMENT = BASE_URL_DRIVER + "ReviewRating";
    String PARAM_RATING_AND_COMMENT_BOOKING_ID = "BookingId";
    String PARAM_RATING_AND_COMMENT_RATING = "Rating";
    String PARAM_RATING_AND_COMMENT_DRESCRIPTION = "Comment";
    String PARAM_RATING_AND_COMMENT_BOOKING_TYPE = "BookingType";

    ////Transfer money wallet to bank ///DriverId,Amount,HolderName,ABN,BankName,BSB,AccountNo
    String WEB_SERVICE_TRANSFERT_TO_BANK = BASE_URL_DRIVER + "TransferToBank";
    String PARAM_TRANSFERT_TO_BANK_DRIVER_ID = "DriverId";
    String PARAM_TRANSFERT_TO_BANK_AMOUNT = "Amount";
    String PARAM_TRANSFERT_TO_BANK_HOLDER_NAME = "HolderName";
    String PARAM_TRANSFERT_TO_BANK_ABN = "ABN";
    String PARAM_TRANSFERT_TO_BANK_BANK_NAME = "BankName";
    String PARAM_TRANSFERT_TO_BANK_BSB = "BSB";
    String PARAM_TRANSFERT_TO_BANK_ACCOUNT_NO = "AccountNo";

    ///GET ASTIMATE FARE//PickupLocation,DropoffLocation,ModelId
    String WEB_SERVICE_GET_ESTIMATE_FARE = BASE_URL_DRIVER + "GetEstimateFare";
    String PARAM_GET_ESTIMATE_FARE_PICKUP_LOCATION = "PickupLocation";
    String PARAM_GET_ESTIMATE_FARE_DROPOFF_LOCATION = "DropoffLocation";
    String PARAM_GET_ESTIMATE_FARE_MODEL_ID = "ModelId";


    ////http://54.206.55.185/web/Drvier_Api/CancelDispatch/  ==== DriverId , BookingId , BookingType
    String WEB_SERVICE_CANCEL_DISPATCH_JOB = BASE_URL_DRIVER + "CancelDispatch";
    String CANCEL_DISPATCH_JOB_DRIVER_ID = "DriverId";
    String CANCEL_DISPATCH_JOB_BOOKING_ID = "BookingId";
    String CANCEL_DISPATCH_JOB_BOOKING_TYPE = "BookingType";


    String WEB_SERVICE_CAR_CLASS = BASE_URL_DRIVER + "TaxiModelCityWise/";


    //  String WEB_SERVICE_SERVICE_PROVIDR_FORM = BASE_URL_DRIVER+"ServiceProviderContactUs";
    String WEB_SERVICE_SERVICE_PROVIDR_FORM = BASE_URL_DRIVER + "ServiceProviderContactUsNew";
    String PARAM_COMPANY_NAME = "CompanyName";
    String PARAM_ABN = "ABN";
    String PARAM_EMAIL_ = "Email";
    String PARAM_MOBILE_NO_ = "MobileNo";
    String PARAM_PHYSICAL_ADDRESS = "CompanyPhysicalAddress";
    String PARAM_CONTACT_NAME = "ContactName";
    String PARAM_BUSINESS_PHONE_NUMBER = "BusinessPhoneNumber";


    //// Driver cancel advance booking after accept CancelAdvanceBookingManually
    String WEB_SERVICE_CANCEL_ADVANCE_BOOKING_MANUALLY = BASE_URL_DRIVER + "CancelAdvanceBookingManually";
    String PARAM_CANCEL_ADVANCE_BOOKING_BOOKINGID = "BookingId";
    String PARAM_CANCEL_ADVANCE_BOOKING_DRIVERID = "DriverId";

}
