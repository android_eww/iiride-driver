package iiride.app.driver.Fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import iiride.app.driver.Activity.Drawer_Activity;
import iiride.app.driver.Adapter.VehicleType_Name_Adapter;
import iiride.app.driver.Been.VehicleType_Been;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.Constants;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;
import iiride.app.driver.Others.ConnectivityReceiver;
import iiride.app.driver.Others.GPSTracker;


import iiride.app.driver.R;
import iiride.app.driver.Util.Utility;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DecimalFormat;


public class DispatchJob_Fragment extends Fragment {

    public String TAG = "DispatchJob_Fragment";
    public static Animation slide_right, slide_left;

    LinearLayout main_layout;

    ArrayList<VehicleType_Been> list_vehicleItem = new ArrayList<>();

    RelativeLayout rl_account, rl_collect;
    ImageView iv_select_account, iv_select_collect;
    public static TextView tv_date_picker, tv_book_now, tv_book_later, tv_back, tv_submit, tv_vehicle_type, tv_dollar, tv_eta;
//    public static AutoCompleteTextView tv_pickupLocation, tv_dropLocation;
    public static TextView tv_pickupLocation, tv_dropLocation;
    public static LinearLayout ll_fill_data, ll_twoButton_dispatch, ll_eta;
    public static EditText et_customerName, et_mobNumber, et_fareAmount, et_passengerFlightNo, et_passengerNote;///et_customerEmail


    ArrayList<String> names, Place_id_type;
    static JSONObject json;
    String jsonurl = "";
    paserdata parse;
    JSONArray contacts = null;
    private static final String TAG_RESULT = "predictions";
    String place_id;
    private boolean dropdownlist_istouch = false;
    String bookClickType = "", BOOK_NOW = "BookNow", BOOK_LATER = "BookLater";
    String amountClickType = "", ACCOUNT = "account", COLLECT = "collect";
    String Pick_Drop_ClickType = "", PICK_UP = "pickUp", DROP_OFF = "dropOff";
    String PickUpLoc_Address = "", DropOffLoc_Address="";
    List<String> list_deviceModelId ;
    int checked=0;

    DialogClass dialogClass;
    AQuery aQuery;
    VehicleType_Name_Adapter adapter;
    public static ListView dialog_ListView;
    String selectedModelId = "", DateBookLater="";
//    static final long ONE_MINUTE_IN_MILLIS=45*60*1000;
    static final long ONE_MINUTE_IN_MILLIS = 18*100000;

    String getEstimateFare="", eta="";

    int PICKUP_AUTOCOMPLETE_REQUEST_CODE = 201;
    int DROPOFF_AUTOCOMPLETE_REQUEST_CODE = 202;
    List<Place.Field> fields;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dispatch, container, false);

        aQuery = new AQuery(getActivity());
        dialogClass = new DialogClass(getActivity(), 1);
        main_layout = (LinearLayout) rootView.findViewById(R.id.main_layout);

        Places.initialize(getActivity(), getString(R.string.api_key_google_map));
        fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);

        getEstimateFare="";

        rl_account = (RelativeLayout) rootView.findViewById(R.id.rl_account);
        rl_collect = (RelativeLayout) rootView.findViewById(R.id.rl_collect);

        ll_twoButton_dispatch = (LinearLayout) rootView.findViewById(R.id.ll_twoButton_dispatch);
        ll_fill_data = (LinearLayout) rootView.findViewById(R.id.ll_fill_data);
        ll_eta = (LinearLayout) rootView.findViewById(R.id.ll_eta);

        ll_twoButton_dispatch.setVisibility(View.VISIBLE);
        ll_fill_data.setVisibility(View.GONE);

        iv_select_account = (ImageView) rootView.findViewById(R.id.iv_select_account);
        iv_select_collect = (ImageView) rootView.findViewById(R.id.iv_select_collect);

        tv_back = (TextView) rootView.findViewById(R.id.tv_back);
        tv_submit = (TextView) rootView.findViewById(R.id.tv_submit);
        tv_book_now = (TextView) rootView.findViewById(R.id.tv_book_now);
        tv_book_later = (TextView) rootView.findViewById(R.id.tv_book_later);
        tv_date_picker = (TextView) rootView.findViewById(R.id.tv_date_picker);
        tv_pickupLocation = rootView.findViewById(R.id.tv_pickupLocation);
        tv_dropLocation = rootView.findViewById(R.id.tv_dropLocation);
        tv_vehicle_type = (TextView) rootView.findViewById(R.id.tv_vehicle_type);
        tv_dollar = (TextView) rootView.findViewById(R.id.tv_dollar);
        tv_eta = (TextView) rootView.findViewById(R.id.tv_eta);

        et_customerName = (EditText) rootView.findViewById(R.id.et_customerName);
        et_mobNumber = (EditText) rootView.findViewById(R.id.et_mobNumber);
//        et_customerEmail = (EditText) rootView.findViewById(R.id.et_customerEmail);
        et_fareAmount = (EditText) rootView.findViewById(R.id.et_fareAmount);
        et_passengerFlightNo = (EditText) rootView.findViewById(R.id.et_passengerFlightNo);
        et_passengerNote = (EditText) rootView.findViewById(R.id.et_passengerNote);

        slide_right = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_slide_in_right);
        slide_left = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_slide_in_left);
        DateBookLater="";



        et_fareAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

                if(charSequence.length()>0)
                {
                    if (charSequence.toString().trim().length()==1 && charSequence.toString().equalsIgnoreCase("."))
                    {
                        et_fareAmount.setText("");
                        tv_dollar.setVisibility(View.GONE);
                    }
                    else
                    {
                        tv_dollar.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    tv_dollar.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        list_vehicleItem.clear();
        if (SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL,getActivity())!= null && !SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL,getActivity()).equalsIgnoreCase(""))
        {
            list_deviceModelId = Arrays.asList(SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL,getActivity()).split("\\s*,\\s*"));
        }

        tv_book_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookClickType = BOOK_NOW;
                ll_twoButton_dispatch.setVisibility(View.GONE);
                ll_fill_data.setVisibility(View.VISIBLE);
                tv_date_picker.setVisibility(View.GONE);
                ll_fill_data.setAnimation(slide_left);
                slide_left.start();
                callClearData();
            }
        });

        tv_book_later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookClickType = BOOK_LATER;
                ll_twoButton_dispatch.setVisibility(View.GONE);
                ll_fill_data.setVisibility(View.VISIBLE);
                tv_date_picker.setVisibility(View.VISIBLE);
                ll_fill_data.setAnimation(slide_left);
                slide_left.start();
                callClearData();
            }
        });

        amountClickType = ACCOUNT;
        rl_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                amountClickType = ACCOUNT;
                rl_account.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorDarkGray_transparent));
                rl_collect.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorWhite));
                iv_select_account.setVisibility(View.VISIBLE);
                iv_select_collect.setVisibility(View.GONE);
            }
        });

        rl_collect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                amountClickType = COLLECT;
                rl_collect.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorDarkGray_transparent));
                rl_account.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorWhite));
                iv_select_account.setVisibility(View.GONE);
                iv_select_collect.setVisibility(View.VISIBLE);
            }
        });

        tv_date_picker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                int yy = calendar.get(Calendar.YEAR);
                int mm = calendar.get(Calendar.MONTH);
                int dd = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePicker = new DatePickerDialog(getActivity(), R.style.DialogThemeReal, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, final int year, final int monthOfYear, final int dayOfMonth)
                    {
                        final String date = String.valueOf(dayOfMonth) +"-"+String.valueOf(monthOfYear+1) +"-"+String.valueOf(year);


                        final Calendar mcurrentTime = Calendar.getInstance();
                        mcurrentTime.add(Calendar.MINUTE, 31);
                        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                        final int minute = mcurrentTime.get(Calendar.MINUTE);
                        Log.e("minute","1111111111111111111: "+minute);

                        TimePickerDialog mTimePicker;
                        mTimePicker = new TimePickerDialog(getActivity(), R.style.DialogThemeReal , new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                                Calendar datetime = Calendar.getInstance();
                                datetime.add(Calendar.MINUTE, 31);
                                datetime.set(Calendar.HOUR_OF_DAY, selectedHour);
                                datetime.set(Calendar.MINUTE, selectedMinute);

                                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                                try
                                {
                                    String abcd = String.valueOf(dayOfMonth) +"-"+String.valueOf(monthOfYear+1) +"-"+String.valueOf(year)+" "+ selectedHour+":"+selectedMinute;
                                    Date date1 = formatter.parse(abcd);
                                    Log.e("aa","date2 :  : is Greater than my date1 : "+ formatter.format(date1));
                                    Log.e("aa","date2 :  : is Greater than my date1 : "+ formatter.format(new Date(System.currentTimeMillis()+ ONE_MINUTE_IN_MILLIS)));

                                    tv_date_picker.setText("");

                                    if (date1.before(new Date(System.currentTimeMillis()+ ONE_MINUTE_IN_MILLIS)))
                                    {
                                        Log.e("aa","date2 : before ");
                                        tv_date_picker.setText("");
                                        DateBookLater="";
                                        Toast.makeText(getActivity(), "Invalid Time", Toast.LENGTH_SHORT).show();
                                    }
                                    else
                                    {
                                        Log.e("aa","date2 : after or equal ");
                                        DateBookLater = String.valueOf(year)+"-"+String.valueOf(monthOfYear+1)+"-"+String.valueOf(dayOfMonth)+" "+ selectedHour + "-" + selectedMinute + "-00";

                                        String format;
                                        if (selectedHour == 0)
                                        {
                                            selectedHour += 12;
                                            format = "AM";
                                        }
                                        else if (selectedHour == 12)
                                        {
                                            format = "PM";
                                        }
                                        else if (selectedHour > 12)
                                        {
                                            selectedHour -= 12;
                                            format = "PM";
                                        }
                                        else
                                        {
                                            format = "AM";
                                        }
                                        DecimalFormat formatterDeci = new DecimalFormat("00");
                                        tv_date_picker.setText(String.valueOf(dayOfMonth) +"-"+String.valueOf(monthOfYear+1) +"-"+String.valueOf(year) + " "+ formatterDeci.format(selectedHour) + ":" + formatterDeci.format(selectedMinute) + " "+format);
                                        Log.e("DateBookLater","DateBookLater : "+DateBookLater);
                                    }
                                }
                                catch (ParseException e)
                                {
                                    e.printStackTrace();
                                    tv_date_picker.setText("");
                                    DateBookLater="";
                                    Log.e("aa","date2 :  : ParseException : "+ e.getMessage().toString());
                                }
                            }
                        }, hour, minute, false);//Yes 24 hour time
                        mTimePicker.setTitle("Select Time");
                        mTimePicker.show();
                    }
                }, yy, mm, dd);
                datePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePicker.show();
            }
        });

        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_customerName.setText("");
                et_mobNumber.setText("");
//                et_customerEmail.setText("");
                et_fareAmount.setText("");
                tv_pickupLocation.setText("");
                tv_dropLocation.setText("");
                tv_date_picker.setText("");
                et_passengerFlightNo.setText("");
                et_passengerNote.setText("");
                DateBookLater ="";

                ll_fill_data.setVisibility(View.GONE);
                ll_twoButton_dispatch.setVisibility(View.VISIBLE);
                ll_twoButton_dispatch.setAnimation(slide_right);
                slide_right.start();
            }
        });

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String DriverId = SessionSave.getUserSession(Comman.USER_ID, getActivity());
                if(DriverId!=null && !DriverId.equalsIgnoreCase(""))
                {
                    CheckData(DriverId);
                }
            }
        });

        selectedModelId = "";
        tv_vehicle_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected())
                {
                    if (SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL, getActivity()) != null && !SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL, getActivity()).equalsIgnoreCase(""))
                    {
                        list_deviceModelId = Arrays.asList(SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL, getActivity()).split("\\s*,\\s*"));
                    }
                    final Dialog dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_vehicle_type);

                    dialog_ListView = (ListView) dialog.findViewById(R.id.lv_vehicle_type);
                    adapter = new VehicleType_Name_Adapter(getActivity(), list_vehicleItem);
                    dialog_ListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                    dialog_ListView.setAdapter(adapter);

                    TextView dialogButton = (TextView) dialog.findViewById(R.id.dialogButtonOK);

                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            String SelectedVehicle = "";
                            for (int i = 0; i < list_vehicleItem.size(); i++) {
                                if (list_vehicleItem.get(i).getStatus().equalsIgnoreCase("1")) {
                                    SelectedVehicle = list_vehicleItem.get(i).getName();
                                    selectedModelId = list_vehicleItem.get(i).getId();
                                    CallMethodGetEstimatedFare();
                                }
                            }

                            if (SelectedVehicle != null && !SelectedVehicle.equalsIgnoreCase("")) {
                                tv_vehicle_type.setText(SelectedVehicle);
                            } else {
                                tv_vehicle_type.setText(getActivity().getResources().getString(R.string.spinner_title));
                                selectedModelId = "";
                            }
                        }
                    });

                    dialog.show();
                    FindWhichTypeIs_Visible();
                }
                else
                {
                    new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        });


//        tv_pickupLocation.addTextChangedListener(new TextWatcher() {
//            String search_text[];
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                // TODO Auto-generated method stub
//                Pick_Drop_ClickType = PICK_UP;
//                PickUpLoc_Address ="";
//
//                search_text = tv_pickupLocation.getText().toString().split(",");
//                GPSTracker gpsTracker = new GPSTracker(getActivity());
//                double lat=0,lng=0;
//                if (gpsTracker.canGetLocation())
//                {
//                    lat = gpsTracker.getLatitude();
//                    lng = gpsTracker.getLongitude();
//                }
//                jsonurl = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + search_text[0].replace(" ", "%20")+"&location="+ lat+","+ lng+ "&radius=1000&sensor=true&key="+getResources().getString(R.string.api_key_google_map)+"&components=country:au&language=en";
//                Log.e("jsonurl","jsonurl : "+jsonurl);
//                names = new ArrayList<String>();
//                Place_id_type = new ArrayList<String>();
//                if (parse != null) {
//                    parse.cancel(true);
//                    parse = null;
//                }
//                parse = new paserdata(tv_pickupLocation);
//                parse.execute();
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                // TODO Auto-generated method stub
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//            }
//        });
//
//        tv_dropLocation.addTextChangedListener(new TextWatcher() {
//            String search_text[];
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                // TODO Auto-generated method stub
//                Pick_Drop_ClickType = DROP_OFF;
//                DropOffLoc_Address ="";
//
//                search_text = tv_dropLocation.getText().toString().split(",");
//                GPSTracker gpsTracker = new GPSTracker(getActivity());
//                double lat=0,lng=0;
//                if (gpsTracker.canGetLocation())
//                {
//                    lat = gpsTracker.getLatitude();
//                    lng = gpsTracker.getLongitude();
//                }
//
//                jsonurl = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + search_text[0].replace(" ", "%20")+"&location="+ lat+","+ lng + "&radius=1000&sensor=true&key="+getResources().getString(R.string.api_key_google_map)+"&components=country:au&language=en";
//                Log.e("jsonurl","jsonurl : "+jsonurl);
//                names = new ArrayList<String>();
//                Place_id_type = new ArrayList<String>();
//                if (parse != null) {
//                    parse.cancel(true);
//                    parse = null;
//                }
//                parse = new paserdata(tv_dropLocation);
//                parse.execute();
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                // TODO Auto-generated method stub
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//            }
//        });

        tv_pickupLocation.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
//                        .setCountry("AU")
                        .setCountry("IN")
                        .build(getActivity());
                startActivityForResult(intent, PICKUP_AUTOCOMPLETE_REQUEST_CODE);
            }
        });

        tv_dropLocation.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
//                        .setCountry("AU")
                        .setCountry("IN")
                        .build(getActivity());
                startActivityForResult(intent, DROPOFF_AUTOCOMPLETE_REQUEST_CODE);
            }
        });

        goBackToFirst();

        return rootView;
    }

    private void callClearData()
    {
        Log.e(TAG, "callClearData()");
        tv_pickupLocation.setText("");
        tv_dropLocation.setText("");
        DropOffLoc_Address = "";
        PickUpLoc_Address = "";
    }

    private void CallMethodGetEstimatedFare()
    {
        getEstimateFare="";
        eta="";
        if (!PickUpLoc_Address.isEmpty() && !DropOffLoc_Address.isEmpty() && selectedModelId!=null && !selectedModelId.equalsIgnoreCase(""))
        {
            et_fareAmount.setEnabled(true);
            dialogClass.showDialog();
            Map<String, Object> params = new HashMap<String, Object>();

            String url = WebServiceAPI.WEB_SERVICE_GET_ESTIMATE_FARE;

            params.put(WebServiceAPI.PARAM_GET_ESTIMATE_FARE_PICKUP_LOCATION, PickUpLoc_Address);
            params.put(WebServiceAPI.PARAM_GET_ESTIMATE_FARE_DROPOFF_LOCATION, DropOffLoc_Address);
            params.put(WebServiceAPI.PARAM_GET_ESTIMATE_FARE_MODEL_ID, selectedModelId);

            Log.e(TAG, "CallMethodGetEstimatedFare Url:- " + url);
            Log.e(TAG, "CallMethodGetEstimatedFare parms:- " + params);

            aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {

                    try {
                        int responseCode = status.getCode();
                        Log.e("responseCode", "CallMethodGetEstimatedFare = " + responseCode);
                        Log.e("Response", "CallMethodGetEstimatedFare = " + json);

                        if (json != null)
                        {
                            if (json.has("status"))
                            {
                                if (json.getBoolean("status"))
                                {
                                    Log.e("status", "true");
                                    dialogClass.hideDialog();
                                    getEstimateFare="";
                                    if (json.has("estimate_fare"))
                                    {
                                        JSONObject estimate_fareObj = json.getJSONObject("estimate_fare");
                                        if (estimate_fareObj!=null)
                                        {
                                            if (estimate_fareObj.has("total"))
                                            {
                                                getEstimateFare = estimate_fareObj.getString("total");
                                                et_fareAmount.setText(getEstimateFare);
                                            }
                                            else
                                            {
                                                getEstimateFare="";
                                            }

                                            ////tv_eta, ll_eta

                                            if (bookClickType.equalsIgnoreCase(BOOK_NOW))
                                            {
                                                ll_eta.setBackgroundColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorDarkGray_transparent));
                                                tv_eta.setVisibility(View.VISIBLE);

                                                if (estimate_fareObj.has("eta"))
                                                {
                                                    eta = estimate_fareObj.getString("eta");
                                                    if (eta!=null && !eta.equalsIgnoreCase("") && !eta.equalsIgnoreCase("0"))
                                                    {
                                                        ll_eta.setBackgroundColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorDarkGray_transparent));
                                                        tv_eta.setVisibility(View.VISIBLE);
                                                        tv_eta.setText("eta : "+eta+" min");
                                                    }
                                                    else
                                                    {
                                                        eta="";
                                                        tv_eta.setVisibility(View.GONE);
                                                        ll_eta.setBackgroundColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorTransparent));

                                                        new SnackbarUtils(main_layout, "No driver available",
                                                                ContextCompat.getColor(Drawer_Activity.activity, R.color.snakbar_color),
                                                                ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color),
                                                                ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color)).snackieBar().show();
                                                    }
                                                }
                                                else
                                                {
                                                    eta="";
                                                    tv_eta.setVisibility(View.GONE);
                                                    ll_eta.setBackgroundColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorTransparent));

                                                    new SnackbarUtils(main_layout, "No driver available",
                                                            ContextCompat.getColor(Drawer_Activity.activity, R.color.snakbar_color),
                                                            ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color),
                                                            ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color)).snackieBar().show();
                                                }
                                            }
                                            else
                                            {
                                                tv_eta.setVisibility(View.GONE);
                                                ll_eta.setBackgroundColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorTransparent));
                                            }
                                        }
                                        else
                                        {
                                            Log.e("estimate_fareObj", "null");

                                            getEstimateFare="";
                                            eta="";
                                            tv_eta.setVisibility(View.GONE);
                                            ll_eta.setBackgroundColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorTransparent));

                                            dialogClass.hideDialog();
                                            if (json.has("message")) {
                                                new SnackbarUtils(main_layout, json.getString("message"),
                                                        ContextCompat.getColor(Drawer_Activity.activity, R.color.snakbar_color),
                                                        ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color),
                                                        ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color)).snackieBar().show();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Log.e("estimate_fare", "no estimate_fare");

                                        getEstimateFare="";
                                        getEstimateFare="";
                                        eta="";
                                        tv_eta.setVisibility(View.GONE);
                                        ll_eta.setBackgroundColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorTransparent));

                                        dialogClass.hideDialog();
                                        if (json.has("message")) {
                                            new SnackbarUtils(main_layout, json.getString("message"),
                                                    ContextCompat.getColor(Drawer_Activity.activity, R.color.snakbar_color),
                                                    ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color),
                                                    ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color)).snackieBar().show();
                                        }
                                    }
                                }
                                else
                                {
                                    Log.e("status", "false");

                                    getEstimateFare="";
                                    getEstimateFare="";
                                    eta="";
                                    tv_eta.setVisibility(View.GONE);
                                    ll_eta.setBackgroundColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorTransparent));

                                    dialogClass.hideDialog();
                                    if (json.has("message")) {
                                        new SnackbarUtils(main_layout, json.getString("message"),
                                                ContextCompat.getColor(Drawer_Activity.activity, R.color.snakbar_color),
                                                ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color),
                                                ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color)).snackieBar().show();
                                    }
                                }
                            } else {
                                Log.e("json", "no status found");

                                getEstimateFare="";
                                getEstimateFare="";
                                eta="";
                                tv_eta.setVisibility(View.GONE);
                                ll_eta.setBackgroundColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorTransparent));

                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(Drawer_Activity.activity, R.color.snakbar_color),
                                            ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color),
                                            ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        } else {
                            Log.e("json", "null");

                            getEstimateFare="";
                            getEstimateFare="";
                            eta="";
                            tv_eta.setVisibility(View.GONE);
                            ll_eta.setBackgroundColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorTransparent));

                            dialogClass.hideDialog();
                            new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                    ContextCompat.getColor(Drawer_Activity.activity, R.color.snakbar_color),
                                    ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color),
                                    ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color)).snackieBar().show();
                        }
                    } catch (Exception e) {
                        Log.e("CallMethodGetEstimated", "Exception : " + e.toString());

                        getEstimateFare="";
                        getEstimateFare="";
                        eta="";
                        tv_eta.setVisibility(View.GONE);
                        ll_eta.setBackgroundColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorTransparent));

                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                ContextCompat.getColor(Drawer_Activity.activity, R.color.snakbar_color),
                                ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color)).snackieBar().show();
                    }
                }
            }.header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
        }
    }

    public void goBackToFirst()
    {
        et_customerName.setText("");
        et_mobNumber.setText("");
//        et_customerEmail.setText("");
        et_fareAmount.setText("");
        tv_pickupLocation.setText("");
        tv_dropLocation.setText("");
        tv_date_picker.setText("");
        et_passengerFlightNo.setText("");
        et_passengerNote.setText("");
        DateBookLater="";

        tv_dollar.setVisibility(View.GONE);
        ll_fill_data.setVisibility(View.GONE);
        ll_twoButton_dispatch.setVisibility(View.VISIBLE);

        tv_eta.setVisibility(View.GONE);
        ll_eta.setBackgroundColor(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorTransparent));
    }

    private void CheckData(String driverId)
    {
        if (TextUtils.isEmpty(et_customerName.getText().toString()))
        {
            //getString(R.string.please_enter_customerName)
            new SnackbarUtils(main_layout, "whoops, you have missed a field, please complete and then try again",
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else if (TextUtils.isEmpty(et_mobNumber.getText().toString()))
        {
            //getString(R.string.please_enter_customerMobileNo)
            new SnackbarUtils(main_layout, "whoops, you have missed a field, please complete and then try again",
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
       /* else if (TextUtils.isEmpty(et_customerEmail.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_customerMobileNo),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }*/
        else if (TextUtils.isEmpty(PickUpLoc_Address))
        {
            //getString(R.string.please_enter_pickup_loc)
            new SnackbarUtils(main_layout, "whoops, you have missed a field, please complete and then try again",
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else if (TextUtils.isEmpty(DropOffLoc_Address))
        {
            //getString(R.string.please_enter_dropoff_loc)
            new SnackbarUtils(main_layout, "whoops, you have missed a field, please complete and then try again",
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else if (TextUtils.isEmpty(selectedModelId))
        {
            //getString(R.string.please_select_model_type)
            new SnackbarUtils(main_layout, "whoops, you have missed a field, please complete and then try again",
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else if (TextUtils.isEmpty(et_fareAmount.getText().toString()))
        {
            //getString(R.string.please_enter_fare_amount)
            new SnackbarUtils(main_layout, "whoops, you have missed a field, please complete and then try again",
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else if (Double.parseDouble(et_fareAmount.getText().toString())<Double.parseDouble(getEstimateFare))
        {
            new SnackbarUtils(main_layout, "You can not enter amount for this trip below $"+getEstimateFare+".",
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else if (bookClickType.equalsIgnoreCase(BOOK_LATER))
        {
            if (TextUtils.isEmpty(tv_date_picker.getText().toString()))
            {
                //getString(R.string.please_select_date_and_time)
                new SnackbarUtils(main_layout, "whoops, you have missed a field, please complete and then try again",
                        ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
            }
            else
            {
                if (ConnectivityReceiver.isConnected())
                {
                    callApiForDispatchJob(driverId);
                }
                else
                {
                    new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }
        else
        {
            if (ConnectivityReceiver.isConnected())
            {
                callApiForDispatchJob(driverId);
            }
            else
            {
                new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                        ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
            }
        }
    }

    private void callApiForDispatchJob(String driverId)
    {
        checked =0;
        list_vehicleItem.clear();
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();
        String url;
        if (bookClickType.equalsIgnoreCase(BOOK_NOW))
        {
            url = WebServiceAPI.WEB_SERVICE_DISPATCH_JOB_BOOK_NOW;
        }
        else
        {
            url = WebServiceAPI.WEB_SERVICE_DISPATCH_JOB_BOOK_LATER;
            params.put(WebServiceAPI.DISPATCH_JOB_BOOK_LATER_PARAM_PICK_UP_DATE_TIME, DateBookLater);
        }

        params.put(WebServiceAPI.DISPATCH_JOB_BOOK_NOW_PARAM_DRIVER_ID, driverId);
        params.put(WebServiceAPI.DISPATCH_JOB_BOOK_NOW_PARAM_MODEL_ID, selectedModelId);
        params.put(WebServiceAPI.DISPATCH_JOB_BOOK_NOW_PARAM_PICK_UP_LOC, PickUpLoc_Address);
        params.put(WebServiceAPI.DISPATCH_JOB_BOOK_NOW_PARAM_DROP_OFF_LOC, DropOffLoc_Address);
        params.put(WebServiceAPI.DISPATCH_JOB_BOOK_NOW_PARAM_PASSENGER_NAME, et_customerName.getText().toString());
        params.put(WebServiceAPI.DISPATCH_JOB_BOOK_NOW_PARAM_PASSENGER_CONTACT_NO, et_mobNumber.getText().toString());
//        params.put(WebServiceAPI.DISPATCH_JOB_BOOK_NOW_PARAM_PASSENGER_EMAIL, et_customerEmail.getText().toString());
        params.put(WebServiceAPI.DISPATCH_JOB_BOOK_NOW_PARAM_FARE_AMOUNT, et_fareAmount.getText().toString());
        params.put(WebServiceAPI.DISPATCH_JOB_BOOK_NOW_PARAM_PAYMENT_TYPE, amountClickType);

        if (!TextUtils.isEmpty(et_passengerFlightNo.getText().toString()))
        {
            params.put(WebServiceAPI.DISPATCH_JOB_BOOK_NOW_PARAM_FLIGHT_NO, et_passengerFlightNo.getText().toString());
        }
        if (!TextUtils.isEmpty(et_passengerNote.getText().toString()))
        {
            params.put(WebServiceAPI.DISPATCH_JOB_BOOK_NOW_PARAM_NOTE, et_passengerNote.getText().toString());
        }


        Log.e("url", "FindWhichTypeIs_Visible = " + url);
        Log.e("param", "FindWhichTypeIs_Visible = " + params);


        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "FindWhichTypeIs_Visible = " + responseCode);
                    Log.e("Response", "FindWhichTypeIs_Visible = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                                }
                                else
                                {
                                    new SnackbarUtils(main_layout, getActivity().getResources().getString(R.string.dispatch_job_successfully),
                                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                                }
                                et_customerName.setText("");
                                et_mobNumber.setText("");
                                et_fareAmount.setText("");
                                tv_pickupLocation.setText("");
                                tv_dropLocation.setText("");
                                tv_date_picker.setText("");
                                et_passengerFlightNo.setText("");
                                et_passengerNote.setText("");

                                DateBookLater="";

                                ll_fill_data.setVisibility(View.GONE);
                                ll_twoButton_dispatch.setVisibility(View.VISIBLE);
                                ll_twoButton_dispatch.setAnimation(slide_right);
                                slide_right.start();
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message")) {
                                new SnackbarUtils(main_layout, json.getString("message"),
                                        ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                    }
                } catch (Exception e) {
                    Log.e("FindWhichTypeIs_Visible", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }.header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }

    public class paserdata extends AsyncTask<Void, Integer, Void> {

        AutoCompleteTextView autoCompleteTextView;

        public paserdata(AutoCompleteTextView autoCompleteTextView)
        {
            this.autoCompleteTextView = autoCompleteTextView;
        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub
            try {
                json = GetAddress(jsonurl.toString());
                if (json != null) {
                    names.clear();
                    Place_id_type.clear();
                    // Getting Array of Contacts
                    contacts = json.getJSONArray(TAG_RESULT);
                    for (int i = 0; i < contacts.length(); i++) {
                        JSONObject c = contacts.getJSONObject(i);
                        String description = c.getString("description");
                        String plc_id = c.getString("place_id");
                        Log.d("description", description);
                        names.add(description);
                        Place_id_type.add(plc_id);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.e("names", "" + names);
            try {
                ArrayAdapter<String> adp;
                adp = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, names) {
                    @Override
                    public View getView(final int position, View convertView, ViewGroup parent) {
                        View view = super.getView(position, convertView, parent);
                        final TextView text = (TextView) view.findViewById(android.R.id.text1);
                        text.setTextColor(getResources().getColor(R.color.colorBlack));
                        text.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // TODO Auto-generated method stub
                                if (Place_id_type.size() > 0) {
                                    place_id = Place_id_type.get(position);
                                    System.out.println("Place_id_type" + Place_id_type.get(position));
                                }

                                autoCompleteTextView.setText(text.getText().toString());
                                if (Pick_Drop_ClickType.equalsIgnoreCase(PICK_UP))
                                {
                                    PickUpLoc_Address = text.getText().toString();
                                    Log.e("Address", "PickUpLoc_Address : "+PickUpLoc_Address);
                                }
                                else
                                {
                                    DropOffLoc_Address = text.getText().toString();
                                    Log.e("Address", "DropOffLoc_Address : "+DropOffLoc_Address);
                                }
                                CallMethodGetEstimatedFare();


                                getLocationFromAddress(text.getText().toString(), autoCompleteTextView);
                                autoCompleteTextView.dismissDropDown();
                            }
                        });
                        return view;
                    }
                };
                autoCompleteTextView.setAdapter(adp);
                adp.notifyDataSetChanged();
            } catch (Exception e) {
                // TODO: handle exception
            }
        }
    }

    // Get the google API result and convert into JSON format.
    private JSONObject GetAddress(String Url) {
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httppost = new HttpGet(Url);
            HttpResponse response = httpclient.execute(httppost);
            String jsonResult = Utility.inputStreamToString(response.getEntity().getContent()).toString();
            JSONObject json = new JSONObject(jsonResult);
            return json;
        } catch (Exception e) {
            // TODO: handle exception
        }
        return null;
    }

    // Get the lat and lng from given address
    public void getLocationFromAddress(String strAddress, AutoCompleteTextView autoCompleteTextView) {
        Geocoder coder = new Geocoder(getActivity());
        List<Address> address;
        Address location = null;
        try {
            address = coder.getFromLocationName(strAddress, 5);

            address = coder.getFromLocationName(strAddress, 5);
            if (!address.isEmpty()) {
                location = address.get(0);
                location.getLatitude();
                location.getLongitude();
            }

            if (!address.isEmpty()) {
                Constants.newgpsLatitude_pickUp = location.getLatitude();
                Constants.newgpsLongitude_pickUp = location.getLongitude();
                if (autoCompleteTextView.getText().toString().trim().length() > 0) {
//                    dropdownlist_istouch = false;
                    Constants.address_pickUp = autoCompleteTextView.getText().toString();
                }
            } else {
                if (autoCompleteTextView.getText().toString().trim().length() > 0) {
                    Constants.address_pickUp = autoCompleteTextView.getText().toString();
                    String encode_url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + place_id + "&key="+getResources().getString(R.string.api_key_google_map);

                    System.out.println("encode_url" + encode_url);
                    new GetGeoCoderAddress(encode_url, autoCompleteTextView).execute();
                }
            }
        } catch (IndexOutOfBoundsException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            // TODO: handle exception
            Constants.newgpsLatitude_pickUp = 0.0;
            Constants.newgpsLongitude_pickUp = 0.0;
            Constants.address_pickUp = "";
        }
    }

    public class GetGeoCoderAddress extends AsyncTask<String, Void, Void> {
        private final String Urlcoreconfig;
        String lat, lng;
        private String jsonResult;
        AutoCompleteTextView autoCompleteTextView;

        public GetGeoCoderAddress(String url, AutoCompleteTextView autoCompleteTextView) {
            this.Urlcoreconfig = url;
            this.autoCompleteTextView= autoCompleteTextView;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpGet httppost = new HttpGet(Urlcoreconfig);
                HttpResponse response = httpclient.execute(httppost);
                jsonResult = inputStreamToString(response.getEntity().getContent()).toString();
                System.out.println("sureshhhhhhhhh " + jsonResult);
                JSONObject json = new JSONObject(jsonResult);
                System.out.println("ssssssssssss " + json);
                json = json.getJSONObject("result");
                json = json.getJSONObject("geometry");
                json = json.getJSONObject("location");
                lat = json.getString("lat");
                lng = json.getString("lng");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (lat!=null && !lat.equalsIgnoreCase(""))
            {
                Constants.newgpsLatitude_pickUp = Double.valueOf(lat);
                Constants.newgpsLongitude_pickUp = Double.valueOf(lng);
                Constants.address_pickUp = autoCompleteTextView.getText().toString();
                System.out.println("sureshhhhhhhhh " +Constants.address_pickUp + Constants.newgpsLatitude_pickUp + " , " + Constants.newgpsLongitude_pickUp);
            }
            else
            {
                Constants.newgpsLatitude_pickUp = 0.0;
                Constants.newgpsLongitude_pickUp = 0.0;
                Constants.address_pickUp = "";
            }
        }
    }

    public StringBuilder inputStreamToString(InputStream is) {
        String rLine = "";
        StringBuilder answer = new StringBuilder();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        try {
            while ((rLine = rd.readLine()) != null) {
                answer.append(rLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return answer;
    }


    private void FindWhichTypeIs_Visible()
    {
        checked =0;
        list_vehicleItem.clear();
        if(!dialogClass.isShowingDialog()) {
            dialogClass.showDialog();
        }
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_CAR_CLASS+SessionSave.getUserSession(Comman.USER_CITY_ID, getActivity());

        Log.e("url", "FindWhichTypeIs_Visible = " + url);
        Log.e("param", "FindWhichTypeIs_Visible = " + params);


        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "FindWhichTypeIs_Visible = " + responseCode);
                    Log.e("Response", "FindWhichTypeIs_Visible = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");
                                String Id ="", Name="", Description="", Status="0";

                                if (json.has("cars_and_taxi"))
                                {
                                    list_vehicleItem.clear();
                                    String cars_and_taxi = json.getString("cars_and_taxi");
                                    if (cars_and_taxi!=null && !cars_and_taxi.equalsIgnoreCase(""))
                                    {
                                        JSONArray arrayCarTaxi = json.getJSONArray("cars_and_taxi");
                                        if(arrayCarTaxi.length() == 0){
                                            SessionSave.saveUserSession(Comman.USER_CITY_ID,"4", getActivity());
                                            FindWhichTypeIs_Visible();
                                        }
                                        for (int i= 0; i<arrayCarTaxi.length(); i++)
                                        {
                                            JSONObject objectCarTexi = arrayCarTaxi.getJSONObject(i);
                                            if (objectCarTexi.has("Id"))
                                            {
                                                Id = objectCarTexi.getString("Id");
                                            }

                                            if (objectCarTexi.has("Name"))
                                            {
                                                Name = objectCarTexi.getString("Name");
                                            }

                                            if (objectCarTexi.has("Description"))
                                            {
                                                Description = objectCarTexi.getString("Description");
                                            }
                                            Status="0";

                                            list_vehicleItem.add(new VehicleType_Been(Id, Name, Description, Status));
                                            for (int l=0; l<list_deviceModelId.size(); l++)
                                            {
                                                Log.e("list_deviceModelId","list_deviceModelId22 : "+list_deviceModelId.get(l));
                                                if (list_deviceModelId.get(l).equalsIgnoreCase(Id))
                                                {
                                                    checked=1;
                                                }
                                            }
                                            if (checked==1)
                                            {
                                                adapter.notifyDataSetChanged();
                                            }
                                            else
                                            {
                                                checked =0;
                                            }
                                        }
                                        if (checked==1)
                                        {
                                            adapter.notifyDataSetChanged();
                                        }
                                        Log.e("list size", "list sizeeeeeeeeeeeee :"+list_vehicleItem.size());
                                    }
                                    else
                                    {
                                        Log.e("cars_and_taxi", "null");
                                        checked =0;
                                    }
                                }

                                if (checked!=1)
                                {
                                    checked =0;
                                    if (json.has("delivery_services"))
                                    {
                                        list_vehicleItem.clear();
                                        String delivery_services = json.getString("delivery_services");
                                        if (delivery_services!=null && !delivery_services.equalsIgnoreCase(""))
                                        {
                                            JSONArray arrayDeliveryServices = json.getJSONArray("delivery_services");
                                            for (int i= 0; i<arrayDeliveryServices.length(); i++)
                                            {
                                                JSONObject objectDeliveryService = arrayDeliveryServices.getJSONObject(i);
                                                if (objectDeliveryService.has("Id"))
                                                {
                                                    Id = objectDeliveryService.getString("Id");
                                                }

                                                if (objectDeliveryService.has("Name"))
                                                {
                                                    Name = objectDeliveryService.getString("Name");
                                                }

                                                if (objectDeliveryService.has("Description"))
                                                {
                                                    Description = objectDeliveryService.getString("Description");
                                                }
                                                Status="0";

                                                list_vehicleItem.add(new VehicleType_Been(Id, Name, Description, Status));

                                                for (int l=0; l<list_deviceModelId.size(); l++)
                                                {
                                                    Log.e("list_deviceModelId","list_deviceModelId22 : "+list_deviceModelId.get(l));
                                                    if (list_deviceModelId.get(l).equalsIgnoreCase(Id))
                                                    {
                                                        checked=1;
                                                    }
                                                }
                                            }
                                            if (checked==1)
                                            {
                                                adapter.notifyDataSetChanged();
                                            }
                                            else
                                            {
                                                checked =0;
                                            }
                                        }
                                        else
                                        {
                                            Log.e("cars_and_taxi", "null");
                                        }
                                    }
                                }
                                dialogClass.hideDialog();
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message")) {
                                new SnackbarUtils(main_layout, json.getString("message"),
                                        ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                    }
                } catch (Exception e) {
                    Log.e("FindWhichTypeIs_Visible", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }.header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK)
        {
            if (requestCode == PICKUP_AUTOCOMPLETE_REQUEST_CODE )
            {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.e(TAG, "onActivityResult tv_pickupLocation:- " + place.getName() + ", " + place.getAddress());
                tv_pickupLocation.setText(place.getName() + "," + place.getAddress());

                PickUpLoc_Address = place.getName() + "," + place.getAddress();
                Log.e(TAG, "onActivityResult PickUpLoc_Address:- " + PickUpLoc_Address);
            }
            else if (requestCode == DROPOFF_AUTOCOMPLETE_REQUEST_CODE)
            {

                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.e(TAG, "onActivityResult tv_dropLocation:- " + place.getName() + ", " + place.getAddress());
                tv_dropLocation.setText(place.getName() + "," + place.getAddress());

                DropOffLoc_Address = place.getName() + "," + place.getAddress();
                Log.e(TAG, "onActivityResult DropOffLoc_Address:- " + DropOffLoc_Address);
            }

            CallMethodGetEstimatedFare();

//            if (!DropOffLoc_Address.equalsIgnoreCase("") &&
//                    !PickUpLoc_Address.equalsIgnoreCase(""))
//            {
//                CallMethodGetEstimatedFare();
//            }
        }
    }
}
