package iiride.app.driver.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.R;


import com.squareup.picasso.Picasso;


public class Wallet_Transfer_ReceiveMoney_Fragment extends Fragment {


    private ImageView iv_qr_code;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_receive_transfer_wallet, container, false);

        iv_qr_code = (ImageView) rootView.findViewById(R.id.iv_qr_code);

        String QRCode = SessionSave.getUserSession(Comman.USER_QR_CODE, getActivity());
        if(QRCode!=null && !QRCode.equalsIgnoreCase(""))
        {
            Log.e("GET QRCODE","QRCode:- " + QRCode);
            Picasso.with(getActivity()).load(QRCode).into(iv_qr_code);
        }
        return rootView;
    }
}