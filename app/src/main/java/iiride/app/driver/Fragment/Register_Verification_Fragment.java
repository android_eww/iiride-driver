package iiride.app.driver.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import iiride.app.driver.Activity.Registration_Tab_Activity;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;


import iiride.app.driver.R;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;




public class Register_Verification_Fragment extends Fragment {

    TextView tv_next;
    EditText et_Otp;

    LinearLayout main_layout;

    AQuery aQuery;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_verification_register, container, false);

        aQuery = new AQuery(getActivity());
        main_layout = (LinearLayout) rootView.findViewById(R.id.main_layout);


        Registration_Tab_Activity registration_tab_activity = new Registration_Tab_Activity();
        registration_tab_activity.LogoutButton_Display(1);

        tv_next = (TextView) rootView.findViewById(R.id.tv_next);

        et_Otp =(EditText) rootView.findViewById(R.id.et_Otp);

//        if (SessionSave.getUserSession(Comman.USER_ONE_TIME_OTP,getActivity())!= null && !SessionSave.getUserSession(Comman.USER_ONE_TIME_OTP,getActivity()).equalsIgnoreCase(""))
//        {
//            et_Otp.setText(SessionSave.getUserSession(Comman.USER_ONE_TIME_OTP,getActivity()));
//        }


        tv_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(et_Otp.getText().toString()))
                {
                    new SnackbarUtils(main_layout, getString(R.string.please_enter_otp_number),
                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                }
                else
                {
                    OTPVerification();
                }

            }
        });


        return rootView;
    }


    private void OTPVerification()
    {
        String currentOtp = SessionSave.getUserSession(Comman.USER_ONE_TIME_OTP,getActivity());
        if (currentOtp!= null && !currentOtp.equalsIgnoreCase(""))
        {
            if (currentOtp.equalsIgnoreCase(et_Otp.getText().toString().trim()))
            {
                SessionSave.saveUserSession(Comman.REGISTER_OTP_ACTIVITY, "1",getActivity());
                ((Registration_Tab_Activity)getActivity()).setTabAddProfileDetails();
            }
            else
            {
                new SnackbarUtils(main_layout, getString(R.string.otp_is_not_same),
                        ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
            }
        }
        else
        {
            new SnackbarUtils(main_layout, getString(R.string.please_try_to_login_again),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
    }
}