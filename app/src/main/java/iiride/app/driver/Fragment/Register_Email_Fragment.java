package iiride.app.driver.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import iiride.app.driver.Activity.Main_Activity;
import iiride.app.driver.Activity.Registration_Activity;
import iiride.app.driver.Activity.Registration_Tab_Activity;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;


import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.hbb20.CountryCodePicker;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Register_Email_Fragment extends Fragment {

    TextView tv_next;
    EditText et_email_register,et_mobile_register;
    LinearLayout main_layout;

    DialogClass dialogClass;
    AQuery aQuery;
    private CountryCodePicker codePicker;

    JSONArray jsonArray_CompanyId = new JSONArray();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_email_register, container, false);

        aQuery = new AQuery(getActivity());

        main_layout = (LinearLayout) rootView.findViewById(R.id.main_layout);

        tv_next = (TextView) rootView.findViewById(R.id.tv_next);

        et_email_register = (EditText) rootView.findViewById(R.id.et_email_register);
        et_mobile_register = (EditText) rootView.findViewById(R.id.et_mobile_register);
        codePicker = rootView.findViewById(R.id.countryCodePicker);

        Registration_Tab_Activity registration_tab_activity = new Registration_Tab_Activity();
        registration_tab_activity.LogoutButton_Display(0);


        if (SessionSave.getUserSession(Comman.USER_EMAIL,getActivity())!= null && !SessionSave.getUserSession(Comman.USER_EMAIL,getActivity()).equalsIgnoreCase(""))
        {
            et_email_register.setText(SessionSave.getUserSession(Comman.USER_EMAIL,getActivity()));
        }
        if (SessionSave.getUserSession(Comman.USER_MOBILE_NUMBER,getActivity())!= null && !SessionSave.getUserSession(Comman.USER_MOBILE_NUMBER,getActivity()).equalsIgnoreCase(""))
        {
            et_mobile_register.setText(SessionSave.getUserSession(Comman.USER_MOBILE_NUMBER,getActivity()));
        }

        tv_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(et_email_register.getText().toString()))
                {
                    new SnackbarUtils(main_layout, getString(R.string.please_enter_email),
                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                }
                else if(!isEmailValid(et_email_register.getText().toString()))
                {
                    new SnackbarUtils(main_layout, getString(R.string.please_enter_valid_email),
                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                }
                else  if (TextUtils.isEmpty(et_mobile_register.getText().toString()))
                {
                    new SnackbarUtils(main_layout, getString(R.string.please_enter_mobile_number),
                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                }
                else if(codePicker.getSelectedCountryCodeWithPlus().equalsIgnoreCase("+61") && et_mobile_register.getText().toString().length() != 9)
                {
                    new SnackbarUtils(main_layout, getString(R.string.please_enter_valid_mobile_number),
                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                }
                else if(codePicker.getSelectedCountryCodeWithPlus().equalsIgnoreCase("+1") && et_mobile_register.getText().toString().length() != 10)
                {
                    new SnackbarUtils(main_layout, getString(R.string.please_enter_valid_mobile_number),
                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                }
                else
                {
                    EmailVerification();
                }
            }
        });

        initializeCountry();

        return rootView;
    }

    private void initializeCountry()
    {
        try
        {
            if (SessionSave.getInitDataString(getActivity())!=null && !SessionSave.getInitDataString(getActivity()).equalsIgnoreCase(""))
            {
                JSONObject jsonObject = new JSONObject(SessionSave.getInitDataString(getActivity()));

                if (jsonObject!=null && jsonObject.has("countries"))
                {
                    JSONArray jsonArray = jsonObject.getJSONArray("countries");

                    if (jsonArray!=null && jsonArray.length()>0)
                    {
                        String country = null;
                        String countryNameCode = "AU";
                        for (int i=0; i<jsonArray.length(); i++)
                        {
                            JSONObject countryObject = jsonArray.getJSONObject(i);

                            if (country==null)
                            {
                                if (countryObject!=null && countryObject.has("CountryCode"))
                                {
                                    if (countryObject.getString("CountryCode")!=null && countryObject.getString("CountryCode").equalsIgnoreCase("AU"))
                                    {
                                        Log.e("pos::", i + "");
                                        country = "AU";
                                        countryNameCode = "AU";

                                    }
                                }

                                if (countryObject!=null && countryObject.has("CountryCode"))
                                {
                                    if (countryObject.getString("CountryCode")!=null && countryObject.getString("CountryCode").equalsIgnoreCase("US"))
                                    {
                                        Log.e("pos::", i + "");
                                        country = "US";
                                        countryNameCode = "US";
                                    }
                                }
                            }
                            else
                            {
                                if (countryObject!=null && countryObject.has("CountryCode"))
                                {
                                    if (countryObject.getString("CountryCode")!=null && countryObject.getString("CountryCode").equalsIgnoreCase("AU"))
                                    {
                                        Log.e("pos::", i + "");
                                        country += ",AU";

                                    }
                                }

                                if (countryObject!=null && countryObject.has("CountryCode"))
                                {
                                    if (countryObject.getString("CountryCode")!=null && countryObject.getString("CountryCode").equalsIgnoreCase("US"))
                                    {
                                        Log.e("pos::", i + "");
                                        country += ",US";
                                    }
                                }
                            }
                        }

                        if (country != null)
                            codePicker.setCustomMasterCountries(country);
                        else
                            codePicker.setCustomMasterCountries("");

                        codePicker.setCountryForNameCode(countryNameCode);
                    }
                    else
                    {
                        /*codePicker.setCustomMasterCountries("AU,US");
                        codePicker.setCountryForNameCode("AU,US");*/
                    }
                }
                else
                {
                    /*codePicker.setCustomMasterCountries("AU,US");
                    codePicker.setCountryForNameCode("AU,US");*/
                }
            }
            else
            {
                /*codePicker.setCustomMasterCountries("AU,US");
                codePicker.setCountryForNameCode("AU,US");*/
            }

        }
        catch (Exception e)
        {
            Log.e("call","country parse exception : "+e.getMessage());
            /*codePicker.setCustomMasterCountries("AU,US");
            codePicker.setCountryForNameCode("AU,US");*/
        }
    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private void EmailVerification()
    {
        dialogClass = new DialogClass(getActivity(), 1);
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_OTP_FOR_REGISTER;

        params.put(WebServiceAPI.OTP_FOR_REGISTER_PARAM_EMAIL, et_email_register.getText().toString().trim());
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_MOBILE_NUMBER, codePicker.getSelectedCountryCodeWithPlus()+et_mobile_register.getText().toString().trim());


        Log.e("url", "UserSignIn = " + url);
        Log.e("param", "UserSignIn = " + params);


        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "UserSignIn = " + responseCode);
                    Log.e("Response", "UserSignIn = " + json);

                    /// {"status":true,"otp":378048,"message":"Mobile verification coede has sent on your email address"}

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                if (json.has("company"))
                                {
                                    String  CompanyId="", CompanyName="", Address="", City="", State="", Country="";
                                    jsonArray_CompanyId = new JSONArray();

                                    JSONArray arrayCompany = json.getJSONArray("company");

                                    for (int i=0; i<arrayCompany.length() ; i++)
                                    {
                                        JSONObject objectCompany = arrayCompany.getJSONObject(i);
                                        if (objectCompany.has("Id"))
                                        {
                                            CompanyId = objectCompany.getString("Id");
                                        }
                                        if (objectCompany.has("CompanyName"))
                                        {
                                            CompanyName = objectCompany.getString("CompanyName");
                                        }
                                        if (objectCompany.has("City"))
                                        {
                                            City = objectCompany.getString("City");
                                        }
                                        if (objectCompany.has("State"))
                                        {
                                            State = objectCompany.getString("State");
                                        }
                                        if (objectCompany.has("Country"))
                                        {
                                            Country = objectCompany.getString("Country");
                                        }
                                        JSONObject companyIdObject = new JSONObject();
                                        companyIdObject.put(Comman.USER_COMPANY_ID_FOR_STORE, CompanyId);
                                        companyIdObject.put(Comman.USER_COMPANY_NAME_FOR_STORE, CompanyName);
                                        companyIdObject.put(Comman.USER_COMPANY_CITY_FOR_STORE, City);
                                        companyIdObject.put(Comman.USER_COMPANY_STATE_FOR_STORE, State);
                                        companyIdObject.put(Comman.USER_COMPANY_COUNTRY_FOR_STORE, Country);
                                        jsonArray_CompanyId.put(companyIdObject);
                                    }

                                    JSONObject studentsObj = new JSONObject();
                                    studentsObj.put(Comman.USER_COMPANY_ARRAY_NAME_FOR_STORE, jsonArray_CompanyId);

                                    String jsonCompanyIdStr = studentsObj.toString();
                                    SessionSave.saveUserSession(Comman.USER_COMPANY_ID_ARRAY, jsonCompanyIdStr, getActivity());

                                    Log.e("jsonArray_CompanyId", "jsonArray_CompanyIdj222222222222222 :"+SessionSave.getUserSession(Comman.USER_COMPANY_ID_ARRAY,getActivity()));
                                }

                                SessionSave.saveUserSession(Comman.REGISTER_EMAIL_ACTIVITY,"1",getActivity());
                                if (Main_Activity.activity!=null)
                                {
                                    Main_Activity.activity.finish();
                                }
                                if (Registration_Activity.activity!=null)
                                {
                                    Registration_Activity.activity.finish();
                                }

                                Log.e("status", "true");
                                if (json.has("otp"))
                                {
                                    SessionSave.saveUserSession(Comman.USER_EMAIL, et_email_register.getText().toString(),getActivity());
                                    SessionSave.saveUserSession(Comman.USER_MOBILE_NUMBER, et_mobile_register.getText().toString(),getActivity());
                                    SessionSave.saveUserSession(Comman.USER_COUNTRY_CODE, codePicker.getSelectedCountryCodeWithPlus()+"",getActivity());
                                    SessionSave.saveUserSession(Comman.USER_ONE_TIME_OTP,json.getString("otp"),getActivity());

                                    Log.e("SessionSave.","et_email_register ; "+SessionSave.getUserSession(Comman.USER_EMAIL,getActivity()));
                                    Log.e("SessionSave.","otp ; "+SessionSave.getUserSession(Comman.USER_ONE_TIME_OTP,getActivity()));

                                    ((Registration_Tab_Activity)getActivity()).setTabVarification();

                                    dialogClass.hideDialog();
                                }
                                else
                                {
                                    Log.e("otp", "not Find");
                                    dialogClass.hideDialog();
                                    if (json.has("message")) {
                                        new SnackbarUtils(main_layout, json.getString("message"),
                                                ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                                    }
                                }
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message")) {
                                new SnackbarUtils(main_layout, json.getString("message"),
                                        ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                    }
                } catch (Exception e) {
                    Log.e("UserSignIn", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }.header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }
}
