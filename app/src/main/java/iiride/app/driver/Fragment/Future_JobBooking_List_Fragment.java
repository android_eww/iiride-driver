package iiride.app.driver.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import iiride.app.driver.Activity.Drawer_Activity;
import iiride.app.driver.Adapter.Future_Job_List_Adapter;
import iiride.app.driver.Been.Future_BookingList_Been;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;
import iiride.app.driver.Others.ConnectivityReceiver;


import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.hendraanggrian.widget.ExpandableRecyclerView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Future_JobBooking_List_Fragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    private ExpandableRecyclerView recyclerView;
    private ExpandableRecyclerView.Adapter mAdapter;
    private ExpandableRecyclerView.LayoutManager layoutManager;
    public static List<Future_BookingList_Been> list = new ArrayList<Future_BookingList_Been>();

    String TAG = "CallDispatched_List";
    private AQuery aQuery;
    DialogClass dialogClass;
    public SwipeRefreshLayout swipeRefreshLayout;
    int RunAct_FirstTime;
    TextView tv_NoDataFound;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_future_job_booking_list, container, false);

        aQuery = new AQuery(getActivity());

        ((Drawer_Activity)getActivity()).setTitleOfScreenToolbar("Future Booking");

        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);
        tv_NoDataFound = (TextView) rootView.findViewById(R.id.tv_NoDataFound);
        tv_NoDataFound.setVisibility(View.GONE);

        recyclerView = (ExpandableRecyclerView) rootView.findViewById(R.id.rv_future_job);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new Future_Job_List_Adapter((LinearLayoutManager) layoutManager);
        recyclerView.setAdapter(mAdapter);

        swipeRefreshLayout.setOnRefreshListener(this);

        String userId = SessionSave.getUserSession(Comman.USER_ID, getActivity());
        if (userId!=null && !userId.equalsIgnoreCase(""))
        {
            Log.e(TAG, "firstTime call");
            RunAct_FirstTime=0;
            if (ConnectivityReceiver.isConnected())
            {
                CallPendingJob_List(userId);
            }
            else
            {
                tv_NoDataFound.setVisibility(View.VISIBLE);
                new SnackbarUtils(Drawer_Activity.main_layout, getString(R.string.not_connected_to_internet),
                        ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
            }
        }
        return rootView;
    }

    @Override
    public void onRefresh()
    {
        String userId = SessionSave.getUserSession(Comman.USER_ID, getActivity());
        if (userId!=null && !userId.equalsIgnoreCase(""))
        {
            Log.e(TAG, "On refresh");
            if (ConnectivityReceiver.isConnected())
            {
                CallPendingJob_List(userId);
            }
            else
            {
                tv_NoDataFound.setVisibility(View.VISIBLE);
                new SnackbarUtils(Drawer_Activity.main_layout, getString(R.string.not_connected_to_internet),
                        ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
            }
        }
    }

    private void CallPendingJob_List(String userId)
    {
        tv_NoDataFound.setVisibility(View.GONE);
        list.clear();
        /*list.add(new Future_BookingList_Been("21", "12-23-1009", "abcd", "abiurvd", "1", "1", "1", "1",
                "arru", "9876543216", "business"));
        mAdapter.notifyDataSetChanged();*/
        dialogClass = new DialogClass(Drawer_Activity.activity, 1);
        if (RunAct_FirstTime==0)
        {
            dialogClass.showDialog();
            RunAct_FirstTime=1;
        }


        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_FUTURE_BOOKING_LIST + userId;

        Log.e(TAG, "URL = " + url);
        Log.e(TAG, "PARAMS = " + params);

        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>(){

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG, "responseCode = " + responseCode);
                    Log.e(TAG, "Response = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.d(TAG,"status:true");
                                if (json.has("dispath_job"))
                                {
                                    String dispath_job = json.getString("dispath_job");
                                    if (dispath_job !=null && !dispath_job.equalsIgnoreCase(""))
                                    {
                                        JSONArray dispatchJob_Array = json.getJSONArray("dispath_job");

                                        int CountList= 0;
                                        for (int i = 0; i < dispatchJob_Array.length(); i++)
                                        {
                                            JSONObject dispatchJobObj = dispatchJob_Array.getJSONObject(i);

                                            if (dispatchJobObj != null)
                                            {
                                                String TripId = "", PickupDateTime = "", PickupLocation = "", DropoffLocation = "", CompanyId = "", PassengerId = "", TripDistance = "", TripFare=""
                                                        , ModelId = "", PassengerName = "", PassengerContact = "", Model = "", FlightNumber="", Notes="", PaymentType="", DispatcherDriverInfo=""
                                                        , dispatherEmail="", dispatherFullname="", dispatherMobileNo="",DropoffSuburb="",PickupSuburb="",EstDriverEarning="";

                                                if (dispatchJobObj.has("Id")) {
                                                    TripId = dispatchJobObj.getString("Id");
                                                }
                                                if (dispatchJobObj.has("PassengerId")) {
                                                    PassengerId = dispatchJobObj.getString("PassengerId");
                                                }
                                                if (dispatchJobObj.has("ModelId")) {
                                                    ModelId = dispatchJobObj.getString("ModelId");
                                                }
                                                if (dispatchJobObj.has("TripDistance")) {
                                                    TripDistance = dispatchJobObj.getString("TripDistance");
                                                }
                                                if (dispatchJobObj.has("TripFare"))
                                                {
                                                    TripFare = dispatchJobObj.getString("TripFare");
                                                }
                                                if (dispatchJobObj.has("PickupLocation")) {
                                                    PickupLocation = dispatchJobObj.getString("PickupLocation");
                                                }
                                                if (dispatchJobObj.has("DropoffLocation")) {
                                                    DropoffLocation = dispatchJobObj.getString("DropoffLocation");
                                                }
                                                if (dispatchJobObj.has("Model")) {
                                                    Model = dispatchJobObj.getString("Model");
                                                }
                                                if (dispatchJobObj.has("PassengerName")) {
                                                    PassengerName = dispatchJobObj.getString("PassengerName");
                                                }
                                                if (dispatchJobObj.has("PassengerContact")) {
                                                    PassengerContact = dispatchJobObj.getString("PassengerContact");
                                                }
                                                if (dispatchJobObj.has("PickupDateTime")) {
                                                    PickupDateTime = dispatchJobObj.getString("PickupDateTime");
                                                }
                                                if (dispatchJobObj.has("FlightNumber")) {
                                                    FlightNumber = dispatchJobObj.getString("FlightNumber");
                                                }
                                                if (dispatchJobObj.has("Notes")) {
                                                    Notes = dispatchJobObj.getString("Notes");
                                                }
                                                if (dispatchJobObj.has("PaymentType")) {
                                                    PaymentType = dispatchJobObj.getString("PaymentType");
                                                }

                                                if (dispatchJobObj.has("PickupSuburb")) {
                                                    PickupSuburb = dispatchJobObj.getString("PickupSuburb");
                                                }
                                                if (dispatchJobObj.has("DropoffSuburb")) {
                                                    DropoffSuburb = dispatchJobObj.getString("DropoffSuburb");
                                                }
                                                if (dispatchJobObj.has("EstDriverEarning")) {
                                                    EstDriverEarning = dispatchJobObj.getString("EstDriverEarning");
                                                }

                                                if (dispatchJobObj.has("DispatcherDriverInfo"))
                                                {
                                                    DispatcherDriverInfo = dispatchJobObj.getString("DispatcherDriverInfo");
                                                    if (DispatcherDriverInfo!=null && !DispatcherDriverInfo.equalsIgnoreCase(""))
                                                    {
                                                        JSONObject DispatcherDriverInfoObj = dispatchJobObj.getJSONObject("DispatcherDriverInfo");
                                                        if (DispatcherDriverInfoObj!=null)
                                                        {
                                                            if (DispatcherDriverInfoObj.has("Email"))
                                                            {
                                                                dispatherEmail = DispatcherDriverInfoObj.getString("Email");
                                                            }
                                                            if (DispatcherDriverInfoObj.has("Fullname"))
                                                            {
                                                                dispatherFullname = DispatcherDriverInfoObj.getString("Fullname");
                                                            }
                                                            if (DispatcherDriverInfoObj.has("MobileNo"))
                                                            {
                                                                dispatherMobileNo = DispatcherDriverInfoObj.getString("MobileNo");
                                                            }
                                                        }
                                                    }
                                                }
                                                Log.e("ccccccccc","TripId : "+TripId+"\nPassengerId : "+PassengerId+"\nModelId : "+ModelId+"\nTripDistance : "+TripDistance+"\nPickupLocation : "+PickupLocation
                                                        +"\nDropoffLocation : "+DropoffLocation+"\nModel : "+Model+"\nPassengerName : "+PassengerName +"\nFlightNumber : "+FlightNumber+"\nNotes : "+Notes
                                                        +"\nPaymentType : "+PaymentType);

                                                list.add(new Future_BookingList_Been(TripId, PickupDateTime, PickupLocation, DropoffLocation, CompanyId, PassengerId, TripDistance, ModelId,
                                                        PassengerName, PassengerContact, Model, FlightNumber, Notes, PaymentType, dispatherEmail, dispatherFullname, dispatherMobileNo, TripFare,DropoffSuburb,PickupSuburb,EstDriverEarning));
                                            } else {
                                                Log.d(TAG, "bookingHistoryObj:NULL");
                                            }
                                            CountList++;
                                        }
                                        dialogClass.hideDialog();
                                        swipeRefreshLayout.setRefreshing(false);
                                        mAdapter.notifyDataSetChanged();
                                        if (list.size()>0)
                                        {
                                            tv_NoDataFound.setVisibility(View.GONE);
                                        }
                                        else
                                        {
                                            tv_NoDataFound.setVisibility(View.VISIBLE);
                                        }
                                        SessionSave.saveUserSession(Comman.NOTIFICATION_COUNT_FUTURE_BOOKING,CountList+"", Drawer_Activity.activity);
                                        Drawer_Activity.activity.Set_FutureBookingCount();
                                    }
                                    else
                                    {
                                        Log.d(TAG,"history:NULL");
                                        dialogClass.hideDialog();
                                        swipeRefreshLayout.setRefreshing(false);
                                        tv_NoDataFound.setVisibility(View.VISIBLE);
                                        if (json.has("message"))
                                        {
                                            Log.d(TAG, "message:" + json.getString("message"));
                                            new SnackbarUtils(Drawer_Activity.main_layout, json.getString("message"),
                                                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                                        }
                                    }
                                }
                                else
                                {
                                    Log.d(TAG,"history:NULL");
                                    dialogClass.hideDialog();
                                    swipeRefreshLayout.setRefreshing(false);
                                    tv_NoDataFound.setVisibility(View.VISIBLE);
                                    if (json.has("message"))
                                    {
                                        Log.d(TAG, "message:" + json.getString("message"));
                                        new SnackbarUtils(Drawer_Activity.main_layout, json.getString("message"),
                                                ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                                    }
                                }
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                swipeRefreshLayout.setRefreshing(false);
                                tv_NoDataFound.setVisibility(View.VISIBLE);
                                if (json.has("message"))
                                {
                                    Log.d(TAG, "message:" + json.getString("message"));
                                    new SnackbarUtils(Drawer_Activity.main_layout, json.getString("message"),
                                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        }
                        else
                        {
                            Log.e("status", "no status");
                            dialogClass.hideDialog();
                            swipeRefreshLayout.setRefreshing(false);
                            tv_NoDataFound.setVisibility(View.VISIBLE);
                            new SnackbarUtils(Drawer_Activity.main_layout, getString(R.string.something_is_wrong),
                                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                        }
                    }
                    else
                    {
                        Log.e(TAG, "getMessage = " + "null");
                        dialogClass.hideDialog();
                        swipeRefreshLayout.setRefreshing(false);
                        tv_NoDataFound.setVisibility(View.VISIBLE);
                        new SnackbarUtils(Drawer_Activity.main_layout, getString(R.string.something_is_wrong),
                                ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG, "Exception = " + e.getMessage() + "something_is_wrong");
                    dialogClass.hideDialog();
                    swipeRefreshLayout.setRefreshing(false);
                    tv_NoDataFound.setVisibility(View.VISIBLE);
                    new SnackbarUtils(Drawer_Activity.main_layout, getString(R.string.something_is_wrong),
                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }
}
