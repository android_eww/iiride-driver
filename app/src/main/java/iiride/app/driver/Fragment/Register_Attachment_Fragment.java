package iiride.app.driver.Fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import iiride.app.driver.Activity.Drawer_Activity;
import iiride.app.driver.Activity.Registration_Tab_Activity;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.Constants;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;


import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;



public class Register_Attachment_Fragment extends Fragment implements View.OnClickListener{

    TextView tv_next, tv_driver_licence_expiry, tv_accreditation_expiry, tv_carRegistration_expiry, tv_vehicle_Insurance_expiry;
    LinearLayout ll_driver_licence, ll_acrreditation_certy, ll_car_registration_certy, ll_vehicle_Insurance;
    ImageView iv_driverLicence, iv_accreditation_certy, iv_car_Registration, iv_vehicle_insurance;
    String ImageViewClicked = "", DRIVER_LICENCE="driverLicence", ACRREDITATION= "accreditation_certy", CAR_REGISTRATION="car_Registration", VEHICLE_INSURANCE="vehicle_insurance";
    String chooseCamera="camera", chooseGallery="gallery", userChoosenTask="";

    Intent intent;

    private static final String IMAGE_DIRECTORY = "/TickToc";
    private int GALLERY = 1, CAMERA = 0;
    private static final int MY_REQUEST_CODE = 102;

    Bitmap finalImageBitmap, oldBitmap;
    Transformation mTransformation;


    byte[] DriverLicence_ByteImage=null, Accreditation_certy_ByteImage=null, Car_Registration_ByteImage=null, Vehicle_insurance_ByteImage=null;

    DialogClass dialogClass;
    AQuery aQuery;

    LinearLayout main_layout;

    private DatePickerDialog fromDatePickerDialog;
    private SimpleDateFormat dateFormatter;
    String encodedImage;

    Uri mImageUri;
    private Uri imageUri;
    Bitmap bitmapImage;
    String picturePath;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_attachment_register, container, false);


        mImageUri=null;
        imageUri=null;
        bitmapImage=null;
        picturePath=null;

        aQuery = new AQuery(getActivity());

        main_layout = (LinearLayout) rootView.findViewById(R.id.main_layout);

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

        tv_next = (TextView) rootView.findViewById(R.id.tv_next);

        ll_driver_licence = (LinearLayout) rootView.findViewById(R.id.ll_driver_licence);
        ll_acrreditation_certy = (LinearLayout) rootView.findViewById(R.id.ll_acrreditation_certy);
        ll_car_registration_certy = (LinearLayout) rootView.findViewById(R.id.ll_car_registration_certy);
        ll_vehicle_Insurance = (LinearLayout) rootView.findViewById(R.id.ll_vehicle_Insurance);

        tv_driver_licence_expiry = (TextView) rootView.findViewById(R.id.tv_driver_licence_expiry);
        tv_accreditation_expiry = (TextView) rootView.findViewById(R.id.tv_accreditation_expiry);
        tv_carRegistration_expiry = (TextView) rootView.findViewById(R.id.tv_carRegistration_expiry);
        tv_vehicle_Insurance_expiry = (TextView) rootView.findViewById(R.id.tv_vehicle_Insurance_expiry);

        iv_driverLicence = (ImageView) rootView.findViewById(R.id.iv_driverLicence);
        iv_accreditation_certy = (ImageView) rootView.findViewById(R.id.iv_accreditation_certy);
        iv_car_Registration = (ImageView) rootView.findViewById(R.id.iv_car_Registration);
        iv_vehicle_insurance = (ImageView) rootView.findViewById(R.id.iv_vehicle_insurance);

        mTransformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(100)
                .borderColor(ContextCompat.getColor(getActivity(), R.color.colorGray))
                .borderWidthDp(2)
                .oval(true)
                .build();

        Registration_Tab_Activity registration_tab_activity = new Registration_Tab_Activity();
        registration_tab_activity.LogoutButton_Display(1);


        tv_next.setOnClickListener(this);
        ll_driver_licence.setOnClickListener(this);
        ll_acrreditation_certy.setOnClickListener(this);
        ll_car_registration_certy.setOnClickListener(this);
        ll_vehicle_Insurance.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tv_next:
                String Token = SessionSave.getToken(Comman.DEVICE_TOKEN, getActivity());
                if (Token!=null && !Token.equalsIgnoreCase("")) {
                    checkData();
                }else {
                    new SnackbarUtils(main_layout, getString(R.string.please_restart_app),
                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                }
                break;

            case R.id.ll_driver_licence:
                ImageViewClicked = DRIVER_LICENCE;
                showPictureDialog();
                break;

            case R.id.ll_acrreditation_certy:
                ImageViewClicked = ACRREDITATION;
                showPictureDialog();
                break;

            case R.id.ll_car_registration_certy:
                ImageViewClicked = CAR_REGISTRATION;
                showPictureDialog();
                break;

            case R.id.ll_vehicle_Insurance:
                ImageViewClicked = VEHICLE_INSURANCE;
                showPictureDialog();
                break;
        }
    }



    private void checkData() {
        if(tv_driver_licence_expiry.getText().toString().equalsIgnoreCase(""))
        {
            new SnackbarUtils(main_layout, getActivity().getResources().getString(R.string.please_enter_whole_licence_expire_date),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else if (tv_accreditation_expiry.getText().toString().equalsIgnoreCase(""))
        {
            new SnackbarUtils(main_layout,getActivity().getResources().getString(R.string.please_enter_whole_licence_expire_date),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else if (tv_carRegistration_expiry.getText().toString().equalsIgnoreCase(""))
        {
            new SnackbarUtils(main_layout,getActivity().getResources().getString(R.string.please_enter_whole_licence_expire_date),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else if (tv_vehicle_Insurance_expiry.getText().toString().equalsIgnoreCase(""))
        {
            new SnackbarUtils(main_layout, getActivity().getResources().getString(R.string.please_enter_whole_licence_expire_date),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else
        {
            UserRegistration();
        }

    }

    private void UserRegistration()
    {
        /*"CompanyId,SubUrb,City,State,Country,Zipcode,DriverImage,DriverLicenseExpire,AccreditationCertificateExpire,AccountHolderName,BankName,BankAcNo
        ,BSB,ABN,VehicleClass,VehicleColor,VehicleRegistrationNo
            ,RegistrationCertificateExpire,VehicleInsuranceCertificateExpire,VehicleImage, CompanyModel*/


        dialogClass = new DialogClass(getActivity(), 1);
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_OTP_FOR_DRIVER_REGISTER;

        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_DEVICE_TYPE, Comman.DEVICE_TYPE);
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_TOKEN, SessionSave.getToken(Comman.DEVICE_TOKEN, getActivity()));

        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_EMAIL, SessionSave.getUserSession(Comman.USER_EMAIL,getActivity()));
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_FULL_NAME, SessionSave.getUserSession(Comman.USER_FULL_NAME,getActivity()));
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_MOBILE_NUMBER, SessionSave.getUserSession(Comman.USER_COUNTRY_CODE,getActivity())+SessionSave.getUserSession(Comman.USER_MOBILE_NUMBER,getActivity()));
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_GENDER, SessionSave.getUserSession(Comman.USER_GENDER,getActivity()));
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_PASSWORD,SessionSave.getUserSession(Comman.USER_PASSWORD,getActivity()));
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_ADDRESS,SessionSave.getUserSession(Comman.USER_RESIDENTIAL_ADDRESS,getActivity()));
        if (SessionSave.getUserSession(Comman.USER_REFERRAL_CODE,getActivity())!=null && !SessionSave.getUserSession(Comman.USER_REFERRAL_CODE,getActivity()).equalsIgnoreCase(""))
        {
            params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_REFERRAL_CODE,SessionSave.getUserSession(Comman.USER_REFERRAL_CODE,getActivity()));
        }
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_LATITUDE, Constants.newgpsLatitude);
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_LONGITUDE, Constants.newgpsLatitude);
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_VEHICLE_CLASS, SessionSave.getUserSession(Comman.USER_VEHICLE_MODEL,getActivity()));
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_DRIVER_LICENCE, DriverLicence_ByteImage);
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_ACCREDITATION_CERTY, Accreditation_certy_ByteImage);
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_CAR_REGISTRATION, Car_Registration_ByteImage);
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_VEHICLE_INSURANCE,Vehicle_insurance_ByteImage);

        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_COMPANY_ID, SessionSave.getUserSession(Comman.USER_COMPANY_ID,getActivity()));
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_SUB_URB, SessionSave.getUserSession(Comman.USER_SUB_URB,getActivity()));
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_CITY, SessionSave.getUserSession(Comman.USER_CITY,getActivity()));
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_COUNTRY, SessionSave.getUserSession(Comman.USER_COUNTRY,getActivity()));
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_STATE, SessionSave.getUserSession(Comman.USER_STATE,getActivity()));
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_ZIPCODE, SessionSave.getUserSession(Comman.USER_POST_CODE,getActivity()));
        byte[] driverImage_ByteImage = Base64.decode(SessionSave.getUserSession(Comman.USER_PROFILE_IMAGE,getActivity()), Base64.DEFAULT);
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_DRIVER_IMAGE, driverImage_ByteImage);
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_DRIVER_LICENCE_EXPIRY, tv_driver_licence_expiry.getText().toString());

        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_ACCREDITATION_CERTY_EXPIRY, tv_accreditation_expiry.getText().toString());
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_BANK_ACCOUNT_HOLDER_NALE, SessionSave.getUserSession(Comman.USER_BANK_ACCOUNT_HOLDER_NAME,getActivity()));
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_BANK_NAME, SessionSave.getUserSession(Comman.USER_BANK_NAME,getActivity()));
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_BANK_ACCOUNT_NO, SessionSave.getUserSession(Comman.USER_BANK_AC_NO,getActivity()));
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_BSB, SessionSave.getUserSession(Comman.USER_BSB,getActivity()));
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_ABN, SessionSave.getUserSession(Comman.USER_ABN,getActivity()));
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_SERVICE_DESCRIPTION, SessionSave.getUserSession(Comman.USER_SERVICE_DESCRIPTION,getActivity()));
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_VEHICLE_COLOR, SessionSave.getUserSession(Comman.USER_CAR_COLOR,getActivity()));
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_VEHICLE_RIGISTRATION_NO, SessionSave.getUserSession(Comman.USER_VEHICLE_REGISTRATION_NO,getActivity()));

        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_REGISTRATION_CERTY_EXPIRY, tv_carRegistration_expiry.getText().toString());
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_VEHICLE_INSURANCE_CERTY_EXPIRE, tv_vehicle_Insurance_expiry.getText().toString());
        byte[] vehicleImage_ByteImage = Base64.decode(SessionSave.getUserSession(Comman.USER_VEHICLE_IMAGE,getActivity()), Base64.DEFAULT);
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_VEHICLE_IMAGE, vehicleImage_ByteImage);
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_COMPANY_MODEL, SessionSave.getUserSession(Comman.USER_CAR_COMPANY,getActivity()));
        params.put(WebServiceAPI.DRIVER_REGISTER_PARAM_CITY_ID, SessionSave.getUserSession(Comman.USER_CITY_ID,getActivity()));


        Log.e("url", "UserRegistration = " + url);
        Log.e("param", "UserRegistration = " + params);


        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "UserRegistration = " + responseCode);
                    Log.e("Response", "UserRegistration = " + json);

                    /// {"status":true,"otp":378048,"message":"Mobile verification code sent on your email address"}

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {

                                SessionSave.saveUserSession(Comman.LOGIN_FLAG_USER,"1",getActivity());
                                SessionSave.saveUserSession(Comman.REGISTER_ATTACHMENT_ACTIVITY, "1",getActivity());
                                SessionSave.saveUserSession(Comman.CREATED_PASSCODE,"",getActivity());

                                if (json.has("profile"))
                                {
                                    JSONObject driverProfile = json.getJSONObject("profile");
                                    if (driverProfile!=null)
                                    {
                                        String DriverId="", CompanyId="", DispatcherId="", Email="", FullName="", MobileNo="", Gender="", Image="", QRCode="", Password="", Address="", SubUrb = ""
                                                , City="", State="", Country="", Zipcode="", ReferralCode="", DriverLicense="", AccreditationCertificate="", DriverLicenseExpire=""
                                                , AccreditationCertificateExpire="", BankHolderName, BankName="", BankAcNo="", BSB="", Lat="", Lng="", Status=""
                                                , Availability="", DriverDuty="", ABN="", serviceDescription="", DCNumber="", ProfileComplete="", CategoryId="", Balance="", ReferralAmount="";
                                        if (driverProfile.has("Id"))
                                        {
                                            DriverId = driverProfile.getString("Id");
                                            SessionSave.saveUserSession(Comman.USER_ID, DriverId, getActivity());
                                        }
                                        if (driverProfile.has("CompanyId"))
                                        {
                                            CompanyId = driverProfile.getString("CompanyId");
                                            SessionSave.saveUserSession(Comman.USER_COMPANY_ID, CompanyId, getActivity());
                                        }
                                        if (driverProfile.has("CityId"))
                                        {
                                            SessionSave.saveUserSession(Comman.USER_CITY_ID, driverProfile.getString("CityId"), getActivity());
                                        }
                                        if (driverProfile.has("DispatcherId"))
                                        {
                                            DispatcherId = driverProfile.getString("DispatcherId");
                                            SessionSave.saveUserSession(Comman.USER_DISPATHER_ID, DispatcherId, getActivity());
                                        }
                                        if (driverProfile.has("Email"))
                                        {
                                            Email = driverProfile.getString("Email");
                                            SessionSave.saveUserSession(Comman.USER_EMAIL, Email, getActivity());
                                        }
                                        if (driverProfile.has("Fullname"))
                                        {
                                            FullName = driverProfile.getString("Fullname");
                                            SessionSave.saveUserSession(Comman.USER_FULL_NAME, FullName, getActivity());
                                        }


                                        if (driverProfile.has("MobileNo"))
                                        {
                                            MobileNo = driverProfile.getString("MobileNo");
                                            SessionSave.saveUserSession(Comman.USER_MOBILE_NUMBER, MobileNo, getActivity());
                                        }
                                        if (driverProfile.has("Gender"))
                                        {
                                            Gender = driverProfile.getString("Gender");
                                            SessionSave.saveUserSession(Comman.USER_GENDER, Gender, getActivity());
                                        }
                                        if (driverProfile.has("Image"))
                                        {
                                            Image = driverProfile.getString("Image");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_IMAGE, Image, getActivity());
                                        }
                                        if (driverProfile.has("QRCode"))
                                        {
                                            QRCode = driverProfile.getString("QRCode");
                                            SessionSave.saveUserSession(Comman.USER_QR_CODE, QRCode, getActivity());
                                        }
                                        if (driverProfile.has("Password"))
                                        {
                                            Password = driverProfile.getString("Password");
                                            SessionSave.saveUserSession(Comman.USER_PASSWORD, Password, getActivity());
                                        }
                                        if (driverProfile.has("Address"))
                                        {
                                            Address = driverProfile.getString("Address");
                                            SessionSave.saveUserSession(Comman.USER_RESIDENTIAL_ADDRESS, Address, getActivity());
                                        }
                                        if (driverProfile.has("SubUrb"))
                                        {
                                            SubUrb = driverProfile.getString("SubUrb");
                                            SessionSave.saveUserSession(Comman.USER_SUB_URB, SubUrb, getActivity());
                                        }


                                        if (driverProfile.has("City"))
                                        {
                                            City = driverProfile.getString("City");
                                            SessionSave.saveUserSession(Comman.USER_CITY, City, getActivity());
                                        }
                                        if (driverProfile.has("State"))
                                        {
                                            State = driverProfile.getString("State");
                                            SessionSave.saveUserSession(Comman.USER_STATE, State, getActivity());
                                        }
                                        if (driverProfile.has("Country"))
                                        {
                                            Country = driverProfile.getString("Country");
                                            SessionSave.saveUserSession(Comman.USER_COUNTRY, Country, getActivity());
                                        }
                                        if (driverProfile.has("ZipCode"))
                                        {
                                            Zipcode = driverProfile.getString("ZipCode");
                                            SessionSave.saveUserSession(Comman.USER_POST_CODE, Zipcode, getActivity());
                                        }
                                        if (driverProfile.has("ReferralCode"))
                                        {
                                            ReferralCode = driverProfile.getString("ReferralCode");
                                            SessionSave.saveUserSession(Comman.USER_REFERRAL_CODE, ReferralCode, getActivity());
                                        }



                                        if (driverProfile.has("DriverLicense"))
                                        {
                                            DriverLicense = driverProfile.getString("DriverLicense");
                                            SessionSave.saveUserSession(Comman.USER_DRIVER_LICENCE, DriverLicense, getActivity());
                                        }
                                        if (driverProfile.has("AccreditationCertificate"))
                                        {
                                            AccreditationCertificate = driverProfile.getString("AccreditationCertificate");
                                            SessionSave.saveUserSession(Comman.USER_ACCREDITATION_CERTY, AccreditationCertificate, getActivity());
                                        }
                                        if (driverProfile.has("DriverLicenseExpire"))
                                        {
                                            DriverLicenseExpire = driverProfile.getString("DriverLicenseExpire");
                                            SessionSave.saveUserSession(Comman.USER_DRIVER_lICENSE_EXPIRE, DriverLicenseExpire, getActivity());
                                        }
                                        if (driverProfile.has("AccreditationCertificateExpire"))
                                        {
                                            AccreditationCertificateExpire = driverProfile.getString("AccreditationCertificateExpire");
                                            SessionSave.saveUserSession(Comman.USER_ACCREDITATION_CERTY_EXPIRE, AccreditationCertificateExpire, getActivity());
                                        }
                                        if (driverProfile.has("BankHolderName"))
                                        {
                                            BankHolderName = driverProfile.getString("BankHolderName");
                                            SessionSave.saveUserSession(Comman.USER_BANK_ACCOUNT_HOLDER_NAME, BankHolderName, getActivity());
                                        }
                                        if (driverProfile.has("BankName"))
                                        {
                                            BankName = driverProfile.getString("BankName");
                                            SessionSave.saveUserSession(Comman.USER_BANK_NAME, BankName, getActivity());
                                        }



                                        if (driverProfile.has("BankAcNo"))
                                        {
                                            BankAcNo = driverProfile.getString("BankAcNo");
                                            SessionSave.saveUserSession(Comman.USER_BANK_AC_NO, BankAcNo, getActivity());
                                        }
                                        if (driverProfile.has("BSB"))
                                        {
                                            BSB = driverProfile.getString("BSB");
                                            SessionSave.saveUserSession(Comman.USER_BSB, BSB, getActivity());
                                        }
                                        if (driverProfile.has("Lat"))
                                        {
                                            Lat = driverProfile.getString("Lat");
                                            SessionSave.saveUserSession(Comman.USER_LATITUDE, Lat, getActivity());
                                        }
                                        if (driverProfile.has("Lng"))
                                        {
                                            Lng = driverProfile.getString("Lng");
                                            SessionSave.saveUserSession(Comman.USER_LONGITUDE, Lng, getActivity());
                                        }
                                        if (driverProfile.has("Status"))
                                        {
                                            Status = driverProfile.getString("Status");
                                            SessionSave.saveUserSession(Comman.USER_STATE, Status, getActivity());
                                        }
                                        if (driverProfile.has("Availability"))
                                        {
                                            Availability = driverProfile.getString("Availability");
                                            SessionSave.saveUserSession(Comman.USER_AVAILABILITY, Availability, getActivity());
                                        }



                                        if (driverProfile.has("DriverDuty"))
                                        {
                                            DriverDuty = driverProfile.getString("DriverDuty");
                                            SessionSave.saveUserSession(Comman.USER_DRIVER_DUTY, DriverDuty, getActivity());
                                        }
                                        if (driverProfile.has("ABN"))
                                        {
                                            ABN = driverProfile.getString("ABN");
                                            SessionSave.saveUserSession(Comman.USER_ABN, ABN, getActivity());
                                        }
                                        if (driverProfile.has("Description"))
                                        {
                                            serviceDescription = driverProfile.getString("Description");
                                            SessionSave.saveUserSession(Comman.USER_SERVICE_DESCRIPTION, serviceDescription, getActivity());
                                        }
                                        if (driverProfile.has("DCNumber"))
                                        {
                                            DCNumber = driverProfile.getString("DCNumber");
                                            SessionSave.saveUserSession(Comman.USER_DC_NUMBER, DCNumber, getActivity());
                                        }
                                        if (driverProfile.has("ProfileComplete"))
                                        {
                                            ProfileComplete = driverProfile.getString("ProfileComplete");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_COMPLETE, ProfileComplete, getActivity());
                                        }
                                        if (driverProfile.has("CategoryId"))
                                        {
                                            CategoryId = driverProfile.getString("CategoryId");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_CATEGORY_ID, CategoryId, getActivity());
                                        }
                                        if (driverProfile.has("Balance"))
                                        {
                                            Balance = driverProfile.getString("Balance");
                                            SessionSave.saveUserSession(Comman.DRIVER_WALLET_BALLENCE, Balance,  getActivity());
                                        }
                                        if (driverProfile.has("ReferralAmount"))
                                        {
                                            ReferralAmount = driverProfile.getString("ReferralAmount");
                                            SessionSave.saveUserSession(Comman.USER_PROFILE_REFERRAL_AMOUNT, ReferralAmount,  getActivity());
                                        }


                                        if (driverProfile.has("Vehicle"))
                                        {
                                            JSONObject driverVehicle = driverProfile.getJSONObject("Vehicle");
                                            if (driverVehicle!=null)
                                            {
                                                String VehicleId, VehicleModel, CarCompany, CarColor, VehicleRegistrationNo, RegistrationCertificate, VehicleInsuranceCertificate
                                                        , RegistrationCertificateExpire, VehicleInsuranceCertificateExpire, VehicleImage, Description, VehicleModelName;

                                                if (driverVehicle.has("Id"))
                                                {
                                                    VehicleId = driverVehicle.getString("Id");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_ID, VehicleId, getActivity());
                                                }
                                                if (driverVehicle.has("VehicleModel"))
                                                {
                                                    VehicleModel = driverVehicle.getString("VehicleModel");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL, VehicleModel, getActivity());
                                                }
                                                if (driverVehicle.has("Company"))
                                                {
                                                    CarCompany = driverVehicle.getString("Company");
                                                    SessionSave.saveUserSession(Comman.USER_CAR_COMPANY, CarCompany, getActivity());
                                                }
                                                if (driverVehicle.has("Color"))
                                                {
                                                    CarColor = driverVehicle.getString("Color");
                                                    SessionSave.saveUserSession(Comman.USER_CAR_COLOR, CarColor, getActivity());
                                                }
                                                if (driverVehicle.has("VehicleRegistrationNo"))
                                                {
                                                    VehicleRegistrationNo = driverVehicle.getString("VehicleRegistrationNo");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_REGISTRATION_NO, VehicleRegistrationNo, getActivity());
                                                }


                                                if (driverVehicle.has("RegistrationCertificate"))
                                                {
                                                    RegistrationCertificate = driverVehicle.getString("RegistrationCertificate");
                                                    SessionSave.saveUserSession(Comman.USER_REGISTRATION_CERTY, RegistrationCertificate, getActivity());
                                                }
                                                if (driverVehicle.has("VehicleInsuranceCertificate"))
                                                {
                                                    VehicleInsuranceCertificate = driverVehicle.getString("VehicleInsuranceCertificate");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_INSURANCE_CERTY, VehicleInsuranceCertificate, getActivity());
                                                }
                                                if (driverVehicle.has("RegistrationCertificateExpire"))
                                                {
                                                    RegistrationCertificateExpire = driverVehicle.getString("RegistrationCertificateExpire");
                                                    SessionSave.saveUserSession(Comman.USER_REGISTRATION_CERTY_EXPIRE, RegistrationCertificateExpire, getActivity());
                                                }
                                                if (driverVehicle.has("VehicleInsuranceCertificateExpire"))
                                                {
                                                    VehicleInsuranceCertificateExpire = driverVehicle.getString("VehicleInsuranceCertificateExpire");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_INSURANCE_CERTY_EXPIRE, VehicleInsuranceCertificateExpire, getActivity());
                                                }
                                                if (driverVehicle.has("VehicleImage"))
                                                {
                                                    VehicleImage = driverVehicle.getString("VehicleImage");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE, VehicleImage, getActivity());
                                                }

                                                if (driverVehicle.has("Description"))
                                                {
                                                    Description = driverVehicle.getString("Description");
                                                    SessionSave.saveUserSession(Comman.USER_CAR_DESCRIPTION, Description, getActivity());
                                                }
                                                if (driverVehicle.has("VehicleClass"))
                                                {
                                                    VehicleModelName = driverVehicle.getString("VehicleClass");
                                                    SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL_NAME, VehicleModelName, getActivity());

                                                    Log.e("VehicleModelName","VehicleModelName : "+VehicleModelName);
                                                    if (VehicleModelName!=null && VehicleModelName.contains("First Class"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "First Class", getActivity());
                                                    }
                                                    else if (VehicleModelName.contains("Business Class"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", getActivity());
                                                    }
                                                    else if (VehicleModelName.contains("Economy"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Economy", getActivity());
                                                    }
                                                    else if (VehicleModelName.contains("Taxi"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Taxi", getActivity());
                                                    }
                                                    else if (VehicleModelName.contains("LUX-VAN"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "LUX-VAN", getActivity());
                                                    }
                                                    else if (VehicleModelName.contains("Disability"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Disability", getActivity());
                                                    }
                                                    else
                                                    {
                                                        SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", getActivity());
                                                    }
                                                }
                                                else
                                                {
                                                    SessionSave.saveUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, "Business Class", getActivity());
                                                }
                                                dialogClass.hideDialog();
                                            }
                                            else
                                            {
                                                //////// else
                                                dialogClass.hideDialog();
                                            }
                                            dialogClass.hideDialog();

                                            Log.e("GGGGGGGGGGG","GGGGGGGGGGG :"+SessionSave.getUserSession(Comman.USER_ID, getActivity()));
                                        }

                                    }
                                    else
                                    {
                                        dialogClass.hideDialog();
                                        if (json.has("message")) {
                                            new SnackbarUtils(main_layout, json.getString("message"),
                                                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                                        }
                                    }
                                }


                                dialogClass.hideDialog();
//                                SessionSave.clearUserSession(getActivity());
                                intent = new Intent(getActivity(), Drawer_Activity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message")) {
                                new SnackbarUtils(main_layout, json.getString("message"),
                                        ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                    }
                } catch (Exception e) {
                    Log.e("UserRegistration", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }.header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }

    private void showPictureDialog()
    {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            requestPermissions(new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_REQUEST_CODE);
        }
        else
        {
            AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getActivity());
            pictureDialog.setTitle("Select Action");
            String[] pictureDialogItems = {"Select photo from gallery", "Capture photo from camera" };
            pictureDialog.setItems(pictureDialogItems,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:
                                    choosePhotoFromGallary();
                                    break;
                                case 1:
                                    takePhotoFromCamera();
                                    break;
                            }
                        }
                    });
            pictureDialog.show();
        }
    }

    public void choosePhotoFromGallary()
    {
        userChoosenTask =chooseGallery;
        Intent intent_gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent_gallery, GALLERY);

    }

    private void takePhotoFromCamera()
    {
        userChoosenTask = chooseCamera;
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "NewPicture");
        imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        Log.e("imageUri", "imageUri11111111111111 : "+imageUri);
        startActivityForResult(intent, CAMERA);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.e("showPictureDialog", "onRequestPermissionsResult requestCode "+requestCode);
        switch (requestCode) {
            case MY_REQUEST_CODE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED)
                {
                    showPictureDialog();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("showPictureDialog", "onActivityResult requestCode "+requestCode);
        if (resultCode == getActivity().RESULT_OK)
        {
            if (requestCode == GALLERY)
            {
                onSelectFromGalleryResult(data);
            }
            else if (requestCode == CAMERA)
            {
                onCaptureImageResult();
            }
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data)
    {
        Bitmap bitmap= null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("MediaStore","MediaStore");
        }

        bitmapImage = getResizedBitmap(bitmap,400);

        if (ImageViewClicked!=null && !ImageViewClicked.equalsIgnoreCase(""))
        {
            if (ImageViewClicked.equalsIgnoreCase(DRIVER_LICENCE))
            {
                Picasso.with(getActivity())
                        .load(data.getData())
                        .fit()
                        .into(iv_driverLicence);
                DriverLicence_ByteImage = ConvertToByteArray(bitmapImage);
                encodedImage = Base64.encodeToString(DriverLicence_ByteImage, Base64.DEFAULT);
                SessionSave.saveUserSession(Comman.USER_DRIVER_LICENCE, encodedImage,getActivity());

                OpenPopopFor_datePicker(tv_driver_licence_expiry, iv_driverLicence);
            }
            else if(ImageViewClicked.equalsIgnoreCase(ACRREDITATION))
            {
                Picasso.with(getActivity())
                        .load(data.getData())
                        .fit()
                        .into(iv_accreditation_certy);
                Accreditation_certy_ByteImage = ConvertToByteArray(bitmapImage);

                encodedImage = Base64.encodeToString(DriverLicence_ByteImage, Base64.DEFAULT);
                SessionSave.saveUserSession(Comman.USER_ACCREDITATION_CERTY, encodedImage,getActivity());

                OpenPopopFor_datePicker(tv_accreditation_expiry, iv_accreditation_certy);
            }
            else if(ImageViewClicked.equalsIgnoreCase(CAR_REGISTRATION))
            {
                Picasso.with(getActivity())
                        .load(data.getData())
                        .fit()
                        .into(iv_car_Registration);
                Car_Registration_ByteImage = ConvertToByteArray(bitmapImage);

                encodedImage = Base64.encodeToString(DriverLicence_ByteImage, Base64.DEFAULT);
                SessionSave.saveUserSession(Comman.USER_REGISTRATION_CERTY, encodedImage,getActivity());

                OpenPopopFor_datePicker(tv_carRegistration_expiry, iv_car_Registration);
            }
            else if(ImageViewClicked.equalsIgnoreCase(VEHICLE_INSURANCE))
            {
                Picasso.with(getActivity())
                        .load(data.getData())
                        .fit()
                        .into(iv_vehicle_insurance);
                Vehicle_insurance_ByteImage = ConvertToByteArray(bitmapImage);

                encodedImage = Base64.encodeToString(DriverLicence_ByteImage, Base64.DEFAULT);
                SessionSave.saveUserSession(Comman.USER_VEHICLE_INSURANCE_CERTY, encodedImage,getActivity());

                OpenPopopFor_datePicker(tv_vehicle_Insurance_expiry, iv_vehicle_insurance);
            }
        }
    }

    private void onCaptureImageResult()
    {
        try
        {
            Log.e("imageUri", "imageUri2222222222222222222 : "+imageUri);
            Bitmap thumbnail = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageUri);
            Log.e("#########","bmp height = "+thumbnail.getHeight());
            Log.e("#########","bmp width = "+thumbnail.getWidth());
            thumbnail = getResizedBitmap(thumbnail,400);
            bitmapImage=thumbnail;

            if (ImageViewClicked!=null && !ImageViewClicked.equalsIgnoreCase(""))
            {
                if (ImageViewClicked.equalsIgnoreCase(DRIVER_LICENCE))
                {
                    Picasso.with(getActivity())
                            .load(imageUri)
                            .fit()
                            .into(iv_driverLicence);
                    DriverLicence_ByteImage = ConvertToByteArray(thumbnail);

                    encodedImage = Base64.encodeToString(DriverLicence_ByteImage, Base64.DEFAULT);
                    SessionSave.saveUserSession(Comman.USER_DRIVER_LICENCE, encodedImage,getActivity());

                    OpenPopopFor_datePicker(tv_driver_licence_expiry, iv_driverLicence);
                }
                else if(ImageViewClicked.equalsIgnoreCase(ACRREDITATION))
                {
                    Picasso.with(getActivity())
                            .load(imageUri)
                            .fit()
                            .into(iv_accreditation_certy);
                    Accreditation_certy_ByteImage = ConvertToByteArray(thumbnail);

                    encodedImage = Base64.encodeToString(Accreditation_certy_ByteImage, Base64.DEFAULT);
                    SessionSave.saveUserSession(Comman.USER_ACCREDITATION_CERTY, encodedImage,getActivity());

                    OpenPopopFor_datePicker(tv_accreditation_expiry, iv_accreditation_certy);
                }
                else if(ImageViewClicked.equalsIgnoreCase(CAR_REGISTRATION))
                {
                    Picasso.with(getActivity())
                            .load(imageUri)
                            .fit()
                            .into(iv_car_Registration);
                    Car_Registration_ByteImage = ConvertToByteArray(thumbnail);

                    encodedImage = Base64.encodeToString(Car_Registration_ByteImage, Base64.DEFAULT);
                    SessionSave.saveUserSession(Comman.USER_REGISTRATION_CERTY, encodedImage,getActivity());

                    OpenPopopFor_datePicker(tv_carRegistration_expiry, iv_car_Registration);
                }
                else if(ImageViewClicked.equalsIgnoreCase(VEHICLE_INSURANCE))
                {
                    Picasso.with(getActivity())
                            .load(imageUri)
                            .fit()
                            .into(iv_vehicle_insurance);
                    Vehicle_insurance_ByteImage = ConvertToByteArray(thumbnail);

                    encodedImage = Base64.encodeToString(Vehicle_insurance_ByteImage, Base64.DEFAULT);
                    SessionSave.saveUserSession(Comman.USER_VEHICLE_INSURANCE_CERTY, encodedImage,getActivity());

                    OpenPopopFor_datePicker(tv_vehicle_Insurance_expiry, iv_vehicle_insurance);
                }
            }
        }
        catch (Exception e)
        {
            Log.e("call","exception = "+e.getMessage());
        }
    }

    public void OpenPopopFor_datePicker(final TextView textView, final ImageView imageView)
    {
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(getActivity(), R.style.DialogThemeReal, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                textView.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
//        fromDatePickerDialog.setSpinnersShown

        fromDatePickerDialog.setTitle("Please add Expiry Date");
        fromDatePickerDialog.show();
        fromDatePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Log.e("yyyyyy","yyyyyyyyyyy: ");
                textView.setText("");
                imageView.setImageResource(R.drawable.icon_camera);
            }
        });
    }

    public byte[] ConvertToByteArray(Bitmap bmp)
    {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 10, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }
}
