package iiride.app.driver.Fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import iiride.app.driver.Activity.Registration_Tab_Activity;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;


import iiride.app.driver.R;
import iiride.app.driver.View.SnackbarUtils;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Register_ProfileDetail_Fragment extends Fragment implements View.OnClickListener{


    CheckBox checkBox_male, checkBox_Female, checkBox_others;
    TextView tv_next;
    LinearLayout main_layout;

    EditText et_email_registration, et_full_name, et_mobile_register, et_password_register, et_residential_address_register, et_sb_urb_register, et_post_code_register, et_city_register, et_country_register
            , et_state_register, et_invite_code_register, et_accountHolder_Name_register, et_Abn_register, et_BankName_register, et_BankAccountNo_register
            , et_Bsb_register, et_serviceDescription ;
    String MALE="male", FEMALE ="female", OTHERS="others", chackBox_Selected ="";

    ImageView iv_profile_register;


    String chooseCamera="camera", chooseGallery="gallery", userChoosenTask="";
    private static final int MY_REQUEST_CODE = 102;
    private int GALLERY = 1, CAMERA = 0;
    byte[] Profile_ByteImage=null;
    Transformation mTransformation;
    String encodedImage;

    Spinner spinner_CompanyId;
    List<String> list_companyName = new ArrayList<String>();

    Uri mImageUri;
    private Uri imageUri;
    Bitmap bitmapImage;
    String picturePath;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_profile_register, container, false);

        mImageUri=null;
        imageUri=null;
        bitmapImage=null;
        picturePath=null;

        main_layout = (LinearLayout) rootView.findViewById(R.id.main_layout_profRegister);

        iv_profile_register= (ImageView) rootView.findViewById(R.id.iv_profile_register);

        spinner_CompanyId = (Spinner) rootView.findViewById(R.id.spinner_CompanyId);

        mTransformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(100)
                .borderColor(ContextCompat.getColor(getActivity(), R.color.colorGray))
                .borderWidthDp(0)
                .oval(true)
                .build();

        Registration_Tab_Activity registration_tab_activity = new Registration_Tab_Activity();
        registration_tab_activity.LogoutButton_Display(1);


        checkBox_male = (CheckBox) rootView.findViewById(R.id.checkBox_male);
        checkBox_male.setChecked(true);
        chackBox_Selected = MALE;
        checkBox_Female = (CheckBox) rootView.findViewById(R.id.checkBox_Female);
        checkBox_others = (CheckBox) rootView.findViewById(R.id.checkBox_others);

        et_email_registration = (EditText) rootView.findViewById(R.id.et_email_registration);
        et_email_registration.setEnabled(false);
        et_full_name = (EditText) rootView.findViewById(R.id.et_full_name);
        et_mobile_register = (EditText) rootView.findViewById(R.id.et_mobile_register);
        et_password_register = (EditText) rootView.findViewById(R.id.et_password_register);
        et_residential_address_register = (EditText) rootView.findViewById(R.id.et_residential_address_register);
        et_sb_urb_register = (EditText) rootView.findViewById(R.id.et_sb_urb_register);
        et_post_code_register = (EditText) rootView.findViewById(R.id.et_post_code_register);
        et_city_register = (EditText) rootView.findViewById(R.id.et_city_register);
        et_country_register = (EditText) rootView.findViewById(R.id.et_country_register);
        et_state_register = (EditText) rootView.findViewById(R.id.et_state_register);
        et_invite_code_register = (EditText) rootView.findViewById(R.id.et_invite_code_register);
        et_serviceDescription = (EditText) rootView.findViewById(R.id.et_serviceDescription);

        et_accountHolder_Name_register = (EditText) rootView.findViewById(R.id.et_accountHolder_Name_register);
        et_Abn_register = (EditText) rootView.findViewById(R.id.et_Abn_register);
        et_BankName_register = (EditText) rootView.findViewById(R.id.et_BankName_register);
        et_BankAccountNo_register = (EditText) rootView.findViewById(R.id.et_BankAccountNo_register);
        et_Bsb_register = (EditText) rootView.findViewById(R.id.et_Bsb_register);

        tv_next = (TextView) rootView.findViewById(R.id.tv_next);


        String UserEmail = SessionSave.getUserSession(Comman.USER_EMAIL,getActivity());
        if (UserEmail!= null && !UserEmail.equalsIgnoreCase(""))
        {
            et_email_registration.setText(UserEmail);
        }
        else
        {
            new SnackbarUtils(main_layout, getString(R.string.please_try_to_login_again),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }

       /* if (SessionSave.getUserSession(Comman.USER_PROFILE_IMAGE,getActivity())!=null && !SessionSave.getUserSession(Comman.USER_PROFILE_IMAGE,getActivity()).equalsIgnoreCase(""))
        {
            Picasso.with(getActivity())
                    .load(getImageUri(getActivity(),decodeBase64(SessionSave.getUserSession(Comman.USER_PROFILE_IMAGE,getActivity()))))
                    .fit()
                    .transform(mTransformation)
                    .into(iv_profile_register);
        }*/

        spinner_CompanyId.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                Log.e("tttttttt","onItemSelected : "+position);
                Log.e("tttttttt","onItemSelected 11 : "+spinner_CompanyId.getSelectedItem().toString());
                try
                {
                    JSONObject jsonObj = new JSONObject(SessionSave.getUserSession(Comman.USER_COMPANY_ID_ARRAY,getActivity()));
                    JSONArray jsonArray = jsonObj.getJSONArray(Comman.USER_COMPANY_ARRAY_NAME_FOR_STORE);
                    for (int i=0; i<jsonArray.length(); i++)
                    {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String CompanyId = jsonObject.getString(Comman.USER_COMPANY_ID_FOR_STORE);
                        String CompanyName = jsonObject.getString(Comman.USER_COMPANY_NAME_FOR_STORE);
                        if (String.valueOf(spinner_CompanyId.getSelectedItem())!=null && !String.valueOf(spinner_CompanyId.getSelectedItem()).equalsIgnoreCase("")
                                && String.valueOf(spinner_CompanyId.getSelectedItem()).equalsIgnoreCase(CompanyName))
                        {
                            Log.e("selected ", " :"+String.valueOf(spinner_CompanyId.getSelectedItem()+" : "+CompanyId+"\n "+jsonObject.getString(Comman.USER_COMPANY_CITY_FOR_STORE)));
                            et_city_register.setText(jsonObject.getString(Comman.USER_COMPANY_CITY_FOR_STORE));
                            et_state_register.setText(jsonObject.getString(Comman.USER_COMPANY_STATE_FOR_STORE));
                            et_country_register.setText(jsonObject.getString(Comman.USER_COMPANY_COUNTRY_FOR_STORE));
                        }
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });

        if (SessionSave.getUserSession(Comman.USER_COMPANY_ID_ARRAY,getActivity())!=null && !SessionSave.getUserSession(Comman.USER_COMPANY_ID_ARRAY,getActivity()).equalsIgnoreCase(""))
        {
            try
            {

                list_companyName.clear();
                JSONObject jsonObj = new JSONObject(SessionSave.getUserSession(Comman.USER_COMPANY_ID_ARRAY,getActivity()));
                JSONArray jsonArray = jsonObj.getJSONArray(Comman.USER_COMPANY_ARRAY_NAME_FOR_STORE);
                Log.e("CompanyName","CompanyNameCompanyName 111111111:"+jsonArray);
                for (int i=0; i<jsonArray.length(); i++)
                {
                    Log.e("CompanyName","CompanyNameCompanyName 2222222222:");

                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    String CompanyId = jsonObject.getString(Comman.USER_COMPANY_ID_FOR_STORE);
                    String CompanyName = jsonObject.getString(Comman.USER_COMPANY_NAME_FOR_STORE);
                    Log.e("CompanyName","CompanyNameCompanyName :"+CompanyName);
                    list_companyName.add(CompanyName);
                }
                Log.e("CompanyName","CompanyNameCompanyName 3333333333333:");

                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list_companyName);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner_CompanyId.setAdapter(dataAdapter);
            }
            catch (JSONException e) {
                e.printStackTrace();
            }

        }

        if (SessionSave.getUserSession(Comman.USER_FULL_NAME,getActivity())!= null && !SessionSave.getUserSession(Comman.USER_FULL_NAME,getActivity()).equalsIgnoreCase(""))
        {
            et_full_name.setText(SessionSave.getUserSession(Comman.USER_FULL_NAME,getActivity()));
        }

        /*if (SessionSave.getUserSession(Comman.USER_MOBILE_NUMBER,getActivity())!= null && !SessionSave.getUserSession(Comman.USER_MOBILE_NUMBER,getActivity()).equalsIgnoreCase(""))
        {
            et_mobile_register.setText(SessionSave.getUserSession(Comman.USER_MOBILE_NUMBER,getActivity()));
        }*/

        if (SessionSave.getUserSession(Comman.USER_PASSWORD,getActivity())!= null && !SessionSave.getUserSession(Comman.USER_PASSWORD,getActivity()).equalsIgnoreCase(""))
        {
            et_password_register.setText(SessionSave.getUserSession(Comman.USER_PASSWORD,getActivity()));
        }

        if (SessionSave.getUserSession(Comman.USER_RESIDENTIAL_ADDRESS,getActivity())!= null && !SessionSave.getUserSession(Comman.USER_RESIDENTIAL_ADDRESS,getActivity()).equalsIgnoreCase(""))
        {
            et_residential_address_register.setText(SessionSave.getUserSession(Comman.USER_RESIDENTIAL_ADDRESS,getActivity()));
        }
        if (SessionSave.getUserSession(Comman.USER_SUB_URB,getActivity())!= null && !SessionSave.getUserSession(Comman.USER_SUB_URB,getActivity()).equalsIgnoreCase(""))
        {
            et_sb_urb_register.setText(SessionSave.getUserSession(Comman.USER_SUB_URB,getActivity()));
        }

        if (SessionSave.getUserSession(Comman.USER_POST_CODE,getActivity())!= null && !SessionSave.getUserSession(Comman.USER_POST_CODE,getActivity()).equalsIgnoreCase(""))
        {
            et_post_code_register.setText(SessionSave.getUserSession(Comman.USER_POST_CODE,getActivity()));
        }

        if (SessionSave.getUserSession(Comman.USER_CITY,getActivity())!= null && !SessionSave.getUserSession(Comman.USER_CITY,getActivity()).equalsIgnoreCase(""))
        {
            et_city_register.setText(SessionSave.getUserSession(Comman.USER_CITY,getActivity()));
        }

        if (SessionSave.getUserSession(Comman.USER_COUNTRY,getActivity())!= null && !SessionSave.getUserSession(Comman.USER_COUNTRY,getActivity()).equalsIgnoreCase(""))
        {
            et_country_register.setText(SessionSave.getUserSession(Comman.USER_COUNTRY,getActivity()));
        }

        if (SessionSave.getUserSession(Comman.USER_STATE,getActivity())!= null && !SessionSave.getUserSession(Comman.USER_STATE,getActivity()).equalsIgnoreCase(""))
        {
            et_state_register.setText(SessionSave.getUserSession(Comman.USER_STATE,getActivity()));
        }



        if (SessionSave.getUserSession(Comman.USER_REFERRAL_CODE,getActivity())!= null && !SessionSave.getUserSession(Comman.USER_REFERRAL_CODE,getActivity()).equalsIgnoreCase(""))
        {
            et_invite_code_register.setText(SessionSave.getUserSession(Comman.USER_REFERRAL_CODE,getActivity()));
        }
        if (SessionSave.getUserSession(Comman.USER_BANK_ACCOUNT_HOLDER_NAME,getActivity())!= null && !SessionSave.getUserSession(Comman.USER_BANK_ACCOUNT_HOLDER_NAME,getActivity()).equalsIgnoreCase(""))
        {
            et_accountHolder_Name_register.setText(SessionSave.getUserSession(Comman.USER_BANK_ACCOUNT_HOLDER_NAME,getActivity()));
        }
        if (SessionSave.getUserSession(Comman.USER_ABN,getActivity())!= null && !SessionSave.getUserSession(Comman.USER_ABN,getActivity()).equalsIgnoreCase(""))
        {
            et_Abn_register.setText(SessionSave.getUserSession(Comman.USER_ABN,getActivity()));
        }

        if (SessionSave.getUserSession(Comman.USER_BANK_NAME,getActivity())!= null && !SessionSave.getUserSession(Comman.USER_BANK_NAME,getActivity()).equalsIgnoreCase(""))
        {
            et_BankName_register.setText(SessionSave.getUserSession(Comman.USER_BANK_NAME,getActivity()));
        }
        if (SessionSave.getUserSession(Comman.USER_BANK_AC_NO,getActivity())!= null && !SessionSave.getUserSession(Comman.USER_BANK_AC_NO,getActivity()).equalsIgnoreCase(""))
        {
            et_BankAccountNo_register.setText(SessionSave.getUserSession(Comman.USER_BANK_AC_NO,getActivity()));
        }
        if (SessionSave.getUserSession(Comman.USER_BSB,getActivity())!= null && !SessionSave.getUserSession(Comman.USER_BSB,getActivity()).equalsIgnoreCase(""))
        {
            et_Bsb_register.setText(SessionSave.getUserSession(Comman.USER_BSB,getActivity()));
        }
        if (SessionSave.getUserSession(Comman.USER_GENDER,getActivity())!= null && !SessionSave.getUserSession(Comman.USER_GENDER,getActivity()).equalsIgnoreCase(""))
        {
            if (SessionSave.getUserSession(Comman.USER_GENDER,getActivity()).equalsIgnoreCase(MALE))
            {
                selectMale();
            }
            else if (SessionSave.getUserSession(Comman.USER_GENDER,getActivity()).equalsIgnoreCase(FEMALE))
            {
                selectFemale();
            }
            else
            {
                selectOthers();
            }
        }




        checkBox_male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               selectMale();
            }
        });
        checkBox_Female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              selectFemale();
            }
        });
        checkBox_others.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               selectOthers();
            }
        });

        tv_next.setOnClickListener(this);
        iv_profile_register.setOnClickListener(this);


        return rootView;
    }

    public void selectMale()
    {
        chackBox_Selected = MALE;
        checkBox_male.setChecked(true);
        checkBox_Female.setChecked(false);
        checkBox_others.setChecked(false);
    }
    public void selectFemale()
    {
        chackBox_Selected = FEMALE;
        checkBox_male.setChecked(false);
        checkBox_Female.setChecked(true);
        checkBox_others.setChecked(false);
    }
    public void selectOthers()
    {
        chackBox_Selected = OTHERS;
        checkBox_male.setChecked(false);
        checkBox_Female.setChecked(false);
        checkBox_others.setChecked(true);
    }

    private void CheckUserData()
    {
        if (Profile_ByteImage==null)
        {
            new SnackbarUtils(main_layout, getString(R.string.please_select_profile_pic),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else if (TextUtils.isEmpty(et_full_name.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_full_name),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        /*else  if (TextUtils.isEmpty(et_mobile_register.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_mobile_number),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }*/
        else  if (TextUtils.isEmpty(et_password_register.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_password),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_residential_address_register.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_residential_address),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_sb_urb_register.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_sub_arb),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_post_code_register.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_post_code),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_city_register.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_city),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_country_register.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_country),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_state_register.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_state),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_accountHolder_Name_register.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_company_holder_name),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_Abn_register.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_abn),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_serviceDescription.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_service_description),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_BankName_register.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_bank_name),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_BankAccountNo_register.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_bank_no),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_Bsb_register.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_bsb),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else
        {
            try
            {
                JSONObject jsonObj = new JSONObject(SessionSave.getUserSession(Comman.USER_COMPANY_ID_ARRAY,getActivity()));
                JSONArray jsonArray = jsonObj.getJSONArray(Comman.USER_COMPANY_ARRAY_NAME_FOR_STORE);
                for (int i=0; i<jsonArray.length(); i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    String CompanyId = jsonObject.getString(Comman.USER_COMPANY_ID_FOR_STORE);
                    String CompanyName = jsonObject.getString(Comman.USER_COMPANY_NAME_FOR_STORE);
                    if (String.valueOf(spinner_CompanyId.getSelectedItem())!=null && !String.valueOf(spinner_CompanyId.getSelectedItem()).equalsIgnoreCase("")
                            && String.valueOf(spinner_CompanyId.getSelectedItem()).equalsIgnoreCase(CompanyName))
                    {
                        SessionSave.saveUserSession(Comman.USER_COMPANY_ID,CompanyId, getActivity());
                        Log.e("selected ", " :"+String.valueOf(spinner_CompanyId.getSelectedItem()+" : "+CompanyId));
                    }
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
                Log.e("gxthjdfr","JSONException :"+e.getMessage().toString());
            }

            SessionSave.saveUserSession(Comman.USER_FULL_NAME, et_full_name.getText().toString(),getActivity());
            //SessionSave.saveUserSession(Comman.USER_MOBILE_NUMBER, et_mobile_register.getText().toString(),getActivity());
            SessionSave.saveUserSession(Comman.USER_PASSWORD, et_password_register.getText().toString(),getActivity());
            SessionSave.saveUserSession(Comman.USER_RESIDENTIAL_ADDRESS, et_residential_address_register.getText().toString(),getActivity());
            SessionSave.saveUserSession(Comman.USER_SUB_URB, et_sb_urb_register.getText().toString(),getActivity());
            SessionSave.saveUserSession(Comman.USER_POST_CODE, et_post_code_register.getText().toString(),getActivity());
            SessionSave.saveUserSession(Comman.USER_CITY, et_city_register.getText().toString(),getActivity());
            SessionSave.saveUserSession(Comman.USER_COUNTRY, et_country_register.getText().toString(),getActivity());
            SessionSave.saveUserSession(Comman.USER_STATE, et_state_register.getText().toString(),getActivity());
            SessionSave.saveUserSession(Comman.USER_GENDER, chackBox_Selected,getActivity());

            if (et_invite_code_register.getText().toString()!=null && !et_invite_code_register.getText().toString().equalsIgnoreCase(""))
            {
                SessionSave.saveUserSession(Comman.USER_REFERRAL_CODE, et_invite_code_register.getText().toString(),getActivity());
            }
            else
            {
                SessionSave.saveUserSession(Comman.USER_REFERRAL_CODE, "",getActivity());
            }

            SessionSave.saveUserSession(Comman.USER_BANK_ACCOUNT_HOLDER_NAME, et_accountHolder_Name_register.getText().toString(),getActivity());
            SessionSave.saveUserSession(Comman.USER_ABN, et_Abn_register.getText().toString(),getActivity());
            SessionSave.saveUserSession(Comman.USER_SERVICE_DESCRIPTION, et_serviceDescription.getText().toString(),getActivity());
            SessionSave.saveUserSession(Comman.USER_BANK_NAME, et_BankName_register.getText().toString(),getActivity());
            SessionSave.saveUserSession(Comman.USER_BANK_AC_NO, et_BankAccountNo_register.getText().toString(),getActivity());
            SessionSave.saveUserSession(Comman.USER_BSB, et_Bsb_register.getText().toString(),getActivity());

            SessionSave.saveUserSession(Comman.REGISTER_PROFILE_ACTIVITY, "1",getActivity());

            ((Registration_Tab_Activity)getActivity()).setTabCarInfo1();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.iv_profile_register:
                ShowPictureDialog();
                break;

            case R.id.tv_next:
                CheckUserData();
                break;
        }
    }

    private void ShowPictureDialog()
    {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            requestPermissions(new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_REQUEST_CODE);
        }
        else
        {
            AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getActivity());
            pictureDialog.setTitle("Select Action");
            String[] pictureDialogItems = {"Select photo from gallery", "Capture photo from camera" };
            pictureDialog.setItems(pictureDialogItems,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:
                                    choosePhotoFromGallary();
                                    break;
                                case 1:
                                    takePhotoFromCamera();
                                    break;
                            }
                        }
                    });
            pictureDialog.show();
        }
    }

    public void choosePhotoFromGallary()
    {
        userChoosenTask =chooseGallery;
        Intent intent_gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent_gallery, GALLERY);
    }

    private void takePhotoFromCamera()
    {
        userChoosenTask = chooseCamera;
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "NewPicture");
        imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        Log.e("imageUri", "imageUri11111111111111 : "+imageUri);
        startActivityForResult(intent, CAMERA);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.e("showPictureDialog", "onRequestPermissionsResult requestCode "+requestCode);
        switch (requestCode) {
            case MY_REQUEST_CODE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED)
                {
                    ShowPictureDialog();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK)
        {
            if (requestCode == GALLERY)
            {
                onSelectFromGalleryResult(data);
            }
            else if (requestCode == CAMERA)
            {
                onCaptureImageResult();
            }
            else
            {
                Profile_ByteImage=null;
            }
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data)
    {
        Bitmap bitmap= null;
        try
        {
            bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
        }
        catch (IOException e) {
            e.printStackTrace();
            Log.e("MediaStore","MediaStore");
        }

        bitmapImage = getResizedBitmap(bitmap,400);

        if (bitmapImage!=null)
        {
            Picasso.with(getActivity())
                    .load(data.getData())
                    .fit()
                    .transform(mTransformation)
                    .into(iv_profile_register);
            Profile_ByteImage = ConvertToByteArray(bitmapImage);

            encodedImage = Base64.encodeToString(Profile_ByteImage, Base64.DEFAULT);
            SessionSave.saveUserSession(Comman.USER_PROFILE_IMAGE, encodedImage,getActivity());
        }
        else
        {
            Profile_ByteImage=null;
            SessionSave.saveUserSession(Comman.USER_PROFILE_IMAGE, "",getActivity());

            new SnackbarUtils(main_layout, getString(R.string.please_select_profile_again),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
    }

    private void onCaptureImageResult()
    {
        try
        {
            Log.e("imageUri", "imageUri2222222222222222222 : "+imageUri);
            Bitmap thumbnail = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageUri);
            Log.e("#########","bmp height = "+thumbnail.getHeight());
            Log.e("#########","bmp width = "+thumbnail.getWidth());
            thumbnail = getResizedBitmap(thumbnail,400);
            bitmapImage=thumbnail;

            if (thumbnail!=null)
            {
                Picasso.with(getActivity())
                        .load(imageUri)
                        .fit()
                        .transform(mTransformation)
                        .into(iv_profile_register);
                Profile_ByteImage = ConvertToByteArray(thumbnail);

                encodedImage = Base64.encodeToString(Profile_ByteImage, Base64.DEFAULT);
                SessionSave.saveUserSession(Comman.USER_PROFILE_IMAGE, encodedImage,getActivity());
            }
            else
            {
                iv_profile_register.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.prof_image_demo));
                SessionSave.saveUserSession(Comman.USER_PROFILE_IMAGE, "",getActivity());
                Profile_ByteImage=null;
                new SnackbarUtils(main_layout, getString(R.string.please_select_profile_again),
                        ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
            }
        }
        catch (Exception e)
        {
            iv_profile_register.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.prof_image_demo));
            SessionSave.saveUserSession(Comman.USER_PROFILE_IMAGE, "",getActivity());
            Profile_ByteImage=null;
            Log.e("call","exception = "+e.getMessage());
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public byte[] ConvertToByteArray(Bitmap bmp)
    {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 60, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public static Bitmap decodeBase64(String input)
    {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
}
