package iiride.app.driver.Fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import iiride.app.driver.Activity.Drawer_Activity;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.R;


import java.util.List;


public class MyJob_Fragment extends Fragment {

    CardView cv_past_jobs, cv_dispatch_jobs, cv_future_booking, cv_pending_job;
    public static LinearLayout ll_main_view;
    public static FrameLayout frame_myjob;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_myjob, container, false);

        cv_past_jobs = (CardView) rootView.findViewById(R.id.cv_past_jobs);
        cv_dispatch_jobs = (CardView) rootView.findViewById(R.id.cv_dispatch_jobs);
        cv_future_booking = (CardView) rootView.findViewById(R.id.cv_future_booking);
        cv_pending_job = (CardView) rootView.findViewById(R.id.cv_pending_job);
        ll_main_view = (LinearLayout) rootView.findViewById(R.id.ll_main_view);
        frame_myjob = (FrameLayout) rootView.findViewById(R.id.frame_myjob);

        ll_main_view.setVisibility(View.VISIBLE);
        frame_myjob.setVisibility(View.GONE);

        cv_dispatch_jobs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Drawer_Activity)getActivity()).setTitleOfScreenToolbar("Dispatched Jobs");
                if (ll_main_view!=null)
                {
                    ll_main_view.setVisibility(View.GONE);
                }
                if (frame_myjob!=null)
                {
                    frame_myjob.setVisibility(View.VISIBLE);
                }


                Dispatched_Job_List_Fragment homeFragment = new Dispatched_Job_List_Fragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame_myjob, homeFragment);
                fragmentTransaction.commitAllowingStateLoss();
            }
        });

        cv_future_booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Drawer_Activity)getActivity()).setTitleOfScreenToolbar("Future Booking");
                if (ll_main_view!=null)
                {
                    ll_main_view.setVisibility(View.GONE);
                }
                if (frame_myjob!=null)
                {
                    frame_myjob.setVisibility(View.VISIBLE);
                }

                Future_JobBooking_List_Fragment homeFragment = new Future_JobBooking_List_Fragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame_myjob, homeFragment);
                fragmentTransaction.commitAllowingStateLoss();

            }
        });

        cv_pending_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Drawer_Activity)getActivity()).setTitleOfScreenToolbar("Scheduled Bookings");

                if (ll_main_view!=null)
                {
                    ll_main_view.setVisibility(View.GONE);
                }
                if (frame_myjob!=null)
                {
                    frame_myjob.setVisibility(View.VISIBLE);
                }

                Pending_Job_List_Fragment homeFragment = new Pending_Job_List_Fragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame_myjob, homeFragment);
                fragmentTransaction.commitAllowingStateLoss();
            }
        });

        cv_past_jobs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((Drawer_Activity)getActivity()).setTitleOfScreenToolbar("Past Jobs");
                if (ll_main_view!=null)
                {
                    ll_main_view.setVisibility(View.GONE);
                }
                if (frame_myjob!=null)
                {
                    frame_myjob.setVisibility(View.VISIBLE);
                }

                Past_Job_List_Fragment homeFragment = new Past_Job_List_Fragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame_myjob, homeFragment);
                fragmentTransaction.commitAllowingStateLoss();
            }
        });

        String NotifiFlag = SessionSave.getUserSession(Comman.NOTIFICATION_PUT_EXTRA, Drawer_Activity.activity);
        if (NotifiFlag!=null && !NotifiFlag.equalsIgnoreCase(""))
        {
            if (NotifiFlag.equalsIgnoreCase("BookLaterDriverNotify"))
            {
                ((Drawer_Activity)getActivity()).setTitleOfScreenToolbar("Future Booking");
                if (ll_main_view!=null)
                {
                    ll_main_view.setVisibility(View.GONE);
                }
                if (frame_myjob!=null)
                {
                    frame_myjob.setVisibility(View.VISIBLE);
                }

                Future_JobBooking_List_Fragment homeFragment = new Future_JobBooking_List_Fragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame_myjob, homeFragment);
                fragmentTransaction.commitAllowingStateLoss();
                SessionSave.saveUserSession(Comman.NOTIFICATION_PUT_EXTRA, "", Drawer_Activity.activity);
            }
            else if (NotifiFlag.equalsIgnoreCase("RejectDispatchJobRequest"))
            {
                ((Drawer_Activity)getActivity()).setTitleOfScreenToolbar("Past Jobs");
                if (ll_main_view!=null)
                {
                    ll_main_view.setVisibility(View.GONE);
                }
                if (frame_myjob!=null)
                {
                    frame_myjob.setVisibility(View.VISIBLE);
                }

                Past_Job_List_Fragment homeFragment = new Past_Job_List_Fragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame_myjob, homeFragment);
                fragmentTransaction.commitAllowingStateLoss();
                SessionSave.saveUserSession(Comman.NOTIFICATION_PUT_EXTRA, "", Drawer_Activity.activity);
            }
            else if (NotifiFlag.equalsIgnoreCase("RejectBooking"))
            {
                ((Drawer_Activity)getActivity()).setTitleOfScreenToolbar("Past Jobs");
                if (ll_main_view!=null)
                {
                    ll_main_view.setVisibility(View.GONE);
                }
                if (frame_myjob!=null)
                {
                    frame_myjob.setVisibility(View.VISIBLE);
                }

                Past_Job_List_Fragment homeFragment = new Past_Job_List_Fragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame_myjob, homeFragment);
                fragmentTransaction.commitAllowingStateLoss();
                SessionSave.saveUserSession(Comman.NOTIFICATION_PUT_EXTRA, "", Drawer_Activity.activity);
            }
            else if (NotifiFlag.equalsIgnoreCase("CancelRequest"))
            {
                ((Drawer_Activity)getActivity()).setTitleOfScreenToolbar("Past Jobs");
                if (ll_main_view!=null)
                {
                    ll_main_view.setVisibility(View.GONE);
                }
                if (frame_myjob!=null)
                {
                    frame_myjob.setVisibility(View.VISIBLE);
                }

                Past_Job_List_Fragment homeFragment = new Past_Job_List_Fragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame_myjob, homeFragment);
                fragmentTransaction.commitAllowingStateLoss();
                SessionSave.saveUserSession(Comman.NOTIFICATION_PUT_EXTRA, "", Drawer_Activity.activity);
            }
        }

        return rootView;
    }


    public static void Call_SubFragment()
    {
        String NotifiFlag = SessionSave.getUserSession(Comman.NOTIFICATION_PUT_EXTRA, Drawer_Activity.activity);
        if (NotifiFlag!=null && !NotifiFlag.equalsIgnoreCase(""))
        {
            if (NotifiFlag.equalsIgnoreCase("BookLaterDriverNotify"))
            {
                Drawer_Activity.activity.setTitleOfScreenToolbar("Future Booking");
                if (ll_main_view!=null)
                {
                    ll_main_view.setVisibility(View.GONE);
                }
                if (frame_myjob!=null)
                {
                    frame_myjob.setVisibility(View.VISIBLE);
                }

                Future_JobBooking_List_Fragment homeFragment = new Future_JobBooking_List_Fragment();
                FragmentTransaction fragmentTransaction = Drawer_Activity.activity.getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame_myjob, homeFragment);
                fragmentTransaction.commitAllowingStateLoss();
                SessionSave.saveUserSession(Comman.NOTIFICATION_PUT_EXTRA, "", Drawer_Activity.activity);
            }
            else if (NotifiFlag.equalsIgnoreCase("RejectDispatchJobRequest"))
            {
                Drawer_Activity.activity.setTitleOfScreenToolbar("Past Jobs");
                if (ll_main_view!=null)
                {
                    ll_main_view.setVisibility(View.GONE);
                }
                if (frame_myjob!=null)
                {
                    frame_myjob.setVisibility(View.VISIBLE);
                }

                Past_Job_List_Fragment homeFragment = new Past_Job_List_Fragment();
                FragmentTransaction fragmentTransaction = Drawer_Activity.activity.getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame_myjob, homeFragment);
                fragmentTransaction.commitAllowingStateLoss();
                SessionSave.saveUserSession(Comman.NOTIFICATION_PUT_EXTRA, "", Drawer_Activity.activity);
            }
            else if (NotifiFlag.equalsIgnoreCase("RejectBooking"))
            {
                Drawer_Activity.activity.setTitleOfScreenToolbar("Past Jobs");
                if (ll_main_view!=null)
                {
                    ll_main_view.setVisibility(View.GONE);
                }
                if (frame_myjob!=null)
                {
                    frame_myjob.setVisibility(View.VISIBLE);
                }

                Past_Job_List_Fragment homeFragment = new Past_Job_List_Fragment();
                FragmentTransaction fragmentTransaction = Drawer_Activity.activity.getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame_myjob, homeFragment);
                fragmentTransaction.commitAllowingStateLoss();
                SessionSave.saveUserSession(Comman.NOTIFICATION_PUT_EXTRA, "", Drawer_Activity.activity);
            }
            else if (NotifiFlag.equalsIgnoreCase("CancelRequest"))
            {
                Drawer_Activity.activity.setTitleOfScreenToolbar("Past Jobs");
                if (ll_main_view!=null)
                {
                    ll_main_view.setVisibility(View.GONE);
                }
                if (frame_myjob!=null)
                {
                    frame_myjob.setVisibility(View.VISIBLE);
                }

                Past_Job_List_Fragment homeFragment = new Past_Job_List_Fragment();
                FragmentTransaction fragmentTransaction = Drawer_Activity.activity.getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame_myjob, homeFragment);
                fragmentTransaction.commitAllowingStateLoss();
                SessionSave.saveUserSession(Comman.NOTIFICATION_PUT_EXTRA, "", Drawer_Activity.activity);
            }
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden)
        {
            String NotifiFlag = SessionSave.getUserSession(Comman.NOTIFICATION_PUT_EXTRA, Drawer_Activity.activity);
            if (NotifiFlag!=null && !NotifiFlag.equalsIgnoreCase(""))
            {
                if (NotifiFlag.equalsIgnoreCase("BookLaterDriverNotify"))
                {
                    ((Drawer_Activity)getActivity()).setTitleOfScreenToolbar("Future Booking");
                    if (ll_main_view!=null)
                    {
                        ll_main_view.setVisibility(View.GONE);
                    }
                    if (frame_myjob!=null)
                    {
                        frame_myjob.setVisibility(View.VISIBLE);
                    }

                    Future_JobBooking_List_Fragment homeFragment = new Future_JobBooking_List_Fragment();
                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                    fragmentTransaction.replace(R.id.frame_myjob, homeFragment);
                    fragmentTransaction.commitAllowingStateLoss();
                    SessionSave.saveUserSession(Comman.NOTIFICATION_PUT_EXTRA, "", Drawer_Activity.activity);
                }
                else if (NotifiFlag.equalsIgnoreCase("RejectDispatchJobRequest"))
                {
                    ((Drawer_Activity)getActivity()).setTitleOfScreenToolbar("Past Jobs");
                    if (ll_main_view!=null)
                    {
                        ll_main_view.setVisibility(View.GONE);
                    }
                    if (frame_myjob!=null)
                    {
                        frame_myjob.setVisibility(View.VISIBLE);
                    }

                    Past_Job_List_Fragment homeFragment = new Past_Job_List_Fragment();
                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                    fragmentTransaction.replace(R.id.frame_myjob, homeFragment);
                    fragmentTransaction.commitAllowingStateLoss();
                    SessionSave.saveUserSession(Comman.NOTIFICATION_PUT_EXTRA, "", Drawer_Activity.activity);
                }
                else if (NotifiFlag.equalsIgnoreCase("RejectBooking"))
                {
                    ((Drawer_Activity)getActivity()).setTitleOfScreenToolbar("Past Jobs");
                    if (ll_main_view!=null)
                    {
                        ll_main_view.setVisibility(View.GONE);
                    }
                    if (frame_myjob!=null)
                    {
                        frame_myjob.setVisibility(View.VISIBLE);
                    }

                    Past_Job_List_Fragment homeFragment = new Past_Job_List_Fragment();
                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                    fragmentTransaction.replace(R.id.frame_myjob, homeFragment);
                    fragmentTransaction.commitAllowingStateLoss();
                    SessionSave.saveUserSession(Comman.NOTIFICATION_PUT_EXTRA, "", Drawer_Activity.activity);
                }
                else if (NotifiFlag.equalsIgnoreCase("CancelRequest"))
                {
                    ((Drawer_Activity)getActivity()).setTitleOfScreenToolbar("Past Jobs");
                    if (ll_main_view!=null)
                    {
                        ll_main_view.setVisibility(View.GONE);
                    }
                    if (frame_myjob!=null)
                    {
                        frame_myjob.setVisibility(View.VISIBLE);
                    }

                    Past_Job_List_Fragment homeFragment = new Past_Job_List_Fragment();
                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                    fragmentTransaction.replace(R.id.frame_myjob, homeFragment);
                    fragmentTransaction.commitAllowingStateLoss();
                    SessionSave.saveUserSession(Comman.NOTIFICATION_PUT_EXTRA, "", Drawer_Activity.activity);
                }
            }
        }
    }

    public void setMainLayoutVisible()
    {
        if (ll_main_view!=null)
        {
            ll_main_view.setVisibility(View.VISIBLE);
        }
        if (frame_myjob!=null)
        {
            frame_myjob.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 0) {
            List<Fragment> fragments = getActivity().getSupportFragmentManager().getFragments();
            if (fragments != null) {
                for (Fragment fragment : fragments) {
                    if(fragment instanceof Pending_Job_List_Fragment)
                    {
                        fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
                    }
                }
            }
        }
    }
}
