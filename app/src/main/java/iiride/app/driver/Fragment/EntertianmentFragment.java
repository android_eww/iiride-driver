package iiride.app.driver.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import iiride.app.driver.Activity.WebViewActivity;
import iiride.app.driver.R;


public class EntertianmentFragment extends Fragment implements View.OnClickListener {

    private LinearLayout ll_movies, ll_groceries, ll_retial, ll_discount, ll_giftcard, ll_festival;
    private String title, url;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View rootView = inflater.inflate(R.layout.fragment_entertianment, container,false);

        ll_movies = (LinearLayout) rootView.findViewById(R.id.ll_movies);
        ll_groceries = (LinearLayout) rootView.findViewById(R.id.ll_groceries);
        ll_retial = (LinearLayout) rootView.findViewById(R.id.ll_retial);
        ll_discount = (LinearLayout) rootView.findViewById(R.id.ll_discount);
        ll_giftcard = (LinearLayout) rootView.findViewById(R.id.ll_giftcard);
        ll_festival = (LinearLayout) rootView.findViewById(R.id.ll_festival);


        ll_movies.setOnClickListener(this);
        ll_groceries.setOnClickListener(this);
        ll_retial.setOnClickListener(this);
        ll_discount.setOnClickListener(this);
        ll_giftcard.setOnClickListener(this);
        ll_festival.setOnClickListener(this);


        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.ll_movies:

                url = "https://www.hoyts.com.au/";
                title = "MOVIES";

                intentWebview(url, title);

                break;

            case R.id.ll_groceries:

                url = "https://www.coles.com.au/";
                title = "GROCERIES";

                intentWebview(url, title);


                break;

            case R.id.ll_retial:

                url = "https://www.chadstone.com.au/";
                title = "RETAIL";

                intentWebview(url, title);
                break;

            case R.id.ll_discount:

                url = "https://www.chadstone.com.au/whats-on";
                title = "DISCOUNT";

                intentWebview(url, title);
                break;

            case R.id.ll_giftcard:

                url = "https://www.chadstone.com.au/services-facilities/services/gift-card";
                title = "GIFT CARD";

                intentWebview(url, title);
                break;

            case R.id.ll_festival:

                url = "https://www.festival.melbourne/2017/";
                title = "FESTIVAL";

                intentWebview(url, title);
                break;
        }

    }

    private void intentWebview(String url, String title)
    {
        Log.e("call", "Title:- " + title);
        Log.e("call", "Url:- " + url);

//        Wallet__Activity.resumeFlag = 0;

        Intent intent = new Intent(getActivity(), WebViewActivity.class);
        intent.putExtra("url",url);
        intent.putExtra("title",title);
        startActivity(intent);
        ((Activity)getActivity()).overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }
}
