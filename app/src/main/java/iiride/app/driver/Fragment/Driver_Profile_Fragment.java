package iiride.app.driver.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import iiride.app.driver.Activity.Drawer_Activity;
import iiride.app.driver.R;


public class Driver_Profile_Fragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_driver_profile, container, false);

        ((Drawer_Activity)getActivity()).setTitleOfScreenToolbar("Profile");

        return rootView;
    }
}
