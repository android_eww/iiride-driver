package iiride.app.driver.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;


import iiride.app.driver.Activity.WeeklyEarninigsActivity;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.R;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.formatter.YAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;


import java.text.DateFormatSymbols;
import java.util.ArrayList;



public class TiCKPAYEarningFragment extends Fragment {

    private BarChart barChart;
    int colors[]  = {R.color.chartBar1, R.color.chartBar2,R.color.chartBar3,R.color.chartBar4,R.color.chartBar5,R.color.chartBar6, R.color.chartBar7};
    Button btnPress;
    RelativeLayout layoutChart;
    ArrayList<String> labels;
    ArrayList<BarEntry> entries;
    TextView tv_date, weeklyTotal, tv_WalletBallence;

    Float weeklyTotalF = 0f;


    //Overriden method onCreateView
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup view, Bundle savedInstanceState) {

        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes
        View rootView = inflater.inflate(R.layout.fragment_earning_tickpay, view, false);

        tv_date = (TextView) rootView.findViewById(R.id.tv_date);
        weeklyTotal = (TextView) rootView.findViewById(R.id.weeklyTotal);
        tv_WalletBallence = (TextView) rootView.findViewById(R.id.tv_WalletBallence);

        barChart = (BarChart) rootView.findViewById(R.id.barchart);
        btnPress = (Button) rootView.findViewById(R.id.btnPress);
        layoutChart = (RelativeLayout) rootView.findViewById(R.id.layoutChart);

        String walleteBallence = SessionSave.getUserSession(Comman.DRIVER_WALLET_BALLENCE,getActivity());
        if (walleteBallence!=null && !walleteBallence.equalsIgnoreCase(""))
        {
            tv_WalletBallence.setText("$"+String.format("%.2f", Float.parseFloat(walleteBallence)));
        }
        else
        {
            tv_WalletBallence.setVisibility(View.GONE);
        }


        entries = new ArrayList<>();
        labels  = new ArrayList<String>();
        weeklyTotalF = 0f;
        if (WeeklyEarninigsActivity.list_tickpay.size() > 0)
        {
            for (int i=0; i<WeeklyEarninigsActivity.list_tickpay.size(); i++)
            {
                entries.add(new BarEntry(Float.parseFloat(WeeklyEarninigsActivity.list_tickpay.get(i).getDayValue()), i));
                weeklyTotalF = weeklyTotalF + Float.parseFloat(WeeklyEarninigsActivity.list_tickpay.get(i).getDayValue());
                labels.add(WeeklyEarninigsActivity.list_tickpay.get(i).getDayName());
            }
        }

        weeklyTotal.setText("$"+String.format("%.2f", weeklyTotalF));
        BarDataSet bardataset = new BarDataSet(entries, "WeeksColor");

        String startDate = SessionSave.getUserSession(Comman.USER_PREFERENCE_WEEKLY_EARNING_START_DATE, WeeklyEarninigsActivity.activity);
        String endDate = SessionSave.getUserSession(Comman.USER_PREFERENCE_WEEKLY_EARNING_END_DATE, WeeklyEarninigsActivity.activity);

        //2017-12-09  //23rd Oct - 29th Oct
        if (startDate!=null && !startDate.equalsIgnoreCase(""))
        {
            String[] startDateArry= startDate.split("-");
            if (startDateArry.length>1)
            {
                String monthString = new DateFormatSymbols().getMonths()[(Integer.parseInt(startDateArry[1]))-1];
                tv_date.setText(Integer.parseInt(startDateArry[2]) + getDayOfMonthSuffix(Integer.parseInt(startDateArry[2]))+ " "+ monthString.substring(0,3));
            }
        }
        if (endDate!=null && !endDate.equalsIgnoreCase(""))
        {
            String[] startDateArry= endDate.split("-");
            if (startDateArry.length>1)
            {
                String monthString = new DateFormatSymbols().getMonths()[(Integer.parseInt(startDateArry[1]))-1];
                tv_date.append(" - "+Integer.parseInt(startDateArry[2]) + getDayOfMonthSuffix(Integer.parseInt(startDateArry[2]))+ " "+ monthString.substring(0,3));
            }
        }
        Log.e("kkkkkkkkk","kkkkkkkkkkk : "+tv_date.getText().toString());

        /*entries = new ArrayList<>();
        entries.add(new BarEntry(100, 0));
        entries.add(new BarEntry(150, 1));
        entries.add(new BarEntry(200, 2));
        entries.add(new BarEntry(250, 3));
        entries.add(new BarEntry(300, 4));
        *//*entries.add(new BarEntry(152, 5));
        entries.add(new BarEntry(100, 6));*//*

        BarDataSet bardataset = new BarDataSet(entries, "WeeksColor");


        labels  = new ArrayList<String>();

        labels.add("Monday");
        labels.add("Tuesday");
        labels.add("Wednesday");
        labels.add("Thursday");
        labels.add("Friday");*/
            /*labels.add("Saturday");
            labels.add("Sunday");*/

        BarData data = new BarData(labels,bardataset);

        bardataset.setColors(ColorTemplate.createColors(getResources(),colors));
        bardataset.setValueTextColor(getResources().getColor(R.color.chartText));
        bardataset.setValueTextSize(8);
        bardataset.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                return ("$"+value);
            }
        });

        barChart.getAxisLeft().setAxisMinValue(0);
        barChart.setPinchZoom(false);
        barChart.setTouchEnabled(false);
        barChart.setData(data); // set the data and list of lables into chart
        barChart.setDescription("");  // set the description
        barChart.animateY(3000);
        barChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM); // x axis lable set bottom
        barChart.getLegend().setEnabled(false); //legend Hide
        barChart.getLegend().setTextColor(getResources().getColor(R.color.chartText)); //Legend Color change
        barChart.getAxisRight().setEnabled(false); // Right Axis Text Disable
        barChart.getXAxis().setTextSize(7f); // X Axis Text size
        barChart.getAxisLeft().setTextSize(7f);  // Y Left Axis Size
        barChart.getXAxis().setTextColor(getResources().getColor(R.color.chartText)); // X Axis label color
        barChart.getAxisLeft().setTextColor(getResources().getColor(R.color.chartText)); // Y Axis Color
        barChart.getXAxis().setGridColor(getResources().getColor(R.color.chartAxisColor)); // x Axis Color
        barChart.getAxisLeft().setGridColor(getResources().getColor(R.color.chartAxisColor)); // Y Axis Left Color
        barChart.getAxisRight().setGridColor(getResources().getColor(R.color.chartAxisColor)); // Y Axis Right Color
        barChart.getAxisLeft().setValueFormatter(new YAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, YAxis yAxis) {
                return ("$"+value);
            }
        });

        return  rootView;
    }

    String getDayOfMonthSuffix(final int n) {
//        checkArgument(n >= 1 && n <= 31, "illegal day of month: " + n);
        if (n >= 11 && n <= 13) {
            return "th";
        }
        switch (n % 10) {
            case 1:  return "st";
            case 2:  return "nd";
            case 3:  return "rd";
            default: return "th";
        }
    }
}
