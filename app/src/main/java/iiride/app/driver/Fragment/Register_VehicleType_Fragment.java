package iiride.app.driver.Fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import iiride.app.driver.Activity.Registration_Tab_Activity;

import iiride.app.driver.Adapter.Register_vehi_deliv_Type_Adapter;
import iiride.app.driver.Been.Register_vehi_deliv_Type_Been;
import iiride.app.driver.Been.initData.InitData;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.Constants;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;


import iiride.app.driver.Others.GPSTracker;
import iiride.app.driver.Others.Utility;
import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Register_VehicleType_Fragment extends Fragment implements View.OnClickListener{


    public static String VEHICLE_TYPE = "car_and_texi", DELIVERY_TYPE = "delivery_service";

    LinearLayout  main_layout;
    ListView lv_vehicleType, lv_deliveryType;
    CheckBox cb_carTexi, cb_deliveryService;
    RelativeLayout rl_vehicleType, rl_deliveryType, rl_check_carTexi, rl_check_deliveryService, rl_carImage;
    TextView tv_deliveryType, tv_next_vehicleType;
    EditText et_vehiRegistrationNo, et_carCompany, et_carColor;
    ImageView iv_carImage;


    ArrayList<Register_vehi_deliv_Type_Been> list_vehicleItem = new ArrayList<>();
    Register_vehi_deliv_Type_Adapter adapter;

    DialogClass dialogClass;
    AQuery aQuery;

    public static String clickedType="";
    private static final int MY_REQUEST_CODE = 102;
    String chooseCamera="camera", chooseGallery="gallery", userChoosenTask="";
    private int GALLERY = 1, CAMERA = 0;
    byte[] DriverCar_ByteImage=null;
    Transformation mTransformation;
    String encodedImage;

    Uri mImageUri;
    private Uri imageUri;
    Bitmap bitmapImage;
    String picturePath;

    String cityName="",cityId="";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_vehicle_type_register, container, false);

        mImageUri=null;
        imageUri=null;
        bitmapImage=null;
        picturePath=null;

        Log.e("fragment","Register_VehicleType_Fragment");
        aQuery = new AQuery(getActivity());
        main_layout= (LinearLayout) rootView.findViewById(R.id.main_layout);
        rl_check_carTexi= (RelativeLayout) rootView.findViewById(R.id.rl_check_carTexi);
        rl_check_deliveryService= (RelativeLayout) rootView.findViewById(R.id.rl_check_deliveryService);
        rl_vehicleType = (RelativeLayout) rootView.findViewById(R.id.rl_vehicleType);
        rl_deliveryType = (RelativeLayout) rootView.findViewById(R.id.rl_deliveryType);

        et_vehiRegistrationNo = (EditText) rootView.findViewById(R.id.et_vehiRegistrationNo);
        et_carCompany = (EditText) rootView.findViewById(R.id.et_carCompany);
        et_carColor = (EditText) rootView.findViewById(R.id.et_carColor);

        rl_carImage = (RelativeLayout) rootView.findViewById(R.id.rl_carImage);
        iv_carImage = (ImageView) rootView.findViewById(R.id.iv_carImage);

        rl_vehicleType.setVisibility(View.GONE);
        rl_deliveryType.setVisibility(View.GONE);

        mTransformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(100)
                .borderColor(ContextCompat.getColor(getActivity(), R.color.colorGray))
                .borderWidthDp(0)
                .oval(true)
                .build();

        Registration_Tab_Activity registration_tab_activity = new Registration_Tab_Activity();
        registration_tab_activity.LogoutButton_Display(1);



        lv_vehicleType = (ListView) rootView.findViewById(R.id.lv_vehicle_type);
        lv_deliveryType = (ListView) rootView.findViewById(R.id.lv_deliveryType);


        cb_carTexi = (CheckBox) rootView.findViewById(R.id.cb_carTexi);
        cb_deliveryService = (CheckBox) rootView.findViewById(R.id.cb_deliveryService);

        tv_deliveryType = (TextView) rootView.findViewById(R.id.tv_deliveryType);
        tv_next_vehicleType = (TextView) rootView.findViewById(R.id.tv_next_vehicleType);

        adapter = new Register_vehi_deliv_Type_Adapter(getActivity(), list_vehicleItem, main_layout, clickedType);

        rl_carImage.setOnClickListener(this);
        iv_carImage.setOnClickListener(this);
        rl_check_carTexi.setOnClickListener(this);
        rl_check_deliveryService.setOnClickListener(this);
        tv_deliveryType.setOnClickListener(this);
        tv_next_vehicleType.setOnClickListener(this);

//        if (SessionSave.getUserSession(Comman.USER_VEHICLE_IMAGE,getActivity())!=null && !SessionSave.getUserSession(Comman.USER_VEHICLE_IMAGE,getActivity()).equalsIgnoreCase(""))
//        {
//            Picasso.with(getActivity())
//                    .load(getImageUri(getActivity(),decodeBase64(SessionSave.getUserSession(Comman.USER_VEHICLE_IMAGE,getActivity()))))
//                    .fit()
//                    .transform(mTransformation)
//                    .into(iv_carImage);
//        }

        if (SessionSave.getUserSession(Comman.USER_VEHICLE_REGISTRATION_NO,getActivity())!= null && !SessionSave.getUserSession(Comman.USER_VEHICLE_REGISTRATION_NO,getActivity()).equalsIgnoreCase(""))
        {
            et_vehiRegistrationNo.setText(SessionSave.getUserSession(Comman.USER_VEHICLE_REGISTRATION_NO,getActivity()));
        }

        if (SessionSave.getUserSession(Comman.USER_CAR_COMPANY,getActivity())!= null && !SessionSave.getUserSession(Comman.USER_CAR_COMPANY,getActivity()).equalsIgnoreCase(""))
        {
            et_carCompany.setText(SessionSave.getUserSession(Comman.USER_CAR_COMPANY,getActivity()));
        }

        if (SessionSave.getUserSession(Comman.USER_CAR_COLOR,getActivity())!= null && !SessionSave.getUserSession(Comman.USER_CAR_COLOR,getActivity()).equalsIgnoreCase(""))
        {
            et_carColor.setText(SessionSave.getUserSession(Comman.USER_CAR_COLOR,getActivity()));
        }

        getCityName();

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.rl_check_carTexi:
                cb_carTexi.setChecked(true);
                cb_deliveryService.setChecked(false);
                clickedType = VEHICLE_TYPE;
                FindWhichTypeIs_Visible();
                break;

            case R.id.rl_check_deliveryService:
                cb_carTexi.setChecked(false);
                cb_deliveryService.setChecked(true);
                clickedType = DELIVERY_TYPE;
                FindWhichTypeIs_Visible();
                break;

            case R.id.tv_deliveryType:
                CheckData();
                break;

            case R.id.tv_next_vehicleType:
                CheckData();
                break;

            case R.id.rl_carImage:
                ShowPictureDialog();
                break;

            case R.id.iv_carImage:
                ShowPictureDialog();
                break;
        }
    }

    private void getCityName() {

        final InitData initData = SessionSave.getInitData(getActivity());
        GPSTracker gpsTracker = new GPSTracker(getActivity());
        /*if (gpsTracker.canGetLocation())
        {
            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
            List<Address> addresses = null;
            try {
                addresses = geocoder.getFromLocation(gpsTracker.getLatitude(), gpsTracker.getLongitude(), 1);
                //addresses = geocoder.getFromLocation(-37.946048, 145.085055, 1);
                cityName = addresses.get(0).getLocality();
                Log.e("TAG", "***** cityName = "+ cityName);
                if(initData != null && initData.getCityList() != null){
                    for(int  i = 0;i<initData.getCityList().size();i++){
                        if(initData.getCityList().get(i).getCityName().equalsIgnoreCase(cityName)){
                            cityId = initData.getCityList().get(i).getId();
                            break;
                        }
                    }
                }
                String stateName = addresses.get(0).getAddressLine(1);
                String countryName = addresses.get(0).getAddressLine(2);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {*/
      //  Log.w("Constants.newgpsLatitude",""+ Constants.newgpsLatitude);
       // Log.w("Constants.newgpsLatitude",""+ Constants.newgpsLongitude);
        if (!Constants.newgpsLatitude.equalsIgnoreCase("") && !Constants.newgpsLongitude.equalsIgnoreCase("") && !Constants.newgpsLatitude.equalsIgnoreCase("0") && !Constants.newgpsLongitude.equalsIgnoreCase("0")) {

            String urlLocation = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + Constants.newgpsLatitude + "," +  Constants.newgpsLongitude + "&key=" + getString(R.string.api_key_google_map);
            Log.e("urlLocation", "" + urlLocation);
            dialogClass = new DialogClass(getActivity(), 1);
            dialogClass.showDialog();
            aQuery.ajax(urlLocation.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {

                    try {
                        int responseCode = status.getCode();
                        Log.e("TAGCityFind", "responseCode = " + responseCode);
                        Log.e("TAGCityFind", "Response = " + json);
                        dialogClass.hideDialog();

                        JSONArray resultsAry = json.getJSONArray("results");
                        JSONObject jObj = new JSONObject(resultsAry.getString(0));
                        JSONArray addCompAry = jObj.getJSONArray("address_components");
                        String short_name = "";
                        int idxCity = -1;
                        boolean isContainsCity = false;
                        boolean isFoundCity = false;

                        for (int adComp = 0; adComp < addCompAry.length(); adComp++) {
                            JSONObject cityObj = addCompAry.getJSONObject(adComp);
                            JSONArray typesAry = cityObj.getJSONArray("types");

                            for (int j = 0; j < typesAry.length(); j++) {
                                if (typesAry.get(j).toString().equalsIgnoreCase("administrative_area_level_2")) {
                                    isContainsCity = true;
                                    idxCity = adComp;
                                    break;
                                }
                            }
                        }

                        for (int adComp = 0; adComp < addCompAry.length(); adComp++) {
                            JSONObject cityObj = addCompAry.getJSONObject(adComp);
                            JSONArray typesAry = cityObj.getJSONArray("types");
                            if(typesAry.length()>1) {
                                if (/*typesAry.getString(0).equalsIgnoreCase("administrative_area_level_2") */isContainsCity && adComp == idxCity && typesAry.getString(1).equalsIgnoreCase("political")) {
                                    cityName = cityObj.getString("long_name");
                                    short_name = cityObj.getString("short_name");
                                    for(int  i = 0;i<initData.getCityList().size();i++){
                                        String listCityName = initData.getCityList().get(i).getCityName().toLowerCase().trim();
                                        if(cityName.equalsIgnoreCase(listCityName)){
                                            isFoundCity =true;
                                            cityId = initData.getCityList().get(i).getId();
                                            break;
                                        }
                                        else if(short_name.equalsIgnoreCase(listCityName)){
                                            isFoundCity =true;
                                            cityName = short_name;
                                            cityId = initData.getCityList().get(i).getId();
                                            break;
                                        }
                                    }
                                    break;
                                }else if (!isContainsCity && typesAry.getString(0).equalsIgnoreCase("locality") && typesAry.getString(1).equalsIgnoreCase("political")) {
                                    // JSONObject cityObjFinal = new JSONObject(addCompAry.getString(1));
                                    cityName = cityObj.getString("long_name");
                                    for(int  i = 0;i<initData.getCityList().size();i++){
                                        String listCityName = initData.getCityList().get(i).getCityName().toLowerCase().trim();
                                        if(cityName.equalsIgnoreCase(listCityName)){
                                            isFoundCity =true;
                                            cityId = initData.getCityList().get(i).getId();
                                            break;
                                        }
                                        else if(short_name.equalsIgnoreCase(listCityName)){
                                            isFoundCity =true;
                                            cityName = short_name;
                                            cityId = initData.getCityList().get(i).getId();
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                        }

                        if(!isFoundCity){
                            cityName = "Melbourne";
                            cityId = "4";
                        }

                        Log.e("resultAry", "" + cityName);
                    } catch (JSONException e) {
                        Log.e("Exception", "Exception " + e.toString());
                        dialogClass.hideDialog();

                    }
                }

            }.method(AQuery.METHOD_GET));

        }

            /*if (cityName == null || cityName.equalsIgnoreCase("")) {
                String ip="";

                ip = Utility.getIPAddress(true);
                Log.e("TAG", "***** IP = "+ ip);// IPv4
                Log.e("TAG", "***** IP = "+ Utility.getIPAddress(false));// IPv6
                String url = "http://ip-api.com/json/"+ip;
                Log.e("TAG","URL = "+url);
                dialogClass = new DialogClass(getActivity(), 1);
                dialogClass.showDialog();
                aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

                    @Override
                    public void callback(String url, JSONObject json, AjaxStatus status) {

                        try
                        {
                            int responseCode = status.getCode();
                            Log.e("TAG", "responseCode = " + responseCode);
                            Log.e("TAG", "Response = " + json);
                            dialogClass.hideDialog();
                            cityName = json.optString("city");
                            if(initData != null && initData.getCityList() != null){
                                for(int  i = 0;i<initData.getCityList().size();i++){
                                    if(initData.getCityList().get(i).getCityName().equalsIgnoreCase(cityName)){
                                        cityId = initData.getCityList().get(i).getId();
                                        break;
                                    }
                                }
                            }
                            //{"status":"success","country":"India","countryCode":"IN","region":"GJ","regionName":"Gujarat","city":"Ahmedabad","zip":"380001","lat":23.0276,"lon":72.5871,"timezone":"Asia\/Kolkata","isp":"Reliance Jio Infocomm Limited","org":"RJIL Gujarat LTE SUBSCRIBER PUBLIC","as":"AS55836 Reliance Jio Infocomm Limited","query":"157.32.236.138"}
                        }
                        catch (Exception e)
                        {
                            Log.e("Exception","Exception "+e.toString());
                            dialogClass.hideDialog();

                        }
                    }

                }.method(AQuery.METHOD_GET));
            }*/
        //}
    }

    private void ShowPictureDialog()
    {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            requestPermissions(new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_REQUEST_CODE);
        }
        else
        {
            AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getActivity());
            pictureDialog.setTitle("Select Action");
            String[] pictureDialogItems = {"Select photo from gallery", "Capture photo from camera" };
            pictureDialog.setItems(pictureDialogItems,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:
                                    choosePhotoFromGallary();
                                    break;
                                case 1:
                                    takePhotoFromCamera();
                                    break;
                            }
                        }
                    });
            pictureDialog.show();
        }
    }

    public void choosePhotoFromGallary()
    {
        userChoosenTask =chooseGallery;
        Intent intent_gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent_gallery, GALLERY);
    }

    private void takePhotoFromCamera()
    {
        userChoosenTask = chooseCamera;
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "NewPicture");
        imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        Log.e("imageUri", "imageUri11111111111111 : "+imageUri);
        startActivityForResult(intent, CAMERA);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.e("showPictureDialog", "onRequestPermissionsResult requestCode "+requestCode);
        switch (requestCode) {
            case MY_REQUEST_CODE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED)
                {
                    ShowPictureDialog();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK)
        {
            if (requestCode == GALLERY)
            {
                onSelectFromGalleryResult(data);
            }
            else if (requestCode == CAMERA)
            {
                onCaptureImageResult();
            }
            else
            {
                DriverCar_ByteImage=null;
            }
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data)
    {
        Bitmap bitmap= null;
        try
        {
            bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
        }
        catch (IOException e) {
            e.printStackTrace();
            Log.e("MediaStore","MediaStore");
        }

        bitmapImage = getResizedBitmap(bitmap,400);

        if (bitmapImage!=null)
        {
            Picasso.with(getActivity())
                    .load(data.getData())
                    .fit()
                    .transform(mTransformation)
                    .into(iv_carImage);
            DriverCar_ByteImage = ConvertToByteArray(bitmapImage);

            encodedImage = Base64.encodeToString(DriverCar_ByteImage, Base64.DEFAULT);
            SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE, encodedImage,getActivity());
        }
        else
        {
            DriverCar_ByteImage=null;
            SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE, "",getActivity());

            new SnackbarUtils(main_layout, getString(R.string.please_select_vehicle_image),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
    }

    private void onCaptureImageResult()
    {
        try
        {
            Log.e("imageUri", "imageUri2222222222222222222 : "+imageUri);
            Bitmap thumbnail = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageUri);
            Log.e("#########","bmp height = "+thumbnail.getHeight());
            Log.e("#########","bmp width = "+thumbnail.getWidth());
            thumbnail = getResizedBitmap(thumbnail,400);
            bitmapImage=thumbnail;

            if (thumbnail!=null)
            {
                Picasso.with(getActivity())
                        .load(imageUri)
                        .fit()
                        .transform(mTransformation)
                        .into(iv_carImage);
                DriverCar_ByteImage = ConvertToByteArray(thumbnail);

                encodedImage = Base64.encodeToString(DriverCar_ByteImage, Base64.DEFAULT);
                SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE, encodedImage,getActivity());
            }
            else
            {
                SessionSave.saveUserSession(Comman.USER_VEHICLE_IMAGE, "",getActivity());
                DriverCar_ByteImage=null;
                new SnackbarUtils(main_layout, getString(R.string.please_select_vehicle_image),
                        ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
            }
        }
        catch (Exception e)
        {
            DriverCar_ByteImage=null;
            Log.e("call","exception = "+e.getMessage());
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public byte[] ConvertToByteArray(Bitmap bmp)
    {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 60, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }


    private void CheckData() {
        if (DriverCar_ByteImage==null)
        {
            new SnackbarUtils(main_layout, getString(R.string.please_select_vehicle_image),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else if (TextUtils.isEmpty(et_vehiRegistrationNo.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_vehicle_registration_no),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_carCompany.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_car_company),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else  if (TextUtils.isEmpty(et_carColor.getText().toString()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_car_color),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else
        {
            SessionSave.saveUserSession(Comman.USER_VEHICLE_REGISTRATION_NO, et_vehiRegistrationNo.getText().toString(),getActivity());
            SessionSave.saveUserSession(Comman.USER_CAR_COMPANY, et_carCompany.getText().toString(),getActivity());
            SessionSave.saveUserSession(Comman.USER_CAR_COLOR, et_carColor.getText().toString(),getActivity());
            SessionSave.saveUserSession(Comman.USER_CITY_ID, cityId,getActivity());
            GotoNextPage();
        }
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    private void GotoNextPage()
    {
        String AllSelectedVehicle="", VehicleId="";

        for (int i=0; i<list_vehicleItem.size() ; i++)
        {
            if (list_vehicleItem.get(i).getStatus().equalsIgnoreCase("1"))
            {
                if (AllSelectedVehicle !=null && !AllSelectedVehicle.equalsIgnoreCase(""))
                {
                    AllSelectedVehicle = AllSelectedVehicle + "," + list_vehicleItem.get(i).getBicycleName();
                    VehicleId = VehicleId + "," + list_vehicleItem.get(i).getId();
                }
                else
                {
                    AllSelectedVehicle = list_vehicleItem.get(i).getBicycleName();
                    VehicleId =  list_vehicleItem.get(i).getId();
                }
            }
        }
        if (VehicleId!= null && !VehicleId.equalsIgnoreCase(""))
        {
            SessionSave.saveUserSession(Comman.USER_VEHICLE_MODEL, VehicleId, getActivity());

            SessionSave.saveUserSession(Comman.REGISTER_VEHICLE_ACTIVITY, "1",getActivity());
            Log.e("tttttttttt", "tttttttttttt :" + AllSelectedVehicle);
            ((Registration_Tab_Activity) getActivity()).setTabAttachment();
        }
        else {
            new SnackbarUtils(main_layout, "You have to select at least 1 item.",
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
    }


    private void FindWhichTypeIs_Visible()
    {
        list_vehicleItem.clear();
        if(dialogClass == null) {
            dialogClass = new DialogClass(getActivity(), 1);
        }
        if(!dialogClass.isShowingDialog()) {
            dialogClass.showDialog();
        }
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_CAR_CLASS+cityId;

        Log.e("url", "FindWhichTypeIs_Visible = " + url);
        Log.e("param", "FindWhichTypeIs_Visible = " + params);


        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "FindWhichTypeIs_Visible = " + responseCode);
                    Log.e("Response", "FindWhichTypeIs_Visible = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                list_vehicleItem.clear();
                                String Id ="", CategoryId = "", Name="", Sort="", BaseFare="", MinKm="", PerKmCharge="", CancellationFee="", NightCharge="",NightTimeFrom=""
                                        , NightTimeTo="", SpecialEventSurcharge="", SpecialEventTimeFrom="", SpecialEventTimeTo="", WaitingTimeCost="", MinuteFare="", BookingFee=""
                                        , Capacity="", Image="", Description="", Status="";
                                Log.e("status", "true");

                                if(clickedType.equalsIgnoreCase(Register_VehicleType_Fragment.VEHICLE_TYPE))
                                {
                                    lv_vehicleType.setAdapter(adapter);
                                    rl_vehicleType.setVisibility(View.VISIBLE);
                                    rl_deliveryType.setVisibility(View.GONE);

                                    if (json.has("cars_and_taxi"))
                                    {
                                        String cars_and_taxi = json.getString("cars_and_taxi");
                                        if (cars_and_taxi!=null && !cars_and_taxi.equalsIgnoreCase(""))
                                        {
                                            JSONArray arrayCarTaxi = json.getJSONArray("cars_and_taxi");
                                            if(arrayCarTaxi.length() == 0){
                                                cityId = "4";
                                                SessionSave.saveUserSession(Comman.USER_CITY_ID,"4", getActivity());
                                                FindWhichTypeIs_Visible();
                                            }
                                            for (int i= 0; i<arrayCarTaxi.length(); i++)
                                            {
                                                JSONObject objectCarTexi = arrayCarTaxi.getJSONObject(i);
                                                if (objectCarTexi.has("Id"))
                                                {
                                                    Id = objectCarTexi.getString("Id");
                                                }

                                                if (objectCarTexi.has("Name"))
                                                {
                                                    Name = objectCarTexi.getString("Name");
                                                }

                                                if (objectCarTexi.has("Description"))
                                                {
                                                    Description = objectCarTexi.getString("Description");
                                                }
                                                list_vehicleItem.add(new Register_vehi_deliv_Type_Been(Id, Name, Description, "0"));
                                            }
                                            adapter = new Register_vehi_deliv_Type_Adapter(getActivity(), list_vehicleItem, main_layout, clickedType);
                                            lv_vehicleType.setAdapter(adapter);
//                                            adapter.notifyDataSetChanged();
                                            setListViewHeightBasedOnChildren(lv_vehicleType);

                                            Log.e("list size", "list sizeeeeeeeeeeeee :"+list_vehicleItem.size());
                                        }
                                        else
                                        {
                                            Log.e("cars_and_taxi", "null");
                                        }
                                    }
                                }
                                else
                                {
                                    lv_deliveryType.setAdapter(adapter);
                                    rl_vehicleType.setVisibility(View.GONE);
                                    rl_deliveryType.setVisibility(View.VISIBLE);

                                    if (json.has("delivery_services"))
                                    {
                                        String delivery_services = json.getString("delivery_services");
                                        if (delivery_services!=null && !delivery_services.equalsIgnoreCase(""))
                                        {
                                            JSONArray arrayDeliveryServices = json.getJSONArray("delivery_services");
                                            for (int i= 0; i<arrayDeliveryServices.length(); i++)
                                            {
                                                JSONObject objectDeliveryService = arrayDeliveryServices.getJSONObject(i);
                                                if (objectDeliveryService.has("Id"))
                                                {
                                                    Id = objectDeliveryService.getString("Id");
                                                }

                                                if (objectDeliveryService.has("Name"))
                                                {
                                                    Name = objectDeliveryService.getString("Name");
                                                }

                                                if (objectDeliveryService.has("Description"))
                                                {
                                                    Description = objectDeliveryService.getString("Description");
                                                }
                                                list_vehicleItem.add(new Register_vehi_deliv_Type_Been(Id, Name, Description, "0"));
                                            }
//                                            adapter.notifyDataSetChanged();
                                            adapter = new Register_vehi_deliv_Type_Adapter(getActivity(), list_vehicleItem, main_layout, clickedType);
                                            lv_deliveryType.setAdapter(adapter);
                                            setListViewHeightBasedOnChildren(lv_deliveryType);


                                            Log.e("list size", "list sizeeeeeeeeeeeee :"+list_vehicleItem.size());
                                        }
                                        else
                                        {
                                            Log.e("cars_and_taxi", "null");
                                        }
                                    }
                                }
                                dialogClass.hideDialog();
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message")) {
                                new SnackbarUtils(main_layout, json.getString("message"),
                                        ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                    }
                } catch (Exception e) {
                    Log.e("FindWhichTypeIs_Visible", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }.header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }

}
