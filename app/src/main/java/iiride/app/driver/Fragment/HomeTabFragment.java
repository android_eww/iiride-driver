package iiride.app.driver.Fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import iiride.app.driver.Activity.Drawer_Activity;
import iiride.app.driver.R;


public class HomeTabFragment extends Fragment implements View.OnClickListener{

    FrameLayout frame_home_fragment;

    LinearLayout ll_home, ll_dispatch, ll_mayJob, ll_hailed_fare, ll_pay;
    ImageView iv_home, iv_dispatch, iv_mayJob, iv_hailed_fare, iv_pay;
    TextView tv_home, tv_dispatch, tv_mayJob, tv_hailed_fare, tv_pay;
    public static Map_Fragment map_fragment;
    public static DispatchJob_Fragment dispatchJob_fragment;
    public static MyJob_Fragment myJob_fragment;
    public static Hailed_Fair_Fragment hailed_fail_fragment;
    public static Tick_Pay_Fragment tick_pay_fragment;
    public static Past_Job_List_Fragment pastJobList_fragment;
    public static Dispatched_Job_List_Fragment dispatchedJobListFragment;
    public static Pending_Job_List_Fragment pendingJobListFragment;
    public static Future_JobBooking_List_Fragment Future_JobBooking_List_Fragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home_tab, container, false);

        ((Drawer_Activity)getActivity()).setImageHeaderOnToolbar();

        frame_home_fragment = (FrameLayout) rootView.findViewById(R.id.frame_home_fragment);

        ll_home = (LinearLayout) rootView.findViewById(R.id.ll_home);
        ll_dispatch = (LinearLayout) rootView.findViewById(R.id.ll_dispatch);
        ll_mayJob = (LinearLayout) rootView.findViewById(R.id.ll_mayJob);
        ll_hailed_fare = (LinearLayout) rootView.findViewById(R.id.ll_hailed_fare);
        ll_pay = (LinearLayout) rootView.findViewById(R.id.ll_pay);

        ll_home.setOnClickListener(this);
        ll_dispatch.setOnClickListener(this);
        ll_mayJob.setOnClickListener(this);
        ll_hailed_fare.setOnClickListener(this);
        ll_pay.setOnClickListener(this);

        iv_home  = (ImageView) rootView.findViewById(R.id.iv_home);
        iv_dispatch  = (ImageView) rootView.findViewById(R.id.iv_dispatch);
        iv_mayJob  = (ImageView) rootView.findViewById(R.id.iv_mayJob);
        iv_hailed_fare  = (ImageView) rootView.findViewById(R.id.iv_hailed_fare);
        iv_pay  = (ImageView) rootView.findViewById(R.id.iv_pay);

        tv_home = (TextView) rootView.findViewById(R.id.tv_home);
        tv_dispatch = (TextView) rootView.findViewById(R.id.tv_dispatch);
        tv_mayJob = (TextView) rootView.findViewById(R.id.tv_mayJob);
        tv_hailed_fare = (TextView) rootView.findViewById(R.id.tv_hailed_fare);
        tv_pay = (TextView) rootView.findViewById(R.id.tv_pay);


        map_fragment = new Map_Fragment();
        dispatchJob_fragment = new DispatchJob_Fragment();
        myJob_fragment = new MyJob_Fragment();
        hailed_fail_fragment = new Hailed_Fair_Fragment();
        tick_pay_fragment = new Tick_Pay_Fragment();
        pastJobList_fragment = new Past_Job_List_Fragment();
        dispatchedJobListFragment = new Dispatched_Job_List_Fragment();
        pendingJobListFragment = new Pending_Job_List_Fragment();
        Future_JobBooking_List_Fragment = new Future_JobBooking_List_Fragment();


        setTab_Home();

        return rootView;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.ll_home:
                setTab_Home();
                break;

            case R.id.ll_dispatch:
                setTab_Dispatch();
                break;

            case R.id.ll_mayJob:
                setTab_MyJob();
                break;

            case R.id.ll_hailed_fare:
                setTab_HailedFare();
                break;

            case R.id.ll_pay:
                setTab_Pay();
                break;
        }
    }

    private void setTab_Home()
    {
        /*https://guides.codepath.com/android/Creating-and-Using-Fragments*/
        ((Drawer_Activity) getActivity()).setImageHeaderOnToolbar();

        iv_home.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.icon_home_select));
        iv_dispatch.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.icon_dispatch_unlselect));
        iv_mayJob.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.icon_job_unselect));
        iv_hailed_fare.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.hailed_fare_unselected));
        iv_pay.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.icon_tick_pay_unselect));

        tv_home.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorRed));
        tv_dispatch.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorGray));
        tv_mayJob.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorGray));
        tv_hailed_fare.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorGray));
        tv_pay.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorGray));

        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();

        if (map_fragment.isAdded()) { // if the fragment is already in container
            ft.show(map_fragment);
        } else { // fragment needs to be added to frame container
            ft.add(R.id.frame_home_fragment, map_fragment);
        }
        if (dispatchJob_fragment.isAdded()) { ft.hide(dispatchJob_fragment); }
        if (myJob_fragment.isAdded()) { ft.hide(myJob_fragment); }
        if (hailed_fail_fragment.isAdded()) { ft.hide(hailed_fail_fragment); }
        if (tick_pay_fragment.isAdded()) { ft.hide(tick_pay_fragment); }
        if (pastJobList_fragment.isAdded()) { ft.hide(pastJobList_fragment); }
        if (dispatchedJobListFragment.isAdded()) { ft.hide(dispatchedJobListFragment); }
        if (pendingJobListFragment.isAdded()) { ft.hide(pendingJobListFragment); }
        if (Future_JobBooking_List_Fragment.isAdded()) { ft.hide(Future_JobBooking_List_Fragment); }
        // Commit changes
        ft.commit();
    }

    private void setTab_Dispatch()
    {
        ((Drawer_Activity)getActivity()).setTitleOfScreenToolbar("Dispatch Jobs");

        iv_home.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.icon_home_unselect));
        iv_dispatch.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.icon_dispatch_lselect));
        iv_mayJob.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.icon_job_unselect));
        iv_hailed_fare.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.hailed_fare_unselected));
        iv_pay.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.icon_tick_pay_unselect));

        tv_home.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorGray));
        tv_dispatch.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorRed));
        tv_mayJob.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorGray));
        tv_hailed_fare.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorGray));
        tv_pay.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorGray));

        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();

        if (dispatchJob_fragment.isAdded()) { // if the fragment is already in container
            ft.show(dispatchJob_fragment);
        } else { // fragment needs to be added to frame container
            ft.add(R.id.frame_home_fragment, dispatchJob_fragment);
        }
        if (map_fragment.isAdded()) { ft.hide(map_fragment); }
        if (myJob_fragment.isAdded()) { ft.hide(myJob_fragment); }
        if (hailed_fail_fragment.isAdded()) { ft.hide(hailed_fail_fragment); }
        if (tick_pay_fragment.isAdded()) { ft.hide(tick_pay_fragment); }
        if (pastJobList_fragment.isAdded()) { ft.hide(pastJobList_fragment); }
        if (dispatchedJobListFragment.isAdded()) { ft.hide(dispatchedJobListFragment); }
        if (pendingJobListFragment.isAdded()) { ft.hide(pendingJobListFragment); }
        if (Future_JobBooking_List_Fragment.isAdded()) { ft.hide(Future_JobBooking_List_Fragment); }


        // Commit changes
        ft.commit();
    }

    private void setTab_MyJob()
    {
        ((Drawer_Activity)getActivity()).setTitleOfScreenToolbar("My Job");

        iv_home.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.icon_home_unselect));
        iv_dispatch.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.icon_dispatch_unlselect));
        iv_mayJob.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.icon_job_select));
        iv_hailed_fare.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.hailed_fare_unselected));
        iv_pay.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.icon_tick_pay_unselect));


        tv_home.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorGray));
        tv_dispatch.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorGray));
        tv_mayJob.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorRed));
        tv_hailed_fare.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorGray));
        tv_pay.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorGray));

        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();

        if (myJob_fragment.isAdded()) { // if the fragment is already in container
            ft.show(myJob_fragment);
        } else { // fragment needs to be added to frame container
            ft.add(R.id.frame_home_fragment, myJob_fragment);
        }
        if (map_fragment.isAdded()) { ft.hide(map_fragment); }
        if (dispatchJob_fragment.isAdded()) { ft.hide(dispatchJob_fragment); }
        if (hailed_fail_fragment.isAdded()) { ft.hide(hailed_fail_fragment); }
        if (tick_pay_fragment.isAdded()) { ft.hide(tick_pay_fragment); }
        if (pastJobList_fragment.isAdded()) { ft.hide(pastJobList_fragment); }
        if (dispatchedJobListFragment.isAdded()) { ft.hide(dispatchedJobListFragment); }
        if (pendingJobListFragment.isAdded()) { ft.hide(pendingJobListFragment); }
        if (Future_JobBooking_List_Fragment.isAdded()) { ft.hide(Future_JobBooking_List_Fragment); }


        // Commit changes
        ft.commit();
    }

    private void setTab_HailedFare()
    {
        ((Drawer_Activity)getActivity()).setTitleOfScreenToolbar("Hailed Fare");

        iv_home.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.icon_home_unselect));
        iv_dispatch.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.icon_dispatch_unlselect));
        iv_mayJob.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.icon_job_unselect));
        iv_hailed_fare.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.hailed_fare_selected));
        iv_pay.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.icon_tick_pay_unselect));


        tv_home.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorGray));
        tv_dispatch.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorGray));
        tv_mayJob.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorGray));
        tv_hailed_fare.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorRed));
        tv_pay.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorGray));

        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();

        if (hailed_fail_fragment.isAdded()) { // if the fragment is already in container
            ft.show(hailed_fail_fragment);
        } else { // fragment needs to be added to frame container
            ft.add(R.id.frame_home_fragment, hailed_fail_fragment);
        }
        if (dispatchJob_fragment.isAdded()) { ft.hide(dispatchJob_fragment); }
        if (myJob_fragment.isAdded()) { ft.hide(myJob_fragment); }
        if (map_fragment.isAdded()) { ft.hide(map_fragment); }
        if (tick_pay_fragment.isAdded()) { ft.hide(tick_pay_fragment); }
        if (pastJobList_fragment.isAdded()) { ft.hide(pastJobList_fragment); }
        if (dispatchedJobListFragment.isAdded()) { ft.hide(dispatchedJobListFragment); }
        if (pendingJobListFragment.isAdded()) { ft.hide(pendingJobListFragment); }
        if (Future_JobBooking_List_Fragment.isAdded()) { ft.hide(Future_JobBooking_List_Fragment); }


        // Commit changes
        ft.commit();
    }

    private void setTab_Pay()
    {
        ((Drawer_Activity)getActivity()).setTitleOfScreenToolbar("Tick Pay");

        iv_home.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.icon_home_unselect));
        iv_dispatch.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.icon_dispatch_unlselect));
        iv_mayJob.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.icon_job_unselect));
        iv_hailed_fare.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.hailed_fare_unselected));
        iv_pay.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.icon_tick_pay_select));


        tv_home.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorGray));
        tv_dispatch.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorGray));
        tv_mayJob.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorGray));
        tv_hailed_fare.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorGray));
        tv_pay.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorRed));

        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();

        if (tick_pay_fragment.isAdded()) { // if the fragment is already in container
            ft.show(tick_pay_fragment);
        } else { // fragment needs to be added to frame container
            ft.add(R.id.frame_home_fragment, tick_pay_fragment);
        }
        if (dispatchJob_fragment.isAdded()) { ft.hide(dispatchJob_fragment); }
        if (myJob_fragment.isAdded()) { ft.hide(myJob_fragment); }
        if (hailed_fail_fragment.isAdded()) { ft.hide(hailed_fail_fragment); }
        if (map_fragment.isAdded()) { ft.hide(map_fragment); }
        if (map_fragment.isAdded()) { ft.hide(map_fragment); }
        if (pastJobList_fragment.isAdded()) { ft.hide(pastJobList_fragment); }
        if (dispatchedJobListFragment.isAdded()) { ft.hide(dispatchedJobListFragment); }
        if (pendingJobListFragment.isAdded()) { ft.hide(pendingJobListFragment); }
        if (Future_JobBooking_List_Fragment.isAdded()) { ft.hide(Future_JobBooking_List_Fragment); }


        // Commit changes
        ft.commit();
    }

}
