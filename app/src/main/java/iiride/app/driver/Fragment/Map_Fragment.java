package iiride.app.driver.Fragment;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import iiride.app.driver.Activity.Drawer_Activity;
import iiride.app.driver.Activity.Main_Activity;
import iiride.app.driver.Application.TiCKTOC_Driver_Application;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.Constants;
import iiride.app.driver.Comman.DataParser;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;
import iiride.app.driver.Kalman.KalmanLocationManager;
import iiride.app.driver.Others.BubbleService;
import iiride.app.driver.Others.ConnectivityReceiver;
import iiride.app.driver.Others.GPSTracker;


import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.MyAlertDialog;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


public class Map_Fragment extends Fragment {

    MapView mMapView;
    public static GoogleMap googleMap;
    public static Polyline polyline;
    public static Marker marker, marker_pickUpLocation, marker_dropOffLocation;

    double latitude, longitude;
    public static double pickUpLat, pickUpLng;
    public static double DropOffLat, DropOffLng;

    TextView tv_location;

    ImageView reset_current_location;

    public static GPSTracker gpsTracker;
//    Handler handler;

    public static LinearLayout ll_start_trip_option, ll_complete_hold;
    public static RelativeLayout main_layout;
    public static TextView tv_start_trip, tv_passengerInfo, tv_directionAcceptReq, tv_directionStartTrip, tv_complete_trip;////, tv_start_waiting, tv_stop_waiting

    private AQuery aQuery;
    DialogClass dialogClass;

    String distance, dropOffLocationApi="";
    public static int intMarkerAnim = 0;
    private static final int PERMISSION_REQUEST_CODE = 1;
    int ClickRelocateButton=0;
    public static int readBearing =0;

    public static LatLng sydney=null;
    /////
    private static final long GPS_TIME = 5000;
    private static final long NET_TIME = 5000;
    private static final long FILTER_TIME = 5000;

    public static Float bearingF = 0f;

    // Context
    private KalmanLocationManager mKalmanLocationManager;

    int flagCameraAnim=0;
    MediaPlayer ringTone;
    Dialog dialogTollFee;
    String TollFeeApi="";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);

        intMarkerAnim=0;
        flagCameraAnim=0;
        ClickRelocateButton=0;
        aQuery = new AQuery(Drawer_Activity.activity);
        dialogClass = new DialogClass(Drawer_Activity.activity, 1);

        mKalmanLocationManager = new KalmanLocationManager(Drawer_Activity.activity);

        main_layout = (RelativeLayout) rootView.findViewById(R.id.main_layout);
        ll_start_trip_option = (LinearLayout) rootView.findViewById(R.id.ll_start_trip_option);
        ll_complete_hold = (LinearLayout) rootView.findViewById(R.id.ll_complete_hold);
        ll_start_trip_option.setVisibility(View.GONE);
        ll_complete_hold.setVisibility(View.GONE);

        tv_start_trip = (TextView) rootView.findViewById(R.id.tv_start_trip);
        tv_passengerInfo = (TextView) rootView.findViewById(R.id.tv_passengerInfo);
        tv_directionAcceptReq = (TextView) rootView.findViewById(R.id.tv_directionAcceptReq);
        tv_directionStartTrip = (TextView) rootView.findViewById(R.id.tv_directionStartTrip);
        tv_complete_trip = (TextView) rootView.findViewById(R.id.tv_complete_trip);
//        tv_start_waiting = (TextView) rootView.findViewById(R.id.tv_start_waiting);
//        tv_stop_waiting = (TextView) rootView.findViewById(R.id.tv_stop_waiting);

        reset_current_location = (ImageView) rootView.findViewById(R.id.reset_current_location);
        tv_location = (TextView) rootView.findViewById(R.id.tv_location);

        tv_location.post(new Runnable() {
            @Override
            public void run() {
                tv_location.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                tv_location.setSelected(true);
            }
        });

        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;
                googleMap.getUiSettings().setRotateGesturesEnabled(false);

                // For showing a move to my location button
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    return;
                }
                googleMap.setMyLocationEnabled(false);
                ll_start_trip_option.setVisibility(View.GONE);
                // For dropping a marker at a point on the Map
                ClickRelocateButton=0;
                ClearTripSession();
                RefreshLocation();
            }
        });

        tv_start_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String DutyStatus = SessionSave.getUserSession(Comman.USER_DRIVER_DUTY, Drawer_Activity.activity);
                if (DutyStatus!=null && !DutyStatus.equalsIgnoreCase("") && !DutyStatus.equalsIgnoreCase("0"))
                {
                    if (ConnectivityReceiver.isConnected())
                    {
                        tv_start_trip.setEnabled(false);
                        SessionSave.saveUserSession(Comman.START_TRIP_FLAG, "1", Drawer_Activity.activity);
                        SetDriverTo_DropOffLocation(0);
                    }
                    else
                    {
                        new SnackbarUtils(Drawer_Activity.main_layout, getString(R.string.not_connected_to_internet),
                                ContextCompat.getColor(Drawer_Activity.activity, R.color.snakbar_color),
                                ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color)).snackieBar().show();
                    }
                }
                else
                {
                    new SnackbarUtils(main_layout, getResources().getString(R.string.get_online_first),
                            ContextCompat.getColor(Drawer_Activity.activity, R.color.snakbar_color),
                            ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        });

        tv_directionAcceptReq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected())
                {
                    if(Build.VERSION.SDK_INT >= 23) {
                        if (Settings.canDrawOverlays(Drawer_Activity.activity)) {
                            ((Drawer_Activity)getActivity()).startService(new Intent(Drawer_Activity.activity, BubbleService.class));
                            tv_directionAcceptReq.setEnabled(false);
                            Uri.Builder directionsBuilder = new Uri.Builder()
                                    .scheme("https")
                                    .authority("www.google.com")
                                    .appendPath("maps")
                                    .appendPath("dir")
                                    .appendPath("")
                                    .appendQueryParameter("api", "1")
                                    .appendQueryParameter("destination", SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LAT, Drawer_Activity.activity) +","+SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LNG, Drawer_Activity.activity));

                            startActivity(new Intent(Intent.ACTION_VIEW, directionsBuilder.build()));
                            tv_directionAcceptReq.setEnabled(true);
                            //((Drawer_Activity)getActivity()).finish();
                        } else {
                            ((Drawer_Activity)getActivity()).askForSystemOverlayPermission();
                        }
                    }else{
                        ((Drawer_Activity)getActivity()).startService(new Intent(Drawer_Activity.activity, BubbleService.class));
                        tv_directionAcceptReq.setEnabled(false);
                        Uri.Builder directionsBuilder = new Uri.Builder()
                                .scheme("https")
                                .authority("www.google.com")
                                .appendPath("maps")
                                .appendPath("dir")
                                .appendPath("")
                                .appendQueryParameter("api", "1")
                                .appendQueryParameter("destination", SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LAT, Drawer_Activity.activity) +","+SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LNG, Drawer_Activity.activity));

                        startActivity(new Intent(Intent.ACTION_VIEW, directionsBuilder.build()));
                        tv_directionAcceptReq.setEnabled(true);
                        //((Drawer_Activity)getActivity()).finish();
                    }


                }
                else
                {
                    new SnackbarUtils(Drawer_Activity.main_layout, getString(R.string.not_connected_to_internet),
                            ContextCompat.getColor(Drawer_Activity.activity, R.color.snakbar_color),
                            ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        });

        tv_directionStartTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected())
                {
                    if(Build.VERSION.SDK_INT >= 23) {
                        if (Settings.canDrawOverlays(Drawer_Activity.activity)) {
                            ((Drawer_Activity)getActivity()).startService(new Intent(Drawer_Activity.activity, BubbleService.class));
                            tv_directionStartTrip.setEnabled(false);
                            Uri.Builder directionsBuilder = new Uri.Builder()
                                    .scheme("https")
                                    .authority("www.google.com")
                                    .appendPath("maps")
                                    .appendPath("dir")
                                    .appendPath("")
                                    .appendQueryParameter("api", "1")
                                    .appendQueryParameter("destination", SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LAT, Drawer_Activity.activity) +","+SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LNG, Drawer_Activity.activity));

                            startActivity(new Intent(Intent.ACTION_VIEW, directionsBuilder.build()));
                            tv_directionStartTrip.setEnabled(true);
                            //((Drawer_Activity)getActivity()).finish();
                        } else {
                            ((Drawer_Activity)getActivity()).askForSystemOverlayPermission();
                        }
                    }else{
                        ((Drawer_Activity)getActivity()).startService(new Intent(Drawer_Activity.activity, BubbleService.class));
                        tv_directionStartTrip.setEnabled(false);
                        Uri.Builder directionsBuilder = new Uri.Builder()
                                .scheme("https")
                                .authority("www.google.com")
                                .appendPath("maps")
                                .appendPath("dir")
                                .appendPath("")
                                .appendQueryParameter("api", "1")
                                .appendQueryParameter("destination", SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LAT, Drawer_Activity.activity) +","+SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LNG, Drawer_Activity.activity));

                        startActivity(new Intent(Intent.ACTION_VIEW, directionsBuilder.build()));
                        tv_directionStartTrip.setEnabled(true);
                        //((Drawer_Activity)getActivity()).finish();
                    }
                }
                else
                {
                    new SnackbarUtils(Drawer_Activity.main_layout, getString(R.string.not_connected_to_internet),
                            ContextCompat.getColor(Drawer_Activity.activity, R.color.snakbar_color),
                            ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        });

        tv_passengerInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Drawer_Activity.ExpandBottomShit();
            }
        });

        tv_complete_trip.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String DutyStatus1 = SessionSave.getUserSession(Comman.USER_DRIVER_DUTY, Drawer_Activity.activity);
                if (DutyStatus1!=null && !DutyStatus1.equalsIgnoreCase("") && !DutyStatus1.equalsIgnoreCase("0"))
                {
                    if (ConnectivityReceiver.isConnected())
                    {
                        TollFeeApi="";
                        tv_complete_trip.setEnabled(false);
                        getDistanceBetween_TwoLoc();
                    }
                    else
                    {
                        new SnackbarUtils(Drawer_Activity.main_layout, getString(R.string.not_connected_to_internet),
                                ContextCompat.getColor(Drawer_Activity.activity, R.color.snakbar_color),
                                ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color)).snackieBar().show();
                    }
                }
                else
                {
                    new SnackbarUtils(main_layout, getResources().getString(R.string.get_online_first),
                            ContextCompat.getColor(Drawer_Activity.activity, R.color.snakbar_color),
                            ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color),
                            ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color)).snackieBar().show();
                }

                int y = 0;
                if(y == 0){return;}



                dialogTollFee = new Dialog(Drawer_Activity.activity, R.style.DialogTheme);
                dialogTollFee.requestWindowFeature(Window.FEATURE_NO_TITLE);
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialogTollFee.getWindow().getAttributes());
                lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
                lp.height = LinearLayout.LayoutParams.MATCH_PARENT;
                dialogTollFee.getWindow().setAttributes(lp);
                dialogTollFee.setCancelable(false);
                dialogTollFee.setContentView(R.layout.dialog_add_toll_fee);

                final TextView tv_dollar, tv_dialog_yes, tv_dialog_no;
                final EditText et_tollfee;

                tv_dollar = (TextView) dialogTollFee.findViewById(R.id.tv_dollar);
                tv_dollar.setVisibility(View.GONE);
                tv_dialog_yes = (TextView) dialogTollFee.findViewById(R.id.tv_dialog_yes);
                tv_dialog_no = (TextView) dialogTollFee.findViewById(R.id.tv_dialog_no);
                et_tollfee = (EditText) dialogTollFee.findViewById(R.id.et_tollfee);

                et_tollfee.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length()>0)
                        {
                            tv_dollar.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            tv_dollar.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                tv_dialog_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TollFeeApi="";
                        dialogTollFee.dismiss();
                    }
                });
                tv_dialog_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (TextUtils.isEmpty(et_tollfee.getText().toString()))
                        {
                            et_tollfee.setError("Please enter Toll fee");
                        }
                        else
                        {
                            TollFeeApi = et_tollfee.getText().toString();
                            dialogTollFee.dismiss();
                        }

                    }
                });
                dialogTollFee.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        Log.e("dialogTollFee","setOnDismissListener");
                        String DutyStatus = SessionSave.getUserSession(Comman.USER_DRIVER_DUTY, Drawer_Activity.activity);
                        if (DutyStatus!=null && !DutyStatus.equalsIgnoreCase("") && !DutyStatus.equalsIgnoreCase("0"))
                        {
                            if (ConnectivityReceiver.isConnected())
                            {
                                tv_complete_trip.setEnabled(false);
                                getDistanceBetween_TwoLoc();
                            }
                            else
                            {
                                new SnackbarUtils(Drawer_Activity.main_layout, getString(R.string.not_connected_to_internet),
                                        ContextCompat.getColor(Drawer_Activity.activity, R.color.snakbar_color),
                                        ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color),
                                        ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }
                        else
                        {
                            new SnackbarUtils(main_layout, getResources().getString(R.string.get_online_first),
                                    ContextCompat.getColor(Drawer_Activity.activity, R.color.snakbar_color),
                                    ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color),
                                    ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color)).snackieBar().show();
                        }
                    }
                });

                String BookingType = SessionSave.getUserSession(Comman.PASSENGER_BOOKING_TYPE, Drawer_Activity.activity);
                if (BookingType!=null && !BookingType.equalsIgnoreCase("dispatch"))
                {
                    Log.e("dialogTollFee","dialogTollFee dispatch show");
                    dialogTollFee.show();
                }
                else
                {
                    Log.e("dialogTollFee","dialogTollFee dispatch dismiss");
//                    dialogTollFee.dismiss();
                    String DutyStatus = SessionSave.getUserSession(Comman.USER_DRIVER_DUTY, Drawer_Activity.activity);
                    if (DutyStatus!=null && !DutyStatus.equalsIgnoreCase("") && !DutyStatus.equalsIgnoreCase("0"))
                    {
                        if (ConnectivityReceiver.isConnected())
                        {
                            tv_complete_trip.setEnabled(false);
                            getDistanceBetween_TwoLoc();
                        }
                        else
                        {
                            new SnackbarUtils(Drawer_Activity.main_layout, getString(R.string.not_connected_to_internet),
                                    ContextCompat.getColor(Drawer_Activity.activity, R.color.snakbar_color),
                                    ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color),
                                    ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color)).snackieBar().show();
                        }
                    }
                    else
                    {
                        new SnackbarUtils(main_layout, getResources().getString(R.string.get_online_first),
                                ContextCompat.getColor(Drawer_Activity.activity, R.color.snakbar_color),
                                ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(Drawer_Activity.activity, R.color.snackbar_text_color)).snackieBar().show();
                    }
                }

            }
        });

       /* tv_start_waiting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SessionSave.saveUserSession(Comman.START_WAITING, "1", Drawer_Activity.activity);
                SessionSave.saveUserSession(Comman.STOP_WAITING, "0", Drawer_Activity.activity);
                tv_stop_waiting.setVisibility(View.VISIBLE);
                tv_start_waiting.setVisibility(View.GONE);
                ll_start_trip_option.setVisibility(View.GONE);
                ((Drawer_Activity)getActivity()).HoldTrip(SessionSave.getUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, Drawer_Activity.activity));
            }
        });

        tv_stop_waiting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SessionSave.saveUserSession(Comman.STOP_WAITING, "1", Drawer_Activity.activity);
                SessionSave.saveUserSession(Comman.START_WAITING, "0", Drawer_Activity.activity);
                tv_start_waiting.setVisibility(View.VISIBLE);
                tv_stop_waiting.setVisibility(View.GONE);
                ll_start_trip_option.setVisibility(View.GONE);
                ((Drawer_Activity)getActivity()).EndHoldTrip(SessionSave.getUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, Drawer_Activity.activity));

            }
        });*/

        reset_current_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClickRelocateButton=1;
//                RefreshLocation();
                setCameraOnMarker();
            }
        });


        return rootView;
    }

    private void setCameraOnMarker()
    {
        intMarkerAnim=0;
        GPSTracker tracker = new GPSTracker(getActivity());
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        }
        else
        {
            try {
                int locationMode = Settings.Secure.getInt(getActivity().getContentResolver(), Settings.Secure.LOCATION_MODE);
                int LOCATION_MODE_HIGH_ACCURACY = 3;
                if(locationMode == LOCATION_MODE_HIGH_ACCURACY)
                {
                    if (!tracker.canGetLocation())
                    {
                        tracker.showSettingsAlert();
                    }
                    else
                    {
                        gpsTracker = new GPSTracker(getActivity());
                        gpsTracker.getLocation();
                        if (Constants.newgpsLatitude!=null && !Constants.newgpsLatitude.equalsIgnoreCase(""))
                        {
                            if (Double.parseDouble(Constants.newgpsLatitude)!=0)
                            {

                            }
                            else
                            {
                                Constants.newgpsLatitude = gpsTracker.getLatitude() + "";
                                Constants.newgpsLongitude = gpsTracker.getLongitude() + "";
                            }
                        }
                        else
                        {
                            Constants.newgpsLatitude = gpsTracker.getLatitude() + "";
                            Constants.newgpsLongitude = gpsTracker.getLongitude() + "";
                        }

                        latitude = Double.parseDouble(Constants.newgpsLatitude);
                        longitude = Double.parseDouble(Constants.newgpsLongitude);
                        sydney=null;
                        sydney = new LatLng(latitude, longitude);

                        Location location = new Location(LocationManager.GPS_PROVIDER);
                        location.setLatitude(latitude);
                        location.setLongitude(longitude);

                        if (marker != null)
                        {
                            Log.e("marker","marker not null");
                            marker.remove();
                            marker=null;
                            marker = googleMap.addMarker(new MarkerOptions().position(sydney).icon(BitmapDescriptorFactory.fromResource(GetIconMarker())).anchor(0.5f, 0.5f).title("You").snippet("Marker Description"));
                            animateMarkerNew(location, marker);
                        }
                        else
                        {
                            Log.e("marker","marker nulllllllllllllllllll");
                            marker = googleMap.addMarker(new MarkerOptions().position(sydney).icon(BitmapDescriptorFactory.fromResource(GetIconMarker())).anchor(0.5f, 0.5f).title("You").snippet("Marker Description"));
                            animateMarkerNew(location, marker);
                        }
                        getAddress(latitude, longitude);
                    }
                }
                else
                {
                    //redirect user to settings page
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("GPS is settings");

                    alertDialog.setMessage("Please set your Location Mode to High accuracy.");

                    // On pressing Settings button
                    alertDialog.setPositiveButton("Settings",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            });
                    alertDialog.show();

                }
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public static void clearMap()
    {
        ll_start_trip_option.setVisibility(View.GONE);
        ll_complete_hold.setVisibility(View.GONE);
        sydney=null;
        sydney = new LatLng(Double.parseDouble(Constants.newgpsLatitude), Double.parseDouble(Constants.newgpsLongitude));
        if (marker != null)
        {
            marker.remove();
            marker=null;
        }
        if (marker_pickUpLocation != null)
        {
            marker_pickUpLocation.remove();
            marker_pickUpLocation=null;
        }
        if (marker_dropOffLocation != null)
        {
            marker_dropOffLocation.remove();
            marker_dropOffLocation=null;
        }
        googleMap.clear();

        marker = googleMap.addMarker(new MarkerOptions().position(sydney).icon(BitmapDescriptorFactory.fromResource(GetIconMarker())).rotation(90).anchor(0.5f, 0.5f).title("You").snippet("Marker Description"));
        CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(17.5f).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    public void RefreshLocation()
    {
        intMarkerAnim=0;
        GPSTracker tracker = new GPSTracker(getActivity());
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        }
        else
        {
            try {
                int locationMode = Settings.Secure.getInt(getActivity().getContentResolver(), Settings.Secure.LOCATION_MODE);
                int LOCATION_MODE_HIGH_ACCURACY = 3;
                if(locationMode == LOCATION_MODE_HIGH_ACCURACY)
                {
                    if (!tracker.canGetLocation())
                    {
                        tracker.showSettingsAlert();
                    }
                    else
                    {
                        String DriverId = SessionSave.getUserSession(Comman.USER_ID, Drawer_Activity.activity);
                        if (DriverId!=null && !DriverId.equalsIgnoreCase(""))
                        {
                            CallApiFor_FindStatus(DriverId);
                        }
                    }
                }
                else
                {
                    //redirect user to settings page
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("GPS is settings");

                    alertDialog.setMessage("Please set your Location Mode to High accuracy.");

                    // On pressing Settings button
                    alertDialog.setPositiveButton("Settings",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            });
                    alertDialog.show();

                }
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private void CallApiFor_FindStatus(String driverId)
    {
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_CURRENT_TRIP_FLOW + driverId+"/"+SessionSave.getToken(Comman.DEVICE_TOKEN, Drawer_Activity.activity);

        Log.e("url", "CallApiFor_FindStatus = " + url);
        Log.e("param", "CallApiFor_FindStatus = " + params);


        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "CallApiFor_FindStatus = " + responseCode);
                    Log.e("Response", "CallApiFor_FindStatus = " + json);

                    if (json != null)
                    {
                        if (json.has("rating") && json.getString("rating")!=null && !json.getString("rating").equalsIgnoreCase(""))
                        {
                            SessionSave.saveUserSession(Comman.USER_RATE_POINT, json.getString("rating"), Drawer_Activity.activity);
                        }
                        else
                        {
                            SessionSave.saveUserSession(Comman.USER_RATE_POINT,"0", Drawer_Activity.activity);
                        }

                        if (json.has("balance") && json.getString("balance")!=null && !json.getString("balance").equalsIgnoreCase(""))
                        {
                            SessionSave.saveUserSession(Comman.DRIVER_WALLET_BALLENCE, json.getString("balance"), Drawer_Activity.activity);
                        }
                        else
                        {
                            SessionSave.saveUserSession(Comman.DRIVER_WALLET_BALLENCE,"", Drawer_Activity.activity);
                        }

                        if (json.has("future_booking") && json.getString("future_booking")!=null && !json.getString("future_booking").equalsIgnoreCase(""))
                        {
                            SessionSave.saveUserSession(Comman.NOTIFICATION_COUNT_FUTURE_BOOKING, json.getString("future_booking"), Drawer_Activity.activity);
                        }
                        else
                        {
                            SessionSave.saveUserSession(Comman.NOTIFICATION_COUNT_FUTURE_BOOKING,"0", Drawer_Activity.activity);
                        }
                        Drawer_Activity.activity.Set_FutureBookingCount();

                        if (json.has("login") && json.getString("login")!=null && !json.getString("login").equalsIgnoreCase("false"))
                        {
                            if (json.has("status"))
                            {
                                if (json.getBoolean("status"))
                                {
                                    Log.e("status", "true");
                                    if (json.has("BookingType"))
                                    {
                                        String BookingType = json.getString("BookingType");
                                        if (BookingType!=null && !BookingType.equalsIgnoreCase(""))
                                        {
                                            if (BookingType.equalsIgnoreCase("BookLater"))
                                            {
                                                SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "1", Drawer_Activity.activity);
                                            }
                                            else
                                            {
                                                SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "0", Drawer_Activity.activity);
                                            }
                                        }
                                        else
                                        {
                                            SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "0", Drawer_Activity.activity);
                                        }
                                    }
                                    else
                                    {
                                        SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "0", Drawer_Activity.activity);
                                    }
                                    if (json.has("BookingInfo"))
                                    {
                                        String BookingInfo = json.getString("BookingInfo");
                                        if (BookingInfo!=null && !BookingInfo.equalsIgnoreCase(""))
                                        {
                                            String TripId="", PassengerId="", DropOffLat="", DropOffLon="", DropoffLocation="", PickupLat="", PickupLng="", PickupLocation="", Notes=""
                                                    , PaymentType="", FlightNumber="", PassengerType="", BookingType="";
                                            JSONObject BookingInfoObj = json.getJSONObject("BookingInfo");
                                            if (BookingInfoObj.has("Id"))
                                            {
                                                TripId = BookingInfoObj.getString("Id");
                                                SessionSave.saveUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, TripId, Drawer_Activity.activity);
                                            }
                                            if (BookingInfoObj.has("PassengerId"))
                                            {
                                                PassengerId = BookingInfoObj.getString("PassengerId");
                                                SessionSave.saveUserSession(Comman.PASSENGER_ID, PassengerId, Drawer_Activity.activity);
                                            }
                                            if (BookingInfoObj.has("DropOffLat"))
                                            {
                                                DropOffLat = BookingInfoObj.getString("DropOffLat");
                                                SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LAT, DropOffLat, Drawer_Activity.activity);
                                            }
                                            if (BookingInfoObj.has("DropOffLon"))
                                            {
                                                DropOffLon = BookingInfoObj.getString("DropOffLon");
                                                SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LNG, DropOffLon, Drawer_Activity.activity);
                                            }
                                            if (BookingInfoObj.has("DropoffLocation"))
                                            {
                                                DropoffLocation = BookingInfoObj.getString("DropoffLocation");
                                                SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LOCATION, DropoffLocation, Drawer_Activity.activity);
                                            }
                                            if (BookingInfoObj.has("PickupLat"))
                                            {
                                                PickupLat = BookingInfoObj.getString("PickupLat");
                                                SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LAT, PickupLat, Drawer_Activity.activity);
                                            }
                                            if (BookingInfoObj.has("PickupLng"))
                                            {
                                                PickupLng = BookingInfoObj.getString("PickupLng");
                                                SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LNG, PickupLng, Drawer_Activity.activity);
                                            }
                                            if (BookingInfoObj.has("PickupLocation"))
                                            {
                                                PickupLocation = BookingInfoObj.getString("PickupLocation");
                                                SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LOCATION, PickupLocation, Drawer_Activity.activity);
                                            }

                                            if (BookingInfoObj.has("BookingType")) {
                                                BookingType = BookingInfoObj.getString("BookingType");
                                                SessionSave.saveUserSession(Comman.PASSENGER_BOOKING_TYPE, BookingType, Drawer_Activity.activity);
                                            }

                                            if (BookingInfoObj.has("Status"))
                                            {
                                                String Status = BookingInfoObj.getString("Status");
                                                if (Status!=null && !Status.equalsIgnoreCase(""))
                                                {
                                                    if (Status.equalsIgnoreCase("accepted"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.ACCEPT_REQUEST_FLAG, "1", Drawer_Activity.activity);
                                                        SessionSave.saveUserSession(Comman.START_TRIP_FLAG, "0", Drawer_Activity.activity);
                                                    }
                                                    else if (Status.equalsIgnoreCase("traveling"))
                                                    {
                                                        SessionSave.saveUserSession(Comman.ACCEPT_REQUEST_FLAG, "1", Drawer_Activity.activity);
                                                        SessionSave.saveUserSession(Comman.START_TRIP_FLAG, "1", Drawer_Activity.activity);
                                                    }
                                                }
                                            }
                                            if (BookingInfoObj.has("Notes"))
                                            {
                                                Notes = BookingInfoObj.getString("Notes");
                                                SessionSave.saveUserSession(Comman.PASSENGER_NOTE, Notes, Drawer_Activity.activity);
                                            }
                                            if (BookingInfoObj.has("PaymentType"))
                                            {
                                                PaymentType = BookingInfoObj.getString("PaymentType");
                                                SessionSave.saveUserSession(Comman.PASSENGER_PAYMENT_TYPE, PaymentType, Drawer_Activity.activity);
                                            }
                                            if (BookingInfoObj.has("FlightNumber"))
                                            {
                                                FlightNumber = BookingInfoObj.getString("FlightNumber");
                                                SessionSave.saveUserSession(Comman.PASSENGER_FLIGHT_NO, FlightNumber, Drawer_Activity.activity);
                                            }
                                            if (BookingInfoObj.has("PassengerType"))
                                            {
                                                PassengerType = BookingInfoObj.getString("PassengerType");
                                                SessionSave.saveUserSession(Comman.PASSENGER_TYPE, PassengerType, Drawer_Activity.activity);
                                            }
                                            if (PassengerType!=null && !PassengerType.equalsIgnoreCase("") && (PassengerType.equalsIgnoreCase("others") || PassengerType.equalsIgnoreCase("other")))
                                            {
                                                if (BookingInfoObj.has("PassengerName"))
                                                {
                                                    String Fullname = BookingInfoObj.getString("PassengerName");
                                                    SessionSave.saveUserSession(Comman.PASSENGER_NAME, Fullname, Drawer_Activity.activity);
                                                }
                                                if (BookingInfoObj.has("PassengerContact"))
                                                {
                                                    String MobileNo = BookingInfoObj.getString("PassengerContact");
                                                    SessionSave.saveUserSession(Comman.PASSENGER_MOB_NO_OTHER, MobileNo, Drawer_Activity.activity);
                                                }
                                            }
                                            else
                                            {
                                                if (json.has("PassengerInfo"))
                                                {
                                                    String PassengerInfo = json.getString("PassengerInfo");
                                                    if (PassengerInfo!=null && !PassengerInfo.equalsIgnoreCase(""))
                                                    {
                                                        JSONObject PassengerInfoObj = json.getJSONObject("PassengerInfo");
                                                        if (PassengerInfoObj.has("Fullname"))
                                                        {
                                                            String Fullname = PassengerInfoObj.getString("Fullname");
                                                            SessionSave.saveUserSession(Comman.PASSENGER_NAME, Fullname, Drawer_Activity.activity);
                                                        }
                                                        if (PassengerInfoObj.has("MobileNo"))
                                                        {
                                                            String MobileNo = PassengerInfoObj.getString("MobileNo");
                                                            SessionSave.saveUserSession(Comman.PASSENGER_MOBILE_NO, MobileNo, Drawer_Activity.activity);
                                                        }
                                                        if (PassengerInfoObj.has("Image"))
                                                        {
                                                            String image = PassengerInfoObj.getString("Image");
                                                            SessionSave.saveUserSession(Comman.PASSENGER_IMAGE, image, Drawer_Activity.activity);
                                                        }
                                                    }
                                                }

                                            }
                                        }
                                        else
                                        {
                                            Log.e("status", "false");
                                            dialogClass.hideDialog();
                                            ClearDataOnWeserviceCall();
                                        }
                                    }
                                    else
                                    {
                                        Log.e("status", "false");
                                        dialogClass.hideDialog();
                                        ClearDataOnWeserviceCall();
                                    }
                                    dialogClass.hideDialog();
                                    if (ClickRelocateButton==0)
                                    {
                                        if (SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, Drawer_Activity.activity).equalsIgnoreCase("")
                                                && SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, Drawer_Activity.activity).equalsIgnoreCase("1"))
                                        {
                                            if (SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity).equalsIgnoreCase("")
                                                    && SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity).equalsIgnoreCase("1"))
                                            {
                                                SetDriverTo_DropOffLocation(1);
                                            }
                                            else
                                            {
                                                SetDriverToPickUpLocation();
                                            }
                                        }
                                    }
                                    dialogClass.hideDialog();
                                }
                                else
                                {
                                    Log.e("status", "false");
                                    dialogClass.hideDialog();
                                    ClearDataOnWeserviceCall();
                                }
                            }
                            else
                            {
                                Log.e("json", "no status found");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                                }
                                ClearDataOnWeserviceCall();
                            }
                        }
                        else
                        {
                            SessionSave.clearUserSession(Drawer_Activity.activity);

                            final Dialog dialog;
                            if (TiCKTOC_Driver_Application.currentActivity()!=null)
                            {
                                dialog = new Dialog(TiCKTOC_Driver_Application.currentActivity(), R.style.DialogTheme);
                            }
                            else
                            {
                                dialog = new Dialog(Drawer_Activity.activity, R.style.DialogTheme);
                            }
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            lp.copyFrom(dialog.getWindow().getAttributes());
                            lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
                            lp.height = LinearLayout.LayoutParams.MATCH_PARENT;
                            dialog.getWindow().setAttributes(lp);
                            dialog.setCancelable(false);
                            dialog.setContentView(R.layout.dialog_receive_cancel_trip);

                            TextView tv_dialog_ok, tv_title, tv_message;

                            tv_title = (TextView) dialog.findViewById(R.id.tv_title);
                            tv_dialog_ok = (TextView) dialog.findViewById(R.id.tv_dialog_ok);
                            tv_message = (TextView) dialog.findViewById(R.id.tv_message);

                            tv_title.setText("Session Expire");
                            tv_message.setText(getResources().getString(R.string.session_expire));

                            tv_dialog_ok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(TiCKTOC_Driver_Application.currentActivity(), Main_Activity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    dialog.dismiss();
                                }
                            });
                            dialog.show();
                        }

                        if (Constants.newgpsLatitude!=null && !Constants.newgpsLatitude.equalsIgnoreCase("") && Constants.newgpsLongitude!=null && !Constants.newgpsLongitude.equalsIgnoreCase(""))
                        {
                            Location location = new Location(LocationManager.GPS_PROVIDER);
                            location.setLatitude(Double.parseDouble(Constants.newgpsLatitude));
                            location.setLongitude(Double.parseDouble(Constants.newgpsLongitude));
                            getAddress(location.getLatitude(), location.getLongitude());
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                        ClearDataOnWeserviceCall();
                    }
                }
                catch (Exception e)
                {
                    Log.e("CallApiFor_FindStatus", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                    ClearDataOnWeserviceCall();
                }
            }
        }.header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }

    private void ClearDataOnWeserviceCall()
    {
        SessionSave.saveUserSession(Comman.ACCEPT_REQUEST_FLAG, "0", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.START_TRIP_FLAG, "0",  Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.STOP_WAITING, "0", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.START_WAITING, "0", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "0", Drawer_Activity.activity);

        setCameraOnMarker();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode)
        {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    gpsTracker = new GPSTracker(getActivity());
                    Log.e("call","onRequestPermissionsResult() 22 = ");
                    if (gpsTracker.canGetLocation())
                    {
                        Log.e("call","onRequestPermissionsResult() 33 = ");
                        gpsTracker.getLocation();

                        Constants.newgpsLatitude = gpsTracker.getLatitude() + "";
                        Constants.newgpsLongitude = gpsTracker.getLongitude() + "";
                    }
                    else
                    {
                        Log.e("call","onRequestPermissionsResult() 44 = ");
                    }
                }
                else
                {
                    Log.e("call","onRequestPermissionsResult() 55 = ");
                    MyAlertDialog dialog = new MyAlertDialog(getActivity());
                    dialog.setCancelable(false);
                    dialog.setAlertDialog(1, getResources().getString(R.string.permission_denied_for_location));
                }
                break;
        }
    }

////// https://stackoverflow.com/questions/35554796/rotate-marker-and-move-animation-on-map-like-uber-android
    public static void animateMarkerNew(final Location destination, final Marker marker) {

        if (marker != null)
        {
            readBearing=0;
            final LatLng startPosition = marker.getPosition();
            final LatLng endPosition = new LatLng(destination.getLatitude(), destination.getLongitude());

            final float startRotation = marker.getRotation();
            final LatLngInterpolatorNew latLngInterpolator = new LatLngInterpolatorNew.LinearFixed();

            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(4000); // duration 3 second
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    try {
                        float v = animation.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                        marker.setPosition(newPosition);
                        bearingF = getBearing(startPosition, new LatLng(destination.getLatitude(), destination.getLongitude()));

                        if (intMarkerAnim==0)
                        {
                            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                                    .target(newPosition)
                                    .zoom(17.5f)
//                                    .tilt(30)
                                    .build()));
                            intMarkerAnim=1;
                        }

                        if (!String.valueOf(bearingF).equalsIgnoreCase("NaN"))
                        {
                           /* if (intMarkerAnim==1)
                            {
                                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                                        .target(newPosition)
                                        .zoom(17.5f)
                                        .tilt(30)
                                        .bearing((bearingF+120))
                                        .build()));
                                intMarkerAnim=2;
                            }*/
                            if (readBearing==0)
                            {
                                marker.setRotation(bearingF);
                                marker.setIcon(BitmapDescriptorFactory.fromResource(GetIconMarker()));
                                readBearing=1;
                            }
                        }
                    } catch (Exception ex) {}
                }
            });
            valueAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                }
            });
            valueAnimator.start();
        }
    }

    private interface LatLngInterpolatorNew {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolatorNew {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }
    }


    //Method for finding bearing between two points
    public static float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) ((Math.toDegrees(Math.atan(lng / lat))));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }

    public void getAddress(double lat, double lng)
    {
        Geocoder geocoder = new Geocoder(Drawer_Activity.activity, Locale.getDefault());
        try
        {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            if (addresses.size()>0)
            {
                if (addresses.get(0) != null)
                {
                    tv_location.setVisibility(View.VISIBLE);
                    Address obj = addresses.get(0);
                    String add="";
                    if (obj.getAddressLine(0)!= null && !obj.getAddressLine(0).equalsIgnoreCase(""))
                    {
                        add = obj.getAddressLine(0);
                        Log.e("IGA", "Address1 " + add);
                    }
                    Log.e("IGA", "Address" + add);
                    tv_location.setText(add);
                }
                else
                {
                    tv_location.setVisibility(View.INVISIBLE);
                }
            }
            else
            {
                tv_location.setVisibility(View.INVISIBLE);
            }

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Log.e("IOException","getAddress IOException :"+e.getMessage());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.e("onDestroyView", "onDestroyView onDestroyView onDestroyView");

        /*if (handler!=null)
        {
            handler.removeCallbacksAndMessages(null);
        }*/
    }

    public void SetDriverToPickUpLocation()
    {
        TollFeeApi="";
        flagCameraAnim=0;
        Log.e("wwwwwwww","wwwwwwwwwwwwwwwwww SetDriverToPickUpLocation");
        ll_start_trip_option.setVisibility(View.VISIBLE);
        ll_complete_hold.setVisibility(View.GONE);
        if (SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LAT, Drawer_Activity.activity)!=null
                && !SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LAT, Drawer_Activity.activity).equalsIgnoreCase(""))
        {
            pickUpLat = Double.parseDouble(SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LAT, Drawer_Activity.activity));
        }
        else
        {
            pickUpLat = 0;
        }

        if (SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LNG, Drawer_Activity.activity)!=null
                && !SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LNG, Drawer_Activity.activity).equalsIgnoreCase(""))
        {
            pickUpLng = Double.parseDouble(SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LNG, Drawer_Activity.activity));
        }
        else
        {
            pickUpLng = 0;
        }

        LatLng pickUpLoc = new LatLng(pickUpLat, pickUpLng);
        if (marker_pickUpLocation != null)
        {
            marker_pickUpLocation.remove();
        }
        if (polyline!=null)
        {
            polyline.remove();
        }
        marker_pickUpLocation = googleMap.addMarker(new MarkerOptions().position(pickUpLoc).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)).anchor(0.5f, 1).title("Passenger").snippet("Description"));
//        marker_pickUpLocation = googleMap.addMarker(new MarkerOptions().position(pickUpLoc).icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_passenger)).anchor(0.5f, 1).title("Passenger").snippet("Description"));
        ll_start_trip_option.setVisibility(View.VISIBLE);

        Location location = new Location(LocationManager.GPS_PROVIDER);
        location.setLatitude(Double.parseDouble(Constants.newgpsLatitude));
        location.setLongitude(Double.parseDouble(Constants.newgpsLongitude));
        LatLng currentLocation = new LatLng(Double.parseDouble(Constants.newgpsLatitude), Double.parseDouble(Constants.newgpsLongitude));

        if (marker != null)
        {
            Log.e("marker","marker not null");
            animateMarkerNew(location, marker);
        }
        else
        {
            Log.e("marker","marker nulllllllllllllllllll");
            marker = googleMap.addMarker(new MarkerOptions().position(currentLocation).icon(BitmapDescriptorFactory.fromResource(GetIconMarker())).anchor(0.5f, 0.5f).title("You").snippet("Marker Description"));
            animateMarkerNew(location, marker);
        }



        LatLng origin = new LatLng(Double.parseDouble(Constants.newgpsLatitude), Double.parseDouble(Constants.newgpsLongitude));
        LatLng dest = new LatLng(pickUpLat, pickUpLng);

        // Getting URL to the Google Directions API
        String url = getUrl(origin, dest);
        Log.d("onMapClick", url.toString());
        FetchUrl FetchUrl = new FetchUrl();

        // Start downloading json data from Google Directions API
        FetchUrl.execute(url);
        //move map camera
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(origin));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
    }

    public static int GetIconMarker()
    {
        String modelName = SessionSave.getUserSession(Comman.USER_CAR_MODEL_FOR_MAKER, Drawer_Activity.activity);
        Log.e("modelName ","modelName : "+modelName);

        if (modelName!=null && modelName.equalsIgnoreCase("First Class"))
        {
            return R.drawable.icon_car_firstclass;
        }
        else if (modelName.equalsIgnoreCase("Business Class"))
        {
            return R.drawable.icon_car_business;
        }
        else if (modelName.equalsIgnoreCase("Economy"))
        {
            return R.drawable.icon_car_economy;
        }
        else if (modelName.equalsIgnoreCase("Taxi"))
        {
            return R.drawable.icon_car_taxi;
        }
        else if (modelName.equalsIgnoreCase("LUX-VAN"))
        {
            return R.drawable.icon_car_lux_van;
        }
        else if (modelName.equalsIgnoreCase("Disability"))
        {
            return R.drawable.icon_car_disability;
        }
        else
        {
            return R.drawable.icon_car_business;
        }
    }

    public void SetDriverTo_DropOffLocation(int flag)
    {
        TollFeeApi="";
        flagCameraAnim=1;
        if (marker != null)
        {
            marker.remove();
            marker=null;
        }
        if (marker_pickUpLocation != null)
        {
            marker_pickUpLocation.remove();
            marker_pickUpLocation=null;
        }
        if (marker_dropOffLocation != null)
        {
            marker_dropOffLocation.remove();
            marker_dropOffLocation=null;
        }
        googleMap.clear();

        ll_start_trip_option.setVisibility(View.GONE);
        ll_complete_hold.setVisibility(View.VISIBLE);
        tv_start_trip.setEnabled(true);
        SessionSave.saveUserSession(Comman.DRIVER_START_TRIP_LAT, Constants.newgpsLatitude, Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.DRIVER_START_TRIP_LNG, Constants.newgpsLongitude, Drawer_Activity.activity);

        if (flag==0)
        {
            ((Drawer_Activity)getActivity()).StartTrip(SessionSave.getUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, Drawer_Activity.activity)
                    , SessionSave.getUserSession(Comman.USER_ID, Drawer_Activity.activity));
        }

       /* if (SessionSave.getUserSession(Comman.START_WAITING, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.START_WAITING, Drawer_Activity.activity).equalsIgnoreCase("")
                && SessionSave.getUserSession(Comman.START_WAITING, Drawer_Activity.activity).equalsIgnoreCase("1"))
        {
            tv_start_waiting.setVisibility(View.GONE);
            tv_stop_waiting.setVisibility(View.VISIBLE);
        }
        else if (SessionSave.getUserSession(Comman.STOP_WAITING, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.STOP_WAITING, Drawer_Activity.activity).equalsIgnoreCase("")
                && SessionSave.getUserSession(Comman.STOP_WAITING, Drawer_Activity.activity).equalsIgnoreCase("1"))
        {
            tv_start_waiting.setVisibility(View.VISIBLE);
            tv_stop_waiting.setVisibility(View.GONE);
        }
        else
        {
            tv_start_waiting.setVisibility(View.VISIBLE);
            tv_stop_waiting.setVisibility(View.GONE);
        }*/

        if (SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LAT, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LAT, Drawer_Activity.activity).equalsIgnoreCase(""))
        {
            DropOffLat = Double.parseDouble(SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LAT, Drawer_Activity.activity));
        }
        else
        {
            DropOffLat = 0;
        }

        if (SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LNG, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LNG, Drawer_Activity.activity).equalsIgnoreCase(""))
        {
            DropOffLng = Double.parseDouble(SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LNG, Drawer_Activity.activity));
        }
        else
        {
            DropOffLng = 0;
        }

        LatLng DropUpLoc = new LatLng(DropOffLat, DropOffLng);
        /*if (marker != null)
        {
            marker.remove();
            marker=null;
        }*/

        if (polyline!=null)
        {
            polyline.remove();
        }

        LatLng currentLocation = new LatLng(Double.parseDouble(Constants.newgpsLatitude), Double.parseDouble(Constants.newgpsLongitude));

//        marker = googleMap.addMarker(new MarkerOptions().position(currentLocation).icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_car_from_top1)).rotation(90).anchor(0.5f, 0.5f).title("You").snippet("Marker Description"));
        marker_dropOffLocation = googleMap.addMarker(new MarkerOptions().position(DropUpLoc).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)).anchor(0.5f, 1).title("Passenger").snippet("Description"));

        Location location = new Location(LocationManager.GPS_PROVIDER);
        location.setLatitude(Double.parseDouble(Constants.newgpsLatitude));
        location.setLongitude(Double.parseDouble(Constants.newgpsLongitude));

        if (marker != null)
        {
            Log.e("marker","marker not null");
            animateMarkerNew(location, marker);
        }
        else
        {
            Log.e("marker","marker nulllllllllllllllllll");
            marker = googleMap.addMarker(new MarkerOptions().position(currentLocation).icon(BitmapDescriptorFactory.fromResource(GetIconMarker())).anchor(0.5f, 0.5f).title("You").snippet("Marker Description"));
            animateMarkerNew(location, marker);
        }


        // Checks, whether start and end locations are captured
        LatLng origin = new LatLng(Double.parseDouble(Constants.newgpsLatitude), Double.parseDouble(Constants.newgpsLongitude));
        LatLng dest = new LatLng(DropOffLat, DropOffLng);

        // Getting URL to the Google Directions API
        String url = getUrl(origin, dest);
        Log.d("onMapClick", url.toString());
        FetchUrl FetchUrl = new FetchUrl();

        // Start downloading json data from Google Directions API
        FetchUrl.execute(url);
        //move map camera
        /*googleMap.moveCamera(CameraUpdateFactory.newLatLng(origin));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));*/

        ll_start_trip_option.setVisibility(View.GONE);
    }

    private String getUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;


        // Sensor enabled
        String sensor = "sensor=false";

        String key = "key="+ Drawer_Activity.activity.getResources().getString(R.string.api_key_google_map);
        // Building the parameters to the web service
        String parameters = str_origin + "&" + key + "&" +  str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
        return url;
    }

    // Fetches data from url passed
    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data);

            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            if (result!=null)
            {
                try
                {
                    JSONObject resultOb = new JSONObject(result);
                    if (resultOb.has("status"))
                    {
                        String status = resultOb.getString("status");
                        if (status!=null)
                        {
                            if (!status.equalsIgnoreCase("OVER_QUERY_LIMIT"))
                            {
                                parserTask.execute(result);
                            }
                            else
                            {
                                /*if (SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, Drawer_Activity.activity).equalsIgnoreCase("")
                                        && SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG,Drawer_Activity.activity).equalsIgnoreCase("1"))
                                {
                                    if (SessionSave.getUserSession(Comman.START_TRIP_FLAG,Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity).equalsIgnoreCase("")
                                            && SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity).equalsIgnoreCase("1"))
                                    {
                                        SetDriverTo_DropOffLocation(1);
                                    }
                                    else
                                    {
                                        SetDriverToPickUpLocation();
                                    }
                                }*/
                            }
                        }
                        else
                        {
                            /*if (SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, Drawer_Activity.activity).equalsIgnoreCase("")
                                    && SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, Drawer_Activity.activity).equalsIgnoreCase("1"))
                            {
                                if (SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity).equalsIgnoreCase("")
                                        && SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity).equalsIgnoreCase("1"))
                                {
                                    SetDriverTo_DropOffLocation(1);
                                }
                                else
                                {
                                    SetDriverToPickUpLocation();
                                }
                            }*/
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("wwwwwwww","wwwwwwwwwwwwwwwwww JSONException 7777777777777"+e.getMessage());
                    /*if (SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, Drawer_Activity.activity).equalsIgnoreCase("")
                            && SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, Drawer_Activity.activity).equalsIgnoreCase("1"))
                    {
                        if (SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity).equalsIgnoreCase("")
                                && SessionSave.getUserSession(Comman.START_TRIP_FLAG, Drawer_Activity.activity).equalsIgnoreCase("1"))
                        {
                            SetDriverTo_DropOffLocation(1);
                        }
                        else
                        {
                            SetDriverToPickUpLocation();
                        }
                    }*/
                }
            }
        }
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.d("downloadUrl", data.toString());
            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask",jsonData[0].toString());
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());

                // Starts parsing data
                routes = parser.parse(jObject);
                Log.d("ParserTask","Executing routes");
                Log.d("ParserTask",routes.toString());

            } catch (Exception e) {
                Log.d("ParserTask",e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;

            PolylineOptions lineOptions = null;

            LatLngBounds.Builder builder = new LatLngBounds.Builder();

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    builder.include(position);
                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(13);
                lineOptions.color(ContextCompat.getColor(Drawer_Activity.activity, R.color.colorPolyline));

                Log.d("onPostExecute","onPostExecute lineoptions decoded");

            }

            // Drawing polyline in the Google Map for the i-th route
            if(lineOptions != null) {
                polyline = googleMap.addPolyline(lineOptions);

                if (flagCameraAnim==1)
                {
                    Log.e("flagCameraAnim","11111111111111111");
                    LatLngBounds bounds = builder.build();
                    int padding = 200; // offset from edges of the map in pixels
                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                    googleMap.animateCamera(cu);
                }
                else
                {
                    Log.e("flagCameraAnim","2222222222222222");
                    double latitude1 = Double.parseDouble(Constants.newgpsLatitude);
                    double longitude1 = Double.parseDouble(Constants.newgpsLongitude);

                    sydney=null;
                    sydney = new LatLng(latitude1, longitude1);

                    CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(15).build();
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                }
            }
            else {
                Log.d("onPostExecute","without Polylines drawn");
            }
        }
    }


    public void getDistanceBetween_TwoLoc()
    {
        SessionSave.saveUserSession(Comman.DRIVER_END_TRIP_LAT, Constants.newgpsLatitude, Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.DRIVER_END_TRIP_LNG, Constants.newgpsLongitude, Drawer_Activity.activity);
        double lat1 = 0, lng1 = 0, lat2 = 0, lng2 = 0;

        if (SessionSave.getUserSession(Comman.DRIVER_START_TRIP_LAT,getActivity())!=null &&!SessionSave.getUserSession(Comman.DRIVER_START_TRIP_LAT,getActivity()).equalsIgnoreCase(""))
        {
            lat1= Double.parseDouble(SessionSave.getUserSession(Comman.DRIVER_START_TRIP_LAT,getActivity()));
        }
        if (SessionSave.getUserSession(Comman.DRIVER_START_TRIP_LNG,getActivity())!=null &&!SessionSave.getUserSession(Comman.DRIVER_START_TRIP_LNG,getActivity()).equalsIgnoreCase(""))
        {
            lng1= Double.parseDouble(SessionSave.getUserSession(Comman.DRIVER_START_TRIP_LNG,getActivity()));
        }
        if (SessionSave.getUserSession(Comman.DRIVER_END_TRIP_LAT,getActivity())!=null &&!SessionSave.getUserSession(Comman.DRIVER_END_TRIP_LAT,getActivity()).equalsIgnoreCase(""))
        {
            lat2= Double.parseDouble(SessionSave.getUserSession(Comman.DRIVER_END_TRIP_LAT,getActivity()));
        }
        if (SessionSave.getUserSession(Comman.DRIVER_END_TRIP_LNG,getActivity())!=null &&!SessionSave.getUserSession(Comman.DRIVER_END_TRIP_LNG,getActivity()).equalsIgnoreCase(""))
        {
            lng2= Double.parseDouble(SessionSave.getUserSession(Comman.DRIVER_END_TRIP_LNG,getActivity()));
        }
        /*if (SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LAT,getActivity())!=null &&!SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LAT,getActivity()).equalsIgnoreCase(""))
        {
            lat1= Double.parseDouble(SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LAT,getActivity()));
        }
        if (SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LNG,getActivity())!=null &&!SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LNG,getActivity()).equalsIgnoreCase(""))
        {
            lng1= Double.parseDouble(SessionSave.getUserSession(Comman.PASSENGER_PICKUP_LNG,getActivity()));
        }
        if (SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LAT,getActivity())!=null &&!SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LAT,getActivity()).equalsIgnoreCase(""))
        {
            lat2= Double.parseDouble(SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LAT,getActivity()));
        }
        if (SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LNG,getActivity())!=null &&!SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LNG,getActivity()).equalsIgnoreCase(""))
        {
            lng2= Double.parseDouble(SessionSave.getUserSession(Comman.PASSENGER_DROP_OFF_LNG,getActivity()));
        }*/


        Log.e("getDistanceBetween","lat1 : "+lat1+"\nlng1 : "+lng1+"\nlat2 : "+lat2+"\nlng2 : "+lng2);
        if (lat1!=0 && lng1!=0 && lat2!=0 && lng2!=0)
        {
//            LatLng pickUpLoc = new LatLng(lat1, lng1);
//            LatLng dropOffLoc = new LatLng(lat2, lng2);

            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(getActivity(), Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(lat2, lng2, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                dropOffLocationApi = addresses.get(0).getAddressLine(0);
            } catch (IOException e) {
                e.printStackTrace();
                dropOffLocationApi ="";
            }

            Log.e("getDistanceBetween","dropOffLocationApi : "+dropOffLocationApi);

//            String url = getUrl(pickUpLoc, dropOffLoc);
//            Log.e("getDistanceBetween","URL : "+ url.toString());

            Call_CompleteTrip(String.valueOf(distance), lat2, lng2);

//            FetchUrlForDistance FetchUrl = new FetchUrlForDistance();
//
//             //Start downloading json data from Google Directions API
//            FetchUrl.execute(url);
        }
        else
        {
            new SnackbarUtils(main_layout, Drawer_Activity.activity.getResources().getString(R.string.restart_app_again),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
    }

/*
    private class FetchUrlForDistance extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data);

                final JSONObject json = new JSONObject(data);
                JSONArray routeArray = json.getJSONArray("routes");
                JSONObject routes = routeArray.getJSONObject(0);

                JSONArray newTempARr = routes.getJSONArray("legs");
                JSONObject newDisTimeOb = newTempARr.getJSONObject(0);

                JSONObject distOb = newDisTimeOb.getJSONObject("distance");
                JSONObject timeOb = newDisTimeOb.getJSONObject("duration");

                Log.i("Diatance :", distOb.getString("text"));
                Log.i("Time :", timeOb.getString("text"));

                distance = distOb.getString("text");
                if (distance.contains("km"))
                {
                    String str = distance;
                    String[] splited = str.split("\\s+");
                    distance = splited[0];
                }
                else
                {
                    String str = distance;
                    String[] splited = str.split("\\s+");
                    distance = String.valueOf(Double.parseDouble(splited[0])/1000.00);
                }
                Log.e("getDistanceBetween","kkkkkkkkkkkkk distance :"+distance);

            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            JSONObject json = null;
            try {
                json = new JSONObject(result);
                if (json!=null && json.has("status") && !json.getString("status").equalsIgnoreCase("") && !json.getString("status").equalsIgnoreCase("OVER_QUERY_LIMIT"))
                {
                    dialogClass.showDialog();
                    if (distance!=null && !distance.equalsIgnoreCase(null) && !distance.equalsIgnoreCase(""))
                    {

                    }
                    else
                    {
                        Call_CompleteTrip(String.valueOf(0));
                    }
                }
                else
                {
                    dialogClass.showDialog();
                    getDistanceBetween_TwoLoc();
                }

            } catch (JSONException e) {
                e.printStackTrace();
                dialogClass.showDialog();
                getDistanceBetween_TwoLoc();
            }
        }
    }
*/


    private void Call_CompleteTrip(String distance, double lat, double lng)
    {
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();
        String url;
        if (SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, Drawer_Activity.activity).equalsIgnoreCase(""))
        {
            if (SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, Drawer_Activity.activity).equalsIgnoreCase("0"))
            {
                url = WebServiceAPI.WEB_SERVICE_COMPLETE_TRIP;
            }
            else
            {
                url = WebServiceAPI.WEB_SERVICE_COMPLETE_TRIP_LATER;
            }

            String PaymentType = SessionSave.getUserSession(Comman.PASSENGER_PAYMENT_TYPE, Drawer_Activity.activity);
            if (PaymentType!=null && !PaymentType.equalsIgnoreCase(""))
            {
                if (PaymentType.equalsIgnoreCase("collect"))
                {
                    PaymentType = "cash";
                }
            }
            else
            {
                PaymentType="cash";
            }

//            params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_TRIP_DISTANCE, distance);
            params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_BOOKING_ID, SessionSave.getUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, Drawer_Activity.activity));
            params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_NIGHT_FARE_APPLICABLE, "0");
            params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_PROMO_CODE, "");
            params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_PAYMENT_TYPE, PaymentType);
            params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_PAYMENT_STATUS, "");
            params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_TRANSACTION_ID, "");
            params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_LAT, lat);
            params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_LONG, lng);

            if (TollFeeApi!=null && !TollFeeApi.equalsIgnoreCase(""))
            {
                params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_TOLL_FEE, TollFeeApi);
            }
            else
            {
                params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_TOLL_FEE, "0");
            }

            if (dropOffLocationApi!=null && !dropOffLocationApi.equalsIgnoreCase(""))
            {
                params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_DROP_OFF_LOCATION, dropOffLocationApi);
            }
            else
            {
                params.put(WebServiceAPI.COMPLETE_TRIP_PARAM_DROP_OFF_LOCATION, "");
            }


            Log.e("url", "Call_CompleteTrip = " + url);
            Log.e("param", "Call_CompleteTrip = " + params);


            aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {

                    try {
                        int responseCode = status.getCode();
                        Log.e("responseCode", "Call_CompleteTrip = " + responseCode);
                        Log.e("Response", "Call_CompleteTrip = " + json);

                        if (json != null)
                        {
                            if (json.has("status"))
                            {
                                if (json.getBoolean("status"))
                                {
                                    Log.e("status", "true");
                                    if (marker != null)
                                    {
                                        marker.remove();
                                        marker=null;
                                    }
                                    if (marker_pickUpLocation != null)
                                    {
                                        marker_pickUpLocation.remove();
                                        marker_pickUpLocation=null;
                                    }
                                    if (marker_dropOffLocation != null)
                                    {
                                        marker_dropOffLocation.remove();
                                        marker_dropOffLocation=null;
                                    }

                                    googleMap.clear();
                                    intMarkerAnim=0;

                                    ll_start_trip_option.setVisibility(View.GONE);
                                    ll_complete_hold.setVisibility(View.GONE);
                                    dialogClass.hideDialog();

                                    SessionSave.saveUserSession(Comman.ACCEPT_REQUEST_FLAG, "0", Drawer_Activity.activity);
                                    SessionSave.saveUserSession(Comman.START_TRIP_FLAG, "0", Drawer_Activity.activity);
                                    SessionSave.saveUserSession(Comman.STOP_WAITING, "0", Drawer_Activity.activity);
                                    SessionSave.saveUserSession(Comman.START_WAITING, "0", Drawer_Activity.activity);
                                    //SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "0", Drawer_Activity.activity);

                                    String PaymentType = SessionSave.getUserSession(Comman.PASSENGER_PAYMENT_TYPE, Drawer_Activity.activity);
                                    final String PassType = SessionSave.getUserSession(Comman.PASSENGER_TYPE, Drawer_Activity.activity);
                                    Log.e("PassType","PassType : "+PassType);
                                    Log.e("PaymentType","PaymentType : "+PaymentType);

                                    final Dialog dialogTy = new Dialog(Drawer_Activity.activity, R.style.DialogTheme);
                                    dialogTy.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    lp.copyFrom(dialogTy.getWindow().getAttributes());
                                    lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
                                    lp.height = LinearLayout.LayoutParams.MATCH_PARENT;
                                    dialogTy.getWindow().setAttributes(lp);
                                    dialogTy.setCancelable(false);
                                    dialogTy.setContentView(R.layout.dialog_receive_cancel_trip);

                                    TextView tv_dialog_ok, tv_title, tv_message;

                                    tv_title = (TextView) dialogTy.findViewById(R.id.tv_title);
                                    tv_dialog_ok = (TextView) dialogTy.findViewById(R.id.tv_dialog_ok);
                                    tv_message = (TextView) dialogTy.findViewById(R.id.tv_message);

                                    tv_title.setText("Alert!    This is a cash job");
                                    tv_message.setText("Please Collect Money From Passenger");


                                    final Dialog dialogCompleteTrip = new Dialog(getActivity(), R.style.DialogTheme);
                                    dialogCompleteTrip.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    WindowManager.LayoutParams lp1 = new WindowManager.LayoutParams();
                                    lp1.copyFrom(dialogCompleteTrip.getWindow().getAttributes());
                                    lp1.width = LinearLayout.LayoutParams.MATCH_PARENT;
                                    lp1.height = LinearLayout.LayoutParams.MATCH_PARENT;
                                    dialogCompleteTrip.getWindow().setAttributes(lp1);
                                    dialogCompleteTrip.setCancelable(false);
                                    dialogCompleteTrip.setContentView(R.layout.dialog_show_complete_trip_details);

                                    TextView tv_drop_off_location, tv_pick_up_location, tv_dialog_ok1, tv_trip_fare, tv_waiting_time_cost, tv_toll_fee, tv_booking_charge, tv_tax, tv_discount, tv_sub_total, tv_grand_total, tv_distanceFare, tv_nightFare;

                                    tv_drop_off_location = (TextView) dialogCompleteTrip.findViewById(R.id.tv_drop_off_location);
                                    tv_pick_up_location = (TextView) dialogCompleteTrip.findViewById(R.id.tv_pick_up_location);
                                    tv_trip_fare = (TextView) dialogCompleteTrip.findViewById(R.id.tv_trip_fare);
                                    tv_waiting_time_cost = (TextView) dialogCompleteTrip.findViewById(R.id.tv_waiting_time_cost);
                                    tv_toll_fee = (TextView) dialogCompleteTrip.findViewById(R.id.tv_toll_fee);
                                    tv_tax = (TextView) dialogCompleteTrip.findViewById(R.id.tv_tax);
                                    tv_booking_charge = (TextView) dialogCompleteTrip.findViewById(R.id.tv_booking_charge);
                                    tv_discount = (TextView) dialogCompleteTrip.findViewById(R.id.tv_discount);
                                    tv_sub_total = (TextView) dialogCompleteTrip.findViewById(R.id.tv_sub_total);
                                    tv_grand_total = (TextView) dialogCompleteTrip.findViewById(R.id.tv_grand_total);
                                    tv_dialog_ok1 = (TextView) dialogCompleteTrip.findViewById(R.id.tv_dialog_ok);
                                    tv_distanceFare = (TextView) dialogCompleteTrip.findViewById(R.id.tv_distanceFare);
                                    tv_nightFare = (TextView) dialogCompleteTrip.findViewById(R.id.tv_nightFare);

                                    tv_dialog_ok1.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            ClickRelocateButton = 0;
                                            dialogCompleteTrip.dismiss();
                                            if (PassType != null && !PassType.equalsIgnoreCase("") && (PassType.equalsIgnoreCase("others") || PassType.equalsIgnoreCase("other"))) {
                                                ClearTripSession();

                                                Location location = new Location(LocationManager.GPS_PROVIDER);
                                                location.setLatitude(Double.parseDouble(Constants.newgpsLatitude));
                                                location.setLongitude(Double.parseDouble(Constants.newgpsLongitude));
                                                LatLng currentLocation = new LatLng(Double.parseDouble(Constants.newgpsLatitude), Double.parseDouble(Constants.newgpsLongitude));

                                                if (marker != null) {
                                                    Log.e("marker", "marker not null");
                                                    animateMarkerNew(location, marker);
                                                } else {
                                                    Log.e("marker", "marker nulllllllllllllllllll");
                                                    marker = googleMap.addMarker(new MarkerOptions().position(currentLocation).icon(BitmapDescriptorFactory.fromResource(GetIconMarker())).anchor(0.5f, 0.5f).title("You").snippet("Marker Description"));
                                                    animateMarkerNew(location, marker);
                                                }
                                            } else {
                                                showRatingDialog();
                                            }
                                        }
                                    });

                                    tv_complete_trip.setEnabled(true);

                                    if (json.has("details")) {
                                        JSONObject detailsObject = json.getJSONObject("details");
                                        if (detailsObject != null) {
                                            String PickupLocation = "", DropoffLocation = "", TripFare = "", WaitingTimeCost = "", TollFee = "", BookingCharge = "", Tax = "", Discount = "", SubTotal = "", GrandTotal = "", DistanceFare = "", NightFare = "";
                                            if (detailsObject.has("PickupLocation"))
                                            {
                                                PickupLocation = detailsObject.getString("PickupLocation");
                                                tv_pick_up_location.setText(PickupLocation);
                                            }
                                            if (detailsObject.has("DropoffLocation")) {
                                                DropoffLocation = detailsObject.getString("DropoffLocation");
                                                tv_drop_off_location.setText(DropoffLocation);
                                            }
                                            if (detailsObject.has("TripFare")) {
                                                TripFare = detailsObject.getString("TripFare");
                                                tv_trip_fare.setText(TripFare);
                                            }
                                            if (detailsObject.has("DistanceFare")) {
                                                DistanceFare = detailsObject.getString("DistanceFare");
                                                tv_distanceFare.setText(DistanceFare);
                                            }
                                            if (detailsObject.has("WaitingTimeCost")) {
                                                WaitingTimeCost = detailsObject.getString("WaitingTimeCost");
                                                tv_waiting_time_cost.setText(WaitingTimeCost);
                                            }
                                            if (detailsObject.has("NightFare")) {
                                                NightFare = detailsObject.getString("NightFare");
                                                tv_nightFare.setText(NightFare);
                                            }
                                            if (detailsObject.has("TollFee")) {
                                                TollFee = detailsObject.getString("TollFee");
                                                tv_toll_fee.setText(TollFee);
                                            }
                                            if (detailsObject.has("BookingCharge")) {
                                                BookingCharge = detailsObject.getString("BookingCharge");
                                                tv_booking_charge.setText(BookingCharge);
                                            }
                                            if (detailsObject.has("Tax")) {
                                                Tax = detailsObject.getString("Tax");
                                                tv_tax.setText(Tax);
                                            }
                                            if (detailsObject.has("Discount")) {
                                                Discount = detailsObject.getString("Discount");
                                                tv_discount.setText(Discount);
                                            }
                                            if (detailsObject.has("SubTotal")) {
                                                SubTotal = detailsObject.getString("SubTotal");
                                                tv_sub_total.setText(SubTotal);
                                            }
                                            if (detailsObject.has("GrandTotal")) {
                                                GrandTotal = detailsObject.getString("GrandTotal");
                                                tv_grand_total.setText(GrandTotal);
                                            }
                                        }


                                    tv_dialog_ok.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            ringTone.stop();
                                            dialogTy.dismiss();
                                            dialogCompleteTrip.show();
                                    }
                                    });


                                        if (PaymentType!=null && !PaymentType.equalsIgnoreCase("") && (PaymentType.equalsIgnoreCase("cash") || PaymentType.equalsIgnoreCase("collect")))
                                        {
                                            ringTone= MediaPlayer.create(Drawer_Activity.activity,R.raw.request_ring);
                                            ringTone.setLooping(true);
                                            ringTone.start();
                                            dialogTy.show();
                                        }
                                        else
                                        {
                                            dialogClass.hideDialog();
                                            tv_complete_trip.setEnabled(true);
                                            dialogCompleteTrip.show();
                                        }

                                        dialogClass.hideDialog();
                                        tv_complete_trip.setEnabled(true);
                                    }
                                    dialogClass.hideDialog();
                                    tv_complete_trip.setEnabled(true);
                                }
                                else
                                {
                                    Log.e("status", "false");
                                    ll_start_trip_option.setVisibility(View.GONE);
                                    ll_complete_hold.setVisibility(View.VISIBLE);
                                    tv_complete_trip.setEnabled(true);
                                    dialogClass.hideDialog();
                                    if (json.has("message")) {
                                        new SnackbarUtils(main_layout, json.getString("message"),
                                                ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                                    }
                                }
                            } else {
                                Log.e("json", "no status found");
                                dialogClass.hideDialog();
                                ll_start_trip_option.setVisibility(View.GONE);
                                ll_complete_hold.setVisibility(View.VISIBLE);
                                tv_complete_trip.setEnabled(true);
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        } else {
                            Log.e("json", "null");
                            dialogClass.hideDialog();
                            ll_start_trip_option.setVisibility(View.GONE);
                            ll_complete_hold.setVisibility(View.VISIBLE);
                            tv_complete_trip.setEnabled(true);

                            new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                        }
                    } catch (Exception e) {
                        Log.e("Call_CompleteTrip", "Exception : " + e.toString());
                        dialogClass.hideDialog();
                        ll_start_trip_option.setVisibility(View.GONE);
                        ll_complete_hold.setVisibility(View.VISIBLE);
                        tv_complete_trip.setEnabled(true);
                        new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                    }
                }
            }.header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
        }
    }

    private void showRatingDialog()
    {
        final Dialog dialogRating = new Dialog(getActivity(), R.style.DialogTheme);
        dialogRating.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lpRating = new WindowManager.LayoutParams();
        lpRating.copyFrom(dialogRating.getWindow().getAttributes());
        lpRating.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lpRating.height = LinearLayout.LayoutParams.MATCH_PARENT;
        dialogRating.getWindow().setAttributes(lpRating);
        dialogRating.setCancelable(false);
        dialogRating.setContentView(R.layout.dialog_rating);

        TextView tv_submit, tv_maybe_later, tv_message_Rating;
        final EditText et_description;
        final RatingBar ratingBar;

        et_description = (EditText) dialogRating.findViewById(R.id.et_description);
        tv_submit = (TextView) dialogRating.findViewById(R.id.tv_submit);
        tv_maybe_later = (TextView) dialogRating.findViewById(R.id.tv_maybe_later);
        tv_message_Rating = (TextView) dialogRating.findViewById(R.id.tv_message_Rating);
        tv_maybe_later.setVisibility(View.GONE);
        ratingBar = (RatingBar) dialogRating.findViewById(R.id.ratingBar);


        String passengerName = SessionSave.getUserSession(Comman.PASSENGER_NAME, Drawer_Activity.activity);
        if (passengerName!=null && !passengerName.equalsIgnoreCase(""))
        {
            tv_message_Rating.setText("How was your experience with "+passengerName+"?");
        }
        else
        {
            tv_message_Rating.setText("How was your experience with Passenger?");
        }

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ratingBar.getRating();
                if (TextUtils.isEmpty(et_description.getText().toString().trim()))
                {
                    callRateMethod(dialogRating, ratingBar.getRating(), "");
                }
                else
                {
                    callRateMethod(dialogRating, ratingBar.getRating(), et_description.getText().toString());
                }
            }
        });

        tv_maybe_later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogRating.hide();
            }
        });

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                Log.e("RatingBar","RatingBar rating : "+rating);
            }
        });

        dialogRating.show();
    }

    private void callRateMethod(final Dialog dialogRating, float rating, String description)
    {
        Log.e("callRateMethod","callRateMethod description : "+ description);
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();
        String url = WebServiceAPI.WEB_SERVICE_RATING_AND_COMMENT;

        if (SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, Drawer_Activity.activity)!=null && !SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, Drawer_Activity.activity).equalsIgnoreCase(""))
        {
            if (SessionSave.getUserSession(Comman.BOOKING_REQUEST_FOR, Drawer_Activity.activity).equalsIgnoreCase("0"))
            {
                params.put(WebServiceAPI.PARAM_RATING_AND_COMMENT_BOOKING_TYPE, "BookNow");
            }
            else
            {
                params.put(WebServiceAPI.PARAM_RATING_AND_COMMENT_BOOKING_TYPE, "BookLater");
            }

            if (description!=null && !description.equalsIgnoreCase(""))
            {
                params.put(WebServiceAPI.PARAM_RATING_AND_COMMENT_DRESCRIPTION, description);
            }

            params.put(WebServiceAPI.PARAM_RATING_AND_COMMENT_BOOKING_ID, SessionSave.getUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, Drawer_Activity.activity));
            params.put(WebServiceAPI.PARAM_RATING_AND_COMMENT_RATING, rating+"");

            Log.e("url", "callRateMethod = " + url);
            Log.e("param", "callRateMethod = " + params);


            aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {

                    try {
                        int responseCode = status.getCode();
                        Log.e("responseCode", "callRateMethod = " + responseCode);
                        Log.e("Response", "callRateMethod = " + json);

                        if (json != null)
                        {
                            if (json.has("status"))
                            {
                                if (json.getBoolean("status"))
                                {
                                    Log.e("status", "true");
                                    dialogClass.hideDialog();
                                    dialogRating.hide();
                                    dialogRating.dismiss();
//                                    dialog.show();
                                    ClearTripSession();

                                    Location location = new Location(LocationManager.GPS_PROVIDER);
                                    location.setLatitude(Double.parseDouble(Constants.newgpsLatitude));
                                    location.setLongitude(Double.parseDouble(Constants.newgpsLongitude));
                                    LatLng currentLocation = new LatLng(Double.parseDouble(Constants.newgpsLatitude), Double.parseDouble(Constants.newgpsLongitude));

                                    if (marker != null)
                                    {
                                        Log.e("marker","marker not null");
                                        animateMarkerNew(location, marker);
                                    }
                                    else
                                    {
                                        Log.e("marker","marker nulllllllllllllllllll");
                                        marker = googleMap.addMarker(new MarkerOptions().position(currentLocation).icon(BitmapDescriptorFactory.fromResource(GetIconMarker())).anchor(0.5f, 0.5f).title("You").snippet("Marker Description"));
                                        animateMarkerNew(location, marker);
                                    }
                                }
                                else
                                {
                                    Log.e("status", "false");
                                    dialogClass.hideDialog();
                                    if (json.has("message")) {
                                        new SnackbarUtils(main_layout, json.getString("message"),
                                                ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                                    }
                                }
                            } else {
                                Log.e("json", "no status found");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        } else {
                            Log.e("json", "null");
                            dialogClass.hideDialog();
                            new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                        }
                    } catch (Exception e) {
                        Log.e("callRateMethod", "Exception : " + e.toString());
                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                    }
                }
            }.header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
        mKalmanLocationManager.requestLocationUpdates(KalmanLocationManager.UseProvider.GPS_AND_NET, FILTER_TIME, GPS_TIME, NET_TIME, mLocationListener, true);
    }



    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    private LocationListener mLocationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {

            Log.e("Change_DriverMarker","Change_DriverMarkerPosition() location.getLatitude(): "+location.getLatitude()+"\n location.getLongitude() : "+location.getLongitude());
            Log.e("wowwwwww","11111111111111111111111");
            // GPS location
            if (location.getProvider().equals(LocationManager.GPS_PROVIDER))
            {
                Log.e("Change_DriverMarker","2222222222222");

            }
            else if (location.getProvider().equals(LocationManager.NETWORK_PROVIDER))  // Network location
            {
                Log.e("wowwwwww","333333333333");
            }
            else
            {
                Log.e("wowwwwww","444444444");
                if (location.getLatitude()!=0)
                {
                    Constants.newgpsLatitude = location.getLatitude() + "";
                    Constants.newgpsLongitude = location.getLongitude() + "";

                    Log.e("Change_DriverMarker","location getLatitude()"+ "rrrrrrrrrrrrrr" + location.getLatitude());
                    Log.e("Change_DriverMarker","location getLongitude()"+ "rrrrrrrrrrrrrr" + location.getLongitude());

                    sydney=null;
                    sydney = new LatLng(location.getLatitude(), location.getLongitude());

                    if (marker != null)
                    {
                        Log.e("Change_DriverMarker","marker" + "marker not null");
                        if (marker.isVisible())
                        {
                            animateMarkerNew(location, marker);
                        }
                        else
                        {
                            Log.e("markermarker","marker setVisible: "+marker.isVisible());
                            marker.setVisible(true);
                        }

                    }
                    else
                    {
                        Log.e("Change_DriverMarker", "marker" + "marker nulllllllllllllllllll");
                        marker = googleMap.addMarker(new MarkerOptions().position(sydney).icon(BitmapDescriptorFactory.fromResource(GetIconMarker())).anchor(0.5f, 0.5f).title("You").snippet("Marker Description"));
                        animateMarkerNew(location, marker);
                    }
                    getAddress(location.getLatitude(), location.getLongitude());
                    ((Drawer_Activity)getActivity()).startTimer();
                }
            }

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

            String statusString = "Unknown";

            switch (status) {

                case LocationProvider.OUT_OF_SERVICE:
                    statusString = "Out of service";
                    break;

                case LocationProvider.TEMPORARILY_UNAVAILABLE:
                    statusString = "Temporary unavailable";
                    break;

                case LocationProvider.AVAILABLE:
                    statusString = "Available";
                    break;
            }

            Log.e("mKalmanLocationManager","onStatusChanged : "+statusString);
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.e("mKalmanLocationManager","onProviderEnabled : "+provider);
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.e("mKalmanLocationManager","onProviderDisabled : "+provider);
        }
    };



    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
        if (marker!=null)
        {
            marker.remove();
            marker=null;
        }
        if (marker_pickUpLocation!=null)
        {
            marker_pickUpLocation.remove();
            marker_pickUpLocation=null;
        }
        if (marker_dropOffLocation!=null)
        {
            marker_dropOffLocation.remove();
            marker_dropOffLocation=null;
        }
        mKalmanLocationManager.removeUpdates(mLocationListener);
    }

    public static void ClearTripSession()
    {
        SessionSave.saveUserSession(Comman.USER_SOCKET_BOOKING_REQUEST_BOOKING_ID, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_ID, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LOCATION, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LAT, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_PICKUP_LNG, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LOCATION, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LAT, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_DROP_OFF_LNG, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.DRIVER_START_TRIP_LAT, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.DRIVER_START_TRIP_LNG, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.DRIVER_END_TRIP_LAT, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.DRIVER_END_TRIP_LNG, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_NAME, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_MOBILE_NO, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_IMAGE, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_NOTE, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_FLIGHT_NO, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_TYPE, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_MOB_NO_OTHER, "", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.ACCEPT_REQUEST_FLAG, "0", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.START_TRIP_FLAG, "0",  Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.STOP_WAITING, "0", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.START_WAITING, "0", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "0", Drawer_Activity.activity);
        SessionSave.saveUserSession(Comman.PASSENGER_BOOKING_TYPE, "", Drawer_Activity.activity);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}
