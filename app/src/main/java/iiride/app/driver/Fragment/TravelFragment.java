package iiride.app.driver.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import iiride.app.driver.Activity.WebViewActivity;
import iiride.app.driver.R;


public class TravelFragment extends Fragment implements View.OnClickListener {

    private LinearLayout ll_train, ll_bus, ll_tram, ll_vline, ll_ferry, ll_flight;
    private String title, url;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        final View rootView = inflater.inflate(R.layout.fragment_travel, container,false);

        ll_train = (LinearLayout) rootView.findViewById(R.id.ll_train);
        ll_bus = (LinearLayout) rootView.findViewById(R.id.ll_bus);
        ll_tram = (LinearLayout) rootView.findViewById(R.id.ll_tram);
        ll_vline = (LinearLayout) rootView.findViewById(R.id.ll_vline);
        ll_ferry = (LinearLayout) rootView.findViewById(R.id.ll_ferry);
        ll_flight = (LinearLayout) rootView.findViewById(R.id.ll_flight);


        ll_train.setOnClickListener(this);
        ll_bus.setOnClickListener(this);
        ll_tram.setOnClickListener(this);
        ll_vline.setOnClickListener(this);
        ll_ferry.setOnClickListener(this);
        ll_flight.setOnClickListener(this);


        // Inflate the layout for this fragment
        return rootView;


    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.ll_train:

                url = "http://www.metrotrains.com.au/";
                title = "TRAIN";

                intentWebview(url, title);

                break;

            case R.id.ll_bus:

                url = "https://www.ptv.vic.gov.au/projects/buses/";
                title = "BUS";

                intentWebview(url, title);


                break;

            case R.id.ll_tram:

                url = "https://www.ptv.vic.gov.au/getting-around/maps/metropolitan-trams/";
                title = "TRAM";

                intentWebview(url, title);
                break;

            case R.id.ll_vline:

                url = "https://www.vline.com.au/";
                title = "VLINE";

                intentWebview(url, title);
                break;

            case R.id.ll_ferry:

                url = "https://www.ptv.vic.gov.au/getting-around/ferries/";
                title = "FERRY";

                intentWebview(url, title);
                break;

            case R.id.ll_flight:

//                url = "http://www.jetstar.com/au/en/home";
                url = "https://www.virginaustralia.com/au/en/bookings/flights/make-a-booking/";
                title = "FLIGHT";

                intentWebview(url, title);
                break;
        }

    }

    private void intentWebview(String url, String title)
    {
        Log.e("call", "Title:- " + title);
        Log.e("call", "Url:- " + url);

//        Wallet__Activity.resumeFlag = 0;

        Intent intent = new Intent(getActivity(), WebViewActivity.class);
        intent.putExtra("url",url);
        intent.putExtra("title",title);
        startActivity(intent);
        ((Activity)getActivity()).overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

}
