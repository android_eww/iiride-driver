package iiride.app.driver.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import iiride.app.driver.Activity.Drawer_Activity;
import iiride.app.driver.Adapter.Past_Job_List_Adapter;
import iiride.app.driver.Been.Pending_JobList_Been;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;
import iiride.app.driver.Others.ConnectivityReceiver;


import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.hendraanggrian.widget.ExpandableRecyclerView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Past_Job_List_Fragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    private ExpandableRecyclerView recyclerView;
    private ExpandableRecyclerView.Adapter mAdapter;
    private ExpandableRecyclerView.LayoutManager layoutManager;
    public static List<Pending_JobList_Been> list = new ArrayList<Pending_JobList_Been>();

    String TAG = "CallDispatched_List";
    private AQuery aQuery;
    DialogClass dialogClass;
    public SwipeRefreshLayout swipeRefreshLayout;
    int RunAct_FirstTime;
    TextView tv_NoDataFound;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_past_job, container, false);

        aQuery = new AQuery(getActivity());

        ((Drawer_Activity)getActivity()).setTitleOfScreenToolbar("Past Job");

        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);
        tv_NoDataFound = (TextView) rootView.findViewById(R.id.tv_NoDataFound);
        tv_NoDataFound.setVisibility(View.GONE);

        recyclerView = (ExpandableRecyclerView) rootView.findViewById(R.id.rv_past_job);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new Past_Job_List_Adapter((LinearLayoutManager) layoutManager);
        recyclerView.setAdapter(mAdapter);

        swipeRefreshLayout.setOnRefreshListener(this);

        String userId = SessionSave.getUserSession(Comman.USER_ID, getActivity());
        if (userId!=null && !userId.equalsIgnoreCase(""))
        {
            Log.e(TAG, "firstTime call");
            RunAct_FirstTime=0;

            if (ConnectivityReceiver.isConnected())
            {
                CallPendingJob_List(userId);
            }
            else
            {
                tv_NoDataFound.setVisibility(View.VISIBLE);
                new SnackbarUtils(Drawer_Activity.main_layout, getString(R.string.not_connected_to_internet),
                        ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
            }
        }
        return rootView;
    }

    @Override
    public void onRefresh()
    {
        String userId = SessionSave.getUserSession(Comman.USER_ID, getActivity());
        if (userId!=null && !userId.equalsIgnoreCase(""))
        {
            Log.e(TAG, "On refresh");
            swipeRefreshLayout.setRefreshing(true);
            if (ConnectivityReceiver.isConnected())
            {
                CallPendingJob_List(userId);
            }
            else
            {
                tv_NoDataFound.setVisibility(View.VISIBLE);
                new SnackbarUtils(Drawer_Activity.main_layout, getString(R.string.not_connected_to_internet),
                        ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
            }
        }
    }

    private void CallPendingJob_List(String userId)
    {
        tv_NoDataFound.setVisibility(View.GONE);
        list.clear();
        dialogClass = new DialogClass(Drawer_Activity.activity, 1);
        if (RunAct_FirstTime==0)
        {
            dialogClass.showDialog();
            RunAct_FirstTime=1;
        }


        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_BOOKING_HISTORY_LIST + userId;

        Log.e(TAG, "URL = " + url);
        Log.e(TAG, "PARAMS = " + params);

        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>(){

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG, "responseCode = " + responseCode);
                    Log.e(TAG, "Response = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.d(TAG,"status:true");
                                if (json.has("history"))
                                {
                                    JSONArray bookingHistoryArray = json.getJSONArray("history");

                                    if (bookingHistoryArray != null && bookingHistoryArray.length() >0)
                                    {
                                        Log.d(TAG,"bookingHistory:\n" + bookingHistoryArray);
                                        Log.d(TAG,"bookingHistory Length:\n" + bookingHistoryArray.length());
                                        for (int i = 0; i < bookingHistoryArray.length() ; i++)

                                        {
                                            JSONObject bookingHistoryObj = bookingHistoryArray.getJSONObject(i);
                                            Log.d(TAG,"bookingHistoryObj:\n" + bookingHistoryObj);

                                            if (bookingHistoryObj != null)
                                            {
                                                String Id="", PassengerId="", ModelId="", DriverId="", CreatedDate="", TransactionId="", PaymentStatus="",
                                                        PickupDateTime="", DropOffDateTime="",TripDuration="",TripDistance="",PickupLocation="",DropoffLocation="",
                                                        NightFareApplicable="",NightFare="",TripFare="",WaitingTime="",WaitingTimeCost="",TollFee="",BookingCharge="",
                                                        Tax="",PromoCode="",Discount="",SubTotal="",GrandTotal="",Status="",Reason="",PaymentType="",AdminAmount="",
                                                        CompanyAmount="",PickupLat="",PickupLng="",DropOffLat="",DropOffLon="",Model="",PassengerName="",PassengerEmail="",
                                                        PassengerMobileNo="",HistoryType="", FlightNumber="", Notes="", DispatcherDriverInfo="", dispatcherEmail=""
                                                        , dispatcherFullname="", dispatcherMobileNo="";

                                                if(bookingHistoryObj.has("Id"))
                                                {
                                                    Id = bookingHistoryObj.getString("Id");
                                                    Log.d(TAG,"Id:\n" + Id);
                                                }
                                                if (bookingHistoryObj.has("PassengerId"))
                                                {
                                                    PassengerId = bookingHistoryObj.getString("PassengerId");
                                                    Log.d(TAG,"PassengerId:\n" + PassengerId);
                                                }
                                                if (bookingHistoryObj.has("ModelId"))
                                                {
                                                    ModelId = bookingHistoryObj.getString("ModelId");
                                                    Log.d(TAG,"ModelId:\n" + ModelId);
                                                }
                                                if (bookingHistoryObj.has("DriverId"))
                                                {
                                                    DriverId = bookingHistoryObj.getString("DriverId");
                                                    Log.d(TAG,"DriverId:\n" + DriverId);
                                                }
                                                if (bookingHistoryObj.has("CreatedDate"))
                                                {
                                                    CreatedDate = bookingHistoryObj.getString("CreatedDate");
                                                    Log.d(TAG,"CreatedDate:\n" + CreatedDate);
                                                }
                                                if (bookingHistoryObj.has("TransactionId"))
                                                {
                                                    TransactionId = bookingHistoryObj.getString("TransactionId");
                                                }
                                                if (bookingHistoryObj.has("PaymentStatus"))
                                                {
                                                    PaymentStatus = bookingHistoryObj.getString("PaymentStatus");
                                                }
                                                if (bookingHistoryObj.has("PickupDateTime"))
                                                {
                                                    PickupDateTime = bookingHistoryObj.getString("PickupDateTime");
                                                }
                                                if (bookingHistoryObj.has("DropOffDateTime"))
                                                {
                                                    DropOffDateTime = bookingHistoryObj.getString("DropOffDateTime");
                                                }
                                                if (bookingHistoryObj.has("TripDuration"))
                                                {
                                                    TripDuration = bookingHistoryObj.getString("TripDuration");
                                                }
                                                if (bookingHistoryObj.has("TripDistance"))
                                                {
                                                    TripDistance = bookingHistoryObj.getString("TripDistance");
                                                }
                                                if (bookingHistoryObj.has("PickupLocation"))
                                                {
                                                    PickupLocation = bookingHistoryObj.getString("PickupLocation");
                                                }
                                                if (bookingHistoryObj.has("DropoffLocation"))
                                                {
                                                    DropoffLocation = bookingHistoryObj.getString("DropoffLocation");
                                                }
                                                if (bookingHistoryObj.has("NightFareApplicable"))
                                                {
                                                    NightFareApplicable = bookingHistoryObj.getString("NightFareApplicable");
                                                }
                                                if (bookingHistoryObj.has("NightFare"))
                                                {
                                                    NightFare = bookingHistoryObj.getString("NightFare");
                                                }
                                                if (bookingHistoryObj.has("TripFare"))
                                                {
                                                    TripFare = bookingHistoryObj.getString("TripFare");
                                                }
                                                if (bookingHistoryObj.has("WaitingTime"))
                                                {
                                                    WaitingTime = bookingHistoryObj.getString("WaitingTime");
                                                }
                                                if (bookingHistoryObj.has("WaitingTimeCost"))
                                                {
                                                    WaitingTimeCost = bookingHistoryObj.getString("WaitingTimeCost");
                                                }
                                                if (bookingHistoryObj.has("TollFee"))
                                                {
                                                    TollFee = bookingHistoryObj.getString("TollFee");
                                                }
                                                if (bookingHistoryObj.has("BookingCharge"))
                                                {
                                                    BookingCharge = bookingHistoryObj.getString("BookingCharge");
                                                }
                                                if (bookingHistoryObj.has("Tax"))
                                                {
                                                    Tax = bookingHistoryObj.getString("Tax");
                                                }
                                                if (bookingHistoryObj.has("PromoCode"))
                                                {
                                                    PromoCode = bookingHistoryObj.getString("PromoCode");
                                                }
                                                if (bookingHistoryObj.has("Discount"))
                                                {
                                                    Discount = bookingHistoryObj.getString("Discount");
                                                }
                                                if (bookingHistoryObj.has("SubTotal"))
                                                {
                                                    SubTotal = bookingHistoryObj.getString("SubTotal");
                                                }
                                                if (bookingHistoryObj.has("GrandTotal"))
                                                {
                                                    GrandTotal = bookingHistoryObj.getString("GrandTotal");
                                                }
                                                if (bookingHistoryObj.has("Status"))
                                                {
                                                    Status = bookingHistoryObj.getString("Status");
                                                }
                                                if (bookingHistoryObj.has("Reason"))
                                                {
                                                    Reason = bookingHistoryObj.getString("Reason");
                                                }
                                                if (bookingHistoryObj.has("PaymentType"))
                                                {
                                                    PaymentType = bookingHistoryObj.getString("PaymentType");
                                                }
                                                if (bookingHistoryObj.has("AdminAmount"))
                                                {
                                                    AdminAmount = bookingHistoryObj.getString("AdminAmount");
                                                }
                                                if (bookingHistoryObj.has("CompanyAmount"))
                                                {
                                                    CompanyAmount = bookingHistoryObj.getString("CompanyAmount");
                                                }
                                                if (bookingHistoryObj.has("PickupLat"))
                                                {
                                                    PickupLat = bookingHistoryObj.getString("PickupLat");
                                                }
                                                if (bookingHistoryObj.has("PickupLng"))
                                                {
                                                    PickupLng = bookingHistoryObj.getString("PickupLng");
                                                }
                                                if (bookingHistoryObj.has("DropOffLat"))
                                                {
                                                    DropOffLat = bookingHistoryObj.getString("DropOffLat");
                                                }
                                                if (bookingHistoryObj.has("DropOffLon"))
                                                {
                                                    DropOffLon = bookingHistoryObj.getString("DropOffLon");
                                                }
                                                if (bookingHistoryObj.has("Model"))
                                                {
                                                    Model = bookingHistoryObj.getString("Model");
                                                }
                                                if (bookingHistoryObj.has("PassengerName"))
                                                {
                                                    PassengerName = bookingHistoryObj.getString("PassengerName");
                                                }
                                                if (bookingHistoryObj.has("PassengerEmail"))
                                                {
                                                    PassengerEmail = bookingHistoryObj.getString("PassengerEmail");
                                                }
                                                if (bookingHistoryObj.has("PassengerContact"))
                                                {
                                                    PassengerMobileNo = bookingHistoryObj.getString("PassengerContact");
                                                }
                                                if (bookingHistoryObj.has("HistoryType"))
                                                {
                                                    HistoryType = bookingHistoryObj.getString("HistoryType");
                                                }
                                                if (bookingHistoryObj.has("FlightNumber")) {
                                                    FlightNumber = bookingHistoryObj.getString("FlightNumber");
                                                }
                                                if (bookingHistoryObj.has("Notes")) {
                                                    Notes = bookingHistoryObj.getString("Notes");
                                                }
                                                if (bookingHistoryObj.has("DispatcherDriverInfo"))
                                                {
                                                    DispatcherDriverInfo = bookingHistoryObj.getString("DispatcherDriverInfo");
                                                    if (DispatcherDriverInfo!=null && !DispatcherDriverInfo.equalsIgnoreCase(""))
                                                    {
                                                        JSONObject DispatcherDriverInfoObj = bookingHistoryObj.getJSONObject("DispatcherDriverInfo");
                                                        if (DispatcherDriverInfoObj!=null)
                                                        {
                                                            if (DispatcherDriverInfoObj.has("Email"))
                                                            {
                                                                dispatcherEmail = DispatcherDriverInfoObj.getString("Email");
                                                            }
                                                            if (DispatcherDriverInfoObj.has("Fullname"))
                                                            {
                                                                dispatcherFullname = DispatcherDriverInfoObj.getString("Fullname");
                                                            }
                                                            if (DispatcherDriverInfoObj.has("MobileNo"))
                                                            {
                                                                dispatcherMobileNo = DispatcherDriverInfoObj.getString("MobileNo");
                                                            }
                                                        }
                                                    }
                                                }
                                                if (HistoryType!=null && !HistoryType.equalsIgnoreCase("") && HistoryType.equalsIgnoreCase("Past"))
                                                {
                                                    list.add(new Pending_JobList_Been(Id, PassengerId, ModelId, DriverId, CreatedDate, TransactionId, PaymentStatus, PickupDateTime, DropOffDateTime,TripDuration,TripDistance,
                                                            PickupLocation,DropoffLocation,NightFareApplicable,NightFare,TripFare,WaitingTime,WaitingTimeCost,TollFee,BookingCharge,Tax,
                                                            PromoCode,Discount,SubTotal,GrandTotal,Status,Reason,PaymentType,AdminAmount,CompanyAmount,PickupLat,PickupLng,DropOffLat,
                                                            DropOffLon,Model,PassengerName,PassengerEmail,PassengerMobileNo,HistoryType, FlightNumber, Notes, dispatcherEmail, dispatcherFullname
                                                            , dispatcherMobileNo));
                                                }
                                            }
                                            else
                                            {
                                                Log.d(TAG,"bookingHistoryObj:NULL");
                                            }
                                        }

                                        if (list.size()>0)
                                        {
                                            tv_NoDataFound.setVisibility(View.GONE);
                                        }
                                        else
                                        {
                                            tv_NoDataFound.setVisibility(View.VISIBLE);
                                        }
                                        mAdapter.notifyDataSetChanged();
                                        dialogClass.hideDialog();
                                        swipeRefreshLayout.setRefreshing(false);
                                        if (json.has("message"))
                                        {
                                            Log.d(TAG, "message:" + json.getString("message"));
                                            new SnackbarUtils(Drawer_Activity.main_layout, json.getString("message"),
                                                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                                        }
                                    }
                                    else
                                    {
                                        Log.d(TAG,"history:NULL");
                                        dialogClass.hideDialog();
                                        swipeRefreshLayout.setRefreshing(false);
                                        tv_NoDataFound.setVisibility(View.VISIBLE);
                                        if (json.has("message"))
                                        {
                                            Log.d(TAG, "message:" + json.getString("message"));
                                            new SnackbarUtils(Drawer_Activity.main_layout, json.getString("message"),
                                                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                                        }
                                    }

                                }
                                else
                                {
                                    dialogClass.hideDialog();
                                    swipeRefreshLayout.setRefreshing(false);
                                    tv_NoDataFound.setVisibility(View.VISIBLE);
                                    if (json.has("message"))
                                    {
                                        Log.d(TAG, "message:" + json.getString("message"));
                                        new SnackbarUtils(Drawer_Activity.main_layout, json.getString("message"),
                                                ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                                    }
                                }
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                swipeRefreshLayout.setRefreshing(false);
                                tv_NoDataFound.setVisibility(View.VISIBLE);
                                if (json.has("message"))
                                {
                                    Log.d(TAG, "message:" + json.getString("message"));
                                    new SnackbarUtils(Drawer_Activity.main_layout, json.getString("message"),
                                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        }
                        else
                        {
                            Log.e("status", "no status");
                            dialogClass.hideDialog();
                            swipeRefreshLayout.setRefreshing(false);
                            tv_NoDataFound.setVisibility(View.VISIBLE);
                            new SnackbarUtils(Drawer_Activity.main_layout, getString(R.string.something_is_wrong),
                                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                        }
                    }
                    else
                    {
                        Log.e(TAG, "getMessage = " + "null");
                        dialogClass.hideDialog();
                        swipeRefreshLayout.setRefreshing(false);
                        tv_NoDataFound.setVisibility(View.VISIBLE);
                        new SnackbarUtils(Drawer_Activity.main_layout, getString(R.string.something_is_wrong),
                                ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG, "Exception = " + e.getMessage() + "something_is_wrong");
                    dialogClass.hideDialog();
                    swipeRefreshLayout.setRefreshing(false);
                    tv_NoDataFound.setVisibility(View.VISIBLE);
                    new SnackbarUtils(Drawer_Activity.main_layout, getString(R.string.something_is_wrong),
                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }
}
