package iiride.app.driver.Fragment;

import android.graphics.PointF;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import iiride.app.driver.Activity.Wallet_Transfer_Activity;
import iiride.app.driver.Comman.WebServiceAPI;


import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.dlazaro66.qrcodereaderview.QRCodeReaderView;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Wallet_Transfer_SendMoney_Fragment extends Fragment implements View.OnClickListener,QRCodeReaderView.OnQRCodeReadListener {

    String TAG = "wallSendMoneyFragment";

    private LinearLayout ll_qr_reader,ll_qr_result;
    public  static TextView tv_qr_result;
    private QRCodeReaderView qr_code_reader_view;

    private AQuery aQuery;
    DialogClass dialogClass;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sendmoney_transfer_wallet, container, false);

        aQuery = new AQuery(getActivity());

        qr_code_reader_view = (QRCodeReaderView) rootView.findViewById(R.id.qr_code_reader);
        ll_qr_reader = (LinearLayout) rootView.findViewById(R.id.ll_qr_reder);
        ll_qr_result = (LinearLayout) rootView.findViewById(R.id.ll_qr_result);
        tv_qr_result = (TextView) rootView.findViewById(R.id.tv_qr_result);

        init();

        return rootView;
    }

    private void init()
    {
        ll_qr_result.setOnClickListener(this);
        tv_qr_result.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.ll_qr_result:

                ll_qr_result.setVisibility(View.GONE);
                ll_qr_reader.setVisibility(View.VISIBLE);

                initQrScanner();

                break;

            case R.id.tv_qr_result:

                ll_qr_result.setVisibility(View.GONE);
                ll_qr_reader.setVisibility(View.VISIBLE);

                initQrScanner();

                break;
        }
    }

    private void initQrScanner() {

        qr_code_reader_view.setOnQRCodeReadListener(this);
        qr_code_reader_view.setQRDecodingEnabled(true);
        qr_code_reader_view.setAutofocusInterval(10L);
        qr_code_reader_view.setFrontCamera();
        qr_code_reader_view.setBackCamera();

    }

    @Override
    public void onQRCodeRead(String text, PointF[] points) {

        Log.e("QRCode RESULT:- " , "QrCode Result:- " + text );

        ll_qr_reader.setVisibility(View.GONE);
        ll_qr_result.setVisibility(View.VISIBLE);
        qr_code_reader_view.setOnQRCodeReadListener(null);

        Wallet_Transfer_Activity.getQrCodeResult = text;

        if (Wallet_Transfer_Activity.getQrCodeResult != null && !Wallet_Transfer_Activity.getQrCodeResult.equalsIgnoreCase(""))
        {
            getQrcoderesult();
        }
        else
        {
            new SnackbarUtils(Wallet_Transfer_Activity.main_layout, getString(R.string.scan_bar_code_error_message),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }


    }

    private void getQrcoderesult() {

        dialogClass = new DialogClass(getActivity(), 1);
        dialogClass.showDialog();

        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_QR_CODE_DETAILS;

        params.put(WebServiceAPI.PARAM_QR_CODE,Wallet_Transfer_Activity.getQrCodeResult);

        Log.e(TAG, "URL = " + url);
        Log.e(TAG, "PARAMS = " + params);

        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>()
        {
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status)
            {
                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG, "responseCode = " + responseCode);
                    Log.e(TAG, "Response = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");
                                dialogClass.hideDialog();
                                if (json.has("data"))
                                {
                                    JSONObject data = json.getJSONObject("data");
                                    if (data != null)
                                    {
                                        String scanId="", scanName="", scanMobileNo="", scanQRCode="";

                                        if(data.has("Id"))
                                        {
                                            scanId = data.getString("Id");
                                        }
                                        if(data.has("Fullname"))
                                        {
                                            scanName = data.getString("Fullname");
                                        }
                                        if(data.has("MobileNo"))
                                        {
                                            scanMobileNo = data.getString("MobileNo");
                                        }
                                        if(data.has("QRCode"))
                                        {
                                            scanQRCode = data.getString("QRCode");
                                        }

                                        tv_qr_result.setText("Name:- " + scanName + "\n\n" + "MobileNo:- " +  scanMobileNo);
                                    }
                                    else
                                    {
                                        dialogClass.hideDialog();
                                        if (json.has("message")) {
                                            new SnackbarUtils(Wallet_Transfer_Activity.main_layout, json.getString("message"),
                                                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                                        }
                                    }

                                }

                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(Wallet_Transfer_Activity.main_layout, json.getString("message"),
                                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }

                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message")) {
                                new SnackbarUtils(Wallet_Transfer_Activity.main_layout, json.getString("message"),
                                        ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }

                    }

                }
                catch (Exception e)
                {
                    Log.e(TAG, "Exception = " + e.getMessage() + "something_is_wrong");
                    dialogClass.hideDialog();
                    new SnackbarUtils(Wallet_Transfer_Activity.main_layout, getString(R.string.something_is_wrong),
                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                }

            }
        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY_NAME,WebServiceAPI.HEADER_KEY_VALUE));
    }

    public void refreshScanLayout()
    {
        tv_qr_result.setText(R.string.bar_qr_scanner);

    }

    @Override
    public void onResume() {
        super.onResume();
        qr_code_reader_view.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        qr_code_reader_view.stopCamera();
    }
}