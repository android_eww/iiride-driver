package iiride.app.driver.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import iiride.app.driver.Activity.Drawer_Activity;
import iiride.app.driver.Adapter.Dispatched_Job_List_Adapter;
import iiride.app.driver.Been.Dispatched_JobList_Been;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;
import iiride.app.driver.Others.ConnectivityReceiver;


import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.hendraanggrian.widget.ExpandableRecyclerView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Dispatched_Job_List_Fragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    private ExpandableRecyclerView recyclerView;
    public static ExpandableRecyclerView.Adapter mAdapter;
    private ExpandableRecyclerView.LayoutManager layoutManager;
    public static List<Dispatched_JobList_Been> list = new ArrayList<Dispatched_JobList_Been>();

    String TAG = "CallDispatched_List";
    private AQuery aQuery;
    DialogClass dialogClass;
    public SwipeRefreshLayout swipeRefreshLayout;
    int RunAct_FirstTime;
    TextView tv_NoDataFound;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dispatched_job_list, container, false);

        aQuery = new AQuery(getActivity());
        list.clear();

        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);

        tv_NoDataFound = (TextView) rootView.findViewById(R.id.tv_NoDataFound);
        tv_NoDataFound.setVisibility(View.GONE);

        recyclerView = (ExpandableRecyclerView) rootView.findViewById(R.id.rv_dispatched_job);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new Dispatched_Job_List_Adapter((LinearLayoutManager) layoutManager);
        recyclerView.setAdapter(mAdapter);

        swipeRefreshLayout.setOnRefreshListener(this);

        String userId = SessionSave.getUserSession(Comman.USER_ID, getActivity());
        if (userId!=null && !userId.equalsIgnoreCase(""))
        {
            Log.e(TAG, "firstTime call");
            RunAct_FirstTime=0;
            if (ConnectivityReceiver.isConnected())
            {
                CallDispatched_List(userId);
            }
            else
            {
                tv_NoDataFound.setVisibility(View.VISIBLE);
                new SnackbarUtils(Drawer_Activity.main_layout, getString(R.string.not_connected_to_internet),
                        ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
            }
        }
        return rootView;
    }

    @Override
    public void onRefresh()
    {
        String userId = SessionSave.getUserSession(Comman.USER_ID, getActivity());
        if (userId!=null && !userId.equalsIgnoreCase(""))
        {
            Log.e(TAG, "On refresh");
            if (ConnectivityReceiver.isConnected())
            {
                CallDispatched_List(userId);
            }
            else
            {
                tv_NoDataFound.setVisibility(View.VISIBLE);
                new SnackbarUtils(Drawer_Activity.main_layout, getString(R.string.not_connected_to_internet),
                        ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
            }
        }
    }

    public void CallDispatched_List(String userId)
    {
        tv_NoDataFound.setVisibility(View.GONE);
        list.clear();
        mAdapter.notifyDataSetChanged();
        dialogClass = new DialogClass(Drawer_Activity.activity, 1);
        if (RunAct_FirstTime==0)
        {
            dialogClass.showDialog();
            RunAct_FirstTime=1;
        }

        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_MY_DISPATCH_JOB_LIST + userId;

        Log.e(TAG, "URL = " + url);
        Log.e(TAG, "PARAMS = " + params);

        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>(){

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG, "responseCode = " + responseCode);
                    Log.e(TAG, "Response = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");

                                if (json.has("history"))
                                {
                                    String history = json.getString("history");
                                    if (history!=null && !history.equalsIgnoreCase(""))
                                    {
                                        JSONArray historyArray = json.getJSONArray("history");
                                        for (int h=0; h<historyArray.length(); h++)
                                        {
                                            String DispatchJob_Id="", PickupDateTime="", PickupLocation="", DropoffLocation="", CompanyId="", PassengerId="", TripDistance=""
                                                    , ModelId="", PassengerName="", PassengerContact="", Model="",TripFare="", Tax="", SubTotal="", GrandTotal="", PaymentType=""
                                                    , PassengerEmail="", CreatedDate="", Status="", FlightNumber="", Notes="", DispatcherDriverInfo="", dispatcherEmail=""
                                                , dispatcherFullname="", dispatcherMobileNo="", BookingType="";

                                            JSONObject historyObject = historyArray.getJSONObject(h);

                                            if (historyObject.has("Id"))
                                            {
                                                DispatchJob_Id = historyObject.getString("Id");
                                            }
                                            if (historyObject.has("PickupTime"))
                                            {
                                                PickupDateTime = historyObject.getString("PickupTime");
                                            }
                                            if (historyObject.has("CreatedDate"))
                                            {
                                                CreatedDate = historyObject.getString("CreatedDate");
                                            }
                                            if (historyObject.has("PickupLocation"))
                                            {
                                                PickupLocation = historyObject.getString("PickupLocation");
                                            }
                                            if (historyObject.has("DropoffLocation"))
                                            {
                                                DropoffLocation = historyObject.getString("DropoffLocation");
                                            }
                                            if (historyObject.has("CompanyId"))
                                            {
                                                CompanyId = historyObject.getString("CompanyId");
                                            }
                                            if (historyObject.has("BookingType"))
                                            {
                                                BookingType = historyObject.getString("BookingType");
                                            }


                                            if (historyObject.has("PassengerId"))
                                            {
                                                PassengerId = historyObject.getString("PassengerId");
                                            }
                                            if (historyObject.has("TripDistance"))
                                            {
                                                TripDistance = historyObject.getString("TripDistance");
                                            }
                                            if (historyObject.has("ModelId"))
                                            {
                                                ModelId = historyObject.getString("ModelId");
                                            }
                                            if (historyObject.has("PassengerName"))
                                            {
                                                PassengerName = historyObject.getString("PassengerName");
                                            }
                                            if (historyObject.has("PassengerContact"))
                                            {
                                                PassengerContact = historyObject.getString("PassengerContact");
                                            }
                                            if (historyObject.has("Model"))
                                            {
                                                Model = historyObject.getString("Model");
                                            }

                                            if (historyObject.has("TripFare"))
                                            {
                                                TripFare = historyObject.getString("TripFare");
                                            }
                                            if (historyObject.has("Tax"))
                                            {
                                                Tax = historyObject.getString("Tax");
                                            }
                                            if (historyObject.has("SubTotal"))
                                            {
                                                SubTotal = historyObject.getString("SubTotal");
                                            }
                                            if (historyObject.has("GrandTotal"))
                                            {
                                                GrandTotal = historyObject.getString("GrandTotal");
                                            }
                                            if (historyObject.has("PassengerEmail"))
                                            {
                                                PassengerEmail = historyObject.getString("PassengerEmail");
                                            }

                                            if (historyObject.has("Status"))
                                            {
                                                Status = historyObject.getString("Status");
                                            }
                                            if (historyObject.has("PaymentType"))
                                            {
                                                PaymentType = historyObject.getString("PaymentType");
                                            }
                                            if (historyObject.has("FlightNumber")) {
                                                FlightNumber = historyObject.getString("FlightNumber");
                                            }
                                            if (historyObject.has("Notes")) {
                                                Notes = historyObject.getString("Notes");
                                            }

                                            if (historyObject.has("DispatcherDriverInfo"))
                                            {
                                                DispatcherDriverInfo = historyObject.getString("DispatcherDriverInfo");
                                                if (DispatcherDriverInfo!=null && !DispatcherDriverInfo.equalsIgnoreCase(""))
                                                {
                                                    JSONObject DispatcherDriverInfoObj = historyObject.getJSONObject("DispatcherDriverInfo");
                                                    if (DispatcherDriverInfoObj!=null)
                                                    {
                                                        if (DispatcherDriverInfoObj.has("Email"))
                                                        {
                                                            dispatcherEmail = DispatcherDriverInfoObj.getString("Email");
                                                        }
                                                        if (DispatcherDriverInfoObj.has("Fullname"))
                                                        {
                                                            dispatcherFullname = DispatcherDriverInfoObj.getString("Fullname");
                                                        }
                                                        if (DispatcherDriverInfoObj.has("MobileNo"))
                                                        {
                                                            dispatcherMobileNo = DispatcherDriverInfoObj.getString("MobileNo");
                                                        }
                                                    }
                                                }
                                            }
                                            ///TripFare, Tax, SubTotal, GrandTotal, PaymentType, PassengerName, PassengerContact, PassengerEmail

                                            list.add(new Dispatched_JobList_Been(DispatchJob_Id, PickupDateTime, PickupLocation, DropoffLocation, CompanyId, PassengerId, TripDistance
                                                    , ModelId, PassengerName, PassengerContact, Model, TripFare, Tax, SubTotal, GrandTotal, PaymentType, PassengerEmail, CreatedDate
                                                    , Status, FlightNumber, Notes, dispatcherEmail, dispatcherFullname, dispatcherMobileNo, BookingType));
                                            /*for (int i=0; i<10; i++)
                                            {
                                                list.add(new Dispatched_JobList_Been(DispatchJob_Id, PickupDateTime, PickupLocation, DropoffLocation, CompanyId, PassengerId, TripDistance, ModelId, PassengerName, PassengerContact, Model));
                                            }*/
                                        }
                                        mAdapter.notifyDataSetChanged();

                                        dialogClass.hideDialog();
                                        swipeRefreshLayout.setRefreshing(false);
                                        if (list.size()>0)
                                        {
                                            tv_NoDataFound.setVisibility(View.GONE);
                                        }
                                        else
                                        {
                                            tv_NoDataFound.setVisibility(View.VISIBLE);
                                        }
                                    }
                                    else
                                    {
                                        Log.e("status", "false");
                                        dialogClass.hideDialog();
                                        swipeRefreshLayout.setRefreshing(false);
                                        tv_NoDataFound.setVisibility(View.VISIBLE);
                                        if (json.has("message"))
                                        {
                                            Log.d(TAG, "message:" + json.getString("message"));
                                            new SnackbarUtils(Drawer_Activity.main_layout, json.getString("message"),
                                                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                                        }
                                    }
                                }
                                else
                                {
                                    Log.e("status", "false");
                                    dialogClass.hideDialog();
                                    swipeRefreshLayout.setRefreshing(false);
                                    tv_NoDataFound.setVisibility(View.VISIBLE);
                                    if (json.has("message"))
                                    {
                                        Log.d(TAG, "message:" + json.getString("message"));
                                        new SnackbarUtils(Drawer_Activity.main_layout, json.getString("message"),
                                                ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                                    }
                                }
                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                swipeRefreshLayout.setRefreshing(false);
                                tv_NoDataFound.setVisibility(View.VISIBLE);
                                if (json.has("message"))
                                {
                                    Log.d(TAG, "message:" + json.getString("message"));
                                    new SnackbarUtils(Drawer_Activity.main_layout, json.getString("message"),
                                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        }
                        else
                        {
                            Log.e("status", "no status");
                            dialogClass.hideDialog();
                            swipeRefreshLayout.setRefreshing(false);
                            tv_NoDataFound.setVisibility(View.VISIBLE);
                            new SnackbarUtils(Drawer_Activity.main_layout, getString(R.string.something_is_wrong),
                                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                        }
                    }
                    else
                    {
                        Log.e(TAG, "getMessage = " + "null");
                        dialogClass.hideDialog();
                        swipeRefreshLayout.setRefreshing(false);
                        tv_NoDataFound.setVisibility(View.VISIBLE);
                        new SnackbarUtils(Drawer_Activity.main_layout, getString(R.string.something_is_wrong),
                                ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG, "Exception = " + e.getMessage() + "something_is_wrong");
                    dialogClass.hideDialog();
                    swipeRefreshLayout.setRefreshing(false);
                    tv_NoDataFound.setVisibility(View.VISIBLE);
                    new SnackbarUtils(Drawer_Activity.main_layout, getString(R.string.something_is_wrong),
                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }
}
