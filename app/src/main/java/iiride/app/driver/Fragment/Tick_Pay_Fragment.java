package iiride.app.driver.Fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import iiride.app.driver.Activity.Drawer_Activity;
import iiride.app.driver.Activity.Receipt_Invoice_Activity;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;
import iiride.app.driver.Others.ConnectivityReceiver;


import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;


public class Tick_Pay_Fragment extends Fragment implements View.OnClickListener{

    private static String TAG = Tick_Pay_Fragment.class.getSimpleName();

    private static final int REQUEST_SCAN = 100;
    private static final int REQUEST_AUTOTEST =  200;

    TextView  tv_pay_now, tv_scanCard, tv_scanNfcCard, tv_dollar;
    EditText et_companyName, et_ABN, et_TotalAmount; //et_pincode
    public static EditText et_CardNumber, et_Cvv;
    public static TextView tv_ExpireDate;
    LinearLayout main_layout;
    Button btn_scanNfcCard;
    RelativeLayout rl_Gif;
    ImageView iv_close;
//    VideoView videoView;

    private AQuery aQuery;
    DialogClass dialogClass;
    View BottomShitview;

    ///for Bottom Dialog
    private BottomSheetDialog mBottomSheetDialog;
    private NumberPicker numberPickerMonth,numberPickerYear;
    private String[] valueMonth ;
    private String[] valueYear ;
    int MonthInNumber=0, YearInNumber=0 ;
    private String[] monthName = {
            "Jan", "Feb", "Mar",
            "Apr", "May", "Jun",
            "Jul", "Aug", "Sep",
            "Oct", "Nov", "Dec"
    };
    Bundle SavedInstanceState;

    String ExpiryDate="";
    private String cardNumber;

    boolean validornot = false;
    private static final int MY_SCAN_REQUEST_CODE = 200;
    private double rate = 0;
    private double transactionRate = 0;
    private TextView tv_Rate;
    private String strRate = "% Service fee";
    private String strTransactionRate = "% Transaction fee";
    private TextView tv_FinalTotal;
    public static String payId = "";

    public static String InvoiceRate="";




//    private Drawer_Activity drawerActivity;USER_BANK_ACCOUNT_HOLDER_NAME

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tick_pay, container, false);
        payId = "";
        transactionRate = 0;
        aQuery = new AQuery(getActivity());
        dialogClass = new DialogClass(getActivity(), 1);
        SavedInstanceState = savedInstanceState;
//        drawerActivity = new Drawer_Activity();

        main_layout = (LinearLayout) rootView.findViewById(R.id.main_layout);
        btn_scanNfcCard = (Button) rootView.findViewById(R.id.btn_scanNfcCard);


        et_CardNumber = (EditText) rootView.findViewById(R.id.et_CardNumber);
        et_companyName = (EditText) rootView.findViewById(R.id.et_companyName);
        et_ABN = (EditText) rootView.findViewById(R.id.et_ABN);
        et_TotalAmount = (EditText) rootView.findViewById(R.id.et_TotalAmount);
        et_Cvv = (EditText) rootView.findViewById(R.id.et_Cvv);
//        et_pincode = (EditText) rootView.findViewById(R.id.et_pincode);
        tv_Rate = (TextView) rootView.findViewById(R.id.tv_rate);
        tv_FinalTotal = (TextView) rootView.findViewById(R.id.tv_final_total);
        rl_Gif = (RelativeLayout) rootView.findViewById(R.id.rl_Gif);
//        videoView =(VideoView)rootView.findViewById(R.id.video);

        et_CardNumber.setFilters(new InputFilter[] { new InputFilter.LengthFilter(19) });

        et_CardNumber.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Drawer_Activity.flagScan=false;
                return false;
            }
        });
        et_CardNumber.addTextChangedListener(new FourDigitCardFormatWatcher());

        tv_pay_now = (TextView) rootView.findViewById(R.id.tv_pay_now);
        tv_scanCard = (TextView) rootView.findViewById(R.id.tv_scanCard);
        tv_scanNfcCard = (TextView) rootView.findViewById(R.id.tv_scanNfcCard);
        tv_ExpireDate = (TextView) rootView.findViewById(R.id.tv_ExpireDate);
        iv_close = (ImageView) rootView.findViewById(R.id.iv_close);
        tv_dollar = (TextView) rootView.findViewById(R.id.tv_dollar);

        tv_pay_now.setOnClickListener(this);
        tv_scanNfcCard.setOnClickListener(this);
        tv_ExpireDate.setOnClickListener(this);
        tv_scanCard.setOnClickListener(this);
        btn_scanNfcCard.setOnClickListener(this);
        iv_close.setOnClickListener(this);

        String Sample = "<font color='#ff0000'>PayWave</font><font color='#006400'> Swipe Your Card to the back of your phone</font>";
        btn_scanNfcCard.setText(Html.fromHtml(Sample), TextView.BufferType.SPANNABLE);

        String str_drivername = SessionSave.getUserSession(Comman.USER_BANK_ACCOUNT_HOLDER_NAME,getActivity());
        String str_abn = SessionSave.getUserSession(Comman.USER_ABN,getActivity());


        Log.e(TAG, "str_drivername:- " + str_drivername );
        Log.e(TAG, "str_abn:- " + str_abn );

        if (str_drivername != null && !str_drivername.equalsIgnoreCase(""))
        {
            et_companyName.setText(str_drivername);
        }
        if (str_abn != null && !str_abn.equalsIgnoreCase(""))
        {
            et_ABN.setText(str_abn);
        }

        et_TotalAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count)
            {

                if (charSequence.length() <= 0 )
                {
                    tv_dollar.setVisibility(View.GONE);
                    tv_FinalTotal.setText("");
                }
                else
                {
                    if (charSequence.toString().trim().length()==1 && charSequence.toString().equalsIgnoreCase("."))
                    {
                        et_TotalAmount.setText("");
                    }
                    else
                    {
                        tv_dollar.setVisibility(View.VISIBLE);

                        double rateM = rate * Double.parseDouble(et_TotalAmount.getText().toString());
                        double rateD = (Double.parseDouble(et_TotalAmount.getText().toString())) + (rateM / 100);

                        tv_FinalTotal.setText("$"+String.format("%.2f",rateD));
                        Log.e("rateD","rateDrateDrateD : "+rateD);
                        if (Double.parseDouble(String.format("%.2f",rateD))>=Double.parseDouble("100"))
                        {
                            et_Cvv.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            et_Cvv.setVisibility(View.GONE);
                            et_Cvv.setText("");
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        if (ConnectivityReceiver.isConnected())
        {
            getTickPayRate();
        }
        else
        {
            new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }

        return rootView;
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.tv_pay_now:
                CheckData();
                break;

            case R.id.btn_scanNfcCard:
                break;

            case R.id.tv_scanNfcCard:
                break;

            case R.id.tv_ExpireDate:
                showBottomSheetDailog_payment();
                break;

            case R.id.tv_scanCard:
                onScanPress();
                break;

            case R.id.iv_close:
                rl_Gif.setVisibility(View.GONE);
                break;
        }
    }

    public static void emptyFild() {
        if (et_CardNumber!=null && tv_ExpireDate!=null && et_Cvv!=null)
        {
            et_CardNumber.setText("");
            tv_ExpireDate.setText("");
            et_Cvv.setText("");
        }
        Drawer_Activity.flagScan=false;
    }

    public void getResultNfc(String strCardNum, String strCardExpiry)
    {
        rl_Gif.setVisibility(View.GONE);

        Log.e(TAG, "getResultNfc:- getResultNfc()" );
        Log.e(TAG, "strCardNum:- " +  strCardNum);
        Log.e(TAG, "strCardExpiry:- " + strCardExpiry );

        strCardNum = strCardNum.replace(" ","");
        cardNumber="";
        cardNumber = strCardNum;
//        et_CardNumber.setText(strCardNum);
        String cNoEncry = getFormattedCardNumber(strCardNum);

        et_CardNumber.setText(cNoEncry);

        tv_ExpireDate.setText(strCardExpiry);
        String[] abc = strCardExpiry.split(",");
        if (abc.length>1)
        {
            String abc1 = abc[0];
            try {
                Date date = new SimpleDateFormat("MMMM").parse(abc1);
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                Log.e("cal.get(Calendar.MONTH)","cal.get(Calendar.MONTH)"+cal.get(Calendar.MONTH));
                abc1 = String.valueOf((cal.get(Calendar.MONTH)+1));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String abc2 = abc[1];
            abc2 = abc2.substring(Math.max(abc2.length() - 2, 0));

            if (abc1!=null && abc2!=null)
            {
                strCardExpiry = abc1+"/"+abc2;
                ExpiryDate = strCardExpiry;
            }
            else {
                ExpiryDate = "";
            }
        }
        else {
            ExpiryDate = "";
        }
    }


    public static String getFormattedCardNumber(String input)
    {
        String cardNumber = "";
        String strCardFirst = input.substring(0,(input.length()-4));
        String strCardEnd = input.substring(input.length()-4);

        Log.e("call","strCardFirst = "+strCardFirst);
        Log.e("call","strCardEnd = "+strCardEnd);

        String displayCard = "";
        for (int i=0; i<strCardFirst.length(); i++)
        {
            displayCard = displayCard + "*";
        }

        displayCard = displayCard + strCardEnd;



        for (int i=0; i<displayCard.length(); i++)
        {
            if (i>3 && (i%4)==0)
            {
                cardNumber = cardNumber  + " " + displayCard.substring(i,i+1);
            }
            else
            {
                cardNumber = cardNumber  + displayCard.substring(i,i+1);
            }
        }

        return cardNumber;
    }


    public void onScanPress()
    {
        emptyFild();

        Intent scanIntent = new Intent(getActivity(), CardIOActivity.class);

        // customize these values to suit your needs.
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, false); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false); // default: false

        // MY_SCAN_REQUEST_CODE is arbitrary and is only used within this activity.
        startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MY_SCAN_REQUEST_CODE) {
            String resultDisplayStr;
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

                // Never log a raw card number. Avoid displaying it, but if necessary use getFormattedCardNumber()
                resultDisplayStr = "Card Number: " + scanResult.getFormattedCardNumber() + "\n";
                et_CardNumber.setText(scanResult.getFormattedCardNumber());

                // Do something with the raw number, e.g.:
                // myService.setCardNumber( scanResult.cardNumber );

                if (scanResult.isExpiryValid()) {
                    resultDisplayStr += "Expiration Date: " + scanResult.expiryMonth + "/" + scanResult.expiryYear + "\n";
                    String str = String.valueOf(scanResult.expiryYear);
                    String substring = str.substring(Math.max(str.length() - 2, 0));

                    if (substring!=null && !substring.equalsIgnoreCase(""))
                    {
                        ExpiryDate = scanResult.expiryMonth +"/"+ substring;
                    }
                    else
                    {
                        ExpiryDate ="";
                    }
                    tv_ExpireDate.setText(monthName[scanResult.expiryMonth-1]);
                    tv_ExpireDate.append(", "+scanResult.expiryYear);

//                    tv_ExpireDate.setText(scanResult.expiryMonth + "/" + scanResult.expiryYear);
                }

                if (scanResult.cvv != null) {
                    // Never log or display a CVV
                    resultDisplayStr += "CVV has " + scanResult.cvv.length() + " digits.\n";
                }

                if (scanResult.postalCode != null) {
                    resultDisplayStr += "Postal Code: " + scanResult.postalCode + "\n";
                }
            }
            else {
                resultDisplayStr = "Scan was canceled.";
            }
            Log.e("resultDisplayStr","resultDisplayStr"+resultDisplayStr);
            // do something with resultDisplayStr, maybe display it in a textView
            // resultTextView.setText(resultDisplayStr);
        }
        else
        {
            Log.e("resultDisplayStr","resultDisplayStr llllllllllllllllllllllllllllllll");
        }
        // else handle other activity results
    }

    private void getTickPayRate()
    {
        dialogClass.showDialog();
        String url = WebServiceAPI.WEB_SERVICE_GET_TICKPAY_RATE;

        Log.e("call", "url = " + url);

        aQuery.ajax(url.trim(), null, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e("responseCode", " = " + responseCode);
                    Log.e("Response", " = " + json);

                    if (json!=null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                String message = "";

                                if (json.has("rate"))
                                {
                                    if (json.getString("rate")!=null && !json.getString("rate").equalsIgnoreCase(""))
                                    {
                                        rate = Double.parseDouble(json.getString("rate"));
                                    }
                                    else
                                    {
                                        rate = 0;
                                    }

                                    message = rate + strRate;
                                }

                                /*if (json.has("TransactionFee"))
                                {
                                    if (json.getString("TransactionFee")!=null && !json.getString("TransactionFee").equalsIgnoreCase(""))
                                    {
                                        transactionRate = Double.parseDouble(json.getString("TransactionFee"));
                                    }
                                    else
                                    {
                                        transactionRate = 0;
                                    }

                                    message = message +"\n"+ String.format("%.1f",transactionRate) + strTransactionRate;
                                }*/

                                tv_Rate.setText(message);

                                dialogClass.hideDialog();
                            }
                            else
                            {
                                String message = "Please try again later";
                                if (json.has("message"))
                                {
                                    message = json.getString("message");
                                }
                                dialogClass.hideDialog();
                                new SnackbarUtils(main_layout, message,
                                        ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }
                        else
                        {
                            String message = "Please try again later";
                            if (json.has("message"))
                            {
                                message = json.getString("message");
                            }
                            dialogClass.hideDialog();
                            new SnackbarUtils(main_layout, message,
                                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                        }
                    }
                    else
                    {
                        String message = "Please try again later";
                        if (json.has("message"))
                        {
                            message = json.getString("message");
                        }
                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout, message,
                                ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Exception","Exception "+e.toString());
                    dialogClass.hideDialog();
                    new SnackbarUtils(main_layout, "Please try again later!",
                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                }
            }

        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }

    public void showYesNoDialog(final String total)
    {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.yes_no_dialog);

        TextView tv_yes = (TextView) dialog.findViewById(R.id.yes);
        TextView tv_No = (TextView) dialog.findViewById(R.id.no);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        LinearLayout ll_Yes = (LinearLayout) dialog.findViewById(R.id.yes_layout);
        LinearLayout ll_No = (LinearLayout) dialog.findViewById(R.id.no_layout);

        tv_Message.setText("Do you want to send invoice receipt ?");

        ll_Yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent intent = new Intent(getActivity(),Receipt_Invoice_Activity.class);
                intent.putExtra("total",total);
                startActivity(intent);
            }
        });

        tv_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent intent = new Intent(getActivity(),Receipt_Invoice_Activity.class);
                intent.putExtra("total",total);
                startActivity(intent);
            }
        });

        ll_No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tv_No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    private void CheckData()
    {
        if (TextUtils.isEmpty(et_companyName.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_name),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else if (TextUtils.isEmpty(et_ABN.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_abn),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else if (TextUtils.isEmpty(et_TotalAmount.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_total_amount),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else if (TextUtils.isEmpty(et_CardNumber.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_card_number),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else if (TextUtils.isEmpty(ExpiryDate))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_expiry_date),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else if (et_Cvv.getVisibility()==View.VISIBLE && TextUtils.isEmpty(et_Cvv.getText().toString().trim()))
        {
            new SnackbarUtils(main_layout, getString(R.string.please_enter_cvv),
                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
        }
        else
        {
            if(et_CardNumber.getText().length()>11) {
                Log.e("flagScan","PaymentTiCKPay : "+Drawer_Activity.flagScan);
                if (!Drawer_Activity.flagScan)
                {
                    if (validornot)
                    {
                        String driverId = SessionSave.getUserSession(Comman.USER_ID, getActivity());
                        if (driverId!=null && !driverId.equalsIgnoreCase(""))
                        {
                            if (ConnectivityReceiver.isConnected())
                            {
                                PaymentTiCKPay(driverId);
                            }
                            else
                            {
                                new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                                        ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                            }

                        }

                    }
                    else {
                        new SnackbarUtils(main_layout, getString(R.string.invalid_card_number),
                                ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                    }
                }
                else
                {
                    String driverId = SessionSave.getUserSession(Comman.USER_ID, getActivity());
                    if (driverId!=null && !driverId.equalsIgnoreCase(""))
                    {
                        if (ConnectivityReceiver.isConnected())
                        {
                            PaymentTiCKPay(driverId);
                        }
                        else
                        {
                            new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                        }
                    }
                }
            }
            else
            {
                new SnackbarUtils(main_layout, getString(R.string.invalid_card_number),
                        ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
            }
        }
    }


    // Log.e("onViewStateRestored","onViewStateRestored");


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        Log.e("onViewStateRestored","onViewStateRestored hidden : "+hidden);
        if (!hidden)
        {
            if (ConnectivityReceiver.isConnected())
            {
                getTickPayRate();
            }
            else
            {
                new SnackbarUtils(main_layout, getString(R.string.not_connected_to_internet),
                        ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
            }
        }
    }

    private void PaymentTiCKPay(String driverId)
    {
        dialogClass.showDialog();
        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_TICKPAY;

        String cardNumber1;
        if(!Drawer_Activity.flagScan)
        {
            cardNumber1 = et_CardNumber.getText().toString();
        }
        else
        {
            cardNumber1 = cardNumber;
        }
//        String cardNumber = et_CardNumber.getText().toString();
        cardNumber1 = cardNumber1.replace(" ","");

        params.put(WebServiceAPI.TICKPAY_PARAM_DRIVER_ID, driverId);
        params.put(WebServiceAPI.TICKPAY_PARAM_DRIVER_NAME, et_companyName.getText().toString());
        params.put(WebServiceAPI.TICKPAY_PARAM_AMOUNT, et_TotalAmount.getText().toString());
        params.put(WebServiceAPI.TICKPAY_PARAM_CARD_NO, cardNumber1);
        params.put(WebServiceAPI.TICKPAY_PARAM_CARD_EXPIRY, ExpiryDate);
        params.put(WebServiceAPI.TICKPAY_PARAM_ABN, et_ABN.getText().toString());

        InvoiceRate = "";
        InvoiceRate = et_TotalAmount.getText().toString();

        if (et_Cvv.getVisibility()==View.VISIBLE)
        {
            params.put(WebServiceAPI.TICKPAY_PARAM_CVV, et_Cvv.getText().toString());
        }

        Log.e("url", "PaymentTiCKPay = " + url);
        Log.e("param", "PaymentTiCKPay = " + params);
        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e("responseCode", "PaymentTiCKPay = " + responseCode);
                    Log.e("Response", "PaymentTiCKPay = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "TRUE");
                                dialogClass.hideDialog();

                                String totalStr = "";
                                if (tv_FinalTotal.getText().toString().length()>=1)
                                {
                                    totalStr = tv_FinalTotal.getText().toString().substring(1,tv_FinalTotal.getText().toString().length());
                                }
                                else
                                {
                                    totalStr = "";
                                }

                                final String total = totalStr;

                                if (json.has("tickpay_id"))
                                {
                                    payId = json.getString("tickpay_id");
                                }
                                else
                                {
                                    payId = "";
                                }

                                et_Cvv.setText("");
                                et_Cvv.setVisibility(View.GONE);
                                et_TotalAmount.setText("");
                                et_CardNumber.setText("");
                                tv_FinalTotal.setText("");
                                showYesNoDialog(total);
//                                if (json.has("message")) {
//                                    new SnackbarUtils(main_layout, json.getString("message"),
//                                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
//                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
//                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
//                                }
                            }
                            else
                            {
                                Log.e("status", "FALSE");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message")) {
                                new SnackbarUtils(main_layout, json.getString("message"),
                                        ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                        ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }
                    }
                    else
                    {
                        Log.e("json", "null");
                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                                ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();

                    }
                }
                catch (Exception e)
                {
                    Log.e("PaymentTiCKPay", "Exception : " + e.toString());
                    dialogClass.hideDialog();
                    new SnackbarUtils(main_layout, getString(R.string.something_is_wrong),
                            ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                            ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }.header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }


    public void showBottomSheetDailog_payment()
    {
        BottomShitview = getLayoutInflater(SavedInstanceState).inflate(R.layout.bottom_sheet_payment, null);
        mBottomSheetDialog = new BottomSheetDialog(getActivity());
        mBottomSheetDialog.setContentView(BottomShitview);

        final TextView tv_Ok, tv_Cancle;

        tv_Ok = (TextView) BottomShitview.findViewById(R.id.bottom_sheet_payment_ok_textview);
        tv_Cancle = (TextView) BottomShitview.findViewById(R.id.bottom_sheet_payment_cancle_textview);
        numberPickerMonth = (NumberPicker) BottomShitview.findViewById(R.id.numberPickerMonth);
        numberPickerYear = (NumberPicker) BottomShitview.findViewById(R.id.numberPickerYear);

        numberPickerMonth.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        numberPickerYear.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        setDividerColor(numberPickerMonth,ContextCompat.getColor(getActivity(), R.color.colorRed));
        setDividerColor(numberPickerYear,ContextCompat.getColor(getActivity(), R.color.colorRed));

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH)+1;

        Log.e("Date","year"+year+"month"+month);

        numberPickerMonth.setMinValue(1);
        numberPickerMonth.setMaxValue(12);
        numberPickerYear.setMinValue(year);
        numberPickerYear.setMaxValue(year+5);

        valueMonth = new String[12];
        valueYear = new String[6];


        for(int i=0; i<12; i++)
        {
            valueMonth[i] = monthName[i]+"";
            Log.d("valueNumber","valueMonth : "+ valueMonth[i]);
        }

        for(int i=0; i<6; i++)
        {
            valueYear[i] = (year+i)+"";
            Log.d("valueNumber","valueYear : "+ valueYear[i]);
        }

        numberPickerMonth.setValue(month);
        numberPickerMonth.setDisplayedValues(monthName);
        numberPickerYear.setValue(year);


        tv_Cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mBottomSheetDialog!=null)
                {
                    mBottomSheetDialog.dismiss();
                }
            }
        });

        tv_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBottomSheetDialog!=null)
                {
                    MonthInNumber = numberPickerMonth.getValue();
                    YearInNumber = numberPickerYear.getValue();

                    Log.e("MonthInNumber"," = "+MonthInNumber);
                    Log.e("YearInNumber"," = "+YearInNumber);
                    String str = String.valueOf(YearInNumber);
                    String substring = str.substring(Math.max(str.length() - 2, 0));

                    if (substring!=null && !substring.equalsIgnoreCase(""))
                    {
                        ExpiryDate = MonthInNumber +"/"+ substring;
                    }
                    else
                    {
                        ExpiryDate ="";
                    }


                    tv_ExpireDate.setText(monthName[MonthInNumber-1]);
                    tv_ExpireDate.append(", "+YearInNumber);

                    mBottomSheetDialog.dismiss();
                }
            }
        });

        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });

        mBottomSheetDialog.show();
    }
    private void setDividerColor(NumberPicker picker, int color) {

        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    public class FourDigitCardFormatWatcher implements TextWatcher {

        // Change this to what you want... ' ', '-' etc..
        private static final char space = ' ';

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            // Remove spacing char
            if (s.length() > 0 && (s.length() % 5) == 0) {
                final char c = s.charAt(s.length() - 1);
                if (space == c) {
                    s.delete(s.length() - 1, s.length());
                }
            }
            // Insert char where needed.
            if (s.length() > 0 && (s.length() % 5) == 0) {
                char c = s.charAt(s.length() - 1);
                // Only if its a digit where there should be a space we insert a space
                if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 3) {
                    s.insert(s.length() - 1, String.valueOf(space));
                }
            }
            FindCardType(s);
        }
    }

    public void FindCardType(CharSequence s) {

        String valid = "INVALID";

        if (s.toString().length() > 5) {
            String number = s.toString().replace(" ", "");
            String digit1 = number.substring(0, 1);
            String digit2 = number.substring(0, 2);
            String digit3 = number.substring(0, 3);
            String digit4 = number.substring(0, 4);

            et_CardNumber.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorBlack));

            if (digit1.equals("4")) {
                if (number.length() == 13 || number.length() == 16) {
                    valid = "VISA";
                    validornot = validCCNumber(number);

                    if (number.length() > 12) {
                        if (validornot) {
                            et_CardNumber.setTextColor(ContextCompat.getColor(getActivity(), R.color.green));
                        } else {
                            et_CardNumber.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorRed));
                            new SnackbarUtils(main_layout, getResources().getString(R.string.card_is_not_valid),
                                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                        }
                    }
                }
            } else if (digit4.equalsIgnoreCase("5018") || digit4.equalsIgnoreCase("5020") || digit4.equalsIgnoreCase("5038") || digit4.equalsIgnoreCase("5612") || digit4.equalsIgnoreCase("5893")
                    || digit4.equalsIgnoreCase("6304") || digit4.equalsIgnoreCase("6759") || digit4.equalsIgnoreCase("6761") || digit4.equalsIgnoreCase("6762") || digit4.equalsIgnoreCase("6763")
                    || digit4.equalsIgnoreCase("0604") || digit4.equalsIgnoreCase("6390")) {
                if (number.length() == 16) {
                    valid = "MAESTRO";
                    Log.e("MAESTRO", "MAESTRO");
                    validornot = validCCNumber(number);

                    if (number.length() == 16) {
                        if (validornot) {
                            et_CardNumber.setTextColor(ContextCompat.getColor(getActivity(), R.color.green));

                        } else {
                            et_CardNumber.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorRed));
                            new SnackbarUtils(main_layout,getResources().getString(R.string.card_is_not_valid),
                                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                        }
                    }
                }
            } else if (digit2.equals("34") || digit2.equals("37")) {
                if (number.length() == 15) {
                    valid = "AMERICAN_EXPRESS";
                    validornot = validCCNumber(number);

                    if (number.length() == 15) {
                        if (validornot) {
                            et_CardNumber.setTextColor(ContextCompat.getColor(getActivity(), R.color.green));

                        } else {
                            et_CardNumber.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorRed));
                            new SnackbarUtils(main_layout, getResources().getString(R.string.card_is_not_valid),
                                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                        }
                    }
                }
            } else if (digit2.equals("36") || digit2.equals("38") || (digit3.compareTo("300") >= 0 && digit3.compareTo("305") <= 0)) {
                if (number.length() == 14) {
                    valid = "DINERS_CLUB";
                    validornot = validCCNumber(number);

                    if (number.length() == 14) {
                        if (validornot) {
                            et_CardNumber.setTextColor(ContextCompat.getColor(getActivity(), R.color.green));

                        } else {
                            et_CardNumber.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorRed));
                            new SnackbarUtils(main_layout, getResources().getString(R.string.card_is_not_valid),
                                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                        }
                    }
                }

            } else if (digit1.equals("6")) {
                if (number.length() == 16) {
                    valid = "DISCOVER";
                    validornot = validCCNumber(number);

                    if (number.length() == 16) {
                        if (validornot) {
                            et_CardNumber.setTextColor(ContextCompat.getColor(getActivity(), R.color.green));

                        } else {
                            et_CardNumber.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorRed));
                            new SnackbarUtils(main_layout, getResources().getString(R.string.card_is_not_valid),
                                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                        }
                    }
                }

            } else if (digit2.equals("35")) {
                if (number.length() == 16 || number.length() == 17 || number.length() == 18 || number.length() == 19) {
                    valid = "JBC";
                    validornot = validCCNumber(number);

                    if (number.length() > 15) {
                        if (validornot) {
                            et_CardNumber.setTextColor(ContextCompat.getColor(getActivity(), R.color.green));

                        } else {
                            et_CardNumber.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorRed));
                            new SnackbarUtils(main_layout, getResources().getString(R.string.card_is_not_valid),
                                    ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                    ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                        }
                    }
                }

            } else if (digit2.compareTo("51") >= 0 && digit2.compareTo("55") <= 0 || digit1.equalsIgnoreCase("2")) {
                if (number.length() == 16)
                    valid = "MASTERCARD";
                validornot = validCCNumber(number);

                if (number.length() == 16) {
                    if (validornot) {
                        et_CardNumber.setTextColor(ContextCompat.getColor(getActivity(), R.color.green));

                    } else {
                        et_CardNumber.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorRed));
                        new SnackbarUtils(main_layout, getResources().getString(R.string.card_is_not_valid),
                                ContextCompat.getColor(getActivity(), R.color.snakbar_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color),
                                ContextCompat.getColor(getActivity(), R.color.snackbar_text_color)).snackieBar().show();
                    }
                }
            }
        }
    }

    public static boolean validCCNumber(String n) {
        try {

            int j = n.length();

            String [] s1 = new String[j];
            for (int i=0; i < n.length(); i++) s1[i] = "" + n.charAt(i);

            int checksum = 0;

            for (int i=s1.length-1; i >= 0; i-= 2) {
                int k = 0;

                if (i > 0) {
                    k = Integer.valueOf(s1[i-1]).intValue() * 2;
                    if (k > 9) {
                        String s = "" + k;
                        k = Integer.valueOf(s.substring(0,1)).intValue() +
                                Integer.valueOf(s.substring(1)).intValue();
                    }
                    checksum += Integer.valueOf(s1[i]).intValue() + k;
                }
                else
                    checksum += Integer.valueOf(s1[0]).intValue();
            }
            return ((checksum % 10) == 0);
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
