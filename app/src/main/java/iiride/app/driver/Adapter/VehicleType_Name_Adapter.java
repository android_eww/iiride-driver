package iiride.app.driver.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import iiride.app.driver.Been.VehicleType_Been;
import iiride.app.driver.R;


import java.util.ArrayList;

/**
 * Created by EXCELLENT-14 on 13-Oct-17.
 */

public class VehicleType_Name_Adapter extends BaseAdapter {

    private Activity activity;
    private static LayoutInflater inflater=null;
    ArrayList<VehicleType_Been> list_vehicleItem;

    public VehicleType_Name_Adapter(Activity a, ArrayList<VehicleType_Been> list_vehicleItem) {
        activity = a;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list_vehicleItem = list_vehicleItem;
    }

    public int getCount() {
        return list_vehicleItem.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, final ViewGroup parent)
    {
        View vi=convertView;
        if(convertView==null) vi = inflater.inflate(R.layout.listview_row_item_vehicle_type, null);

        LinearLayout row_vehicleType_item;
        TextView tv_vehicle_type, tv_vehicle_description;
        final CheckBox ckBox_vehi_type;

        row_vehicleType_item = (LinearLayout) vi.findViewById(R.id.row_vehicleType_item);
        tv_vehicle_type = (TextView)vi.findViewById(R.id.tv_vehicle_type);
        tv_vehicle_description = (TextView)vi.findViewById(R.id.tv_vehicle_description);
        ckBox_vehi_type = (CheckBox)vi.findViewById(R.id.ckBox_vehi_type);

        tv_vehicle_type.setText(list_vehicleItem.get(position).getName());
        tv_vehicle_description.setText(list_vehicleItem.get(position).getDescription());

        if (list_vehicleItem.get(position).getStatus().equalsIgnoreCase("1"))
        {
            ckBox_vehi_type.setChecked(true);
        }
        else
        {
            ckBox_vehi_type.setChecked(false);
        }


        row_vehicleType_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0; i<list_vehicleItem.size() ; i++)
                {
                    list_vehicleItem.get(i).setStatus("0");
                }
                list_vehicleItem.get(position).setStatus("1");
                notifyDataSetChanged();
            }
        });


        return vi;
    }
}