package iiride.app.driver.Adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.telephony.TelephonyManager;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import iiride.app.driver.Fragment.Past_Job_List_Fragment;


import iiride.app.driver.R;
import iiride.app.driver.Util.Utility;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.MyAlertDialog;
import com.androidquery.AQuery;
import com.hendraanggrian.widget.ExpandableItem;
import com.hendraanggrian.widget.ExpandableRecyclerView;

import java.text.SimpleDateFormat;
import java.util.Calendar;


public class Past_Job_List_Adapter extends ExpandableRecyclerView.Adapter<Past_Job_List_Adapter.ViewHolder> {
    Context context;
    int POSITION;
    String TAG = "AcceptDispatchJob_Req";
    private AQuery aQuery;
    DialogClass dialogClass;


    public Past_Job_List_Adapter(LinearLayoutManager lm) {
        super(lm);
    }


    public class ViewHolder extends ExpandableRecyclerView.ViewHolder {
    public TextView  tv_tripId;
    public View layout;

    LinearLayout ll_row_item_pastJob, ll_pickUpLoc, ll_passengerNo, ll_tripDistance, ll_carModel, ll_suburb, ll_dropOffLoc, ll_dateTime,ll_pickupTime,
            ll_passengerEmail, ll_dropOffTime, ll_tripDuration, ll_nightFare, ll_tripFare, ll_waitingTime, ll_waitingTimeCost,ll_tollFee,
            ll_BookingCharge, ll_tax, ll_promocode, ll_discount, ll_subTotal, ll_grandTotal, ll_TripStatus, ll_PaymentType, ll_passengerFlightNo,
            ll_passengerNote,ll_dispatcherName, ll_dispatcherEmail, ll_dispatcherNo;

    public TextView tv_drop_off_location, tv_pickupLocation, tv_date_time, tv_PassengerName, tv_passengerNo, tv_tripDistance, tv_carModel, tv_suburb,tv_pickupTime, tv_passengerEmail, tv_dropOffTime
            , tv_tripDuration, tv_nightFare, tv_tripFare, tv_waitingTime, tv_waitingTimeCost,tv_tollFee, tv_BookingCharge, tv_tax, tv_promocode, tv_discount, tv_subTotal, tv_grandTotal
            , tv_TripStatus, tv_PaymentType, tv_passengerFlightNo, tv_passengerNote,tv_dispatcherName, tv_dispatcherEmail, tv_dispatcherNo;

    ExpandableItem expandableItem;


    public ViewHolder(View v) {
        super(v);
        layout = v;
        expandableItem = (ExpandableItem) v. findViewById(R.id.row);
        tv_drop_off_location = (TextView) expandableItem.findViewById(R.id.tv_drop_off_location);
        tv_pickupLocation = (TextView) expandableItem.findViewById(R.id.tv_pickupLocation);
        tv_suburb = (TextView) expandableItem.findViewById(R.id.tv_suburb);
        tv_date_time = (TextView) expandableItem.findViewById(R.id.tv_date_time);
        tv_PassengerName = (TextView) expandableItem.findViewById(R.id.tv_PassengerName);
        tv_tripId = (TextView) expandableItem.findViewById(R.id.tv_tripId);
        tv_passengerNo = (TextView) expandableItem.findViewById(R.id.tv_passengerNo);
        tv_tripDistance = (TextView) expandableItem.findViewById(R.id.tv_tripDistance);
        tv_carModel = (TextView) expandableItem.findViewById(R.id.tv_carModel);
        tv_pickupTime = (TextView) expandableItem.findViewById(R.id.tv_pickupTime);
        tv_passengerEmail = (TextView) expandableItem.findViewById(R.id.tv_passengerEmail);

        tv_dropOffTime = (TextView) expandableItem.findViewById(R.id.tv_dropOffTime);
        tv_tripDuration = (TextView) expandableItem.findViewById(R.id.tv_tripDuration);
        tv_nightFare = (TextView) expandableItem.findViewById(R.id.tv_nightFare);
        tv_tripFare = (TextView) expandableItem.findViewById(R.id.tv_tripFare);
        tv_waitingTime = (TextView) expandableItem.findViewById(R.id.tv_waitingTime);

        tv_waitingTimeCost = (TextView) expandableItem.findViewById(R.id.tv_waitingTimeCost);
        tv_tollFee = (TextView) expandableItem.findViewById(R.id.tv_tollFee);
        tv_BookingCharge = (TextView) expandableItem.findViewById(R.id.tv_BookingCharge);
        tv_tax = (TextView) expandableItem.findViewById(R.id.tv_tax);
        tv_promocode = (TextView) expandableItem.findViewById(R.id.tv_promocode);

        tv_discount = (TextView) expandableItem.findViewById(R.id.tv_discount);
        tv_subTotal = (TextView) expandableItem.findViewById(R.id.tv_subTotal);
        tv_grandTotal = (TextView) expandableItem.findViewById(R.id.tv_grandTotal);
        tv_TripStatus = (TextView) expandableItem.findViewById(R.id.tv_TripStatus);
        tv_PaymentType = (TextView) expandableItem.findViewById(R.id.tv_PaymentType);
        tv_passengerFlightNo = (TextView) expandableItem.findViewById(R.id.tv_passengerFlightNo);
        tv_passengerNote = (TextView) expandableItem.findViewById(R.id.tv_passengerNote);
        tv_dispatcherName = (TextView) expandableItem.findViewById(R.id.tv_dispatcherName);
        tv_dispatcherEmail = (TextView) expandableItem.findViewById(R.id.tv_dispatcherEmail);
        tv_dispatcherNo = (TextView) expandableItem.findViewById(R.id.tv_dispatcherNo);


        ll_row_item_pastJob = (LinearLayout) expandableItem.findViewById(R.id.ll_row_item_pastJob);
        ll_pickUpLoc = (LinearLayout) expandableItem.findViewById(R.id.ll_pickUpLoc);
        ll_passengerNo = (LinearLayout) expandableItem.findViewById(R.id.ll_passengerNo);
        ll_tripDistance = (LinearLayout) expandableItem.findViewById(R.id.ll_tripDistance);
        ll_carModel = (LinearLayout) expandableItem.findViewById(R.id.ll_carModel);
        ll_suburb = (LinearLayout) expandableItem.findViewById(R.id.ll_suburb);
        ll_dropOffLoc = (LinearLayout) expandableItem.findViewById(R.id.ll_dropOffLoc);
        ll_dateTime = (LinearLayout) expandableItem.findViewById(R.id.ll_dateTime);
        ll_pickupTime = (LinearLayout) expandableItem.findViewById(R.id.ll_pickupTime);
        ll_passengerEmail = (LinearLayout) expandableItem.findViewById(R.id.ll_passengerEmail);

        ll_dropOffTime = (LinearLayout) expandableItem.findViewById(R.id.ll_dropOffTime);
        ll_tripDuration = (LinearLayout) expandableItem.findViewById(R.id.ll_tripDuration);
        ll_nightFare = (LinearLayout) expandableItem.findViewById(R.id.ll_nightFare);
        ll_tripFare = (LinearLayout) expandableItem.findViewById(R.id.ll_tripFare);
        ll_waitingTime = (LinearLayout) expandableItem.findViewById(R.id.ll_waitingTime);

        ll_waitingTimeCost = (LinearLayout) expandableItem.findViewById(R.id.ll_waitingTimeCost);
        ll_tollFee = (LinearLayout) expandableItem.findViewById(R.id.ll_tollFee);
        ll_BookingCharge = (LinearLayout) expandableItem.findViewById(R.id.ll_BookingCharge);
        ll_tax = (LinearLayout) expandableItem.findViewById(R.id.ll_tax);
        ll_promocode = (LinearLayout) expandableItem.findViewById(R.id.ll_promocode);

        ll_discount = (LinearLayout) expandableItem.findViewById(R.id.ll_discount);
        ll_subTotal = (LinearLayout) expandableItem.findViewById(R.id.ll_subTotal);
        ll_grandTotal = (LinearLayout) expandableItem.findViewById(R.id.ll_grandTotal);
        ll_TripStatus = (LinearLayout) expandableItem.findViewById(R.id.ll_TripStatus);
        ll_PaymentType = (LinearLayout) expandableItem.findViewById(R.id.ll_PaymentType);
        ll_passengerFlightNo = (LinearLayout) expandableItem.findViewById(R.id.ll_passengerFlightNo);
        ll_passengerNote = (LinearLayout) expandableItem.findViewById(R.id.ll_passengerNote);

        ll_dispatcherName = (LinearLayout) expandableItem.findViewById(R.id.ll_dispatcherName);
        ll_dispatcherEmail = (LinearLayout) expandableItem.findViewById(R.id.ll_dispatcherEmail);
        ll_dispatcherNo = (LinearLayout) expandableItem.findViewById(R.id.ll_dispatcherNo);
    }
}


    @Override
    public Past_Job_List_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        this.context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.row_layout_past_job, parent, false);

        Past_Job_List_Adapter.ViewHolder vh = new Past_Job_List_Adapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final Past_Job_List_Adapter.ViewHolder holder, final int position)
    {
        super.onBindViewHolder(holder, position);
        POSITION = position;

        if (Past_Job_List_Fragment.list.get(position).getDropoffLocation()!=null && !Past_Job_List_Fragment.list.get(position).getDropoffLocation().equalsIgnoreCase(""))
        {
            holder.ll_dropOffLoc.setVisibility(View.VISIBLE);
            holder.tv_drop_off_location.setText(Past_Job_List_Fragment.list.get(position).getDropoffLocation());
        }
        else
        {
            holder.ll_dropOffLoc.setVisibility(View.GONE);
        }

        if (Past_Job_List_Fragment.list.get(position).getStatus()!=null && !Past_Job_List_Fragment.list.get(position).getStatus().equalsIgnoreCase(""))
        {
            holder.ll_TripStatus.setVisibility(View.VISIBLE);
            holder.tv_TripStatus.setAllCaps(true);
            holder.tv_TripStatus.setText(Past_Job_List_Fragment.list.get(position).getStatus());
        }
        else
        {
            holder.ll_TripStatus.setVisibility(View.GONE);
        }

        holder.ll_suburb.setVisibility(View.GONE);
        /*if (Past_Job_List_Fragment.list.get(position).getDropoffLocation()!=null && !Past_Job_List_Fragment.list.get(position).getDropoffLocation().equalsIgnoreCase(""))
        {
            holder.tv_suburb.setText(Past_Job_List_Fragment.list.get(position).getDropoffLocation());
        }
        else
        {
            holder.ll_suburb.setVisibility(View.GONE);
        }*/

        if (Past_Job_List_Fragment.list.get(position).getPickupLocation()!=null && !Past_Job_List_Fragment.list.get(position).getPickupLocation().equalsIgnoreCase(""))
        {
            holder.ll_pickUpLoc.setVisibility(View.VISIBLE);
            holder.tv_pickupLocation.setText(Past_Job_List_Fragment.list.get(position).getPickupLocation());
        }
        else
        {
            holder.ll_pickUpLoc.setVisibility(View.GONE);
        }

        if (Past_Job_List_Fragment.list.get(position).getPickupDateTime()!=null && !Past_Job_List_Fragment.list.get(position).getPickupDateTime().equalsIgnoreCase("")&& !Past_Job_List_Fragment.list.get(position).getPickupDateTime().equalsIgnoreCase("false"))
        {
            holder.ll_dateTime.setVisibility(View.VISIBLE);
            /*String pickUpDateTime = getDate(Long.parseLong(Past_Job_List_Fragment.list.get(position).getPickupDateTime()+"000"), "HH:mm  dd/MM/yyyy");
            Log.e("pickUpDateTime","pickUpDateTime : "+pickUpDateTime);*/
            holder.tv_date_time.setText(Utility.getDateInFormat(Past_Job_List_Fragment.list.get(position).getPickupDateTime()));
        }
        else
        {
            holder.ll_dateTime.setVisibility(View.GONE);
        }

        if (Past_Job_List_Fragment.list.get(position).getPassengerName()!=null &&
                !Past_Job_List_Fragment.list.get(position).getPassengerName().equalsIgnoreCase(""))
        {
            holder.tv_PassengerName.setVisibility(View.VISIBLE);
            holder.tv_PassengerName.setText(Past_Job_List_Fragment.list.get(position).getPassengerName());
        }
        else
        {
            holder.tv_PassengerName.setVisibility(View.GONE);
        }

        if (Past_Job_List_Fragment.list.get(position).getPassengerMobileNo()!=null && !Past_Job_List_Fragment.list.get(position).getPassengerMobileNo().equalsIgnoreCase(""))
        {
            holder.ll_passengerNo.setVisibility(View.VISIBLE);
            String PassengerMobNo = Past_Job_List_Fragment.list.get(position).getPassengerMobileNo();
            SpannableString content = new SpannableString(PassengerMobNo);
            content.setSpan(new UnderlineSpan(), 0, PassengerMobNo.length(), 0);
            holder.tv_passengerNo.setText(content);
        }
        else
        {
            holder.ll_passengerNo.setVisibility(View.GONE);
        }

        if (Past_Job_List_Fragment.list.get(position).getTripDistance()!=null && !Past_Job_List_Fragment.list.get(position).getTripDistance().equalsIgnoreCase(""))
        {
            holder.ll_tripDistance.setVisibility(View.VISIBLE);
            holder.tv_tripDistance.setText(Past_Job_List_Fragment.list.get(position).getTripDistance());
        }
        else
        {
            holder.ll_tripDistance.setVisibility(View.GONE);
        }

        if (Past_Job_List_Fragment.list.get(position).getModel()!=null && !Past_Job_List_Fragment.list.get(position).getModel().equalsIgnoreCase(""))
        {
            holder.ll_carModel.setVisibility(View.VISIBLE);
            holder.tv_carModel.setText(Past_Job_List_Fragment.list.get(position).getModel());
        }
        else
        {
            holder.ll_carModel.setVisibility(View.GONE);
        }

        if (Past_Job_List_Fragment.list.get(position).getPickupDateTime()!=null && !Past_Job_List_Fragment.list.get(position).getPickupDateTime().equalsIgnoreCase("") && !Past_Job_List_Fragment.list.get(position).getPickupDateTime().equalsIgnoreCase("false"))
        {
            holder.ll_pickupTime.setVisibility(View.VISIBLE);
            /*String pickUpDateTime = getDate(Long.parseLong(Past_Job_List_Fragment.list.get(position).getPickupDateTime()+"000"), "HH:mm  dd/MM/yyyy");
            Log.e("pickUpDateTime","pickUpDateTime : "+pickUpDateTime);*/
            holder.tv_pickupTime.setText(Utility.getDateInFormat(Past_Job_List_Fragment.list.get(position).getPickupDateTime()));
        }
        else
        {
            holder.ll_pickupTime.setVisibility(View.GONE);
        }
        if (Past_Job_List_Fragment.list.get(position).getPassengerEmail()!=null && !Past_Job_List_Fragment.list.get(position).getPassengerEmail().equalsIgnoreCase(""))
        {
            holder.ll_passengerEmail.setVisibility(View.VISIBLE);
            holder.tv_passengerEmail.setText(Past_Job_List_Fragment.list.get(position).getPassengerEmail());
        }
        else
        {
            holder.ll_passengerEmail.setVisibility(View.GONE);
        }

        if (Past_Job_List_Fragment.list.get(position).getDropOffDateTime()!=null && !Past_Job_List_Fragment.list.get(position).getDropOffDateTime().equalsIgnoreCase("") && !Past_Job_List_Fragment.list.get(position).getDropOffDateTime().equalsIgnoreCase("false"))
        {
            holder.ll_dropOffTime.setVisibility(View.VISIBLE);
           /* String getDropTime = getDate(Long.parseLong(Past_Job_List_Fragment.list.get(position).getDropOffDateTime()+"000"), "HH:mm  dd/MM/yyyy");
            Log.e("pickUpDateTime","getDropTime : "+getDropTime);*/
            holder.tv_dropOffTime.setText(Utility.getDateInFormat(Past_Job_List_Fragment.list.get(position).getDropOffDateTime()));
        }
        else
        {
            holder.ll_dropOffTime.setVisibility(View.GONE);
        }

        if (Past_Job_List_Fragment.list.get(position).getTripDuration()!=null && !Past_Job_List_Fragment.list.get(position).getTripDuration().equalsIgnoreCase(""))
        {
            holder.ll_tripDuration.setVisibility(View.VISIBLE);
            holder.tv_tripDuration.setText(Past_Job_List_Fragment.list.get(position).getTripDuration());
        }
        else
        {
            holder.ll_tripDuration.setVisibility(View.GONE);
        }

        if (Past_Job_List_Fragment.list.get(position).getNightFare()!=null && !Past_Job_List_Fragment.list.get(position).getNightFare().equalsIgnoreCase(""))
        {
            holder.ll_nightFare.setVisibility(View.VISIBLE);
            holder.tv_nightFare.setText(Past_Job_List_Fragment.list.get(position).getNightFare());
        }
        else
        {
            holder.ll_nightFare.setVisibility(View.GONE);
        }

        if (Past_Job_List_Fragment.list.get(position).getTripFare()!=null && !Past_Job_List_Fragment.list.get(position).getTripFare().equalsIgnoreCase(""))
        {
            holder.ll_tripFare.setVisibility(View.VISIBLE);
            holder.tv_tripFare.setText(Past_Job_List_Fragment.list.get(position).getTripFare());
        }
        else
        {
            holder.ll_tripFare.setVisibility(View.GONE);
        }

        if (Past_Job_List_Fragment.list.get(position).getWaitingTime()!=null && !Past_Job_List_Fragment.list.get(position).getWaitingTime().equalsIgnoreCase(""))
        {
            holder.ll_waitingTime.setVisibility(View.VISIBLE);
            holder.tv_waitingTime.setText(Past_Job_List_Fragment.list.get(position).getWaitingTime());
        }
        else
        {
            holder.ll_waitingTime.setVisibility(View.GONE);
        }

        if (Past_Job_List_Fragment.list.get(position).getWaitingTimeCost()!=null && !Past_Job_List_Fragment.list.get(position).getWaitingTimeCost().equalsIgnoreCase(""))
        {
            holder.ll_waitingTimeCost.setVisibility(View.VISIBLE);
            holder.tv_waitingTimeCost.setText(Past_Job_List_Fragment.list.get(position).getWaitingTimeCost());
        }
        else
        {
            holder.ll_waitingTimeCost.setVisibility(View.GONE);
        }

        if (Past_Job_List_Fragment.list.get(position).getTollFee()!=null && !Past_Job_List_Fragment.list.get(position).getTollFee().equalsIgnoreCase(""))
        {
            holder.ll_tollFee.setVisibility(View.GONE);
            holder.tv_tollFee.setText(Past_Job_List_Fragment.list.get(position).getTollFee());
        }
        else
        {
            holder.ll_tollFee.setVisibility(View.GONE);
        }

        if (Past_Job_List_Fragment.list.get(position).getBookingCharge()!=null && !Past_Job_List_Fragment.list.get(position).getBookingCharge().equalsIgnoreCase(""))
        {
            holder.ll_BookingCharge.setVisibility(View.VISIBLE);
            holder.tv_BookingCharge.setText(Past_Job_List_Fragment.list.get(position).getBookingCharge());
        }
        else
        {
            holder.ll_BookingCharge.setVisibility(View.GONE);
        }

        if (Past_Job_List_Fragment.list.get(position).getTax()!=null && !Past_Job_List_Fragment.list.get(position).getTax().equalsIgnoreCase(""))
        {
            holder.ll_tax.setVisibility(View.VISIBLE);
            holder.tv_tax.setText(Past_Job_List_Fragment.list.get(position).getTax());
        }
        else
        {
            holder.ll_tax.setVisibility(View.GONE);
        }

        if (Past_Job_List_Fragment.list.get(position).getPromoCode()!=null && !Past_Job_List_Fragment.list.get(position).getPromoCode().equalsIgnoreCase(""))
        {
            holder.ll_promocode.setVisibility(View.VISIBLE);
            holder.tv_promocode.setText(Past_Job_List_Fragment.list.get(position).getPromoCode());
        }
        else
        {
            holder.ll_promocode.setVisibility(View.GONE);
        }

        if (Past_Job_List_Fragment.list.get(position).getDiscount()!=null && !Past_Job_List_Fragment.list.get(position).getDiscount().equalsIgnoreCase(""))
        {
            holder.ll_discount.setVisibility(View.VISIBLE);
            holder.tv_discount.setText(Past_Job_List_Fragment.list.get(position).getDiscount());
        }
        else
        {
            holder.ll_discount.setVisibility(View.GONE);
        }

        if (Past_Job_List_Fragment.list.get(position).getSubTotal()!=null && !Past_Job_List_Fragment.list.get(position).getSubTotal().equalsIgnoreCase(""))
        {
            holder.ll_subTotal.setVisibility(View.VISIBLE);
            holder.tv_subTotal.setText(Past_Job_List_Fragment.list.get(position).getSubTotal());
        }
        else
        {
            holder.ll_subTotal.setVisibility(View.GONE);
        }

        if (Past_Job_List_Fragment.list.get(position).getGrandTotal()!=null && !Past_Job_List_Fragment.list.get(position).getGrandTotal().equalsIgnoreCase(""))
        {
            holder.ll_grandTotal.setVisibility(View.VISIBLE);
            holder.tv_grandTotal.setText(Past_Job_List_Fragment.list.get(position).getGrandTotal());
        }
        else
        {
            holder.ll_grandTotal.setVisibility(View.GONE);
        }

        if (Past_Job_List_Fragment.list.get(position).getPaymentType()!=null && !Past_Job_List_Fragment.list.get(position).getPaymentType().equalsIgnoreCase(""))
        {
            holder.ll_PaymentType.setVisibility(View.VISIBLE);
            holder.tv_PaymentType.setText(Past_Job_List_Fragment.list.get(position).getPaymentType());
        }
        else
        {
            holder.ll_PaymentType.setVisibility(View.GONE);
        }

        if (Past_Job_List_Fragment.list.get(position).getFlightNumber()!=null && !Past_Job_List_Fragment.list.get(position).getFlightNumber().equalsIgnoreCase(""))
        {
            holder.ll_passengerFlightNo.setVisibility(View.VISIBLE);
            holder.tv_passengerFlightNo.setText(Past_Job_List_Fragment.list.get(position).getFlightNumber());
        }
        else
        {
            holder.ll_passengerFlightNo.setVisibility(View.GONE);
        }

        if (Past_Job_List_Fragment.list.get(position).getNotes()!=null && !Past_Job_List_Fragment.list.get(position).getNotes().equalsIgnoreCase(""))
        {
            holder.ll_passengerNote.setVisibility(View.VISIBLE);
            holder.tv_passengerNote.setText(Past_Job_List_Fragment.list.get(position).getNotes());
        }
        else
        {
            holder.ll_passengerNote.setVisibility(View.GONE);
        }

        if (Past_Job_List_Fragment.list.get(position).getDispatherFullname()!=null && !Past_Job_List_Fragment.list.get(position).getDispatherFullname().equalsIgnoreCase(""))
        {
            holder.ll_dispatcherName.setVisibility(View.VISIBLE);
            holder.tv_dispatcherName.setText(Past_Job_List_Fragment.list.get(position).getDispatherFullname());
        }
        else
        {
            holder.ll_dispatcherName.setVisibility(View.GONE);
            holder.ll_dispatcherName.setVisibility(View.GONE);
        }

        if (Past_Job_List_Fragment.list.get(position).getDispatherEmail()!=null && !Past_Job_List_Fragment.list.get(position).getDispatherEmail().equalsIgnoreCase(""))
        {
            holder.ll_dispatcherEmail.setVisibility(View.VISIBLE);
            holder.tv_dispatcherEmail.setText(Past_Job_List_Fragment.list.get(position).getDispatherEmail());
        }
        else
        {
            holder.ll_dispatcherEmail.setVisibility(View.GONE);
        }

        if (Past_Job_List_Fragment.list.get(position).getDispatherMobileNo()!=null && !Past_Job_List_Fragment.list.get(position).getDispatherMobileNo().equalsIgnoreCase(""))
        {
            holder.ll_dispatcherNo.setVisibility(View.VISIBLE);
            holder.tv_dispatcherNo.setText(Past_Job_List_Fragment.list.get(position).getDispatherMobileNo());
        }
        else
        {
            holder.ll_dispatcherNo.setVisibility(View.GONE);
        }

        holder.tv_passengerNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callToDriver(Past_Job_List_Fragment.list.get(position).getPassengerMobileNo());
            }
        });
    }

    public void callToDriver(String passengerContact)
    {
        TelephonyManager telMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        MyAlertDialog internetDialog = null;
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                internetDialog = new MyAlertDialog(context);
                internetDialog.showOKDialog("Sim card not available");
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                internetDialog = new MyAlertDialog(context);
                internetDialog.showOKDialog("Sim state network locked");
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                internetDialog = new MyAlertDialog(context);
                internetDialog.showOKDialog("Sim state pin required");
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                internetDialog = new MyAlertDialog(context);
                internetDialog.showOKDialog("Sim state puk required");
                break;
            case TelephonyManager.SIM_STATE_READY:
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + passengerContact));
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
                {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CALL_PHONE}, 1234);
                }
                else
                {
                    context.startActivity(intent);
                }
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                internetDialog = new MyAlertDialog(context);
                internetDialog.showOKDialog("Sim state unknown");
                break;
        }
    }


    public String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    @Override
    public int getItemCount() {
        return Past_Job_List_Fragment.list.size();
    }


}