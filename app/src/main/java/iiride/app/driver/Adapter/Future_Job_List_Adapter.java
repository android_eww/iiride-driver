package iiride.app.driver.Adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.telephony.TelephonyManager;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import iiride.app.driver.Activity.Drawer_Activity;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;
import iiride.app.driver.Fragment.Future_JobBooking_List_Fragment;
import iiride.app.driver.Fragment.Pending_Job_List_Fragment;


import iiride.app.driver.R;
import iiride.app.driver.Util.Utility;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.MyAlertDialog;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.hendraanggrian.widget.ExpandableItem;
import com.hendraanggrian.widget.ExpandableRecyclerView;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


public class Future_Job_List_Adapter extends ExpandableRecyclerView.Adapter<Future_Job_List_Adapter.ViewHolder> {
    Context context;
    int POSITION;
    String TAG = "AcceptDispatchJob_Req";
    private AQuery aQuery;
    DialogClass dialogClass;


    public Future_Job_List_Adapter(LinearLayoutManager lm) {
        super(lm);
    }


    public class ViewHolder extends ExpandableRecyclerView.ViewHolder {
        public View layout;

        LinearLayout ll_row_item_pastJob, ll_suburb, ll_dropOffLoc, ll_dateTime, ll_pickUpLoc, ll_TripDistance, ll_passengerNo, ll_carModel, ll_PaymentType, ll_passengerFlightNo, ll_passengerNote
                ,ll_dispatcherName, ll_dispatcherEmail, ll_dispatcherNo, ll_TripFare, ll_passengerName, ll_passengerEmail;
        TextView tv_PassengerName, tv_suburb, tv_drop_off_location, tv_date_time, tv_pickupLocation, tv_TripDistance, tv_passengerNo, tv_carModel,tv_carModel1, tv_PaymentType, tv_passengerFlightNo, tv_passengerNote
                ,tv_dispatcherName, tv_dispatcherEmail, tv_dispatcherNo, tv_TripFare,  tv_PassengerEmail, tv_PassengerName1;
        ImageView iv_status_active;
        ExpandableItem expandableItem;

        TextView tv_estimateEarning;
        LinearLayout ll_driverEarning;


        public ViewHolder(View v) {
            super(v);
            layout = v;
            expandableItem = (ExpandableItem) v. findViewById(R.id.row);

            ll_row_item_pastJob = (LinearLayout) expandableItem.findViewById(R.id.ll_row_item_pastJob);
            ll_suburb = (LinearLayout) expandableItem.findViewById(R.id.ll_suburb);
            ll_dropOffLoc = (LinearLayout) expandableItem.findViewById(R.id.ll_dropOffLoc);
            ll_dateTime = (LinearLayout) expandableItem.findViewById(R.id.ll_dateTime);
            ll_pickUpLoc = (LinearLayout) expandableItem.findViewById(R.id.ll_pickUpLoc);
            ll_TripDistance = (LinearLayout) expandableItem.findViewById(R.id.ll_TripDistance);
            ll_passengerNo = (LinearLayout) expandableItem.findViewById(R.id.ll_passengerNo);
            ll_carModel = (LinearLayout) expandableItem.findViewById(R.id.ll_carModel);
            ll_PaymentType = (LinearLayout) expandableItem.findViewById(R.id.ll_PaymentType);
            ll_passengerFlightNo = (LinearLayout) expandableItem.findViewById(R.id.ll_passengerFlightNo);
            ll_passengerNote = (LinearLayout) expandableItem.findViewById(R.id.ll_passengerNote);
            ll_dispatcherName = (LinearLayout) expandableItem.findViewById(R.id.ll_dispatcherName);
            ll_dispatcherEmail = (LinearLayout) expandableItem.findViewById(R.id.ll_dispatcherEmail);
            ll_dispatcherNo = (LinearLayout) expandableItem.findViewById(R.id.ll_dispatcherNo);
            ll_TripFare = (LinearLayout) expandableItem.findViewById(R.id.ll_TripFare);
            ll_passengerName = (LinearLayout) expandableItem.findViewById(R.id.ll_passengerName);
            ll_passengerEmail = (LinearLayout) expandableItem.findViewById(R.id.ll_passengerEmail);

            tv_PassengerName = (TextView) expandableItem.findViewById(R.id.tv_PassengerName);
            tv_suburb = (TextView) expandableItem.findViewById(R.id.tv_suburb);
            tv_drop_off_location = (TextView) expandableItem.findViewById(R.id.tv_drop_off_location);
            tv_date_time = (TextView) expandableItem.findViewById(R.id.tv_date_time);
            tv_pickupLocation = (TextView) expandableItem.findViewById(R.id.tv_pickupLocation);
            tv_TripDistance = (TextView) expandableItem.findViewById(R.id.tv_TripDistance);
            tv_passengerNo = (TextView) expandableItem.findViewById(R.id.tv_passengerNo);
            tv_carModel = (TextView) expandableItem.findViewById(R.id.tv_carModel);
            tv_carModel1 = (TextView) expandableItem.findViewById(R.id.tv_carModel1);
            tv_PaymentType = (TextView) expandableItem.findViewById(R.id.tv_PaymentType);
            tv_passengerFlightNo = (TextView) expandableItem.findViewById(R.id.tv_passengerFlightNo);
            tv_passengerNote = (TextView) expandableItem.findViewById(R.id.tv_passengerNote);
            tv_dispatcherName = (TextView) expandableItem.findViewById(R.id.tv_dispatcherName);
            tv_dispatcherEmail = (TextView) expandableItem.findViewById(R.id.tv_dispatcherEmail);
            tv_dispatcherNo = (TextView) expandableItem.findViewById(R.id.tv_dispatcherNo);
            tv_TripFare = (TextView) expandableItem.findViewById(R.id.tv_TripFare);
            tv_PassengerName1 = (TextView) expandableItem.findViewById(R.id.tv_PassengerName1);
            tv_PassengerEmail = (TextView) expandableItem.findViewById(R.id.tv_PassengerEmail);

            iv_status_active = (ImageView) expandableItem.findViewById(R.id.iv_status_active);

            tv_estimateEarning = expandableItem.findViewById(R.id.tv_estimateEarning);
            ll_driverEarning = expandableItem.findViewById(R.id.ll_driverEarning);
        }
    }


    @Override
    public Future_Job_List_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        this.context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.row_layout_future_job, parent, false);

        Future_Job_List_Adapter.ViewHolder vh = new Future_Job_List_Adapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final Future_Job_List_Adapter.ViewHolder holder, final int position)
    {
        super.onBindViewHolder(holder, position);
        POSITION = position;

        if (Future_JobBooking_List_Fragment.list.get(position).getPassengerName()!=null && !Future_JobBooking_List_Fragment.list.get(position).getPassengerName().equalsIgnoreCase(""))
        {
            holder.tv_PassengerName.setVisibility(View.GONE);
            holder.tv_PassengerName.setText(Future_JobBooking_List_Fragment.list.get(position).getPassengerName());
        }
        else
        {
            holder.tv_PassengerName.setVisibility(View.GONE);
        }
        holder.ll_suburb.setVisibility(View.GONE);

        /*--------------*/
        if (Future_JobBooking_List_Fragment.list.get(position).getEstDriverEarning()!=null && !Future_JobBooking_List_Fragment.list.get(position).getEstDriverEarning().equalsIgnoreCase(""))
        {
            holder.tv_estimateEarning.setVisibility(View.VISIBLE);
            holder.tv_estimateEarning.setText("$"+Future_JobBooking_List_Fragment.list.get(position).getEstDriverEarning());
        }
        else
        {
            holder.ll_driverEarning.setVisibility(View.GONE);
        }
        /*----------------*/
         /*if (Future_JobBooking_List_Fragment.list.get(position).getDropoffLocation()!=null && !Future_JobBooking_List_Fragment.list.get(position).getDropoffLocation().equalsIgnoreCase(""))
        {
            holder.tv_suburb.setText(Future_JobBooking_List_Fragment.list.get(position).getDropoffLocation());
        }
        else
        {
            holder.ll_suburb.setVisibility(View.GONE);
        }*/

        if (Future_JobBooking_List_Fragment.list.get(position).getDropoffLocation()!=null && !Future_JobBooking_List_Fragment.list.get(position).getDropoffLocation().equalsIgnoreCase(""))
        {
            holder.ll_dropOffLoc.setVisibility(View.VISIBLE);
            if(Future_JobBooking_List_Fragment.list.get(position).getDropoffSuburb()!=null && !Future_JobBooking_List_Fragment.list.get(position).getDropoffSuburb().equalsIgnoreCase(""))
            {holder.tv_drop_off_location.setText(Future_JobBooking_List_Fragment.list.get(position).getDropoffSuburb());}
            else
            {holder.tv_drop_off_location.setText(Future_JobBooking_List_Fragment.list.get(position).getDropoffLocation());}
        }
        else
        {
            holder.ll_dropOffLoc.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getPickupDateTime()!=null && !Future_JobBooking_List_Fragment.list.get(position).getPickupDateTime().equalsIgnoreCase(""))
        {
            holder.ll_dateTime.setVisibility(View.VISIBLE);
            holder.tv_date_time.setText(Utility.getDateInFormat1(Future_JobBooking_List_Fragment.list.get(position).getPickupDateTime()));
        }
        else
        {
            holder.ll_dateTime.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getPickupLocation()!=null && !Future_JobBooking_List_Fragment.list.get(position).getPickupLocation().equalsIgnoreCase(""))
        {
            holder.ll_pickUpLoc.setVisibility(View.VISIBLE);

            if(Future_JobBooking_List_Fragment.list.get(position).getPickupSuburb()!=null && !Future_JobBooking_List_Fragment.list.get(position).getPickupSuburb().equalsIgnoreCase(""))
            {holder.tv_pickupLocation.setText(Future_JobBooking_List_Fragment.list.get(position).getPickupSuburb());}
            else
            {holder.tv_pickupLocation.setText(Future_JobBooking_List_Fragment.list.get(position).getPickupLocation());}
        }
        else
        {
            holder.ll_pickUpLoc.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getTripDistance()!=null && !Future_JobBooking_List_Fragment.list.get(position).getTripDistance().equalsIgnoreCase(""))
        {
            holder.ll_TripDistance.setVisibility(View.VISIBLE);
            holder.tv_TripDistance.setText(Future_JobBooking_List_Fragment.list.get(position).getTripDistance());
        }
        else
        {
            holder.ll_TripDistance.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getPassengerContact()!=null && !Future_JobBooking_List_Fragment.list.get(position).getPassengerContact().equalsIgnoreCase(""))
        {
            holder.ll_passengerNo.setVisibility(View.VISIBLE);
            String PassengerMobNo = Future_JobBooking_List_Fragment.list.get(position).getPassengerContact();
            SpannableString content = new SpannableString(PassengerMobNo);
            content.setSpan(new UnderlineSpan(), 0, PassengerMobNo.length(), 0);
            holder.tv_passengerNo.setText(content);
        }
        else
        {
            holder.ll_passengerNo.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getPassengerName()!=null &&
                !Future_JobBooking_List_Fragment.list.get(position).getPassengerName().equalsIgnoreCase(""))
        {
            holder.ll_passengerName.setVisibility(View.VISIBLE);
            holder.tv_PassengerName1.setText(Future_JobBooking_List_Fragment.list.get(position).getPassengerName());
        }
        else
        {
            holder.ll_passengerName.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getModel()!=null && !Future_JobBooking_List_Fragment.list.get(position).getModel().equalsIgnoreCase(""))
        {
            //holder.ll_carModel.setVisibility(View.VISIBLE);
            holder.tv_carModel.setText(Future_JobBooking_List_Fragment.list.get(position).getModel());
            holder.tv_carModel1.setText(Future_JobBooking_List_Fragment.list.get(position).getModel());
        }
        else
        {
            holder.ll_carModel.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getPaymentType()!=null && !Future_JobBooking_List_Fragment.list.get(position).getPaymentType().equalsIgnoreCase(""))
        {
            holder.ll_PaymentType.setVisibility(View.VISIBLE);
            holder.tv_PaymentType.setText(Future_JobBooking_List_Fragment.list.get(position).getPaymentType());
        }
        else
        {
            holder.ll_PaymentType.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getFlightNumber()!=null && !Future_JobBooking_List_Fragment.list.get(position).getFlightNumber().equalsIgnoreCase(""))
        {
            holder.ll_passengerFlightNo.setVisibility(View.VISIBLE);
            holder.tv_passengerFlightNo.setText(Future_JobBooking_List_Fragment.list.get(position).getFlightNumber());
        }
        else
        {
            holder.ll_passengerFlightNo.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getNotes()!=null && !Future_JobBooking_List_Fragment.list.get(position).getNotes().equalsIgnoreCase(""))
        {
            holder.ll_passengerNote.setVisibility(View.VISIBLE);
            holder.tv_passengerNote.setText(Future_JobBooking_List_Fragment.list.get(position).getNotes());
        }
        else
        {
            holder.ll_passengerNote.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getDispatherFullname()!=null && !Future_JobBooking_List_Fragment.list.get(position).getDispatherFullname().equalsIgnoreCase(""))
        {
            holder.ll_dispatcherName.setVisibility(View.VISIBLE);
            holder.tv_dispatcherName.setText(Future_JobBooking_List_Fragment.list.get(position).getDispatherFullname());
        }
        else
        {
            holder.ll_dispatcherName.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getDispatherEmail()!=null && !Future_JobBooking_List_Fragment.list.get(position).getDispatherEmail().equalsIgnoreCase(""))
        {
            holder.ll_dispatcherEmail.setVisibility(View.VISIBLE);
            holder.tv_dispatcherEmail.setText(Future_JobBooking_List_Fragment.list.get(position).getDispatherEmail());
        }
        else
        {
            holder.ll_dispatcherEmail.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getDispatherMobileNo()!=null && !Future_JobBooking_List_Fragment.list.get(position).getDispatherMobileNo().equalsIgnoreCase(""))
        {
            holder.ll_dispatcherNo.setVisibility(View.VISIBLE);
            holder.tv_dispatcherNo.setText(Future_JobBooking_List_Fragment.list.get(position).getDispatherMobileNo());
        }
        else
        {
            holder.ll_dispatcherNo.setVisibility(View.GONE);
        }

        if (Future_JobBooking_List_Fragment.list.get(position).getTripFare()!=null && !Future_JobBooking_List_Fragment.list.get(position).getTripFare().equalsIgnoreCase(""))
        {
            holder.ll_TripFare.setVisibility(View.VISIBLE);
            holder.tv_TripFare.setText(Future_JobBooking_List_Fragment.list.get(position).getTripFare());
        }
        else
        {
            holder.ll_TripFare.setVisibility(View.GONE);
        }

        holder.iv_status_active.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userId = SessionSave.getUserSession(Comman.USER_ID, context);
                if (userId!=null && !userId.equalsIgnoreCase(""))
                {
                    AcceptDispatchJob_Request(userId, Future_JobBooking_List_Fragment.list.get(position).getTripId(), holder.iv_status_active, position);
                }
            }
        });

        holder.tv_passengerNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callToDriver(Future_JobBooking_List_Fragment.list.get(position).getPassengerContact());
            }
        });


    }



    public void callToDriver(String passengerContact)
    {
        TelephonyManager telMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        MyAlertDialog internetDialog = null;
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                internetDialog = new MyAlertDialog(context);
                internetDialog.showOKDialog("Sim card not available");
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                internetDialog = new MyAlertDialog(context);
                internetDialog.showOKDialog("Sim state network locked");
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                internetDialog = new MyAlertDialog(context);
                internetDialog.showOKDialog("Sim state pin required");
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                internetDialog = new MyAlertDialog(context);
                internetDialog.showOKDialog("Sim state puk required");
                break;
            case TelephonyManager.SIM_STATE_READY:
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + passengerContact));
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
                {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CALL_PHONE}, 1234);
                }
                else
                {
                    context.startActivity(intent);
                }
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                internetDialog = new MyAlertDialog(context);
                internetDialog.showOKDialog("Sim state unknown");
                break;
        }
    }


    public String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    @Override
    public int getItemCount() {
        return Future_JobBooking_List_Fragment.list.size();
    }


    private void AcceptDispatchJob_Request(String userId, String tripId, final ImageView iv_status_active, final int position)
    {
        aQuery = new AQuery(context);
        dialogClass = new DialogClass(context, 1);
        dialogClass.showDialog();
        iv_status_active.setVisibility(View.GONE);

        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_ACCEPT_FUTURE_JOB_REQUEST + userId+"/"+tripId;

        Log.e(TAG, "URL = " + url);
        Log.e(TAG, "PARAMS = " + params);

        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>(){

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG, "responseCode = " + responseCode);
                    Log.e(TAG, "Response = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");
                                Future_JobBooking_List_Fragment.list.remove(position);
                                notifyDataSetChanged();
                                SessionSave.saveUserSession(Comman.NOTIFICATION_COUNT_FUTURE_BOOKING,Future_JobBooking_List_Fragment.list.size()+"", Drawer_Activity.activity);
                                Drawer_Activity.activity.Set_FutureBookingCount();
                                dialogClass.hideDialog();

                                if (json.has("message"))
                                {
                                    new SnackbarUtils(Drawer_Activity.main_layout, json.getString("message"),
                                            ContextCompat.getColor(context, R.color.snakbar_color),
                                            ContextCompat.getColor(context, R.color.snackbar_text_color),
                                            ContextCompat.getColor(context, R.color.snackbar_text_color)).snackieBar().show();
                                }

                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        ((Drawer_Activity)context).setTitleOfScreenToolbar("Scheduled Bookings");

                                        Pending_Job_List_Fragment homeFragment = new Pending_Job_List_Fragment();
                                        FragmentTransaction fragmentTransaction = ((Drawer_Activity) context).getSupportFragmentManager().beginTransaction();
                                        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                                        fragmentTransaction.replace(R.id.frame_myjob, homeFragment);
                                        fragmentTransaction.commitAllowingStateLoss();
                                    }
                                }, 500);
                            }
                            else
                            {
                                Log.e("status", "false");
                                iv_status_active.setVisibility(View.VISIBLE);
                                dialogClass.hideDialog();
                                if (json.has("message"))
                                {
                                    new SnackbarUtils(Drawer_Activity.main_layout, json.getString("message"),
                                            ContextCompat.getColor(context, R.color.snakbar_color),
                                            ContextCompat.getColor(context, R.color.snackbar_text_color),
                                            ContextCompat.getColor(context, R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        }
                        else
                        {
                            iv_status_active.setVisibility(View.VISIBLE);
                            Log.e("status", "no status");
                            dialogClass.hideDialog();
                        }
                    }
                    else
                    {
                        iv_status_active.setVisibility(View.VISIBLE);
                        Log.e(TAG, "getMessage = " + "null");
                        dialogClass.hideDialog();
                    }
                }
                catch (Exception e)
                {
                    iv_status_active.setVisibility(View.VISIBLE);
                    Log.e(TAG, "Exception = " + e.getMessage() + "something_is_wrong");
                    dialogClass.hideDialog();
                }
            }
        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }
}
