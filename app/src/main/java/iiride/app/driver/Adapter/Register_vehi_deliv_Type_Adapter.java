package iiride.app.driver.Adapter;

import android.app.Activity;
import android.content.Context;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import iiride.app.driver.Been.Register_vehi_deliv_Type_Been;
import iiride.app.driver.Fragment.Register_VehicleType_Fragment;


import iiride.app.driver.R;
import iiride.app.driver.View.SnackbarUtils;

import java.util.ArrayList;


public class Register_vehi_deliv_Type_Adapter extends BaseAdapter {

    private Activity activity;
    private static LayoutInflater inflater=null;
    ArrayList<Register_vehi_deliv_Type_Been> list_vehicleItem;
    LinearLayout ll_dialog;
    int MINIMUM_CHECK_BOX_SELECTED=3;
    String clickedType;

    public Register_vehi_deliv_Type_Adapter(Activity a, ArrayList<Register_vehi_deliv_Type_Been> list_vehicleItem, LinearLayout ll_dialog, String clickedType) {
        activity = a;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list_vehicleItem = list_vehicleItem;
        this.ll_dialog =  ll_dialog;
        this.clickedType = clickedType;
    }

    public int getCount() {
        return list_vehicleItem.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent)
    {
        View vi=convertView;
        LinearLayout row_vehicleType_item, ll_vehiInfo, ll_deliInfo;
        TextView tv_vehicle_type, tv_vehicle_description;
        final CheckBox ckBox_vehi_type;
        RelativeLayout rl_vehicle_type;

        if(convertView==null)
        {
            vi = inflater.inflate(R.layout.listview_row_item_vehicle_type, null);
        }

        row_vehicleType_item = (LinearLayout) vi.findViewById(R.id.row_vehicleType_item);
        ll_vehiInfo = (LinearLayout) vi.findViewById(R.id.ll_vehiInfo);
        ll_deliInfo = (LinearLayout) vi.findViewById(R.id.ll_deliInfo);

        tv_vehicle_type = (TextView)vi.findViewById(R.id.tv_vehicle_type);
        tv_vehicle_description = (TextView)vi.findViewById(R.id.tv_vehicle_description);

        ckBox_vehi_type = (CheckBox)vi.findViewById(R.id.ckBox_vehi_type);
        ckBox_vehi_type.setClickable(false);

        rl_vehicle_type = (RelativeLayout)vi.findViewById(R.id.rl_vehicle_type);

        Log.e("clickedType","clickedTypeclickedType :"+clickedType);

        if (position==0)
        {
            if (clickedType != null && !clickedType.equalsIgnoreCase(""))
            {
                if (clickedType.equalsIgnoreCase(Register_VehicleType_Fragment.VEHICLE_TYPE))
                {
                    ll_vehiInfo.setVisibility(View.VISIBLE);
                    ll_deliInfo.setVisibility(View.GONE);
                }
                else
                {
                    ll_vehiInfo.setVisibility(View.GONE);
                    ll_deliInfo.setVisibility(View.VISIBLE);
                }
            }
            else
            {
                ll_vehiInfo.setVisibility(View.GONE);
                ll_deliInfo.setVisibility(View.GONE);
            }
        }
        else
        {
            ll_vehiInfo.setVisibility(View.GONE);
            ll_deliInfo.setVisibility(View.GONE);
        }


        if (list_vehicleItem.get(position).getBicycleName()!=null && !list_vehicleItem.get(position).getBicycleName().equalsIgnoreCase(""))
        {
            row_vehicleType_item.setVisibility(View.VISIBLE);
            tv_vehicle_type.setText(list_vehicleItem.get(position).getBicycleName());
        }
        else
        {
            row_vehicleType_item.setVisibility(View.GONE);
        }

        if (list_vehicleItem.get(position).getDescription()!=null && !list_vehicleItem.get(position).getDescription().equalsIgnoreCase(""))
        {
            tv_vehicle_description.setVisibility(View.VISIBLE);
            tv_vehicle_description.setText(list_vehicleItem.get(position).getDescription());
        }
        else
        {
            tv_vehicle_description.setVisibility(View.GONE);
        }


        if (list_vehicleItem.get(position).getStatus().equalsIgnoreCase("1"))
        {
            ckBox_vehi_type.setChecked(true);
        }
        else
        {
            ckBox_vehi_type.setChecked(false);
        }


        rl_vehicle_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int CheckedNumber = 0;
                for (int i=0; i<list_vehicleItem.size() ; i++)
                {
                    if (list_vehicleItem.get(i).getStatus().equalsIgnoreCase("1"))
                    {
                        CheckedNumber++;
                    }
                }

                if (ckBox_vehi_type.isChecked())
                {
                    ckBox_vehi_type.setChecked(false);
                    list_vehicleItem.get(position).setStatus("0");
                }
                else
                {
                    if (CheckedNumber<MINIMUM_CHECK_BOX_SELECTED)
                    {
                        ckBox_vehi_type.setChecked(true);
                        list_vehicleItem.get(position).setStatus("1");
                    }
                    else
                    {
                        new SnackbarUtils(ll_dialog, activity.getResources().getString(R.string.you_can_select_only_three_type),
                                ContextCompat.getColor(activity, R.color.snakbar_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color),
                                ContextCompat.getColor(activity, R.color.snackbar_text_color)).snackieBar().show();
                    }
                }
            }
        });


        return vi;
    }
}