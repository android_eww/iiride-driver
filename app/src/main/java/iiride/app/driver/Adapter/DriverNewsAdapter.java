package iiride.app.driver.Adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import iiride.app.driver.Activity.Driver_News_Detail_Activity;
import iiride.app.driver.Been.DriverNewsBeen;
import iiride.app.driver.R;


import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Admin on 12/6/2017.
 */

public class DriverNewsAdapter extends RecyclerView.Adapter<DriverNewsAdapter.MyViewHolder> {

    private String TAG = DriverNewsAdapter.class.getSimpleName();
    private List<DriverNewsBeen> newsList;
    private Context context;

    /**
     * View holder class
     * */
    public class MyViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout ll_driver_news;
        private TextView tv_news_title,tv_news_description;
        private ImageView iv_news_image;


        public MyViewHolder(View view) {
            super(view);
            ll_driver_news = (LinearLayout) view.findViewById(R.id.ll_driver_news);
            tv_news_title = (TextView) view.findViewById(R.id.tv_news_title);
            iv_news_image = (ImageView) view.findViewById(R.id.iv_news_image);
            tv_news_description = (TextView) view.findViewById(R.id.tv_news_description);
        }
    }

    public DriverNewsAdapter(List<DriverNewsBeen> newsList, Context context) {
        this.newsList = newsList;
        this.context = context;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final DriverNewsBeen driverNewsBeen = newsList.get(position);

        holder.tv_news_title.setText(driverNewsBeen.getTitle());
        holder.tv_news_description.setText(driverNewsBeen.getDescription());

        Picasso.with(context)
                .load(driverNewsBeen.getUrlToImage())
                .into(holder.iv_news_image);

        holder.ll_driver_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e(TAG, "NewsUrl:- " + driverNewsBeen.getUrl());
                Intent intentNews = new Intent(context, Driver_News_Detail_Activity.class);
                intentNews.putExtra("newsTitle",driverNewsBeen.getTitle());
                intentNews.putExtra("newsUrl",driverNewsBeen.getUrl());
                context.startActivity(intentNews);

            }
        });
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.driver_news_item,parent, false);
        return new MyViewHolder(v);
    }
}
