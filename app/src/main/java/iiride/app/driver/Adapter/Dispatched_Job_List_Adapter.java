package iiride.app.driver.Adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.telephony.TelephonyManager;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import iiride.app.driver.Activity.Drawer_Activity;
import iiride.app.driver.Been.Dispatched_JobList_Been;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;
import iiride.app.driver.Fragment.Dispatched_Job_List_Fragment;


import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.MyAlertDialog;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.hendraanggrian.widget.ExpandableItem;
import com.hendraanggrian.widget.ExpandableRecyclerView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


public class Dispatched_Job_List_Adapter extends ExpandableRecyclerView.Adapter<Dispatched_Job_List_Adapter.ViewHolder> {
    Context context;
    int POSITION;
    String TAG = "AcceptDispatchJob_Req";
    private AQuery aQuery;
    DialogClass dialogClass;


    public Dispatched_Job_List_Adapter(LinearLayoutManager lm) {
        super(lm);
    }


    public class ViewHolder extends ExpandableRecyclerView.ViewHolder {
        public View layout;
//        ImageView iv_status_active, iv_status_inactive;

        LinearLayout ll_row_item_pastJob, ll_pickUpLoc, ll_passengerNo, ll_tripDistance, ll_carModel, ll_suburb, ll_dropOffLoc, ll_PassengerEmail, ll_TripFare, ll_Tax, ll_SubTotal, ll_GrandTotal
                , ll_PaymentType, ll_date_time, ll_Status, ll_passengerFlightNo, ll_passengerNote, ll_driverName, ll_driverEmail, ll_driverMobNo;

        public TextView tv_drop_off_location, tv_pickupLocation, tv_date_time, tv_PassengerName, tv_passengerNo, tv_tripDistance, tv_carModel, tv_suburb, tv_PassengerEmail, tv_TripFare
                , tv_Tax, tv_SubTotal, tv_GrandTotal, tv_PaymentType, tv_Status, tv_passengerFlightNo, tv_passengerNote, tv_driverName, tv_driverEmail, tv_driverMobNo, tv_CancelRequest;

        ExpandableItem expandableItem;


        public ViewHolder(View v) {
            super(v);
            layout = v;
            expandableItem = (ExpandableItem) v.findViewById(R.id.row);
            tv_drop_off_location = (TextView) expandableItem.findViewById(R.id.tv_drop_off_location);
            tv_pickupLocation = (TextView) expandableItem.findViewById(R.id.tv_pickupLocation);
            tv_suburb = (TextView) expandableItem.findViewById(R.id.tv_suburb);
            tv_date_time = (TextView) expandableItem.findViewById(R.id.tv_date_time);
            tv_PassengerName = (TextView) expandableItem.findViewById(R.id.tv_PassengerName);
            tv_passengerNo = (TextView) expandableItem.findViewById(R.id.tv_passengerNo);
            tv_tripDistance = (TextView) expandableItem.findViewById(R.id.tv_tripDistance);
            tv_carModel = (TextView) expandableItem.findViewById(R.id.tv_carModel);

            tv_PassengerEmail = (TextView) expandableItem.findViewById(R.id.tv_PassengerEmail);
            tv_TripFare = (TextView) expandableItem.findViewById(R.id.tv_TripFare);
            tv_Tax = (TextView) expandableItem.findViewById(R.id.tv_Tax);
            tv_SubTotal = (TextView) expandableItem.findViewById(R.id.tv_SubTotal);
            tv_GrandTotal = (TextView) expandableItem.findViewById(R.id.tv_GrandTotal);
            tv_PaymentType = (TextView) expandableItem.findViewById(R.id.tv_PaymentType);
            tv_Status = (TextView) expandableItem.findViewById(R.id.tv_Status);
            tv_passengerFlightNo = (TextView) expandableItem.findViewById(R.id.tv_passengerFlightNo);
            tv_passengerNote = (TextView) expandableItem.findViewById(R.id.tv_passengerNote);
            tv_driverName = (TextView) expandableItem.findViewById(R.id.tv_driverName);
            tv_driverEmail = (TextView) expandableItem.findViewById(R.id.tv_driverEmail);
            tv_driverMobNo = (TextView) expandableItem.findViewById(R.id.tv_driverMobNo);
            tv_CancelRequest = (TextView) expandableItem.findViewById(R.id.tv_CancelRequest);

            /*iv_status_active = (ImageView) expandableItem.findViewById(R.id.iv_status_active);
            iv_status_inactive = (ImageView) expandableItem.findViewById(R.id.iv_status_inactive);*/

            ll_row_item_pastJob = (LinearLayout) expandableItem.findViewById(R.id.ll_row_item_pastJob);

            ll_pickUpLoc = (LinearLayout) expandableItem.findViewById(R.id.ll_pickUpLoc);
            ll_passengerNo = (LinearLayout) expandableItem.findViewById(R.id.ll_passengerNo);
            ll_tripDistance = (LinearLayout) expandableItem.findViewById(R.id.ll_tripDistance);
            ll_carModel = (LinearLayout) expandableItem.findViewById(R.id.ll_carModel);
            ll_suburb = (LinearLayout) expandableItem.findViewById(R.id.ll_suburb);
            ll_dropOffLoc = (LinearLayout) expandableItem.findViewById(R.id.ll_dropOffLoc);

            ll_PassengerEmail = (LinearLayout) expandableItem.findViewById(R.id.ll_PassengerEmail);
            ll_TripFare = (LinearLayout) expandableItem.findViewById(R.id.ll_TripFare);
            ll_Tax = (LinearLayout) expandableItem.findViewById(R.id.ll_Tax);
            ll_SubTotal = (LinearLayout) expandableItem.findViewById(R.id.ll_SubTotal);
            ll_GrandTotal = (LinearLayout) expandableItem.findViewById(R.id.ll_GrandTotal);
            ll_PaymentType = (LinearLayout) expandableItem.findViewById(R.id.ll_PaymentType);
            ll_date_time = (LinearLayout) expandableItem.findViewById(R.id.ll_date_time);
            ll_Status = (LinearLayout) expandableItem.findViewById(R.id.ll_Status);
            ll_passengerFlightNo = (LinearLayout) expandableItem.findViewById(R.id.ll_passengerFlightNo);
            ll_passengerNote = (LinearLayout) expandableItem.findViewById(R.id.ll_passengerNote);
            ll_driverName = (LinearLayout) expandableItem.findViewById(R.id.ll_driverName);
            ll_driverEmail = (LinearLayout) expandableItem.findViewById(R.id.ll_driverEmail);
            ll_driverMobNo = (LinearLayout) expandableItem.findViewById(R.id.ll_driverMobNo);
        }
    }

    @Override
    public int getItemCount() {
        return Dispatched_Job_List_Fragment.list.size();
    }


    @Override
    public Dispatched_Job_List_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.row_layout_dispatched_job, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);
        POSITION = position;

        /*holder.iv_status_active.setVisibility(View.GONE);
        holder.iv_status_inactive.setVisibility(View.GONE);*/

        if (Dispatched_Job_List_Fragment.list.get(position).getDropoffLocation() != null && !Dispatched_Job_List_Fragment.list.get(position).getDropoffLocation().equalsIgnoreCase("")) {
            holder.ll_dropOffLoc.setVisibility(View.VISIBLE);
            holder.tv_drop_off_location.setText(Dispatched_Job_List_Fragment.list.get(position).getDropoffLocation());
        } else {
            holder.ll_dropOffLoc.setVisibility(View.GONE);
        }

        holder.ll_suburb.setVisibility(View.GONE);
        if (Dispatched_Job_List_Fragment.list.get(position).getStatus() != null && !Dispatched_Job_List_Fragment.list.get(position).getStatus().equalsIgnoreCase("")) {
            holder.tv_Status.setText(Dispatched_Job_List_Fragment.list.get(position).getStatus());
        } else {
            holder.ll_Status.setVisibility(View.GONE);
        }

        if (Dispatched_Job_List_Fragment.list.get(position).getPickupLocation() != null && !Dispatched_Job_List_Fragment.list.get(position).getPickupLocation().equalsIgnoreCase("")) {
            holder.ll_pickUpLoc.setVisibility(View.VISIBLE);
            holder.tv_pickupLocation.setText(Dispatched_Job_List_Fragment.list.get(position).getPickupLocation());
        } else {
            holder.ll_pickUpLoc.setVisibility(View.GONE);
        }

        if (Dispatched_Job_List_Fragment.list.get(position).getPickupDateTime() != null && !Dispatched_Job_List_Fragment.list.get(position).getPickupDateTime().equalsIgnoreCase("")) {
            holder.ll_date_time.setVisibility(View.VISIBLE);
            String pickUpDateTime = getDate(Long.parseLong(Dispatched_Job_List_Fragment.list.get(position).getPickupDateTime() + "000"), "HH:mm  dd/MM/yyyy");
            Log.e("pickUpDateTime", "pickUpDateTime : " + pickUpDateTime);
            holder.tv_date_time.setText(pickUpDateTime);
        } else {
            holder.ll_date_time.setVisibility(View.GONE);
        }

        if (Dispatched_Job_List_Fragment.list.get(position).getPassengerName() != null && !Dispatched_Job_List_Fragment.list.get(position).getPassengerName().equalsIgnoreCase("")) {
            holder.tv_PassengerName.setVisibility(View.VISIBLE);
            holder.tv_PassengerName.setText(Dispatched_Job_List_Fragment.list.get(position).getPassengerName());
        } else {
            holder.tv_PassengerName.setVisibility(View.GONE);
        }

        if (Dispatched_Job_List_Fragment.list.get(position).getPassengerContact() != null && !Dispatched_Job_List_Fragment.list.get(position).getPassengerContact().equalsIgnoreCase("")) {
            holder.ll_passengerNo.setVisibility(View.VISIBLE);

            String PassengerMobNo = Dispatched_Job_List_Fragment.list.get(position).getPassengerContact();
            SpannableString content = new SpannableString(PassengerMobNo);
            content.setSpan(new UnderlineSpan(), 0, PassengerMobNo.length(), 0);
            holder.tv_passengerNo.setText(content);

        } else {
            holder.ll_passengerNo.setVisibility(View.GONE);
        }

        if (Dispatched_Job_List_Fragment.list.get(position).getTripDistance() != null && !Dispatched_Job_List_Fragment.list.get(position).getTripDistance().equalsIgnoreCase("")) {
            holder.ll_tripDistance.setVisibility(View.VISIBLE);
            holder.tv_tripDistance.setText(Dispatched_Job_List_Fragment.list.get(position).getTripDistance());
        } else {
            holder.ll_tripDistance.setVisibility(View.GONE);
        }

        if (Dispatched_Job_List_Fragment.list.get(position).getModel() != null && !Dispatched_Job_List_Fragment.list.get(position).getModel().equalsIgnoreCase("")) {
            holder.ll_carModel.setVisibility(View.VISIBLE);
            holder.tv_carModel.setText(Dispatched_Job_List_Fragment.list.get(position).getModel());
        } else {
            holder.ll_carModel.setVisibility(View.GONE);
        }

        if (Dispatched_Job_List_Fragment.list.get(position).getPassengerEmail() != null && !Dispatched_Job_List_Fragment.list.get(position).getPassengerEmail().equalsIgnoreCase("")) {
            holder.ll_PassengerEmail.setVisibility(View.VISIBLE);
            holder.tv_PassengerEmail.setText(Dispatched_Job_List_Fragment.list.get(position).getPassengerEmail());
        } else {
            holder.ll_PassengerEmail.setVisibility(View.GONE);
        }

        if (Dispatched_Job_List_Fragment.list.get(position).getTripFare() != null && !Dispatched_Job_List_Fragment.list.get(position).getTripFare().equalsIgnoreCase("")) {
            holder.ll_TripFare.setVisibility(View.VISIBLE);
            holder.tv_TripFare.setText(Dispatched_Job_List_Fragment.list.get(position).getTripFare());
        } else {
            holder.ll_TripFare.setVisibility(View.GONE);
        }

        if (Dispatched_Job_List_Fragment.list.get(position).getTax() != null && !Dispatched_Job_List_Fragment.list.get(position).getTax().equalsIgnoreCase("")) {
            holder.tv_Tax.setVisibility(View.VISIBLE);
            holder.tv_Tax.setText(Dispatched_Job_List_Fragment.list.get(position).getTax());
        } else {
            holder.tv_Tax.setVisibility(View.GONE);
        }

        if (Dispatched_Job_List_Fragment.list.get(position).getSubTotal() != null && !Dispatched_Job_List_Fragment.list.get(position).getSubTotal().equalsIgnoreCase("")) {
            holder.tv_SubTotal.setVisibility(View.VISIBLE);
            holder.tv_SubTotal.setText(Dispatched_Job_List_Fragment.list.get(position).getSubTotal());
        } else {
            holder.tv_SubTotal.setVisibility(View.GONE);
        }

        if (Dispatched_Job_List_Fragment.list.get(position).getGrandTotal() != null && !Dispatched_Job_List_Fragment.list.get(position).getGrandTotal().equalsIgnoreCase("")) {
            holder.ll_GrandTotal.setVisibility(View.VISIBLE);
            holder.tv_GrandTotal.setText(Dispatched_Job_List_Fragment.list.get(position).getGrandTotal());
        } else {
            holder.ll_GrandTotal.setVisibility(View.GONE);
        }
        if (Dispatched_Job_List_Fragment.list.get(position).getPaymentType() != null && !Dispatched_Job_List_Fragment.list.get(position).getPaymentType().equalsIgnoreCase("")) {
            holder.ll_PaymentType.setVisibility(View.VISIBLE);
            holder.tv_PaymentType.setText(Dispatched_Job_List_Fragment.list.get(position).getPaymentType());
        } else {
            holder.ll_PaymentType.setVisibility(View.GONE);
        }

        if (Dispatched_Job_List_Fragment.list.get(position).getFlightNumber() != null && !Dispatched_Job_List_Fragment.list.get(position).getFlightNumber().equalsIgnoreCase("")) {
            holder.ll_passengerFlightNo.setVisibility(View.VISIBLE);
            holder.tv_passengerFlightNo.setText(Dispatched_Job_List_Fragment.list.get(position).getFlightNumber());
        } else {
            holder.ll_passengerFlightNo.setVisibility(View.GONE);
        }

        if (Dispatched_Job_List_Fragment.list.get(position).getNotes() != null && !Dispatched_Job_List_Fragment.list.get(position).getNotes().equalsIgnoreCase("")) {
            holder.ll_passengerNote.setVisibility(View.VISIBLE);
            holder.tv_passengerNote.setText(Dispatched_Job_List_Fragment.list.get(position).getNotes());
        } else {
            holder.ll_passengerNote.setVisibility(View.GONE);
        }

        if (Dispatched_Job_List_Fragment.list.get(position).getDispatcherFullname() != null && !Dispatched_Job_List_Fragment.list.get(position).getDispatcherFullname().equalsIgnoreCase("")) {
            holder.ll_driverName.setVisibility(View.VISIBLE);
            holder.tv_driverName.setText(Dispatched_Job_List_Fragment.list.get(position).getDispatcherFullname());
        } else {
            holder.ll_driverName.setVisibility(View.GONE);
        }

        if (Dispatched_Job_List_Fragment.list.get(position).getDispatcherEmail() != null && !Dispatched_Job_List_Fragment.list.get(position).getDispatcherEmail().equalsIgnoreCase("")) {
            holder.ll_driverEmail.setVisibility(View.VISIBLE);
            holder.tv_driverEmail.setText(Dispatched_Job_List_Fragment.list.get(position).getDispatcherEmail());
        } else {
            holder.ll_driverEmail.setVisibility(View.GONE);
        }

        if (Dispatched_Job_List_Fragment.list.get(position).getDispatcherMobileNo() != null && !Dispatched_Job_List_Fragment.list.get(position).getDispatcherMobileNo().equalsIgnoreCase("")) {
            holder.ll_driverMobNo.setVisibility(View.VISIBLE);
            holder.tv_driverMobNo.setText(Dispatched_Job_List_Fragment.list.get(position).getDispatcherMobileNo());
        } else {
            holder.ll_driverMobNo.setVisibility(View.GONE);
        }

        holder.tv_passengerNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callToDriver(Dispatched_Job_List_Fragment.list.get(position).getPassengerContact());
            }
        });

        if (Dispatched_Job_List_Fragment.list.get(position).getStatus().equalsIgnoreCase("pending") || Dispatched_Job_List_Fragment.list.get(position).getStatus().equalsIgnoreCase("accepted"))
        {
            holder.tv_CancelRequest.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.tv_CancelRequest.setVisibility(View.GONE);
        }

        holder.tv_CancelRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Dispatched_Job_List_Fragment.list.get(position).getStatus()!=null && !Dispatched_Job_List_Fragment.list.get(position).getStatus().equalsIgnoreCase(""))
                {
                    if (Dispatched_Job_List_Fragment.list.get(position).getStatus().equalsIgnoreCase("pending") || Dispatched_Job_List_Fragment.list.get(position).getStatus().equalsIgnoreCase("accepted"))
                    {
                        String driverId = SessionSave.getUserSession(Comman.USER_ID, context);
                        if (driverId!=null && !driverId.equalsIgnoreCase("")) {
                            OnCancelDispatchJob_Request(position, Dispatched_Job_List_Fragment.list.get(position).getDispatchJob_Id()
                                    , Dispatched_Job_List_Fragment.list.get(position).getBookingType(), driverId, holder.tv_Status,  holder.tv_CancelRequest);
                        }
                    }
                    else
                    {
                        if (Dispatched_Job_List_Fragment.list.get(position).getStatus().equalsIgnoreCase("traveling"))
                        {
                            new SnackbarUtils(Drawer_Activity.main_layout, "You can't cancel Trip.It is already started",
                                    ContextCompat.getColor(context, R.color.snakbar_color),
                                    ContextCompat.getColor(context, R.color.snackbar_text_color),
                                    ContextCompat.getColor(context, R.color.snackbar_text_color)).snackieBar().show();
                        }
                        if (Dispatched_Job_List_Fragment.list.get(position).getStatus().equalsIgnoreCase("completed"))
                        {
                            new SnackbarUtils(Drawer_Activity.main_layout, "You can't cancel Trip.It is already completed",
                                    ContextCompat.getColor(context, R.color.snakbar_color),
                                    ContextCompat.getColor(context, R.color.snackbar_text_color),
                                    ContextCompat.getColor(context, R.color.snackbar_text_color)).snackieBar().show();
                        }
                    }
                }
            }
        });
    }


    public void callToDriver(String passengerContact)
    {
        TelephonyManager telMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        MyAlertDialog internetDialog = null;
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                internetDialog = new MyAlertDialog(context);
                internetDialog.showOKDialog("Sim card not available");
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                internetDialog = new MyAlertDialog(context);
                internetDialog.showOKDialog("Sim state network locked");
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                internetDialog = new MyAlertDialog(context);
                internetDialog.showOKDialog("Sim state pin required");
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                internetDialog = new MyAlertDialog(context);
                internetDialog.showOKDialog("Sim state puk required");
                break;
            case TelephonyManager.SIM_STATE_READY:
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + passengerContact));
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
                {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CALL_PHONE}, 1234);
                }
                else
                {
                    context.startActivity(intent);
                }
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                internetDialog = new MyAlertDialog(context);
                internetDialog.showOKDialog("Sim state unknown");
                break;
        }
    }

    public String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    private void OnCancelDispatchJob_Request(final int position1, String dispatchJob_id, final String BookingType, final String driverId, final TextView tv_Status, final TextView tv_CancelRequest)
    {
        dialogClass = new DialogClass(context, 1);
        String url = WebServiceAPI.WEB_SERVICE_CANCEL_DISPATCH_JOB ;
        Map<String, Object> params = new HashMap<String, Object>();

        params.put(WebServiceAPI.CANCEL_DISPATCH_JOB_DRIVER_ID, driverId);
        params.put(WebServiceAPI.CANCEL_DISPATCH_JOB_BOOKING_ID, dispatchJob_id);
        params.put(WebServiceAPI.CANCEL_DISPATCH_JOB_BOOKING_TYPE, BookingType);

        Log.e(TAG, "URL = " + url);
        Log.e(TAG, "params = " + params);
        dialogClass.showDialog();
        aQuery = new AQuery(context);

        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>(){

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG, "responseCode = " + responseCode);
                    Log.e(TAG, "Response = " + json);

                    if (json != null)
                    {
                        if (json.getBoolean("status"))
                        {
                            dialogClass.hideDialog();
                           /* Dispatched_Job_List_Fragment.list.set(position1, new Dispatched_JobList_Been(Dispatched_Job_List_Fragment.list.get(position1).getDispatchJob_Id()
                                    ,Dispatched_Job_List_Fragment.list.get(position1).getPickupDateTime(),Dispatched_Job_List_Fragment.list.get(position1).getPickupLocation()
                                    ,Dispatched_Job_List_Fragment.list.get(position1).getDropoffLocation(),Dispatched_Job_List_Fragment.list.get(position1).getCompanyId()
                                    , Dispatched_Job_List_Fragment.list.get(position1).getPassengerId(),Dispatched_Job_List_Fragment.list.get(position1).getTripDistance()
                                    ,Dispatched_Job_List_Fragment.list.get(position1).getModelId(),Dispatched_Job_List_Fragment.list.get(position1).getPassengerName()
                                    ,Dispatched_Job_List_Fragment.list.get(position1).getPassengerContact(),Dispatched_Job_List_Fragment.list.get(position1).getModel()
                                    ,Dispatched_Job_List_Fragment.list.get(position1).getTripFare(),Dispatched_Job_List_Fragment.list.get(position1).getTax()
                                    ,Dispatched_Job_List_Fragment.list.get(position1).getSubTotal(),Dispatched_Job_List_Fragment.list.get(position1).getGrandTotal()
                                    ,Dispatched_Job_List_Fragment.list.get(position1).getPaymentType(),Dispatched_Job_List_Fragment.list.get(position1).getPassengerEmail()
                                    ,Dispatched_Job_List_Fragment.list.get(position1).getCreatedDate(),Dispatched_Job_List_Fragment.list.get(position1).getStatus()
                                    ,Dispatched_Job_List_Fragment.list.get(position1).getFlightNumber(),Dispatched_Job_List_Fragment.list.get(position1).getNotes()
                                    ,Dispatched_Job_List_Fragment.list.get(position1).getDispatcherEmail(),Dispatched_Job_List_Fragment.list.get(position1).getDispatcherFullname()
                                    ,Dispatched_Job_List_Fragment.list.get(position1).getDispatcherMobileNo(),"canceled"));
                            notifyDataSetChanged(position1);*/
                            CallDispatched_List(driverId);
                            if (json.has("message"))
                            {
                                new SnackbarUtils(Drawer_Activity.main_layout, json.getString("message"),
                                        ContextCompat.getColor(context, R.color.snakbar_color),
                                        ContextCompat.getColor(context, R.color.snackbar_text_color),
                                        ContextCompat.getColor(context, R.color.snackbar_text_color)).snackieBar().show();
                            }
                            else
                            {
                                new SnackbarUtils(Drawer_Activity.main_layout, "Delete dispatch job successfully.",
                                        ContextCompat.getColor(context, R.color.snakbar_color),
                                        ContextCompat.getColor(context, R.color.snackbar_text_color),
                                        ContextCompat.getColor(context, R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }
                        else
                        {
                            dialogClass.hideDialog();
                            new SnackbarUtils(Drawer_Activity.main_layout, context.getString(R.string.something_is_wrong),
                                    ContextCompat.getColor(context, R.color.snakbar_color),
                                    ContextCompat.getColor(context, R.color.snackbar_text_color),
                                    ContextCompat.getColor(context, R.color.snackbar_text_color)).snackieBar().show();
                        }
                    }
                    else
                    {
                        Log.e(TAG, "getMessage = " + "null");
                        dialogClass.hideDialog();
                        new SnackbarUtils(Drawer_Activity.main_layout, context.getString(R.string.something_is_wrong),
                                ContextCompat.getColor(context, R.color.snakbar_color),
                                ContextCompat.getColor(context, R.color.snackbar_text_color),
                                ContextCompat.getColor(context, R.color.snackbar_text_color)).snackieBar().show();
                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG, "Exception = " + e.getMessage() + "something_is_wrong");
                    dialogClass.hideDialog();
                    new SnackbarUtils(Drawer_Activity.main_layout, context.getString(R.string.something_is_wrong),
                            ContextCompat.getColor(context, R.color.snakbar_color),
                            ContextCompat.getColor(context, R.color.snackbar_text_color),
                            ContextCompat.getColor(context, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }

    public void CallDispatched_List(String userId)
    {

        Map<String, Object> params = new HashMap<String, Object>();

        String url = WebServiceAPI.WEB_SERVICE_MY_DISPATCH_JOB_LIST + userId;

        Log.e(TAG, "URL = " + url);
        Log.e(TAG, "PARAMS = " + params);
        Dispatched_Job_List_Fragment.list.clear();

        aQuery.ajax(url, null, JSONObject.class, new AjaxCallback<JSONObject>(){

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG, "responseCode = " + responseCode);
                    Log.e(TAG, "Response = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if (json.getBoolean("status"))
                            {
                                Log.e("status", "true");

                                if (json.has("history"))
                                {
                                    String history = json.getString("history");
                                    if (history!=null && !history.equalsIgnoreCase(""))
                                    {
                                        JSONArray historyArray = json.getJSONArray("history");
                                        for (int h=0; h<historyArray.length(); h++)
                                        {
                                            String DispatchJob_Id="", PickupDateTime="", PickupLocation="", DropoffLocation="", CompanyId="", PassengerId="", TripDistance=""
                                                    , ModelId="", PassengerName="", PassengerContact="", Model="",TripFare="", Tax="", SubTotal="", GrandTotal="", PaymentType=""
                                                    , PassengerEmail="", CreatedDate="", Status="", FlightNumber="", Notes="", DispatcherDriverInfo="", dispatcherEmail=""
                                                    , dispatcherFullname="", dispatcherMobileNo="", BookingType="";

                                            JSONObject historyObject = historyArray.getJSONObject(h);

                                            if (historyObject.has("Id"))
                                            {
                                                DispatchJob_Id = historyObject.getString("Id");
                                            }
                                            if (historyObject.has("PickupTime"))
                                            {
                                                PickupDateTime = historyObject.getString("PickupTime");
                                            }
                                            if (historyObject.has("CreatedDate"))
                                            {
                                                CreatedDate = historyObject.getString("CreatedDate");
                                            }
                                            if (historyObject.has("PickupLocation"))
                                            {
                                                PickupLocation = historyObject.getString("PickupLocation");
                                            }
                                            if (historyObject.has("DropoffLocation"))
                                            {
                                                DropoffLocation = historyObject.getString("DropoffLocation");
                                            }
                                            if (historyObject.has("CompanyId"))
                                            {
                                                CompanyId = historyObject.getString("CompanyId");
                                            }
                                            if (historyObject.has("BookingType"))
                                            {
                                                BookingType = historyObject.getString("BookingType");
                                            }


                                            if (historyObject.has("PassengerId"))
                                            {
                                                PassengerId = historyObject.getString("PassengerId");
                                            }
                                            if (historyObject.has("TripDistance"))
                                            {
                                                TripDistance = historyObject.getString("TripDistance");
                                            }
                                            if (historyObject.has("ModelId"))
                                            {
                                                ModelId = historyObject.getString("ModelId");
                                            }
                                            if (historyObject.has("PassengerName"))
                                            {
                                                PassengerName = historyObject.getString("PassengerName");
                                            }
                                            if (historyObject.has("PassengerContact"))
                                            {
                                                PassengerContact = historyObject.getString("PassengerContact");
                                            }
                                            if (historyObject.has("Model"))
                                            {
                                                Model = historyObject.getString("Model");
                                            }

                                            if (historyObject.has("TripFare"))
                                            {
                                                TripFare = historyObject.getString("TripFare");
                                            }
                                            if (historyObject.has("Tax"))
                                            {
                                                Tax = historyObject.getString("Tax");
                                            }
                                            if (historyObject.has("SubTotal"))
                                            {
                                                SubTotal = historyObject.getString("SubTotal");
                                            }
                                            if (historyObject.has("GrandTotal"))
                                            {
                                                GrandTotal = historyObject.getString("GrandTotal");
                                            }
                                            if (historyObject.has("PassengerEmail"))
                                            {
                                                PassengerEmail = historyObject.getString("PassengerEmail");
                                            }

                                            if (historyObject.has("Status"))
                                            {
                                                Status = historyObject.getString("Status");
                                            }
                                            if (historyObject.has("PaymentType"))
                                            {
                                                PaymentType = historyObject.getString("PaymentType");
                                            }
                                            if (historyObject.has("FlightNumber")) {
                                                FlightNumber = historyObject.getString("FlightNumber");
                                            }
                                            if (historyObject.has("Notes")) {
                                                Notes = historyObject.getString("Notes");
                                            }

                                            if (historyObject.has("DispatcherDriverInfo"))
                                            {
                                                DispatcherDriverInfo = historyObject.getString("DispatcherDriverInfo");
                                                if (DispatcherDriverInfo!=null && !DispatcherDriverInfo.equalsIgnoreCase(""))
                                                {
                                                    JSONObject DispatcherDriverInfoObj = historyObject.getJSONObject("DispatcherDriverInfo");
                                                    if (DispatcherDriverInfoObj!=null)
                                                    {
                                                        if (DispatcherDriverInfoObj.has("Email"))
                                                        {
                                                            dispatcherEmail = DispatcherDriverInfoObj.getString("Email");
                                                        }
                                                        if (DispatcherDriverInfoObj.has("Fullname"))
                                                        {
                                                            dispatcherFullname = DispatcherDriverInfoObj.getString("Fullname");
                                                        }
                                                        if (DispatcherDriverInfoObj.has("MobileNo"))
                                                        {
                                                            dispatcherMobileNo = DispatcherDriverInfoObj.getString("MobileNo");
                                                        }
                                                    }
                                                }
                                            }

                                            Dispatched_Job_List_Fragment.list.add(new Dispatched_JobList_Been(DispatchJob_Id, PickupDateTime, PickupLocation, DropoffLocation, CompanyId, PassengerId, TripDistance
                                                    , ModelId, PassengerName, PassengerContact, Model, TripFare, Tax, SubTotal, GrandTotal, PaymentType, PassengerEmail, CreatedDate
                                                    , Status, FlightNumber, Notes, dispatcherEmail, dispatcherFullname, dispatcherMobileNo, BookingType));
                                        }
                                        notifyDataSetChanged();
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                }
            }
        }.method(AQuery.METHOD_GET).header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }
}
