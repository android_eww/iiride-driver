package iiride.app.driver.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import iiride.app.driver.Activity.Add_Card_In_List_Activity;
import iiride.app.driver.Activity.Wallet_Balance_TransferToBank_Activity;
import iiride.app.driver.Activity.Wallet_Cards_Activity;
import iiride.app.driver.Activity.Wallet_Balance_TopUp_Activity;
import iiride.app.driver.Been.CreditCard_List_Been;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;
import iiride.app.driver.Others.ConnectivityReceiver;


import iiride.app.driver.R;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class CreditCardList_Adapter extends RecyclerView.Adapter<CreditCardList_Adapter.MyViewHolder> {

    private Context mContext;
    private List<CreditCard_List_Been> list;
    public static String ClickecardNumber = "";
    public static String ClickcardId = "", TAG="CreditCardList_Adapter";
    public static List<CreditCard_List_Been> cardList = new ArrayList<>();


    private AQuery aQuery;
    DialogClass dialogClass;
    LinearLayout main_layout;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_cardType, tv_cardNo;
        public ImageView iv_creditCard, iv_delete;
        LinearLayout ll_row_card, ll_addCard;

        public MyViewHolder(View view) {
            super(view);

            tv_cardType = (TextView) view.findViewById(R.id.tv_cardType);
            tv_cardNo = (TextView) view.findViewById(R.id.tv_cardNo);
            iv_creditCard = (ImageView) view.findViewById(R.id.iv_creditCard);
            iv_delete = (ImageView) view.findViewById(R.id.iv_delete);
            ll_row_card = (LinearLayout) view.findViewById(R.id.ll_row_card);
            ll_addCard = (LinearLayout) view.findViewById(R.id.ll_addCard);

            aQuery = new AQuery(mContext);
            dialogClass = new DialogClass(mContext, 1);
        }
    }

    public CreditCardList_Adapter(Context mContext, List<CreditCard_List_Been> list, LinearLayout main_layout) {
        this.mContext = mContext;
        this.list = list;
        this.main_layout=main_layout;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_credit_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        holder.tv_cardType.setText( list.get(position).getCardName());
        holder.tv_cardNo.setText( list.get(position).getCardNo());

        if (position==list.size()-1)
        {
            holder.ll_addCard.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.ll_addCard.setVisibility(View.GONE);
        }

        if (position%2==0)
        {
//            holder.iv_creditCard.setImageResource(R.drawable.icon_creditcard_maestro);
            holder.ll_row_card.setBackgroundColor(ContextCompat.getColor(mContext,R.color.darkorange));
        }
        else
        {
//            holder.iv_creditCard.setImageResource(R.drawable.icon_creditcard_visa);
            holder.ll_row_card.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorcardVisa));
        }
        if (list.get(position).getCardType()!=null && !list.get(position).getCardType().equalsIgnoreCase(""))
        {
            if (list.get(position).getCardType().equalsIgnoreCase("visa"))
            {
                holder.iv_creditCard.setImageResource(R.drawable.icon_creditcard_visa);
            }
            else if (list.get(position).getCardType().equalsIgnoreCase("discover"))
            {
                holder.iv_creditCard.setImageResource(R.drawable.card_discover);
            }
            else if (list.get(position).getCardType().equalsIgnoreCase("amex"))
            {
                holder.iv_creditCard.setImageResource(R.drawable.card_american);
            }
            else if (list.get(position).getCardType().equalsIgnoreCase("diners"))
            {
                holder.iv_creditCard.setImageResource(R.drawable.card_dinner);
            }
            else if (list.get(position).getCardType().equalsIgnoreCase("jcb"))
            {
                holder.iv_creditCard.setImageResource(R.drawable.card_jcbs);
            }
            else if (list.get(position).getCardType().equalsIgnoreCase("mastercard"))
            {
                holder.iv_creditCard.setImageResource(R.drawable.card_master);
            }
            else if (list.get(position).getCardType().equalsIgnoreCase("maestro"))
            {
                holder.iv_creditCard.setImageResource(R.drawable.card_maestro);
            }
            else
            {
                holder.iv_creditCard.setImageResource(R.drawable.card_dummy);
            }
        }
        else
        {
            holder.iv_creditCard.setImageResource(R.drawable.card_dummy);
        }

        holder.ll_addCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Wallet_Cards_Activity.resumeFlag=0;
                Intent i =  new Intent(mContext, Add_Card_In_List_Activity.class);
                i.putExtra("from","");
                mContext.startActivity(i);
                ((Wallet_Cards_Activity)mContext).overridePendingTransition(R.anim.right_in, R.anim.left_out);
            }
        });

        holder.ll_row_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Wallet_Cards_Activity.from!=null && !Wallet_Cards_Activity.from.equalsIgnoreCase(""))
                {
                    if (list.get(position).getCardNo()!=null && !list.get(position).getCardNo().equalsIgnoreCase(""))
                    {
                        if (Wallet_Cards_Activity.from.equalsIgnoreCase("TopUp"))
                        {
                            Wallet_Balance_TopUp_Activity.flagResumeUse = 1;
                            ClickecardNumber = list.get(position).getCardNo();
                            ClickcardId = list.get(position).getId();
                            ((Activity)mContext).finish();
                        }
                        else
                        {
                            Wallet_Balance_TransferToBank_Activity.flagResumeUse = 1;
                            ClickecardNumber = list.get(position).getCardNo();
                            ClickcardId = list.get(position).getId();
                            ((Activity)mContext).finish();
                        }
                    }
                }
            }
        });


        holder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected())
                {
                    if (list.get(position).getId()!=null && !list.get(position).getId().equalsIgnoreCase(""))
                    {
                        String driverId = SessionSave.getUserSession(Comman.USER_ID, mContext);
                        deleteCard(driverId, list.get(position).getId(), position);
                    }
                }
                else
                {
                    new SnackbarUtils(main_layout, mContext.getString(R.string.not_connected_to_internet),
                            ContextCompat.getColor(mContext, R.color.snakbar_color),
                            ContextCompat.getColor(mContext, R.color.snackbar_text_color),
                            ContextCompat.getColor(mContext, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        });

    }


    public void deleteCard(String driverId, String CardId, final int Pos)
    {
        dialogClass.showDialog();
        String url = WebServiceAPI.WEB_SERVICE_REMOVE_CARD + driverId + "/" + CardId;
        cardList.clear();
        Log.e(TAG, "URL = " + url);

        aQuery.ajax(url, null , JSONObject.class , new AjaxCallback<JSONObject>()
        {
            @Override
            public void callback(String url, JSONObject json, AjaxStatus status)
            {

                try
                {
                    int responseCode = status.getCode();
                    Log.e(TAG, "responseCode = " + responseCode);
                    Log.e(TAG, "Response = " + json);

                    if (json != null)
                    {
                        if (json.has("status"))
                        {
                            if(json.getBoolean("status"))
                            {
                                Log.e(TAG,"Status = " + "true");
                                dialogClass.hideDialog();
                                list.remove(Pos);
                                if (json.has("cards"))
                                {
                                    String cards = json.getString("cards");
                                    if (cards!=null && !cards.equalsIgnoreCase(""))
                                    {
                                        JSONArray cardsArray = json.getJSONArray("cards");
                                        if (cardsArray!=null && cardsArray.length()>0)
                                        {
                                            for (int i=0; i<cardsArray.length(); i++)
                                            {
                                                String Id = "", CardNum="", CardNum2="", Type="", Alias="";

                                                JSONObject cardObj = cardsArray.getJSONObject(i);
                                                if (cardObj.has("Id"))
                                                {
                                                    Id = cardObj.getString("Id");
                                                }
                                                if (cardObj.has("CardNum"))
                                                {
                                                    CardNum = cardObj.getString("CardNum");
                                                }
                                                if (cardObj.has("CardNum2"))
                                                {
                                                    CardNum2 = cardObj.getString("CardNum2");
                                                }
                                                if (cardObj.has("Type"))
                                                {
                                                    Type = cardObj.getString("Type");
                                                }
                                                if (cardObj.has("Alias"))
                                                {
                                                    Alias = cardObj.getString("Alias");
                                                }
                                                Log.e("GetCardList","Id : "+Id+"\nCardNum : "+CardNum+"\nCardNum2 : "+CardNum2+"\nType : "+Type+"\nAlias : "+Alias);
                                                cardList.add(new CreditCard_List_Been(Id, Alias, CardNum2, Type));
                                            }
                                            SessionSave.saveUserSession(Comman.DRIVER_CARD_COUNT_ADDED_IN_LIST, cardList.size()+"", mContext);
                                        }
                                        else
                                        {
                                            Log.e("cardsArray", "null");
                                            SessionSave.saveUserSession(Comman.DRIVER_CARD_COUNT_ADDED_IN_LIST, "0", mContext);
                                            dialogClass.hideDialog();
                                            if (json.has("message")) {
                                                new SnackbarUtils(main_layout, json.getString("message"),
                                                        ContextCompat.getColor(mContext, R.color.snakbar_color),
                                                        ContextCompat.getColor(mContext, R.color.snackbar_text_color),
                                                        ContextCompat.getColor(mContext, R.color.snackbar_text_color)).snackieBar().show();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Log.e("cards", "null");
                                        dialogClass.hideDialog();
                                        SessionSave.saveUserSession(Comman.DRIVER_CARD_COUNT_ADDED_IN_LIST, "0", mContext);
                                        if (json.has("message")) {
                                            new SnackbarUtils(main_layout, json.getString("message"),
                                                    ContextCompat.getColor(mContext, R.color.snakbar_color),
                                                    ContextCompat.getColor(mContext, R.color.snackbar_text_color),
                                                    ContextCompat.getColor(mContext, R.color.snackbar_text_color)).snackieBar().show();
                                        }
                                    }
                                }
                                else
                                {
                                    Log.e("cards", "noy found");
                                    dialogClass.hideDialog();
                                    SessionSave.saveUserSession(Comman.DRIVER_CARD_COUNT_ADDED_IN_LIST, "0", mContext);
                                    if (json.has("message")) {
                                        new SnackbarUtils(main_layout, json.getString("message"),
                                                ContextCompat.getColor(mContext, R.color.snakbar_color),
                                                ContextCompat.getColor(mContext, R.color.snackbar_text_color),
                                                ContextCompat.getColor(mContext, R.color.snackbar_text_color)).snackieBar().show();
                                    }
                                }
                                notifyDataSetChanged();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(mContext, R.color.snakbar_color),
                                            ContextCompat.getColor(mContext, R.color.snackbar_text_color),
                                            ContextCompat.getColor(mContext, R.color.snackbar_text_color)).snackieBar().show();
                                }


                            }
                            else
                            {
                                Log.e("status", "false");
                                dialogClass.hideDialog();
                                if (json.has("message")) {
                                    new SnackbarUtils(main_layout, json.getString("message"),
                                            ContextCompat.getColor(mContext, R.color.snakbar_color),
                                            ContextCompat.getColor(mContext, R.color.snackbar_text_color),
                                            ContextCompat.getColor(mContext, R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }

                        }
                        else
                        {
                            Log.e("json", "no status found");
                            dialogClass.hideDialog();
                            if (json.has("message")) {
                                new SnackbarUtils(main_layout,mContext.getResources().getString(R.string.something_is_wrong),
                                        ContextCompat.getColor(mContext, R.color.snakbar_color),
                                        ContextCompat.getColor(mContext, R.color.snackbar_text_color),
                                        ContextCompat.getColor(mContext, R.color.snackbar_text_color)).snackieBar().show();
                            }

                        }

                    }
                    else
                    {
                        Log.e(TAG, "getMessage = "  + "Null");
                        dialogClass.hideDialog();
                        new SnackbarUtils(main_layout,mContext.getResources().getString(R.string.something_is_wrong),
                                ContextCompat.getColor(mContext, R.color.snakbar_color),
                                ContextCompat.getColor(mContext, R.color.snackbar_text_color),
                                ContextCompat.getColor(mContext, R.color.snackbar_text_color)).snackieBar().show();
                    }

                }
                catch (Exception e)
                {
                    Log.e(TAG, "Exception = " + e.getMessage() + "somthing_is_wrong");
                    dialogClass.hideDialog();
                    new SnackbarUtils(main_layout,mContext.getResources().getString(R.string.something_is_wrong),
                            ContextCompat.getColor(mContext, R.color.snakbar_color),
                            ContextCompat.getColor(mContext, R.color.snackbar_text_color),
                            ContextCompat.getColor(mContext, R.color.snackbar_text_color)).snackieBar().show();

                }

            }
        }.header(WebServiceAPI.HEADER_KEY_NAME,WebServiceAPI.HEADER_KEY_VALUE));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}