package iiride.app.driver.Adapter;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.CalendarContract;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.telephony.TelephonyManager;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import iiride.app.driver.Activity.Drawer_Activity;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.Comman.WebServiceAPI;
import iiride.app.driver.Fragment.Pending_Job_List_Fragment;


import iiride.app.driver.R;
import iiride.app.driver.Util.Utility;
import iiride.app.driver.View.DialogClass;
import iiride.app.driver.View.MyAlertDialog;
import iiride.app.driver.View.SnackbarUtils;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.hendraanggrian.widget.ExpandableItem;
import com.hendraanggrian.widget.ExpandableRecyclerView;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class Pending_Job_List_Adapter  extends ExpandableRecyclerView.Adapter<Pending_Job_List_Adapter.ViewHolder> {
    Context context;
    int POSITION;
    String TAG = "AcceptDispatchJob_Req";
    private AQuery aQuery;
    DialogClass dialogClass;


    public Pending_Job_List_Adapter(LinearLayoutManager lm) {
        super(lm);
    }


    public class ViewHolder extends ExpandableRecyclerView.ViewHolder {
        public View layout;

        LinearLayout ll_row_item_pastJob, ll_pickUpLoc, ll_passengerNo, ll_tripDistance, ll_carModel, ll_suburb, ll_dropOffLoc, ll_dateTime, ll_pickupTime,
                ll_passengerName, ll_passengerEmail, ll_PaymentType, ll_passengerFlightNo, ll_passengerNote ,ll_dispatcherName, ll_dispatcherEmail,
                ll_dispatcherNo, ll_TripFare;

        public TextView tv_drop_off_location, tv_pickupLocation, tv_date_time, tv_PassengerName, tv_PassengerName1, tv_passengerNo, tv_tripDistance, tv_carModel,
                tv_suburb,tv_pickupTime, tv_passengerEmail, tv_startTrip, tv_PaymentType, tv_passengerFlightNo, tv_passengerNote ,tv_dispatcherName,
                tv_dispatcherEmail, tv_dispatcherNo, tv_TripFare,tvAddCalender,tvCancelRequest;

        ExpandableItem expandableItem;


        public ViewHolder(View v) {
            super(v);
            layout = v;
            expandableItem = (ExpandableItem) v. findViewById(R.id.row);
            tv_drop_off_location = (TextView) expandableItem.findViewById(R.id.tv_drop_off_location);
            tv_pickupLocation = (TextView) expandableItem.findViewById(R.id.tv_pickupLocation);
            tv_suburb = (TextView) expandableItem.findViewById(R.id.tv_suburb);
            tv_date_time = (TextView) expandableItem.findViewById(R.id.tv_date_time);
            tv_PassengerName = (TextView) expandableItem.findViewById(R.id.tv_PassengerName);
            tv_PassengerName1 = (TextView) expandableItem.findViewById(R.id.tv_PassengerName1);
            tv_passengerNo = (TextView) expandableItem.findViewById(R.id.tv_passengerNo);
            tv_tripDistance = (TextView) expandableItem.findViewById(R.id.tv_tripDistance);
            tv_carModel = (TextView) expandableItem.findViewById(R.id.tv_carModel);
            tv_pickupTime = (TextView) expandableItem.findViewById(R.id.tv_pickupTime);
            tv_passengerEmail = (TextView) expandableItem.findViewById(R.id.tv_passengerEmail);
            tv_startTrip = (TextView) expandableItem.findViewById(R.id.tv_startTrip);
            tv_PaymentType = (TextView) expandableItem.findViewById(R.id.tv_PaymentType);
            tv_passengerFlightNo = (TextView) expandableItem.findViewById(R.id.tv_passengerFlightNo);
            tv_passengerNote = (TextView) expandableItem.findViewById(R.id.tv_passengerNote);
            tv_dispatcherName = (TextView) expandableItem.findViewById(R.id.tv_dispatcherName);
            tv_dispatcherEmail = (TextView) expandableItem.findViewById(R.id.tv_dispatcherEmail);
            tv_dispatcherNo = (TextView) expandableItem.findViewById(R.id.tv_dispatcherNo);
            tv_TripFare = (TextView) expandableItem.findViewById(R.id.tv_TripFare);

            ll_row_item_pastJob = (LinearLayout) expandableItem.findViewById(R.id.ll_row_item_pastJob);
            ll_pickUpLoc = (LinearLayout) expandableItem.findViewById(R.id.ll_pickUpLoc);
            ll_passengerNo = (LinearLayout) expandableItem.findViewById(R.id.ll_passengerNo);
            ll_tripDistance = (LinearLayout) expandableItem.findViewById(R.id.ll_tripDistance);
            ll_carModel = (LinearLayout) expandableItem.findViewById(R.id.ll_carModel);
            ll_suburb = (LinearLayout) expandableItem.findViewById(R.id.ll_suburb);
            ll_dropOffLoc = (LinearLayout) expandableItem.findViewById(R.id.ll_dropOffLoc);
            ll_dateTime = (LinearLayout) expandableItem.findViewById(R.id.ll_dateTime);
            ll_pickupTime = (LinearLayout) expandableItem.findViewById(R.id.ll_pickupTime);
            ll_passengerEmail = (LinearLayout) expandableItem.findViewById(R.id.ll_passengerEmail);
            ll_passengerName = (LinearLayout) expandableItem.findViewById(R.id.ll_passengerName);
            ll_PaymentType = (LinearLayout) expandableItem.findViewById(R.id.ll_PaymentType);
            ll_passengerFlightNo = (LinearLayout) expandableItem.findViewById(R.id.ll_passengerFlightNo);
            ll_passengerNote = (LinearLayout) expandableItem.findViewById(R.id.ll_passengerNote);
            ll_dispatcherName = (LinearLayout) expandableItem.findViewById(R.id.ll_dispatcherName);
            ll_dispatcherEmail = (LinearLayout) expandableItem.findViewById(R.id.ll_dispatcherEmail);
            ll_dispatcherNo = (LinearLayout) expandableItem.findViewById(R.id.ll_dispatcherNo);
            ll_TripFare = (LinearLayout) expandableItem.findViewById(R.id.ll_TripFare);

            tvAddCalender = expandableItem.findViewById(R.id.tvAddCalender);
            tvCancelRequest = expandableItem.findViewById(R.id.tvCancelRequest);
        }
    }


    @Override
    public Pending_Job_List_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        this.context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.row_layout_pending_job, parent, false);

        Pending_Job_List_Adapter.ViewHolder vh = new Pending_Job_List_Adapter.ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(final Pending_Job_List_Adapter.ViewHolder holder, final int position)
    {
        super.onBindViewHolder(holder, position);
        POSITION = position;

        if (Pending_Job_List_Fragment.list.get(position).getDropoffLocation()!=null && !Pending_Job_List_Fragment.list.get(position).getDropoffLocation().equalsIgnoreCase(""))
        {
            holder.ll_dropOffLoc.setVisibility(View.VISIBLE);
            holder.tv_drop_off_location.setText(Pending_Job_List_Fragment.list.get(position).getDropoffLocation());
        }
        else
        {
            holder.ll_dropOffLoc.setVisibility(View.GONE);
        }

        holder.ll_suburb.setVisibility(View.GONE);

        if (Pending_Job_List_Fragment.list.get(position).getPickupLocation()!=null && !Pending_Job_List_Fragment.list.get(position).getPickupLocation().equalsIgnoreCase(""))
        {
            holder.ll_pickUpLoc.setVisibility(View.VISIBLE);
            holder.tv_pickupLocation.setText(Pending_Job_List_Fragment.list.get(position).getPickupLocation());
        }
        else
        {
            holder.ll_pickUpLoc.setVisibility(View.GONE);
        }

        Log.e("PickupTime","222222222 : "+Pending_Job_List_Fragment.list.get(position).getPickupDateTime());
        if (Pending_Job_List_Fragment.list.get(position).getPickupDateTime()!=null && !Pending_Job_List_Fragment.list.get(position).getPickupDateTime().equalsIgnoreCase("") && !Pending_Job_List_Fragment.list.get(position).getPickupDateTime().equalsIgnoreCase("false"))
        {
            holder.ll_dateTime.setVisibility(View.VISIBLE);
            holder.tv_date_time.setText(Utility.getDateInFormat(Pending_Job_List_Fragment.list.get(position).getPickupDateTime()));
        }
        else
        {
            holder.ll_dateTime.setVisibility(View.GONE);
        }

        /*if (Pending_Job_List_Fragment.list.get(position).getPassengerName()!=null && !Pending_Job_List_Fragment.list.get(position).getPassengerName().equalsIgnoreCase(""))
        {
            holder.tv_PassengerName.setVisibility(View.VISIBLE);
            holder.tv_PassengerName.setText(Pending_Job_List_Fragment.list.get(position).getPassengerName());
        }
        else
        {
            holder.tv_PassengerName.setVisibility(View.GONE);
        }*/

        if (Pending_Job_List_Fragment.list.get(position).getTripFare()!=null && !Pending_Job_List_Fragment.list.get(position).getTripFare().equalsIgnoreCase(""))
        {
            holder.ll_TripFare.setVisibility(View.VISIBLE);
            holder.tv_TripFare.setText(Pending_Job_List_Fragment.list.get(position).getTripFare());
        }
        else
        {
            holder.ll_TripFare.setVisibility(View.GONE);
        }

        if (Pending_Job_List_Fragment.list.get(position).getPassengerMobileNo()!=null &&
                !Pending_Job_List_Fragment.list.get(position).getPassengerMobileNo().equalsIgnoreCase(""))
        {
            holder.ll_passengerNo.setVisibility(View.VISIBLE);
            String PassengerMobNo = Pending_Job_List_Fragment.list.get(position).getPassengerMobileNo();
            SpannableString content = new SpannableString(PassengerMobNo);
            content.setSpan(new UnderlineSpan(), 0, PassengerMobNo.length(), 0);
            holder.tv_passengerNo.setText(content);
        }
        else
        {
            holder.ll_passengerNo.setVisibility(View.GONE);
        }

        if (Pending_Job_List_Fragment.list.get(position).getTripDistance()!=null && !Pending_Job_List_Fragment.list.get(position).getTripDistance().equalsIgnoreCase(""))
        {
            holder.ll_tripDistance.setVisibility(View.VISIBLE);
            holder.tv_tripDistance.setText(Pending_Job_List_Fragment.list.get(position).getTripDistance());
        }
        else
        {
            holder.ll_tripDistance.setVisibility(View.GONE);
        }

        if (Pending_Job_List_Fragment.list.get(position).getModel()!=null && !Pending_Job_List_Fragment.list.get(position).getModel().equalsIgnoreCase(""))
        {
            holder.ll_carModel.setVisibility(View.VISIBLE);
            holder.tv_carModel.setText(Pending_Job_List_Fragment.list.get(position).getModel());
        }
        else
        {
            holder.ll_carModel.setVisibility(View.GONE);
        }

        if (Pending_Job_List_Fragment.list.get(position).getPassengerName()!=null &&
                !Pending_Job_List_Fragment.list.get(position).getPassengerName().equalsIgnoreCase(""))
        {
            holder.ll_passengerName.setVisibility(View.VISIBLE);
            holder.tv_PassengerName1.setText(Pending_Job_List_Fragment.list.get(position).getPassengerName());
        }
        else
        {
            holder.ll_passengerName.setVisibility(View.GONE);
        }

        if (Pending_Job_List_Fragment.list.get(position).getPassengerEmail()!=null && !Pending_Job_List_Fragment.list.get(position).getPassengerEmail().equalsIgnoreCase(""))
        {
            holder.ll_passengerEmail.setVisibility(View.VISIBLE);
            holder.tv_passengerEmail.setText(Pending_Job_List_Fragment.list.get(position).getPassengerEmail());
        }
        else
        {
            holder.ll_passengerEmail.setVisibility(View.GONE);
        }

        if (Pending_Job_List_Fragment.list.get(position).getPaymentType()!=null && !Pending_Job_List_Fragment.list.get(position).getPaymentType().equalsIgnoreCase(""))
        {
            holder.ll_PaymentType.setVisibility(View.VISIBLE);
            holder.tv_PaymentType.setText(Pending_Job_List_Fragment.list.get(position).getPaymentType());
        }
        else
        {
            holder.ll_PaymentType.setVisibility(View.GONE);
        }

        if (Pending_Job_List_Fragment.list.get(position).getFlightNumber()!=null && !Pending_Job_List_Fragment.list.get(position).getFlightNumber().equalsIgnoreCase(""))
        {
            holder.ll_passengerFlightNo.setVisibility(View.VISIBLE);
            holder.tv_passengerFlightNo.setText(Pending_Job_List_Fragment.list.get(position).getFlightNumber());
        }
        else
        {
            holder.ll_passengerFlightNo.setVisibility(View.GONE);
        }

        if (Pending_Job_List_Fragment.list.get(position).getNotes()!=null && !Pending_Job_List_Fragment.list.get(position).getNotes().equalsIgnoreCase(""))
        {
            holder.ll_passengerNote.setVisibility(View.VISIBLE);
            holder.tv_passengerNote.setText(Pending_Job_List_Fragment.list.get(position).getNotes());
        }
        else
        {
            holder.ll_passengerNote.setVisibility(View.GONE);
        }

        if (Pending_Job_List_Fragment.list.get(position).getDispatherFullname()!=null && !Pending_Job_List_Fragment.list.get(position).getDispatherFullname().equalsIgnoreCase(""))
        {
            holder.ll_dispatcherName.setVisibility(View.VISIBLE);
            holder.tv_dispatcherName.setText(Pending_Job_List_Fragment.list.get(position).getDispatherFullname());
        }
        else
        {
            holder.ll_dispatcherName.setVisibility(View.GONE);
        }

        if (Pending_Job_List_Fragment.list.get(position).getDispatherEmail()!=null && !Pending_Job_List_Fragment.list.get(position).getDispatherEmail().equalsIgnoreCase(""))
        {
            holder.ll_dispatcherEmail.setVisibility(View.VISIBLE);
            holder.tv_dispatcherEmail.setText(Pending_Job_List_Fragment.list.get(position).getDispatherEmail());
        }
        else
        {
            holder.ll_dispatcherEmail.setVisibility(View.GONE);
        }

        if (Pending_Job_List_Fragment.list.get(position).getDispatherMobileNo()!=null && !Pending_Job_List_Fragment.list.get(position).getDispatherMobileNo().equalsIgnoreCase(""))
        {
            holder.ll_dispatcherNo.setVisibility(View.VISIBLE);
            holder.tv_dispatcherNo.setText(Pending_Job_List_Fragment.list.get(position).getDispatherMobileNo());
        }
        else
        {
            holder.ll_dispatcherNo.setVisibility(View.GONE);
        }

        holder.tv_passengerNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              callToDriver(Pending_Job_List_Fragment.list.get(position).getPassengerMobileNo());
            }
        });

        holder.tvCancelRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String bookingId = Pending_Job_List_Fragment.list.get(position).getId();
                showYesNoDialog("Are you sure you want to cancel your trip?",0,bookingId,position);
            }
        });

        if (Pending_Job_List_Fragment.list.get(position).getStatus()!=null && Pending_Job_List_Fragment.list.get(position).getStatus().equalsIgnoreCase("travelling"))
        {
            holder.tv_startTrip.setVisibility(View.GONE);
        }
        else
        {
            holder.tv_startTrip.setVisibility(View.VISIBLE);
        }

        holder.tv_startTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String TripOnOff = SessionSave.getUserSession(Comman.ACCEPT_REQUEST_FLAG, context);
                if (TripOnOff!=null && !TripOnOff.equalsIgnoreCase("") && !TripOnOff.equalsIgnoreCase("1"))
                {
                    String DutyStatus = SessionSave.getUserSession(Comman.USER_DRIVER_DUTY, context);
                    if (DutyStatus!=null && !DutyStatus.equalsIgnoreCase("") && !DutyStatus.equalsIgnoreCase("0"))
                    {
                        String bookingId = Pending_Job_List_Fragment.list.get(position).getId();
                        if (bookingId!=null && !bookingId.equalsIgnoreCase(""))
                        {
                            showYesNoDialog("Are you sure you want to Go for pickup passenger?",1,bookingId,position);
                        }
                    }
                    else
                    {
                        if (Drawer_Activity.main_layout!=null)
                        {
                            new SnackbarUtils(Drawer_Activity.main_layout, context.getResources().getString(R.string.get_online_first),
                                    ContextCompat.getColor(context, R.color.snakbar_color),
                                    ContextCompat.getColor(context, R.color.snackbar_text_color),
                                    ContextCompat.getColor(context, R.color.snackbar_text_color)).snackieBar().show();
                        }
                    }
                }
                else
                {
                    new SnackbarUtils(Drawer_Activity.main_layout, "You are already on Your trip, first finished your trip",
                            ContextCompat.getColor(context, R.color.snakbar_color),
                            ContextCompat.getColor(context, R.color.snackbar_text_color),
                            ContextCompat.getColor(context, R.color.snackbar_text_color)).snackieBar().show();
                }
            }
        });

        holder.tvAddCalender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("TAG","CALENDER");

                if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                    // Permission is not granted

                    new AlertDialog.Builder(context)
                            .setTitle("Require Calender Permission")
                            .setMessage("Please allow calender permission from setting.")
                            .setPositiveButton("Setting", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", context.getPackageName(), null);
                                    intent.setData(uri);
                                    context.startActivity(intent);
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .show();
                }
                else
                {
                    showYesNoDialog("Are you sure you want to add this trip into your calender?",2,"",position);
                }
            }
        });
    }

    public void showYesNoDialog(String message, final int flag, final String bookingId, final int position)
    {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.confirmation_dialog);

        TextView tv_yes = (TextView) dialog.findViewById(R.id.yes);
        TextView tv_No = (TextView) dialog.findViewById(R.id.no);
        TextView tv_Message = (TextView) dialog.findViewById(R.id.message);
        LinearLayout ll_Yes = (LinearLayout) dialog.findViewById(R.id.yes_layout);
        LinearLayout ll_No = (LinearLayout) dialog.findViewById(R.id.no_layout);

        tv_Message.setText(message);

        ll_Yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (flag == 1) {
                    SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "1", context);
                    ((Drawer_Activity)context).NotifyPassengerForAdvancedTrip(bookingId);
                } else if (flag == 0) {
                    NotifyPassengerForTripCancel(bookingId, position);
                } else if (flag == 2){
                    addEventInCalender(Pending_Job_List_Fragment.list.get(position).getPickupLocation(),
                            Pending_Job_List_Fragment.list.get(position).getDropoffLocation(),
                            Pending_Job_List_Fragment.list.get(position).getPickupDateTime());
                }
            }
        });

        tv_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (flag == 1) {
                    SessionSave.saveUserSession(Comman.BOOKING_REQUEST_FOR, "1", context);
                    ((Drawer_Activity)context).NotifyPassengerForAdvancedTrip(bookingId);
                } else if (flag == 0) {
                    NotifyPassengerForTripCancel(bookingId, position);
                } else if (flag == 2){
                    addEventInCalender(Pending_Job_List_Fragment.list.get(position).getPickupLocation(),
                            Pending_Job_List_Fragment.list.get(position).getDropoffLocation(),
                            Pending_Job_List_Fragment.list.get(position).getPickupDateTime());
                }
            }
        });

        ll_No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tv_No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        //lp.width = Comman.DEVICE_WIDTH;
        lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
        lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    private void addEventInCalender(String pickup,String dropoff, String pickupTime)
    {
        //03:45 PM 05/03/2020
        try {
            SimpleDateFormat input = new SimpleDateFormat("hh:mm a dd/MM/yyyy");
            Date dateObj = input.parse(pickupTime);
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd/MM/yyyy");
            pickupTime = sdf.format(dateObj);
        }
        catch (Exception e)
        {

        }

        String[] x = pickupTime.split(" ");
        String time = x[0];
        String date = x[1];

        int hour = 0 ,minute = 0;
        if(time != null && !time.equalsIgnoreCase(""))
        {
            String[] p = time.split(":");
            hour = Integer.parseInt(p[0]);
            minute = Integer.parseInt(p[1]);
        }


        int day = 0 ,month = 0 ,year = 0;
        if(date != null && !date.equalsIgnoreCase(""))
        {
            String[] p = date.split("/");
            day = Integer.parseInt(p[0]);
            month = Integer.parseInt(p[1]);
            year = Integer.parseInt(p[2]);

            month -= 1;
        }


        long calID = 3;
        long startMillis = 0;
        long endMillis = 0;
        Calendar beginTime = Calendar.getInstance();
        beginTime.set(year, month, day, hour-1, minute);
        startMillis = beginTime.getTimeInMillis();

        Calendar endTime = Calendar.getInstance();
        endTime.set(year, month, day, hour, minute);
        endMillis = endTime.getTimeInMillis();

//        ContentResolver cr = context.getContentResolver();
//        ContentValues values = new ContentValues();
//        values.put(CalendarContract.Events.DTSTART, startMillis);
//        values.put(CalendarContract.Events.DTEND, endMillis);
//        values.put(CalendarContract.Events.TITLE, "Upcoming trip at \nPickup Location: \n"
//                +pickup+
//                "\nDropoff Location: \n"+dropoff+
//                "\nTime:\n"+
//                pickupTime
//        );
//        values.put(CalendarContract.Events.DESCRIPTION, pickup);
//        values.put(CalendarContract.Events.CALENDAR_ID, calID);
//        values.put(CalendarContract.Events.EVENT_TIMEZONE, "Australia/Melbourne");
        //values.put(CalendarContract.Events.EVENT_TIMEZONE, "Asia/Kolkata");
//        Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);

//        long eventID = Long.parseLong(uri.getLastPathSegment());
//
//        new AlertDialog.Builder(context)
//                .setTitle("iiRide")
//                .setMessage("Event Added Successfully.")
//                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                    }
//                })
//                .show();

        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startMillis)
                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endMillis)
                .putExtra(CalendarContract.Events.TITLE, "Upcoming trip at \nPickup Location: \n"
                        +pickup+
                        "\nDropoff Location: \n"+dropoff+
                        "\nTime:\n"+
                        pickupTime)
                .putExtra(CalendarContract.Events.DESCRIPTION, pickup)
//                .putExtra(CalendarContract.Events.EVENT_LOCATION, "The gym")
                .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);
//                .putExtra(Intent.EXTRA_EMAIL, "rowan@example.com,trevor@example.com");
        context.startActivity(intent);

    }

    public void callToDriver(String passengerContact)
    {
        TelephonyManager telMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        MyAlertDialog internetDialog = null;
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                internetDialog = new MyAlertDialog(context);
                internetDialog.showOKDialog("Sim card not available");
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                internetDialog = new MyAlertDialog(context);
                internetDialog.showOKDialog("Sim state network locked");
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                internetDialog = new MyAlertDialog(context);
                internetDialog.showOKDialog("Sim state pin required");
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                internetDialog = new MyAlertDialog(context);
                internetDialog.showOKDialog("Sim state puk required");
                break;
            case TelephonyManager.SIM_STATE_READY:
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + passengerContact));
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
                {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CALL_PHONE}, 1234);
                }
                else
                {
                    context.startActivity(intent);
                }
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                internetDialog = new MyAlertDialog(context);
                internetDialog.showOKDialog("Sim state unknown");
                break;
        }
    }



    public String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    @Override
    public int getItemCount() {
        return Pending_Job_List_Fragment.list.size();
    }


    public void NotifyPassengerForTripCancel(String BookingId, final int position) {
        aQuery = new AQuery(context);
        dialogClass = new DialogClass(context, 1);
        dialogClass.showDialog();

        String driverId = SessionSave.getUserSession(Comman.USER_ID, context);

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(WebServiceAPI.PARAM_CANCEL_ADVANCE_BOOKING_BOOKINGID,BookingId);
        params.put(WebServiceAPI.PARAM_CANCEL_ADVANCE_BOOKING_DRIVERID,driverId);


        String url = WebServiceAPI.WEB_SERVICE_CANCEL_ADVANCE_BOOKING_MANUALLY ;

        Log.e(TAG, "URL = " + url);
        Log.e(TAG, "PARAMS = " + params);

        aQuery.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                try {
                    int responseCode = status.getCode();
                    Log.e(TAG, "responseCode = " + responseCode);
                    Log.e(TAG, "Response = " + json);

                    if (json != null) {
                        if (json.has("status")) {
                            if (json.getBoolean("status")) {
                                Log.e("status", "true");
                                dialogClass.hideDialog();
                                 Pending_Job_List_Fragment.list.remove(position);
                                 if(Pending_Job_List_Fragment.list.isEmpty()){
                                    Pending_Job_List_Fragment.tv_NoDataFound.setVisibility(View.VISIBLE);
                                     notifyDataSetChanged();
                                 }else {
                                     notifyDataSetChanged();
                                 }

                                if (json.has("message"))
                                {
                                    new SnackbarUtils(Drawer_Activity.main_layout, json.getString("message"),
                                            ContextCompat.getColor(context, R.color.snakbar_color),
                                            ContextCompat.getColor(context, R.color.snackbar_text_color),
                                            ContextCompat.getColor(context, R.color.snackbar_text_color)).snackieBar().show();
                                }


                            } else {
                                Log.e("status", "false");
                                dialogClass.hideDialog();

                                if (json.has("message"))
                                {
                                    new SnackbarUtils(Drawer_Activity.main_layout, json.getString("message"),
                                            ContextCompat.getColor(context, R.color.snakbar_color),
                                            ContextCompat.getColor(context, R.color.snackbar_text_color),
                                            ContextCompat.getColor(context, R.color.snackbar_text_color)).snackieBar().show();
                                }
                            }
                        } else {
                            Log.e("status", "no status");
                            dialogClass.hideDialog();
                            if (json.has("message"))
                            {
                                new SnackbarUtils(Drawer_Activity.main_layout, json.getString("message"),
                                        ContextCompat.getColor(context, R.color.snakbar_color),
                                        ContextCompat.getColor(context, R.color.snackbar_text_color),
                                        ContextCompat.getColor(context, R.color.snackbar_text_color)).snackieBar().show();
                            }
                        }
                    } else {
                        Log.e(TAG, "getMessage = " + "null");
                        dialogClass.hideDialog();
                        new SnackbarUtils(Drawer_Activity.main_layout,context.getResources().getString(R.string.something_is_wrong),
                                ContextCompat.getColor(context, R.color.snakbar_color),
                                ContextCompat.getColor(context, R.color.snackbar_text_color),
                                ContextCompat.getColor(context, R.color.snackbar_text_color)).snackieBar().show();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception = " + e.getMessage() + "something_is_wrong");
                    dialogClass.hideDialog();
                }
            }
        }.method(AQuery.METHOD_POST).header(WebServiceAPI.HEADER_KEY_NAME, WebServiceAPI.HEADER_KEY_VALUE));
    }
}