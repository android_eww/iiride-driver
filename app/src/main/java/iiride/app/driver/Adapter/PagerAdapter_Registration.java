package iiride.app.driver.Adapter;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import android.util.Log;


import iiride.app.driver.Fragment.Register_ProfileDetail_Fragment;
import iiride.app.driver.Fragment.Register_Attachment_Fragment;
import iiride.app.driver.Fragment.Register_VehicleType_Fragment;
import iiride.app.driver.Fragment.Register_Email_Fragment;
import iiride.app.driver.Fragment.Register_Verification_Fragment;


public class PagerAdapter_Registration extends FragmentStatePagerAdapter {

    int mNumOfTabs;
    Context context;

    public PagerAdapter_Registration(Context context, FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {

        Log.e("Pager_Registration ","getItem getItem :"+position);
        switch (position) {
            case 0:
                Log.e("fragment","fragment0");
                Register_Email_Fragment fragment0 = new Register_Email_Fragment();
                return fragment0;
            case 1:
                Log.e("fragment","fragment1");
                Register_Verification_Fragment fragment1 = new Register_Verification_Fragment();
                return fragment1;

            case 2:
                Log.e("fragment","fragment2");
                Register_ProfileDetail_Fragment fragment2 = new Register_ProfileDetail_Fragment();
                return fragment2;

            case 3:
                Log.e("fragment","fragment3");
                Register_VehicleType_Fragment fragment3 = new Register_VehicleType_Fragment();
                return fragment3;

            case 5:
                Log.e("fragment","fragment5");
                Register_Attachment_Fragment fragment5 = new Register_Attachment_Fragment();
                return fragment5;

            default:
                Log.e("fragment","fragment default");
                Register_Email_Fragment fragment_default = new Register_Email_Fragment();
                return fragment_default;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

}

