package iiride.app.driver.notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Color;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;

import iiride.app.driver.Activity.Drawer_Activity;
import iiride.app.driver.Comman.Comman;
import iiride.app.driver.Comman.SessionSave;
import iiride.app.driver.R;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


public class MyAndroidFirebaseMsgService extends FirebaseMessagingService {
    private SharedPreferences pref;
    String body="", title="", type="", brief ="";

    Intent contentIntent;
    PendingIntent pendingIntent;

    Context context;



    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        body="";
        title="";
        type="";
        brief ="";

        Log.e("call", "onMessageReceived");
        //Log data to Log Cat
        Log.e("MyAndroidFCMService", "remoteMessage");
        Log.w("MyAndroidFCMService", "DATA: " + remoteMessage.getData());
        Log.w("MyAndroidFCMService", "TO: " + remoteMessage.getTo());

        Object obj_title = remoteMessage.getData().get("title");
        Object obj_body = remoteMessage.getData().get("body");
        Object obj_type = remoteMessage.getData().get("type");
        Object obj_brief = remoteMessage.getData().get("brief");


        if (obj_title != null) {
            title = String.valueOf(obj_title.toString());
            Log.e("MyAndroidFCMService", "title" + title);
        }

        if (obj_body != null) {
            body = String.valueOf(obj_body.toString());
            Log.e("MyAndroidFCMService", "body" + body);
        }

        if (obj_type != null) {
            type = String.valueOf(obj_type.toString());
            Log.e("MyAndroidFCMService", "type" + type);
        }

        if (obj_brief != null) {
            brief = String.valueOf(obj_brief.toString());
            Log.e("MyAndroidFCMService", "brief" + brief);
        }

        createNotification(title, body,type);
    }

    private void createNotification(String title, String messageBody, String type) {

        if (type!=null && !type.equalsIgnoreCase(""))
        {
            if (type.equalsIgnoreCase("BookLaterDriverNotify"))
            {
                String oldCount = SessionSave.getUserSession(Comman.NOTIFICATION_COUNT_FUTURE_BOOKING, getApplicationContext());
                if (oldCount!=null && !oldCount.trim().equalsIgnoreCase(""))
                {
                    int oldIntCount = Integer.parseInt(oldCount);
                    oldIntCount = oldIntCount + 1;
                    SessionSave.saveUserSession(Comman.NOTIFICATION_COUNT_FUTURE_BOOKING, oldIntCount+"",  getApplicationContext());
                    if (Drawer_Activity.activity!=null)
                    {
                        Drawer_Activity.activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Drawer_Activity.activity.Set_FutureBookingCount(); }
                        });
                    }
                }
                contentIntent =  new Intent(getApplicationContext(), Drawer_Activity.class);
                SessionSave.saveUserSession(Comman.NOTIFICATION_PUT_EXTRA, "BookLaterDriverNotify", getApplicationContext());
                contentIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
                pendingIntent = PendingIntent.getActivity(this, 1, contentIntent, PendingIntent.FLAG_ONE_SHOT);
            }
            else if (type.equalsIgnoreCase("RejectDispatchJobRequest"))
            {
                contentIntent =  new Intent(getApplicationContext(), Drawer_Activity.class);
                SessionSave.saveUserSession(Comman.NOTIFICATION_PUT_EXTRA, "RejectDispatchJobRequest", getApplicationContext());
                contentIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
                pendingIntent = PendingIntent.getActivity(this, 1, contentIntent, PendingIntent.FLAG_ONE_SHOT);
            }
            else if (type.equalsIgnoreCase("RejectBooking"))
            {
                contentIntent =  new Intent(getApplicationContext(), Drawer_Activity.class);
                SessionSave.saveUserSession(Comman.NOTIFICATION_PUT_EXTRA, "RejectBooking", getApplicationContext());
                contentIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
                pendingIntent = PendingIntent.getActivity(this, 1, contentIntent, PendingIntent.FLAG_ONE_SHOT);
            }
            else if (type.equalsIgnoreCase("CancelRequest"))
            {
                contentIntent =  new Intent(getApplicationContext(), Drawer_Activity.class);
                SessionSave.saveUserSession(Comman.NOTIFICATION_PUT_EXTRA, "CancelRequest", getApplicationContext());
                contentIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
                pendingIntent = PendingIntent.getActivity(this, 1, contentIntent, PendingIntent.FLAG_ONE_SHOT);
            }
            else if (type.equalsIgnoreCase("AddMoney"))
            {
                contentIntent =  new Intent(getApplicationContext(), Drawer_Activity.class);
                SessionSave.saveUserSession(Comman.NOTIFICATION_PUT_EXTRA, "AddMoney", getApplicationContext());
                contentIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
                pendingIntent = PendingIntent.getActivity(this, 1, contentIntent, PendingIntent.FLAG_ONE_SHOT);
            }
            else if (type.equalsIgnoreCase("TransferMoney"))
            {
                contentIntent =  new Intent(getApplicationContext(), Drawer_Activity.class);
                SessionSave.saveUserSession(Comman.NOTIFICATION_PUT_EXTRA, "TransferMoney", getApplicationContext());
                contentIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
                pendingIntent = PendingIntent.getActivity(this, 1, contentIntent, PendingIntent.FLAG_ONE_SHOT);
            }
            else if (type.equalsIgnoreCase("Tickpay"))
            {
                contentIntent =  new Intent(getApplicationContext(), Drawer_Activity.class);
                SessionSave.saveUserSession(Comman.NOTIFICATION_PUT_EXTRA, "Tickpay", getApplicationContext());
                contentIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
                pendingIntent = PendingIntent.getActivity(this, 1, contentIntent, PendingIntent.FLAG_ONE_SHOT);
            }
            else
            {
                pendingIntent=null;
            }
        }
        else
        {
            pendingIntent=null;
        }


        Log.e("call", "createNotification"+messageBody);
        /*NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_push_icon)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.app_logo))
                .setContentTitle(title)
                .setContentText(messageBody)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true)
                .setColor(ContextCompat.getColor(MyAndroidFirebaseMsgService.this, R.color.colorBlack))
//                .setSound(notificationSoundURI)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setContentIntent(pendingIntent)
                .setGroupSummary(true)
                .setGroup("KEY_NOTIFICATION_GROUP");

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(0, mNotificationBuilder.build());*/

        NotificationManager notificationManager1 = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager1.cancelAll();

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
        {
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            String channelId = "All Notifications";
            CharSequence channelName = getString(R.string.default_notification_channel_id);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.parseColor("#841e9b"));
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            notificationManager.createNotificationChannel(notificationChannel);

            Notification notification = new Notification.Builder(this)
                    .setSmallIcon(R.drawable.notification_small_icon)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.notification_big))
                    .setContentTitle(title)
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setColor(ContextCompat.getColor(MyAndroidFirebaseMsgService.this, R.color.colorBlack))
                    .setStyle(new Notification.BigTextStyle().bigText(brief))
                    .setContentIntent(pendingIntent)
                    .setGroupSummary(true)
                    .setGroup("KEY_NOTIFICATION_GROUP")
                    .setChannelId(channelId)
                    .build();

            notificationManager.notify(0, notification);
        }
        else
        {
            NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.notification_small_icon)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.notification_big))
                    .setContentTitle(title)
                    .setContentText(messageBody)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setAutoCancel(true)
                    .setColor(ContextCompat.getColor(MyAndroidFirebaseMsgService.this, R.color.colorBlack))
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(brief))
                    .setContentIntent(pendingIntent)
                    .setGroupSummary(true)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400})
                    .setGroup("KEY_NOTIFICATION_GROUP");

            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

            notificationManager.notify(0, mNotificationBuilder.build());
        }
    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
    }
}