package iiride.app.driver.Been.initData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InitData {

@SerializedName("status")
@Expose
private Boolean status;
@SerializedName("city_list")
@Expose
private List<CityList> cityList = null;

public Boolean getStatus() {
return status;
}

public void setStatus(Boolean status) {
this.status = status;
}

public List<CityList> getCityList() {
return cityList;
}

public void setCityList(List<CityList> cityList) {
this.cityList = cityList;
}

}
