package iiride.app.driver.Been.initData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CityList {

@SerializedName("Id")
@Expose
private String id;
@SerializedName("CityName")
@Expose
private String cityName;
@SerializedName("State")
@Expose
private String state;

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public String getCityName() {
return cityName;
}

public void setCityName(String cityName) {
this.cityName = cityName;
}

public String getState() {
return state;
}

public void setState(String state) {
this.state = state;
}

}


