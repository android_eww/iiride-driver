package iiride.app.driver.Been;


public class Pending_JobList_Been {

    private String Id, PassengerId, ModelId, DriverId, CreatedDate, TransactionId, PaymentStatus, PickupDateTime, DropOffDateTime,TripDuration,TripDistance,
            PickupLocation,DropoffLocation,NightFareApplicable,NightFare,TripFare,WaitingTime,WaitingTimeCost,TollFee,BookingCharge,Tax,
            PromoCode,Discount,SubTotal,GrandTotal,Status,Reason,PaymentType,AdminAmount,CompanyAmount,PickupLat,PickupLng,DropOffLat,
            DropOffLon,Model,PassengerName,PassengerEmail,PassengerMobileNo,HistoryType, FlightNumber, Notes, dispatcherEmail, dispatcherFullname, dispatcherMobileNo;

    public Pending_JobList_Been(String id, String passengerId, String modelId, String driverId, String createdDate, String transactionId,
                                String paymentStatus, String PickupDateTime, String DropOffDateTime, String tripDuration, String tripDistance,
                                String pickupLocation, String dropoffLocation, String nightFareApplicable, String nightFare, String tripFare,
                                String waitingTime, String waitingTimeCost, String tollFee, String bookingCharge, String tax, String promoCode,
                                String discount, String subTotal, String grandTotal, String status, String reason, String paymentType,
                                String adminAmount, String companyAmount, String pickupLat, String pickupLng, String dropOffLat, String dropOffLon,
                                String model, String passengerName, String passengerEmail, String passengerMobileNo, String historyType, String FlightNumber, String Notes,
                                String dispatcherEmail, String dispatcherFullname, String dispatcherMobileNo)
    {
        Id = id;
        PassengerId = passengerId;
        ModelId = modelId;
        DriverId = driverId;
        CreatedDate = createdDate;
        TransactionId = transactionId;
        PaymentStatus = paymentStatus;
        this.PickupDateTime = PickupDateTime;
        this.DropOffDateTime = DropOffDateTime;
        TripDuration = tripDuration;
        TripDistance = tripDistance;
        PickupLocation = pickupLocation;
        DropoffLocation = dropoffLocation;
        NightFareApplicable = nightFareApplicable;
        NightFare = nightFare;
        TripFare = tripFare;
        WaitingTime = waitingTime;
        WaitingTimeCost = waitingTimeCost;
        TollFee = tollFee;
        BookingCharge = bookingCharge;
        Tax = tax;
        PromoCode = promoCode;
        Discount = discount;
        SubTotal = subTotal;
        GrandTotal = grandTotal;
        Status = status;
        Reason = reason;
        PaymentType = paymentType;
        AdminAmount = adminAmount;
        CompanyAmount = companyAmount;
        PickupLat = pickupLat;
        PickupLng = pickupLng;
        DropOffLat = dropOffLat;
        DropOffLon = dropOffLon;
        Model = model;
        PassengerName = passengerName;
        PassengerEmail = passengerEmail;
        PassengerMobileNo = passengerMobileNo;
        HistoryType = historyType;
        this.FlightNumber= FlightNumber;
        this.Notes = Notes;
        this.dispatcherEmail=dispatcherEmail;
        this.dispatcherFullname=dispatcherFullname;
        this.dispatcherMobileNo=dispatcherMobileNo;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getPassengerId() {
        return PassengerId;
    }

    public void setPassengerId(String passengerId) {
        PassengerId = passengerId;
    }

    public String getModelId() {
        return ModelId;
    }

    public void setModelId(String modelId) {
        ModelId = modelId;
    }

    public String getDriverId() {
        return DriverId;
    }

    public void setDriverId(String driverId) {
        DriverId = driverId;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getTransactionId() {
        return TransactionId;
    }

    public void setTransactionId(String transactionId) {
        TransactionId = transactionId;
    }

    public String getPaymentStatus() {
        return PaymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        PaymentStatus = paymentStatus;
    }

    public String getPickupDateTime() {
        return PickupDateTime;
    }

    public void setPickupDateTime(String PickupDateTime) {
        PickupDateTime = PickupDateTime;
    }

    public String getDropOffDateTime() {
        return DropOffDateTime;
    }

    public void setDropOffDateTime(String DropOffDateTime) {
        DropOffDateTime = DropOffDateTime;
    }

    public String getTripDuration() {
        return TripDuration;
    }

    public void setTripDuration(String tripDuration) {
        TripDuration = tripDuration;
    }

    public String getTripDistance() {
        return TripDistance;
    }

    public void setTripDistance(String tripDistance) {
        TripDistance = tripDistance;
    }

    public String getPickupLocation() {
        return PickupLocation;
    }

    public void setPickupLocation(String pickupLocation) {
        PickupLocation = pickupLocation;
    }

    public String getDropoffLocation() {
        return DropoffLocation;
    }

    public void setDropoffLocation(String dropoffLocation) {
        DropoffLocation = dropoffLocation;
    }

    public String getNightFareApplicable() {
        return NightFareApplicable;
    }

    public void setNightFareApplicable(String nightFareApplicable) {
        NightFareApplicable = nightFareApplicable;
    }

    public String getNightFare() {
        return NightFare;
    }

    public void setNightFare(String nightFare) {
        NightFare = nightFare;
    }

    public String getTripFare() {
        return TripFare;
    }

    public void setTripFare(String tripFare) {
        TripFare = tripFare;
    }

    public String getWaitingTime() {
        return WaitingTime;
    }

    public void setWaitingTime(String waitingTime) {
        WaitingTime = waitingTime;
    }

    public String getWaitingTimeCost() {
        return WaitingTimeCost;
    }

    public void setWaitingTimeCost(String waitingTimeCost) {
        WaitingTimeCost = waitingTimeCost;
    }

    public String getTollFee() {
        return TollFee;
    }

    public void setTollFee(String tollFee) {
        TollFee = tollFee;
    }

    public String getBookingCharge() {
        return BookingCharge;
    }

    public void setBookingCharge(String bookingCharge) {
        BookingCharge = bookingCharge;
    }

    public String getTax() {
        return Tax;
    }

    public void setTax(String tax) {
        Tax = tax;
    }

    public String getPromoCode() {
        return PromoCode;
    }

    public void setPromoCode(String promoCode) {
        PromoCode = promoCode;
    }

    public String getDiscount() {
        return Discount;
    }

    public void setDiscount(String discount) {
        Discount = discount;
    }

    public String getSubTotal() {
        return SubTotal;
    }

    public void setSubTotal(String subTotal) {
        SubTotal = subTotal;
    }

    public String getGrandTotal() {
        return GrandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        GrandTotal = grandTotal;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getReason() {
        return Reason;
    }

    public void setReason(String reason) {
        Reason = reason;
    }

    public String getPaymentType() {
        return PaymentType;
    }

    public void setPaymentType(String paymentType) {
        PaymentType = paymentType;
    }

    public String getAdminAmount() {
        return AdminAmount;
    }

    public void setAdminAmount(String adminAmount) {
        AdminAmount = adminAmount;
    }

    public String getCompanyAmount() {
        return CompanyAmount;
    }

    public void setCompanyAmount(String companyAmount) {
        CompanyAmount = companyAmount;
    }

    public String getPickupLat() {
        return PickupLat;
    }

    public void setPickupLat(String pickupLat) {
        PickupLat = pickupLat;
    }

    public String getPickupLng() {
        return PickupLng;
    }

    public void setPickupLng(String pickupLng) {
        PickupLng = pickupLng;
    }

    public String getDropOffLat() {
        return DropOffLat;
    }

    public void setDropOffLat(String dropOffLat) {
        DropOffLat = dropOffLat;
    }

    public String getDropOffLon() {
        return DropOffLon;
    }

    public void setDropOffLon(String dropOffLon) {
        DropOffLon = dropOffLon;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String model) {
        Model = model;
    }

    public String getPassengerName() {
        return PassengerName;
    }

    public void setPassengerName(String passengerName) {
        PassengerName = passengerName;
    }

    public String getPassengerEmail() {
        return PassengerEmail;
    }

    public void setPassengerEmail(String passengerEmail) {
        PassengerEmail = passengerEmail;
    }

    public String getPassengerMobileNo() {
        return PassengerMobileNo;
    }

    public void setPassengerMobileNo(String passengerMobileNo) {
        PassengerMobileNo = passengerMobileNo;
    }

    public String getHistoryType() {
        return HistoryType;
    }

    public void setHistoryType(String historyType) {
        HistoryType = historyType;
    }

    public void setFlightNumber(String flightNumber) {
        FlightNumber = flightNumber;
    }
    public String getFlightNumber() {
        return FlightNumber;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }
    public String getNotes() {
        return Notes;
    }


    public void setDispatherEmail(String dispatcherEmail) {
        this.dispatcherEmail = dispatcherEmail;
    }
    public String getDispatherEmail() {
        return dispatcherEmail;
    }

    public void setDispatherFullname(String dispatcherFullname) {
        this.dispatcherFullname = dispatcherFullname;
    }
    public String getDispatherFullname() {
        return dispatcherFullname;
    }

    public void setDispatherMobileNo(String dispatcherMobileNo) {
        this.dispatcherMobileNo = dispatcherMobileNo;
    }
    public String getDispatherMobileNo() {
        return dispatcherMobileNo;
    }

}
