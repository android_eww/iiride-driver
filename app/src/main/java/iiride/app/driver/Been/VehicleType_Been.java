package iiride.app.driver.Been;



public class VehicleType_Been
{
    private String id, name, description, Status;

    public VehicleType_Been(String id, String name, String description, String Status) {

        this.id = id;
        this.name =  name;
        this.description =  description;
        this.Status =  Status;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescription() {
        return description;
    }

    public void setStatus(String status) {
        Status = status;
    }
    public String getStatus() {
        return Status;
    }
}
