package iiride.app.driver.Been;


public class Transfer_History_Been {

    private String History_Name, HistoryDate_Time, History_Money, Status;

    public Transfer_History_Been(String History_Name, String HistoryDate_Time, String History_Money, String Status)
    {
        this.History_Name = History_Name;
        this.HistoryDate_Time = HistoryDate_Time;
        this.History_Money = History_Money;
        this.Status = Status;
    }

    public void setHistory_Name(String history_Name) {
        History_Name = history_Name;
    }
    public String getHistory_Name() {
        return History_Name;
    }

    public void setHistoryDate_Time(String historyDate_Time) {
        HistoryDate_Time = historyDate_Time;
    }
    public String getHistoryDate_Time() {
        return HistoryDate_Time;
    }

    public void setHistory_Money(String history_Money) {
        History_Money = history_Money;
    }
    public String getHistory_Money() {
        return History_Money;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getStatus() {
        return Status;
    }
}
