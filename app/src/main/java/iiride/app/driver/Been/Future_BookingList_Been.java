package iiride.app.driver.Been;

public class Future_BookingList_Been {


    private String tripId, pickupDateTime, pickupLocation, dropoffLocation, companyId, passengerId, tripDistance, modelId,
            passengerName, passengerContact, model, FlightNumber, Notes, PaymentType, dispatherEmail, dispatherFullname, dispatherMobileNo, TripFare,DropoffSuburb,PickupSuburb,EstDriverEarning;

    public Future_BookingList_Been(String tripId, String pickupDateTime, String pickupLocation, String dropoffLocation, String companyId, String passengerId, String tripDistance, String modelId
            , String passengerName, String passengerContact, String model, String FlightNumber, String Notes, String PaymentType, String dispatherEmail, String dispatherFullname
            , String dispatherMobileNo, String TripFare,String DropoffSuburb,String PickupSuburb,String EstDriverEarning)
    {
        this.tripId= tripId;
        this.pickupDateTime= pickupDateTime;
        this.pickupLocation= pickupLocation;
        this.dropoffLocation= dropoffLocation;
        this.companyId= companyId;
        this.passengerId= passengerId;
        this.tripDistance= tripDistance;
        this.modelId= modelId;
        this.passengerName= passengerName;
        this.passengerContact= passengerContact;
        this.model= model;
        this.FlightNumber= FlightNumber;
        this.Notes= Notes;
        this.PaymentType=PaymentType;
        this.dispatherEmail=dispatherEmail;
        this.dispatherFullname=dispatherFullname;
        this.dispatherMobileNo=dispatherMobileNo;
        this.TripFare=TripFare;
        this.DropoffSuburb = DropoffSuburb;
        this.PickupSuburb = PickupSuburb;
        this.EstDriverEarning = EstDriverEarning;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }
    public String getTripId() {
        return tripId;
    }

    public void setPickupDateTime(String pickupDateTime) {
        this.pickupDateTime = pickupDateTime;
    }
    public String getPickupDateTime() {
        return pickupDateTime;
    }

    public void setPickupLocation(String pickupLocation) {
        this.pickupLocation = pickupLocation;
    }
    public String getPickupLocation() {
        return pickupLocation;
    }

    public void setDropoffLocation(String dropoffLocation) {
        this.dropoffLocation = dropoffLocation;
    }
    public String getDropoffLocation() {
        return dropoffLocation;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
    public String getCompanyId() {
        return companyId;
    }

    public void setPassengerId(String passengerId) {
        this.passengerId = passengerId;
    }
    public String getPassengerId() {
        return passengerId;
    }

    public void setTripDistance(String tripDistance) {
        this.tripDistance = tripDistance;
    }
    public String getTripDistance() {
        return tripDistance;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }
    public String getModelId() {
        return modelId;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }
    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerContact(String passengerContact) {
        this.passengerContact = passengerContact;
    }
    public String getPassengerContact() {
        return passengerContact;
    }

    public void setModel(String model) {
        this.model = model;
    }
    public String getModel() {
        return model;
    }

    public void setFlightNumber(String flightNumber) {
        FlightNumber = flightNumber;
    }
    public String getFlightNumber() {
        return FlightNumber;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }
    public String getNotes() {
        return Notes;
    }

    public void setPaymentType(String paymentType) {
        PaymentType = paymentType;
    }
    public String getPaymentType() {
        return PaymentType;
    }

    public void setDispatherEmail(String dispatherEmail) {
        this.dispatherEmail = dispatherEmail;
    }
    public String getDispatherEmail() {
        return dispatherEmail;
    }

    public void setDispatherFullname(String dispatherFullname) {
        this.dispatherFullname = dispatherFullname;
    }
    public String getDispatherFullname() {
        return dispatherFullname;
    }

    public void setDispatherMobileNo(String dispatherMobileNo) {
        this.dispatherMobileNo = dispatherMobileNo;
    }
    public String getDispatherMobileNo() {
        return dispatherMobileNo;
    }

    public void setTripFare(String tripFare) {
        TripFare = tripFare;
    }
    public String getTripFare() {
        return TripFare;
    }

    public String getDropoffSuburb() {
        return DropoffSuburb;
    }

    public void setDropoffSuburb(String dropoffSuburb) {
        DropoffSuburb = dropoffSuburb;
    }

    public String getPickupSuburb() {
        return PickupSuburb;
    }

    public void setPickupSuburb(String pickupSuburb) {
        PickupSuburb = pickupSuburb;
    }

    public String getEstDriverEarning() {
        return EstDriverEarning;
    }

    public void setEstDriverEarning(String estDriverEarning) {
        EstDriverEarning = estDriverEarning;
    }
}
