package iiride.app.driver.Been;


public class CreditCard_List_Been {
    private String Id, CardType, CardNo, CardName;


    public CreditCard_List_Been(String Id, String CardName, String CardNo, String CardType) {
        this.Id=Id;
        this.CardName = CardName;
        this.CardNo = CardNo;
        this.CardType = CardType;
    }

    public void setId(String id) {
        Id = id;
    }
    public String getId() {
        return Id;
    }

    public void setCardName(String cardName) {
        CardName = cardName;
    }
    public String getCardName() {
        return CardName;
    }

    public void setCardNo(String cardNo) {
        CardNo = cardNo;
    }
    public String getCardNo() {
        return CardNo;
    }

    public void setCardType(String cardType) {
        CardType = cardType;
    }
    public String getCardType() {
        return CardType;
    }
}
