package iiride.app.driver.Been;



public class Dispatched_JobList_Been {

    private String DispatchJob_Id="", PickupDateTime="", PickupLocation="", DropoffLocation="", CompanyId="", PassengerId="", TripDistance=""
            , ModelId="", PassengerName="", PassengerContact="", Model="", TripFare, Tax, SubTotal, GrandTotal, PaymentType, PassengerEmail, CreatedDate, Status,  FlightNumber, Notes
            , dispatcherEmail, dispatcherFullname, dispatcherMobileNo, BookingType;

    public Dispatched_JobList_Been(String dispatchJob_Id, String pickupDateTime, String pickupLocation, String dropoffLocation, String companyId, String passengerId
            , String tripDistance, String modelId, String passengerName, String passengerContact, String model, String TripFare, String Tax, String SubTotal, String GrandTotal
            , String PaymentType, String PassengerEmail, String CreatedDate, String Status, String FlightNumber, String Notes, String dispatcherEmail, String dispatcherFullname
            , String dispatcherMobileNo, String BookingType)
    {
        this.DispatchJob_Id = dispatchJob_Id;
        this.PickupDateTime = pickupDateTime;
        this.PickupLocation = pickupLocation;
        this.DropoffLocation = dropoffLocation;
        this.CompanyId = companyId;

        this.PassengerId = passengerId;
        this.TripDistance = tripDistance;
        this.ModelId = modelId;
        this.PassengerName = passengerName;
        this.PassengerContact = passengerContact;

        this.Model = model;
        this.TripFare = TripFare;
        this.Tax = Tax;
        this.SubTotal = SubTotal;
        this.GrandTotal = GrandTotal;
        this.PaymentType = PaymentType;
        this.PassengerEmail = PassengerEmail;
        this.CreatedDate=CreatedDate;
        this.Status=Status;
        this.FlightNumber= FlightNumber;
        this.Notes= Notes;
        this.dispatcherEmail= dispatcherEmail;
        this.dispatcherFullname= dispatcherFullname;
        this.dispatcherMobileNo= dispatcherMobileNo;
        this.BookingType= BookingType;
    }

    public void setDispatchJob_Id(String dispatchJob_Id) {
        DispatchJob_Id = dispatchJob_Id;
    }
    public String getDispatchJob_Id() {
        return DispatchJob_Id;
    }

    public void setPickupDateTime(String pickupDateTime) {
        PickupDateTime = pickupDateTime;
    }
    public String getPickupDateTime() {
        return PickupDateTime;
    }

    public void setPickupLocation(String pickupLocation) {
        PickupLocation = pickupLocation;
    }
    public String getPickupLocation() {
        return PickupLocation;
    }

    public void setDropoffLocation(String dropoffLocation) {
        DropoffLocation = dropoffLocation;
    }
    public String getDropoffLocation() {
        return DropoffLocation;
    }

    public void setCompanyId(String companyId) {
        CompanyId = companyId;
    }
    public String getCompanyId() {
        return CompanyId;
    }

    public void setPassengerId(String passengerId) {
        PassengerId = passengerId;
    }
    public String getPassengerId() {
        return PassengerId;
    }

    public void setTripDistance(String tripDistance) {
        TripDistance = tripDistance;
    }
    public String getTripDistance() {
        return TripDistance;
    }

    public void setModelId(String modelId) {
        ModelId = modelId;
    }
    public String getModelId() {
        return ModelId;
    }

    public void setPassengerName(String passengerName) {
        PassengerName = passengerName;
    }
    public String getPassengerName() {
        return PassengerName;
    }

    public void setPassengerContact(String passengerContact) {
        PassengerContact = passengerContact;
    }
    public String getPassengerContact() {
        return PassengerContact;
    }

    public void setModel(String model) {
        Model = model;
    }
    public String getModel() {
        return Model;
    }



    public void setTripFare(String tripFare) {
        TripFare = tripFare;
    }
    public String getTripFare() {
        return TripFare;
    }

    public void setTax(String tax) {
        Tax = tax;
    }
    public String getTax() {
        return Tax;
    }

    public void setSubTotal(String subTotal) {
        SubTotal = subTotal;
    }
    public String getSubTotal() {
        return SubTotal;
    }

    public void setGrandTotal(String grandTotal) {
        GrandTotal = grandTotal;
    }
    public String getGrandTotal() {
        return GrandTotal;
    }

    public void setPaymentType(String paymentType) {
        PaymentType = paymentType;
    }
    public String getPaymentType() {
        return PaymentType;
    }

    public void setPassengerEmail(String passengerEmail) {
        PassengerEmail = passengerEmail;
    }
    public String getPassengerEmail() {
        return PassengerEmail;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }
    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setStatus(String status) {
        Status = status;
    }
    public String getStatus() {
        return Status;
    }

    public void setFlightNumber(String flightNumber) {
        FlightNumber = flightNumber;
    }
    public String getFlightNumber() {
        return FlightNumber;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }
    public String getNotes() {
        return Notes;
    }

    public void setDispatcherFullname(String dispatcherFullname) {
        this.dispatcherFullname = dispatcherFullname;
    }
    public String getDispatcherFullname() {
        return dispatcherFullname;
    }

    public void setDispatcherEmail(String dispatcherEmail) {
        this.dispatcherEmail = dispatcherEmail;
    }
    public String getDispatcherEmail() {
        return dispatcherEmail;
    }

    public void setDispatcherMobileNo(String dispatcherMobileNo) {
        this.dispatcherMobileNo = dispatcherMobileNo;
    }
    public String getDispatcherMobileNo() {
        return dispatcherMobileNo;
    }

    public void setBookingType(String bookingType) {
        BookingType = bookingType;
    }
    public String getBookingType() {
        return BookingType;
    }
}
