package iiride.app.driver.Been;


public class Weekly_Earning_Been {

    String dayName, dayValue;

    public Weekly_Earning_Been(String dayName, String dayValue) {
        this.dayName = dayName;
        this.dayValue = dayValue;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }
    public String getDayName() {
        return dayName;
    }

    public void setDayValue(String dayValue) {
        this.dayValue = dayValue;
    }
    public String getDayValue() {
        return dayValue;
    }
}
