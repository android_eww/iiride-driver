package iiride.app.driver.Been;



public class Register_vehi_deliv_Type_Been {

    String Id, bicycleName, Description, status;

    public Register_vehi_deliv_Type_Been(String Id, String bicycleName, String Description, String status) {

        this.Id = Id;
        this.bicycleName = bicycleName;
        this.Description = Description;
        this.status =  status;
    }

    public void setId(String id) {
        Id = id;
    }
    public String getId() {
        return Id;
    }

    public void setBicycleName(String bicycleName) {
        this.bicycleName = bicycleName;
    }

    public String getBicycleName() {
        return bicycleName;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getDescription() {
        return Description;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}

